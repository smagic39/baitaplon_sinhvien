<?php
/**
 * The template for displaying featured posts on home page
 *
 * @package deTube
 * @subpackage Template
 * @since deTube 1.0
 */
?>

<div class="home-featured wall">
<div class="wrap cf">
			
<?php
	$args = (array)get_option('dp_home_featured');
	$args = dp_parse_query_args($args);
	$autoplay = !empty($args['autoplay']) ? true : false;
	$query = new WP_Query($args); 
?>
	
<?php if($query->have_posts()): ?>

	<?php
		/* Load scripts only when needed */
		wp_enqueue_script('jquery-carousel');
		wp_enqueue_script('jquery-slides'); 
		wp_enqueue_script('jplayer'); 
	?>

	<div class="slider">
	<div class="slider-wrap">
		<?php // jQuery slides pluin will add tag '<div class="slides">' here ?>
			<?php $items = ''; $i = 0; while ($query->have_posts()) : $query->the_post(); global $post; $i++; ?>
			<div class="slide <?php echo is_video() ? 'item-video' : 'item-post'; ?>" data-id="<?php the_ID(); ?>">
				<?php
					$thumb_size = 'custom-large';
					dp_thumb_html($thumb_size);
				?>
			
				<div class="caption">
					<h2 class="title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php printf(__('Permalink to %s', 'dp'), get_the_title()); ?>"><?php the_title(); ?></a></h2>
				</div>
			</div><!-- end .slide -->
			<?php endwhile; ?>
		<?php // </div><!-- end .slides--> ?>
	</div><!-- end .slider-wrap -->
	</div><!-- end .slider -->
		
	<div class="carousel">
	<div class="carousel-container">
		<div class="carousel-clip">
			<ul class="slider-pagination carousel-list">
				<?php $items = ''; $i = 0; while ($query->have_posts()) : $query->the_post(); global $post; ?>
				<li data-id="<?php the_ID(); ?>" class="<?php echo is_video() ? 'item-video' : 'item-post'; ?>">
				<div class="inner">
					<?php
					$thumb_size = 'custom-small';
					dp_thumb_html($thumb_size);
					?>
			
					<div class="data">
						<h2 class="title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php printf(__('Permalink to %s', 'dp'), get_the_title()); ?>"><?php the_title(); ?></a></h2>
			
						<p class="meta">
							<span class="time"><?php printf(__('%s ago', 'dp'), human_time(get_the_time('U'))); ?></span>
						</p>
					</div>
				</div>
				</li>
				<?php $i++; endwhile; ?>
			</ul>
		</div><!-- end .carousel-clip -->
		
		<div class="carousel-prev"></div>
		<div class="carousel-next"></div>
	</div><!-- end .carousel-container -->
	</div><!-- end .carousel -->

<?php endif; wp_reset_query(); ?>

</div><!-- end .wrap -->
</div><!-- end #wall -->