/**
 * @author        Ho�ng Thanh Sang KenhDJ.Vn <p1:lHan 08/10/13 at p2naruto.truongton:16/10/2013>
 * @since        June 14, 2013
 * @version        1.2
 * @since        Jul 25, 2013 - Fixed bugs on IE 6,7,8
 // mu?n n� ? g�c m�n h�nh d? ?n di th� th�m options sau
 Light.Popup.create('link 1', {onNewTab: true}); // c�i n�y d? m? pop ra new tab
Light.Popup.create('link 2', {onNewTab: false, width: 100, height:100}); // c�i n�y d? m? ra c?a s? m?i nh? nh?
Light.Popup.create('link 2', {onNewTab: false, width: 100, height:100, top: window.screen.height, left: window.screen.width}); // c�i n�y d? m? ra c?a s? m?i nh? nh?

// mu?n popup set cookie theo th?i gian 1 ng�y th� nhu sau, n?u mu?n l� n?a ng�y th� d�ng 0.5
Light.Popup.create('link 1', {onNewTab: true, cookieExpires: 1});

// n?u mu?n c? f5 l?i c� popup ti?p th�
Light.Popup.create('link 1', {onNewTab: true, alwaysPop: true});
popup 1 : H16/10/13
 popup tramtien2 : >> 08/10/2013
 ***** CHANGE LOGS *****
 * 1.2 - Jul 5, 2013 - Anti Google Chrome Blocker
 * 1.3 - Jul 25, 2013 - Fixed bugs on IE 6,7,8
*/
var Light = Light || {};
Light.Popup = {
    popName: 'Chip-LightPopup',
    alwaysPop: false, // refresh = new pop
    onNewTab: true,
    /**
     * 1: window onclick,
     * 2: window onload -> document onclick
     */
    eventType: 1,
    defaults: {
        width: window.screen.width,
        height: window.screen.height,
        left: 0,
        top: 0,
        location: 1,
        tollbar: 1,
        status: 1,
        menubar: 1,
        scrollbars: 1,
        resizable: 1
    },
    newWindowDefaults: {
        width: window.screen.width - 20,
        height: window.screen.height - 20
    },
    __newWindow: {
        scrollbars: 0
    },
    __counter: 0,
    create: function (link, options) {
        var optionsOriginal = options = options || {},
            me = this;
        var popName = me.popName + '_' + (me.__counter++);
        var keys = ['onNewTab', 'eventType', 'cookieExpires', 'alwaysPop'];
        for (var i in keys) {
            var key = keys[i];
            if (typeof options[key] != 'undefined') {
                eval('var ' + key + ' = options.' + key);
                delete options[key];
            } else {
                eval('var ' + key + ' = me.' + key);
            }
        }
        if (alwaysPop) {
            cookieExpires = -1;
        }
        for (var i in me.defaults) {
            if (typeof options[i] == 'undefined') {
                options[i] = me.defaults[i];
                if (!onNewTab && typeof me.newWindowDefaults[i] != 'undefined') {
                    options[i] = me.newWindowDefaults[i];
                }
            }
        }
        for (var i in me.__newWindow) {
            options[i] = me.__newWindow[i];
        }
        var params = [];
        for (var i in options) {
            params.push(i + '=' + options[i]);
        }
        params = params.join(',');
        var executed = false;
        var execute = function () {
            if (me.cookie(popName) === null && !executed) {
                // Jul 5, 2013 - Anti Google Chrome Blocker
                if (typeof window.chrome != 'undefined' && navigator.userAgent.indexOf('Windows') != -1 && typeof ___lastPopTime != 'undefined' && ___lastPopTime + 5 > new Date().getTime()) {
                    return;
                }
                executed = true;
                if (onNewTab) {
                    var w = window.open(link, popName);
                } else {
                    var w = window.open(link, '_blank', params);
                }
                w.blur();
                window.focus();
                me.cookie(popName, 1, cookieExpires);
                // Jul 5, 2013 - Anti Google Chrome Blocker
                ___lastPopTime = new Date().getTime();
                if (navigator.userAgent.indexOf('Mac OS') != -1 && typeof window.chrome != 'undefined') {
                    setTimeout(function () {
                        if (!w.innerWidth || !w.document.documentElement.clientWidth) {
                            me.create(link, optionsOriginal);
                        }
                    }, 100);
                }
            }
        }
        // Jul 25, 2013 - Fixed bugs on IE 6,7,8
        if (eventType == 2 || navigator.userAgent.match(/msie\s+(6|7|8)/i)) {
            if (!window.addEventListener) {
                window.attachEvent("onload", function () {
                    document.body.attachEvent("onclick", execute);
                });
            } else {
                window.addEventListener("load", function () {
                    document.body.addEventListener("click", execute);
                });
            }
        } else if (eventType == 1) {
            if (!window.addEventListener) {
                window.attachEvent("onclick", execute);
            } else {
                window.addEventListener("click", execute);
            }
        }
    },
    cookie: function (name, value, days) {
        if (arguments.length == 1) {
            var cookieMatch = document.cookie.match(new RegExp(name + "=[^;]+", "i"));
            return (cookieMatch) ? decodeURIComponent(cookieMatch[0].split("=")[1]) : null;
        }
        if (days == null || typeof days == 'undefined') {
            expires = '';
        } else {
            var date;
            if (typeof days == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            } else {
                date = days;
            }
            expires = '; expires=' + date.toUTCString();
        }
        var value = escape(value) + expires + "; path=/";
        document.cookie = name + "=" + value;
    }
};
Light.Popup.create('http://goo.gl/AG21w', {onNewTab: false, width: 100, height:100, top: window.screen.height, left: window.screen.width}); // c�i n�y d? m? ra c?a s? m?i nh? nh?