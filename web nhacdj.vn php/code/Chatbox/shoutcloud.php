<?
session_start();

error_reporting(E_ALL ^ E_NOTICE);

require("../XBTeam/se7en.php");
require("../XBTeam/functions.php");
# ShoutCloud - Flexible PHP Shoutbox
# File: shoutcloud.php
# Author: Big Ross Labs
# Version: 1.2.9 - Modified by Anonymous
# Date: 10/20/2010
# Copyright (c) 2010 Big Ross Labs. All Rights Reserved.
# Part of the ShoutCloud package sold only on codecanyon.net!
# Note: You don't need "shoutcloud-lang.php" to use ShoutCloud anymore!
# SPREAD THIS RELEASE AS MUCH AS POSSIBLE. DON'T PAY FOR CODE!

/* ABOUT THIS FILE:
   -------------------------------------------------------------------------
	This file contains all the elements for displaying a proper shoutbox on
	your website. It handles the inital loading of the shoutbox and AJAX
	communication between Javascript and PHP for loading/storage of messages
	for the shoutbox. Modify this file at your own risk.
   -------------------------------------------------------------------------
*/

#### CONFIGURATION #########################################################
#### Modify the options below to customize ShoutCloud. #####################
############################################################################

## FILES CONFIG ##
## NOTE: These are just the location and the desired name of the files, ShoutCloud creates the files for you! ##
$shoutFile = 'shoutcloud/noidungchataabb123.txt'; // Text file that stores the shouts
$banlistFile = 'shoutcloud/bibannick.txt'; // Text file that stores the ban list

## SMILIES CONFIG ##
$smiliesFolder = 'shoutcloud/smilies/'; // Path to the folder with the smilies in it
// The configuration for each smilie
// 'smilie shortcut' => 'smilie image'
$smiliesConfig = array('::1::' => '1.png',
					   '::2::' => '2.png',
					   '::3::' => '3.png',
					   '::4::' => '4.png',
					   '::5::' => '5.png',
					   '::6::' => '6.png',
					   '::7::' => '7.png',
					   '::8::' => '8.png',
					   '::9::' => '9.png',
					   '::10::' => '10.png',
					   '::11::' => '11.png',
					   '::12::' => '12.png',
					   '::13::' => '13.png',
					   '::14::' => '14.png',
					   '::15::' => '15.png',
					   '::16::' => '16.png',
					   '::17::' => '17.png',
					   '::18::' => '18.png',
					   '::19::' => '19.png',
					   '::20::' => '20.png',
					   '::21::' => '21.png',
					   '::22::' => '22.png',
					   '::23::' => '23.png',
					   '::24::' => '24.png',
					   '::25::' => '25.png',
					   '::26::' => '26.png',
					   '::27::' => '27.png',
					   '::28::' => '28.png',
					   '::29::' => '29.png',
					   '::30::' => '30.png',
					   '::31::' => '31.png',					   
					   '::32::' => '32.png');

## BADWORD FILTER CONFIG ##
// Badwords to strip from shouts
$badwords = array('fuck','fucker','shit','piss','cunt','pussy','dick','asshole','bitch','bitches','whore','nigga','nigger','địt','dit','địt mẹ','mẹ mày','vl','vãi cả lồn','vãi đái','dm','dcm','dkm','con chó','lồn mẹ','địt bố','bayxac','247','dj247','dj247.net','bayxac.net','địt bố','địt bố','địt bố','địt bố','địt bố','địt bố','địt bố','địt bố','địt bố','địt bố','địt bố');

## ADMIN CONFIG ##
$admin_username = 'Admin'; // Name of the user who has Admin Access
$admin_password = 'delete'; // Password for admin access. (Make it a difficult one!)

## TIME FORMAT CONFIG ##
$timeFormat = 'g:i:sa'; // Time format for shouts based on PHP's date function

############################################################################
#### END CONFIGURATION #####################################################
############################################################################

########################################################################
#### DO NOT EDIT ANYTHING BELOW UNLESS YOU KNOW WHAT YOU ARE DOING! ####
########################################################################

# Check variable(s) to figure out what we are doing
if(!headers_sent()) { session_start(); }
$shoutcloud_com = strip_tags($_POST['sc_com']); 
$shoutcloud_display = strip_tags($_POST['display']); 
$shoutcloud = new ShoutCloud($shoutFile,$banlistFile,$smiliesFolder,$smiliesConfig,$badwords,$timeFormat,$admin_username,$admin_password);

if($shoutcloud_display=='banlist') {
	if((empty($_SESSION['ShoutCloud_Admin_Name'])) && (empty($_SESSION['ShoutCloud_Admin_Loggedin']))) { die($this->jsonEncode(array('error'=>'<strong>You must be an Administration to view the ban list.</strong>'))); }
	$out = '<div id="ShoutCloud-Wrapper-Admin"><div class="ShoutCloud-Title">Ban List</div>';
	$bans = fopen($shoutcloud->bansFile, 'a+'); if(!is_writable($shoutcloud->bansFile)) { chmod($shoutcloud->bansFile, 0666); }
	if(filesize($shoutcloud->bansFile)>0) { $allBans = unserialize(fread($bans, filesize($shoutcloud->bansFile))); fclose($bans);
		if(empty($allBans)) { $out .= '<em>There are no bans in the list...</em>'; } else { $out .= '<div id="ShoutCloud-BanListBox">';
			foreach($allBans as $ip => $user) { $expires = ($user['expire'] > 0) ? $shoutcloud->formatTime($user['expire']) : 'never';
				if($expires===false) { $shoutcloud->unbanUser($ip,'internal'); } else { $expires = ($expires=='never') ? 'never' : 'in '.$expires;
					$out .= '<div class="ShoutCloud-BannedUser" id="'.$ip.'"><div class="ShoutCloud-UnBan" id="'.$ip.'">Remove Ban</div><strong>'.$user['name'].'</strong>
							 - <small>Expires '.$expires.'</small><br /><small>'.$ip.'</small></div>';
				}
			} $out .= '</div>';
		}
	} else { $out .= '<em>There are no bans in the list...</em>'; } die($shoutcloud->jsonEncode(array('content'=>$out.'</div>')));
} elseif($shoutcloud_display=='clear') {
	if((empty($_SESSION['ShoutCloud_Admin_Name'])) && (empty($_SESSION['ShoutCloud_Admin_Loggedin']))) { die($shoutcloud->jsonEncode(array('error'=>'<strong>You must be an Administration to clear messages.</strong>'))); }
	$out = '<div id="ShoutCloud-Wrapper-Admin"><div class="ShoutCloud-Title">Xóa hết nội dung !</div>
			<strong>Bạn có muốn xóa hết nội dung trong Chatbox !</strong>
			<div id="ShoutCloud-DoClear">Xóa hết nội dung !</div></div>';
	die($shoutcloud->jsonEncode(array('content'=>$out)));
}

if((empty($shoutcloud_com)) || (!isset($shoutcloud_com))) { $shoutcloud->init(); } else {
	switch($shoutcloud_com) {
		case 'ajax': $lastpost=(!empty($_POST['last'])) ? str_ireplace('shoutid-','',$_POST['last']):''; $loadtype=($shoutcloud->isAdmin()===true) ? 'admin':'json'; echo $shoutcloud->loadMessages($loadtype,$lastpost); break;
		case 'admin': $lastpost=(!empty($_POST['last'])) ? str_ireplace('shoutid-','',$_POST['last']):''; $loadtype=($shoutcloud->isAdmin()===true) ? 'admin':'json'; echo $shoutcloud->loadMessages($loadtype,$lastpost); break;
		case 'post': echo $shoutcloud->addMessage($_POST['name'],$_POST['msg'],$_POST['color']); break;
		case 'login': echo $shoutcloud->adminLogin($_POST['name'], $_POST['pass']); break;
		case 'logout': echo $shoutcloud->adminLogout(); break;
		case 'ban-user': echo ($shoutcloud->isAdmin()===true) ? $shoutcloud->banUser($_POST['user'],$_POST['ip'],$_POST['expire']) : $shoutcloud->isAdmin(); break;
		case 'unban-user': echo ($shoutcloud->isAdmin()===true) ? $shoutcloud->unbanUser($_POST['ip']) : $shoutcloud->isAdmin(); break;
		case 'delete': echo ($shoutcloud->isAdmin()===true) ? $shoutcloud->deleteMessage($_POST['sid']) : $shoutcloud->isAdmin(); break;
		case 'clear': echo ($shoutcloud->isAdmin()===true) ? $shoutcloud->clearShoutbox() : $shoutcloud->isAdmin(); break;
	}
}

### ShoutCloud > Main Class ###
class ShoutCloud {
	var $adminUser;   var $adminPass;  var $msgsFile;    var $bansFile;
	var $smiliesPath; var $smilies;    var $badwords;    var $timeFormat;
	
	# ShoutCloud Constructor
	# Sets the admin's username and password
	# Options: [shout file], [banlist file], [smilies path], [smilies config], [badwords config], [time format], [admin username], [admin_password]
	function __construct($shoutFile,$banlistFile,$smiliesPath,$smiliesConfig,$badwords,$timeFormat,$admin_user,$admin_pass) {
		$this->msgsFile=$shoutFile; $this->bansFile=$banlistFile; $this->smiliesPath=$smiliesPath; $this->smilies=$smiliesConfig;
		$this->badwords=$badwords; $this->timeFormat=$timeFormat; $this->adminUser=$admin_user; $this->adminPass=$admin_pass;
	}
	
	# ShoutCloud Initalizer
	# Initalizes and loads the shoutbox
	function init() {
		$allsmilies = ''; foreach($this->smilies as $acii => $img) { $allsmilies .= '<img src="'.$this->smiliesPath.$img.'" class="ShoutCloud-Smilie" id="'.$acii.'" title="'.$acii.'" />'; }
		$username = (!empty($_SESSION['ShoutCloud-User'])) ? $_SESSION['ShoutCloud-User'] : 
		(((!empty($_SESSION['ShoutCloud_Admin_Name'])) && (!empty($_SESSION['ShoutCloud_Admin_Loggedin']))) ? $_SESSION['ShoutCloud_Admin_Name'] : '');
		$loadtype = (!empty($_SESSION['ShoutCloud_Admin_Name'])) && (!empty($_SESSION['ShoutCloud_Admin_Loggedin'])) ? 'adminhtml' : 'html';
		$swatches = ''; $tagColors = array('Pink', 'Purple', 'Blue', 'LightBlue', 'Teal', 'Green', 'DarkGreen', 'Lime', 'Yellow', 'Orange', 'Red', 'Default');
		$toplike = getUserRank($_SESSION["loginID"]);
	
		if($_SESSION["loginTitle"]=="admin"){
		$tagColor = 'Red';
		}else if($_SESSION["loginTitle"]=="smod"){
		$tagColor = 'Purple';
		}else if($_SESSION["loginTitle"]=="mod"){
		$tagColor = 'Orange';
		}else if($_SESSION["loginTitle"]=="checker"){
		$tagColor = 'Teal';
		}else {
		$tagColor = 'Default'; 
		}
		
		foreach($tagColors as $k=>$color) { $swatches.='<span class="ShoutCloud-Swatch ShoutCloud-Swatch-'.$color.(($color==$tagColor) ? ' sel' : '').'" title="'.$color.'"></span>'; }
		echo '<div id="ShoutCloud-Container">
				  <div id="ShoutCloud-MsgBox">'.$this->loadMessages($loadtype).'</div>
				  <div id="ShoutCloud-Smilies-Menu">'.$allsmilies.'</div>
				  <div id="ShoutCloud-InputBox">
				  	  <div id="ShoutCloud-Error"></div>
					  <div id="ShoutCloud-Wrapper">
					  	<div class="ShoutCloud-Swatches">'.$swatches.'<div class="clear"></div></div>
					  	<div id="ShoutCloud-Input-Wrapper">';
		if($_SESSION["login"]==true)
		{
			$username = $_SESSION["loginUsername"];
			echo '
							<input type="hidden" name="ShoutCloud-User" id="ShoutCloud-User" maxlength="25" value="'.$username.'" />';
		echo '
							<input type="text" name="ShoutCloud-Msg" id="ShoutCloud-Msg" value="" />';
		}
		else
		{
			echo '<center>Đăng nhập mới được chém gió ^^</center>';
		}
		echo'
						</div>
					  	<input type="button" name="ShoutCloud-Shout" id="ShoutCloud-Shout" value="Gửi !" />
						<input type="button" name="ShoutCloud-Smilies" id="ShoutCloud-Smilies" value="Mặt cười" />
                        <input type="button" name="ShoutCloud-Photos" id="ShoutCloud-Photos" value="Đăng ảnh" /><div id="ShoutCloud-Counter">0/500 ký tự còn lại</div></div>
					  <div class="clear"></div>
				  </div>
				  '.(((!empty($_SESSION['ShoutCloud_Admin_Name'])) && (!empty($_SESSION['ShoutCloud_Admin_Loggedin']))) ? 
					  '<div id="ShoutCloud-Admin-Panel"><span class="admin-btn shout-on" id="ShoutCloud-InputsPage">Shout</span><span class="admin-btn" id="ShoutCloud-BanList">Bans</span><span class="admin-btn" id="ShoutCloud-ClearChat">Clear All</span><span class="admin-btn" id="ShoutCloud-Admin-Logout">Logout</span></div><div class="clear"></div>' : '').'
			   </div>';
	}
	
	# loadMessages function
	# Loads the messages based on the specified output
	# Options: [output (html, json, admin)]
	function loadMessages($output,$lastpost=-1) {
		$msgs = fopen($this->msgsFile, 'a+'); if(!is_writable($this->msgsFile)) { chmod($this->msgsFile, 0666); }
		if(filesize($this->msgsFile)==0) { fwrite($msgs, serialize((array(0=>array('time' => 0, 'user' => 'Admin', 'msg' => 'Welcome to SDJ.vn !'))))); } 
		fclose($msgs); $contents = unserialize(file_get_contents($this->msgsFile)); $dataout=array();
		if(($output=='admin')||($output=='adminhtml')) {
			foreach($contents as $pos => $data) {
				if($pos > $lastpost) {
					if($data['status']!=='deleted') {
					$adminControls = '<div class="ShoutCloud-Admin-User-Controls" data="ip:\''.$data['ip'].'\',name:\''.$data['user'].'\'">
						  				<span class="shout-user-ip">'.$data['ip'].'</span>
										<span class="shout-ban-opts">
											<b>Ban</b><span class="shout-ban" id="+1 Minute">1 Min</span>
											<span class="shout-ban" id="+10 Minutes">10 Mins</span>
											<span class="shout-ban" id="+1 Hour">1 Hour</span>
											<span class="shout-ban" id="+1 Day">1 Day</span><span class="shout-ban" id="0">Forever</span>
										</span>
										<span class="shout-user-opts"><span class="shout-del">Delete</span><span class="ShoutCloud-Admin-Reply">Reply</span></span>
						  			  </div>';
					$dataout['msgs'] .= '<div class="'.(($data['status']=='deleted') ? 'shout-deleted' : 'shout-msg').'" id="shoutid-'.$pos.'">'.
										((utf8_decode($data['user'])==$this->adminUser) ? '' : $adminControls).'<strong id="'.utf8_decode($data['user']).'" 
										class="'.((!empty($data['color'])) ? ' ShoutCloud-Swatch-'.$data['color'] : '').
										((utf8_decode($data['user'])==$this->adminUser) ? ' shout-admin ShoutCloud-Reply">' : ' shout-admin-user" title="Open Admin User Control">')
										.utf8_decode($data['user']).'</strong>'.(($data['time']==0) ? '' : '<em>'.date('g:i:sa',$data['time'])).'</em>'.$this->formatMessage($data['msg']).'</div>';
				   }
				}
			}
		} else {
			foreach($contents as $pos => $data) {
				if($pos > $lastpost) {
					if($data['status']!=='deleted') {
						$dataout['msgs'] .= '<div class="'.(($data['status']=='deleted') ? 'shout-deleted' : 'shout-msg').'" id="shoutid-'.$pos.'">
										 	 <strong id="'.utf8_decode($data['user']).'"'.(((!empty($_SESSION['ShoutCloud-User'])) && ($_SESSION['ShoutCloud-User']==utf8_decode($data['user']))) ? ' class="' : 
											' title="Reply to '.utf8_decode($data['user']).'" class="ShoutCloud-Reply')
										 	.((!empty($data['color'])) ? ' ShoutCloud-Swatch-'.$data['color'] : '').((utf8_decode($data['user'])==$this->adminUser) ? ' shout-admin">' : '">')
											.utf8_decode($data['user']).'</strong>'.(($data['time']==0) ? '' : '<em>'.date('g:i:sa',$data['time'])).'</em>'.$this->formatMessage($data['msg']).'</div>';
					}
				}
			}
		}
		
		if(($output=='html')||($output=='adminhtml')) { $htmlout=''; foreach($dataout as $k => $v) { $htmlout .= $v; } return $htmlout; 
		} else { if(!empty($dataout)) { return $this->jsonEncode($dataout); } else { return $this->jsonEncode(array('msgs' => '')); }
		}
	}
	
	# formatMessage function 
	# Removes bad words and adds smilies
	# Options: [message]
	function formatMessage($msg) {
		$msg = str_ireplace($this->badwords, '****', $msg); foreach($this->smilies as $acii => $img) { $msg = str_ireplace($acii, '<img src="'.$this->smiliesPath.$img.'" width="22" height="22" align="absmiddle" />', $msg); }
		$find = array(   
			'/\[img](.*?)\[\/img]/i'
		);
		$replace = array(
		'<a href="$1" target="_blank"><img class="chatbox-image" width=150 height=100 src="$1" alt="$1" /> </a>'
		);

		$msg = preg_replace($find, $replace, $msg);
		$text = preg_replace('#(script|about|applet|activex|chrome):#is', "\\1:", $msg);
		$ret = ' ' . $text;
		$ret = preg_replace("#(^|[\n ])([\w]+?://[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a href=\"\\2\" target=\"_blank\" rel=\"no_follow\">\\2</a>", $ret);
		
		$ret = preg_replace("#(^|[\n ])((www|ftp)\.[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $ret);
		$ret = preg_replace("#(^|[\n ])([a-z0-9&\-_.]+?)@([\w\-]+\.([\w\-\.]+\.)*[\w]+)#i", "\\1<a href=\"mailto:\\2@\\3\">\\2@\\3</a>", $ret);
		$ret = substr($ret, 1);
		$patterns = array('~\[@([^\]]*)\]~','~\[([^\]]*)\]~','~{([^}]*)}~','/\s{2}/');
		$replacements = array('<b class="reply">@\\1</b>','<b>\\1</b>','<i>\\1</i>','<br />');
		$msg = preg_replace($patterns, $replacements, $ret);
		
		return stripslashes(stripslashes(utf8_decode($msg)));
	}
	
	# checkUsername function
	# Removes badwords and cleans up a user's name
	# Options: [user's name]
	function checkUsername($name) {
		$bad_usernames = array('admin','Administrator','administrator','ADMIN'); if((empty($_SESSION['ShoutCloud_Admin_Name'])) && (empty($_SESSION['ShoutCloud_Admin_Loggedin']))) { $bad_usernames[] = $this->adminUser; }
		$name = utf8_encode(strip_tags($name)); foreach($bad_usernames as $k=>$n) { if($name==$n) { return false; } } return str_ireplace($this->badwords,'',$name);
	}
	
	# addMessage function
	# Cleans and applies new submitted shout
	# Options: [user], [message], [tag color]
	function addMessage($user, $msg, $color) {
		$user = $this->checkUsername($user); if($user===false) { return $this->jsonEncode(array('error'=>'You cannot use that name!')); }
		if(strlen(utf8_decode($user)) > 30) { return $this->jsonEncode(array('error'=>'Your name is too long!')); } $msg = utf8_encode(addslashes(strip_tags($msg)));
		if(strlen($msg) > 500) { return $this->jsonEncode(array('error'=>'Your message is too long! Limit 500 characters.')); }
		if((empty($user)) || ($user=='Your Name')) { return $this->jsonEncode(array('error'=>'Please enter your name!')); }
		if((empty($msg)) || ($user=='Message')) { return $this->jsonEncode(array('error'=>'Please enter a message!')); }
		if($this->isBanned($_SERVER['REMOTE_ADDR'])===true) { return $this->jsonEncode(array('error'=>'You are banned from this ShoutBox.')); }
		if((empty($_SESSION['ShoutCloud-User'])) || (!isset($_SESSION['ShoutCloud-User'])) || ($_SESSION['ShoutCloud-User']!==$user)) { $_SESSION['ShoutCloud-User'] = $user; }
		if((empty($_SESSION['ShoutCloud_Tag_Color'])) || ($_SESSION['ShoutCloud_Tag_Color']!==$color)) { $_SESSION['ShoutCloud_Tag_Color'] = $color; }
		$allMsgs = unserialize(file_get_contents($this->msgsFile)); if(empty($_SESSION['ShoutCloud-User-Flood'])) { $_SESSION['ShoutCloud-User-Flood'] = 0; }
		if($_SESSION['ShoutCloud-User-Flood'] > time()) { return $this->jsonEncode(array('error'=>'Bạn cần đợi 5 giây để tiếp tục chém gió !')); } $_SESSION['ShoutCloud-User-Flood'] = time() + 5;
		$allMsgs[] = array('time' => time(), 'user' => $user, 'msg' => $msg, 'color' => $color, 'ip' => $_SERVER['REMOTE_ADDR']); $totalMsgs = count($allMsgs);
		if($totalMsgs > 30) { $difference = ($totalMsgs - 30); $i=1; $allMsgs = array_reverse($allMsgs, true); while($i <= $difference) { $remove = array_pop($allMsgs); $i++; } $allMsgs = array_reverse($allMsgs, true);
		} else { $difference = 0; } $msgFile = fopen($this->msgsFile, 'w');
		if(fwrite($msgFile, serialize($allMsgs))) { fclose($msgFile); return $this->jsonEncode(array('status' => 'posted')); } else { fclose($msgFile); 
		return $this->jsonEncode(array('error'=>'Your message could not be posted at this time.')); }
	}
	
	# adminLogin function
	# Handles the checking of admin password
	# Options: [user], [password]
	function adminLogin($user, $pass) {
		$pass = htmlentities(strip_tags($pass)); $user = htmlentities(strip_tags($user));
		if(($pass == $this->adminPass) && ($user == $this->adminUser)) { $_SESSION['ShoutCloud_Admin_Name'] = $user; $_SESSION['ShoutCloud_Admin_Loggedin'] = 'true'; return $this->jsonEncode(array('status'=>'loggedin'));
		} else { return $this->jsonEncode(array('error'=>'Incorrect Username and/or Password!')); }
	}
	
	# adminLogout function
	# Handles logging out of an admin account
	# Options: none
	function adminLogout() { unset($_SESSION['ShoutCloud_Admin_Name']); unset($_SESSION['ShoutCloud_Admin_Loggedin']); return $this->jsonEncode(array('status'=>'loggedout')); }
	
	# isAdmin function
	# Checks if user is an admin
	# Options: none
	function isAdmin() { if((!empty($_SESSION['ShoutCloud_Admin_Name']))&&(!empty($_SESSION['ShoutCloud_Admin_Loggedin']))) { return true; } else { $this->jsonEncode(array('error'=>'You are not an Administrator!')); } }
	
	# banUser function
	# Handles user bans by admins
	# Options: [user's name], [ip address], [expire time]
	function banUser($name, $ip, $expire) {
		$allBans = file_get_contents($this->bansFile); $bans = fopen($this->bansFile, 'w+'); if(!is_writable($this->bansFile)) { chmod($this->bansFile, 0666); }
		$allBans = (filesize($this->bansFile)==0) ? array() : unserialize($allBans); $expire = ((empty($expire)) || ($expire==0)) ? 0 : strtotime($expire); $allBans[$ip] = array('name' => $name, 'expire' => $expire);
		fwrite($bans, serialize($allBans)); fclose($bans); return $this->jsonEncode(array('status'=>'banned'));
	}
	
	# unbanUser function
	# Handles unbanning users
	# Options: [ip address], [type]
	function unbanUser($ip,$type='box') {
		$allBans = file_get_contents($this->bansFile); $bans = fopen($this->bansFile, 'w+'); if(!is_writable($this->bansFile)) { chmod($this->bansFile, 0666); }
		$allBans = (filesize($this->bansFile)==0) ? array() : unserialize($allBans); if(array_key_exists($ip, $allBans)) { unset($allBans[$ip]); }
		fwrite($bans, serialize($allBans)); fclose($bans); if($type == 'box') { return $this->jsonEncode(array('status'=>'removed')); }
	}
	
	# isBanned function
	# Checks if user's IP is banned by an admin
	# Options: [ip address]
	function isBanned($ip) {
		$bans = fopen($this->bansFile, 'a+'); if(!is_writable($this->bansFile)) { chmod($this->bansFile, 0666); }
		if(filesize($this->bansFile)>0) { $allBans = unserialize(fread($bans, filesize($this->bansFile))); fclose($bans);
			if(array_key_exists($ip, $allBans)) { if(($allBans[$ip]['expire'] !== 0) && ($allBans[$ip]['expire'] < time())) { $this->unbanUser($ip,'internal'); return false; } return true; } else {  return false; }
		}
	}
	
	# deleteMessage function
	# Deletes specific message from the shout box
	# Options: [shout id]
	function deleteMessage($id) {
		$allMsgs = unserialize(file_get_contents($this->msgsFile)); $id = str_ireplace('shoutid-','',$id);
		$allMsgs[$id] = array('time' => time(), 'user' => $user, 'msg' => $msg, 'color' => $color, 'ip' => $_SERVER['REMOTE_ADDR'], 'status' => 'deleted'); $msgFile = fopen($this->msgsFile, 'w');
		if(fwrite($msgFile, serialize($allMsgs))) { fclose($msgFile); return $this->jsonEncode(array('status'=>'deleted')); } else { 
		fclose($msgFile); return $this->jsonEncode(array('error'=>'Unable to delete that message. Please try again.'));
		}
	}
	
	# clearShoutbox function
	# Clears all the messages from the shoutbox
	# Options: none
	function clearShoutbox() {
		$msgFile = fopen($this->msgsFile, 'w+'); 
		//fwrite($msgFile, serialize((array(0=>array('time' => 0, 'user' => '', 'msg' => '')))));
		fclose($msgFile); return $this->jsonEncode(array('status'=>'cleared'));
	}
	
	# formatTime function
	# Formats time for the Ban List
	# Options: [timestamp]
	function formatTime($ts) {
		$current = time(); $seconds = $ts-$current; if($seconds < 1) { return false; }
		switch($seconds) {
			case($seconds < 60): $unit=$var=$seconds; $var.=" second"; break;
			case($seconds < 3600): $unit=$var=round($seconds/60); $var.=" minute"; break;
			case($seconds < 86400): $unit=$var=round($seconds/3600); $var.=" hour"; break;
			case($seconds < 2629744): $unit=$var=round($seconds/86400); $var.=" day"; break;
			case($seconds < 31556926): $unit=$var=round($seconds/2629744); $var.=" month"; break;
			default: $unit=$var=round($seconds/31556926); $var.=" year";
		}
		if($unit > 1) {  $var.="s"; } return $var;
	}
	
	function jsonEncode($var) { return json_encode($var); }
}
?>