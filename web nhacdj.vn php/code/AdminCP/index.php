﻿<?php
session_start();
include("../XBTeam/se7en.php");
include("../XBTeam/functions.php");
getLogin();
if($v == "songmanager" ) {
	$status = "Quản lý bài hát";
} else if ($v == "songup"){
	$status = "Nhạc chờ duyệt";
} else if ($v == "home" || empty($v)){
	$status = "Chào mừng đến với quản trị";
} else if ($v == "songtrash"){
	$status = "Nhạc tạm xóa";
} else if ($v == "sendmess"){
	$status = "Gửi tin nhắn đến thành viên";
} else if ($v == "editsong") {
	$status = "Sửa bài hát : ".getSongName( $_GET["id"] )."";
} else if ($v == "editalbum") {
	$status = "Sửa Album : ".getAlbumName( $_GET["id"] )."";
} else if ($v == "editmember") {
	$status = "Sửa thành viên : ".getMemberName( $_GET["id"] )."";
}
 else {
	$status = "Hệ thống quản trị";
}
if(isset($_SESSION['admin']))
{
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?=$status?></title>
<link rel="stylesheet" href="css/style_all.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/font-awesome.css">
<link rel="stylesheet" href="css/sky-mega-menu.css">		
<script type="text/javascript" src="js/function.js"></script>
<script type="text/javascript" src="js/GrapLink.js"></script>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?=$homelink?>js/jquery.livequery.min.js"></script>
<link href="../css/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="../js/facebox.js" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
  $('a[rel*=facebox]').facebox({
	loadingImage : '../img/loading.gif',
	closeImage   : 'img/close.png'
  })
})
</script>
<script type="text/javascript">  
$(document).ready(function(){
if ($.browser.mozilla) {
$('nav ul').css('margin','7px 0 0');
}   
$('#top_search').keyup(function(){
var keyword = $(this).val();
if(keyword.length>=2){
var data = {keyword:keyword};
$.post('server/search_autocomplete.php?do=search_sugess',data,function(re){
$('.search-result').html('');
$('.search-result').html(re);
$('#search-result-box').show();
})
}
}) 
$("body").click(function(e) {

if(e.target!=document.getElementById("#search-result-box")) $('#search-result-box').hide();

});   
});
</script>
</head>
<body>
<? if ($v!= "movetrashsong") {?>

<ul style="z-index:2000" class="sky-mega-menu sky-mega-menu-anim-flip sky-mega-menu-response-to-icons">
<li>
<a href="index.php"><i class="fa fa-single fa-home"></i></a>
</li>
<?if ($_SESSION['loginTitle'] =='mod'){?>
<li aria-haspopup="true">
<a href="#"><i class="fa fa-music"></i><i class="fa fa-indicator fa-chevron-down"></i>Nhạc - Video</a>
<div class="grid-container3">
<ul>
<li><a href="Add-Post.html"><i class="fa fa-plus"></i>Đăng bài mới</a></li>
<li><a href="Song-Available-Manager.html"><i class="fa fa-check"></i>Nhạc đã duyệt</a></li>
<li><a href="Song-Invisible-Manager.html"><i class="fa fa-clock-o"></i>Nhạc chờ duyệt</a></a></li>
<li><a href="Song-Trash-Manager.html"><i class="fa fa-trash-o"></i>Nhạc tạm xóa</a></a></li>
</ul>
</div>
</li>
<?} else if ($_SESSION['loginTitle'] == 'smod'){?>
<li aria-haspopup="true">
<a href="#"><i class="fa fa-music"></i><i class="fa fa-indicator fa-chevron-down"></i>Nhạc - Video</a>
<div class="grid-container3">
<ul>
<li><a href="Add-Post.html"><i class="fa fa-plus"></i>Đăng bài mới</a></li>
<li><a href="Song-Available-Manager.html"><i class="fa fa-check"></i>Nhạc đã duyệt</a></li>
<li><a href="Song-Invisible-Manager.html"><i class="fa fa-clock-o"></i>Nhạc chờ duyệt</a></a></li>
<li><a href="Song-Trash-Manager.html"><i class="fa fa-trash-o"></i>Nhạc tạm xóa</a></a></li>
</ul>
</div>
</li>
<li aria-haspopup="true">
<a href="#"><i class="fa fa-bullhorn"></i><i class="fa fa-indicator fa-chevron-down"></i>Album - Tin tức</a>
<div class="grid-container3">
<ul>
<li><a href="Add-News.html"><i class="fa fa-edit"></i>Thêm mới tin tức</a></li>
</ul>
</div>
</li>	
<?} else if ($_SESSION['loginTitle'] == 'checker'){?>
<li aria-haspopup="true">
<a href="#"><i class="fa fa-music"></i><i class="fa fa-indicator fa-chevron-down"></i>Nhạc - Video</a>
<div class="grid-container3">
<ul>
<li><a href="Add-Post.html"><i class="fa fa-plus"></i>Đăng bài mới</a></li>
<li><a href="Song-Available-Manager.html"><i class="fa fa-check"></i>Nhạc đã duyệt</a></li>
<li><a href="Song-Invisible-Manager.html"><i class="fa fa-clock-o"></i>Nhạc chờ duyệt</a></a></li>
<li><a href="Song-Trash-Manager.html"><i class="fa fa-trash-o"></i>Nhạc tạm xóa</a></a></li>
</ul>
</div>
</li>
<li aria-haspopup="true">
<a href="#"><i class="fa fa-bullhorn"></i><i class="fa fa-indicator fa-chevron-down"></i>Album - Tin tức</a>
<div class="grid-container3">
<ul>
<li><a href="Add-News.html"><i class="fa fa-edit"></i>Thêm mới tin tức</a></li>
</ul>
</div>
</li>	
<?} else {?>
<li aria-haspopup="true">
<a href="#"><i class="fa fa-music"></i><i class="fa fa-indicator fa-chevron-down"></i>Nhạc - Video</a>
<div class="grid-container3">
<ul>
<li><a href="Add-Post.html"><i class="fa fa-plus"></i>Đăng bài mới</a></li>
<li><a href="Song-Available-Manager.html"><i class="fa fa-check"></i>Nhạc đã duyệt</a></li>
<li><a href="Song-Invisible-Manager.html"><i class="fa fa-clock-o"></i>Nhạc chờ duyệt</a></a></li>
<li><a href="Song-Trash-Manager.html"><i class="fa fa-trash-o"></i>Nhạc tạm xóa</a></a></li>
</ul>
</div>
</li>
<li aria-haspopup="true">
<a href="#"><i class="fa fa-bullhorn"></i><i class="fa fa-indicator fa-chevron-down"></i>Album - Tin tức</a>
<div class="grid-container3">
<ul>
<li><a href="Album-Manager.html"><i class="fa fa-book"></i>Quản lý Album</a></li>
<li><a href="Add-Album.html"><i class="fa fa-edit"></i>Thêm mới Album</a></li>
<li><a href="News-Manager.html"><i class="fa fa-book"></i>Quản lý tin tức</a></li>
<li><a href="Add-News.html"><i class="fa fa-edit"></i>Thêm mới tin tức</a></li>
<li><a href="News-Cat-Manager.html"><i class="fa fa-edit"></i>Quản lý danh mục tin</a></li>
<li><a href="Add-News-Cat.html"><i class="fa fa-edit"></i>Thêm mới danh mục tin</a></li>
</ul>
</div>
</li>			
<li aria-haspopup="true">
<a href="#"><i class="fa fa-user"></i><i class="fa fa-indicator fa-chevron-down"></i>Thành viên - Thể loại - Artist</a>
<div class="grid-container3">
<ul>
<li><a href="Member-Manager.html"><i class="fa fa-user"></i>Quản lý thành viên</a></li>
<li><a href="Add-Category.html"><i class="fa fa-inbox"></i>Thêm mới thể loại nhạc</a></li>
<li><a href="Category-Manager.html"><i class="fa fa-inbox"></i>Quản lý Thể loại nhạc</a></li>
<li><a href="Singer-Manager.html"><i class="fa fa-inbox"></i>Quản lý Singer</a></li>
<li><a href="Playlist-Manager.html"><i class="fa fa-inbox"></i>Quản lý Playlist</a></li>
<li><a href="Artist-Manager.html"><i class="fa fa-inbox"></i>Quản lý Artist</a></li>
<li><a href="Add-Artist.html"><i class="fa fa-inbox"></i>Thêm mới Artist</a></li>
</ul>
</div>
</li>
<li aria-haspopup="true">
<a href="#"><i class="fa fa-th-large"></i><i class="fa fa-indicator fa-chevron-down"></i>Khác</a>
<div class="grid-container3">
<ul>
<li><a href="Top-Manager.html"><i class="fa fa-trophy"></i>TOP Chọn lọc</a></li>
<li><a href="Title-Manager.html"><i class="fa fa-certificate"></i>Quản lý Danh hiệu</a></li>
<li><a href="Add-Title.html"><i class="fa fa-edit"></i>Thêm mới danh hiệu</a></li>
<li><a href="Comment-Manager.html"><i class="fa fa-comment"></i>Quản lý bình luận</a></li>
<li><a href="Playlist-Manager.html"><i class="fa fa-certificate"></i>Quản lý Playlist</a></li>
<li><a href="Send-Message.html"><i class="fa fa-certificate"></i>Gửi tin nhắn</a></li>
</ul>
</div>
</li>
<li>
<a href="./gui-thong-bao.html"><i class="fa fa-power-off"></i>Thong Bao</a>
</li>
<li>
<a href="logout.php"><i class="fa fa-power-off"></i>Thoát</a>
</li>
<li class="right search">					
<form action="#">					
<div class="input">
<button type="submit" class="button">GO</button>
<input id="top_search" name="q" type="text" placeholder="Cần tìm gì gõ vào đây">
</div>	
</form>
</li>
<?}?>
</ul>
<div id="search-result-box">
<div class="search-result"></div>   
</div>
<?}?>
<?php
$v = $_GET["v"];

$music	=	"../AdminCP/php";
$trangchu	=	"../AdminCP/php/songmanager.php";

$varchk = isset($v);
if ($varchk == "1")
{ 
$forbidden_1 = ereg("\.\./", $v);
$forbidden_2 = ereg("/", $v);

if ($forbidden_1 OR $forbidden_2)
{
err_index("<b>Trang này bị cấm truy cập!</b><br>Xin liên hệ với webmaster để biết thêm thông tin ...");
}
else
{
if (@fopen("./$music/$v.php", "r"))
{ 
include ("./$music/$v.php"); 
}
elseif (!(@fopen("./$music/$v.php", "r")))
{
err_index("không tìm thấy trang http://$HTTP_HOST$PHP_SELF?v=$v");
}
}
}
elseif ($varchk == "0")
{
include("./$trangchu"); 
}
?>
</body>
<?	}
else
{			
echo("<meta HTTP-EQUIV=\"REFRESH\" content=\"0; url=./login.php\">");
}
?>