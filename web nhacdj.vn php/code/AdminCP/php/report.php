<div class="sortable row-fluid">
				
				<a class="quick-button span2">
					<i class="fa-icon-music"></i>
					<p>Bài hát</p>
					<span class="notification"><?=getTotalSong()?></span>
				</a>
				<a class="quick-button span2">
					<i class="fa-icon-comments-alt"></i>
					<p>Cảm nhận</p>
					<span class="notification"><?=getTotalComment()?></span>
				</a>
				<a class="quick-button span2">
					<i class="fa-icon-headphones"></i>
					<p>Lượt nghe tháng</p>
					<span class="notification"><?=getTotalView("thisMonth")?></span>
				</a>
				<a class="quick-button span2">
					<i class="fa-icon-download-alt"></i>
					<span class="notification"><?=getTotalDownload("thisMonth")?></span>
					<p>Lượt Download tháng</p>
				</a>
				<a class="quick-button span2">
					<i class="fa-icon-headphones"></i>
					<p>Tổng số lượt nghe</p>
					<span class="notification"><?=getTotalView("all")?></span>
				</a>
				<a class="quick-button span2">
					<i class="fa-icon-download-alt"></i>
					<span class="notification"><?=getTotalDownload("all")?></span>
					<p>Tổng số lượt tải</p>
				</a>
				
			</div>
			</br>
			<div class="sortable row-fluid">
				
				<a class="quick-button span2">
					<i class="fa-icon-group"></i>
					<p>Thành viên</p>
					<span class="notification"><?=getTotalMember()?></span>
				</a>
				<a class="quick-button span2">
					<i class="fa-icon-sitemap"></i>
					<p>Thể loại nhạc</p>
					<span class="notification"><?=getTotalCategory()?></span>
				</a>
				<a class="quick-button span2">
					<i class="fa-icon-user"></i>
					<span class="notification"><?=getTotalSinger()?></span>
					<p>Deezay</p>
				</a>
				<a class="quick-button span2">
					<i class="fa-icon-heart"></i>
					<span class="notification"><?=getTotalAlbum()?></span>
					<p>Album</p>
				</a>

				
			</div>

                 
<p style="padding: 10px; font-family:'Courier New', Courier, monospace; font-size:18px;">&nbsp;</p>
<script language="javascript">
	function check() {
		if(!confirm("Bạn có muốn khởi động lại bộ đếm trong tháng?")) {
			return false;
		}
		if(!confirm("Tất cả bộ đếm số lượt truy cập, số lượt tải trong tháng sẽ trở về 0?")) {
				return false;
		}
		return true;
	}
</script>
<form action="He-thong.html" method="post" name="resetCount" onsubmit="return check()">
<input type="hidden" name="act" value="resetCounter">
<center><button id="btnSubmit" name="btnSubmit" type="submit" class="btn btn-inverse inline">Khởi Động Lại Bộ Đếm Trong Tháng</button></center>
</form>
</div>
</div>
</div>