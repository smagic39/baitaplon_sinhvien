-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 05, 2015 at 11:16 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lam`
--

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_authors`
--

CREATE TABLE IF NOT EXISTS `kcxd_authors` (
  `admin_id` mediumint(8) unsigned NOT NULL,
  `editor` varchar(100) DEFAULT '',
  `lev` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `files_level` varchar(255) DEFAULT '',
  `position` varchar(255) NOT NULL,
  `addtime` int(11) NOT NULL DEFAULT '0',
  `edittime` int(11) NOT NULL DEFAULT '0',
  `is_suspend` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `susp_reason` text,
  `check_num` varchar(40) NOT NULL,
  `last_login` int(11) unsigned NOT NULL DEFAULT '0',
  `last_ip` varchar(45) DEFAULT '',
  `last_agent` varchar(255) DEFAULT '',
  PRIMARY KEY (`admin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kcxd_authors`
--

INSERT INTO `kcxd_authors` (`admin_id`, `editor`, `lev`, `files_level`, `position`, `addtime`, `edittime`, `is_suspend`, `susp_reason`, `check_num`, `last_login`, `last_ip`, `last_agent`) VALUES
(1, 'ckeditor', 1, 'adobe,archives,audio,documents,flash,images,real,video|1|1|1', 'Administrator', 0, 0, 0, '', '979fd964f0f5d4705f246247acac6e39bcda6f00', 1423130096, '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:35.0) Gecko/20100101 Firefox/35.0');

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_authors_config`
--

CREATE TABLE IF NOT EXISTS `kcxd_authors_config` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `keyname` varchar(32) DEFAULT NULL,
  `mask` tinyint(4) NOT NULL DEFAULT '0',
  `begintime` int(11) DEFAULT NULL,
  `endtime` int(11) DEFAULT NULL,
  `notice` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `keyname` (`keyname`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_authors_module`
--

CREATE TABLE IF NOT EXISTS `kcxd_authors_module` (
  `mid` mediumint(8) NOT NULL AUTO_INCREMENT,
  `module` varchar(55) NOT NULL,
  `lang_key` varchar(50) NOT NULL DEFAULT '',
  `weight` mediumint(8) NOT NULL DEFAULT '0',
  `act_1` tinyint(4) NOT NULL DEFAULT '0',
  `act_2` tinyint(4) NOT NULL DEFAULT '1',
  `act_3` tinyint(4) NOT NULL DEFAULT '1',
  `checksum` varchar(32) DEFAULT '',
  PRIMARY KEY (`mid`),
  UNIQUE KEY `module` (`module`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `kcxd_authors_module`
--

INSERT INTO `kcxd_authors_module` (`mid`, `module`, `lang_key`, `weight`, `act_1`, `act_2`, `act_3`, `checksum`) VALUES
(1, 'siteinfo', 'mod_siteinfo', 1, 1, 1, 1, '141d95bf81e42b1b711355d032ac99b4'),
(2, 'authors', 'mod_authors', 2, 1, 1, 1, '0f25fed6c58dce00eafe1c816e281ddf'),
(3, 'settings', 'mod_settings', 3, 1, 1, 0, 'bada19f8f30927e2757507b2526aad3c'),
(4, 'database', 'mod_database', 4, 1, 0, 0, 'f01565c52848c4cc3e524669e688b9d6'),
(5, 'webtools', 'mod_webtools', 5, 1, 0, 0, 'c8326df36f52bf23df935866c880ac9f'),
(6, 'seotools', 'mod_seotools', 6, 1, 0, 0, 'd592f90ec83066185405b7687b5d5ae0'),
(7, 'language', 'mod_language', 7, 1, 1, 0, '181d0c8c35015967c66cbb0327096b74'),
(8, 'modules', 'mod_modules', 8, 1, 1, 0, '2385ea1f8a603f8af9cfd07d7bfb557e'),
(9, 'themes', 'mod_themes', 9, 1, 1, 0, '2eb7313f2bc20e1709fbb7087ec3e1ff'),
(10, 'extensions', 'mod_extensions', 10, 1, 0, 0, 'e64e7cac7c11cb1537c61992212d81c7'),
(11, 'upload', 'mod_upload', 11, 1, 1, 1, '959db877b0031b6c20cd38e5bdad2036');

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_banip`
--

CREATE TABLE IF NOT EXISTS `kcxd_banip` (
  `id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `ip` varchar(32) DEFAULT NULL,
  `mask` tinyint(4) NOT NULL DEFAULT '0',
  `area` tinyint(3) NOT NULL,
  `begintime` int(11) DEFAULT NULL,
  `endtime` int(11) DEFAULT NULL,
  `notice` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ip` (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_banners_click`
--

CREATE TABLE IF NOT EXISTS `kcxd_banners_click` (
  `bid` mediumint(8) NOT NULL DEFAULT '0',
  `click_time` int(11) unsigned NOT NULL DEFAULT '0',
  `click_day` int(2) NOT NULL,
  `click_ip` varchar(15) NOT NULL,
  `click_country` varchar(10) NOT NULL,
  `click_browse_key` varchar(100) NOT NULL,
  `click_browse_name` varchar(100) NOT NULL,
  `click_os_key` varchar(100) NOT NULL,
  `click_os_name` varchar(100) NOT NULL,
  `click_ref` varchar(255) NOT NULL,
  KEY `bid` (`bid`),
  KEY `click_day` (`click_day`),
  KEY `click_ip` (`click_ip`),
  KEY `click_country` (`click_country`),
  KEY `click_browse_key` (`click_browse_key`),
  KEY `click_os_key` (`click_os_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_banners_clients`
--

CREATE TABLE IF NOT EXISTS `kcxd_banners_clients` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(60) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `reg_time` int(11) unsigned NOT NULL DEFAULT '0',
  `full_name` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `website` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `yim` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `fax` varchar(100) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `act` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `check_num` varchar(40) NOT NULL,
  `last_login` int(11) unsigned NOT NULL DEFAULT '0',
  `last_ip` varchar(15) NOT NULL,
  `last_agent` varchar(255) NOT NULL,
  `uploadtype` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  UNIQUE KEY `email` (`email`),
  KEY `full_name` (`full_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_banners_plans`
--

CREATE TABLE IF NOT EXISTS `kcxd_banners_plans` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `blang` char(2) DEFAULT '',
  `title` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT '',
  `form` varchar(100) NOT NULL,
  `width` smallint(4) unsigned NOT NULL DEFAULT '0',
  `height` smallint(4) unsigned NOT NULL DEFAULT '0',
  `act` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `title` (`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `kcxd_banners_plans`
--

INSERT INTO `kcxd_banners_plans` (`id`, `blang`, `title`, `description`, `form`, `width`, `height`, `act`) VALUES
(1, '', 'Quang cao giua trang', '', 'sequential', 510, 100, 1),
(2, '', 'Quang cao trai', '', 'sequential', 190, 500, 1);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_banners_rows`
--

CREATE TABLE IF NOT EXISTS `kcxd_banners_rows` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `pid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `clid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `file_name` varchar(255) NOT NULL,
  `file_ext` varchar(100) NOT NULL,
  `file_mime` varchar(100) NOT NULL,
  `width` int(4) unsigned NOT NULL DEFAULT '0',
  `height` int(4) unsigned NOT NULL DEFAULT '0',
  `file_alt` varchar(255) DEFAULT '',
  `imageforswf` varchar(255) DEFAULT '',
  `click_url` varchar(255) DEFAULT '',
  `target` varchar(10) NOT NULL DEFAULT '_blank',
  `add_time` int(11) unsigned NOT NULL DEFAULT '0',
  `publ_time` int(11) unsigned NOT NULL DEFAULT '0',
  `exp_time` int(11) unsigned NOT NULL DEFAULT '0',
  `hits_total` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `act` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`),
  KEY `clid` (`clid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `kcxd_banners_rows`
--

INSERT INTO `kcxd_banners_rows` (`id`, `title`, `pid`, `clid`, `file_name`, `file_ext`, `file_mime`, `width`, `height`, `file_alt`, `imageforswf`, `click_url`, `target`, `add_time`, `publ_time`, `exp_time`, `hits_total`, `act`, `weight`) VALUES
(1, 'Bo ngoai giao', 2, 0, 'bongoaigiao.jpg', 'jpg', 'image/jpeg', 160, 54, '', '', 'http://www.mofa.gov.vn', '_blank', 1421251986, 1421251986, 0, 0, 1, 1),
(2, 'vinades', 2, 0, 'vinades.jpg', 'jpg', 'image/jpeg', 190, 454, '', '', 'http://vinades.vn', '_blank', 1421251986, 1421251986, 0, 0, 1, 2),
(3, 'Quang cao giua trang', 1, 0, 'webnhanh_vn.gif', 'gif', 'image/gif', 510, 65, '', '', 'http://webnhanh.vn', '_blank', 1421251986, 1421251986, 0, 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_config`
--

CREATE TABLE IF NOT EXISTS `kcxd_config` (
  `lang` varchar(3) NOT NULL DEFAULT 'sys',
  `module` varchar(25) NOT NULL DEFAULT 'global',
  `config_name` varchar(30) NOT NULL DEFAULT '',
  `config_value` text,
  UNIQUE KEY `lang` (`lang`,`module`,`config_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kcxd_config`
--

INSERT INTO `kcxd_config` (`lang`, `module`, `config_name`, `config_value`) VALUES
('sys', 'site', 'closed_site', '0'),
('sys', 'site', 'admin_theme', 'admin_default'),
('sys', 'site', 'date_pattern', 'l, d/m/Y'),
('sys', 'site', 'time_pattern', 'H:i'),
('sys', 'site', 'online_upd', '1'),
('sys', 'site', 'statistic', '1'),
('sys', 'site', 'mailer_mode', ''),
('sys', 'site', 'smtp_host', 'smtp.gmail.com'),
('sys', 'site', 'smtp_ssl', '1'),
('sys', 'site', 'smtp_port', '465'),
('sys', 'site', 'smtp_username', 'user@gmail.com'),
('sys', 'site', 'smtp_password', ''),
('sys', 'site', 'googleAnalyticsID', ''),
('sys', 'site', 'googleAnalyticsSetDomainName', '0'),
('sys', 'site', 'googleAnalyticsMethod', 'classic'),
('sys', 'site', 'searchEngineUniqueID', '004347095120907336855:nmqebcxafke'),
('sys', 'site', 'metaTagsOgp', '1'),
('sys', 'site', 'pageTitleMode', 'pagetitle - sitename'),
('sys', 'site', 'description_length', '170'),
('sys', 'global', 'site_keywords', 'NukeViet, portal, mysql, php'),
('sys', 'global', 'site_phone', ''),
('sys', 'global', 'block_admin_ip', '0'),
('sys', 'global', 'admfirewall', '0'),
('sys', 'global', 'dump_autobackup', '1'),
('sys', 'global', 'dump_backup_ext', 'gz'),
('sys', 'global', 'dump_backup_day', '30'),
('sys', 'global', 'gfx_chk', '3'),
('sys', 'global', 'file_allowed_ext', 'adobe,archives,audio,documents,flash,images,real,video'),
('sys', 'global', 'forbid_extensions', 'php,php3,php4,php5,phtml,inc'),
('sys', 'global', 'forbid_mimes', ''),
('sys', 'global', 'nv_max_size', '3145728'),
('sys', 'global', 'upload_checking_mode', 'strong'),
('sys', 'global', 'upload_alt_require', '1'),
('sys', 'global', 'upload_auto_alt', '1'),
('sys', 'global', 'allowuserreg', '1'),
('sys', 'global', 'allowuserlogin', '1'),
('sys', 'global', 'allowloginchange', '0'),
('sys', 'global', 'allowquestion', '0'),
('sys', 'global', 'allowuserpublic', '0'),
('sys', 'global', 'useactivate', '2'),
('sys', 'global', 'allowmailchange', '1'),
('sys', 'global', 'allow_sitelangs', 'vi'),
('sys', 'global', 'allow_adminlangs', 'en,vi'),
('sys', 'global', 'read_type', '0'),
('sys', 'global', 'rewrite_optional', '1'),
('sys', 'global', 'rewrite_endurl', '/'),
('sys', 'global', 'rewrite_exturl', '.html'),
('sys', 'global', 'rewrite_op_mod', 'news'),
('sys', 'global', 'autocheckupdate', '1'),
('sys', 'global', 'autoupdatetime', '24'),
('sys', 'global', 'gzip_method', '1'),
('sys', 'global', 'is_user_forum', '0'),
('sys', 'global', 'openid_mode', '1'),
('sys', 'global', 'authors_detail_main', '0'),
('sys', 'global', 'spadmin_add_admin', '1'),
('sys', 'global', 'openid_servers', 'yahoo,google'),
('sys', 'global', 'optActive', '1'),
('sys', 'global', 'timestamp', '269'),
('sys', 'global', 'mudim_displaymode', '1'),
('sys', 'global', 'mudim_method', '4'),
('sys', 'global', 'mudim_showpanel', '1'),
('sys', 'global', 'mudim_active', '1'),
('sys', 'global', 'captcha_type', '0'),
('sys', 'global', 'version', '4.0.10'),
('sys', 'global', 'whoviewuser', '2'),
('sys', 'global', 'facebook_client_id', ''),
('sys', 'global', 'facebook_client_secret', ''),
('sys', 'global', 'cookie_httponly', '1'),
('sys', 'global', 'admin_check_pass_time', '1800'),
('sys', 'global', 'adminrelogin_max', '3'),
('sys', 'global', 'cookie_secure', '0'),
('sys', 'global', 'nv_unick_type', '4'),
('sys', 'global', 'nv_upass_type', '0'),
('sys', 'global', 'is_flood_blocker', '1'),
('sys', 'global', 'max_requests_60', '40'),
('sys', 'global', 'max_requests_300', '150'),
('sys', 'global', 'nv_display_errors_list', '1'),
('sys', 'global', 'display_errors_list', '1'),
('sys', 'global', 'nv_auto_resize', '1'),
('sys', 'global', 'dump_interval', '1'),
('sys', 'global', 'cdn_url', ''),
('sys', 'define', 'nv_unickmin', '4'),
('sys', 'define', 'nv_unickmax', '20'),
('sys', 'define', 'nv_upassmin', '5'),
('sys', 'define', 'nv_upassmax', '20'),
('sys', 'define', 'nv_gfx_num', '6'),
('sys', 'define', 'nv_gfx_width', '120'),
('sys', 'define', 'nv_gfx_height', '25'),
('sys', 'define', 'nv_max_width', '1500'),
('sys', 'define', 'nv_max_height', '1500'),
('sys', 'define', 'nv_live_cookie_time', '31104000'),
('sys', 'define', 'nv_live_session_time', '0'),
('sys', 'define', 'nv_anti_iframe', '0'),
('sys', 'define', 'nv_anti_agent', '0'),
('sys', 'define', 'nv_allowed_html_tags', 'embed, object, param, a, b, blockquote, br, caption, col, colgroup, div, em, h1, h2, h3, h4, h5, h6, hr, i, img, li, p, span, strong, sub, sup, table, tbody, td, th, tr, u, ul, iframe, figure, figcaption, video, source, track, code, pre'),
('sys', 'define', 'dir_forum', ''),
('vi', 'global', 'site_domain', ''),
('vi', 'global', 'site_name', 'Kết cấu xây dựng'),
('vi', 'global', 'site_logo', 'images/ket-cau-xay-dung_1.png'),
('vi', 'global', 'site_description', 'Kết cấu công trình, kiến trúc, kết cấu thép, nền móng công trình, tư vấn và thi công xây dựng'),
('vi', 'global', 'site_keywords', 'kết cấu xây dựng, kết cấu thép, kết cấu bê tông, kết cấu móng, kết cấu hầm, kết cấu vách, kết cấu lõi, kết cấu dàn không gian, structures'),
('vi', 'global', 'site_theme', 'default'),
('vi', 'global', 'mobile_theme', ''),
('vi', 'global', 'site_home_module', 'news'),
('vi', 'global', 'switch_mobi_des', '1'),
('vi', 'global', 'upload_logo', 'images/logo.png'),
('vi', 'global', 'autologosize1', '50'),
('vi', 'global', 'autologosize2', '40'),
('vi', 'global', 'autologosize3', '30'),
('vi', 'global', 'autologomod', ''),
('vi', 'global', 'cronjobs_next_time', '1423131326'),
('vi', 'global', 'disable_site_content', '<p>Vì lý do kỹ thuật website tạm ngưng hoạt động. Thành thật xin lỗi các bạn vì sự bất tiện này!</p>'),
('vi', 'seotools', 'prcservice', ''),
('vi', 'about', 'auto_postcomm', '1'),
('vi', 'about', 'allowed_comm', '-1'),
('vi', 'about', 'view_comm', '6'),
('vi', 'about', 'setcomm', '4'),
('vi', 'about', 'activecomm', '0'),
('vi', 'about', 'emailcomm', '0'),
('vi', 'about', 'adminscomm', ''),
('vi', 'about', 'sortcomm', '0'),
('vi', 'about', 'captcha', '1'),
('vi', 'news', 'indexfile', 'viewcat_two_column'),
('vi', 'news', 'per_page', '20'),
('vi', 'news', 'st_links', '10'),
('vi', 'news', 'homewidth', '280'),
('vi', 'news', 'homeheight', '173'),
('vi', 'news', 'blockwidth', '100'),
('vi', 'news', 'blockheight', '80'),
('vi', 'news', 'imagefull', '460'),
('vi', 'news', 'copyright', 'Chú ý&#x3A; Việc đăng lại bài viết trên ở website hoặc các phương tiện truyền thông khác mà không ghi rõ nguồn http&#x3A;&#x002F;&#x002F;nukeviet.vn là vi phạm bản quyền'),
('vi', 'news', 'showtooltip', '0'),
('vi', 'news', 'tooltip_position', 'bottom'),
('vi', 'news', 'tooltip_length', '150'),
('vi', 'news', 'showhometext', '1'),
('vi', 'news', 'timecheckstatus', '0'),
('vi', 'news', 'config_source', '0'),
('vi', 'news', 'show_no_image', ''),
('vi', 'news', 'allowed_rating_point', '1'),
('vi', 'news', 'facebookappid', ''),
('vi', 'news', 'socialbutton', '1'),
('vi', 'news', 'tags_alias', '0'),
('vi', 'news', 'auto_tags', '0'),
('vi', 'news', 'tags_remind', '1'),
('vi', 'news', 'structure_upload', 'Ym'),
('vi', 'news', 'imgposition', '1'),
('vi', 'news', 'auto_postcomm', '1'),
('vi', 'news', 'allowed_comm', '-1'),
('vi', 'news', 'view_comm', '6'),
('vi', 'news', 'setcomm', '4'),
('vi', 'news', 'activecomm', '0'),
('vi', 'news', 'emailcomm', '0'),
('vi', 'news', 'adminscomm', ''),
('vi', 'news', 'sortcomm', '0'),
('vi', 'news', 'captcha', '1'),
('vi', 'page', 'auto_postcomm', '1'),
('vi', 'page', 'allowed_comm', '-1'),
('vi', 'page', 'view_comm', '6'),
('vi', 'page', 'setcomm', '4'),
('vi', 'page', 'activecomm', '1'),
('vi', 'page', 'emailcomm', '0'),
('vi', 'page', 'adminscomm', ''),
('vi', 'page', 'sortcomm', '0'),
('vi', 'page', 'captcha', '1'),
('sys', 'site', 'statistics_timezone', 'Asia/Bangkok'),
('sys', 'site', 'site_email', 'tranvanthi@gmail.com'),
('sys', 'global', 'error_send_email', 'tranvanthi@gmail.com'),
('sys', 'global', 'site_lang', 'vi'),
('sys', 'global', 'my_domains', 'localhost'),
('sys', 'global', 'cookie_prefix', 'nv3c_Bm64h'),
('sys', 'global', 'session_prefix', 'nv3s_R5shqa'),
('sys', 'global', 'site_timezone', 'byCountry'),
('sys', 'global', 'proxy_blocker', '0'),
('sys', 'global', 'str_referer_blocker', '0'),
('sys', 'global', 'lang_multi', '0'),
('sys', 'global', 'lang_geo', '0'),
('sys', 'global', 'ftp_server', 'localhost'),
('sys', 'global', 'ftp_port', '21'),
('sys', 'global', 'ftp_user_name', ''),
('sys', 'global', 'ftp_user_pass', 'N3KzoRLcRkQM37nfYLQi2Tdys6ES3EZEDN-532C0Itk,'),
('sys', 'global', 'ftp_path', '/'),
('sys', 'global', 'ftp_check_login', '0'),
('vi', 'download', 'auto_postcomm', '1'),
('vi', 'download', 'allowed_comm', '-1'),
('vi', 'download', 'view_comm', '6'),
('vi', 'download', 'setcomm', '4'),
('vi', 'download', 'activecomm', '1'),
('vi', 'download', 'emailcomm', '0'),
('vi', 'download', 'adminscomm', ''),
('vi', 'download', 'sortcomm', '0'),
('vi', 'download', 'captcha', '1');

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_counter`
--

CREATE TABLE IF NOT EXISTS `kcxd_counter` (
  `c_type` varchar(100) NOT NULL,
  `c_val` varchar(100) NOT NULL,
  `last_update` int(11) NOT NULL DEFAULT '0',
  `c_count` int(11) unsigned NOT NULL DEFAULT '0',
  `vi_count` int(11) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `c_type` (`c_type`,`c_val`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kcxd_counter`
--

INSERT INTO `kcxd_counter` (`c_type`, `c_val`, `last_update`, `c_count`, `vi_count`) VALUES
('c_time', 'start', 0, 0, 0),
('c_time', 'last', 0, 1423129941, 0),
('total', 'hits', 1423129941, 103, 103),
('year', '2015', 1423129941, 103, 103),
('year', '2016', 0, 0, 0),
('year', '2017', 0, 0, 0),
('year', '2018', 0, 0, 0),
('year', '2019', 0, 0, 0),
('year', '2020', 0, 0, 0),
('year', '2021', 0, 0, 0),
('year', '2022', 0, 0, 0),
('year', '2023', 0, 0, 0),
('month', 'Jan', 1422543034, 97, 97),
('month', 'Feb', 1423129941, 6, 6),
('month', 'Mar', 0, 0, 0),
('month', 'Apr', 0, 0, 0),
('month', 'May', 0, 0, 0),
('month', 'Jun', 0, 0, 0),
('month', 'Jul', 0, 0, 0),
('month', 'Aug', 0, 0, 0),
('month', 'Sep', 0, 0, 0),
('month', 'Oct', 0, 0, 0),
('month', 'Nov', 0, 0, 0),
('month', 'Dec', 0, 0, 0),
('day', '01', 1422770271, 4, 4),
('day', '02', 0, 0, 0),
('day', '03', 1422956786, 1, 1),
('day', '04', 0, 0, 0),
('day', '05', 1423129941, 1, 1),
('day', '06', 0, 0, 0),
('day', '07', 0, 0, 0),
('day', '08', 0, 0, 0),
('day', '09', 0, 0, 0),
('day', '10', 0, 0, 0),
('day', '11', 0, 0, 0),
('day', '12', 0, 0, 0),
('day', '13', 0, 0, 0),
('day', '14', 1421254187, 0, 0),
('day', '15', 1421337736, 0, 0),
('day', '16', 1421425296, 0, 0),
('day', '17', 1421513276, 0, 0),
('day', '18', 1421600258, 0, 0),
('day', '19', 1421657391, 0, 0),
('day', '20', 0, 0, 0),
('day', '21', 0, 0, 0),
('day', '22', 1421944300, 0, 0),
('day', '23', 1422007914, 0, 0),
('day', '24', 0, 0, 0),
('day', '25', 0, 0, 0),
('day', '26', 0, 0, 0),
('day', '27', 1422371920, 0, 0),
('day', '28', 0, 0, 0),
('day', '29', 1422543034, 0, 0),
('day', '30', 0, 0, 0),
('day', '31', 0, 0, 0),
('dayofweek', 'Sunday', 1422770271, 24, 24),
('dayofweek', 'Monday', 1421657391, 18, 18),
('dayofweek', 'Tuesday', 1422956786, 5, 5),
('dayofweek', 'Wednesday', 1421254187, 2, 2),
('dayofweek', 'Thursday', 1423129941, 16, 16),
('dayofweek', 'Friday', 1422007914, 23, 23),
('dayofweek', 'Saturday', 1421513276, 15, 15),
('hour', '00', 1422293827, 0, 0),
('hour', '01', 1422295658, 0, 0),
('hour', '02', 1421953933, 0, 0),
('hour', '03', 1421614671, 0, 0),
('hour', '04', 1421618275, 0, 0),
('hour', '05', 1421533001, 0, 0),
('hour', '06', 0, 0, 0),
('hour', '07', 0, 0, 0),
('hour', '08', 1422753404, 0, 0),
('hour', '09', 1421980698, 0, 0),
('hour', '10', 1422761597, 0, 0),
('hour', '11', 1422763540, 0, 0),
('hour', '12', 1422770271, 0, 0),
('hour', '13', 1421994831, 0, 0),
('hour', '14', 1421652926, 0, 0),
('hour', '15', 1421657391, 0, 0),
('hour', '16', 1423129941, 1, 1),
('hour', '17', 1422007914, 0, 0),
('hour', '18', 1421494041, 0, 0),
('hour', '19', 1422535054, 0, 0),
('hour', '20', 1421934188, 0, 0),
('hour', '21', 1422543034, 0, 0),
('hour', '22', 1422371920, 0, 0),
('hour', '23', 1421944300, 0, 0),
('bot', 'Alexa', 0, 0, 0),
('bot', 'AltaVista Scooter', 0, 0, 0),
('bot', 'Altavista Mercator', 0, 0, 0),
('bot', 'Altavista Search', 0, 0, 0),
('bot', 'Aport.ru Bot', 0, 0, 0),
('bot', 'Ask Jeeves', 0, 0, 0),
('bot', 'Baidu', 0, 0, 0),
('bot', 'Exabot', 0, 0, 0),
('bot', 'FAST Enterprise', 0, 0, 0),
('bot', 'FAST WebCrawler', 0, 0, 0),
('bot', 'Francis', 0, 0, 0),
('bot', 'Gigablast', 0, 0, 0),
('bot', 'Google AdsBot', 0, 0, 0),
('bot', 'Google Adsense', 0, 0, 0),
('bot', 'Google Bot', 0, 0, 0),
('bot', 'Google Desktop', 0, 0, 0),
('bot', 'Google Feedfetcher', 0, 0, 0),
('bot', 'Heise IT-Markt', 0, 0, 0),
('bot', 'Heritrix', 0, 0, 0),
('bot', 'IBM Research', 0, 0, 0),
('bot', 'ICCrawler - ICjobs', 0, 0, 0),
('bot', 'Ichiro', 0, 0, 0),
('bot', 'InfoSeek Spider', 0, 0, 0),
('bot', 'Lycos.com Bot', 0, 0, 0),
('bot', 'MSN Bot', 0, 0, 0),
('bot', 'MSN Bot Media', 0, 0, 0),
('bot', 'MSN Bot News', 0, 0, 0),
('bot', 'MSN NewsBlogs', 0, 0, 0),
('bot', 'Majestic-12', 0, 0, 0),
('bot', 'Metager', 0, 0, 0),
('bot', 'NG-Search', 0, 0, 0),
('bot', 'Nutch Bot', 0, 0, 0),
('bot', 'NutchCVS', 0, 0, 0),
('bot', 'OmniExplorer', 0, 0, 0),
('bot', 'Online Link Validator', 0, 0, 0),
('bot', 'Open-source Web Search', 0, 0, 0),
('bot', 'Psbot', 0, 0, 0),
('bot', 'Rambler', 0, 0, 0),
('bot', 'SEO Crawler', 0, 0, 0),
('bot', 'SEOSearch', 0, 0, 0),
('bot', 'Seekport', 0, 0, 0),
('bot', 'Sensis', 0, 0, 0),
('bot', 'Seoma', 0, 0, 0),
('bot', 'Snappy', 0, 0, 0),
('bot', 'Steeler', 0, 0, 0),
('bot', 'Synoo', 0, 0, 0),
('bot', 'Telekom', 0, 0, 0),
('bot', 'TurnitinBot', 0, 0, 0),
('bot', 'Vietnamese Search', 0, 0, 0),
('bot', 'Voyager', 0, 0, 0),
('bot', 'W3 Sitesearch', 0, 0, 0),
('bot', 'W3C Linkcheck', 0, 0, 0),
('bot', 'W3C Validator', 0, 0, 0),
('bot', 'WiseNut', 0, 0, 0),
('bot', 'YaCy', 0, 0, 0),
('bot', 'Yahoo Bot', 0, 0, 0),
('bot', 'Yahoo MMCrawler', 0, 0, 0),
('bot', 'Yahoo Slurp', 0, 0, 0),
('bot', 'YahooSeeker', 0, 0, 0),
('bot', 'Yandex', 0, 0, 0),
('bot', 'Yandex Blog', 0, 0, 0),
('bot', 'Yandex Direct Bot', 0, 0, 0),
('bot', 'Yandex Something', 0, 0, 0),
('browser', 'netcaptor', 0, 0, 0),
('browser', 'opera', 0, 0, 0),
('browser', 'aol', 0, 0, 0),
('browser', 'aol2', 0, 0, 0),
('browser', 'mosaic', 0, 0, 0),
('browser', 'k-meleon', 0, 0, 0),
('browser', 'konqueror', 0, 0, 0),
('browser', 'avantbrowser', 0, 0, 0),
('browser', 'avantgo', 0, 0, 0),
('browser', 'proxomitron', 0, 0, 0),
('browser', 'chrome', 1421557812, 1, 1),
('browser', 'safari', 0, 0, 0),
('browser', 'lynx', 0, 0, 0),
('browser', 'links', 0, 0, 0),
('browser', 'galeon', 0, 0, 0),
('browser', 'abrowse', 0, 0, 0),
('browser', 'amaya', 0, 0, 0),
('browser', 'ant', 0, 0, 0),
('browser', 'aweb', 0, 0, 0),
('browser', 'beonex', 0, 0, 0),
('browser', 'blazer', 0, 0, 0),
('browser', 'camino', 0, 0, 0),
('browser', 'chimera', 0, 0, 0),
('browser', 'columbus', 0, 0, 0),
('browser', 'crazybrowser', 0, 0, 0),
('browser', 'curl', 0, 0, 0),
('browser', 'deepnet', 0, 0, 0),
('browser', 'dillo', 0, 0, 0),
('browser', 'doris', 0, 0, 0),
('browser', 'elinks', 0, 0, 0),
('browser', 'epiphany', 0, 0, 0),
('browser', 'ibrowse', 0, 0, 0),
('browser', 'icab', 0, 0, 0),
('browser', 'ice', 0, 0, 0),
('browser', 'isilox', 0, 0, 0),
('browser', 'lotus', 0, 0, 0),
('browser', 'lunascape', 0, 0, 0),
('browser', 'maxthon', 0, 0, 0),
('browser', 'mbrowser', 0, 0, 0),
('browser', 'multibrowser', 0, 0, 0),
('browser', 'nautilus', 0, 0, 0),
('browser', 'netfront', 0, 0, 0),
('browser', 'netpositive', 0, 0, 0),
('browser', 'omniweb', 0, 0, 0),
('browser', 'oregano', 0, 0, 0),
('browser', 'phaseout', 0, 0, 0),
('browser', 'plink', 0, 0, 0),
('browser', 'phoenix', 0, 0, 0),
('browser', 'shiira', 0, 0, 0),
('browser', 'sleipnir', 0, 0, 0),
('browser', 'slimbrowser', 0, 0, 0),
('browser', 'staroffice', 0, 0, 0),
('browser', 'sunrise', 0, 0, 0),
('browser', 'voyager', 0, 0, 0),
('browser', 'w3m', 0, 0, 0),
('browser', 'webtv', 0, 0, 0),
('browser', 'xiino', 0, 0, 0),
('browser', 'explorer', 0, 0, 0),
('browser', 'firefox', 1423129941, 102, 102),
('browser', 'netscape', 0, 0, 0),
('browser', 'netscape2', 0, 0, 0),
('browser', 'mozilla', 0, 0, 0),
('browser', 'mozilla2', 0, 0, 0),
('browser', 'firebird', 0, 0, 0),
('browser', 'Mobile', 0, 0, 0),
('browser', 'bots', 0, 0, 0),
('browser', 'Unknown', 0, 0, 0),
('browser', 'Unspecified', 0, 0, 0),
('os', 'windows8', 0, 0, 0),
('os', 'windows7', 1423129941, 103, 103),
('os', 'windowsvista', 0, 0, 0),
('os', 'windows2003', 0, 0, 0),
('os', 'windowsxp', 0, 0, 0),
('os', 'windowsxp2', 0, 0, 0),
('os', 'windows2k', 0, 0, 0),
('os', 'windows95', 0, 0, 0),
('os', 'windowsce', 0, 0, 0),
('os', 'windowsme', 0, 0, 0),
('os', 'windowsme2', 0, 0, 0),
('os', 'windowsnt', 0, 0, 0),
('os', 'windowsnt2', 0, 0, 0),
('os', 'windows98', 0, 0, 0),
('os', 'windows', 0, 0, 0),
('os', 'linux', 0, 0, 0),
('os', 'linux2', 0, 0, 0),
('os', 'linux3', 0, 0, 0),
('os', 'macosx', 0, 0, 0),
('os', 'macppc', 0, 0, 0),
('os', 'mac', 0, 0, 0),
('os', 'amiga', 0, 0, 0),
('os', 'beos', 0, 0, 0),
('os', 'freebsd', 0, 0, 0),
('os', 'freebsd2', 0, 0, 0),
('os', 'irix', 0, 0, 0),
('os', 'netbsd', 0, 0, 0),
('os', 'netbsd2', 0, 0, 0),
('os', 'os2', 0, 0, 0),
('os', 'os22', 0, 0, 0),
('os', 'openbsd', 0, 0, 0),
('os', 'openbsd2', 0, 0, 0),
('os', 'palm', 0, 0, 0),
('os', 'palm2', 0, 0, 0),
('os', 'Unspecified', 0, 0, 0),
('country', 'AD', 0, 0, 0),
('country', 'AE', 0, 0, 0),
('country', 'AF', 0, 0, 0),
('country', 'AG', 0, 0, 0),
('country', 'AI', 0, 0, 0),
('country', 'AL', 0, 0, 0),
('country', 'AM', 0, 0, 0),
('country', 'AN', 0, 0, 0),
('country', 'AO', 0, 0, 0),
('country', 'AQ', 0, 0, 0),
('country', 'AR', 0, 0, 0),
('country', 'AS', 0, 0, 0),
('country', 'AT', 0, 0, 0),
('country', 'AU', 0, 0, 0),
('country', 'AW', 0, 0, 0),
('country', 'AZ', 0, 0, 0),
('country', 'BA', 0, 0, 0),
('country', 'BB', 0, 0, 0),
('country', 'BD', 0, 0, 0),
('country', 'BE', 0, 0, 0),
('country', 'BF', 0, 0, 0),
('country', 'BG', 0, 0, 0),
('country', 'BH', 0, 0, 0),
('country', 'BI', 0, 0, 0),
('country', 'BJ', 0, 0, 0),
('country', 'BM', 0, 0, 0),
('country', 'BN', 0, 0, 0),
('country', 'BO', 0, 0, 0),
('country', 'BR', 0, 0, 0),
('country', 'BS', 0, 0, 0),
('country', 'BT', 0, 0, 0),
('country', 'BW', 0, 0, 0),
('country', 'BY', 0, 0, 0),
('country', 'BZ', 0, 0, 0),
('country', 'CA', 0, 0, 0),
('country', 'CD', 0, 0, 0),
('country', 'CF', 0, 0, 0),
('country', 'CG', 0, 0, 0),
('country', 'CH', 0, 0, 0),
('country', 'CI', 0, 0, 0),
('country', 'CK', 0, 0, 0),
('country', 'CL', 0, 0, 0),
('country', 'CM', 0, 0, 0),
('country', 'CN', 0, 0, 0),
('country', 'CO', 0, 0, 0),
('country', 'CR', 0, 0, 0),
('country', 'CS', 0, 0, 0),
('country', 'CU', 0, 0, 0),
('country', 'CV', 0, 0, 0),
('country', 'CY', 0, 0, 0),
('country', 'CZ', 0, 0, 0),
('country', 'DE', 0, 0, 0),
('country', 'DJ', 0, 0, 0),
('country', 'DK', 0, 0, 0),
('country', 'DM', 0, 0, 0),
('country', 'DO', 0, 0, 0),
('country', 'DZ', 0, 0, 0),
('country', 'EC', 0, 0, 0),
('country', 'EE', 0, 0, 0),
('country', 'EG', 0, 0, 0),
('country', 'ER', 0, 0, 0),
('country', 'ES', 0, 0, 0),
('country', 'ET', 0, 0, 0),
('country', 'EU', 0, 0, 0),
('country', 'FI', 0, 0, 0),
('country', 'FJ', 0, 0, 0),
('country', 'FK', 0, 0, 0),
('country', 'FM', 0, 0, 0),
('country', 'FO', 0, 0, 0),
('country', 'FR', 0, 0, 0),
('country', 'GA', 0, 0, 0),
('country', 'GB', 0, 0, 0),
('country', 'GD', 0, 0, 0),
('country', 'GE', 0, 0, 0),
('country', 'GF', 0, 0, 0),
('country', 'GH', 0, 0, 0),
('country', 'GI', 0, 0, 0),
('country', 'GL', 0, 0, 0),
('country', 'GM', 0, 0, 0),
('country', 'GN', 0, 0, 0),
('country', 'GP', 0, 0, 0),
('country', 'GQ', 0, 0, 0),
('country', 'GR', 0, 0, 0),
('country', 'GS', 0, 0, 0),
('country', 'GT', 0, 0, 0),
('country', 'GU', 0, 0, 0),
('country', 'GW', 0, 0, 0),
('country', 'GY', 0, 0, 0),
('country', 'HK', 0, 0, 0),
('country', 'HN', 0, 0, 0),
('country', 'HR', 0, 0, 0),
('country', 'HT', 0, 0, 0),
('country', 'HU', 0, 0, 0),
('country', 'ID', 0, 0, 0),
('country', 'IE', 0, 0, 0),
('country', 'IL', 0, 0, 0),
('country', 'IN', 0, 0, 0),
('country', 'IO', 0, 0, 0),
('country', 'IQ', 0, 0, 0),
('country', 'IR', 0, 0, 0),
('country', 'IS', 0, 0, 0),
('country', 'IT', 0, 0, 0),
('country', 'JM', 0, 0, 0),
('country', 'JO', 0, 0, 0),
('country', 'JP', 0, 0, 0),
('country', 'KE', 0, 0, 0),
('country', 'KG', 0, 0, 0),
('country', 'KH', 0, 0, 0),
('country', 'KI', 0, 0, 0),
('country', 'KM', 0, 0, 0),
('country', 'KN', 0, 0, 0),
('country', 'KR', 0, 0, 0),
('country', 'KW', 0, 0, 0),
('country', 'KY', 0, 0, 0),
('country', 'KZ', 0, 0, 0),
('country', 'LA', 0, 0, 0),
('country', 'LB', 0, 0, 0),
('country', 'LC', 0, 0, 0),
('country', 'LI', 0, 0, 0),
('country', 'LK', 0, 0, 0),
('country', 'LR', 0, 0, 0),
('country', 'LS', 0, 0, 0),
('country', 'LT', 0, 0, 0),
('country', 'LU', 0, 0, 0),
('country', 'LV', 0, 0, 0),
('country', 'LY', 0, 0, 0),
('country', 'MA', 0, 0, 0),
('country', 'MC', 0, 0, 0),
('country', 'MD', 0, 0, 0),
('country', 'MG', 0, 0, 0),
('country', 'MH', 0, 0, 0),
('country', 'MK', 0, 0, 0),
('country', 'ML', 0, 0, 0),
('country', 'MM', 0, 0, 0),
('country', 'MN', 0, 0, 0),
('country', 'MO', 0, 0, 0),
('country', 'MP', 0, 0, 0),
('country', 'MQ', 0, 0, 0),
('country', 'MR', 0, 0, 0),
('country', 'MT', 0, 0, 0),
('country', 'MU', 0, 0, 0),
('country', 'MV', 0, 0, 0),
('country', 'MW', 0, 0, 0),
('country', 'MX', 0, 0, 0),
('country', 'MY', 0, 0, 0),
('country', 'MZ', 0, 0, 0),
('country', 'NA', 0, 0, 0),
('country', 'NC', 0, 0, 0),
('country', 'NE', 0, 0, 0),
('country', 'NF', 0, 0, 0),
('country', 'NG', 0, 0, 0),
('country', 'NI', 0, 0, 0),
('country', 'NL', 0, 0, 0),
('country', 'NO', 0, 0, 0),
('country', 'NP', 0, 0, 0),
('country', 'NR', 0, 0, 0),
('country', 'NU', 0, 0, 0),
('country', 'NZ', 0, 0, 0),
('country', 'OM', 0, 0, 0),
('country', 'PA', 0, 0, 0),
('country', 'PE', 0, 0, 0),
('country', 'PF', 0, 0, 0),
('country', 'PG', 0, 0, 0),
('country', 'PH', 0, 0, 0),
('country', 'PK', 0, 0, 0),
('country', 'PL', 0, 0, 0),
('country', 'PR', 0, 0, 0),
('country', 'PS', 0, 0, 0),
('country', 'PT', 0, 0, 0),
('country', 'PW', 0, 0, 0),
('country', 'PY', 0, 0, 0),
('country', 'QA', 0, 0, 0),
('country', 'RE', 0, 0, 0),
('country', 'RO', 0, 0, 0),
('country', 'RU', 0, 0, 0),
('country', 'RW', 0, 0, 0),
('country', 'SA', 0, 0, 0),
('country', 'SB', 0, 0, 0),
('country', 'SC', 0, 0, 0),
('country', 'SD', 0, 0, 0),
('country', 'SE', 0, 0, 0),
('country', 'SG', 0, 0, 0),
('country', 'SI', 0, 0, 0),
('country', 'SK', 0, 0, 0),
('country', 'SL', 0, 0, 0),
('country', 'SM', 0, 0, 0),
('country', 'SN', 0, 0, 0),
('country', 'SO', 0, 0, 0),
('country', 'SR', 0, 0, 0),
('country', 'ST', 0, 0, 0),
('country', 'SV', 0, 0, 0),
('country', 'SY', 0, 0, 0),
('country', 'SZ', 0, 0, 0),
('country', 'TD', 0, 0, 0),
('country', 'TF', 0, 0, 0),
('country', 'TG', 0, 0, 0),
('country', 'TH', 0, 0, 0),
('country', 'TJ', 0, 0, 0),
('country', 'TK', 0, 0, 0),
('country', 'TL', 0, 0, 0),
('country', 'TM', 0, 0, 0),
('country', 'TN', 0, 0, 0),
('country', 'TO', 0, 0, 0),
('country', 'TR', 0, 0, 0),
('country', 'TT', 0, 0, 0),
('country', 'TV', 0, 0, 0),
('country', 'TW', 0, 0, 0),
('country', 'TZ', 0, 0, 0),
('country', 'UA', 0, 0, 0),
('country', 'UG', 0, 0, 0),
('country', 'US', 0, 0, 0),
('country', 'UY', 0, 0, 0),
('country', 'UZ', 0, 0, 0),
('country', 'VA', 0, 0, 0),
('country', 'VC', 0, 0, 0),
('country', 'VE', 0, 0, 0),
('country', 'VG', 0, 0, 0),
('country', 'VI', 0, 0, 0),
('country', 'VN', 0, 0, 0),
('country', 'VU', 0, 0, 0),
('country', 'WS', 0, 0, 0),
('country', 'YE', 0, 0, 0),
('country', 'YT', 0, 0, 0),
('country', 'YU', 0, 0, 0),
('country', 'ZA', 0, 0, 0),
('country', 'ZM', 0, 0, 0),
('country', 'ZW', 0, 0, 0),
('country', 'ZZ', 1423129941, 103, 103),
('country', 'unkown', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_cronjobs`
--

CREATE TABLE IF NOT EXISTS `kcxd_cronjobs` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `start_time` int(11) unsigned NOT NULL DEFAULT '0',
  `inter_val` int(11) unsigned NOT NULL DEFAULT '0',
  `run_file` varchar(255) NOT NULL,
  `run_func` varchar(255) NOT NULL,
  `params` varchar(255) DEFAULT NULL,
  `del` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_sys` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `act` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `last_time` int(11) unsigned NOT NULL DEFAULT '0',
  `last_result` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `vi_cron_name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `is_sys` (`is_sys`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `kcxd_cronjobs`
--

INSERT INTO `kcxd_cronjobs` (`id`, `start_time`, `inter_val`, `run_file`, `run_func`, `params`, `del`, `is_sys`, `act`, `last_time`, `last_result`, `vi_cron_name`) VALUES
(1, 1421251986, 5, 'online_expired_del.php', 'cron_online_expired_del', '', 0, 1, 1, 1423131026, 1, 'Xóa các dòng ghi trạng thái online đã cũ trong CSDL'),
(2, 1421251986, 1440, 'dump_autobackup.php', 'cron_dump_autobackup', '', 0, 1, 1, 1423129954, 1, 'Tự động lưu CSDL'),
(3, 1421251986, 60, 'temp_download_destroy.php', 'cron_auto_del_temp_download', '', 0, 1, 1, 1423129954, 1, 'Xóa các file tạm trong thư mục tmp'),
(4, 1421251986, 30, 'ip_logs_destroy.php', 'cron_del_ip_logs', '', 0, 1, 1, 1423129954, 1, 'Xóa IP log files Xóa các file logo truy cập'),
(5, 1421251986, 1440, 'error_log_destroy.php', 'cron_auto_del_error_log', '', 0, 1, 1, 1423129954, 1, 'Xóa các file error_log quá hạn'),
(6, 1421251986, 360, 'error_log_sendmail.php', 'cron_auto_sendmail_error_log', '', 0, 1, 0, 0, 0, 'Gửi email các thông báo lỗi cho admin'),
(7, 1421251986, 60, 'ref_expired_del.php', 'cron_ref_expired_del', '', 0, 1, 1, 1423129954, 1, 'Xóa các referer quá hạn'),
(8, 1421251986, 1440, 'siteDiagnostic_update.php', 'cron_siteDiagnostic_update', '', 0, 0, 1, 1423129954, 1, 'Cập nhật đánh giá site từ các máy chủ tìm kiếm'),
(9, 1421251986, 60, 'check_version.php', 'cron_auto_check_version', '', 0, 1, 1, 1423129954, 1, 'Kiểm tra phiên bản NukeViet');

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_googleplus`
--

CREATE TABLE IF NOT EXISTS `kcxd_googleplus` (
  `gid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `idprofile` varchar(25) NOT NULL DEFAULT '',
  `weight` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `add_time` int(11) unsigned NOT NULL DEFAULT '0',
  `edit_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`gid`),
  UNIQUE KEY `idprofile` (`idprofile`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_groups`
--

CREATE TABLE IF NOT EXISTS `kcxd_groups` (
  `group_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text,
  `add_time` int(11) NOT NULL,
  `exp_time` int(11) NOT NULL,
  `publics` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` int(11) unsigned NOT NULL DEFAULT '0',
  `act` tinyint(1) unsigned NOT NULL,
  `idsite` int(11) unsigned NOT NULL DEFAULT '0',
  `numbers` mediumint(9) unsigned NOT NULL DEFAULT '0',
  `siteus` tinyint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`),
  UNIQUE KEY `ktitle` (`title`,`idsite`),
  KEY `exp_time` (`exp_time`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `kcxd_groups`
--

INSERT INTO `kcxd_groups` (`group_id`, `title`, `content`, `add_time`, `exp_time`, `publics`, `weight`, `act`, `idsite`, `numbers`, `siteus`) VALUES
(6, 'All', '', 1421251986, 0, 0, 1, 1, 0, 0, 0),
(5, 'Guest', '', 1421251986, 0, 0, 2, 1, 0, 0, 0),
(4, 'Users', '', 1421251986, 0, 0, 3, 1, 0, 1, 0),
(1, 'Super admin', '', 1421251986, 0, 0, 4, 1, 0, 1, 0),
(2, 'General admin', '', 1421251986, 0, 0, 5, 1, 0, 0, 0),
(3, 'Module admin', '', 1421251986, 0, 0, 6, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_groups_users`
--

CREATE TABLE IF NOT EXISTS `kcxd_groups_users` (
  `group_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  PRIMARY KEY (`group_id`,`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kcxd_groups_users`
--

INSERT INTO `kcxd_groups_users` (`group_id`, `userid`, `data`) VALUES
(1, 1, '0');

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_language`
--

CREATE TABLE IF NOT EXISTS `kcxd_language` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `idfile` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_key` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `filelang` (`idfile`,`lang_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_language_file`
--

CREATE TABLE IF NOT EXISTS `kcxd_language_file` (
  `idfile` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `module` varchar(50) NOT NULL,
  `admin_file` varchar(255) NOT NULL DEFAULT '0',
  `langtype` varchar(50) NOT NULL,
  PRIMARY KEY (`idfile`),
  UNIQUE KEY `module` (`module`,`admin_file`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_logs`
--

CREATE TABLE IF NOT EXISTS `kcxd_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(10) NOT NULL,
  `module_name` varchar(150) NOT NULL,
  `name_key` varchar(255) NOT NULL,
  `note_action` text NOT NULL,
  `link_acess` varchar(255) DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL,
  `log_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=464 ;

--
-- Dumping data for table `kcxd_logs`
--

INSERT INTO `kcxd_logs` (`id`, `lang`, `module_name`, `name_key`, `note_action`, `link_acess`, `userid`, `log_time`) VALUES
(1, 'vi', 'login', '[admin] Đăng nhập', ' Client IP:127.0.0.1', '', 0, 1421252045),
(2, 'vi', 'news', 'Xóa bài viêt', 'Ra mắt công ty mã nguồn mở đầu tiên tại Việt Nam, Công bố dự án NukeViet 3.0 sau 1 tháng ra mắt VINADES.,JSC, Giới thiệu về mã nguồn mở NukeViet, Thư mời hợp tác liên kết quảng cáo và cung cấp hosting thử nghiệm, Tuyển dụng lập trình viên, chuyên viên đồ họa phát triển NukeViet, Webnhanh.vn - website dịch vụ chuyên nghiệp cho NukeViet chính thức ra mắt, NukeViet - Công cụ mã nguồn mở cho cộng đồng thiết kế website Việt Nam, Mã nguồn mở NukeViet giành giải ba Nhân tài đất Việt 2011', '', 1, 1421252064),
(3, 'vi', 'about', 'Delete', 'ID: 1', '', 1, 1421252071),
(4, 'vi', 'about', 'Delete', 'ID: 2', '', 1, 1421252072),
(5, 'vi', 'news', 'Xóa Chuyên mục và các bài viết', 'Sản phẩm', '', 1, 1421252083),
(6, 'vi', 'news', 'Xóa Chuyên mục và các bài viết', 'Đối tác', '', 1, 1421252085),
(7, 'vi', 'news', 'Xóa Chuyên mục và các bài viết', 'Tuyển dụng', '', 1, 1421252087),
(8, 'vi', 'news', 'Xóa Chuyên mục và các bài viết', 'Thông cáo báo chí', '', 1, 1421252094),
(9, 'vi', 'news', 'Xóa Chuyên mục và các bài viết', 'Bản tin nội bộ', '', 1, 1421252096),
(10, 'vi', 'news', 'Xóa Chuyên mục và các bài viết', 'Tin công nghệ', '', 1, 1421252098),
(11, 'vi', 'news', 'Xóa Chuyên mục và các bài viết', 'Tin tức', '', 1, 1421252105),
(12, 'vi', 'news', 'log_del_topic', 'topicid 1', '', 1, 1421252122),
(13, 'vi', 'news', 'log_del_source', 'Báo Hà Nội Mới', '', 1, 1421252127),
(14, 'vi', 'news', 'log_del_source', 'VINADES.,JSC', '', 1, 1421252128),
(15, 'vi', 'news', 'log_del_source', 'NukeViet', '', 1, 1421252130),
(16, 'vi', 'news', 'log_del_source', 'Báo điện tử Dân Trí', '', 1, 1421252132),
(17, 'vi', 'menu', 'Delete menu item', 'Item ID 2', '', 1, 1421252254),
(18, 'vi', 'menu', 'Delete menu item', 'Item ID 3', '', 1, 1421252254),
(19, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421252281),
(20, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421252290),
(21, 'vi', 'modules', 'Sửa module &ldquo;news&rdquo;', '', '', 1, 1421252529),
(22, 'vi', 'news', 'Thêm chuyên mục', 'Ứng dụng tin học trong thiết kế', '', 1, 1421252661),
(23, 'vi', 'news', 'Thêm chuyên mục', 'Phong thủy', '', 1, 1421252790),
(24, 'vi', 'news', 'Thêm chuyên mục', 'Kiến trúc đẹp', '', 1, 1421252887),
(25, 'vi', 'news', 'Thêm chuyên mục', 'Nền móng', '', 1, 1421252995),
(26, 'vi', 'news', 'Thêm chuyên mục', 'Kết cấu', '', 1, 1421253270),
(27, 'vi', 'news', 'Thêm chuyên mục', 'Vật liệu xây dựng', '', 1, 1421253387),
(28, 'vi', 'news', 'log_add_source', ' ', '', 1, 1421253493),
(29, 'vi', 'news', 'log_add_source', ' ', '', 1, 1421253515),
(30, 'vi', 'modules', 'Thiết lập module mới download', '', '', 1, 1421253563),
(31, 'vi', 'modules', 'Sửa module &ldquo;download&rdquo;', '', '', 1, 1421253632),
(32, 'vi', 'download', 'Thêm chủ đề', 'Tiêu chuẩn xây dựng', '', 1, 1421253664),
(33, 'vi', 'download', 'Thêm chủ đề', 'Phần mềm xây dựng', '', 1, 1421253706),
(34, 'vi', 'download', 'Thêm chủ đề', 'Thư viện kết cấu', '', 1, 1421253739),
(35, 'vi', 'modules', 'Cài đặt gói Module + Block', 'block-html-css-javascript.zip', '', 1, 1421253788),
(36, 'vi', 'modules', 'Cài đặt gói Module + Block', 'module-video-clips-v4-0-01.zip', '', 1, 1421253808),
(37, 'vi', 'modules', 'Thiết lập module mới video-clip', '', '', 1, 1421253816),
(38, 'vi', 'modules', 'Sửa module &ldquo;video-clip&rdquo;', '', '', 1, 1421253858),
(39, 'vi', 'upload', 'Upload file', 'uploads/news/2015_01/dong-dat-vn.jpg', '', 1, 1421253990),
(40, 'vi', 'news', 'Thêm bài viết', 'Tính toán tải trọng động đất theo TCXDVN 375&#x3A;2006', '', 1, 1421254180),
(41, 'vi', 'upload', 'Upload file', 'uploads/news/2015_01/chon-dat-lam-nha-1.jpg', '', 1, 1421254300),
(42, 'vi', 'news', 'Thêm bài viết', 'Thuật phong thủy và việc chọn đất làm nhà', '', 1, 1421254382),
(43, 'vi', 'themes', 'Thêm block', 'Name : module block newscenter', '', 1, 1421254475),
(44, 'vi', 'themes', 'Thêm block', 'Name : VIDEO', '', 1, 1421254539),
(45, 'vi', 'themes', 'Thêm block', 'Name : FILE MỚI', '', 1, 1421254598),
(46, 'vi', 'upload', 'Upload file', 'uploads/news/2015_01/cayxanhphongthuy.jpg', '', 1, 1421254676),
(47, 'vi', 'news', 'Thêm bài viết', 'Bố trí cây xanh quanh nhà theo phong thủy', '', 1, 1421254741),
(48, 'vi', 'upload', 'Upload file', 'uploads/news/2015_01/phong-thuy.jpg', '', 1, 1421254937),
(49, 'vi', 'news', 'Thêm bài viết', 'Trang trí tiểu cảnh hợp phong thủy tăng thêm an lành', '', 1, 1421254983),
(50, 'vi', 'upload', 'Upload file', 'uploads/news/2015_01/kiem-tra-vach.jpg', '', 1, 1421255121),
(51, 'vi', 'news', 'Thêm bài viết', 'Kiểm tra Vách phẳng theo giả thiết vùng biên chịu mô men', '', 1, 1421255177),
(52, 'vi', 'news', 'Sửa bài viết', 'Kiểm tra Vách phẳng theo giả thiết vùng biên chịu mô men', '', 1, 1421255201),
(53, 'vi', 'news', 'Thêm bài viết', 'Kỹ sư thậm chí có thể tính nhẩm khi sử dụng phương pháp này.', '', 1, 1421255405),
(54, 'vi', 'upload', 'Upload file', 'uploads/download/images/tcvn2.jpg', '', 1, 1421255491),
(55, 'vi', 'upload', 'Upload file', 'uploads/download/files/tcvn-10304-2014-tieu-chuan-thiet-ke-mong-coc-pdf.pdf', '', 1, 1421255541),
(56, 'vi', 'download', 'Thêm file mới', 'Vì chỉ kể đến một lượng hạn chế vật liệu tham', '', 1, 1421255565),
(57, 'vi', 'upload', 'Upload file', 'uploads/download/files/tcxd-254-2001-cong-trinh-be-tong-cot-thep-toan-khoi-xay-dung-bang-cop-pha-truot-tieu-chuan-thi-cong-va-nghiem-thu.rar', '', 1, 1421255681),
(58, 'vi', 'download', 'Thêm file mới', 'Với hệ số uốn dọc được xác định như sau', '', 1, 1421255697),
(59, 'vi', 'login', '[admin] Đăng nhập', ' Client IP:127.0.0.1', '', 0, 1421320676),
(60, 'vi', 'themes', 'Sửa block', 'Name : Menu Site', '', 1, 1421320858),
(61, 'vi', 'themes', 'Sửa block', 'Name : Menu Site', '', 1, 1421320882),
(62, 'vi', 'themes', 'Sửa block', 'Name : Menu Site', '', 1, 1421320893),
(63, 'vi', 'themes', 'Sửa block', 'Name : Menu Site', '', 1, 1421320903),
(64, 'vi', 'themes', 'Sửa block', 'Name : Menu Site', '', 1, 1421320916),
(65, 'vi', 'themes', 'Sửa block', 'Name : Menu Site', '', 1, 1421320934),
(66, 'vi', 'themes', 'Sửa block', 'Name : Menu Site', '', 1, 1421320948),
(67, 'vi', 'themes', 'Thêm block', 'Name : global block tophits', '', 1, 1421320969),
(68, 'vi', 'themes', 'log_del_theme', 'theme modern', '', 1, 1421321071),
(69, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421322067),
(70, 'vi', 'login', '[admin] Đăng nhập', ' Client IP:127.0.0.1', '', 0, 1421333704),
(71, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421333716),
(72, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421336972),
(73, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421379817),
(74, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421379984),
(75, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421380067),
(76, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421380098),
(77, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421380703),
(78, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421381198),
(79, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421381347),
(80, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421381432),
(81, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421381496),
(82, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421381546),
(83, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421381585),
(84, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421381594),
(85, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421381787),
(86, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421381798),
(87, 'vi', 'themes', 'Sửa block', 'Name : VIDEO', '', 1, 1421381895),
(88, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421381934),
(89, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421382256),
(90, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421382443),
(91, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421382657),
(92, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421382731),
(93, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421383217),
(94, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421383257),
(95, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421383303),
(96, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421383337),
(97, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421383862),
(98, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421383905),
(99, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421383938),
(100, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421384408),
(101, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421384477),
(102, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421384815),
(103, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache', '', 1, 1421384832),
(104, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache', '', 1, 1421384989),
(105, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421385171),
(106, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421385212),
(107, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421385332),
(108, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421385502),
(109, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421385783),
(110, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421386101),
(111, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421386162),
(112, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache', '', 1, 1421386267),
(113, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421386342),
(114, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421386788),
(115, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421386893),
(116, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421387227),
(117, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421387361),
(118, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421387892),
(119, 'vi', 'login', '[admin] Đăng nhập', ' Client IP:127.0.0.1', '', 0, 1421415531),
(120, 'vi', 'upload', 'Upload file', 'images/ket-cau-xay-dung.png', '', 1, 1421415702),
(121, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421415724),
(122, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421422873),
(123, 'vi', 'upload', 'Upload file', 'images/ket-cau-xay-dung_1.png', '', 1, 1421423465),
(124, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421423602),
(125, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421425898),
(126, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421425930),
(127, 'vi', 'themes', 'Thêm block', 'Name : global social', '', 1, 1421426095),
(128, 'vi', 'themes', 'Sửa block', 'Name : global social', '', 1, 1421426105),
(129, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421426194),
(130, 'vi', 'upload', 'Upload file', 'images/ket-cau-xay-dung_2.png', '', 1, 1421426703),
(131, 'vi', 'login', '[admin] Đăng nhập', ' Client IP:127.0.0.1', '', 0, 1421469486),
(132, 'vi', 'themes', 'Thêm block', 'Name : Phần mềm xây dựng', '', 1, 1421469949),
(133, 'vi', 'themes', 'Sửa block', 'Name : Phần mềm xây dựng', '', 1, 1421469980),
(134, 'vi', 'themes', 'Thêm block', 'Name : a', '', 1, 1421470050),
(135, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421471935),
(136, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421472922),
(137, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421473014),
(138, 'vi', 'login', '[admin] Đăng nhập', ' Client IP:127.0.0.1', '', 0, 1421480540),
(139, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421480694),
(140, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421480756),
(141, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421480895),
(142, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421481129),
(143, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421481333),
(144, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421493887),
(145, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421494122),
(146, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421494165),
(147, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421494218),
(148, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421494431),
(149, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421494501),
(150, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421494594),
(151, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421499957),
(152, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421500096),
(153, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421500156),
(154, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421500295),
(155, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421500433),
(156, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421500590),
(157, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421500895),
(158, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421501406),
(159, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421501468),
(160, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421501566),
(161, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421501726),
(162, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421502108),
(163, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421502141),
(164, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421502305),
(165, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421502373),
(166, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421502453),
(167, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421502499),
(168, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421502528),
(169, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421503133),
(170, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421503280),
(171, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421503347),
(172, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421503382),
(173, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421503405),
(174, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421506448),
(175, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421506464),
(176, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421506577),
(177, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421506648),
(178, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421506689),
(179, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421506770),
(180, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421507722),
(181, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421507731),
(182, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421507893),
(183, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421508653),
(184, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421508821),
(185, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421509255),
(186, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421509616),
(187, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421510018),
(188, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421510207),
(189, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421510406),
(190, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421510521),
(191, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421510566),
(192, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421511186),
(193, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421511238),
(194, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421511456),
(195, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421511658),
(196, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421511696),
(197, 'vi', 'news', 'Thêm bài viết', 'Phần mềm xây dựng', '', 1, 1421511824),
(198, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421511918),
(199, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421512045),
(200, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421512593),
(201, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421512685),
(202, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421513271),
(203, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421513295),
(204, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421513357),
(205, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421513405),
(206, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421513484),
(207, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421513620),
(208, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421514061),
(209, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421514442),
(210, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421514586),
(211, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421514961),
(212, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421515045),
(213, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421515116),
(214, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421515387),
(215, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421515419),
(216, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421515517),
(217, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421516066),
(218, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421516115),
(219, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421516230),
(220, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421516829),
(221, 'vi', 'login', '[admin] Đăng nhập', ' Client IP:127.0.0.1', '', 0, 1421532578),
(222, 'vi', 'themes', 'Thêm block', 'Name : a', '', 1, 1421532633),
(223, 'vi', 'news', 'Sửa bài viết', 'Phần mềm xây dựng', '', 1, 1421532680),
(224, 'vi', 'news', 'Sửa bài viết', 'Kỹ sư thậm chí có thể tính nhẩm khi sử dụng phương pháp này.', '', 1, 1421532694),
(225, 'vi', 'login', '[admin] Đăng nhập', ' Client IP:127.0.0.1', '', 0, 1421554565),
(226, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421554575),
(227, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421554619),
(228, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421555166),
(229, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421555484),
(230, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421555562),
(231, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421555770),
(232, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421555814),
(233, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421555917),
(234, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421556340),
(235, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421557292),
(236, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421557464),
(237, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421557509),
(238, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421557670),
(239, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421557770),
(240, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421558124),
(241, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421558176),
(242, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421558242),
(243, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421558684),
(244, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421558868),
(245, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421559354),
(246, 'vi', 'themes', 'Sửa block', 'Name : VIDEO', '', 1, 1421559422),
(247, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421559790),
(248, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421560177),
(249, 'vi', 'news', 'Sửa bài viết', 'Trang trí tiểu cảnh hợp phong thủy tăng thêm an lành', '', 1, 1421560969),
(250, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421561101),
(251, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421562359),
(252, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421562431),
(253, 'vi', 'login', '[admin] Đăng nhập', ' Client IP:127.0.0.1', '', 0, 1421565042),
(254, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421565333),
(255, 'vi', 'login', '[admin] Đăng nhập', ' Client IP:127.0.0.1', '', 0, 1421572362),
(256, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421572371),
(257, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421572635),
(258, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421572761),
(259, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421573554),
(260, 'vi', 'news', 'Sửa bài viết', 'Phần mềm xây dựng', '', 1, 1421573822),
(261, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421575653),
(262, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421576154),
(263, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421576204),
(264, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421576447),
(265, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421576487),
(266, 'vi', 'themes', 'Thiết lập layout theme: "default"', '', '', 1, 1421577458),
(267, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421577483),
(268, 'vi', 'themes', 'Thiết lập layout theme: "default"', '', '', 1, 1421577642),
(269, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421578404),
(270, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421578616),
(271, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421578761),
(272, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421590111),
(273, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421590906),
(274, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421591367),
(275, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421591556),
(276, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421591597),
(277, 'vi', 'themes', 'Thiết lập layout theme: "default"', '', '', 1, 1421592340),
(278, 'vi', 'themes', 'Thiết lập layout theme: "default"', '', '', 1, 1421592382),
(279, 'vi', 'themes', 'Thiết lập layout theme: "default"', '', '', 1, 1421592635),
(280, 'vi', 'login', '[admin] Đăng nhập', ' Client IP:127.0.0.1', '', 0, 1421644224),
(281, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421644248),
(282, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421644315),
(283, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421644479),
(284, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421644582),
(285, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421644616),
(286, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421644706),
(287, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421644978),
(288, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421644986),
(289, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421645083),
(290, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421645149),
(291, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421645528),
(292, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421645691),
(293, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421645953),
(294, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421646277),
(295, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421646325),
(296, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421646387),
(297, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421646579),
(298, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421646678),
(299, 'vi', 'themes', 'Sửa block', 'Name : VIDEO', '', 1, 1421647132),
(300, 'vi', 'themes', 'Sửa block', 'Name : VIDEO', '', 1, 1421647183),
(301, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421647479),
(302, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421647662),
(303, 'vi', 'login', '[admin] Đăng nhập', ' Client IP:127.0.0.1', '', 0, 1421651094),
(304, 'vi', 'news', 'Sửa bài viết', 'Thuật phong thủy và việc chọn đất làm nhà', '', 1, 1421651132),
(305, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421655763),
(306, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421658450),
(307, 'vi', 'login', '[admin] Đăng nhập', ' Client IP:127.0.0.1', '', 0, 1421934809),
(308, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421934820),
(309, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421934909),
(310, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421935016),
(311, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421935777),
(312, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421935825),
(313, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421935942),
(314, 'vi', 'themes', 'Thêm block', 'Name : module block newscenter', '', 1, 1421936153),
(315, 'vi', 'themes', 'Thêm block', 'Name : module block newscenter', '', 1, 1421940832),
(316, 'vi', 'themes', 'Sửa block', 'Name : module block newscenter', '', 1, 1421940866),
(317, 'vi', 'themes', 'Thêm block', 'Name : module block newscenter', '', 1, 1421940888),
(318, 'vi', 'themes', 'Sửa block', 'Name : module block newscenter', '', 1, 1421940901),
(319, 'vi', 'themes', 'Thêm block', 'Name : module block newscenter', '', 1, 1421940934),
(320, 'vi', 'themes', 'Sửa block', 'Name : module block newscenter', '', 1, 1421941377),
(321, 'vi', 'themes', 'Sửa block', 'Name : module block newscenter', '', 1, 1421942779),
(322, 'vi', 'themes', 'Sửa block', 'Name : Kết cấu', '', 1, 1421942838),
(323, 'vi', 'themes', 'Sửa block', 'Name : Kết cấu', '', 1, 1421942869),
(324, 'vi', 'video-clip', 'Thêm thể loại mới', 'ID 1', '', 1, 1421942893),
(325, 'vi', 'video-clip', 'Thêm thể loại mới', 'ID 2', '', 1, 1421942900),
(326, 'vi', 'upload', 'Upload file', 'uploads/video-clip/images/sua-loi-text.jpg', '', 1, 1421942969),
(327, 'vi', 'video-clip', 'Thêm video-clip', 'Id: 1', '', 1, 1421942974),
(328, 'vi', 'themes', 'Sửa block', 'Name : Kết cấu', '', 1, 1421942997),
(329, 'vi', 'upload', 'Upload file', 'uploads/video-clip/images/happy-new-year-abba.jpg', '', 1, 1421943203),
(330, 'vi', 'video-clip', 'Thêm video-clip', 'Id: 2', '', 1, 1421943215),
(331, 'vi', 'video-clip', 'Thêm video-clip', 'Id: 3', '', 1, 1421943263),
(332, 'vi', 'themes', 'Sửa block', 'Name : module block newscenter', '', 1, 1421943305),
(333, 'vi', 'themes', 'Thêm block', 'Name : module block headline', '', 1, 1421943406),
(334, 'vi', 'themes', 'Sửa block', 'Name : module block headline', '', 1, 1421943563),
(335, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421944530),
(336, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421945040),
(337, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421945101),
(338, 'vi', 'themes', 'Thêm block', 'Name : module detail', '', 1, 1421945290),
(339, 'vi', 'themes', 'Sửa block', 'Name : FILE MỚI', '', 1, 1421945371),
(340, 'vi', 'themes', 'Sửa block', 'Name : FILE MỚI', '', 1, 1421945406),
(341, 'vi', 'themes', 'Sửa block', 'Name : module block newscenter', '', 1, 1421945432),
(342, 'vi', 'themes', 'Sửa block', 'Name : Tin tiêu điểm', '', 1, 1421945452),
(343, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421945924),
(344, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421946652),
(345, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421946735),
(346, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421946856),
(347, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421946991),
(348, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421947429),
(349, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421947484),
(350, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421947596),
(351, 'vi', 'themes', 'Sửa block', 'Name : module block newscenter', '', 1, 1421947637),
(352, 'vi', 'upload', 'Upload file', 'uploads/video-clip/images/body-top.jpg', '', 1, 1421947693),
(353, 'vi', 'video-clip', 'Thêm video-clip', 'Id: 4', '', 1, 1421947704),
(354, 'vi', 'upload', 'Upload file', 'uploads/video-clip/images/sapo_new.jpg', '', 1, 1421947779),
(355, 'vi', 'video-clip', 'Thêm video-clip', 'Id: 5', '', 1, 1421947800),
(356, 'vi', 'themes', 'Sửa block', 'Name : module block newscenter', '', 1, 1421947914),
(357, 'vi', 'themes', 'Sửa block', 'Name : module block newscenter', '', 1, 1421948128),
(358, 'vi', 'themes', 'Sửa block', 'Name : Video clip mới', '', 1, 1421948191),
(359, 'vi', 'themes', 'Sửa block', 'Name : Download mới', '', 1, 1421948253),
(360, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421948557),
(361, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421948869),
(362, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421948995),
(363, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421949127),
(364, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421949531),
(365, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421949555),
(366, 'vi', 'themes', 'Sửa block', 'Name : FILE MỚI', '', 1, 1421949686),
(367, 'vi', 'themes', 'Sửa block', 'Name : FILE MỚI', '', 1, 1421949737),
(368, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421950223),
(369, 'vi', 'themes', 'Sửa block', 'Name : FILE MỚI', '', 1, 1421950268),
(370, 'vi', 'themes', 'Sửa block', 'Name : FILE MỚI', '', 1, 1421950286),
(371, 'vi', 'themes', 'Sửa block', 'Name : FILE MỚI', '', 1, 1421950297),
(372, 'vi', 'themes', 'Sửa block', 'Name : Download mới', '', 1, 1421950525),
(373, 'vi', 'themes', 'Sửa block', 'Name : Download mới', '', 1, 1421950562),
(374, 'vi', 'themes', 'Sửa block', 'Name : Download mới', '', 1, 1421950585),
(375, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421951120),
(376, 'vi', 'themes', 'Thêm block', 'Name : global new files', '', 1, 1421951273),
(377, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421951538),
(378, 'vi', 'themes', 'Sửa block', 'Name : Download mới', '', 1, 1421951585),
(379, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421951597),
(380, 'vi', 'themes', 'Sửa block', 'Name : global new files', '', 1, 1421951716),
(381, 'vi', 'themes', 'Sửa block', 'Name : Phần mềm xây dựng', '', 1, 1421951741),
(382, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421951851),
(383, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421952011),
(384, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421952072),
(385, 'vi', 'themes', 'Sửa block', 'Name : Download mới', '', 1, 1421952283),
(386, 'vi', 'themes', 'Sửa block', 'Name : Video clip mới', '', 1, 1421952297),
(387, 'vi', 'themes', 'Sửa block', 'Name : Download mới', '', 1, 1421952364),
(388, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421952553),
(389, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421952671),
(390, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421952805),
(391, 'vi', 'themes', 'Sửa block', 'Name : Video clip mới', '', 1, 1421952839),
(392, 'vi', 'themes', 'Sửa block', 'Name : Video clip mới', '', 1, 1421952864),
(393, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421952949),
(394, 'vi', 'themes', 'Sửa block', 'Name : FILE MỚI', '', 1, 1421953003),
(395, 'vi', 'themes', 'Sửa block', 'Name : Tin được quan tâm', '', 1, 1421953699),
(396, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1421980695),
(397, 'vi', 'login', '[admin] Đăng nhập', ' Client IP:127.0.0.1', '', 0, 1422293859),
(398, 'vi', 'upload', 'Upload file', 'uploads/page/tap-chi-so-4.jpg', '', 1, 1422293898),
(399, 'vi', 'page', 'Add', ' ', '', 1, 1422293937),
(400, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422294442),
(401, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422294464),
(402, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422294505),
(403, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422294633),
(404, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422294664),
(405, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422295259),
(406, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422295345),
(407, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422295571);
INSERT INTO `kcxd_logs` (`id`, `lang`, `module_name`, `name_key`, `note_action`, `link_acess`, `userid`, `log_time`) VALUES
(408, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422295862),
(409, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422295986),
(410, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422296859),
(411, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422297116),
(412, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422297312),
(413, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422297432),
(414, 'vi', 'login', '[admin] Đăng nhập', ' Client IP:127.0.0.1', '', 0, 1422324033),
(415, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422324044),
(416, 'vi', 'login', '[admin] Thoát khỏi tài khoản Quản trị', ' Client IP:127.0.0.1', '', 0, 1422371962),
(417, 'vi', 'login', '[admin] Đăng nhập', ' Client IP:127.0.0.1', '', 0, 1422534757),
(418, 'vi', 'modules', 'Cài đặt gói Module + Block', 'nv4-module-vanban-4-0-02.zip', '', 1, 1422534776),
(419, 'vi', 'modules', 'Thiết lập module mới vanban', '', '', 1, 1422534781),
(420, 'vi', 'modules', 'Sửa module &ldquo;vanban&rdquo;', '', '', 1, 1422534786),
(421, 'vi', 'upload', 'Upload file', 'uploads/vanban/tcvn-9386-2012-thiet-ke-cong-trinh-chiu-dong-dat.zip', '', 1, 1422535032),
(422, 'vi', 'login', '[admin] Đăng nhập', ' Client IP:127.0.0.1', '', 0, 1422541086),
(423, 'vi', 'themes', 'Thiết lập layout theme: "default"', '', '', 1, 1422541182),
(424, 'vi', 'themes', 'Thiết lập layout theme: "default"', '', '', 1, 1422541257),
(425, 'vi', 'themes', 'Thiết lập layout theme: "default"', '', '', 1, 1422541639),
(426, 'vi', 'themes', 'Thêm block', 'Name : module block search', '', 1, 1422541698),
(427, 'vi', 'themes', 'Thiết lập layout theme: "default"', '', '', 1, 1422541840),
(428, 'vi', 'themes', 'Thêm block', 'Name : global block tophits', '', 1, 1422542374),
(429, 'vi', 'themes', 'Sửa block', 'Name : global block tophits', '', 1, 1422542408),
(430, 'vi', 'themes', 'Sửa block', 'Name : global block tophits', '', 1, 1422542432),
(431, 'vi', 'upload', 'Upload file', 'uploads/vanban/cp_1469_qd_ttg_220814.pdf', '', 1, 1422542536),
(432, 'vi', 'login', '[admin] Đăng nhập', ' Client IP:127.0.0.1', '', 0, 1422752526),
(433, 'vi', 'upload', 'Upload file', 'uploads/news/2015_02/tran-ngoc-dong-1-2014.pdf', '', 1, 1422752601),
(434, 'vi', 'upload', 'Upload file', 'uploads/news/2015_02/tran-ngoc-dong-1-2014_1.pdf', '', 1, 1422752620),
(435, 'vi', 'upload', 'Upload file', 'uploads/news/2015_02/message.png', '', 1, 1422752635),
(436, 'vi', 'news', 'Thêm bài viết', 'Tính toán cấu kiện chịu xoắn theo ACI 318M-08', '', 1, 1422753399),
(437, 'vi', 'news', 'Sửa bài viết', 'Tính toán cấu kiện chịu xoắn theo ACI 318M-08', '', 1, 1422753638),
(438, 'vi', 'themes', 'Thêm block', 'Name : global html', '', 1, 1422761730),
(439, 'vi', 'themes', 'Thêm block', 'Name : global html css javascript', '', 1, 1422761840),
(440, 'vi', 'themes', 'Thêm block', 'Name : global html css javascript', '', 1, 1422762475),
(441, 'vi', 'themes', 'Sửa block', 'Name : global html css javascript', '', 1, 1422762566),
(442, 'vi', 'themes', 'Sửa block', 'Name : global html css javascript', '', 1, 1422762654),
(443, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422763021),
(444, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422763086),
(445, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422763217),
(446, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422763296),
(447, 'vi', 'modules', 'Cài đặt gói Module + Block', 'global-search-google.zip', '', 1, 1422763919),
(448, 'vi', 'modules', 'Sửa module &ldquo;seek&rdquo;', '', '', 1, 1422764052),
(449, 'vi', 'modules', 'Sửa module &ldquo;seek&rdquo;', '', '', 1, 1422764065),
(450, 'vi', 'themes', 'Thêm block', 'Name : global menu footer', '', 1, 1422764196),
(451, 'vi', 'modules', 'Cài đặt gói Module + Block', 'global-search-google.zip', '', 1, 1422764503),
(452, 'vi', 'themes', 'Thêm block', 'Name : global rss', '', 1, 1422764907),
(453, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422770262),
(454, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422770348),
(455, 'vi', 'login', '[admin] Đăng nhập', ' Client IP:127.0.0.1', '', 0, 1422956986),
(456, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422956994),
(457, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422957215),
(458, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422957373),
(459, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422957469),
(460, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422957937),
(461, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1422957977),
(462, 'vi', 'login', '[admin] Đăng nhập', ' Client IP:127.0.0.1', '', 0, 1423130096),
(463, 'vi', 'webtools', 'Dọn dẹp hệ thống', 'clearcache, clearsession, clearfiletemp, clearerrorlogs, clearip_logs', '', 1, 1423131028);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_plugin`
--

CREATE TABLE IF NOT EXISTS `kcxd_plugin` (
  `pid` tinyint(4) NOT NULL AUTO_INCREMENT,
  `plugin_file` varchar(50) NOT NULL,
  `plugin_area` tinyint(4) NOT NULL,
  `weight` tinyint(4) NOT NULL,
  PRIMARY KEY (`pid`),
  UNIQUE KEY `plugin_file` (`plugin_file`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_sessions`
--

CREATE TABLE IF NOT EXISTS `kcxd_sessions` (
  `session_id` varchar(50) DEFAULT NULL,
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `full_name` varchar(100) NOT NULL,
  `onl_time` int(11) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `session_id` (`session_id`),
  KEY `onl_time` (`onl_time`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kcxd_sessions`
--

INSERT INTO `kcxd_sessions` (`session_id`, `userid`, `full_name`, `onl_time`) VALUES
('38hn6ggj73vkj1n27hg3ui1082', 1, 'admin', 1423131143);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_setup`
--

CREATE TABLE IF NOT EXISTS `kcxd_setup` (
  `lang` char(2) NOT NULL,
  `module` varchar(50) NOT NULL,
  `tables` varchar(255) NOT NULL,
  `version` varchar(100) NOT NULL,
  `setup_time` int(11) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `lang` (`lang`,`module`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_setup_language`
--

CREATE TABLE IF NOT EXISTS `kcxd_setup_language` (
  `lang` char(2) NOT NULL,
  `setup` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lang`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kcxd_setup_language`
--

INSERT INTO `kcxd_setup_language` (`lang`, `setup`) VALUES
('vi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_setup_modules`
--

CREATE TABLE IF NOT EXISTS `kcxd_setup_modules` (
  `title` varchar(55) NOT NULL,
  `is_sysmod` tinyint(1) NOT NULL DEFAULT '0',
  `virtual` tinyint(1) NOT NULL DEFAULT '0',
  `module_file` varchar(50) NOT NULL DEFAULT '',
  `module_data` varchar(55) NOT NULL DEFAULT '',
  `mod_version` varchar(50) NOT NULL,
  `addtime` int(11) NOT NULL DEFAULT '0',
  `author` text NOT NULL,
  `note` varchar(255) DEFAULT '',
  PRIMARY KEY (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kcxd_setup_modules`
--

INSERT INTO `kcxd_setup_modules` (`title`, `is_sysmod`, `virtual`, `module_file`, `module_data`, `mod_version`, `addtime`, `author`, `note`) VALUES
('about', 0, 0, 'page', 'about', '4.0.00 1393416212', 1421251986, 'VINADES (contact@vinades.vn)', ''),
('banners', 1, 0, 'banners', 'banners', '4.0.00 1393416212', 1421251986, 'VINADES (contact@vinades.vn)', ''),
('contact', 0, 1, 'contact', 'contact', '4.0.00 1393416212', 1421251986, 'VINADES (contact@vinades.vn)', ''),
('news', 0, 1, 'news', 'news', '4.0.00 1393416212', 1421251986, 'VINADES (contact@vinades.vn)', ''),
('voting', 0, 0, 'voting', 'voting', '4.0.00 1393416212', 1421251986, 'VINADES (contact@vinades.vn)', ''),
('video-clip', 0, 1, 'video-clip', 'video_clip', '4.0.01 1417392000', 1421253812, 'PHAN TAN DUNG (phantandung92@gmail.com)', 'Module playback of video-clips'),
('seek', 1, 0, 'seek', 'seek', '4.0.00 1393416212', 1421251986, 'VINADES (contact@vinades.vn)', ''),
('users', 1, 0, 'users', 'users', '4.0.00 1393416212', 1421251986, 'VINADES (contact@vinades.vn)', ''),
('download', 0, 1, 'download', 'download', '4.0.00 1393416212', 1421251986, 'VINADES (contact@vinades.vn)', ''),
('statistics', 0, 0, 'statistics', 'statistics', '4.0.00 1393416212', 1421251986, 'VINADES (contact@vinades.vn)', ''),
('menu', 0, 1, 'menu', 'menu', '4.0.00 1393416212', 1421251986, 'VINADES (contact@vinades.vn)', ''),
('feeds', 1, 0, 'feeds', 'feeds', '4.0.00 1393416212', 1421251986, 'VINADES (contact@vinades.vn)', ''),
('page', 1, 1, 'page', 'page', '4.0.00 1393416212', 1421251986, 'VINADES (contact@vinades.vn)', ''),
('comment', 1, 0, 'comment', 'comment', '4.0.00 1393416212', 1421251986, 'VINADES (contact@vinades.vn)', ''),
('vanban', 0, 1, 'vanban', 'vanban', '4.0.01 1321926759', 1422534778, 'NhanhGon.vn (vuqueds@gmail.com)', '');

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_upload_dir`
--

CREATE TABLE IF NOT EXISTS `kcxd_upload_dir` (
  `did` mediumint(8) NOT NULL AUTO_INCREMENT,
  `dirname` varchar(255) DEFAULT NULL,
  `time` int(11) NOT NULL DEFAULT '0',
  `thumb_type` tinyint(4) NOT NULL DEFAULT '0',
  `thumb_width` smallint(6) NOT NULL DEFAULT '0',
  `thumb_height` smallint(6) NOT NULL DEFAULT '0',
  `thumb_quality` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`did`),
  UNIQUE KEY `name` (`dirname`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `kcxd_upload_dir`
--

INSERT INTO `kcxd_upload_dir` (`did`, `dirname`, `time`, `thumb_type`, `thumb_width`, `thumb_height`, `thumb_quality`) VALUES
(0, '', 0, 3, 600, 400, 99),
(1, 'uploads/menu', 0, 0, 0, 0, 0),
(2, 'images', 1421415688, 0, 0, 0, 0),
(3, 'images/rank', 0, 0, 0, 0, 0),
(4, 'uploads', 0, 0, 0, 0, 0),
(5, 'uploads/about', 0, 0, 0, 0, 0),
(6, 'uploads/banners', 0, 0, 0, 0, 0),
(7, 'uploads/contact', 0, 0, 0, 0, 0),
(8, 'uploads/news', 0, 0, 0, 0, 0),
(9, 'uploads/news/source', 0, 0, 0, 0, 0),
(10, 'uploads/news/temp_pic', 0, 0, 0, 0, 0),
(11, 'uploads/news/topics', 0, 0, 0, 0, 0),
(12, 'uploads/page', 1422293886, 0, 0, 0, 0),
(13, 'uploads/users', 0, 0, 0, 0, 0),
(14, 'uploads/news/2015_01', 1421253980, 0, 0, 0, 0),
(15, 'uploads/download', 0, 0, 0, 0, 0),
(16, 'uploads/download/files', 1421255527, 0, 0, 0, 0),
(17, 'uploads/download/images', 1421255470, 0, 0, 0, 0),
(18, 'uploads/download/temp', 0, 0, 0, 0, 0),
(19, 'uploads/video-clip', 0, 0, 0, 0, 0),
(20, 'uploads/video-clip/icons', 0, 0, 0, 0, 0),
(21, 'uploads/video-clip/images', 1421942948, 0, 0, 0, 0),
(22, 'uploads/video-clip/video', 0, 0, 0, 0, 0),
(23, 'uploads/vanban', 1422535001, 0, 0, 0, 0),
(24, 'uploads/news/2015_02', 1422753350, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_upload_file`
--

CREATE TABLE IF NOT EXISTS `kcxd_upload_file` (
  `name` varchar(255) NOT NULL,
  `ext` varchar(10) NOT NULL DEFAULT '',
  `type` varchar(5) NOT NULL DEFAULT '',
  `filesize` int(11) NOT NULL DEFAULT '0',
  `src` varchar(255) NOT NULL DEFAULT '',
  `srcwidth` int(11) NOT NULL DEFAULT '0',
  `srcheight` int(11) NOT NULL DEFAULT '0',
  `sizes` varchar(50) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `mtime` int(11) NOT NULL DEFAULT '0',
  `did` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `alt` varchar(255) NOT NULL DEFAULT '',
  UNIQUE KEY `did` (`did`,`title`),
  KEY `userid` (`userid`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kcxd_upload_file`
--

INSERT INTO `kcxd_upload_file` (`name`, `ext`, `type`, `filesize`, `src`, `srcwidth`, `srcheight`, `sizes`, `userid`, `mtime`, `did`, `title`, `alt`) VALUES
('dong-dat-vn.jpg', 'jpg', 'image', 105162, 'files/news/2015_01/dong-dat-vn.jpg', 80, 53, '600|400', 1, 1421253990, 14, 'dong-dat-vn.jpg', 'kc'),
('chon-dat-l...jpg', 'jpg', 'image', 76572, 'files/news/2015_01/chon-dat-lam-nha-1.jpg', 80, 53, '600|400', 1, 1421254300, 14, 'chon-dat-lam-nha-1.jpg', 'phong thuy'),
('cayxanhpho...jpg', 'jpg', 'image', 103557, 'files/news/2015_01/cayxanhphongthuy.jpg', 80, 53, '600|400', 1, 1421254676, 14, 'cayxanhphongthuy.jpg', 'aaaa'),
('phong-thuy.jpg', 'jpg', 'image', 117658, 'files/news/2015_01/phong-thuy.jpg', 80, 53, '600|400', 1, 1421254938, 14, 'phong-thuy.jpg', 'jjjjjj'),
('kiem-tra-vach.jpg', 'jpg', 'image', 91596, 'files/news/2015_01/kiem-tra-vach.jpg', 80, 53, '600|400', 1, 1421255121, 14, 'kiem-tra-vach.jpg', 'Chú thích cho hình'),
('tcvn2.jpg', 'jpg', 'image', 61963, 'files/download/images/tcvn2.jpg', 80, 38, '619|291', 1, 1421255491, 17, 'tcvn2.jpg', 'tcvn2'),
('tcvn-10304...pdf', 'pdf', 'file', 941828, 'images/doc.gif', 32, 32, '|', 1, 1421255541, 16, 'tcvn-10304-2014-tieu-chuan-thiet-ke-mong-coc-pdf.pdf', 'TCVN 10304 2014 Tieu chuan thiet ke mong coc pdf'),
('tcxd-254-2...rar', 'rar', 'file', 808610, 'images/zip.gif', 32, 32, '|', 1, 1421255681, 16, 'tcxd-254-2001-cong-trinh-be-tong-cot-thep-toan-khoi-xay-dung-bang-cop-pha-truot-tieu-chuan-thi-cong-va-nghiem-thu.rar', 'TCXD 254 2001 Cong trinh be tong cot thep toan khoi xay dung bang cop pha truot tieu chuan thi cong va nghiem thu'),
('add.png', 'png', 'image', 297, 'images/add.png', 12, 12, '12|12', 1, 1416743194, 2, 'add.png', 'add'),
('banner88x31.png', 'png', 'image', 3960, 'images/banner88x31.png', 80, 28, '88|31', 1, 1416743194, 2, 'banner88x31.png', 'banner88x31'),
('banner_nuk...jpg', 'jpg', 'image', 12577, 'images/banner_nukeviet_88x15.jpg', 80, 14, '88|15', 1, 1416743194, 2, 'banner_nukeviet_88x15.jpg', 'banner nukeviet 88x15'),
('calendar.gif', 'gif', 'image', 269, 'images/calendar.gif', 16, 15, '16|15', 1, 1416743194, 2, 'calendar.gif', 'calendar'),
('calendar.jpg', 'jpg', 'image', 1566, 'images/calendar.jpg', 18, 17, '18|17', 1, 1416743194, 2, 'calendar.jpg', 'calendar'),
('delete.png', 'png', 'image', 285, 'images/delete.png', 12, 12, '12|12', 1, 1416743194, 2, 'delete.png', 'delete'),
('doc.gif', 'gif', 'image', 260, 'images/doc.gif', 32, 32, '32|32', 1, 1416743194, 2, 'doc.gif', 'doc'),
('edit.png', 'png', 'image', 390, 'images/edit.png', 12, 12, '12|12', 1, 1416743194, 2, 'edit.png', 'edit'),
('error.png', 'png', 'image', 1467, 'images/error.png', 12, 12, '12|12', 1, 1416743194, 2, 'error.png', 'error'),
('file.gif', 'gif', 'image', 14029, 'images/file.gif', 32, 32, '32|32', 1, 1416743194, 2, 'file.gif', 'file'),
('flash.gif', 'gif', 'image', 2510, 'images/flash.gif', 32, 32, '32|32', 1, 1416743194, 2, 'flash.gif', 'flash'),
('grey.gif', 'gif', 'image', 43, 'images/grey.gif', 1, 1, '1|1', 1, 1416743194, 2, 'grey.gif', 'grey'),
('ico_gif.gif', 'gif', 'image', 336, 'images/ico_gif.gif', 16, 16, '16|16', 1, 1416743194, 2, 'ico_gif.gif', 'ico gif'),
('ico_jpg.gif', 'gif', 'image', 336, 'images/ico_jpg.gif', 16, 16, '16|16', 1, 1416743194, 2, 'ico_jpg.gif', 'ico jpg'),
('ico_png.gif', 'gif', 'image', 337, 'images/ico_png.gif', 16, 16, '16|16', 1, 1416743194, 2, 'ico_png.gif', 'ico png'),
('ico_swf.gif', 'gif', 'image', 625, 'images/ico_swf.gif', 16, 16, '16|16', 1, 1416743194, 2, 'ico_swf.gif', 'ico swf'),
('load.gif', 'gif', 'image', 1077, 'images/load.gif', 16, 16, '16|16', 1, 1416743194, 2, 'load.gif', 'load'),
('load_bar.gif', 'gif', 'image', 308, 'images/load_bar.gif', 33, 8, '33|8', 1, 1416743194, 2, 'load_bar.gif', 'load bar'),
('logo.png', 'png', 'image', 10242, 'images/logo.png', 80, 36, '203|91', 1, 1416743194, 2, 'logo.png', 'logo'),
('mail_new.gif', 'gif', 'image', 1179, 'images/mail_new.gif', 12, 9, '12|9', 1, 1416743194, 2, 'mail_new.gif', 'mail new'),
('mail_old.gif', 'gif', 'image', 1249, 'images/mail_old.gif', 12, 11, '12|11', 1, 1416743194, 2, 'mail_old.gif', 'mail old'),
('mail_reply.gif', 'gif', 'image', 1276, 'images/mail_reply.gif', 13, 14, '13|14', 1, 1416743194, 2, 'mail_reply.gif', 'mail reply'),
('ok.png', 'png', 'image', 1389, 'images/ok.png', 12, 12, '12|12', 1, 1416743194, 2, 'ok.png', 'ok'),
('outgroup.png', 'png', 'image', 4397, 'images/outgroup.png', 16, 16, '16|16', 1, 1416743194, 2, 'outgroup.png', 'outgroup'),
('pix.gif', 'gif', 'image', 43, 'images/pix.gif', 1, 1, '1|1', 1, 1416743194, 2, 'pix.gif', 'pix'),
('refresh.png', 'png', 'image', 947, 'images/refresh.png', 16, 16, '16|16', 1, 1416743194, 2, 'refresh.png', 'refresh'),
('rss.png', 'png', 'image', 742, 'images/rss.png', 16, 16, '16|16', 1, 1416743194, 2, 'rss.png', 'rss'),
('upload.gif', 'gif', 'image', 4393, 'images/upload.gif', 77, 78, '77|78', 1, 1416743194, 2, 'upload.gif', 'upload'),
('zip.gif', 'gif', 'image', 368, 'images/zip.gif', 32, 32, '32|32', 1, 1416743194, 2, 'zip.gif', 'zip'),
('ket-cau-xa...png', 'png', 'image', 13977, 'images/ket-cau-xay-dung.png', 80, 56, '174|122', 1, 1421415703, 2, 'ket-cau-xay-dung.png', 'ket cau xay dung'),
('ket-cau-xa...png', 'png', 'image', 38490, 'images/ket-cau-xay-dung_1.png', 80, 47, '220|130', 1, 1421423466, 2, 'ket-cau-xay-dung_1.png', 'ket cau xay dung'),
('ket-cau-xa...png', 'png', 'image', 6223, 'images/ket-cau-xay-dung_2.png', 80, 60, '170|127', 1, 1421426703, 2, 'ket-cau-xay-dung_2.png', 'ket cau xay dung'),
('sua-loi-text.jpg', 'jpg', 'image', 65306, 'files/video-clip/images/sua-loi-text.jpg', 80, 53, '600|400', 1, 1421942969, 21, 'sua-loi-text.jpg', 'sua loi text'),
('happy-new-...jpg', 'jpg', 'image', 394869, 'files/video-clip/images/happy-new-year-abba.jpg', 80, 60, '800|600', 1, 1421943204, 21, 'happy-new-year-abba.jpg', 'happy new year abba'),
('body-top.jpg', 'jpg', 'image', 383012, 'files/video-clip/images/body-top.jpg', 80, 31, '1360|530', 1, 1421947693, 21, 'body-top.jpg', 'body top'),
('sapo_new.jpg', 'jpg', 'image', 240646, 'files/video-clip/images/sapo_new.jpg', 80, 48, '1000|600', 1, 1421947779, 21, 'sapo_new.jpg', 'sapo new'),
('tap-chi-so-4.jpg', 'jpg', 'image', 65985, 'files/page/tap-chi-so-4.jpg', 76, 80, '343|358', 1, 1422293898, 12, 'tap-chi-so-4.jpg', 'Tap chi so 4'),
('tcvn-9386-...zip', 'zip', 'file', 2962030, 'images/zip.gif', 32, 32, '|', 1, 1422535032, 23, 'tcvn-9386-2012-thiet-ke-cong-trinh-chiu-dong-dat.zip', 'TCVN 9386 2012 Thiet ke cong trinh chiu Dong dat'),
('cp_1469_qd...pdf', 'pdf', 'file', 781334, 'images/doc.gif', 32, 32, '|', 1, 1422542536, 23, 'cp_1469_qd_ttg_220814.pdf', 'CP 1469 QD TTg 220814'),
('tran-ngoc-...pdf', 'pdf', 'file', 307157, 'images/doc.gif', 32, 32, '|', 1, 1422752601, 24, 'tran-ngoc-dong-1-2014.pdf', 'tran ngoc dong 1 2014'),
('tran-ngoc-...pdf', 'pdf', 'file', 307157, 'images/doc.gif', 32, 32, '|', 1, 1422752620, 24, 'tran-ngoc-dong-1-2014_1.pdf', 'tran ngoc dong 1 2014 1'),
('message.png', 'png', 'image', 134885, 'files/news/2015_02/message.png', 80, 57, '400|283', 1, 1422752635, 24, 'message.png', 'message');

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_users`
--

CREATE TABLE IF NOT EXISTS `kcxd_users` (
  `userid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL DEFAULT '',
  `md5username` char(32) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `full_name` varchar(255) NOT NULL DEFAULT '',
  `gender` char(1) DEFAULT '',
  `photo` varchar(255) DEFAULT '',
  `birthday` int(11) NOT NULL,
  `sig` text,
  `regdate` int(11) NOT NULL DEFAULT '0',
  `question` varchar(255) NOT NULL,
  `answer` varchar(255) NOT NULL DEFAULT '',
  `passlostkey` varchar(50) DEFAULT '',
  `view_mail` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `remember` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `in_groups` varchar(255) DEFAULT '',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `checknum` varchar(40) DEFAULT '',
  `last_login` int(11) unsigned NOT NULL DEFAULT '0',
  `last_ip` varchar(45) DEFAULT '',
  `last_agent` varchar(255) DEFAULT '',
  `last_openid` varchar(255) DEFAULT '',
  `idsite` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userid`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `md5username` (`md5username`),
  UNIQUE KEY `email` (`email`),
  KEY `idsite` (`idsite`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `kcxd_users`
--

INSERT INTO `kcxd_users` (`userid`, `username`, `md5username`, `password`, `email`, `full_name`, `gender`, `photo`, `birthday`, `sig`, `regdate`, `question`, `answer`, `passlostkey`, `view_mail`, `remember`, `in_groups`, `active`, `checknum`, `last_login`, `last_ip`, `last_agent`, `last_openid`, `idsite`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '96a83e249bfe1426e1346e04e384ff55c92d65dc', 'tranvanthi@gmail.com', 'admin', '', '', 0, '', 1421252033, 'Ten  ?', 'Lam', '', 0, 1, '', 1, '', 1421252033, '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_users_config`
--

CREATE TABLE IF NOT EXISTS `kcxd_users_config` (
  `config` varchar(100) NOT NULL,
  `content` text,
  `edit_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`config`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kcxd_users_config`
--

INSERT INTO `kcxd_users_config` (`config`, `content`, `edit_time`) VALUES
('access_admin', 'a:6:{s:12:"access_addus";a:3:{i:1;b:1;i:2;b:1;i:3;b:1;}s:14:"access_waiting";a:3:{i:1;b:1;i:2;b:1;i:3;b:1;}s:13:"access_editus";a:3:{i:1;b:1;i:2;b:1;i:3;b:1;}s:12:"access_delus";a:3:{i:1;b:1;i:2;b:1;i:3;b:1;}s:13:"access_passus";a:3:{i:1;b:1;i:2;b:1;i:3;b:1;}s:13:"access_groups";a:3:{i:1;b:1;i:2;b:1;i:3;b:1;}}', 1352873462),
('password_simple', '000000|1234|2000|12345|111111|123123|123456|654321|696969|1234567|12345678|123456789|1234567890|aaaaaa|abc123|abc123@|abc@123|adobe1|adobe123|azerty|baseball|dragon|football|harley|iloveyou|jennifer|jordan|letmein|macromedia|master|michael|monkey|mustang|password|photoshop|pussy|qwerty|shadow|superman', 1421251986),
('deny_email', 'yoursite.com|mysite.com|localhost|xxx', 1421251986),
('deny_name', 'anonimo|anonymous|god|linux|nobody|operator|root', 1421251986),
('avatar_width', '80', 1421251986),
('avatar_height', '80', 1421251986),
('siteterms_vi', '<p> Để trở thành thành viên, bạn phải cam kết đồng ý với các điều khoản dưới đây. Chúng tôi có thể thay đổi lại những điều khoản này vào bất cứ lúc nào và chúng tôi sẽ cố gắng thông báo đến bạn kịp thời.<br /> <br /> Bạn cam kết không gửi bất cứ bài viết có nội dung lừa đảo, thô tục, thiếu văn hoá; vu khống, khiêu khích, đe doạ người khác; liên quan đến các vấn đề tình dục hay bất cứ nội dung nào vi phạm luật pháp của quốc gia mà bạn đang sống, luật pháp của quốc gia nơi đặt máy chủ của website này hay luật pháp quốc tế. Nếu vẫn cố tình vi phạm, ngay lập tức bạn sẽ bị cấm tham gia vào website. Địa chỉ IP của tất cả các bài viết đều được ghi nhận lại để bảo vệ các điều khoản cam kết này trong trường hợp bạn không tuân thủ.<br /> <br /> Bạn đồng ý rằng website có quyền gỡ bỏ, sửa, di chuyển hoặc khoá bất kỳ bài viết nào trong website vào bất cứ lúc nào tuỳ theo nhu cầu công việc.<br /> <br /> Đăng ký làm thành viên của chúng tôi, bạn cũng phải đồng ý rằng, bất kỳ thông tin cá nhân nào mà bạn cung cấp đều được lưu trữ trong cơ sở dữ liệu của hệ thống. Mặc dù những thông tin này sẽ không được cung cấp cho bất kỳ người thứ ba nào khác mà không được sự đồng ý của bạn, chúng tôi không chịu trách nhiệm về việc những thông tin cá nhân này của bạn bị lộ ra bên ngoài từ những kẻ phá hoại có ý đồ xấu tấn công vào cơ sở dữ liệu của hệ thống.</p>', 1274757129);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_users_field`
--

CREATE TABLE IF NOT EXISTS `kcxd_users_field` (
  `fid` mediumint(8) NOT NULL AUTO_INCREMENT,
  `field` varchar(25) NOT NULL,
  `weight` int(10) unsigned NOT NULL DEFAULT '1',
  `field_type` enum('number','date','textbox','textarea','editor','select','radio','checkbox','multiselect') NOT NULL DEFAULT 'textbox',
  `field_choices` text NOT NULL,
  `sql_choices` text NOT NULL,
  `match_type` enum('none','alphanumeric','email','url','regex','callback') NOT NULL DEFAULT 'none',
  `match_regex` varchar(250) NOT NULL DEFAULT '',
  `func_callback` varchar(75) NOT NULL DEFAULT '',
  `min_length` int(11) NOT NULL DEFAULT '0',
  `max_length` bigint(20) unsigned NOT NULL DEFAULT '0',
  `required` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `show_register` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `user_editable` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `show_profile` tinyint(4) NOT NULL DEFAULT '1',
  `class` varchar(50) NOT NULL,
  `language` text NOT NULL,
  `default_value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`fid`),
  UNIQUE KEY `field` (`field`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_users_info`
--

CREATE TABLE IF NOT EXISTS `kcxd_users_info` (
  `userid` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kcxd_users_info`
--

INSERT INTO `kcxd_users_info` (`userid`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_users_openid`
--

CREATE TABLE IF NOT EXISTS `kcxd_users_openid` (
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `openid` varchar(255) NOT NULL DEFAULT '',
  `opid` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`opid`),
  KEY `userid` (`userid`),
  KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_users_question`
--

CREATE TABLE IF NOT EXISTS `kcxd_users_question` (
  `qid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `lang` char(2) NOT NULL DEFAULT '',
  `weight` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `add_time` int(11) unsigned NOT NULL DEFAULT '0',
  `edit_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`qid`),
  UNIQUE KEY `title` (`title`,`lang`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `kcxd_users_question`
--

INSERT INTO `kcxd_users_question` (`qid`, `title`, `lang`, `weight`, `add_time`, `edit_time`) VALUES
(1, 'Bạn thích môn thể thao nào nhất', 'vi', 1, 1274840238, 1274840238),
(2, 'Món ăn mà bạn yêu thích', 'vi', 2, 1274840250, 1274840250),
(3, 'Thần tượng điện ảnh của bạn', 'vi', 3, 1274840257, 1274840257),
(4, 'Bạn thích nhạc sỹ nào nhất', 'vi', 4, 1274840264, 1274840264),
(5, 'Quê ngoại của bạn ở đâu', 'vi', 5, 1274840270, 1274840270),
(6, 'Tên cuốn sách &quot;gối đầu giường&quot;', 'vi', 6, 1274840278, 1274840278),
(7, 'Ngày lễ mà bạn luôn mong đợi', 'vi', 7, 1274840285, 1274840285);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_users_reg`
--

CREATE TABLE IF NOT EXISTS `kcxd_users_reg` (
  `userid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL DEFAULT '',
  `md5username` char(32) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `full_name` varchar(255) NOT NULL DEFAULT '',
  `regdate` int(11) unsigned NOT NULL DEFAULT '0',
  `question` varchar(255) NOT NULL,
  `answer` varchar(255) NOT NULL DEFAULT '',
  `checknum` varchar(50) NOT NULL DEFAULT '',
  `users_info` text,
  PRIMARY KEY (`userid`),
  UNIQUE KEY `login` (`username`),
  UNIQUE KEY `md5username` (`md5username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_about`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_about` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT '',
  `imagealt` varchar(255) DEFAULT '',
  `description` text,
  `bodytext` mediumtext NOT NULL,
  `keywords` text,
  `socialbutton` tinyint(4) NOT NULL DEFAULT '0',
  `activecomm` varchar(255) DEFAULT '',
  `layout_func` varchar(100) DEFAULT '',
  `gid` mediumint(9) NOT NULL DEFAULT '0',
  `weight` smallint(4) NOT NULL DEFAULT '0',
  `admin_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `add_time` int(11) NOT NULL DEFAULT '0',
  `edit_time` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_about_config`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_about_config` (
  `config_name` varchar(30) NOT NULL,
  `config_value` varchar(255) NOT NULL,
  UNIQUE KEY `config_name` (`config_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kcxd_vi_about_config`
--

INSERT INTO `kcxd_vi_about_config` (`config_name`, `config_value`) VALUES
('viewtype', '0'),
('facebookapi', '');

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_blocks_groups`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_blocks_groups` (
  `bid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `theme` varchar(55) NOT NULL,
  `module` varchar(55) NOT NULL,
  `file_name` varchar(55) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `template` varchar(55) DEFAULT NULL,
  `position` varchar(55) DEFAULT NULL,
  `exp_time` int(11) DEFAULT '0',
  `active` tinyint(4) DEFAULT '0',
  `groups_view` varchar(255) DEFAULT '',
  `all_func` tinyint(4) NOT NULL DEFAULT '0',
  `weight` int(11) NOT NULL DEFAULT '0',
  `config` text,
  PRIMARY KEY (`bid`),
  KEY `theme` (`theme`),
  KEY `module` (`module`),
  KEY `position` (`position`),
  KEY `exp_time` (`exp_time`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `kcxd_vi_blocks_groups`
--

INSERT INTO `kcxd_vi_blocks_groups` (`bid`, `theme`, `module`, `file_name`, `title`, `link`, `template`, `position`, `exp_time`, `active`, `groups_view`, `all_func`, `weight`, `config`) VALUES
(3, 'default', 'banners', 'global.banners.php', 'Quảng cáo trái', '', 'no_title', '[LEFT]', 0, 1, '6', 1, 3, 'a:1:{s:12:"idplanbanner";i:2;}'),
(17, 'default', 'menu', 'global.bootstrap.php', 'Menu Site', '', 'no_title', '[MENU_SITE]', 0, 1, '6', 1, 1, 'a:2:{s:6:"menuid";i:1;s:12:"title_length";i:20;}'),
(19, 'default', 'page', 'global.html.php', 'footer site', '', 'no_title', '[FOOTER_SITE]', 0, 1, '6', 1, 1, 'a:1:{s:11:"htmlcontent";s:231:"<p class="footer"> © Copyright NukeViet 4. All right reserved.</p><p> Powered by <a href="http://nukeviet.vn/" title="NukeViet CMS">NukeViet CMS</a>. Design by <a href="http://vinades.vn/" title="VINADES.,JSC">VINADES.,JSC</a></p>";}'),
(20, 'mobile_nukeviet', 'theme', 'global.menu.php', 'global menu', '', 'no_title', '[MENU_SITE]', 0, 1, '6', 1, 1, 'a:1:{s:14:"module_in_menu";a:6:{i:0;s:5:"about";i:1;s:4:"news";i:2;s:5:"users";i:3;s:7:"contact";i:4;s:10:"statistics";i:5;s:6:"voting";}}'),
(22, 'default', 'theme', 'global.menu_footer.php', 'Menu footer', '', 'no_title', '[MENU_FOOTER]', 0, 1, '6', 1, 1, 'a:1:{s:14:"module_in_menu";a:4:{i:0;s:5:"about";i:1;s:4:"news";i:2;s:5:"users";i:3;s:7:"contact";}}'),
(23, 'default', 'news', 'global.block_tophits.php', 'Tin được quan tâm', '', 'primary', '[RIGHT]', 0, 1, '6', 0, 1, 'a:5:{s:10:"number_day";i:365;s:6:"numrow";i:10;s:11:"showtooltip";i:0;s:16:"tooltip_position";s:6:"bottom";s:14:"tooltip_length";s:3:"150";}'),
(25, 'default', 'download', 'global.new_files.php', 'FILE MỚI', '', '', '[RIGHT]', 0, 1, '6', 0, 3, 'a:4:{s:12:"title_length";i:100;s:6:"numrow";i:5;s:10:"class_name";s:9:"list_item";s:10:"img_bullet";s:0:"";}'),
(28, 'default', 'download', 'module.block_lastestdownload.php', 'Phần mềm xây dựng', '', '', '[RIGHT]', 0, 1, '6', 0, 4, ''),
(33, 'default', 'news', 'module.block_newscenter.php', 'module block newscenter', '', 'no_title', '[AT1]', 0, 1, '6', 0, 1, 'a:5:{s:11:"showtooltip";i:0;s:16:"tooltip_position";s:6:"bottom";s:14:"tooltip_length";s:3:"150";s:5:"width";s:3:"500";s:6:"height";s:3:"312";}'),
(34, 'default', 'video-clip', 'global.new_image_video.php', 'Video clip mới', '', 'no_title', '[AT3]', 0, 1, '6', 0, 1, ''),
(35, 'default', 'download', 'global.new_files.php', 'Download mới', '', 'border', '[AT2]', 0, 1, '6', 0, 1, 'a:4:{s:12:"title_length";i:100;s:6:"numrow";i:10;s:10:"class_name";s:9:"list_item";s:10:"img_bullet";s:0:"";}'),
(36, 'default', 'video-clip', 'module.detail.php', 'module detail', '', 'no_title', '[TOP]', 0, 1, '6', 0, 1, ''),
(37, 'default', 'download', 'module.block_topdownload.php', 'global new files', '', '', '[RIGHT]', 0, 1, '6', 0, 6, ''),
(38, 'default', 'vanban', 'module.block_search.php', 'module block search', '', 'no_title', '[TOP]', 0, 1, '6', 0, 2, ''),
(39, 'default', 'download', 'global.new_files.php', 'global block tophits', '', '', '[RIGHT]', 0, 1, '6', 0, 7, 'a:4:{s:12:"title_length";i:100;s:6:"numrow";i:5;s:10:"class_name";s:9:"list_item";s:10:"img_bullet";s:0:"";}'),
(41, 'default', 'theme', 'global.html_css_javascript.php', 'global html css javascript', '', 'no_title', '[AT3]', 0, 1, '6', 0, 2, 'a:5:{s:5:"jsurl";s:0:"";s:5:"jscon";s:0:"";s:6:"cssurl";s:0:"";s:6:"csscon";s:0:"";s:5:"htmlc";s:0:"";}'),
(44, 'default', 'feeds', 'global.rss.php', 'global rss', '', 'no_title', '[SOCIAL_ICONS]', 0, 1, '6', 1, 1, 'a:7:{s:3:"url";s:37:"http://localhost/lam/index.php/feeds/";s:6:"number";i:50;s:13:"isdescription";i:1;s:6:"ishtml";i:1;s:9:"ispubdate";i:1;s:8:"istarget";i:1;s:12:"title_length";i:100;}');

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_blocks_weight`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_blocks_weight` (
  `bid` mediumint(8) NOT NULL DEFAULT '0',
  `func_id` mediumint(8) NOT NULL DEFAULT '0',
  `weight` mediumint(8) NOT NULL DEFAULT '0',
  UNIQUE KEY `bid` (`bid`,`func_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kcxd_vi_blocks_weight`
--

INSERT INTO `kcxd_vi_blocks_weight` (`bid`, `func_id`, `weight`) VALUES
(19, 2, 1),
(19, 36, 1),
(19, 39, 1),
(19, 42, 1),
(19, 43, 1),
(19, 54, 1),
(19, 55, 1),
(19, 56, 1),
(19, 57, 1),
(19, 27, 1),
(19, 47, 1),
(19, 11, 1),
(19, 52, 1),
(19, 5, 1),
(19, 6, 1),
(19, 7, 1),
(19, 13, 1),
(19, 15, 1),
(19, 16, 1),
(19, 51, 1),
(19, 53, 1),
(19, 46, 1),
(19, 33, 1),
(19, 32, 1),
(19, 30, 1),
(19, 29, 1),
(19, 31, 1),
(19, 28, 1),
(19, 34, 1),
(19, 24, 1),
(19, 20, 1),
(19, 21, 1),
(19, 26, 1),
(19, 23, 1),
(19, 18, 1),
(19, 25, 1),
(19, 17, 1),
(19, 22, 1),
(19, 19, 1),
(19, 48, 1),
(19, 50, 1),
(19, 58, 1),
(19, 35, 1),
(3, 2, 1),
(3, 36, 1),
(3, 39, 1),
(3, 42, 1),
(3, 43, 1),
(3, 54, 1),
(3, 55, 1),
(3, 56, 1),
(3, 57, 1),
(3, 27, 1),
(3, 47, 1),
(3, 11, 1),
(3, 52, 1),
(3, 5, 1),
(3, 6, 1),
(3, 7, 1),
(3, 13, 1),
(3, 15, 1),
(3, 16, 1),
(3, 51, 1),
(3, 53, 1),
(3, 46, 1),
(3, 33, 1),
(3, 32, 1),
(3, 30, 1),
(3, 29, 1),
(3, 31, 1),
(3, 28, 1),
(3, 34, 1),
(3, 24, 1),
(3, 20, 1),
(3, 21, 1),
(3, 26, 1),
(3, 23, 1),
(3, 18, 1),
(3, 25, 1),
(3, 17, 1),
(3, 22, 1),
(3, 19, 1),
(3, 48, 1),
(3, 50, 1),
(3, 58, 1),
(3, 35, 1),
(22, 2, 1),
(22, 36, 1),
(22, 39, 1),
(22, 42, 1),
(22, 43, 1),
(22, 54, 1),
(22, 55, 1),
(22, 56, 1),
(22, 57, 1),
(22, 27, 1),
(22, 47, 1),
(22, 11, 1),
(22, 52, 1),
(22, 5, 1),
(22, 6, 1),
(22, 7, 1),
(22, 13, 1),
(22, 15, 1),
(22, 16, 1),
(22, 51, 1),
(22, 53, 1),
(22, 46, 1),
(22, 33, 1),
(22, 32, 1),
(22, 30, 1),
(22, 29, 1),
(22, 31, 1),
(22, 28, 1),
(22, 34, 1),
(22, 24, 1),
(22, 20, 1),
(22, 21, 1),
(22, 26, 1),
(22, 23, 1),
(22, 18, 1),
(22, 25, 1),
(22, 17, 1),
(22, 22, 1),
(22, 19, 1),
(22, 48, 1),
(22, 50, 1),
(22, 58, 1),
(22, 35, 1),
(17, 2, 1),
(17, 36, 1),
(17, 39, 1),
(17, 42, 1),
(17, 43, 1),
(17, 54, 1),
(17, 55, 1),
(17, 56, 1),
(17, 57, 1),
(17, 27, 1),
(17, 47, 1),
(17, 11, 1),
(17, 52, 1),
(17, 5, 1),
(17, 6, 1),
(17, 7, 1),
(17, 13, 1),
(17, 15, 1),
(17, 16, 1),
(17, 51, 1),
(17, 53, 1),
(17, 46, 1),
(17, 33, 1),
(17, 32, 1),
(17, 30, 1),
(17, 29, 1),
(17, 31, 1),
(17, 28, 1),
(17, 34, 1),
(17, 24, 1),
(17, 20, 1),
(17, 21, 1),
(17, 26, 1),
(17, 23, 1),
(17, 18, 1),
(17, 25, 1),
(17, 17, 1),
(17, 22, 1),
(17, 19, 1),
(17, 48, 1),
(17, 50, 1),
(17, 58, 1),
(17, 35, 1),
(20, 2, 1),
(20, 36, 1),
(20, 39, 1),
(20, 42, 1),
(20, 43, 1),
(20, 54, 1),
(20, 55, 1),
(20, 56, 1),
(20, 57, 1),
(20, 27, 1),
(20, 47, 1),
(20, 11, 1),
(20, 52, 1),
(20, 5, 1),
(20, 6, 1),
(20, 7, 1),
(20, 13, 1),
(20, 15, 1),
(20, 16, 1),
(20, 51, 1),
(20, 53, 1),
(20, 46, 1),
(20, 33, 1),
(20, 32, 1),
(20, 30, 1),
(20, 29, 1),
(20, 31, 1),
(20, 28, 1),
(20, 34, 1),
(20, 24, 1),
(20, 20, 1),
(20, 21, 1),
(20, 26, 1),
(20, 23, 1),
(20, 18, 1),
(20, 25, 1),
(20, 17, 1),
(20, 22, 1),
(20, 19, 1),
(20, 48, 1),
(20, 50, 1),
(20, 58, 1),
(20, 35, 1),
(19, 60, 1),
(19, 66, 1),
(19, 67, 1),
(19, 59, 1),
(19, 65, 1),
(19, 61, 1),
(19, 63, 1),
(3, 60, 1),
(3, 66, 1),
(3, 67, 1),
(3, 59, 1),
(3, 65, 1),
(3, 61, 1),
(3, 63, 1),
(22, 60, 1),
(22, 66, 1),
(22, 67, 1),
(22, 59, 1),
(22, 65, 1),
(22, 61, 1),
(22, 63, 1),
(17, 60, 1),
(17, 66, 1),
(17, 67, 1),
(17, 59, 1),
(17, 65, 1),
(17, 61, 1),
(17, 63, 1),
(20, 60, 1),
(20, 66, 1),
(20, 67, 1),
(20, 59, 1),
(20, 65, 1),
(20, 61, 1),
(20, 63, 1),
(19, 69, 1),
(3, 69, 1),
(22, 69, 1),
(17, 69, 1),
(20, 69, 1),
(23, 7, 1),
(25, 60, 1),
(28, 60, 2),
(33, 7, 1),
(34, 7, 1),
(35, 7, 1),
(36, 69, 1),
(37, 60, 3),
(37, 66, 1),
(37, 67, 1),
(37, 59, 1),
(37, 65, 1),
(37, 61, 1),
(37, 63, 1),
(19, 74, 1),
(19, 78, 1),
(19, 76, 1),
(19, 71, 1),
(19, 73, 1),
(19, 75, 1),
(19, 77, 1),
(3, 74, 1),
(3, 78, 1),
(3, 76, 1),
(3, 71, 1),
(3, 73, 1),
(3, 75, 1),
(3, 77, 1),
(22, 74, 1),
(22, 78, 1),
(22, 76, 1),
(22, 71, 1),
(22, 73, 1),
(22, 75, 1),
(22, 77, 1),
(17, 74, 1),
(17, 78, 1),
(17, 76, 1),
(17, 71, 1),
(17, 73, 1),
(17, 75, 1),
(17, 77, 1),
(20, 74, 1),
(20, 78, 1),
(20, 76, 1),
(20, 71, 1),
(20, 73, 1),
(20, 75, 1),
(20, 77, 1),
(38, 74, 1),
(38, 78, 1),
(38, 76, 1),
(38, 71, 1),
(38, 73, 1),
(38, 75, 1),
(38, 77, 1),
(39, 74, 1),
(39, 78, 1),
(39, 76, 1),
(39, 71, 1),
(39, 73, 1),
(39, 75, 1),
(39, 77, 1),
(41, 11, 1),
(41, 52, 1),
(41, 5, 1),
(41, 6, 1),
(41, 7, 2),
(41, 13, 1),
(41, 15, 1),
(41, 16, 1),
(41, 51, 1),
(44, 2, 1),
(44, 11, 1),
(44, 52, 1),
(44, 5, 1),
(44, 6, 1),
(44, 7, 1),
(44, 13, 1),
(44, 15, 1),
(44, 16, 1),
(44, 51, 1),
(44, 24, 1),
(44, 20, 1),
(44, 21, 1),
(44, 26, 1),
(44, 23, 1),
(44, 18, 1),
(44, 25, 1),
(44, 17, 1),
(44, 22, 1),
(44, 19, 1),
(44, 48, 1),
(44, 50, 1),
(44, 58, 1),
(44, 27, 1),
(44, 33, 1),
(44, 32, 1),
(44, 30, 1),
(44, 29, 1),
(44, 31, 1),
(44, 28, 1),
(44, 34, 1),
(44, 35, 1),
(44, 36, 1),
(44, 39, 1),
(44, 42, 1),
(44, 43, 1),
(44, 46, 1),
(44, 47, 1),
(44, 53, 1),
(44, 54, 1),
(44, 55, 1),
(44, 56, 1),
(44, 57, 1),
(44, 60, 1),
(44, 66, 1),
(44, 67, 1),
(44, 59, 1),
(44, 65, 1),
(44, 61, 1),
(44, 63, 1),
(44, 69, 1),
(44, 74, 1),
(44, 78, 1),
(44, 76, 1),
(44, 71, 1),
(44, 73, 1),
(44, 75, 1),
(44, 77, 1);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_comments`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_comments` (
  `cid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `module` varchar(55) NOT NULL,
  `area` tinyint(4) NOT NULL DEFAULT '0',
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `pid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  `post_time` int(11) unsigned NOT NULL DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `post_name` varchar(100) NOT NULL,
  `post_email` varchar(100) NOT NULL,
  `post_ip` varchar(15) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `likes` mediumint(9) NOT NULL DEFAULT '0',
  `dislikes` mediumint(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cid`),
  KEY `mod_id` (`module`,`area`,`id`),
  KEY `post_time` (`post_time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_contact_department`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_contact_department` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `fax` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `yahoo` varchar(100) NOT NULL,
  `skype` varchar(100) NOT NULL,
  `note` text NOT NULL,
  `admins` text NOT NULL,
  `act` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `full_name` (`full_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `kcxd_vi_contact_department`
--

INSERT INTO `kcxd_vi_contact_department` (`id`, `full_name`, `phone`, `fax`, `email`, `yahoo`, `skype`, `note`, `admins`, `act`) VALUES
(1, 'Webmaster', '', '', '', '', '', '', '1/1/1/0;', 1);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_contact_reply`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_contact_reply` (
  `rid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `reply_content` text,
  `reply_time` int(11) unsigned NOT NULL DEFAULT '0',
  `reply_aid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`rid`),
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_contact_send`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_contact_send` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `cid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `send_time` int(11) unsigned NOT NULL DEFAULT '0',
  `sender_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `sender_name` varchar(100) NOT NULL,
  `sender_email` varchar(100) NOT NULL,
  `sender_phone` varchar(255) DEFAULT '',
  `sender_ip` varchar(15) NOT NULL,
  `is_read` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_reply` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sender_name` (`sender_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_download`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_download` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `introtext` text NOT NULL,
  `uploadtime` int(11) unsigned NOT NULL,
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  `user_id` mediumint(8) unsigned NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `author_name` varchar(100) NOT NULL,
  `author_email` varchar(60) NOT NULL,
  `author_url` varchar(255) NOT NULL,
  `fileupload` text NOT NULL,
  `linkdirect` text NOT NULL,
  `version` varchar(20) NOT NULL,
  `filesize` int(11) NOT NULL DEFAULT '0',
  `fileimage` varchar(255) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `copyright` varchar(255) NOT NULL,
  `view_hits` int(11) NOT NULL DEFAULT '0',
  `download_hits` int(11) NOT NULL DEFAULT '0',
  `groups_comment` varchar(255) NOT NULL,
  `groups_view` varchar(255) NOT NULL,
  `groups_download` varchar(255) NOT NULL,
  `comment_hits` int(11) NOT NULL DEFAULT '0',
  `rating_detail` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`),
  KEY `catid` (`catid`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `kcxd_vi_download`
--

INSERT INTO `kcxd_vi_download` (`id`, `catid`, `title`, `alias`, `description`, `introtext`, `uploadtime`, `updatetime`, `user_id`, `user_name`, `author_name`, `author_email`, `author_url`, `fileupload`, `linkdirect`, `version`, `filesize`, `fileimage`, `status`, `copyright`, `view_hits`, `download_hits`, `groups_comment`, `groups_view`, `groups_download`, `comment_hits`, `rating_detail`) VALUES
(1, 1, 'Vì chỉ kể đến một lượng hạn chế vật liệu tham', 'Vi-chi-ke-den-mot-luong-han-che-vat-lieu-tham', '<p>Vì chỉ kể đến một lượng hạn chế vật liệu tham Vì chỉ kể đến một lượng hạn chế vật liệu tham</p>\r\n\r\n<p>&nbsp;</p>', 'Vì chỉ kể đến một lượng hạn chế vật liệu tham Vì chỉ kể đến một lượng hạn chế vật liệu tham', 1421255565, 1421255565, 1, 'admin', 'Lê Minh Long', 'tranvanthi@gmail.com', '', '/download/files/tcvn-10304-2014-tieu-chuan-thiet-ke-mong-coc-pdf.pdf', '', '', 941828, '/download/images/tcvn2.jpg', 1, '', 1, 0, '4', '6', '6', 0, ''),
(2, 3, 'Với hệ số uốn dọc được xác định như sau', 'Voi-he-so-uon-doc-duoc-xac-dinh-nhu-sau', '<p>Vì chỉ kể đến một lượng hạn chế vật liệu tham Vì chỉ kể đến một lượng hạn chế vật liệu tham</p>\r\n\r\n<p>&nbsp;</p>', 'Với hệ số uốn dọc được xác định như sau', 1421255697, 1421255697, 1, 'admin', 'Nguyễn Minh Tâm', 'tranvanthi@gmail.com', '', '/download/files/tcxd-254-2001-cong-trinh-be-tong-cot-thep-toan-khoi-xay-dung-bang-cop-pha-truot-tieu-chuan-thi-cong-va-nghiem-thu.rar', '', '', 808610, 'nload/images/tcvn2.jpg', 1, '', 1, 0, '4', '6', '6', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_download_categories`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_download_categories` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` smallint(5) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `description` text,
  `groups_view` varchar(255) DEFAULT '',
  `groups_download` varchar(255) DEFAULT '',
  `weight` smallint(4) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `kcxd_vi_download_categories`
--

INSERT INTO `kcxd_vi_download_categories` (`id`, `parentid`, `title`, `alias`, `description`, `groups_view`, `groups_download`, `weight`, `status`) VALUES
(1, 0, 'Tiêu chuẩn xây dựng', 'tieu-chuan-xay-dung', 'Tiêu chuẩn xây dựng', '6', '4', 1, 1),
(2, 0, 'Phần mềm xây dựng', 'phan-mem-xay-dung', 'Phần mềm xây dựng', '6', '4', 3, 1),
(3, 0, 'Thư viện kết cấu', 'thu-vien-ket-cau', 'Thư viện kết cấu', '6', '4', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_download_config`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_download_config` (
  `config_name` varchar(30) NOT NULL,
  `config_value` varchar(255) NOT NULL,
  UNIQUE KEY `config_name` (`config_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kcxd_vi_download_config`
--

INSERT INTO `kcxd_vi_download_config` (`config_name`, `config_value`) VALUES
('is_addfile', '1'),
('is_upload', '1'),
('groups_upload', ''),
('maxfilesize', '2097152'),
('upload_filetype', 'doc,xls,zip,rar'),
('upload_dir', 'files'),
('temp_dir', 'temp'),
('groups_addfile', ''),
('is_zip', '1'),
('is_resume', '1'),
('max_speed', '0');

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_download_report`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_download_report` (
  `fid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `post_ip` varchar(45) NOT NULL,
  `post_time` int(11) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `fid` (`fid`),
  KEY `post_time` (`post_time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_download_tmp`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_download_tmp` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `introtext` text NOT NULL,
  `uploadtime` int(11) unsigned NOT NULL DEFAULT '0',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `user_name` varchar(100) NOT NULL,
  `author_name` varchar(100) NOT NULL,
  `author_email` varchar(60) NOT NULL,
  `author_url` varchar(255) NOT NULL,
  `fileupload` text NOT NULL,
  `linkdirect` text NOT NULL,
  `version` varchar(20) NOT NULL,
  `filesize` varchar(255) NOT NULL,
  `fileimage` varchar(255) NOT NULL,
  `copyright` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_menu`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_menu` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `kcxd_vi_menu`
--

INSERT INTO `kcxd_vi_menu` (`id`, `title`) VALUES
(1, 'Top Menu');

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_menu_rows`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_menu_rows` (
  `id` mediumint(5) NOT NULL AUTO_INCREMENT,
  `parentid` mediumint(5) unsigned NOT NULL,
  `mid` smallint(5) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `link` text NOT NULL,
  `icon` varchar(255) DEFAULT '',
  `note` varchar(255) DEFAULT '',
  `weight` int(11) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `lev` int(11) NOT NULL DEFAULT '0',
  `subitem` text,
  `groups_view` varchar(255) DEFAULT '',
  `module_name` varchar(255) DEFAULT '',
  `op` varchar(255) DEFAULT '',
  `target` tinyint(4) DEFAULT '0',
  `css` varchar(255) DEFAULT '',
  `active_type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parentid` (`parentid`,`mid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `kcxd_vi_menu_rows`
--

INSERT INTO `kcxd_vi_menu_rows` (`id`, `parentid`, `mid`, `title`, `link`, `icon`, `note`, `weight`, `sort`, `lev`, `subitem`, `groups_view`, `module_name`, `op`, `target`, `css`, `active_type`, `status`) VALUES
(4, 0, 1, 'Tin Tức', '/lam/index.php?language=vi&nv=news', '', '', 1, 1, 0, '5,6,7,8,30,31,32', '6', 'news', '', 1, '', 1, 1),
(5, 4, 1, 'Tin tức', '/lam/index.php?language=vi&nv=news&amp;op=Tin-tuc', '', '', 1, 2, 1, '', '6', 'news', 'Tin-tuc', 1, '', 1, 1),
(6, 4, 1, 'Sản phẩm', '/lam/index.php?language=vi&nv=news&amp;op=San-pham', '', '', 2, 3, 1, '', '6', 'news', 'San-pham', 1, '', 1, 1),
(7, 4, 1, 'Đối tác', '/lam/index.php?language=vi&nv=news&amp;op=Doi-tac', '', '', 3, 4, 1, '', '6', 'news', 'Doi-tac', 1, '', 1, 1),
(8, 4, 1, 'Tuyển dụng', '/lam/index.php?language=vi&nv=news&amp;op=Tuyen-dung', '', '', 4, 5, 1, '', '6', 'news', 'Tuyen-dung', 1, '', 1, 1),
(9, 0, 1, 'Thành viên', '/lam/index.php?language=vi&nv=users', '', '', 2, 9, 0, '10,11,12,13,14,15,16', '6', 'users', '', 1, '', 1, 1),
(10, 9, 1, 'Đăng nhập', '/lam/index.php?language=vi&nv=users&op=login', '', '', 1, 10, 1, '', '5', 'users', 'login', 1, '', 1, 1),
(11, 9, 1, 'Logout', '/lam/index.php?language=vi&nv=users&op=logout', '', '', 2, 11, 1, '', '4', 'users', 'logout', 1, '', 1, 1),
(12, 9, 1, 'Đăng ký', '/lam/index.php?language=vi&nv=users&op=register', '', '', 3, 12, 1, '', '5', 'users', 'register', 1, '', 1, 1),
(13, 9, 1, 'Quên mật khẩu', '/lam/index.php?language=vi&nv=users&op=lostpass', '', '', 4, 13, 1, '', '5', 'users', 'lostpass', 1, '', 1, 1),
(14, 9, 1, 'Đổi mật khẩu', '/lam/index.php?language=vi&nv=users&op=changepass', '', '', 5, 14, 1, '', '4', 'users', 'changepass', 1, '', 1, 1),
(15, 9, 1, 'Openid', '/lam/index.php?language=vi&nv=users&op=openid', '', '', 6, 15, 1, '', '4', 'users', 'openid', 1, '', 1, 1),
(16, 9, 1, 'Danh sách thành viên', '/lam/index.php?language=vi&nv=users&op=memberlist', '', '', 7, 16, 1, '', '4', 'users', 'memberlist', 1, '', 1, 1),
(17, 0, 1, 'Liên hệ', '/lam/index.php?language=vi&nv=contact', '', '', 5, 24, 0, '18', '6', 'contact', '', 1, '', 1, 1),
(18, 17, 1, 'Webmaster', '/lam/index.php?language=vi&nv=contact&amp;op=1', '', '', 1, 25, 1, '', '6', 'contact', '1', 1, '', 1, 1),
(19, 0, 1, 'Thống kê', '/lam/index.php?language=vi&nv=statistics', '', '', 3, 17, 0, '20,21,22,23,24', '6', 'statistics', '', 1, '', 1, 1),
(20, 19, 1, 'Theo đường dẫn đến site', '/lam/index.php?language=vi&nv=statistics&amp;op=allreferers', '', '', 1, 18, 1, '', '6', 'statistics', 'allreferers', 1, '', 1, 1),
(21, 19, 1, 'Theo quốc gia', '/lam/index.php?language=vi&nv=statistics&amp;op=allcountries', '', '', 2, 19, 1, '', '6', 'statistics', 'allcountries', 1, '', 1, 1),
(22, 19, 1, 'Theo trình duyệt', '/lam/index.php?language=vi&nv=statistics&amp;op=allbrowsers', '', '', 3, 20, 1, '', '6', 'statistics', 'allbrowsers', 1, '', 1, 1),
(23, 19, 1, 'Theo hệ điều hành', '/lam/index.php?language=vi&nv=statistics&amp;op=allos', '', '', 4, 21, 1, '', '6', 'statistics', 'allos', 1, '', 1, 1),
(24, 19, 1, 'Máy chủ tìm kiếm', '/lam/index.php?language=vi&nv=statistics&amp;op=allbots', '', '', 5, 22, 1, '', '6', 'statistics', 'allbots', 1, '', 1, 1),
(25, 0, 1, 'Thăm dò ý kiến', '/lam/index.php?language=vi&nv=voting', '', '', 4, 23, 0, '', '6', 'voting', '', 1, '', 1, 1),
(30, 4, 1, 'Rss', '/lam/index.php?language=vi&nv=news&op=rss', '', '', 5, 6, 1, '', '6', 'news', 'rss', 1, '', 0, 1),
(31, 4, 1, 'Đăng bài viết', '/lam/index.php?language=vi&nv=news&op=content', '', '', 6, 7, 1, '', '6', 'news', 'content', 1, '', 0, 1),
(32, 4, 1, 'Tìm kiếm', '/lam/index.php?language=vi&nv=news&op=search', '', '', 7, 8, 1, '', '6', 'news', 'search', 1, '', 0, 1),
(33, 0, 1, 'Rss Feeds', '/lam/index.php?language=vi&nv=feeds', '', '', 6, 26, 0, '', '6', 'feeds', '', 1, '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_modfuncs`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_modfuncs` (
  `func_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `func_name` varchar(55) NOT NULL,
  `alias` varchar(55) NOT NULL DEFAULT '',
  `func_custom_name` varchar(255) NOT NULL,
  `in_module` varchar(55) NOT NULL,
  `show_func` tinyint(4) NOT NULL DEFAULT '0',
  `in_submenu` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `subweight` smallint(2) unsigned NOT NULL DEFAULT '1',
  `setting` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`func_id`),
  UNIQUE KEY `func_name` (`func_name`,`in_module`),
  UNIQUE KEY `alias` (`alias`,`in_module`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=79 ;

--
-- Dumping data for table `kcxd_vi_modfuncs`
--

INSERT INTO `kcxd_vi_modfuncs` (`func_id`, `func_name`, `alias`, `func_custom_name`, `in_module`, `show_func`, `in_submenu`, `subweight`, `setting`) VALUES
(1, 'sitemap', 'sitemap', 'Sitemap', 'about', 0, 0, 0, ''),
(2, 'main', 'main', 'Main', 'about', 1, 0, 1, ''),
(3, 'sitemap', 'sitemap', 'Sitemap', 'news', 0, 0, 0, ''),
(5, 'content', 'content', 'Content', 'news', 1, 1, 3, ''),
(6, 'detail', 'detail', 'Detail', 'news', 1, 0, 4, ''),
(7, 'main', 'main', 'Main', 'news', 1, 0, 5, ''),
(9, 'print', 'print', 'Print', 'news', 0, 0, 0, ''),
(10, 'rating', 'rating', 'Rating', 'news', 0, 0, 0, ''),
(11, 'rss', 'rss', 'Rss', 'news', 1, 1, 1, ''),
(12, 'savefile', 'savefile', 'Savefile', 'news', 0, 0, 0, ''),
(13, 'search', 'search', 'Search', 'news', 1, 1, 6, ''),
(14, 'sendmail', 'sendmail', 'Sendmail', 'news', 0, 0, 0, ''),
(15, 'topic', 'topic', 'Topic', 'news', 1, 0, 7, ''),
(16, 'viewcat', 'viewcat', 'Viewcat', 'news', 1, 0, 8, ''),
(17, 'active', 'active', 'Active', 'users', 1, 1, 8, ''),
(18, 'changepass', 'changepass', 'Đổi mật khẩu', 'users', 1, 1, 6, ''),
(19, 'editinfo', 'editinfo', 'Editinfo', 'users', 1, 0, 10, ''),
(20, 'login', 'login', 'Đăng nhập', 'users', 1, 1, 2, ''),
(21, 'logout', 'logout', 'Logout', 'users', 1, 1, 3, ''),
(22, 'lostactivelink', 'lostactivelink', 'Lostactivelink', 'users', 1, 0, 9, ''),
(23, 'lostpass', 'lostpass', 'Quên mật khẩu', 'users', 1, 1, 5, ''),
(24, 'main', 'main', 'Main', 'users', 1, 0, 1, ''),
(25, 'openid', 'openid', 'Openid', 'users', 1, 1, 7, ''),
(26, 'register', 'register', 'Đăng ký', 'users', 1, 1, 4, ''),
(27, 'main', 'main', 'Main', 'contact', 1, 0, 1, ''),
(28, 'allbots', 'allbots', 'Máy chủ tìm kiếm', 'statistics', 1, 1, 6, ''),
(29, 'allbrowsers', 'allbrowsers', 'Theo trình duyệt', 'statistics', 1, 1, 4, ''),
(30, 'allcountries', 'allcountries', 'Theo quốc gia', 'statistics', 1, 1, 3, ''),
(31, 'allos', 'allos', 'Theo hệ điều hành', 'statistics', 1, 1, 5, ''),
(32, 'allreferers', 'allreferers', 'Theo đường dẫn đến site', 'statistics', 1, 1, 2, ''),
(33, 'main', 'main', 'Main', 'statistics', 1, 0, 1, ''),
(34, 'referer', 'referer', 'Đường dẫn đến site theo tháng', 'statistics', 1, 0, 7, ''),
(35, 'main', 'main', 'Main', 'voting', 1, 0, 1, ''),
(36, 'addads', 'addads', 'Addads', 'banners', 1, 0, 1, ''),
(37, 'cledit', 'cledit', 'Cledit', 'banners', 0, 0, 0, ''),
(38, 'click', 'click', 'Click', 'banners', 0, 0, 0, ''),
(39, 'clientinfo', 'clientinfo', 'Clientinfo', 'banners', 1, 0, 2, ''),
(40, 'clinfo', 'clinfo', 'Clinfo', 'banners', 0, 0, 0, ''),
(41, 'logininfo', 'logininfo', 'Logininfo', 'banners', 0, 0, 0, ''),
(42, 'main', 'main', 'Main', 'banners', 1, 0, 3, ''),
(43, 'stats', 'stats', 'Stats', 'banners', 1, 0, 4, ''),
(44, 'viewmap', 'viewmap', 'Viewmap', 'banners', 0, 0, 0, ''),
(46, 'main', 'main', 'Main', 'seek', 1, 0, 1, ''),
(47, 'main', 'main', 'Main', 'feeds', 1, 0, 1, ''),
(48, 'regroups', 'regroups', 'Nhóm thành viên', 'users', 1, 0, 11, ''),
(50, 'memberlist', 'memberlist', 'Danh sách thành viên', 'users', 1, 1, 12, ''),
(51, 'groups', 'groups', 'Groups', 'news', 1, 0, 9, ''),
(52, 'tag', 'tag', 'Tag', 'news', 1, 0, 2, ''),
(53, 'main', 'main', 'Main', 'page', 1, 0, 1, ''),
(54, 'main', 'main', 'main', 'comment', 1, 0, 1, ''),
(55, 'post', 'post', 'post', 'comment', 1, 0, 2, ''),
(56, 'like', 'like', 'Like', 'comment', 1, 0, 3, ''),
(57, 'delete', 'delete', 'Delete', 'comment', 1, 0, 4, ''),
(58, 'avatar', 'avatar', 'Avatar', 'users', 1, 0, 13, ''),
(59, 'down', 'down', 'Down', 'download', 1, 0, 4, ''),
(60, 'main', 'main', 'Main', 'download', 1, 1, 1, ''),
(61, 'report', 'report', 'Report', 'download', 1, 0, 6, ''),
(62, 'rss', 'rss', 'Rss', 'download', 0, 0, 0, ''),
(63, 'search', 'search', 'Search', 'download', 1, 1, 7, ''),
(64, 'sitemap', 'sitemap', 'Sitemap', 'download', 0, 0, 0, ''),
(65, 'upload', 'upload', 'Upload', 'download', 1, 1, 5, ''),
(66, 'viewcat', 'viewcat', 'Viewcat', 'download', 1, 0, 2, ''),
(67, 'viewfile', 'viewfile', 'Viewfile', 'download', 1, 0, 3, ''),
(68, 'Sitemap', 'Sitemap', 'Sitemap', 'video-clip', 0, 0, 0, ''),
(69, 'main', 'main', 'Main', 'video-clip', 1, 1, 1, ''),
(70, 'rss', 'rss', 'Rss', 'video-clip', 0, 0, 0, ''),
(71, 'cat', 'cat', 'Cat', 'vanban', 1, 0, 4, ''),
(72, 'down', 'down', 'Down', 'vanban', 0, 0, 0, ''),
(73, 'field', 'field', 'Field', 'vanban', 1, 0, 5, ''),
(74, 'main', 'main', 'Main', 'vanban', 1, 0, 1, ''),
(75, 'organ', 'organ', 'Organ', 'vanban', 1, 0, 6, ''),
(76, 'room', 'room', 'Room', 'vanban', 1, 0, 3, ''),
(77, 'search', 'search', 'Search', 'vanban', 1, 0, 7, ''),
(78, 'view', 'view', 'View', 'vanban', 1, 0, 2, '');

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_modthemes`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_modthemes` (
  `func_id` mediumint(8) DEFAULT NULL,
  `layout` varchar(100) DEFAULT NULL,
  `theme` varchar(100) DEFAULT NULL,
  UNIQUE KEY `func_id` (`func_id`,`layout`,`theme`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kcxd_vi_modthemes`
--

INSERT INTO `kcxd_vi_modthemes` (`func_id`, `layout`, `theme`) VALUES
(0, 'body', 'mobile_nukeviet'),
(0, 'left-body-right', 'default'),
(2, 'body', 'mobile_nukeviet'),
(2, 'left-body-right', 'default'),
(5, 'body', 'mobile_nukeviet'),
(5, 'left-body-right', 'default'),
(6, 'body', 'mobile_nukeviet'),
(6, 'body-right', 'default'),
(7, 'body', 'mobile_nukeviet'),
(7, 'left-body-right', 'default'),
(11, 'body', 'mobile_nukeviet'),
(11, 'left-body-right', 'default'),
(13, 'body', 'mobile_nukeviet'),
(13, 'left-body-right', 'default'),
(15, 'body', 'mobile_nukeviet'),
(15, 'left-body-right', 'default'),
(16, 'body', 'mobile_nukeviet'),
(16, 'left-body-right', 'default'),
(17, 'body', 'mobile_nukeviet'),
(17, 'left-body-right', 'default'),
(18, 'body', 'mobile_nukeviet'),
(18, 'left-body-right', 'default'),
(19, 'body', 'mobile_nukeviet'),
(19, 'left-body-right', 'default'),
(20, 'body', 'mobile_nukeviet'),
(20, 'left-body-right', 'default'),
(21, 'body', 'mobile_nukeviet'),
(21, 'left-body-right', 'default'),
(22, 'body', 'mobile_nukeviet'),
(22, 'left-body-right', 'default'),
(23, 'body', 'mobile_nukeviet'),
(23, 'left-body-right', 'default'),
(24, 'body', 'mobile_nukeviet'),
(24, 'left-body-right', 'default'),
(25, 'body', 'mobile_nukeviet'),
(25, 'left-body-right', 'default'),
(26, 'body', 'mobile_nukeviet'),
(26, 'left-body-right', 'default'),
(27, 'body', 'mobile_nukeviet'),
(27, 'left-body-right', 'default'),
(28, 'body', 'mobile_nukeviet'),
(28, 'left-body', 'default'),
(29, 'body', 'mobile_nukeviet'),
(29, 'left-body', 'default'),
(30, 'body', 'mobile_nukeviet'),
(30, 'left-body', 'default'),
(31, 'body', 'mobile_nukeviet'),
(31, 'left-body', 'default'),
(32, 'body', 'mobile_nukeviet'),
(32, 'left-body', 'default'),
(33, 'body', 'mobile_nukeviet'),
(33, 'left-body', 'default'),
(34, 'body', 'mobile_nukeviet'),
(34, 'left-body', 'default'),
(35, 'body', 'mobile_nukeviet'),
(35, 'left-body-right', 'default'),
(36, 'body', 'mobile_nukeviet'),
(36, 'left-body-right', 'default'),
(39, 'body', 'mobile_nukeviet'),
(39, 'left-body-right', 'default'),
(42, 'body', 'mobile_nukeviet'),
(42, 'left-body-right', 'default'),
(43, 'body', 'mobile_nukeviet'),
(43, 'left-body-right', 'default'),
(46, 'body', 'mobile_nukeviet'),
(46, 'left-body-right', 'default'),
(47, 'body', 'mobile_nukeviet'),
(47, 'left-body-right', 'default'),
(48, 'body', 'mobile_nukeviet'),
(48, 'left-body-right', 'default'),
(50, 'body', 'mobile_nukeviet'),
(50, 'left-body-right', 'default'),
(51, 'body', 'mobile_nukeviet'),
(51, 'left-body-right', 'default'),
(52, 'body', 'mobile_nukeviet'),
(52, 'left-body-right', 'default'),
(53, 'body', 'default'),
(53, 'body', 'mobile_nukeviet'),
(54, 'body', 'mobile_nukeviet'),
(54, 'left-body-right', 'default'),
(55, 'body', 'mobile_nukeviet'),
(55, 'left-body-right', 'default'),
(56, 'body', 'mobile_nukeviet'),
(56, 'left-body-right', 'default'),
(57, 'body', 'mobile_nukeviet'),
(57, 'left-body-right', 'default'),
(58, 'left-body-right', 'default'),
(59, 'body', 'mobile_nukeviet'),
(59, 'left-body-right', 'default'),
(60, 'body', 'mobile_nukeviet'),
(60, 'left-body-right', 'default'),
(61, 'body', 'mobile_nukeviet'),
(61, 'left-body-right', 'default'),
(62, 'left-body-right', 'default'),
(63, 'body', 'mobile_nukeviet'),
(63, 'left-body-right', 'default'),
(64, 'left-body-right', 'default'),
(65, 'body', 'mobile_nukeviet'),
(65, 'left-body-right', 'default'),
(66, 'body', 'mobile_nukeviet'),
(66, 'left-body-right', 'default'),
(67, 'body', 'mobile_nukeviet'),
(67, 'left-body-right', 'default'),
(68, 'left-body-right', 'default'),
(69, 'body', 'mobile_nukeviet'),
(69, 'left-body-right', 'default'),
(70, 'left-body-right', 'default'),
(71, 'body', 'mobile_nukeviet'),
(71, 'body-right', 'default'),
(72, 'left-body-right', 'default'),
(73, 'body', 'mobile_nukeviet'),
(73, 'body-right', 'default'),
(74, 'body', 'mobile_nukeviet'),
(74, 'body-right', 'default'),
(75, 'body', 'mobile_nukeviet'),
(75, 'body-right', 'default'),
(76, 'body', 'mobile_nukeviet'),
(76, 'body-right', 'default'),
(77, 'body', 'mobile_nukeviet'),
(77, 'body-right', 'default'),
(78, 'body', 'default'),
(78, 'body', 'mobile_nukeviet');

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_modules`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_modules` (
  `title` varchar(55) NOT NULL,
  `module_file` varchar(55) NOT NULL DEFAULT '',
  `module_data` varchar(55) NOT NULL DEFAULT '',
  `custom_title` varchar(255) NOT NULL,
  `admin_title` varchar(255) DEFAULT '',
  `set_time` int(11) unsigned NOT NULL DEFAULT '0',
  `main_file` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `admin_file` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `theme` varchar(100) DEFAULT '',
  `mobile` varchar(100) DEFAULT '',
  `description` varchar(255) DEFAULT '',
  `keywords` text,
  `groups_view` varchar(255) NOT NULL,
  `weight` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `act` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `admins` varchar(255) DEFAULT '',
  `rss` tinyint(4) NOT NULL DEFAULT '1',
  `gid` smallint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kcxd_vi_modules`
--

INSERT INTO `kcxd_vi_modules` (`title`, `module_file`, `module_data`, `custom_title`, `admin_title`, `set_time`, `main_file`, `admin_file`, `theme`, `mobile`, `description`, `keywords`, `groups_view`, `weight`, `act`, `admins`, `rss`, `gid`) VALUES
('about', 'page', 'about', 'Giới thiệu', '', 1276333182, 1, 1, '', '', '', '', '6', 1, 1, '', 0, 0),
('news', 'news', 'news', 'Kết cấu', '', 1270400000, 1, 1, '', '', 'Kết cấu xây dựng', 'kết cấu xây dựng, kết cấu thép, kết cấu bê tông, kết cấu móng, kết cấu hầm, kết cấu vách, kết cấu lõi, kết cấu dàn không gian, structures', '6', 2, 1, '', 1, 0),
('users', 'users', 'users', 'Thành viên', 'Tài khoản', 1274080277, 1, 1, '', '', '', '', '6', 3, 1, '', 0, 0),
('contact', 'contact', 'contact', 'Liên hệ', '', 1275351337, 1, 1, '', '', '', '', '6', 4, 1, '', 0, 0),
('statistics', 'statistics', 'statistics', 'Thống kê', '', 1276520928, 1, 0, '', '', '', 'truy cập, online, statistics', '6', 5, 1, '', 0, 0),
('voting', 'voting', 'voting', 'Thăm dò ý kiến', '', 1275315261, 1, 1, '', '', '', '', '6', 6, 1, '', 1, 0),
('banners', 'banners', 'banners', 'Quảng cáo', '', 1270400000, 1, 1, '', '', '', '', '6', 7, 1, '', 0, 0),
('seek', 'seek', 'seek', 'Tìm kiếm', 'Tim kiem', 1273474173, 1, 0, '', '', '', '', '6', 8, 1, '', 0, 0),
('menu', 'menu', 'menu', 'Menu Site', '', 1295287334, 0, 1, '', '', '', '', '6', 9, 1, '', 0, 0),
('feeds', 'feeds', 'feeds', 'Rss Feeds', '', 1279366705, 1, 1, '', '', '', '', '6', 10, 1, '', 0, 0),
('page', 'page', 'page', 'Page', '', 1279366705, 1, 1, '', '', '', '', '6', 11, 1, '', 0, 0),
('comment', 'comment', 'comment', 'Bình luận', 'Quản lý bình luận', 1279366705, 1, 1, '', '', '', '', '6', 12, 1, '', 0, 0),
('download', 'download', 'download', 'Phần mềm - tcxdvn', '', 1421253563, 1, 1, '', '', 'Download phần mềm, tiêu chuẩn xây dựng', 'phần mềm xây dựng, tiêu chuẩn xây dựng, dự toán xây dựng, phần mềm tính thép, etabs, sap', '6', 13, 1, '', 1, 0),
('video-clip', 'video-clip', 'video_clip', 'Clip', '', 1421253816, 1, 1, '', '', 'Video xây dựng', 'video dạy sap, video dạy etabs, video dạy kết cấu, video thiết kế nhà, video kiến trúc nhà đẹp', '6', 14, 1, '', 1, 0),
('vanban', 'vanban', 'vanban', 'vanban', '', 1422534781, 1, 1, '', '', '', '', '6', 15, 1, '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_news_13`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_news_13` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `listcatid` varchar(255) NOT NULL DEFAULT '',
  `topicid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `admin_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `author` varchar(255) DEFAULT '',
  `sourceid` mediumint(8) NOT NULL DEFAULT '0',
  `addtime` int(11) unsigned NOT NULL DEFAULT '0',
  `edittime` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `publtime` int(11) unsigned NOT NULL DEFAULT '0',
  `exptime` int(11) unsigned NOT NULL DEFAULT '0',
  `archive` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `hometext` text NOT NULL,
  `homeimgfile` varchar(255) DEFAULT '',
  `homeimgalt` varchar(255) DEFAULT '',
  `homeimgthumb` tinyint(4) NOT NULL DEFAULT '0',
  `inhome` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `allowed_comm` varchar(255) DEFAULT '',
  `allowed_rating` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hitstotal` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `hitscm` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `total_rating` int(11) NOT NULL DEFAULT '0',
  `click_rating` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`),
  KEY `topicid` (`topicid`),
  KEY `admin_id` (`admin_id`),
  KEY `author` (`author`),
  KEY `title` (`title`),
  KEY `addtime` (`addtime`),
  KEY `publtime` (`publtime`),
  KEY `exptime` (`exptime`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `kcxd_vi_news_13`
--

INSERT INTO `kcxd_vi_news_13` (`id`, `catid`, `listcatid`, `topicid`, `admin_id`, `author`, `sourceid`, `addtime`, `edittime`, `status`, `publtime`, `exptime`, `archive`, `title`, `alias`, `hometext`, `homeimgfile`, `homeimgalt`, `homeimgthumb`, `inhome`, `allowed_comm`, `allowed_rating`, `hitstotal`, `hitscm`, `total_rating`, `click_rating`) VALUES
(12, 16, '16,14,13', 3, 1, 'ST', 0, 1421254382, 1421651132, 1, 1421254382, 0, 2, 'Thuật phong thủy và việc chọn đất làm nhà', 'Thuat-phong-thuy-va-viec-chon-dat-lam-nha', 'Ngày xưa, Phạm Lãi giúp Câu Tiễn phục quốc, trải bao tù đày, khổ nhục, nếm mật, nằm gai, cuối cùng đã đánh bại Phù Sai, rửa được cái nhục lớn ở núi Cối Kê.', '2015_01/chon-dat-lam-nha-1.jpg', 'phong thuy', 1, 1, '6', 0, 1, 0, 0, 0),
(15, 16, '16,14,13', 4, 1, 'ST', 0, 1421255177, 1421255201, 1, 1421255177, 0, 2, 'Kiểm tra Vách phẳng theo giả thiết vùng biên chịu mô men', 'Kiem-tra-Vach-phang-theo-gia-thiet-vung-bien-chiu-mo-men', 'Giới thiệu về phương pháp tính toán kiểm tra vách phẳng dựa trên giả thiết vùng biên chịu mô men. File excel cũng được lập và giới thiệu để các bạn tham khảo.', '2015_01/kiem-tra-vach.jpg', 'Chú thích cho hình', 1, 1, '6', 0, 5, 0, 0, 0),
(14, 16, '16,14,13', 2, 1, 'ST', 0, 1421254983, 1421560969, 1, 1421254983, 0, 2, 'Trang trí tiểu cảnh hợp phong thủy tăng thêm an lành', 'Trang-tri-tieu-canh-hop-phong-thuy-tang-them-an-lanh', 'Thiết kế sân vườn - tiểu cảnh hợp phong thủy vừa là cách bạn mang thiên nhiên vào nhà vừa là cách để hóa giải những khiếm khuyết về phong thủy cho ngôi nhà.', '2015_01/phong-thuy.jpg', 'jjjjjj', 1, 1, '6', 0, 2, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_news_14`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_news_14` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `listcatid` varchar(255) NOT NULL DEFAULT '',
  `topicid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `admin_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `author` varchar(255) DEFAULT '',
  `sourceid` mediumint(8) NOT NULL DEFAULT '0',
  `addtime` int(11) unsigned NOT NULL DEFAULT '0',
  `edittime` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `publtime` int(11) unsigned NOT NULL DEFAULT '0',
  `exptime` int(11) unsigned NOT NULL DEFAULT '0',
  `archive` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `hometext` text NOT NULL,
  `homeimgfile` varchar(255) DEFAULT '',
  `homeimgalt` varchar(255) DEFAULT '',
  `homeimgthumb` tinyint(4) NOT NULL DEFAULT '0',
  `inhome` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `allowed_comm` varchar(255) DEFAULT '',
  `allowed_rating` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hitstotal` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `hitscm` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `total_rating` int(11) NOT NULL DEFAULT '0',
  `click_rating` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`),
  KEY `topicid` (`topicid`),
  KEY `admin_id` (`admin_id`),
  KEY `author` (`author`),
  KEY `title` (`title`),
  KEY `addtime` (`addtime`),
  KEY `publtime` (`publtime`),
  KEY `exptime` (`exptime`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `kcxd_vi_news_14`
--

INSERT INTO `kcxd_vi_news_14` (`id`, `catid`, `listcatid`, `topicid`, `admin_id`, `author`, `sourceid`, `addtime`, `edittime`, `status`, `publtime`, `exptime`, `archive`, `title`, `alias`, `hometext`, `homeimgfile`, `homeimgalt`, `homeimgthumb`, `inhome`, `allowed_comm`, `allowed_rating`, `hitstotal`, `hitscm`, `total_rating`, `click_rating`) VALUES
(12, 16, '16,14,13', 3, 1, 'ST', 0, 1421254382, 1421651132, 1, 1421254382, 0, 2, 'Thuật phong thủy và việc chọn đất làm nhà', 'Thuat-phong-thuy-va-viec-chon-dat-lam-nha', 'Ngày xưa, Phạm Lãi giúp Câu Tiễn phục quốc, trải bao tù đày, khổ nhục, nếm mật, nằm gai, cuối cùng đã đánh bại Phù Sai, rửa được cái nhục lớn ở núi Cối Kê.', '2015_01/chon-dat-lam-nha-1.jpg', 'phong thuy', 1, 1, '6', 0, 1, 0, 0, 0),
(15, 16, '16,14,13', 4, 1, 'ST', 0, 1421255177, 1421255201, 1, 1421255177, 0, 2, 'Kiểm tra Vách phẳng theo giả thiết vùng biên chịu mô men', 'Kiem-tra-Vach-phang-theo-gia-thiet-vung-bien-chiu-mo-men', 'Giới thiệu về phương pháp tính toán kiểm tra vách phẳng dựa trên giả thiết vùng biên chịu mô men. File excel cũng được lập và giới thiệu để các bạn tham khảo.', '2015_01/kiem-tra-vach.jpg', 'Chú thích cho hình', 1, 1, '6', 0, 5, 0, 0, 0),
(14, 16, '16,14,13', 2, 1, 'ST', 0, 1421254983, 1421560969, 1, 1421254983, 0, 2, 'Trang trí tiểu cảnh hợp phong thủy tăng thêm an lành', 'Trang-tri-tieu-canh-hop-phong-thuy-tang-them-an-lanh', 'Thiết kế sân vườn - tiểu cảnh hợp phong thủy vừa là cách bạn mang thiên nhiên vào nhà vừa là cách để hóa giải những khiếm khuyết về phong thủy cho ngôi nhà.', '2015_01/phong-thuy.jpg', 'jjjjjj', 1, 1, '6', 0, 2, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_news_15`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_news_15` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `listcatid` varchar(255) NOT NULL DEFAULT '',
  `topicid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `admin_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `author` varchar(255) DEFAULT '',
  `sourceid` mediumint(8) NOT NULL DEFAULT '0',
  `addtime` int(11) unsigned NOT NULL DEFAULT '0',
  `edittime` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `publtime` int(11) unsigned NOT NULL DEFAULT '0',
  `exptime` int(11) unsigned NOT NULL DEFAULT '0',
  `archive` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `hometext` text NOT NULL,
  `homeimgfile` varchar(255) DEFAULT '',
  `homeimgalt` varchar(255) DEFAULT '',
  `homeimgthumb` tinyint(4) NOT NULL DEFAULT '0',
  `inhome` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `allowed_comm` varchar(255) DEFAULT '',
  `allowed_rating` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hitstotal` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `hitscm` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `total_rating` int(11) NOT NULL DEFAULT '0',
  `click_rating` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`),
  KEY `topicid` (`topicid`),
  KEY `admin_id` (`admin_id`),
  KEY `author` (`author`),
  KEY `title` (`title`),
  KEY `addtime` (`addtime`),
  KEY `publtime` (`publtime`),
  KEY `exptime` (`exptime`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `kcxd_vi_news_15`
--

INSERT INTO `kcxd_vi_news_15` (`id`, `catid`, `listcatid`, `topicid`, `admin_id`, `author`, `sourceid`, `addtime`, `edittime`, `status`, `publtime`, `exptime`, `archive`, `title`, `alias`, `hometext`, `homeimgfile`, `homeimgalt`, `homeimgthumb`, `inhome`, `allowed_comm`, `allowed_rating`, `hitstotal`, `hitscm`, `total_rating`, `click_rating`) VALUES
(11, 17, '17,15,18', 2, 1, 'ST', 0, 1421254180, 1421254180, 1, 1421254180, 0, 2, 'Tính toán tải trọng động đất theo TCXDVN 375&#x3A;2006', 'Tinh-toan-tai-trong-dong-dat-theo-TCXDVN-375-2006', 'Bài viết này tóm tắt quy trình tính toán tải trọng động đất theo Tiêu chuẩn xây dựng Việt Nam 375 - 2006 (TCXDVN 375:2006)', '2015_01/dong-dat-vn.jpg', 'kc', 1, 1, '4', 1, 2, 0, 0, 0),
(13, 17, '17,15,18', 3, 1, 'ST', 0, 1421254741, 1421254741, 1, 1421254741, 0, 2, 'Bố trí cây xanh quanh nhà theo phong thủy', 'Bo-tri-cay-xanh-quanh-nha-theo-phong-thuy', 'Thuật phong thủy trong việc chọn đất làm nhà ngoài việc chú ý xem tướng đất, đặt cuộc đất trong mối liên quan với núi, sông, đường xá, nhà cửa…', '2015_01/cayxanhphongthuy.jpg', 'aaaa', 1, 1, '6', 1, 2, 0, 0, 0),
(16, 17, '17,15,18', 2, 1, 'ST', 0, 1421255405, 1421532694, 1, 1421255405, 0, 2, 'Kỹ sư thậm chí có thể tính nhẩm khi sử dụng phương pháp này.', 'Ky-su-tham-chi-co-the-tinh-nham-khi-su-dung-phuong-phap-nay', 'Phương pháp này dựa trên giả thiết cơ bản và mô men uốn sẽ được phân phối thành cặp ngẫu lực do các vùng biên chịu. Lực dọc sẽ phân phối một ứng suất nén', '2015_01/dong-dat-vn.jpg', 'kc', 1, 1, '6', 1, 8, 0, 5, 1),
(17, 17, '17,15,18', 2, 1, 'ST', 0, 1421511824, 1421573822, 1, 1421511824, 0, 2, 'Phần mềm xây dựng', 'Phan-mem-xay-dung', 'Giới thiệu ngắn gọn (Hiển thị đối với mọi đối tượng kể cả khi không được phân quyền). Số ký tự: 0. Nên nhập tối đa 160 ký tự', '2015_01/cayxanhphongthuy.jpg', 'aaaa', 1, 1, '6', 0, 5, 0, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_news_16`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_news_16` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `listcatid` varchar(255) NOT NULL DEFAULT '',
  `topicid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `admin_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `author` varchar(255) DEFAULT '',
  `sourceid` mediumint(8) NOT NULL DEFAULT '0',
  `addtime` int(11) unsigned NOT NULL DEFAULT '0',
  `edittime` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `publtime` int(11) unsigned NOT NULL DEFAULT '0',
  `exptime` int(11) unsigned NOT NULL DEFAULT '0',
  `archive` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `hometext` text NOT NULL,
  `homeimgfile` varchar(255) DEFAULT '',
  `homeimgalt` varchar(255) DEFAULT '',
  `homeimgthumb` tinyint(4) NOT NULL DEFAULT '0',
  `inhome` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `allowed_comm` varchar(255) DEFAULT '',
  `allowed_rating` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hitstotal` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `hitscm` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `total_rating` int(11) NOT NULL DEFAULT '0',
  `click_rating` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`),
  KEY `topicid` (`topicid`),
  KEY `admin_id` (`admin_id`),
  KEY `author` (`author`),
  KEY `title` (`title`),
  KEY `addtime` (`addtime`),
  KEY `publtime` (`publtime`),
  KEY `exptime` (`exptime`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `kcxd_vi_news_16`
--

INSERT INTO `kcxd_vi_news_16` (`id`, `catid`, `listcatid`, `topicid`, `admin_id`, `author`, `sourceid`, `addtime`, `edittime`, `status`, `publtime`, `exptime`, `archive`, `title`, `alias`, `hometext`, `homeimgfile`, `homeimgalt`, `homeimgthumb`, `inhome`, `allowed_comm`, `allowed_rating`, `hitstotal`, `hitscm`, `total_rating`, `click_rating`) VALUES
(12, 16, '16,14,13', 3, 1, 'ST', 0, 1421254382, 1421651132, 1, 1421254382, 0, 2, 'Thuật phong thủy và việc chọn đất làm nhà', 'Thuat-phong-thuy-va-viec-chon-dat-lam-nha', 'Ngày xưa, Phạm Lãi giúp Câu Tiễn phục quốc, trải bao tù đày, khổ nhục, nếm mật, nằm gai, cuối cùng đã đánh bại Phù Sai, rửa được cái nhục lớn ở núi Cối Kê.', '2015_01/chon-dat-lam-nha-1.jpg', 'phong thuy', 1, 1, '6', 0, 1, 0, 0, 0),
(15, 16, '16,14,13', 4, 1, 'ST', 0, 1421255177, 1421255201, 1, 1421255177, 0, 2, 'Kiểm tra Vách phẳng theo giả thiết vùng biên chịu mô men', 'Kiem-tra-Vach-phang-theo-gia-thiet-vung-bien-chiu-mo-men', 'Giới thiệu về phương pháp tính toán kiểm tra vách phẳng dựa trên giả thiết vùng biên chịu mô men. File excel cũng được lập và giới thiệu để các bạn tham khảo.', '2015_01/kiem-tra-vach.jpg', 'Chú thích cho hình', 1, 1, '6', 0, 5, 0, 0, 0),
(14, 16, '16,14,13', 2, 1, 'ST', 0, 1421254983, 1421560969, 1, 1421254983, 0, 2, 'Trang trí tiểu cảnh hợp phong thủy tăng thêm an lành', 'Trang-tri-tieu-canh-hop-phong-thuy-tang-them-an-lanh', 'Thiết kế sân vườn - tiểu cảnh hợp phong thủy vừa là cách bạn mang thiên nhiên vào nhà vừa là cách để hóa giải những khiếm khuyết về phong thủy cho ngôi nhà.', '2015_01/phong-thuy.jpg', 'jjjjjj', 1, 1, '6', 0, 2, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_news_17`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_news_17` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `listcatid` varchar(255) NOT NULL DEFAULT '',
  `topicid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `admin_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `author` varchar(255) DEFAULT '',
  `sourceid` mediumint(8) NOT NULL DEFAULT '0',
  `addtime` int(11) unsigned NOT NULL DEFAULT '0',
  `edittime` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `publtime` int(11) unsigned NOT NULL DEFAULT '0',
  `exptime` int(11) unsigned NOT NULL DEFAULT '0',
  `archive` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `hometext` text NOT NULL,
  `homeimgfile` varchar(255) DEFAULT '',
  `homeimgalt` varchar(255) DEFAULT '',
  `homeimgthumb` tinyint(4) NOT NULL DEFAULT '0',
  `inhome` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `allowed_comm` varchar(255) DEFAULT '',
  `allowed_rating` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hitstotal` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `hitscm` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `total_rating` int(11) NOT NULL DEFAULT '0',
  `click_rating` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`),
  KEY `topicid` (`topicid`),
  KEY `admin_id` (`admin_id`),
  KEY `author` (`author`),
  KEY `title` (`title`),
  KEY `addtime` (`addtime`),
  KEY `publtime` (`publtime`),
  KEY `exptime` (`exptime`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `kcxd_vi_news_17`
--

INSERT INTO `kcxd_vi_news_17` (`id`, `catid`, `listcatid`, `topicid`, `admin_id`, `author`, `sourceid`, `addtime`, `edittime`, `status`, `publtime`, `exptime`, `archive`, `title`, `alias`, `hometext`, `homeimgfile`, `homeimgalt`, `homeimgthumb`, `inhome`, `allowed_comm`, `allowed_rating`, `hitstotal`, `hitscm`, `total_rating`, `click_rating`) VALUES
(11, 17, '17,15,18', 2, 1, 'ST', 0, 1421254180, 1421254180, 1, 1421254180, 0, 2, 'Tính toán tải trọng động đất theo TCXDVN 375&#x3A;2006', 'Tinh-toan-tai-trong-dong-dat-theo-TCXDVN-375-2006', 'Bài viết này tóm tắt quy trình tính toán tải trọng động đất theo Tiêu chuẩn xây dựng Việt Nam 375 - 2006 (TCXDVN 375:2006)', '2015_01/dong-dat-vn.jpg', 'kc', 1, 1, '4', 1, 2, 0, 0, 0),
(13, 17, '17,15,18', 3, 1, 'ST', 0, 1421254741, 1421254741, 1, 1421254741, 0, 2, 'Bố trí cây xanh quanh nhà theo phong thủy', 'Bo-tri-cay-xanh-quanh-nha-theo-phong-thuy', 'Thuật phong thủy trong việc chọn đất làm nhà ngoài việc chú ý xem tướng đất, đặt cuộc đất trong mối liên quan với núi, sông, đường xá, nhà cửa…', '2015_01/cayxanhphongthuy.jpg', 'aaaa', 1, 1, '6', 1, 2, 0, 0, 0),
(16, 17, '17,15,18', 2, 1, 'ST', 0, 1421255405, 1421532694, 1, 1421255405, 0, 2, 'Kỹ sư thậm chí có thể tính nhẩm khi sử dụng phương pháp này.', 'Ky-su-tham-chi-co-the-tinh-nham-khi-su-dung-phuong-phap-nay', 'Phương pháp này dựa trên giả thiết cơ bản và mô men uốn sẽ được phân phối thành cặp ngẫu lực do các vùng biên chịu. Lực dọc sẽ phân phối một ứng suất nén', '2015_01/dong-dat-vn.jpg', 'kc', 1, 1, '6', 1, 8, 0, 5, 1),
(17, 17, '17,15,18', 2, 1, 'ST', 0, 1421511824, 1421573822, 1, 1421511824, 0, 2, 'Phần mềm xây dựng', 'Phan-mem-xay-dung', 'Giới thiệu ngắn gọn (Hiển thị đối với mọi đối tượng kể cả khi không được phân quyền). Số ký tự: 0. Nên nhập tối đa 160 ký tự', '2015_01/cayxanhphongthuy.jpg', 'aaaa', 1, 1, '6', 0, 5, 0, 5, 1),
(18, 17, '17', 2, 1, 'Kc', 0, 1422753399, 1422753638, 1, 1422753399, 0, 2, 'Tính toán cấu kiện chịu xoắn theo ACI 318M-08', 'Tinh-toan-cau-kien-chiu-xoan-theo-ACI-318M-08', 'Giới thiệu ngắn gọn (Hiển thị đối với mọi đối tượng kể cả khi không được phân quyền). Số ký tự: 0. Nên nhập tối đa 160 ký tự', '2015_02/message.png', 'Kiểm tra Vách phẳng theo giả thiết vùng biên chịu mô men', 1, 1, '4', 1, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_news_18`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_news_18` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `listcatid` varchar(255) NOT NULL DEFAULT '',
  `topicid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `admin_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `author` varchar(255) DEFAULT '',
  `sourceid` mediumint(8) NOT NULL DEFAULT '0',
  `addtime` int(11) unsigned NOT NULL DEFAULT '0',
  `edittime` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `publtime` int(11) unsigned NOT NULL DEFAULT '0',
  `exptime` int(11) unsigned NOT NULL DEFAULT '0',
  `archive` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `hometext` text NOT NULL,
  `homeimgfile` varchar(255) DEFAULT '',
  `homeimgalt` varchar(255) DEFAULT '',
  `homeimgthumb` tinyint(4) NOT NULL DEFAULT '0',
  `inhome` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `allowed_comm` varchar(255) DEFAULT '',
  `allowed_rating` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hitstotal` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `hitscm` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `total_rating` int(11) NOT NULL DEFAULT '0',
  `click_rating` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`),
  KEY `topicid` (`topicid`),
  KEY `admin_id` (`admin_id`),
  KEY `author` (`author`),
  KEY `title` (`title`),
  KEY `addtime` (`addtime`),
  KEY `publtime` (`publtime`),
  KEY `exptime` (`exptime`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `kcxd_vi_news_18`
--

INSERT INTO `kcxd_vi_news_18` (`id`, `catid`, `listcatid`, `topicid`, `admin_id`, `author`, `sourceid`, `addtime`, `edittime`, `status`, `publtime`, `exptime`, `archive`, `title`, `alias`, `hometext`, `homeimgfile`, `homeimgalt`, `homeimgthumb`, `inhome`, `allowed_comm`, `allowed_rating`, `hitstotal`, `hitscm`, `total_rating`, `click_rating`) VALUES
(11, 17, '17,15,18', 2, 1, 'ST', 0, 1421254180, 1421254180, 1, 1421254180, 0, 2, 'Tính toán tải trọng động đất theo TCXDVN 375&#x3A;2006', 'Tinh-toan-tai-trong-dong-dat-theo-TCXDVN-375-2006', 'Bài viết này tóm tắt quy trình tính toán tải trọng động đất theo Tiêu chuẩn xây dựng Việt Nam 375 - 2006 (TCXDVN 375:2006)', '2015_01/dong-dat-vn.jpg', 'kc', 1, 1, '4', 1, 2, 0, 0, 0),
(13, 17, '17,15,18', 3, 1, 'ST', 0, 1421254741, 1421254741, 1, 1421254741, 0, 2, 'Bố trí cây xanh quanh nhà theo phong thủy', 'Bo-tri-cay-xanh-quanh-nha-theo-phong-thuy', 'Thuật phong thủy trong việc chọn đất làm nhà ngoài việc chú ý xem tướng đất, đặt cuộc đất trong mối liên quan với núi, sông, đường xá, nhà cửa…', '2015_01/cayxanhphongthuy.jpg', 'aaaa', 1, 1, '6', 1, 2, 0, 0, 0),
(16, 17, '17,15,18', 2, 1, 'ST', 0, 1421255405, 1421532694, 1, 1421255405, 0, 2, 'Kỹ sư thậm chí có thể tính nhẩm khi sử dụng phương pháp này.', 'Ky-su-tham-chi-co-the-tinh-nham-khi-su-dung-phuong-phap-nay', 'Phương pháp này dựa trên giả thiết cơ bản và mô men uốn sẽ được phân phối thành cặp ngẫu lực do các vùng biên chịu. Lực dọc sẽ phân phối một ứng suất nén', '2015_01/dong-dat-vn.jpg', 'kc', 1, 1, '6', 1, 8, 0, 5, 1),
(17, 17, '17,15,18', 2, 1, 'ST', 0, 1421511824, 1421573822, 1, 1421511824, 0, 2, 'Phần mềm xây dựng', 'Phan-mem-xay-dung', 'Giới thiệu ngắn gọn (Hiển thị đối với mọi đối tượng kể cả khi không được phân quyền). Số ký tự: 0. Nên nhập tối đa 160 ký tự', '2015_01/cayxanhphongthuy.jpg', 'aaaa', 1, 1, '6', 0, 5, 0, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_news_admins`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_news_admins` (
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `catid` smallint(5) NOT NULL DEFAULT '0',
  `admin` tinyint(4) NOT NULL DEFAULT '0',
  `add_content` tinyint(4) NOT NULL DEFAULT '0',
  `pub_content` tinyint(4) NOT NULL DEFAULT '0',
  `edit_content` tinyint(4) NOT NULL DEFAULT '0',
  `del_content` tinyint(4) NOT NULL DEFAULT '0',
  `app_content` tinyint(4) NOT NULL DEFAULT '0',
  UNIQUE KEY `userid` (`userid`,`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_news_block`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_news_block` (
  `bid` smallint(5) unsigned NOT NULL,
  `id` int(11) unsigned NOT NULL,
  `weight` int(11) unsigned NOT NULL,
  UNIQUE KEY `bid` (`bid`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kcxd_vi_news_block`
--

INSERT INTO `kcxd_vi_news_block` (`bid`, `id`, `weight`) VALUES
(2, 13, 3),
(2, 15, 5),
(2, 17, 7),
(2, 11, 1),
(2, 12, 2),
(2, 14, 4),
(2, 16, 6),
(1, 17, 1),
(1, 16, 2),
(2, 18, 8);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_news_block_cat`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_news_block_cat` (
  `bid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `adddefault` tinyint(4) NOT NULL DEFAULT '0',
  `numbers` smallint(5) NOT NULL DEFAULT '10',
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) DEFAULT '',
  `description` varchar(255) DEFAULT '',
  `weight` smallint(5) NOT NULL DEFAULT '0',
  `keywords` text,
  `add_time` int(11) NOT NULL DEFAULT '0',
  `edit_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`bid`),
  UNIQUE KEY `title` (`title`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `kcxd_vi_news_block_cat`
--

INSERT INTO `kcxd_vi_news_block_cat` (`bid`, `adddefault`, `numbers`, `title`, `alias`, `image`, `description`, `weight`, `keywords`, `add_time`, `edit_time`) VALUES
(1, 0, 4, 'Tin tiêu điểm', 'Tin-tieu-diem', '', 'Tin tiêu điểm', 1, '', 1279945710, 1279956943),
(2, 1, 4, 'Tin mới nhất', 'Tin-moi-nhat', '', 'Tin mới nhất', 2, '', 1279945725, 1279956445);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_news_bodyhtml_1`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_news_bodyhtml_1` (
  `id` int(11) unsigned NOT NULL,
  `bodyhtml` longtext NOT NULL,
  `sourcetext` varchar(255) DEFAULT '',
  `imgposition` tinyint(1) NOT NULL DEFAULT '1',
  `copyright` tinyint(1) NOT NULL DEFAULT '0',
  `allowed_send` tinyint(1) NOT NULL DEFAULT '0',
  `allowed_print` tinyint(1) NOT NULL DEFAULT '0',
  `allowed_save` tinyint(1) NOT NULL DEFAULT '0',
  `gid` mediumint(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kcxd_vi_news_bodyhtml_1`
--

INSERT INTO `kcxd_vi_news_bodyhtml_1` (`id`, `bodyhtml`, `sourcetext`, `imgposition`, `copyright`, `allowed_send`, `allowed_print`, `allowed_save`, `gid`) VALUES
(17, '<p>Giới thiệu ngắn gọn (Hiển thị đối với mọi đối tượng kể cả khi không được phân quyền). Số ký tự: 0. Nên nhập tối đa 160 ký tự&nbsp;</p>', '', 1, 0, 0, 0, 0, 0),
(16, '<div class="bodytext">\r\n<h2 style="text-align: justify;"><span style="font-size:14px;"><strong>I. Xác định các thông số cơ bản</strong></span></h2>\r\n\r\n<p style="text-align:center"><img alt="dong dat vn" class="img-thumbnail" height="367" src="http://ketcauxaydung.com/uploads/news/2015_01/dong-dat-vn.jpg" width="550" /></p>\r\n\r\n<p style="text-align: justify;">- Xác định đỉnh gia tốc nền tham chiếu: a<sub>gR</sub>, tra bảng phụ lục I. Chú ý rằng bảng tra này đã được quy đổi theo gia tốc trọng trường g, a<sub>gR</sub> bằng giá trị tra bảng nhân với g, ví dụ đối với địa điểm thành phố Hồ Chí Minh - quận 4, có đỉnh gia tốc nền tham chiếu là 0,0847*g.</p>\r\n\r\n<p style="text-align: justify;">- Xác định hệ số tầm quan trọng, γ<sub>I</sub>, tra bảng phụ lục F và G. Ví dụ với nhà cao từ 20 tầng đến 60 tầng có mức độ quan trọng là I, hệ số tầm quan trọng là γ<sub>I</sub> = 1,25</p>\r\n\r\n<p style="text-align: justify;">- Xác định gia tốc nền thiết kế, a<sub>g</sub> = a<sub>gR</sub>*γ<sub>I</sub>; theo mục 3.2.1 của tiêu chuẩn: nếu a<sub>g</sub> &lt; 0,08*g - trường hợp động đất yếu - thì có thể sử dụng các quy trình thiết kế chịu động đất được giảm nhẹ hoặc đơn giản hóa cho một số loại, dạng kết cấu; nếu a<sub>g</sub> &lt; 0,04*g - trường hợp động đất rất yếu - thì không cần phải tuân theo những điều khoản của tiêu chuẩn. Theo [2] thì việc thiết kế kháng chấn cho các công trình xây dựng theo các quy định đề cập tới trong nội dung của tiêu chuẩn chỉ thực hiện chủ yếu cho các công trình xây dựng trong các vùng động đất mạnh có gia tốc nền a<sub>g</sub> &gt; 0,08*g.</p>\r\n\r\n<p style="text-align: justify;">- Xác định loại kết cấu, đối với kết cấu bê tông cốt thép xác định theo mục 5.1.2. Loại kết cấu của công trình có ảnh hưởng đến việc xác định hệ số ứng xử sẽ được đề cập ở mục 5. Một số trường hợp cần xác định bằng cách so sánh lực cắt (phản lực ngang tại liên kết với móng) mà các cấu kiện (các cột và các vách) sẽ gánh khi chịu lực ngang giả thiết.</p>\r\n\r\n<p style="text-align: justify;">- Xác định hệ số ứng xử q, đối với kết cấu bê tông cốt thép xác định theo mục 5.2.2.2, ví dụ hệ khung, nhiều tầng, nhiều nhịp, cấp độ dẻo trung bình (DCM) có: q = 3,0*1,3 = 3,9. Hệ số ứng xử q có thể khác nhau theo hai phương chính tùy thuộc hệ kết cấu.</p>\r\n\r\n<p style="text-align: justify;">- Xác định loại nền đất, theo mục 3.2.1, có thể sử dụng giá trị trung bình của chỉ số SPT của các lớp đất trong chiều sâu 30m. Loại nền đất có vai trò xác định các tham số mô tả phổ phản ứng gia tốc sẽ được nêu ở mục 7.</p>\r\n\r\n<p style="text-align: justify;">- Xác định các tham số mô tả phổ phản ứng gia tốc S, T<sub>B</sub>, T<sub>C</sub>, T<sub>D</sub>, theo bảng 3.2 - mục 3.2.2.2.</p>\r\n\r\n<h3 style="text-align: justify;"><span style="font-size:14px;"><strong>II. Xác định khối lượng tham gia dao động</strong></span></h3>\r\n\r\n<p style="text-align: justify;">Khối lượng tham gia dao động ảnh hưởng đến chu kỳ, dạng dao động, và tải trọng động đất tác dụng lên công trình.<br  />\r\nKhối lượng tham gia dao động được xác định theo các mục 3.2.4 và 4.2.4, phụ thuộc vào tĩnh tải, hoạt tải và loại hoạt tải. Ví dụ công trình có các loại phòng như văn phòng (HTVP) và phòng họp (HTPH), thì công thức xác định khối lượng tham gia dao động sẽ bằng: TT + 0,5*0,3*HTVP + 0,5*0,6*HTPH (sử dụng hệ số 0,5 do các tầng sử dụng độc lập, không phụ thuộc lẫn nhau).</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">III. Xác định chu kỳ và dạng của các dạng dao động</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Trong một số trường hợp, tiêu chuẩn cho phép sử dụng công thức gần đúng để xác định chu kỳ và dạng dao động (xem mục 4.3.3.2.2)<br  />\r\nKhi công trình thỏa mãn tính đều đặn trong mặt bằng, có thể phân tích dao động bằng hai mô hình phẳng theo hai phương chính.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">IV. Xác định phổ phản ứng gia tốc thiết kế</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Giá trị của phổ phản ứng gia tốc thiết kế của một hệ kết cấu S<sub>d</sub>(T<sub>i</sub>), xác định theo mục 3.2.2.5.(4), phụ thuộc vào chu kỳ dao động riêng của hệ, hệ số ứng xử của kết cấu, gia tốc nền thiết kế, và loại nền đất.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">V. Xác định tải trọng động đất</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Khi công trình thỏa mãn các điều kiện cho trong mục 4.3.3.2.1.(2), có thể sử dụng phương pháp phân tích tĩnh lực ngang tương đương, trong mọi trường hợp có thể sử dụng phương pháp phân tích phổ phản ứng dạng dao động.<br  />\r\nNguyên tắc chung để xác định tải trọng động đất là xác định lực cắt đáy F<sub>b</sub> ứng với mỗi dạng dao động và phân phối lên các tầng dưới dạng tải trọng ngang dựa vào dạng của các dạng dao động: F<sub>i</sub> = F<sub>b</sub>*(m<sub>i</sub>.y<sub>i</sub>)/∑(m<sub>j</sub>.y<sub>j</sub>), trong đó m<sub>i</sub> và y<sub>i</sub> lần lượt là khối lượng và tung độ của dạng dao động của tầng thứ i.</p>\r\n\r\n<h3 style="text-align: justify;"><span style="font-size:14px;"><strong>V.1. Phương pháp phân tích tĩnh lực ngang tương đương (mục 4.3.3.2)</strong></span></h3>\r\n\r\n<p style="text-align: justify;">Điều kiện áp dụng phương pháp tĩnh lực ngang tương đương:</p>\r\n\r\n<ul>\r\n	<li style="text-align: justify;">Chu kỳ dao động cơ bản T<sub>1</sub> theo hai hướng chính nhỏ hơn các giá trị sau: 4.T<sub>C</sub> và 2 giây; với T<sub>C</sub> là thông số xác định dạng của phổ phản ứng gia tốc</li>\r\n	<li style="text-align: justify;">Thỏa mãn tính đều đặn theo mặt đứng</li>\r\n</ul>\r\n\r\n<p style="text-align: justify;">Lực cắt đáy được xác định theo công thức:</p>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>F<sub>b</sub> = S<sub>d</sub>(T<sub>1</sub>).m.λ</p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">Trong đó: m là tổng khối lượng của công trình; λ là hệ số điều chỉnh, xét đến khối lượng hữu hiệu của dạng dao động cơ bản đầu tiên, khi T<sub>1</sub> &lt; 2T<sub>C</sub> và nhà có trên 2 tầng thì λ = 0,85; các trường hợp khác λ = 1.</p>\r\n\r\n<h3 style="text-align: justify;"><strong>V.2. Phương pháp phân tích phổ phản ứng dạng dao động</strong></h3>\r\n\r\n<p style="text-align: justify;">Xác định số dạng dao động cần xét đến theo mục 4.3.3.3.1. Có thể xác định theo điều kiện tổng khối lượng hữu hiệu của các dạng dao động được xét chiếm ít nhất 90% tổng khối lượng của kết cấu. Khối lượng hữu hiệu của công trình ứng với dạng dao động thứ i được xác định theo công thức</p>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>M<sub>td,i</sub> = (∑m<sub>j</sub>.y<sub>j</sub>)<sup>2</sup>/(∑m<sub>j</sub>.y<sub>j</sub><sup>2</sup>)</p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">Lực cắt đáy ứng với dạng dao động thứ i được xác định theo công thức: Fb = M<sub>td,i</sub>.S<sub>d</sub>(T<sub>i</sub>)<br  />\r\nKhi các dạng dao động được xem là độc lập (T<sub>j</sub> &lt; 0,9*T<sub>i</sub>), giá trị lớn nhất E<sub>E</sub> của hệ quả tác động động đất có thể lấy bằng: E<sub>E</sub> = √(∑E<sub>Ei</sub><sup>2</sup>), trong đó E<sub>Ei</sub> là giá trị của hệ quả tác động động đất do dạng dao động thứ i gây ra.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">VI. Tổ hợp các hệ quả của các thành phần tác động động đất</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Các thành phần nằm ngang (thành phần theo phương X và thành phần theo phương Y) của tác động động đất phải được xem là tác động đồng thời. Các hệ quả tác động do tổ hợp các thành phần nằm ngang của tác động động đất có thể xác định bằng cách sử dụng hai tổ hợp sau:</p>\r\n\r\n<ul>\r\n	<li style="text-align: justify;">E<sub>Edx</sub> + 0,3*E<sub>Edy</sub></li>\r\n	<li style="text-align: justify;">0,3*E<sub>Edx</sub> + E<sub>Edy</sub></li>\r\n</ul>\r\n\r\n<p style="text-align: justify;">Trong đó, E<sub>Edx</sub> và E<sub>Edy</sub> là các hệ quả tác động do đặt tác động động đất dọc theo trục X và trục Y.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">VII. Tổ hợp tác động động đất với các tác động khác</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Tổ hợp nội lực có tải trọng động đất đối với trường hợp không có ứng suất trước được xác định theo công thức:</p>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>TH = TT + k.HT + ĐĐ</p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">Trong đó k là hệ số tổ hợp của hoạt tải, xác định theo bảng 3.4 của tiêu chuẩn, ví dụ đối với hoạt tải khu vực văn phòng thì k = 0,3; hoạt tải của khu vực hội họp là k = 0,6.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">VIII. Tính toán chuyển vị</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Chuyển vị của một điểm của hệ kết cấu gây ra bởi tác động động đất thiết kế d<sub>s</sub> được xác định dựa trên chuyển vị của cùng điểm đó của hệ kết cấu được xác định bằng phân tích tuyến tính d<sub>c</sub>:</p>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>d<sub>s</sub> = q<sub>d</sub>*d<sub>c</sub></p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">Trong đó q<sub>d</sub> là hệ số ứng xử chuyển vị, giả thiết bằng hệ số ứng xử q trừ khi có các quy định khác.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">Tài liệu tham khảo</span></strong></h2>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>[1]. <strong>TCXDVN 375:2006</strong>. <em>Thiết kế công trình chịu động đất</em>.<br  />\r\n[2]. <strong>Nguyễn Lê Ninh</strong>. <em>Động đất và thiết kế công trình chịu động đất</em>. Nhà xuất bản Xây dựng, Hà Nội, 2009.</p>\r\n</blockquote>\r\n</div>', '', 1, 0, 1, 1, 1, 0),
(15, '<div class="bodytext">\r\n<h2 style="text-align: justify;"><span style="font-size:14px;"><strong>I. Xác định các thông số cơ bản</strong></span></h2>\r\n\r\n<p style="text-align:center"><img alt="dong dat vn" class="img-thumbnail" height="367" src="http://ketcauxaydung.com/uploads/news/2015_01/dong-dat-vn.jpg" width="550" /></p>\r\n\r\n<p style="text-align: justify;">- Xác định đỉnh gia tốc nền tham chiếu: a<sub>gR</sub>, tra bảng phụ lục I. Chú ý rằng bảng tra này đã được quy đổi theo gia tốc trọng trường g, a<sub>gR</sub> bằng giá trị tra bảng nhân với g, ví dụ đối với địa điểm thành phố Hồ Chí Minh - quận 4, có đỉnh gia tốc nền tham chiếu là 0,0847*g.</p>\r\n\r\n<p style="text-align: justify;">- Xác định hệ số tầm quan trọng, γ<sub>I</sub>, tra bảng phụ lục F và G. Ví dụ với nhà cao từ 20 tầng đến 60 tầng có mức độ quan trọng là I, hệ số tầm quan trọng là γ<sub>I</sub> = 1,25</p>\r\n\r\n<p style="text-align: justify;">- Xác định gia tốc nền thiết kế, a<sub>g</sub> = a<sub>gR</sub>*γ<sub>I</sub>; theo mục 3.2.1 của tiêu chuẩn: nếu a<sub>g</sub> &lt; 0,08*g - trường hợp động đất yếu - thì có thể sử dụng các quy trình thiết kế chịu động đất được giảm nhẹ hoặc đơn giản hóa cho một số loại, dạng kết cấu; nếu a<sub>g</sub> &lt; 0,04*g - trường hợp động đất rất yếu - thì không cần phải tuân theo những điều khoản của tiêu chuẩn. Theo [2] thì việc thiết kế kháng chấn cho các công trình xây dựng theo các quy định đề cập tới trong nội dung của tiêu chuẩn chỉ thực hiện chủ yếu cho các công trình xây dựng trong các vùng động đất mạnh có gia tốc nền a<sub>g</sub> &gt; 0,08*g.</p>\r\n\r\n<p style="text-align: justify;">- Xác định loại kết cấu, đối với kết cấu bê tông cốt thép xác định theo mục 5.1.2. Loại kết cấu của công trình có ảnh hưởng đến việc xác định hệ số ứng xử sẽ được đề cập ở mục 5. Một số trường hợp cần xác định bằng cách so sánh lực cắt (phản lực ngang tại liên kết với móng) mà các cấu kiện (các cột và các vách) sẽ gánh khi chịu lực ngang giả thiết.</p>\r\n\r\n<p style="text-align: justify;">- Xác định hệ số ứng xử q, đối với kết cấu bê tông cốt thép xác định theo mục 5.2.2.2, ví dụ hệ khung, nhiều tầng, nhiều nhịp, cấp độ dẻo trung bình (DCM) có: q = 3,0*1,3 = 3,9. Hệ số ứng xử q có thể khác nhau theo hai phương chính tùy thuộc hệ kết cấu.</p>\r\n\r\n<p style="text-align: justify;">- Xác định loại nền đất, theo mục 3.2.1, có thể sử dụng giá trị trung bình của chỉ số SPT của các lớp đất trong chiều sâu 30m. Loại nền đất có vai trò xác định các tham số mô tả phổ phản ứng gia tốc sẽ được nêu ở mục 7.</p>\r\n\r\n<p style="text-align: justify;">- Xác định các tham số mô tả phổ phản ứng gia tốc S, T<sub>B</sub>, T<sub>C</sub>, T<sub>D</sub>, theo bảng 3.2 - mục 3.2.2.2.</p>\r\n\r\n<h3 style="text-align: justify;"><span style="font-size:14px;"><strong>II. Xác định khối lượng tham gia dao động</strong></span></h3>\r\n\r\n<p style="text-align: justify;">Khối lượng tham gia dao động ảnh hưởng đến chu kỳ, dạng dao động, và tải trọng động đất tác dụng lên công trình.<br  />\r\nKhối lượng tham gia dao động được xác định theo các mục 3.2.4 và 4.2.4, phụ thuộc vào tĩnh tải, hoạt tải và loại hoạt tải. Ví dụ công trình có các loại phòng như văn phòng (HTVP) và phòng họp (HTPH), thì công thức xác định khối lượng tham gia dao động sẽ bằng: TT + 0,5*0,3*HTVP + 0,5*0,6*HTPH (sử dụng hệ số 0,5 do các tầng sử dụng độc lập, không phụ thuộc lẫn nhau).</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">III. Xác định chu kỳ và dạng của các dạng dao động</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Trong một số trường hợp, tiêu chuẩn cho phép sử dụng công thức gần đúng để xác định chu kỳ và dạng dao động (xem mục 4.3.3.2.2)<br  />\r\nKhi công trình thỏa mãn tính đều đặn trong mặt bằng, có thể phân tích dao động bằng hai mô hình phẳng theo hai phương chính.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">IV. Xác định phổ phản ứng gia tốc thiết kế</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Giá trị của phổ phản ứng gia tốc thiết kế của một hệ kết cấu S<sub>d</sub>(T<sub>i</sub>), xác định theo mục 3.2.2.5.(4), phụ thuộc vào chu kỳ dao động riêng của hệ, hệ số ứng xử của kết cấu, gia tốc nền thiết kế, và loại nền đất.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">V. Xác định tải trọng động đất</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Khi công trình thỏa mãn các điều kiện cho trong mục 4.3.3.2.1.(2), có thể sử dụng phương pháp phân tích tĩnh lực ngang tương đương, trong mọi trường hợp có thể sử dụng phương pháp phân tích phổ phản ứng dạng dao động.<br  />\r\nNguyên tắc chung để xác định tải trọng động đất là xác định lực cắt đáy F<sub>b</sub> ứng với mỗi dạng dao động và phân phối lên các tầng dưới dạng tải trọng ngang dựa vào dạng của các dạng dao động: F<sub>i</sub> = F<sub>b</sub>*(m<sub>i</sub>.y<sub>i</sub>)/∑(m<sub>j</sub>.y<sub>j</sub>), trong đó m<sub>i</sub> và y<sub>i</sub> lần lượt là khối lượng và tung độ của dạng dao động của tầng thứ i.</p>\r\n\r\n<h3 style="text-align: justify;"><span style="font-size:14px;"><strong>V.1. Phương pháp phân tích tĩnh lực ngang tương đương (mục 4.3.3.2)</strong></span></h3>\r\n\r\n<p style="text-align: justify;">Điều kiện áp dụng phương pháp tĩnh lực ngang tương đương:</p>\r\n\r\n<ul>\r\n	<li style="text-align: justify;">Chu kỳ dao động cơ bản T<sub>1</sub> theo hai hướng chính nhỏ hơn các giá trị sau: 4.T<sub>C</sub> và 2 giây; với T<sub>C</sub> là thông số xác định dạng của phổ phản ứng gia tốc</li>\r\n	<li style="text-align: justify;">Thỏa mãn tính đều đặn theo mặt đứng</li>\r\n</ul>\r\n\r\n<p style="text-align: justify;">Lực cắt đáy được xác định theo công thức:</p>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>F<sub>b</sub> = S<sub>d</sub>(T<sub>1</sub>).m.λ</p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">Trong đó: m là tổng khối lượng của công trình; λ là hệ số điều chỉnh, xét đến khối lượng hữu hiệu của dạng dao động cơ bản đầu tiên, khi T<sub>1</sub> &lt; 2T<sub>C</sub> và nhà có trên 2 tầng thì λ = 0,85; các trường hợp khác λ = 1.</p>\r\n\r\n<h3 style="text-align: justify;"><strong>V.2. Phương pháp phân tích phổ phản ứng dạng dao động</strong></h3>\r\n\r\n<p style="text-align: justify;">Xác định số dạng dao động cần xét đến theo mục 4.3.3.3.1. Có thể xác định theo điều kiện tổng khối lượng hữu hiệu của các dạng dao động được xét chiếm ít nhất 90% tổng khối lượng của kết cấu. Khối lượng hữu hiệu của công trình ứng với dạng dao động thứ i được xác định theo công thức</p>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>M<sub>td,i</sub> = (∑m<sub>j</sub>.y<sub>j</sub>)<sup>2</sup>/(∑m<sub>j</sub>.y<sub>j</sub><sup>2</sup>)</p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">Lực cắt đáy ứng với dạng dao động thứ i được xác định theo công thức: Fb = M<sub>td,i</sub>.S<sub>d</sub>(T<sub>i</sub>)<br  />\r\nKhi các dạng dao động được xem là độc lập (T<sub>j</sub> &lt; 0,9*T<sub>i</sub>), giá trị lớn nhất E<sub>E</sub> của hệ quả tác động động đất có thể lấy bằng: E<sub>E</sub> = √(∑E<sub>Ei</sub><sup>2</sup>), trong đó E<sub>Ei</sub> là giá trị của hệ quả tác động động đất do dạng dao động thứ i gây ra.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">VI. Tổ hợp các hệ quả của các thành phần tác động động đất</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Các thành phần nằm ngang (thành phần theo phương X và thành phần theo phương Y) của tác động động đất phải được xem là tác động đồng thời. Các hệ quả tác động do tổ hợp các thành phần nằm ngang của tác động động đất có thể xác định bằng cách sử dụng hai tổ hợp sau:</p>\r\n\r\n<ul>\r\n	<li style="text-align: justify;">E<sub>Edx</sub> + 0,3*E<sub>Edy</sub></li>\r\n	<li style="text-align: justify;">0,3*E<sub>Edx</sub> + E<sub>Edy</sub></li>\r\n</ul>\r\n\r\n<p style="text-align: justify;">Trong đó, E<sub>Edx</sub> và E<sub>Edy</sub> là các hệ quả tác động do đặt tác động động đất dọc theo trục X và trục Y.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">VII. Tổ hợp tác động động đất với các tác động khác</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Tổ hợp nội lực có tải trọng động đất đối với trường hợp không có ứng suất trước được xác định theo công thức:</p>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>TH = TT + k.HT + ĐĐ</p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">Trong đó k là hệ số tổ hợp của hoạt tải, xác định theo bảng 3.4 của tiêu chuẩn, ví dụ đối với hoạt tải khu vực văn phòng thì k = 0,3; hoạt tải của khu vực hội họp là k = 0,6.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">VIII. Tính toán chuyển vị</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Chuyển vị của một điểm của hệ kết cấu gây ra bởi tác động động đất thiết kế d<sub>s</sub> được xác định dựa trên chuyển vị của cùng điểm đó của hệ kết cấu được xác định bằng phân tích tuyến tính d<sub>c</sub>:</p>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>d<sub>s</sub> = q<sub>d</sub>*d<sub>c</sub></p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">Trong đó q<sub>d</sub> là hệ số ứng xử chuyển vị, giả thiết bằng hệ số ứng xử q trừ khi có các quy định khác.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">Tài liệu tham khảo</span></strong></h2>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>[1]. <strong>TCXDVN 375:2006</strong>. <em>Thiết kế công trình chịu động đất</em>.<br  />\r\n[2]. <strong>Nguyễn Lê Ninh</strong>. <em>Động đất và thiết kế công trình chịu động đất</em>. Nhà xuất bản Xây dựng, Hà Nội, 2009.</p>\r\n</blockquote>\r\n</div>', '', 1, 0, 0, 0, 0, 0),
(14, '<div class="bodytext">\r\n<h2 style="text-align: justify;"><span style="font-size:14px;"><strong>I. Xác định các thông số cơ bản</strong></span></h2>\r\n\r\n<p style="text-align:center"><img alt="dong dat vn" class="img-thumbnail" height="367" src="http://ketcauxaydung.com/uploads/news/2015_01/dong-dat-vn.jpg" width="550" /></p>\r\n\r\n<p style="text-align: justify;">- Xác định đỉnh gia tốc nền tham chiếu: a<sub>gR</sub>, tra bảng phụ lục I. Chú ý rằng bảng tra này đã được quy đổi theo gia tốc trọng trường g, a<sub>gR</sub> bằng giá trị tra bảng nhân với g, ví dụ đối với địa điểm thành phố Hồ Chí Minh - quận 4, có đỉnh gia tốc nền tham chiếu là 0,0847*g.</p>\r\n\r\n<p style="text-align: justify;">- Xác định hệ số tầm quan trọng, γ<sub>I</sub>, tra bảng phụ lục F và G. Ví dụ với nhà cao từ 20 tầng đến 60 tầng có mức độ quan trọng là I, hệ số tầm quan trọng là γ<sub>I</sub> = 1,25</p>\r\n\r\n<p style="text-align: justify;">- Xác định gia tốc nền thiết kế, a<sub>g</sub> = a<sub>gR</sub>*γ<sub>I</sub>; theo mục 3.2.1 của tiêu chuẩn: nếu a<sub>g</sub> &lt; 0,08*g - trường hợp động đất yếu - thì có thể sử dụng các quy trình thiết kế chịu động đất được giảm nhẹ hoặc đơn giản hóa cho một số loại, dạng kết cấu; nếu a<sub>g</sub> &lt; 0,04*g - trường hợp động đất rất yếu - thì không cần phải tuân theo những điều khoản của tiêu chuẩn. Theo [2] thì việc thiết kế kháng chấn cho các công trình xây dựng theo các quy định đề cập tới trong nội dung của tiêu chuẩn chỉ thực hiện chủ yếu cho các công trình xây dựng trong các vùng động đất mạnh có gia tốc nền a<sub>g</sub> &gt; 0,08*g.</p>\r\n\r\n<p style="text-align: justify;">- Xác định loại kết cấu, đối với kết cấu bê tông cốt thép xác định theo mục 5.1.2. Loại kết cấu của công trình có ảnh hưởng đến việc xác định hệ số ứng xử sẽ được đề cập ở mục 5. Một số trường hợp cần xác định bằng cách so sánh lực cắt (phản lực ngang tại liên kết với móng) mà các cấu kiện (các cột và các vách) sẽ gánh khi chịu lực ngang giả thiết.</p>\r\n\r\n<p style="text-align: justify;">- Xác định hệ số ứng xử q, đối với kết cấu bê tông cốt thép xác định theo mục 5.2.2.2, ví dụ hệ khung, nhiều tầng, nhiều nhịp, cấp độ dẻo trung bình (DCM) có: q = 3,0*1,3 = 3,9. Hệ số ứng xử q có thể khác nhau theo hai phương chính tùy thuộc hệ kết cấu.</p>\r\n\r\n<p style="text-align: justify;">- Xác định loại nền đất, theo mục 3.2.1, có thể sử dụng giá trị trung bình của chỉ số SPT của các lớp đất trong chiều sâu 30m. Loại nền đất có vai trò xác định các tham số mô tả phổ phản ứng gia tốc sẽ được nêu ở mục 7.</p>\r\n\r\n<p style="text-align: justify;">- Xác định các tham số mô tả phổ phản ứng gia tốc S, T<sub>B</sub>, T<sub>C</sub>, T<sub>D</sub>, theo bảng 3.2 - mục 3.2.2.2.</p>\r\n\r\n<h3 style="text-align: justify;"><span style="font-size:14px;"><strong>II. Xác định khối lượng tham gia dao động</strong></span></h3>\r\n\r\n<p style="text-align: justify;">Khối lượng tham gia dao động ảnh hưởng đến chu kỳ, dạng dao động, và tải trọng động đất tác dụng lên công trình.<br  />\r\nKhối lượng tham gia dao động được xác định theo các mục 3.2.4 và 4.2.4, phụ thuộc vào tĩnh tải, hoạt tải và loại hoạt tải. Ví dụ công trình có các loại phòng như văn phòng (HTVP) và phòng họp (HTPH), thì công thức xác định khối lượng tham gia dao động sẽ bằng: TT + 0,5*0,3*HTVP + 0,5*0,6*HTPH (sử dụng hệ số 0,5 do các tầng sử dụng độc lập, không phụ thuộc lẫn nhau).</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">III. Xác định chu kỳ và dạng của các dạng dao động</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Trong một số trường hợp, tiêu chuẩn cho phép sử dụng công thức gần đúng để xác định chu kỳ và dạng dao động (xem mục 4.3.3.2.2)<br  />\r\nKhi công trình thỏa mãn tính đều đặn trong mặt bằng, có thể phân tích dao động bằng hai mô hình phẳng theo hai phương chính.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">IV. Xác định phổ phản ứng gia tốc thiết kế</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Giá trị của phổ phản ứng gia tốc thiết kế của một hệ kết cấu S<sub>d</sub>(T<sub>i</sub>), xác định theo mục 3.2.2.5.(4), phụ thuộc vào chu kỳ dao động riêng của hệ, hệ số ứng xử của kết cấu, gia tốc nền thiết kế, và loại nền đất.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">V. Xác định tải trọng động đất</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Khi công trình thỏa mãn các điều kiện cho trong mục 4.3.3.2.1.(2), có thể sử dụng phương pháp phân tích tĩnh lực ngang tương đương, trong mọi trường hợp có thể sử dụng phương pháp phân tích phổ phản ứng dạng dao động.<br  />\r\nNguyên tắc chung để xác định tải trọng động đất là xác định lực cắt đáy F<sub>b</sub> ứng với mỗi dạng dao động và phân phối lên các tầng dưới dạng tải trọng ngang dựa vào dạng của các dạng dao động: F<sub>i</sub> = F<sub>b</sub>*(m<sub>i</sub>.y<sub>i</sub>)/∑(m<sub>j</sub>.y<sub>j</sub>), trong đó m<sub>i</sub> và y<sub>i</sub> lần lượt là khối lượng và tung độ của dạng dao động của tầng thứ i.</p>\r\n\r\n<h3 style="text-align: justify;"><span style="font-size:14px;"><strong>V.1. Phương pháp phân tích tĩnh lực ngang tương đương (mục 4.3.3.2)</strong></span></h3>\r\n\r\n<p style="text-align: justify;">Điều kiện áp dụng phương pháp tĩnh lực ngang tương đương:</p>\r\n\r\n<ul>\r\n	<li style="text-align: justify;">Chu kỳ dao động cơ bản T<sub>1</sub> theo hai hướng chính nhỏ hơn các giá trị sau: 4.T<sub>C</sub> và 2 giây; với T<sub>C</sub> là thông số xác định dạng của phổ phản ứng gia tốc</li>\r\n	<li style="text-align: justify;">Thỏa mãn tính đều đặn theo mặt đứng</li>\r\n</ul>\r\n\r\n<p style="text-align: justify;">Lực cắt đáy được xác định theo công thức:</p>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>F<sub>b</sub> = S<sub>d</sub>(T<sub>1</sub>).m.λ</p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">Trong đó: m là tổng khối lượng của công trình; λ là hệ số điều chỉnh, xét đến khối lượng hữu hiệu của dạng dao động cơ bản đầu tiên, khi T<sub>1</sub> &lt; 2T<sub>C</sub> và nhà có trên 2 tầng thì λ = 0,85; các trường hợp khác λ = 1.</p>\r\n\r\n<h3 style="text-align: justify;"><strong>V.2. Phương pháp phân tích phổ phản ứng dạng dao động</strong></h3>\r\n\r\n<p style="text-align: justify;">Xác định số dạng dao động cần xét đến theo mục 4.3.3.3.1. Có thể xác định theo điều kiện tổng khối lượng hữu hiệu của các dạng dao động được xét chiếm ít nhất 90% tổng khối lượng của kết cấu. Khối lượng hữu hiệu của công trình ứng với dạng dao động thứ i được xác định theo công thức</p>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>M<sub>td,i</sub> = (∑m<sub>j</sub>.y<sub>j</sub>)<sup>2</sup>/(∑m<sub>j</sub>.y<sub>j</sub><sup>2</sup>)</p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">Lực cắt đáy ứng với dạng dao động thứ i được xác định theo công thức: Fb = M<sub>td,i</sub>.S<sub>d</sub>(T<sub>i</sub>)<br  />\r\nKhi các dạng dao động được xem là độc lập (T<sub>j</sub> &lt; 0,9*T<sub>i</sub>), giá trị lớn nhất E<sub>E</sub> của hệ quả tác động động đất có thể lấy bằng: E<sub>E</sub> = √(∑E<sub>Ei</sub><sup>2</sup>), trong đó E<sub>Ei</sub> là giá trị của hệ quả tác động động đất do dạng dao động thứ i gây ra.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">VI. Tổ hợp các hệ quả của các thành phần tác động động đất</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Các thành phần nằm ngang (thành phần theo phương X và thành phần theo phương Y) của tác động động đất phải được xem là tác động đồng thời. Các hệ quả tác động do tổ hợp các thành phần nằm ngang của tác động động đất có thể xác định bằng cách sử dụng hai tổ hợp sau:</p>\r\n\r\n<ul>\r\n	<li style="text-align: justify;">E<sub>Edx</sub> + 0,3*E<sub>Edy</sub></li>\r\n	<li style="text-align: justify;">0,3*E<sub>Edx</sub> + E<sub>Edy</sub></li>\r\n</ul>\r\n\r\n<p style="text-align: justify;">Trong đó, E<sub>Edx</sub> và E<sub>Edy</sub> là các hệ quả tác động do đặt tác động động đất dọc theo trục X và trục Y.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">VII. Tổ hợp tác động động đất với các tác động khác</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Tổ hợp nội lực có tải trọng động đất đối với trường hợp không có ứng suất trước được xác định theo công thức:</p>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>TH = TT + k.HT + ĐĐ</p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">Trong đó k là hệ số tổ hợp của hoạt tải, xác định theo bảng 3.4 của tiêu chuẩn, ví dụ đối với hoạt tải khu vực văn phòng thì k = 0,3; hoạt tải của khu vực hội họp là k = 0,6.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">VIII. Tính toán chuyển vị</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Chuyển vị của một điểm của hệ kết cấu gây ra bởi tác động động đất thiết kế d<sub>s</sub> được xác định dựa trên chuyển vị của cùng điểm đó của hệ kết cấu được xác định bằng phân tích tuyến tính d<sub>c</sub>:</p>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>d<sub>s</sub> = q<sub>d</sub>*d<sub>c</sub></p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">Trong đó q<sub>d</sub> là hệ số ứng xử chuyển vị, giả thiết bằng hệ số ứng xử q trừ khi có các quy định khác.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">Tài liệu tham khảo</span></strong></h2>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>[1]. <strong>TCXDVN 375:2006</strong>. <em>Thiết kế công trình chịu động đất</em>.<br  />\r\n[2]. <strong>Nguyễn Lê Ninh</strong>. <em>Động đất và thiết kế công trình chịu động đất</em>. Nhà xuất bản Xây dựng, Hà Nội, 2009.</p>\r\n</blockquote>\r\n</div>', '', 1, 0, 0, 0, 0, 0),
(11, '<div class="bodytext">\r\n<h2 style="text-align: justify;"><span style="font-size:14px;"><strong>I. Xác định các thông số cơ bản</strong></span></h2>\r\n\r\n<p style="text-align:center"><img alt="dong dat vn" class="img-thumbnail" height="367" src="http://ketcauxaydung.com/uploads/news/2015_01/dong-dat-vn.jpg" width="550" /></p>\r\n\r\n<p style="text-align: justify;">- Xác định đỉnh gia tốc nền tham chiếu: a<sub>gR</sub>, tra bảng phụ lục I. Chú ý rằng bảng tra này đã được quy đổi theo gia tốc trọng trường g, a<sub>gR</sub> bằng giá trị tra bảng nhân với g, ví dụ đối với địa điểm thành phố Hồ Chí Minh - quận 4, có đỉnh gia tốc nền tham chiếu là 0,0847*g.</p>\r\n\r\n<p style="text-align: justify;">- Xác định hệ số tầm quan trọng, γ<sub>I</sub>, tra bảng phụ lục F và G. Ví dụ với nhà cao từ 20 tầng đến 60 tầng có mức độ quan trọng là I, hệ số tầm quan trọng là γ<sub>I</sub> = 1,25</p>\r\n\r\n<p style="text-align: justify;">- Xác định gia tốc nền thiết kế, a<sub>g</sub> = a<sub>gR</sub>*γ<sub>I</sub>; theo mục 3.2.1 của tiêu chuẩn: nếu a<sub>g</sub> &lt; 0,08*g - trường hợp động đất yếu - thì có thể sử dụng các quy trình thiết kế chịu động đất được giảm nhẹ hoặc đơn giản hóa cho một số loại, dạng kết cấu; nếu a<sub>g</sub> &lt; 0,04*g - trường hợp động đất rất yếu - thì không cần phải tuân theo những điều khoản của tiêu chuẩn. Theo [2] thì việc thiết kế kháng chấn cho các công trình xây dựng theo các quy định đề cập tới trong nội dung của tiêu chuẩn chỉ thực hiện chủ yếu cho các công trình xây dựng trong các vùng động đất mạnh có gia tốc nền a<sub>g</sub> &gt; 0,08*g.</p>\r\n\r\n<p style="text-align: justify;">- Xác định loại kết cấu, đối với kết cấu bê tông cốt thép xác định theo mục 5.1.2. Loại kết cấu của công trình có ảnh hưởng đến việc xác định hệ số ứng xử sẽ được đề cập ở mục 5. Một số trường hợp cần xác định bằng cách so sánh lực cắt (phản lực ngang tại liên kết với móng) mà các cấu kiện (các cột và các vách) sẽ gánh khi chịu lực ngang giả thiết.</p>\r\n\r\n<p style="text-align: justify;">- Xác định hệ số ứng xử q, đối với kết cấu bê tông cốt thép xác định theo mục 5.2.2.2, ví dụ hệ khung, nhiều tầng, nhiều nhịp, cấp độ dẻo trung bình (DCM) có: q = 3,0*1,3 = 3,9. Hệ số ứng xử q có thể khác nhau theo hai phương chính tùy thuộc hệ kết cấu.</p>\r\n\r\n<p style="text-align: justify;">- Xác định loại nền đất, theo mục 3.2.1, có thể sử dụng giá trị trung bình của chỉ số SPT của các lớp đất trong chiều sâu 30m. Loại nền đất có vai trò xác định các tham số mô tả phổ phản ứng gia tốc sẽ được nêu ở mục 7.</p>\r\n\r\n<p style="text-align: justify;">- Xác định các tham số mô tả phổ phản ứng gia tốc S, T<sub>B</sub>, T<sub>C</sub>, T<sub>D</sub>, theo bảng 3.2 - mục 3.2.2.2.</p>\r\n\r\n<h3 style="text-align: justify;"><span style="font-size:14px;"><strong>II. Xác định khối lượng tham gia dao động</strong></span></h3>\r\n\r\n<p style="text-align: justify;">Khối lượng tham gia dao động ảnh hưởng đến chu kỳ, dạng dao động, và tải trọng động đất tác dụng lên công trình.<br  />\r\nKhối lượng tham gia dao động được xác định theo các mục 3.2.4 và 4.2.4, phụ thuộc vào tĩnh tải, hoạt tải và loại hoạt tải. Ví dụ công trình có các loại phòng như văn phòng (HTVP) và phòng họp (HTPH), thì công thức xác định khối lượng tham gia dao động sẽ bằng: TT + 0,5*0,3*HTVP + 0,5*0,6*HTPH (sử dụng hệ số 0,5 do các tầng sử dụng độc lập, không phụ thuộc lẫn nhau).</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">III. Xác định chu kỳ và dạng của các dạng dao động</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Trong một số trường hợp, tiêu chuẩn cho phép sử dụng công thức gần đúng để xác định chu kỳ và dạng dao động (xem mục 4.3.3.2.2)<br  />\r\nKhi công trình thỏa mãn tính đều đặn trong mặt bằng, có thể phân tích dao động bằng hai mô hình phẳng theo hai phương chính.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">IV. Xác định phổ phản ứng gia tốc thiết kế</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Giá trị của phổ phản ứng gia tốc thiết kế của một hệ kết cấu S<sub>d</sub>(T<sub>i</sub>), xác định theo mục 3.2.2.5.(4), phụ thuộc vào chu kỳ dao động riêng của hệ, hệ số ứng xử của kết cấu, gia tốc nền thiết kế, và loại nền đất.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">V. Xác định tải trọng động đất</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Khi công trình thỏa mãn các điều kiện cho trong mục 4.3.3.2.1.(2), có thể sử dụng phương pháp phân tích tĩnh lực ngang tương đương, trong mọi trường hợp có thể sử dụng phương pháp phân tích phổ phản ứng dạng dao động.<br  />\r\nNguyên tắc chung để xác định tải trọng động đất là xác định lực cắt đáy F<sub>b</sub> ứng với mỗi dạng dao động và phân phối lên các tầng dưới dạng tải trọng ngang dựa vào dạng của các dạng dao động: F<sub>i</sub> = F<sub>b</sub>*(m<sub>i</sub>.y<sub>i</sub>)/∑(m<sub>j</sub>.y<sub>j</sub>), trong đó m<sub>i</sub> và y<sub>i</sub> lần lượt là khối lượng và tung độ của dạng dao động của tầng thứ i.</p>\r\n\r\n<h3 style="text-align: justify;"><span style="font-size:14px;"><strong>V.1. Phương pháp phân tích tĩnh lực ngang tương đương (mục 4.3.3.2)</strong></span></h3>\r\n\r\n<p style="text-align: justify;">Điều kiện áp dụng phương pháp tĩnh lực ngang tương đương:</p>\r\n\r\n<ul>\r\n	<li style="text-align: justify;">Chu kỳ dao động cơ bản T<sub>1</sub> theo hai hướng chính nhỏ hơn các giá trị sau: 4.T<sub>C</sub> và 2 giây; với T<sub>C</sub> là thông số xác định dạng của phổ phản ứng gia tốc</li>\r\n	<li style="text-align: justify;">Thỏa mãn tính đều đặn theo mặt đứng</li>\r\n</ul>\r\n\r\n<p style="text-align: justify;">Lực cắt đáy được xác định theo công thức:</p>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>F<sub>b</sub> = S<sub>d</sub>(T<sub>1</sub>).m.λ</p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">Trong đó: m là tổng khối lượng của công trình; λ là hệ số điều chỉnh, xét đến khối lượng hữu hiệu của dạng dao động cơ bản đầu tiên, khi T<sub>1</sub> &lt; 2T<sub>C</sub> và nhà có trên 2 tầng thì λ = 0,85; các trường hợp khác λ = 1.</p>\r\n\r\n<h3 style="text-align: justify;"><strong>V.2. Phương pháp phân tích phổ phản ứng dạng dao động</strong></h3>\r\n\r\n<p style="text-align: justify;">Xác định số dạng dao động cần xét đến theo mục 4.3.3.3.1. Có thể xác định theo điều kiện tổng khối lượng hữu hiệu của các dạng dao động được xét chiếm ít nhất 90% tổng khối lượng của kết cấu. Khối lượng hữu hiệu của công trình ứng với dạng dao động thứ i được xác định theo công thức</p>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>M<sub>td,i</sub> = (∑m<sub>j</sub>.y<sub>j</sub>)<sup>2</sup>/(∑m<sub>j</sub>.y<sub>j</sub><sup>2</sup>)</p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">Lực cắt đáy ứng với dạng dao động thứ i được xác định theo công thức: Fb = M<sub>td,i</sub>.S<sub>d</sub>(T<sub>i</sub>)<br  />\r\nKhi các dạng dao động được xem là độc lập (T<sub>j</sub> &lt; 0,9*T<sub>i</sub>), giá trị lớn nhất E<sub>E</sub> của hệ quả tác động động đất có thể lấy bằng: E<sub>E</sub> = √(∑E<sub>Ei</sub><sup>2</sup>), trong đó E<sub>Ei</sub> là giá trị của hệ quả tác động động đất do dạng dao động thứ i gây ra.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">VI. Tổ hợp các hệ quả của các thành phần tác động động đất</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Các thành phần nằm ngang (thành phần theo phương X và thành phần theo phương Y) của tác động động đất phải được xem là tác động đồng thời. Các hệ quả tác động do tổ hợp các thành phần nằm ngang của tác động động đất có thể xác định bằng cách sử dụng hai tổ hợp sau:</p>\r\n\r\n<ul>\r\n	<li style="text-align: justify;">E<sub>Edx</sub> + 0,3*E<sub>Edy</sub></li>\r\n	<li style="text-align: justify;">0,3*E<sub>Edx</sub> + E<sub>Edy</sub></li>\r\n</ul>\r\n\r\n<p style="text-align: justify;">Trong đó, E<sub>Edx</sub> và E<sub>Edy</sub> là các hệ quả tác động do đặt tác động động đất dọc theo trục X và trục Y.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">VII. Tổ hợp tác động động đất với các tác động khác</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Tổ hợp nội lực có tải trọng động đất đối với trường hợp không có ứng suất trước được xác định theo công thức:</p>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>TH = TT + k.HT + ĐĐ</p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">Trong đó k là hệ số tổ hợp của hoạt tải, xác định theo bảng 3.4 của tiêu chuẩn, ví dụ đối với hoạt tải khu vực văn phòng thì k = 0,3; hoạt tải của khu vực hội họp là k = 0,6.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">VIII. Tính toán chuyển vị</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Chuyển vị của một điểm của hệ kết cấu gây ra bởi tác động động đất thiết kế d<sub>s</sub> được xác định dựa trên chuyển vị của cùng điểm đó của hệ kết cấu được xác định bằng phân tích tuyến tính d<sub>c</sub>:</p>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>d<sub>s</sub> = q<sub>d</sub>*d<sub>c</sub></p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">Trong đó q<sub>d</sub> là hệ số ứng xử chuyển vị, giả thiết bằng hệ số ứng xử q trừ khi có các quy định khác.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">Tài liệu tham khảo</span></strong></h2>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>[1]. <strong>TCXDVN 375:2006</strong>. <em>Thiết kế công trình chịu động đất</em>.<br  />\r\n[2]. <strong>Nguyễn Lê Ninh</strong>. <em>Động đất và thiết kế công trình chịu động đất</em>. Nhà xuất bản Xây dựng, Hà Nội, 2009.</p>\r\n</blockquote>\r\n</div>', '', 1, 0, 1, 1, 1, 0);
INSERT INTO `kcxd_vi_news_bodyhtml_1` (`id`, `bodyhtml`, `sourcetext`, `imgposition`, `copyright`, `allowed_send`, `allowed_print`, `allowed_save`, `gid`) VALUES
(12, '<div class="bodytext">\r\n<h2 style="text-align: justify;"><span style="font-size:14px;"><strong>I. Xác định các thông số cơ bản</strong></span></h2>\r\n\r\n<p style="text-align:center"><img alt="dong dat vn" class="img-thumbnail" height="367" src="http://ketcauxaydung.com/uploads/news/2015_01/dong-dat-vn.jpg" width="550" /></p>\r\n\r\n<p style="text-align: justify;">- Xác định đỉnh gia tốc nền tham chiếu: a<sub>gR</sub>, tra bảng phụ lục I. Chú ý rằng bảng tra này đã được quy đổi theo gia tốc trọng trường g, a<sub>gR</sub> bằng giá trị tra bảng nhân với g, ví dụ đối với địa điểm thành phố Hồ Chí Minh - quận 4, có đỉnh gia tốc nền tham chiếu là 0,0847*g.</p>\r\n\r\n<p style="text-align: justify;">- Xác định hệ số tầm quan trọng, γ<sub>I</sub>, tra bảng phụ lục F và G. Ví dụ với nhà cao từ 20 tầng đến 60 tầng có mức độ quan trọng là I, hệ số tầm quan trọng là γ<sub>I</sub> = 1,25</p>\r\n\r\n<p style="text-align: justify;">- Xác định gia tốc nền thiết kế, a<sub>g</sub> = a<sub>gR</sub>*γ<sub>I</sub>; theo mục 3.2.1 của tiêu chuẩn: nếu a<sub>g</sub> &lt; 0,08*g - trường hợp động đất yếu - thì có thể sử dụng các quy trình thiết kế chịu động đất được giảm nhẹ hoặc đơn giản hóa cho một số loại, dạng kết cấu; nếu a<sub>g</sub> &lt; 0,04*g - trường hợp động đất rất yếu - thì không cần phải tuân theo những điều khoản của tiêu chuẩn. Theo [2] thì việc thiết kế kháng chấn cho các công trình xây dựng theo các quy định đề cập tới trong nội dung của tiêu chuẩn chỉ thực hiện chủ yếu cho các công trình xây dựng trong các vùng động đất mạnh có gia tốc nền a<sub>g</sub> &gt; 0,08*g.</p>\r\n\r\n<p style="text-align: justify;">- Xác định loại kết cấu, đối với kết cấu bê tông cốt thép xác định theo mục 5.1.2. Loại kết cấu của công trình có ảnh hưởng đến việc xác định hệ số ứng xử sẽ được đề cập ở mục 5. Một số trường hợp cần xác định bằng cách so sánh lực cắt (phản lực ngang tại liên kết với móng) mà các cấu kiện (các cột và các vách) sẽ gánh khi chịu lực ngang giả thiết.</p>\r\n\r\n<p style="text-align: justify;">- Xác định hệ số ứng xử q, đối với kết cấu bê tông cốt thép xác định theo mục 5.2.2.2, ví dụ hệ khung, nhiều tầng, nhiều nhịp, cấp độ dẻo trung bình (DCM) có: q = 3,0*1,3 = 3,9. Hệ số ứng xử q có thể khác nhau theo hai phương chính tùy thuộc hệ kết cấu.</p>\r\n\r\n<p style="text-align: justify;">- Xác định loại nền đất, theo mục 3.2.1, có thể sử dụng giá trị trung bình của chỉ số SPT của các lớp đất trong chiều sâu 30m. Loại nền đất có vai trò xác định các tham số mô tả phổ phản ứng gia tốc sẽ được nêu ở mục 7.</p>\r\n\r\n<p style="text-align: justify;">- Xác định các tham số mô tả phổ phản ứng gia tốc S, T<sub>B</sub>, T<sub>C</sub>, T<sub>D</sub>, theo bảng 3.2 - mục 3.2.2.2.</p>\r\n\r\n<h3 style="text-align: justify;"><span style="font-size:14px;"><strong>II. Xác định khối lượng tham gia dao động</strong></span></h3>\r\n\r\n<p style="text-align: justify;">Khối lượng tham gia dao động ảnh hưởng đến chu kỳ, dạng dao động, và tải trọng động đất tác dụng lên công trình.<br  />\r\nKhối lượng tham gia dao động được xác định theo các mục 3.2.4 và 4.2.4, phụ thuộc vào tĩnh tải, hoạt tải và loại hoạt tải. Ví dụ công trình có các loại phòng như văn phòng (HTVP) và phòng họp (HTPH), thì công thức xác định khối lượng tham gia dao động sẽ bằng: TT + 0,5*0,3*HTVP + 0,5*0,6*HTPH (sử dụng hệ số 0,5 do các tầng sử dụng độc lập, không phụ thuộc lẫn nhau).</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">III. Xác định chu kỳ và dạng của các dạng dao động</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Trong một số trường hợp, tiêu chuẩn cho phép sử dụng công thức gần đúng để xác định chu kỳ và dạng dao động (xem mục 4.3.3.2.2)<br  />\r\nKhi công trình thỏa mãn tính đều đặn trong mặt bằng, có thể phân tích dao động bằng hai mô hình phẳng theo hai phương chính.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">IV. Xác định phổ phản ứng gia tốc thiết kế</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Giá trị của phổ phản ứng gia tốc thiết kế của một hệ kết cấu S<sub>d</sub>(T<sub>i</sub>), xác định theo mục 3.2.2.5.(4), phụ thuộc vào chu kỳ dao động riêng của hệ, hệ số ứng xử của kết cấu, gia tốc nền thiết kế, và loại nền đất.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">V. Xác định tải trọng động đất</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Khi công trình thỏa mãn các điều kiện cho trong mục 4.3.3.2.1.(2), có thể sử dụng phương pháp phân tích tĩnh lực ngang tương đương, trong mọi trường hợp có thể sử dụng phương pháp phân tích phổ phản ứng dạng dao động.<br  />\r\nNguyên tắc chung để xác định tải trọng động đất là xác định lực cắt đáy F<sub>b</sub> ứng với mỗi dạng dao động và phân phối lên các tầng dưới dạng tải trọng ngang dựa vào dạng của các dạng dao động: F<sub>i</sub> = F<sub>b</sub>*(m<sub>i</sub>.y<sub>i</sub>)/∑(m<sub>j</sub>.y<sub>j</sub>), trong đó m<sub>i</sub> và y<sub>i</sub> lần lượt là khối lượng và tung độ của dạng dao động của tầng thứ i.</p>\r\n\r\n<h3 style="text-align: justify;"><span style="font-size:14px;"><strong>V.1. Phương pháp phân tích tĩnh lực ngang tương đương (mục 4.3.3.2)</strong></span></h3>\r\n\r\n<p style="text-align: justify;">Điều kiện áp dụng phương pháp tĩnh lực ngang tương đương:</p>\r\n\r\n<ul>\r\n	<li style="text-align: justify;">Chu kỳ dao động cơ bản T<sub>1</sub> theo hai hướng chính nhỏ hơn các giá trị sau: 4.T<sub>C</sub> và 2 giây; với T<sub>C</sub> là thông số xác định dạng của phổ phản ứng gia tốc</li>\r\n	<li style="text-align: justify;">Thỏa mãn tính đều đặn theo mặt đứng</li>\r\n</ul>\r\n\r\n<p style="text-align: justify;">Lực cắt đáy được xác định theo công thức:</p>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>F<sub>b</sub> = S<sub>d</sub>(T<sub>1</sub>).m.λ</p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">Trong đó: m là tổng khối lượng của công trình; λ là hệ số điều chỉnh, xét đến khối lượng hữu hiệu của dạng dao động cơ bản đầu tiên, khi T<sub>1</sub> &lt; 2T<sub>C</sub> và nhà có trên 2 tầng thì λ = 0,85; các trường hợp khác λ = 1.</p>\r\n\r\n<h3 style="text-align: justify;"><strong>V.2. Phương pháp phân tích phổ phản ứng dạng dao động</strong></h3>\r\n\r\n<p style="text-align: justify;">Xác định số dạng dao động cần xét đến theo mục 4.3.3.3.1. Có thể xác định theo điều kiện tổng khối lượng hữu hiệu của các dạng dao động được xét chiếm ít nhất 90% tổng khối lượng của kết cấu. Khối lượng hữu hiệu của công trình ứng với dạng dao động thứ i được xác định theo công thức</p>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>M<sub>td,i</sub> = (∑m<sub>j</sub>.y<sub>j</sub>)<sup>2</sup>/(∑m<sub>j</sub>.y<sub>j</sub><sup>2</sup>)</p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">Lực cắt đáy ứng với dạng dao động thứ i được xác định theo công thức: Fb = M<sub>td,i</sub>.S<sub>d</sub>(T<sub>i</sub>)<br  />\r\nKhi các dạng dao động được xem là độc lập (T<sub>j</sub> &lt; 0,9*T<sub>i</sub>), giá trị lớn nhất E<sub>E</sub> của hệ quả tác động động đất có thể lấy bằng: E<sub>E</sub> = √(∑E<sub>Ei</sub><sup>2</sup>), trong đó E<sub>Ei</sub> là giá trị của hệ quả tác động động đất do dạng dao động thứ i gây ra.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">VI. Tổ hợp các hệ quả của các thành phần tác động động đất</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Các thành phần nằm ngang (thành phần theo phương X và thành phần theo phương Y) của tác động động đất phải được xem là tác động đồng thời. Các hệ quả tác động do tổ hợp các thành phần nằm ngang của tác động động đất có thể xác định bằng cách sử dụng hai tổ hợp sau:</p>\r\n\r\n<ul>\r\n	<li style="text-align: justify;">E<sub>Edx</sub> + 0,3*E<sub>Edy</sub></li>\r\n	<li style="text-align: justify;">0,3*E<sub>Edx</sub> + E<sub>Edy</sub></li>\r\n</ul>\r\n\r\n<p style="text-align: justify;">Trong đó, E<sub>Edx</sub> và E<sub>Edy</sub> là các hệ quả tác động do đặt tác động động đất dọc theo trục X và trục Y.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">VII. Tổ hợp tác động động đất với các tác động khác</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Tổ hợp nội lực có tải trọng động đất đối với trường hợp không có ứng suất trước được xác định theo công thức:</p>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>TH = TT + k.HT + ĐĐ</p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">Trong đó k là hệ số tổ hợp của hoạt tải, xác định theo bảng 3.4 của tiêu chuẩn, ví dụ đối với hoạt tải khu vực văn phòng thì k = 0,3; hoạt tải của khu vực hội họp là k = 0,6.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">VIII. Tính toán chuyển vị</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Chuyển vị của một điểm của hệ kết cấu gây ra bởi tác động động đất thiết kế d<sub>s</sub> được xác định dựa trên chuyển vị của cùng điểm đó của hệ kết cấu được xác định bằng phân tích tuyến tính d<sub>c</sub>:</p>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>d<sub>s</sub> = q<sub>d</sub>*d<sub>c</sub></p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">Trong đó q<sub>d</sub> là hệ số ứng xử chuyển vị, giả thiết bằng hệ số ứng xử q trừ khi có các quy định khác.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">Tài liệu tham khảo</span></strong></h2>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>[1]. <strong>TCXDVN 375:2006</strong>. <em>Thiết kế công trình chịu động đất</em>.<br  />\r\n[2]. <strong>Nguyễn Lê Ninh</strong>. <em>Động đất và thiết kế công trình chịu động đất</em>. Nhà xuất bản Xây dựng, Hà Nội, 2009.</p>\r\n</blockquote>\r\n</div>', '', 1, 0, 0, 0, 0, 0),
(13, '<div class="bodytext">\r\n<h2 style="text-align: justify;"><span style="font-size:14px;"><strong>I. Xác định các thông số cơ bản</strong></span></h2>\r\n\r\n<p style="text-align:center"><img alt="dong dat vn" class="img-thumbnail" height="367" src="http://ketcauxaydung.com/uploads/news/2015_01/dong-dat-vn.jpg" width="550" /></p>\r\n\r\n<p style="text-align: justify;">- Xác định đỉnh gia tốc nền tham chiếu: a<sub>gR</sub>, tra bảng phụ lục I. Chú ý rằng bảng tra này đã được quy đổi theo gia tốc trọng trường g, a<sub>gR</sub> bằng giá trị tra bảng nhân với g, ví dụ đối với địa điểm thành phố Hồ Chí Minh - quận 4, có đỉnh gia tốc nền tham chiếu là 0,0847*g.</p>\r\n\r\n<p style="text-align: justify;">- Xác định hệ số tầm quan trọng, γ<sub>I</sub>, tra bảng phụ lục F và G. Ví dụ với nhà cao từ 20 tầng đến 60 tầng có mức độ quan trọng là I, hệ số tầm quan trọng là γ<sub>I</sub> = 1,25</p>\r\n\r\n<p style="text-align: justify;">- Xác định gia tốc nền thiết kế, a<sub>g</sub> = a<sub>gR</sub>*γ<sub>I</sub>; theo mục 3.2.1 của tiêu chuẩn: nếu a<sub>g</sub> &lt; 0,08*g - trường hợp động đất yếu - thì có thể sử dụng các quy trình thiết kế chịu động đất được giảm nhẹ hoặc đơn giản hóa cho một số loại, dạng kết cấu; nếu a<sub>g</sub> &lt; 0,04*g - trường hợp động đất rất yếu - thì không cần phải tuân theo những điều khoản của tiêu chuẩn. Theo [2] thì việc thiết kế kháng chấn cho các công trình xây dựng theo các quy định đề cập tới trong nội dung của tiêu chuẩn chỉ thực hiện chủ yếu cho các công trình xây dựng trong các vùng động đất mạnh có gia tốc nền a<sub>g</sub> &gt; 0,08*g.</p>\r\n\r\n<p style="text-align: justify;">- Xác định loại kết cấu, đối với kết cấu bê tông cốt thép xác định theo mục 5.1.2. Loại kết cấu của công trình có ảnh hưởng đến việc xác định hệ số ứng xử sẽ được đề cập ở mục 5. Một số trường hợp cần xác định bằng cách so sánh lực cắt (phản lực ngang tại liên kết với móng) mà các cấu kiện (các cột và các vách) sẽ gánh khi chịu lực ngang giả thiết.</p>\r\n\r\n<p style="text-align: justify;">- Xác định hệ số ứng xử q, đối với kết cấu bê tông cốt thép xác định theo mục 5.2.2.2, ví dụ hệ khung, nhiều tầng, nhiều nhịp, cấp độ dẻo trung bình (DCM) có: q = 3,0*1,3 = 3,9. Hệ số ứng xử q có thể khác nhau theo hai phương chính tùy thuộc hệ kết cấu.</p>\r\n\r\n<p style="text-align: justify;">- Xác định loại nền đất, theo mục 3.2.1, có thể sử dụng giá trị trung bình của chỉ số SPT của các lớp đất trong chiều sâu 30m. Loại nền đất có vai trò xác định các tham số mô tả phổ phản ứng gia tốc sẽ được nêu ở mục 7.</p>\r\n\r\n<p style="text-align: justify;">- Xác định các tham số mô tả phổ phản ứng gia tốc S, T<sub>B</sub>, T<sub>C</sub>, T<sub>D</sub>, theo bảng 3.2 - mục 3.2.2.2.</p>\r\n\r\n<h3 style="text-align: justify;"><span style="font-size:14px;"><strong>II. Xác định khối lượng tham gia dao động</strong></span></h3>\r\n\r\n<p style="text-align: justify;">Khối lượng tham gia dao động ảnh hưởng đến chu kỳ, dạng dao động, và tải trọng động đất tác dụng lên công trình.<br  />\r\nKhối lượng tham gia dao động được xác định theo các mục 3.2.4 và 4.2.4, phụ thuộc vào tĩnh tải, hoạt tải và loại hoạt tải. Ví dụ công trình có các loại phòng như văn phòng (HTVP) và phòng họp (HTPH), thì công thức xác định khối lượng tham gia dao động sẽ bằng: TT + 0,5*0,3*HTVP + 0,5*0,6*HTPH (sử dụng hệ số 0,5 do các tầng sử dụng độc lập, không phụ thuộc lẫn nhau).</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">III. Xác định chu kỳ và dạng của các dạng dao động</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Trong một số trường hợp, tiêu chuẩn cho phép sử dụng công thức gần đúng để xác định chu kỳ và dạng dao động (xem mục 4.3.3.2.2)<br  />\r\nKhi công trình thỏa mãn tính đều đặn trong mặt bằng, có thể phân tích dao động bằng hai mô hình phẳng theo hai phương chính.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">IV. Xác định phổ phản ứng gia tốc thiết kế</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Giá trị của phổ phản ứng gia tốc thiết kế của một hệ kết cấu S<sub>d</sub>(T<sub>i</sub>), xác định theo mục 3.2.2.5.(4), phụ thuộc vào chu kỳ dao động riêng của hệ, hệ số ứng xử của kết cấu, gia tốc nền thiết kế, và loại nền đất.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">V. Xác định tải trọng động đất</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Khi công trình thỏa mãn các điều kiện cho trong mục 4.3.3.2.1.(2), có thể sử dụng phương pháp phân tích tĩnh lực ngang tương đương, trong mọi trường hợp có thể sử dụng phương pháp phân tích phổ phản ứng dạng dao động.<br  />\r\nNguyên tắc chung để xác định tải trọng động đất là xác định lực cắt đáy F<sub>b</sub> ứng với mỗi dạng dao động và phân phối lên các tầng dưới dạng tải trọng ngang dựa vào dạng của các dạng dao động: F<sub>i</sub> = F<sub>b</sub>*(m<sub>i</sub>.y<sub>i</sub>)/∑(m<sub>j</sub>.y<sub>j</sub>), trong đó m<sub>i</sub> và y<sub>i</sub> lần lượt là khối lượng và tung độ của dạng dao động của tầng thứ i.</p>\r\n\r\n<h3 style="text-align: justify;"><span style="font-size:14px;"><strong>V.1. Phương pháp phân tích tĩnh lực ngang tương đương (mục 4.3.3.2)</strong></span></h3>\r\n\r\n<p style="text-align: justify;">Điều kiện áp dụng phương pháp tĩnh lực ngang tương đương:</p>\r\n\r\n<ul>\r\n	<li style="text-align: justify;">Chu kỳ dao động cơ bản T<sub>1</sub> theo hai hướng chính nhỏ hơn các giá trị sau: 4.T<sub>C</sub> và 2 giây; với T<sub>C</sub> là thông số xác định dạng của phổ phản ứng gia tốc</li>\r\n	<li style="text-align: justify;">Thỏa mãn tính đều đặn theo mặt đứng</li>\r\n</ul>\r\n\r\n<p style="text-align: justify;">Lực cắt đáy được xác định theo công thức:</p>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>F<sub>b</sub> = S<sub>d</sub>(T<sub>1</sub>).m.λ</p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">Trong đó: m là tổng khối lượng của công trình; λ là hệ số điều chỉnh, xét đến khối lượng hữu hiệu của dạng dao động cơ bản đầu tiên, khi T<sub>1</sub> &lt; 2T<sub>C</sub> và nhà có trên 2 tầng thì λ = 0,85; các trường hợp khác λ = 1.</p>\r\n\r\n<h3 style="text-align: justify;"><strong>V.2. Phương pháp phân tích phổ phản ứng dạng dao động</strong></h3>\r\n\r\n<p style="text-align: justify;">Xác định số dạng dao động cần xét đến theo mục 4.3.3.3.1. Có thể xác định theo điều kiện tổng khối lượng hữu hiệu của các dạng dao động được xét chiếm ít nhất 90% tổng khối lượng của kết cấu. Khối lượng hữu hiệu của công trình ứng với dạng dao động thứ i được xác định theo công thức</p>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>M<sub>td,i</sub> = (∑m<sub>j</sub>.y<sub>j</sub>)<sup>2</sup>/(∑m<sub>j</sub>.y<sub>j</sub><sup>2</sup>)</p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">Lực cắt đáy ứng với dạng dao động thứ i được xác định theo công thức: Fb = M<sub>td,i</sub>.S<sub>d</sub>(T<sub>i</sub>)<br  />\r\nKhi các dạng dao động được xem là độc lập (T<sub>j</sub> &lt; 0,9*T<sub>i</sub>), giá trị lớn nhất E<sub>E</sub> của hệ quả tác động động đất có thể lấy bằng: E<sub>E</sub> = √(∑E<sub>Ei</sub><sup>2</sup>), trong đó E<sub>Ei</sub> là giá trị của hệ quả tác động động đất do dạng dao động thứ i gây ra.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">VI. Tổ hợp các hệ quả của các thành phần tác động động đất</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Các thành phần nằm ngang (thành phần theo phương X và thành phần theo phương Y) của tác động động đất phải được xem là tác động đồng thời. Các hệ quả tác động do tổ hợp các thành phần nằm ngang của tác động động đất có thể xác định bằng cách sử dụng hai tổ hợp sau:</p>\r\n\r\n<ul>\r\n	<li style="text-align: justify;">E<sub>Edx</sub> + 0,3*E<sub>Edy</sub></li>\r\n	<li style="text-align: justify;">0,3*E<sub>Edx</sub> + E<sub>Edy</sub></li>\r\n</ul>\r\n\r\n<p style="text-align: justify;">Trong đó, E<sub>Edx</sub> và E<sub>Edy</sub> là các hệ quả tác động do đặt tác động động đất dọc theo trục X và trục Y.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">VII. Tổ hợp tác động động đất với các tác động khác</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Tổ hợp nội lực có tải trọng động đất đối với trường hợp không có ứng suất trước được xác định theo công thức:</p>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>TH = TT + k.HT + ĐĐ</p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">Trong đó k là hệ số tổ hợp của hoạt tải, xác định theo bảng 3.4 của tiêu chuẩn, ví dụ đối với hoạt tải khu vực văn phòng thì k = 0,3; hoạt tải của khu vực hội họp là k = 0,6.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">VIII. Tính toán chuyển vị</span></strong></h2>\r\n\r\n<p style="text-align: justify;">Chuyển vị của một điểm của hệ kết cấu gây ra bởi tác động động đất thiết kế d<sub>s</sub> được xác định dựa trên chuyển vị của cùng điểm đó của hệ kết cấu được xác định bằng phân tích tuyến tính d<sub>c</sub>:</p>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>d<sub>s</sub> = q<sub>d</sub>*d<sub>c</sub></p>\r\n</blockquote>\r\n\r\n<p style="text-align: justify;">Trong đó q<sub>d</sub> là hệ số ứng xử chuyển vị, giả thiết bằng hệ số ứng xử q trừ khi có các quy định khác.</p>\r\n\r\n<h2 style="text-align: justify;"><strong><span style="font-size:14px;">Tài liệu tham khảo</span></strong></h2>\r\n\r\n<blockquote style="text-align: justify;">\r\n<p>[1]. <strong>TCXDVN 375:2006</strong>. <em>Thiết kế công trình chịu động đất</em>.<br  />\r\n[2]. <strong>Nguyễn Lê Ninh</strong>. <em>Động đất và thiết kế công trình chịu động đất</em>. Nhà xuất bản Xây dựng, Hà Nội, 2009.</p>\r\n</blockquote>\r\n</div>', '', 1, 0, 1, 1, 1, 0),
(18, '<p><iframe frameborder="0" height="920" scrolling="no" src="http://docs.google.com/viewhttps://dochub.com/ktcuxaydng/kZbJ8/tran-ngoc-dong-1-2014" style="border: none;" width="710"></iframe></p>\r\n\r\n<p>https://docs.google.com/file/d/0B9GzJKouH1IVRWdEelJvTkliekk/edit</p>\r\n\r\n<p>https://docs.google.com/file/d/0B9GzJKouH1IVRWdEelJvTkliekk/edit</p>', '', 1, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_news_bodytext`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_news_bodytext` (
  `id` int(11) unsigned NOT NULL,
  `bodytext` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kcxd_vi_news_bodytext`
--

INSERT INTO `kcxd_vi_news_bodytext` (`id`, `bodytext`) VALUES
(17, 'Giới thiệu ngắn gọn (Hiển thị đối với mọi đối tượng kể cả khi không được phân quyền). Số ký tự: 0. Nên nhập tối đa 160 ký tự '),
(16, '\r\nI. Xác định các thông số cơ bản\r\n\r\n http://ketcauxaydung.com/uploads/news/2015_01/dong-dat-vn.jpg dong dat vn\r\n\r\n- Xác định đỉnh gia tốc nền tham chiếu: agR, tra bảng phụ lục I. Chú ý rằng bảng tra này đã được quy đổi theo gia tốc trọng trường g, agR bằng giá trị tra bảng nhân với g, ví dụ đối với địa điểm thành phố Hồ Chí Minh - quận 4, có đỉnh gia tốc nền tham chiếu là 0,0847*g.\r\n\r\n- Xác định hệ số tầm quan trọng, γI, tra bảng phụ lục F và G. Ví dụ với nhà cao từ 20 tầng đến 60 tầng có mức độ quan trọng là I, hệ số tầm quan trọng là γI = 1,25\r\n\r\n- Xác định gia tốc nền thiết kế, ag = agR*γI; theo mục 3.2.1 của tiêu chuẩn: nếu ag &lt; 0,08*g - trường hợp động đất yếu - thì có thể sử dụng các quy trình thiết kế chịu động đất được giảm nhẹ hoặc đơn giản hóa cho một số loại, dạng kết cấu; nếu ag &lt; 0,04*g - trường hợp động đất rất yếu - thì không cần phải tuân theo những điều khoản của tiêu chuẩn. Theo [2] thì việc thiết kế kháng chấn cho các công trình xây dựng theo các quy định đề cập tới trong nội dung của tiêu chuẩn chỉ thực hiện chủ yếu cho các công trình xây dựng trong các vùng động đất mạnh có gia tốc nền ag &gt; 0,08*g.\r\n\r\n- Xác định loại kết cấu, đối với kết cấu bê tông cốt thép xác định theo mục 5.1.2. Loại kết cấu của công trình có ảnh hưởng đến việc xác định hệ số ứng xử sẽ được đề cập ở mục 5. Một số trường hợp cần xác định bằng cách so sánh lực cắt (phản lực ngang tại liên kết với móng) mà các cấu kiện (các cột và các vách) sẽ gánh khi chịu lực ngang giả thiết.\r\n\r\n- Xác định hệ số ứng xử q, đối với kết cấu bê tông cốt thép xác định theo mục 5.2.2.2, ví dụ hệ khung, nhiều tầng, nhiều nhịp, cấp độ dẻo trung bình (DCM) có: q = 3,0*1,3 = 3,9. Hệ số ứng xử q có thể khác nhau theo hai phương chính tùy thuộc hệ kết cấu.\r\n\r\n- Xác định loại nền đất, theo mục 3.2.1, có thể sử dụng giá trị trung bình của chỉ số SPT của các lớp đất trong chiều sâu 30m. Loại nền đất có vai trò xác định các tham số mô tả phổ phản ứng gia tốc sẽ được nêu ở mục 7.\r\n\r\n- Xác định các tham số mô tả phổ phản ứng gia tốc S, TB, TC, TD, theo bảng 3.2 - mục 3.2.2.2.\r\n\r\nII. Xác định khối lượng tham gia dao động\r\n\r\nKhối lượng tham gia dao động ảnh hưởng đến chu kỳ, dạng dao động, và tải trọng động đất tác dụng lên công trình.\r\nKhối lượng tham gia dao động được xác định theo các mục 3.2.4 và 4.2.4, phụ thuộc vào tĩnh tải, hoạt tải và loại hoạt tải. Ví dụ công trình có các loại phòng như văn phòng (HTVP) và phòng họp (HTPH), thì công thức xác định khối lượng tham gia dao động sẽ bằng: TT + 0,5*0,3*HTVP + 0,5*0,6*HTPH (sử dụng hệ số 0,5 do các tầng sử dụng độc lập, không phụ thuộc lẫn nhau).\r\n\r\nIII. Xác định chu kỳ và dạng của các dạng dao động\r\n\r\nTrong một số trường hợp, tiêu chuẩn cho phép sử dụng công thức gần đúng để xác định chu kỳ và dạng dao động (xem mục 4.3.3.2.2)\r\nKhi công trình thỏa mãn tính đều đặn trong mặt bằng, có thể phân tích dao động bằng hai mô hình phẳng theo hai phương chính.\r\n\r\nIV. Xác định phổ phản ứng gia tốc thiết kế\r\n\r\nGiá trị của phổ phản ứng gia tốc thiết kế của một hệ kết cấu Sd(Ti), xác định theo mục 3.2.2.5.(4), phụ thuộc vào chu kỳ dao động riêng của hệ, hệ số ứng xử của kết cấu, gia tốc nền thiết kế, và loại nền đất.\r\n\r\nV. Xác định tải trọng động đất\r\n\r\nKhi công trình thỏa mãn các điều kiện cho trong mục 4.3.3.2.1.(2), có thể sử dụng phương pháp phân tích tĩnh lực ngang tương đương, trong mọi trường hợp có thể sử dụng phương pháp phân tích phổ phản ứng dạng dao động.\r\nNguyên tắc chung để xác định tải trọng động đất là xác định lực cắt đáy Fb ứng với mỗi dạng dao động và phân phối lên các tầng dưới dạng tải trọng ngang dựa vào dạng của các dạng dao động: Fi = Fb*(mi.yi)/∑(mj.yj), trong đó mi và yi lần lượt là khối lượng và tung độ của dạng dao động của tầng thứ i.\r\n\r\nV.1. Phương pháp phân tích tĩnh lực ngang tương đương (mục 4.3.3.2)\r\n\r\nĐiều kiện áp dụng phương pháp tĩnh lực ngang tương đương:\r\n\r\n\r\n	Chu kỳ dao động cơ bản T1 theo hai hướng chính nhỏ hơn các giá trị sau: 4.TC và 2 giây; với TC là thông số xác định dạng của phổ phản ứng gia tốc\r\n	Thỏa mãn tính đều đặn theo mặt đứng\r\n\r\n\r\nLực cắt đáy được xác định theo công thức:\r\n\r\n\r\nFb = Sd(T1).m.λ\r\n\r\n\r\nTrong đó: m là tổng khối lượng của công trình; λ là hệ số điều chỉnh, xét đến khối lượng hữu hiệu của dạng dao động cơ bản đầu tiên, khi T1 &lt; 2TC và nhà có trên 2 tầng thì λ = 0,85; các trường hợp khác λ = 1.\r\n\r\nV.2. Phương pháp phân tích phổ phản ứng dạng dao động\r\n\r\nXác định số dạng dao động cần xét đến theo mục 4.3.3.3.1. Có thể xác định theo điều kiện tổng khối lượng hữu hiệu của các dạng dao động được xét chiếm ít nhất 90% tổng khối lượng của kết cấu. Khối lượng hữu hiệu của công trình ứng với dạng dao động thứ i được xác định theo công thức\r\n\r\n\r\nMtd,i = (∑mj.yj)2/(∑mj.yj2)\r\n\r\n\r\nLực cắt đáy ứng với dạng dao động thứ i được xác định theo công thức: Fb = Mtd,i.Sd(Ti)\r\nKhi các dạng dao động được xem là độc lập (Tj &lt; 0,9*Ti), giá trị lớn nhất EE của hệ quả tác động động đất có thể lấy bằng: EE = √(∑EEi2), trong đó EEi là giá trị của hệ quả tác động động đất do dạng dao động thứ i gây ra.\r\n\r\nVI. Tổ hợp các hệ quả của các thành phần tác động động đất\r\n\r\nCác thành phần nằm ngang (thành phần theo phương X và thành phần theo phương Y) của tác động động đất phải được xem là tác động đồng thời. Các hệ quả tác động do tổ hợp các thành phần nằm ngang của tác động động đất có thể xác định bằng cách sử dụng hai tổ hợp sau:\r\n\r\n\r\n	EEdx + 0,3*EEdy\r\n	0,3*EEdx + EEdy\r\n\r\n\r\nTrong đó, EEdx và EEdy là các hệ quả tác động do đặt tác động động đất dọc theo trục X và trục Y.\r\n\r\nVII. Tổ hợp tác động động đất với các tác động khác\r\n\r\nTổ hợp nội lực có tải trọng động đất đối với trường hợp không có ứng suất trước được xác định theo công thức:\r\n\r\n\r\nTH = TT + k.HT + ĐĐ\r\n\r\n\r\nTrong đó k là hệ số tổ hợp của hoạt tải, xác định theo bảng 3.4 của tiêu chuẩn, ví dụ đối với hoạt tải khu vực văn phòng thì k = 0,3; hoạt tải của khu vực hội họp là k = 0,6.\r\n\r\nVIII. Tính toán chuyển vị\r\n\r\nChuyển vị của một điểm của hệ kết cấu gây ra bởi tác động động đất thiết kế ds được xác định dựa trên chuyển vị của cùng điểm đó của hệ kết cấu được xác định bằng phân tích tuyến tính dc:\r\n\r\n\r\nds = qd*dc\r\n\r\n\r\nTrong đó qd là hệ số ứng xử chuyển vị, giả thiết bằng hệ số ứng xử q trừ khi có các quy định khác.\r\n\r\nTài liệu tham khảo\r\n\r\n\r\n[1]. TCXDVN 375:2006. Thiết kế công trình chịu động đất.\r\n[2]. Nguyễn Lê Ninh. Động đất và thiết kế công trình chịu động đất. Nhà xuất bản Xây dựng, Hà Nội, 2009.\r\n\r\n'),
(15, '\r\nI. Xác định các thông số cơ bản\r\n\r\n http://ketcauxaydung.com/uploads/news/2015_01/dong-dat-vn.jpg dong dat vn\r\n\r\n- Xác định đỉnh gia tốc nền tham chiếu: agR, tra bảng phụ lục I. Chú ý rằng bảng tra này đã được quy đổi theo gia tốc trọng trường g, agR bằng giá trị tra bảng nhân với g, ví dụ đối với địa điểm thành phố Hồ Chí Minh - quận 4, có đỉnh gia tốc nền tham chiếu là 0,0847*g.\r\n\r\n- Xác định hệ số tầm quan trọng, γI, tra bảng phụ lục F và G. Ví dụ với nhà cao từ 20 tầng đến 60 tầng có mức độ quan trọng là I, hệ số tầm quan trọng là γI = 1,25\r\n\r\n- Xác định gia tốc nền thiết kế, ag = agR*γI; theo mục 3.2.1 của tiêu chuẩn: nếu ag &lt; 0,08*g - trường hợp động đất yếu - thì có thể sử dụng các quy trình thiết kế chịu động đất được giảm nhẹ hoặc đơn giản hóa cho một số loại, dạng kết cấu; nếu ag &lt; 0,04*g - trường hợp động đất rất yếu - thì không cần phải tuân theo những điều khoản của tiêu chuẩn. Theo [2] thì việc thiết kế kháng chấn cho các công trình xây dựng theo các quy định đề cập tới trong nội dung của tiêu chuẩn chỉ thực hiện chủ yếu cho các công trình xây dựng trong các vùng động đất mạnh có gia tốc nền ag &gt; 0,08*g.\r\n\r\n- Xác định loại kết cấu, đối với kết cấu bê tông cốt thép xác định theo mục 5.1.2. Loại kết cấu của công trình có ảnh hưởng đến việc xác định hệ số ứng xử sẽ được đề cập ở mục 5. Một số trường hợp cần xác định bằng cách so sánh lực cắt (phản lực ngang tại liên kết với móng) mà các cấu kiện (các cột và các vách) sẽ gánh khi chịu lực ngang giả thiết.\r\n\r\n- Xác định hệ số ứng xử q, đối với kết cấu bê tông cốt thép xác định theo mục 5.2.2.2, ví dụ hệ khung, nhiều tầng, nhiều nhịp, cấp độ dẻo trung bình (DCM) có: q = 3,0*1,3 = 3,9. Hệ số ứng xử q có thể khác nhau theo hai phương chính tùy thuộc hệ kết cấu.\r\n\r\n- Xác định loại nền đất, theo mục 3.2.1, có thể sử dụng giá trị trung bình của chỉ số SPT của các lớp đất trong chiều sâu 30m. Loại nền đất có vai trò xác định các tham số mô tả phổ phản ứng gia tốc sẽ được nêu ở mục 7.\r\n\r\n- Xác định các tham số mô tả phổ phản ứng gia tốc S, TB, TC, TD, theo bảng 3.2 - mục 3.2.2.2.\r\n\r\nII. Xác định khối lượng tham gia dao động\r\n\r\nKhối lượng tham gia dao động ảnh hưởng đến chu kỳ, dạng dao động, và tải trọng động đất tác dụng lên công trình.\r\nKhối lượng tham gia dao động được xác định theo các mục 3.2.4 và 4.2.4, phụ thuộc vào tĩnh tải, hoạt tải và loại hoạt tải. Ví dụ công trình có các loại phòng như văn phòng (HTVP) và phòng họp (HTPH), thì công thức xác định khối lượng tham gia dao động sẽ bằng: TT + 0,5*0,3*HTVP + 0,5*0,6*HTPH (sử dụng hệ số 0,5 do các tầng sử dụng độc lập, không phụ thuộc lẫn nhau).\r\n\r\nIII. Xác định chu kỳ và dạng của các dạng dao động\r\n\r\nTrong một số trường hợp, tiêu chuẩn cho phép sử dụng công thức gần đúng để xác định chu kỳ và dạng dao động (xem mục 4.3.3.2.2)\r\nKhi công trình thỏa mãn tính đều đặn trong mặt bằng, có thể phân tích dao động bằng hai mô hình phẳng theo hai phương chính.\r\n\r\nIV. Xác định phổ phản ứng gia tốc thiết kế\r\n\r\nGiá trị của phổ phản ứng gia tốc thiết kế của một hệ kết cấu Sd(Ti), xác định theo mục 3.2.2.5.(4), phụ thuộc vào chu kỳ dao động riêng của hệ, hệ số ứng xử của kết cấu, gia tốc nền thiết kế, và loại nền đất.\r\n\r\nV. Xác định tải trọng động đất\r\n\r\nKhi công trình thỏa mãn các điều kiện cho trong mục 4.3.3.2.1.(2), có thể sử dụng phương pháp phân tích tĩnh lực ngang tương đương, trong mọi trường hợp có thể sử dụng phương pháp phân tích phổ phản ứng dạng dao động.\r\nNguyên tắc chung để xác định tải trọng động đất là xác định lực cắt đáy Fb ứng với mỗi dạng dao động và phân phối lên các tầng dưới dạng tải trọng ngang dựa vào dạng của các dạng dao động: Fi = Fb*(mi.yi)/∑(mj.yj), trong đó mi và yi lần lượt là khối lượng và tung độ của dạng dao động của tầng thứ i.\r\n\r\nV.1. Phương pháp phân tích tĩnh lực ngang tương đương (mục 4.3.3.2)\r\n\r\nĐiều kiện áp dụng phương pháp tĩnh lực ngang tương đương:\r\n\r\n\r\n	Chu kỳ dao động cơ bản T1 theo hai hướng chính nhỏ hơn các giá trị sau: 4.TC và 2 giây; với TC là thông số xác định dạng của phổ phản ứng gia tốc\r\n	Thỏa mãn tính đều đặn theo mặt đứng\r\n\r\n\r\nLực cắt đáy được xác định theo công thức:\r\n\r\n\r\nFb = Sd(T1).m.λ\r\n\r\n\r\nTrong đó: m là tổng khối lượng của công trình; λ là hệ số điều chỉnh, xét đến khối lượng hữu hiệu của dạng dao động cơ bản đầu tiên, khi T1 &lt; 2TC và nhà có trên 2 tầng thì λ = 0,85; các trường hợp khác λ = 1.\r\n\r\nV.2. Phương pháp phân tích phổ phản ứng dạng dao động\r\n\r\nXác định số dạng dao động cần xét đến theo mục 4.3.3.3.1. Có thể xác định theo điều kiện tổng khối lượng hữu hiệu của các dạng dao động được xét chiếm ít nhất 90% tổng khối lượng của kết cấu. Khối lượng hữu hiệu của công trình ứng với dạng dao động thứ i được xác định theo công thức\r\n\r\n\r\nMtd,i = (∑mj.yj)2/(∑mj.yj2)\r\n\r\n\r\nLực cắt đáy ứng với dạng dao động thứ i được xác định theo công thức: Fb = Mtd,i.Sd(Ti)\r\nKhi các dạng dao động được xem là độc lập (Tj &lt; 0,9*Ti), giá trị lớn nhất EE của hệ quả tác động động đất có thể lấy bằng: EE = √(∑EEi2), trong đó EEi là giá trị của hệ quả tác động động đất do dạng dao động thứ i gây ra.\r\n\r\nVI. Tổ hợp các hệ quả của các thành phần tác động động đất\r\n\r\nCác thành phần nằm ngang (thành phần theo phương X và thành phần theo phương Y) của tác động động đất phải được xem là tác động đồng thời. Các hệ quả tác động do tổ hợp các thành phần nằm ngang của tác động động đất có thể xác định bằng cách sử dụng hai tổ hợp sau:\r\n\r\n\r\n	EEdx + 0,3*EEdy\r\n	0,3*EEdx + EEdy\r\n\r\n\r\nTrong đó, EEdx và EEdy là các hệ quả tác động do đặt tác động động đất dọc theo trục X và trục Y.\r\n\r\nVII. Tổ hợp tác động động đất với các tác động khác\r\n\r\nTổ hợp nội lực có tải trọng động đất đối với trường hợp không có ứng suất trước được xác định theo công thức:\r\n\r\n\r\nTH = TT + k.HT + ĐĐ\r\n\r\n\r\nTrong đó k là hệ số tổ hợp của hoạt tải, xác định theo bảng 3.4 của tiêu chuẩn, ví dụ đối với hoạt tải khu vực văn phòng thì k = 0,3; hoạt tải của khu vực hội họp là k = 0,6.\r\n\r\nVIII. Tính toán chuyển vị\r\n\r\nChuyển vị của một điểm của hệ kết cấu gây ra bởi tác động động đất thiết kế ds được xác định dựa trên chuyển vị của cùng điểm đó của hệ kết cấu được xác định bằng phân tích tuyến tính dc:\r\n\r\n\r\nds = qd*dc\r\n\r\n\r\nTrong đó qd là hệ số ứng xử chuyển vị, giả thiết bằng hệ số ứng xử q trừ khi có các quy định khác.\r\n\r\nTài liệu tham khảo\r\n\r\n\r\n[1]. TCXDVN 375:2006. Thiết kế công trình chịu động đất.\r\n[2]. Nguyễn Lê Ninh. Động đất và thiết kế công trình chịu động đất. Nhà xuất bản Xây dựng, Hà Nội, 2009.\r\n\r\n'),
(14, '\r\nI. Xác định các thông số cơ bản\r\n\r\n http://ketcauxaydung.com/uploads/news/2015_01/dong-dat-vn.jpg dong dat vn\r\n\r\n- Xác định đỉnh gia tốc nền tham chiếu: agR, tra bảng phụ lục I. Chú ý rằng bảng tra này đã được quy đổi theo gia tốc trọng trường g, agR bằng giá trị tra bảng nhân với g, ví dụ đối với địa điểm thành phố Hồ Chí Minh - quận 4, có đỉnh gia tốc nền tham chiếu là 0,0847*g.\r\n\r\n- Xác định hệ số tầm quan trọng, γI, tra bảng phụ lục F và G. Ví dụ với nhà cao từ 20 tầng đến 60 tầng có mức độ quan trọng là I, hệ số tầm quan trọng là γI = 1,25\r\n\r\n- Xác định gia tốc nền thiết kế, ag = agR*γI; theo mục 3.2.1 của tiêu chuẩn: nếu ag &lt; 0,08*g - trường hợp động đất yếu - thì có thể sử dụng các quy trình thiết kế chịu động đất được giảm nhẹ hoặc đơn giản hóa cho một số loại, dạng kết cấu; nếu ag &lt; 0,04*g - trường hợp động đất rất yếu - thì không cần phải tuân theo những điều khoản của tiêu chuẩn. Theo [2] thì việc thiết kế kháng chấn cho các công trình xây dựng theo các quy định đề cập tới trong nội dung của tiêu chuẩn chỉ thực hiện chủ yếu cho các công trình xây dựng trong các vùng động đất mạnh có gia tốc nền ag &gt; 0,08*g.\r\n\r\n- Xác định loại kết cấu, đối với kết cấu bê tông cốt thép xác định theo mục 5.1.2. Loại kết cấu của công trình có ảnh hưởng đến việc xác định hệ số ứng xử sẽ được đề cập ở mục 5. Một số trường hợp cần xác định bằng cách so sánh lực cắt (phản lực ngang tại liên kết với móng) mà các cấu kiện (các cột và các vách) sẽ gánh khi chịu lực ngang giả thiết.\r\n\r\n- Xác định hệ số ứng xử q, đối với kết cấu bê tông cốt thép xác định theo mục 5.2.2.2, ví dụ hệ khung, nhiều tầng, nhiều nhịp, cấp độ dẻo trung bình (DCM) có: q = 3,0*1,3 = 3,9. Hệ số ứng xử q có thể khác nhau theo hai phương chính tùy thuộc hệ kết cấu.\r\n\r\n- Xác định loại nền đất, theo mục 3.2.1, có thể sử dụng giá trị trung bình của chỉ số SPT của các lớp đất trong chiều sâu 30m. Loại nền đất có vai trò xác định các tham số mô tả phổ phản ứng gia tốc sẽ được nêu ở mục 7.\r\n\r\n- Xác định các tham số mô tả phổ phản ứng gia tốc S, TB, TC, TD, theo bảng 3.2 - mục 3.2.2.2.\r\n\r\nII. Xác định khối lượng tham gia dao động\r\n\r\nKhối lượng tham gia dao động ảnh hưởng đến chu kỳ, dạng dao động, và tải trọng động đất tác dụng lên công trình.\r\nKhối lượng tham gia dao động được xác định theo các mục 3.2.4 và 4.2.4, phụ thuộc vào tĩnh tải, hoạt tải và loại hoạt tải. Ví dụ công trình có các loại phòng như văn phòng (HTVP) và phòng họp (HTPH), thì công thức xác định khối lượng tham gia dao động sẽ bằng: TT + 0,5*0,3*HTVP + 0,5*0,6*HTPH (sử dụng hệ số 0,5 do các tầng sử dụng độc lập, không phụ thuộc lẫn nhau).\r\n\r\nIII. Xác định chu kỳ và dạng của các dạng dao động\r\n\r\nTrong một số trường hợp, tiêu chuẩn cho phép sử dụng công thức gần đúng để xác định chu kỳ và dạng dao động (xem mục 4.3.3.2.2)\r\nKhi công trình thỏa mãn tính đều đặn trong mặt bằng, có thể phân tích dao động bằng hai mô hình phẳng theo hai phương chính.\r\n\r\nIV. Xác định phổ phản ứng gia tốc thiết kế\r\n\r\nGiá trị của phổ phản ứng gia tốc thiết kế của một hệ kết cấu Sd(Ti), xác định theo mục 3.2.2.5.(4), phụ thuộc vào chu kỳ dao động riêng của hệ, hệ số ứng xử của kết cấu, gia tốc nền thiết kế, và loại nền đất.\r\n\r\nV. Xác định tải trọng động đất\r\n\r\nKhi công trình thỏa mãn các điều kiện cho trong mục 4.3.3.2.1.(2), có thể sử dụng phương pháp phân tích tĩnh lực ngang tương đương, trong mọi trường hợp có thể sử dụng phương pháp phân tích phổ phản ứng dạng dao động.\r\nNguyên tắc chung để xác định tải trọng động đất là xác định lực cắt đáy Fb ứng với mỗi dạng dao động và phân phối lên các tầng dưới dạng tải trọng ngang dựa vào dạng của các dạng dao động: Fi = Fb*(mi.yi)/∑(mj.yj), trong đó mi và yi lần lượt là khối lượng và tung độ của dạng dao động của tầng thứ i.\r\n\r\nV.1. Phương pháp phân tích tĩnh lực ngang tương đương (mục 4.3.3.2)\r\n\r\nĐiều kiện áp dụng phương pháp tĩnh lực ngang tương đương:\r\n\r\n\r\n	Chu kỳ dao động cơ bản T1 theo hai hướng chính nhỏ hơn các giá trị sau: 4.TC và 2 giây; với TC là thông số xác định dạng của phổ phản ứng gia tốc\r\n	Thỏa mãn tính đều đặn theo mặt đứng\r\n\r\n\r\nLực cắt đáy được xác định theo công thức:\r\n\r\n\r\nFb = Sd(T1).m.λ\r\n\r\n\r\nTrong đó: m là tổng khối lượng của công trình; λ là hệ số điều chỉnh, xét đến khối lượng hữu hiệu của dạng dao động cơ bản đầu tiên, khi T1 &lt; 2TC và nhà có trên 2 tầng thì λ = 0,85; các trường hợp khác λ = 1.\r\n\r\nV.2. Phương pháp phân tích phổ phản ứng dạng dao động\r\n\r\nXác định số dạng dao động cần xét đến theo mục 4.3.3.3.1. Có thể xác định theo điều kiện tổng khối lượng hữu hiệu của các dạng dao động được xét chiếm ít nhất 90% tổng khối lượng của kết cấu. Khối lượng hữu hiệu của công trình ứng với dạng dao động thứ i được xác định theo công thức\r\n\r\n\r\nMtd,i = (∑mj.yj)2/(∑mj.yj2)\r\n\r\n\r\nLực cắt đáy ứng với dạng dao động thứ i được xác định theo công thức: Fb = Mtd,i.Sd(Ti)\r\nKhi các dạng dao động được xem là độc lập (Tj &lt; 0,9*Ti), giá trị lớn nhất EE của hệ quả tác động động đất có thể lấy bằng: EE = √(∑EEi2), trong đó EEi là giá trị của hệ quả tác động động đất do dạng dao động thứ i gây ra.\r\n\r\nVI. Tổ hợp các hệ quả của các thành phần tác động động đất\r\n\r\nCác thành phần nằm ngang (thành phần theo phương X và thành phần theo phương Y) của tác động động đất phải được xem là tác động đồng thời. Các hệ quả tác động do tổ hợp các thành phần nằm ngang của tác động động đất có thể xác định bằng cách sử dụng hai tổ hợp sau:\r\n\r\n\r\n	EEdx + 0,3*EEdy\r\n	0,3*EEdx + EEdy\r\n\r\n\r\nTrong đó, EEdx và EEdy là các hệ quả tác động do đặt tác động động đất dọc theo trục X và trục Y.\r\n\r\nVII. Tổ hợp tác động động đất với các tác động khác\r\n\r\nTổ hợp nội lực có tải trọng động đất đối với trường hợp không có ứng suất trước được xác định theo công thức:\r\n\r\n\r\nTH = TT + k.HT + ĐĐ\r\n\r\n\r\nTrong đó k là hệ số tổ hợp của hoạt tải, xác định theo bảng 3.4 của tiêu chuẩn, ví dụ đối với hoạt tải khu vực văn phòng thì k = 0,3; hoạt tải của khu vực hội họp là k = 0,6.\r\n\r\nVIII. Tính toán chuyển vị\r\n\r\nChuyển vị của một điểm của hệ kết cấu gây ra bởi tác động động đất thiết kế ds được xác định dựa trên chuyển vị của cùng điểm đó của hệ kết cấu được xác định bằng phân tích tuyến tính dc:\r\n\r\n\r\nds = qd*dc\r\n\r\n\r\nTrong đó qd là hệ số ứng xử chuyển vị, giả thiết bằng hệ số ứng xử q trừ khi có các quy định khác.\r\n\r\nTài liệu tham khảo\r\n\r\n\r\n[1]. TCXDVN 375:2006. Thiết kế công trình chịu động đất.\r\n[2]. Nguyễn Lê Ninh. Động đất và thiết kế công trình chịu động đất. Nhà xuất bản Xây dựng, Hà Nội, 2009.\r\n\r\n'),
(11, '\r\nI. Xác định các thông số cơ bản\r\n\r\n http://ketcauxaydung.com/uploads/news/2015_01/dong-dat-vn.jpg dong dat vn\r\n\r\n- Xác định đỉnh gia tốc nền tham chiếu: agR, tra bảng phụ lục I. Chú ý rằng bảng tra này đã được quy đổi theo gia tốc trọng trường g, agR bằng giá trị tra bảng nhân với g, ví dụ đối với địa điểm thành phố Hồ Chí Minh - quận 4, có đỉnh gia tốc nền tham chiếu là 0,0847*g.\r\n\r\n- Xác định hệ số tầm quan trọng, γI, tra bảng phụ lục F và G. Ví dụ với nhà cao từ 20 tầng đến 60 tầng có mức độ quan trọng là I, hệ số tầm quan trọng là γI = 1,25\r\n\r\n- Xác định gia tốc nền thiết kế, ag = agR*γI; theo mục 3.2.1 của tiêu chuẩn: nếu ag &lt; 0,08*g - trường hợp động đất yếu - thì có thể sử dụng các quy trình thiết kế chịu động đất được giảm nhẹ hoặc đơn giản hóa cho một số loại, dạng kết cấu; nếu ag &lt; 0,04*g - trường hợp động đất rất yếu - thì không cần phải tuân theo những điều khoản của tiêu chuẩn. Theo [2] thì việc thiết kế kháng chấn cho các công trình xây dựng theo các quy định đề cập tới trong nội dung của tiêu chuẩn chỉ thực hiện chủ yếu cho các công trình xây dựng trong các vùng động đất mạnh có gia tốc nền ag &gt; 0,08*g.\r\n\r\n- Xác định loại kết cấu, đối với kết cấu bê tông cốt thép xác định theo mục 5.1.2. Loại kết cấu của công trình có ảnh hưởng đến việc xác định hệ số ứng xử sẽ được đề cập ở mục 5. Một số trường hợp cần xác định bằng cách so sánh lực cắt (phản lực ngang tại liên kết với móng) mà các cấu kiện (các cột và các vách) sẽ gánh khi chịu lực ngang giả thiết.\r\n\r\n- Xác định hệ số ứng xử q, đối với kết cấu bê tông cốt thép xác định theo mục 5.2.2.2, ví dụ hệ khung, nhiều tầng, nhiều nhịp, cấp độ dẻo trung bình (DCM) có: q = 3,0*1,3 = 3,9. Hệ số ứng xử q có thể khác nhau theo hai phương chính tùy thuộc hệ kết cấu.\r\n\r\n- Xác định loại nền đất, theo mục 3.2.1, có thể sử dụng giá trị trung bình của chỉ số SPT của các lớp đất trong chiều sâu 30m. Loại nền đất có vai trò xác định các tham số mô tả phổ phản ứng gia tốc sẽ được nêu ở mục 7.\r\n\r\n- Xác định các tham số mô tả phổ phản ứng gia tốc S, TB, TC, TD, theo bảng 3.2 - mục 3.2.2.2.\r\n\r\nII. Xác định khối lượng tham gia dao động\r\n\r\nKhối lượng tham gia dao động ảnh hưởng đến chu kỳ, dạng dao động, và tải trọng động đất tác dụng lên công trình.\r\nKhối lượng tham gia dao động được xác định theo các mục 3.2.4 và 4.2.4, phụ thuộc vào tĩnh tải, hoạt tải và loại hoạt tải. Ví dụ công trình có các loại phòng như văn phòng (HTVP) và phòng họp (HTPH), thì công thức xác định khối lượng tham gia dao động sẽ bằng: TT + 0,5*0,3*HTVP + 0,5*0,6*HTPH (sử dụng hệ số 0,5 do các tầng sử dụng độc lập, không phụ thuộc lẫn nhau).\r\n\r\nIII. Xác định chu kỳ và dạng của các dạng dao động\r\n\r\nTrong một số trường hợp, tiêu chuẩn cho phép sử dụng công thức gần đúng để xác định chu kỳ và dạng dao động (xem mục 4.3.3.2.2)\r\nKhi công trình thỏa mãn tính đều đặn trong mặt bằng, có thể phân tích dao động bằng hai mô hình phẳng theo hai phương chính.\r\n\r\nIV. Xác định phổ phản ứng gia tốc thiết kế\r\n\r\nGiá trị của phổ phản ứng gia tốc thiết kế của một hệ kết cấu Sd(Ti), xác định theo mục 3.2.2.5.(4), phụ thuộc vào chu kỳ dao động riêng của hệ, hệ số ứng xử của kết cấu, gia tốc nền thiết kế, và loại nền đất.\r\n\r\nV. Xác định tải trọng động đất\r\n\r\nKhi công trình thỏa mãn các điều kiện cho trong mục 4.3.3.2.1.(2), có thể sử dụng phương pháp phân tích tĩnh lực ngang tương đương, trong mọi trường hợp có thể sử dụng phương pháp phân tích phổ phản ứng dạng dao động.\r\nNguyên tắc chung để xác định tải trọng động đất là xác định lực cắt đáy Fb ứng với mỗi dạng dao động và phân phối lên các tầng dưới dạng tải trọng ngang dựa vào dạng của các dạng dao động: Fi = Fb*(mi.yi)/∑(mj.yj), trong đó mi và yi lần lượt là khối lượng và tung độ của dạng dao động của tầng thứ i.\r\n\r\nV.1. Phương pháp phân tích tĩnh lực ngang tương đương (mục 4.3.3.2)\r\n\r\nĐiều kiện áp dụng phương pháp tĩnh lực ngang tương đương:\r\n\r\n\r\n	Chu kỳ dao động cơ bản T1 theo hai hướng chính nhỏ hơn các giá trị sau: 4.TC và 2 giây; với TC là thông số xác định dạng của phổ phản ứng gia tốc\r\n	Thỏa mãn tính đều đặn theo mặt đứng\r\n\r\n\r\nLực cắt đáy được xác định theo công thức:\r\n\r\n\r\nFb = Sd(T1).m.λ\r\n\r\n\r\nTrong đó: m là tổng khối lượng của công trình; λ là hệ số điều chỉnh, xét đến khối lượng hữu hiệu của dạng dao động cơ bản đầu tiên, khi T1 &lt; 2TC và nhà có trên 2 tầng thì λ = 0,85; các trường hợp khác λ = 1.\r\n\r\nV.2. Phương pháp phân tích phổ phản ứng dạng dao động\r\n\r\nXác định số dạng dao động cần xét đến theo mục 4.3.3.3.1. Có thể xác định theo điều kiện tổng khối lượng hữu hiệu của các dạng dao động được xét chiếm ít nhất 90% tổng khối lượng của kết cấu. Khối lượng hữu hiệu của công trình ứng với dạng dao động thứ i được xác định theo công thức\r\n\r\n\r\nMtd,i = (∑mj.yj)2/(∑mj.yj2)\r\n\r\n\r\nLực cắt đáy ứng với dạng dao động thứ i được xác định theo công thức: Fb = Mtd,i.Sd(Ti)\r\nKhi các dạng dao động được xem là độc lập (Tj &lt; 0,9*Ti), giá trị lớn nhất EE của hệ quả tác động động đất có thể lấy bằng: EE = √(∑EEi2), trong đó EEi là giá trị của hệ quả tác động động đất do dạng dao động thứ i gây ra.\r\n\r\nVI. Tổ hợp các hệ quả của các thành phần tác động động đất\r\n\r\nCác thành phần nằm ngang (thành phần theo phương X và thành phần theo phương Y) của tác động động đất phải được xem là tác động đồng thời. Các hệ quả tác động do tổ hợp các thành phần nằm ngang của tác động động đất có thể xác định bằng cách sử dụng hai tổ hợp sau:\r\n\r\n\r\n	EEdx + 0,3*EEdy\r\n	0,3*EEdx + EEdy\r\n\r\n\r\nTrong đó, EEdx và EEdy là các hệ quả tác động do đặt tác động động đất dọc theo trục X và trục Y.\r\n\r\nVII. Tổ hợp tác động động đất với các tác động khác\r\n\r\nTổ hợp nội lực có tải trọng động đất đối với trường hợp không có ứng suất trước được xác định theo công thức:\r\n\r\n\r\nTH = TT + k.HT + ĐĐ\r\n\r\n\r\nTrong đó k là hệ số tổ hợp của hoạt tải, xác định theo bảng 3.4 của tiêu chuẩn, ví dụ đối với hoạt tải khu vực văn phòng thì k = 0,3; hoạt tải của khu vực hội họp là k = 0,6.\r\n\r\nVIII. Tính toán chuyển vị\r\n\r\nChuyển vị của một điểm của hệ kết cấu gây ra bởi tác động động đất thiết kế ds được xác định dựa trên chuyển vị của cùng điểm đó của hệ kết cấu được xác định bằng phân tích tuyến tính dc:\r\n\r\n\r\nds = qd*dc\r\n\r\n\r\nTrong đó qd là hệ số ứng xử chuyển vị, giả thiết bằng hệ số ứng xử q trừ khi có các quy định khác.\r\n\r\nTài liệu tham khảo\r\n\r\n\r\n[1]. TCXDVN 375:2006. Thiết kế công trình chịu động đất.\r\n[2]. Nguyễn Lê Ninh. Động đất và thiết kế công trình chịu động đất. Nhà xuất bản Xây dựng, Hà Nội, 2009.\r\n\r\n'),
(12, '\r\nI. Xác định các thông số cơ bản\r\n\r\n http://ketcauxaydung.com/uploads/news/2015_01/dong-dat-vn.jpg dong dat vn\r\n\r\n- Xác định đỉnh gia tốc nền tham chiếu: agR, tra bảng phụ lục I. Chú ý rằng bảng tra này đã được quy đổi theo gia tốc trọng trường g, agR bằng giá trị tra bảng nhân với g, ví dụ đối với địa điểm thành phố Hồ Chí Minh - quận 4, có đỉnh gia tốc nền tham chiếu là 0,0847*g.\r\n\r\n- Xác định hệ số tầm quan trọng, γI, tra bảng phụ lục F và G. Ví dụ với nhà cao từ 20 tầng đến 60 tầng có mức độ quan trọng là I, hệ số tầm quan trọng là γI = 1,25\r\n\r\n- Xác định gia tốc nền thiết kế, ag = agR*γI; theo mục 3.2.1 của tiêu chuẩn: nếu ag &lt; 0,08*g - trường hợp động đất yếu - thì có thể sử dụng các quy trình thiết kế chịu động đất được giảm nhẹ hoặc đơn giản hóa cho một số loại, dạng kết cấu; nếu ag &lt; 0,04*g - trường hợp động đất rất yếu - thì không cần phải tuân theo những điều khoản của tiêu chuẩn. Theo [2] thì việc thiết kế kháng chấn cho các công trình xây dựng theo các quy định đề cập tới trong nội dung của tiêu chuẩn chỉ thực hiện chủ yếu cho các công trình xây dựng trong các vùng động đất mạnh có gia tốc nền ag &gt; 0,08*g.\r\n\r\n- Xác định loại kết cấu, đối với kết cấu bê tông cốt thép xác định theo mục 5.1.2. Loại kết cấu của công trình có ảnh hưởng đến việc xác định hệ số ứng xử sẽ được đề cập ở mục 5. Một số trường hợp cần xác định bằng cách so sánh lực cắt (phản lực ngang tại liên kết với móng) mà các cấu kiện (các cột và các vách) sẽ gánh khi chịu lực ngang giả thiết.\r\n\r\n- Xác định hệ số ứng xử q, đối với kết cấu bê tông cốt thép xác định theo mục 5.2.2.2, ví dụ hệ khung, nhiều tầng, nhiều nhịp, cấp độ dẻo trung bình (DCM) có: q = 3,0*1,3 = 3,9. Hệ số ứng xử q có thể khác nhau theo hai phương chính tùy thuộc hệ kết cấu.\r\n\r\n- Xác định loại nền đất, theo mục 3.2.1, có thể sử dụng giá trị trung bình của chỉ số SPT của các lớp đất trong chiều sâu 30m. Loại nền đất có vai trò xác định các tham số mô tả phổ phản ứng gia tốc sẽ được nêu ở mục 7.\r\n\r\n- Xác định các tham số mô tả phổ phản ứng gia tốc S, TB, TC, TD, theo bảng 3.2 - mục 3.2.2.2.\r\n\r\nII. Xác định khối lượng tham gia dao động\r\n\r\nKhối lượng tham gia dao động ảnh hưởng đến chu kỳ, dạng dao động, và tải trọng động đất tác dụng lên công trình.\r\nKhối lượng tham gia dao động được xác định theo các mục 3.2.4 và 4.2.4, phụ thuộc vào tĩnh tải, hoạt tải và loại hoạt tải. Ví dụ công trình có các loại phòng như văn phòng (HTVP) và phòng họp (HTPH), thì công thức xác định khối lượng tham gia dao động sẽ bằng: TT + 0,5*0,3*HTVP + 0,5*0,6*HTPH (sử dụng hệ số 0,5 do các tầng sử dụng độc lập, không phụ thuộc lẫn nhau).\r\n\r\nIII. Xác định chu kỳ và dạng của các dạng dao động\r\n\r\nTrong một số trường hợp, tiêu chuẩn cho phép sử dụng công thức gần đúng để xác định chu kỳ và dạng dao động (xem mục 4.3.3.2.2)\r\nKhi công trình thỏa mãn tính đều đặn trong mặt bằng, có thể phân tích dao động bằng hai mô hình phẳng theo hai phương chính.\r\n\r\nIV. Xác định phổ phản ứng gia tốc thiết kế\r\n\r\nGiá trị của phổ phản ứng gia tốc thiết kế của một hệ kết cấu Sd(Ti), xác định theo mục 3.2.2.5.(4), phụ thuộc vào chu kỳ dao động riêng của hệ, hệ số ứng xử của kết cấu, gia tốc nền thiết kế, và loại nền đất.\r\n\r\nV. Xác định tải trọng động đất\r\n\r\nKhi công trình thỏa mãn các điều kiện cho trong mục 4.3.3.2.1.(2), có thể sử dụng phương pháp phân tích tĩnh lực ngang tương đương, trong mọi trường hợp có thể sử dụng phương pháp phân tích phổ phản ứng dạng dao động.\r\nNguyên tắc chung để xác định tải trọng động đất là xác định lực cắt đáy Fb ứng với mỗi dạng dao động và phân phối lên các tầng dưới dạng tải trọng ngang dựa vào dạng của các dạng dao động: Fi = Fb*(mi.yi)/∑(mj.yj), trong đó mi và yi lần lượt là khối lượng và tung độ của dạng dao động của tầng thứ i.\r\n\r\nV.1. Phương pháp phân tích tĩnh lực ngang tương đương (mục 4.3.3.2)\r\n\r\nĐiều kiện áp dụng phương pháp tĩnh lực ngang tương đương:\r\n\r\n\r\n	Chu kỳ dao động cơ bản T1 theo hai hướng chính nhỏ hơn các giá trị sau: 4.TC và 2 giây; với TC là thông số xác định dạng của phổ phản ứng gia tốc\r\n	Thỏa mãn tính đều đặn theo mặt đứng\r\n\r\n\r\nLực cắt đáy được xác định theo công thức:\r\n\r\n\r\nFb = Sd(T1).m.λ\r\n\r\n\r\nTrong đó: m là tổng khối lượng của công trình; λ là hệ số điều chỉnh, xét đến khối lượng hữu hiệu của dạng dao động cơ bản đầu tiên, khi T1 &lt; 2TC và nhà có trên 2 tầng thì λ = 0,85; các trường hợp khác λ = 1.\r\n\r\nV.2. Phương pháp phân tích phổ phản ứng dạng dao động\r\n\r\nXác định số dạng dao động cần xét đến theo mục 4.3.3.3.1. Có thể xác định theo điều kiện tổng khối lượng hữu hiệu của các dạng dao động được xét chiếm ít nhất 90% tổng khối lượng của kết cấu. Khối lượng hữu hiệu của công trình ứng với dạng dao động thứ i được xác định theo công thức\r\n\r\n\r\nMtd,i = (∑mj.yj)2/(∑mj.yj2)\r\n\r\n\r\nLực cắt đáy ứng với dạng dao động thứ i được xác định theo công thức: Fb = Mtd,i.Sd(Ti)\r\nKhi các dạng dao động được xem là độc lập (Tj &lt; 0,9*Ti), giá trị lớn nhất EE của hệ quả tác động động đất có thể lấy bằng: EE = √(∑EEi2), trong đó EEi là giá trị của hệ quả tác động động đất do dạng dao động thứ i gây ra.\r\n\r\nVI. Tổ hợp các hệ quả của các thành phần tác động động đất\r\n\r\nCác thành phần nằm ngang (thành phần theo phương X và thành phần theo phương Y) của tác động động đất phải được xem là tác động đồng thời. Các hệ quả tác động do tổ hợp các thành phần nằm ngang của tác động động đất có thể xác định bằng cách sử dụng hai tổ hợp sau:\r\n\r\n\r\n	EEdx + 0,3*EEdy\r\n	0,3*EEdx + EEdy\r\n\r\n\r\nTrong đó, EEdx và EEdy là các hệ quả tác động do đặt tác động động đất dọc theo trục X và trục Y.\r\n\r\nVII. Tổ hợp tác động động đất với các tác động khác\r\n\r\nTổ hợp nội lực có tải trọng động đất đối với trường hợp không có ứng suất trước được xác định theo công thức:\r\n\r\n\r\nTH = TT + k.HT + ĐĐ\r\n\r\n\r\nTrong đó k là hệ số tổ hợp của hoạt tải, xác định theo bảng 3.4 của tiêu chuẩn, ví dụ đối với hoạt tải khu vực văn phòng thì k = 0,3; hoạt tải của khu vực hội họp là k = 0,6.\r\n\r\nVIII. Tính toán chuyển vị\r\n\r\nChuyển vị của một điểm của hệ kết cấu gây ra bởi tác động động đất thiết kế ds được xác định dựa trên chuyển vị của cùng điểm đó của hệ kết cấu được xác định bằng phân tích tuyến tính dc:\r\n\r\n\r\nds = qd*dc\r\n\r\n\r\nTrong đó qd là hệ số ứng xử chuyển vị, giả thiết bằng hệ số ứng xử q trừ khi có các quy định khác.\r\n\r\nTài liệu tham khảo\r\n\r\n\r\n[1]. TCXDVN 375:2006. Thiết kế công trình chịu động đất.\r\n[2]. Nguyễn Lê Ninh. Động đất và thiết kế công trình chịu động đất. Nhà xuất bản Xây dựng, Hà Nội, 2009.\r\n\r\n');
INSERT INTO `kcxd_vi_news_bodytext` (`id`, `bodytext`) VALUES
(13, '\r\nI. Xác định các thông số cơ bản\r\n\r\n http://ketcauxaydung.com/uploads/news/2015_01/dong-dat-vn.jpg dong dat vn\r\n\r\n- Xác định đỉnh gia tốc nền tham chiếu: agR, tra bảng phụ lục I. Chú ý rằng bảng tra này đã được quy đổi theo gia tốc trọng trường g, agR bằng giá trị tra bảng nhân với g, ví dụ đối với địa điểm thành phố Hồ Chí Minh - quận 4, có đỉnh gia tốc nền tham chiếu là 0,0847*g.\r\n\r\n- Xác định hệ số tầm quan trọng, γI, tra bảng phụ lục F và G. Ví dụ với nhà cao từ 20 tầng đến 60 tầng có mức độ quan trọng là I, hệ số tầm quan trọng là γI = 1,25\r\n\r\n- Xác định gia tốc nền thiết kế, ag = agR*γI; theo mục 3.2.1 của tiêu chuẩn: nếu ag &lt; 0,08*g - trường hợp động đất yếu - thì có thể sử dụng các quy trình thiết kế chịu động đất được giảm nhẹ hoặc đơn giản hóa cho một số loại, dạng kết cấu; nếu ag &lt; 0,04*g - trường hợp động đất rất yếu - thì không cần phải tuân theo những điều khoản của tiêu chuẩn. Theo [2] thì việc thiết kế kháng chấn cho các công trình xây dựng theo các quy định đề cập tới trong nội dung của tiêu chuẩn chỉ thực hiện chủ yếu cho các công trình xây dựng trong các vùng động đất mạnh có gia tốc nền ag &gt; 0,08*g.\r\n\r\n- Xác định loại kết cấu, đối với kết cấu bê tông cốt thép xác định theo mục 5.1.2. Loại kết cấu của công trình có ảnh hưởng đến việc xác định hệ số ứng xử sẽ được đề cập ở mục 5. Một số trường hợp cần xác định bằng cách so sánh lực cắt (phản lực ngang tại liên kết với móng) mà các cấu kiện (các cột và các vách) sẽ gánh khi chịu lực ngang giả thiết.\r\n\r\n- Xác định hệ số ứng xử q, đối với kết cấu bê tông cốt thép xác định theo mục 5.2.2.2, ví dụ hệ khung, nhiều tầng, nhiều nhịp, cấp độ dẻo trung bình (DCM) có: q = 3,0*1,3 = 3,9. Hệ số ứng xử q có thể khác nhau theo hai phương chính tùy thuộc hệ kết cấu.\r\n\r\n- Xác định loại nền đất, theo mục 3.2.1, có thể sử dụng giá trị trung bình của chỉ số SPT của các lớp đất trong chiều sâu 30m. Loại nền đất có vai trò xác định các tham số mô tả phổ phản ứng gia tốc sẽ được nêu ở mục 7.\r\n\r\n- Xác định các tham số mô tả phổ phản ứng gia tốc S, TB, TC, TD, theo bảng 3.2 - mục 3.2.2.2.\r\n\r\nII. Xác định khối lượng tham gia dao động\r\n\r\nKhối lượng tham gia dao động ảnh hưởng đến chu kỳ, dạng dao động, và tải trọng động đất tác dụng lên công trình.\r\nKhối lượng tham gia dao động được xác định theo các mục 3.2.4 và 4.2.4, phụ thuộc vào tĩnh tải, hoạt tải và loại hoạt tải. Ví dụ công trình có các loại phòng như văn phòng (HTVP) và phòng họp (HTPH), thì công thức xác định khối lượng tham gia dao động sẽ bằng: TT + 0,5*0,3*HTVP + 0,5*0,6*HTPH (sử dụng hệ số 0,5 do các tầng sử dụng độc lập, không phụ thuộc lẫn nhau).\r\n\r\nIII. Xác định chu kỳ và dạng của các dạng dao động\r\n\r\nTrong một số trường hợp, tiêu chuẩn cho phép sử dụng công thức gần đúng để xác định chu kỳ và dạng dao động (xem mục 4.3.3.2.2)\r\nKhi công trình thỏa mãn tính đều đặn trong mặt bằng, có thể phân tích dao động bằng hai mô hình phẳng theo hai phương chính.\r\n\r\nIV. Xác định phổ phản ứng gia tốc thiết kế\r\n\r\nGiá trị của phổ phản ứng gia tốc thiết kế của một hệ kết cấu Sd(Ti), xác định theo mục 3.2.2.5.(4), phụ thuộc vào chu kỳ dao động riêng của hệ, hệ số ứng xử của kết cấu, gia tốc nền thiết kế, và loại nền đất.\r\n\r\nV. Xác định tải trọng động đất\r\n\r\nKhi công trình thỏa mãn các điều kiện cho trong mục 4.3.3.2.1.(2), có thể sử dụng phương pháp phân tích tĩnh lực ngang tương đương, trong mọi trường hợp có thể sử dụng phương pháp phân tích phổ phản ứng dạng dao động.\r\nNguyên tắc chung để xác định tải trọng động đất là xác định lực cắt đáy Fb ứng với mỗi dạng dao động và phân phối lên các tầng dưới dạng tải trọng ngang dựa vào dạng của các dạng dao động: Fi = Fb*(mi.yi)/∑(mj.yj), trong đó mi và yi lần lượt là khối lượng và tung độ của dạng dao động của tầng thứ i.\r\n\r\nV.1. Phương pháp phân tích tĩnh lực ngang tương đương (mục 4.3.3.2)\r\n\r\nĐiều kiện áp dụng phương pháp tĩnh lực ngang tương đương:\r\n\r\n\r\n	Chu kỳ dao động cơ bản T1 theo hai hướng chính nhỏ hơn các giá trị sau: 4.TC và 2 giây; với TC là thông số xác định dạng của phổ phản ứng gia tốc\r\n	Thỏa mãn tính đều đặn theo mặt đứng\r\n\r\n\r\nLực cắt đáy được xác định theo công thức:\r\n\r\n\r\nFb = Sd(T1).m.λ\r\n\r\n\r\nTrong đó: m là tổng khối lượng của công trình; λ là hệ số điều chỉnh, xét đến khối lượng hữu hiệu của dạng dao động cơ bản đầu tiên, khi T1 &lt; 2TC và nhà có trên 2 tầng thì λ = 0,85; các trường hợp khác λ = 1.\r\n\r\nV.2. Phương pháp phân tích phổ phản ứng dạng dao động\r\n\r\nXác định số dạng dao động cần xét đến theo mục 4.3.3.3.1. Có thể xác định theo điều kiện tổng khối lượng hữu hiệu của các dạng dao động được xét chiếm ít nhất 90% tổng khối lượng của kết cấu. Khối lượng hữu hiệu của công trình ứng với dạng dao động thứ i được xác định theo công thức\r\n\r\n\r\nMtd,i = (∑mj.yj)2/(∑mj.yj2)\r\n\r\n\r\nLực cắt đáy ứng với dạng dao động thứ i được xác định theo công thức: Fb = Mtd,i.Sd(Ti)\r\nKhi các dạng dao động được xem là độc lập (Tj &lt; 0,9*Ti), giá trị lớn nhất EE của hệ quả tác động động đất có thể lấy bằng: EE = √(∑EEi2), trong đó EEi là giá trị của hệ quả tác động động đất do dạng dao động thứ i gây ra.\r\n\r\nVI. Tổ hợp các hệ quả của các thành phần tác động động đất\r\n\r\nCác thành phần nằm ngang (thành phần theo phương X và thành phần theo phương Y) của tác động động đất phải được xem là tác động đồng thời. Các hệ quả tác động do tổ hợp các thành phần nằm ngang của tác động động đất có thể xác định bằng cách sử dụng hai tổ hợp sau:\r\n\r\n\r\n	EEdx + 0,3*EEdy\r\n	0,3*EEdx + EEdy\r\n\r\n\r\nTrong đó, EEdx và EEdy là các hệ quả tác động do đặt tác động động đất dọc theo trục X và trục Y.\r\n\r\nVII. Tổ hợp tác động động đất với các tác động khác\r\n\r\nTổ hợp nội lực có tải trọng động đất đối với trường hợp không có ứng suất trước được xác định theo công thức:\r\n\r\n\r\nTH = TT + k.HT + ĐĐ\r\n\r\n\r\nTrong đó k là hệ số tổ hợp của hoạt tải, xác định theo bảng 3.4 của tiêu chuẩn, ví dụ đối với hoạt tải khu vực văn phòng thì k = 0,3; hoạt tải của khu vực hội họp là k = 0,6.\r\n\r\nVIII. Tính toán chuyển vị\r\n\r\nChuyển vị của một điểm của hệ kết cấu gây ra bởi tác động động đất thiết kế ds được xác định dựa trên chuyển vị của cùng điểm đó của hệ kết cấu được xác định bằng phân tích tuyến tính dc:\r\n\r\n\r\nds = qd*dc\r\n\r\n\r\nTrong đó qd là hệ số ứng xử chuyển vị, giả thiết bằng hệ số ứng xử q trừ khi có các quy định khác.\r\n\r\nTài liệu tham khảo\r\n\r\n\r\n[1]. TCXDVN 375:2006. Thiết kế công trình chịu động đất.\r\n[2]. Nguyễn Lê Ninh. Động đất và thiết kế công trình chịu động đất. Nhà xuất bản Xây dựng, Hà Nội, 2009.\r\n\r\n'),
(18, '\r\n\r\nhttps://docs.google.com/file/d/0B9GzJKouH1IVRWdEelJvTkliekk/edit\r\n\r\nhttps://docs.google.com/file/d/0B9GzJKouH1IVRWdEelJvTkliekk/edit');

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_news_cat`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_news_cat` (
  `catid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `titlesite` varchar(255) DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `descriptionhtml` text,
  `image` varchar(255) DEFAULT '',
  `viewdescription` tinyint(2) NOT NULL DEFAULT '0',
  `weight` smallint(5) unsigned NOT NULL DEFAULT '0',
  `sort` smallint(5) NOT NULL DEFAULT '0',
  `lev` smallint(5) NOT NULL DEFAULT '0',
  `viewcat` varchar(50) NOT NULL DEFAULT 'viewcat_page_new',
  `numsubcat` smallint(5) NOT NULL DEFAULT '0',
  `subcatid` varchar(255) DEFAULT '',
  `inhome` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `numlinks` tinyint(2) unsigned NOT NULL DEFAULT '3',
  `newday` tinyint(2) unsigned NOT NULL DEFAULT '2',
  `keywords` text,
  `admins` text,
  `add_time` int(11) unsigned NOT NULL DEFAULT '0',
  `edit_time` int(11) unsigned NOT NULL DEFAULT '0',
  `groups_view` varchar(255) DEFAULT '',
  PRIMARY KEY (`catid`),
  UNIQUE KEY `alias` (`alias`),
  KEY `parentid` (`parentid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `kcxd_vi_news_cat`
--

INSERT INTO `kcxd_vi_news_cat` (`catid`, `parentid`, `title`, `titlesite`, `alias`, `description`, `descriptionhtml`, `image`, `viewdescription`, `weight`, `sort`, `lev`, `viewcat`, `numsubcat`, `subcatid`, `inhome`, `numlinks`, `newday`, `keywords`, `admins`, `add_time`, `edit_time`, `groups_view`) VALUES
(13, 0, 'Ứng dụng tin học trong thiết kế', 'Ứng dụng tin học trong thiết kế xây dựng', 'ung-dung-tin-hoc-trong-thiet-ke', 'Ứng dụng tin học vào thiết kế kết cấu xây dựng', '', '', 0, 6, 6, 0, 'viewcat_page_new', 0, '', 1, 5, 2, 'etabs, sap, staad pro, xsteel, safe, rdw', '', 1421252661, 1421252661, '6'),
(14, 0, 'Phong thủy', 'Phong thủy trong kiến trúc xây dựng', 'phong-thuy', 'Phong thủy trong kiến trúc xây dựng', '', '', 0, 4, 4, 0, 'viewcat_page_new', 0, '', 1, 5, 2, 'phong thủy, phong thủy xây dựng, chọn hướng nhà, phong thủy phòng ngủ, phong thủy phòng khách', '', 1421252790, 1421252790, '6'),
(15, 0, 'Kiến trúc đẹp', 'Kiến trúc nhà đẹp, kiến trúc cổ, kiến trúc hiện đại', 'kien-truc-dep', 'Kiến trúc nhà đẹp, kiến trúc cổ, kiến trúc hiện đại, tư vấn kiến trúc', '', '', 0, 3, 3, 0, 'viewcat_page_new', 0, '', 1, 3, 2, 'kiến trúc, nhà đẹp, tư vấn kiến trúc, kiến trúc xưa, kiến trúc hiện đại', '', 1421252887, 1421252887, '6'),
(16, 0, 'Nền móng', 'Thiết kế nền móng công trình và địa kỹ thuật', 'nen-mong', '- Thiết kế móng&#x3A; móng đơn, móng băng, móng bè, móng cọc đóng, cọc ép, cọc khoan nhồi, dầm móng, phương pháp tính toán và công nghệ gia cố nền đất yếu', '', '', 0, 2, 2, 0, 'viewcat_page_new', 0, '', 1, 3, 2, 'móng đơn, móng băng, móng bè, móng cọc đóng, cọc ép, cọc khoan nhồi, dầm móng, gia cố nền đất', '', 1421252995, 1421252995, '6'),
(17, 0, 'Kết cấu', 'Kết cấu công trình bê tông cốt thép, kết cấu thép nhà công nghiệp, nhà cao tầng, tải trọng tác dụng', 'ket-cau', 'Kết cấu thép, nhà công nghiệp, thép nhà cao tầng', '', '', 0, 1, 1, 0, 'viewcat_page_new', 0, '', 1, 3, 2, 'tải trọng tác dụng, kết cấu thép, kết cấu khung, kết cấu vách, vách cứng, kết cấu lõi, gạch đá, btct nhà cao tầng, bê tông ứng lực trước', '', 1421253270, 1421253270, '6'),
(18, 0, 'Vật liệu xây dựng', 'Vật liệu mới trong xây dựng', 'vat-lieu-xay-dung', 'Vật liệu trong xây dựng, vật liệu mới, vật liệu bền, vật liệu thay thế', '', '', 0, 5, 5, 0, 'viewcat_page_new', 0, '', 1, 3, 2, 'vật liệu mới trong xây dựng, vật liệu xây dựng, vật liệu rẻ, vật liệu bền', '', 1421253387, 1421253387, '6');

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_news_config_post`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_news_config_post` (
  `group_id` smallint(5) NOT NULL,
  `addcontent` tinyint(4) NOT NULL,
  `postcontent` tinyint(4) NOT NULL,
  `editcontent` tinyint(4) NOT NULL,
  `delcontent` tinyint(4) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kcxd_vi_news_config_post`
--

INSERT INTO `kcxd_vi_news_config_post` (`group_id`, `addcontent`, `postcontent`, `editcontent`, `delcontent`) VALUES
(5, 0, 0, 0, 0),
(4, 0, 0, 0, 0),
(1, 0, 0, 0, 0),
(2, 0, 0, 0, 0),
(3, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_news_rows`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_news_rows` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `listcatid` varchar(255) NOT NULL DEFAULT '',
  `topicid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `admin_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `author` varchar(255) DEFAULT '',
  `sourceid` mediumint(8) NOT NULL DEFAULT '0',
  `addtime` int(11) unsigned NOT NULL DEFAULT '0',
  `edittime` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `publtime` int(11) unsigned NOT NULL DEFAULT '0',
  `exptime` int(11) unsigned NOT NULL DEFAULT '0',
  `archive` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `hometext` text NOT NULL,
  `homeimgfile` varchar(255) DEFAULT '',
  `homeimgalt` varchar(255) DEFAULT '',
  `homeimgthumb` tinyint(4) NOT NULL DEFAULT '0',
  `inhome` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `allowed_comm` varchar(255) DEFAULT '',
  `allowed_rating` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hitstotal` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `hitscm` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `total_rating` int(11) NOT NULL DEFAULT '0',
  `click_rating` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`),
  KEY `topicid` (`topicid`),
  KEY `admin_id` (`admin_id`),
  KEY `author` (`author`),
  KEY `title` (`title`),
  KEY `addtime` (`addtime`),
  KEY `publtime` (`publtime`),
  KEY `exptime` (`exptime`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `kcxd_vi_news_rows`
--

INSERT INTO `kcxd_vi_news_rows` (`id`, `catid`, `listcatid`, `topicid`, `admin_id`, `author`, `sourceid`, `addtime`, `edittime`, `status`, `publtime`, `exptime`, `archive`, `title`, `alias`, `hometext`, `homeimgfile`, `homeimgalt`, `homeimgthumb`, `inhome`, `allowed_comm`, `allowed_rating`, `hitstotal`, `hitscm`, `total_rating`, `click_rating`) VALUES
(17, 17, '17,15,18', 2, 1, 'ST', 0, 1421511824, 1421573822, 1, 1421511824, 0, 2, 'Phần mềm xây dựng', 'Phan-mem-xay-dung', 'Giới thiệu ngắn gọn (Hiển thị đối với mọi đối tượng kể cả khi không được phân quyền). Số ký tự: 0. Nên nhập tối đa 160 ký tự', '2015_01/cayxanhphongthuy.jpg', 'aaaa', 1, 1, '6', 0, 5, 0, 5, 1),
(15, 16, '16,14,13', 4, 1, 'ST', 0, 1421255177, 1421255201, 1, 1421255177, 0, 2, 'Kiểm tra Vách phẳng theo giả thiết vùng biên chịu mô men', 'Kiem-tra-Vach-phang-theo-gia-thiet-vung-bien-chiu-mo-men', 'Giới thiệu về phương pháp tính toán kiểm tra vách phẳng dựa trên giả thiết vùng biên chịu mô men. File excel cũng được lập và giới thiệu để các bạn tham khảo.', '2015_01/kiem-tra-vach.jpg', 'Chú thích cho hình', 1, 1, '6', 0, 5, 0, 0, 0),
(16, 17, '17,15,18', 2, 1, 'ST', 0, 1421255405, 1421532694, 1, 1421255405, 0, 2, 'Kỹ sư thậm chí có thể tính nhẩm khi sử dụng phương pháp này.', 'Ky-su-tham-chi-co-the-tinh-nham-khi-su-dung-phuong-phap-nay', 'Phương pháp này dựa trên giả thiết cơ bản và mô men uốn sẽ được phân phối thành cặp ngẫu lực do các vùng biên chịu. Lực dọc sẽ phân phối một ứng suất nén', '2015_01/dong-dat-vn.jpg', 'kc', 1, 1, '6', 1, 8, 0, 5, 1),
(13, 17, '17,15,18', 3, 1, 'ST', 0, 1421254741, 1421254741, 1, 1421254741, 0, 2, 'Bố trí cây xanh quanh nhà theo phong thủy', 'Bo-tri-cay-xanh-quanh-nha-theo-phong-thuy', 'Thuật phong thủy trong việc chọn đất làm nhà ngoài việc chú ý xem tướng đất, đặt cuộc đất trong mối liên quan với núi, sông, đường xá, nhà cửa…', '2015_01/cayxanhphongthuy.jpg', 'aaaa', 1, 1, '6', 1, 2, 0, 0, 0),
(14, 16, '16,14,13', 2, 1, 'ST', 0, 1421254983, 1421560969, 1, 1421254983, 0, 2, 'Trang trí tiểu cảnh hợp phong thủy tăng thêm an lành', 'Trang-tri-tieu-canh-hop-phong-thuy-tang-them-an-lanh', 'Thiết kế sân vườn - tiểu cảnh hợp phong thủy vừa là cách bạn mang thiên nhiên vào nhà vừa là cách để hóa giải những khiếm khuyết về phong thủy cho ngôi nhà.', '2015_01/phong-thuy.jpg', 'jjjjjj', 1, 1, '6', 0, 2, 0, 0, 0),
(11, 17, '17,15,18', 2, 1, 'ST', 0, 1421254180, 1421254180, 1, 1421254180, 0, 2, 'Tính toán tải trọng động đất theo TCXDVN 375&#x3A;2006', 'Tinh-toan-tai-trong-dong-dat-theo-TCXDVN-375-2006', 'Bài viết này tóm tắt quy trình tính toán tải trọng động đất theo Tiêu chuẩn xây dựng Việt Nam 375 - 2006 (TCXDVN 375:2006)', '2015_01/dong-dat-vn.jpg', 'kc', 1, 1, '4', 1, 2, 0, 0, 0),
(12, 16, '16,14,13', 3, 1, 'ST', 0, 1421254382, 1421651132, 1, 1421254382, 0, 2, 'Thuật phong thủy và việc chọn đất làm nhà', 'Thuat-phong-thuy-va-viec-chon-dat-lam-nha', 'Ngày xưa, Phạm Lãi giúp Câu Tiễn phục quốc, trải bao tù đày, khổ nhục, nếm mật, nằm gai, cuối cùng đã đánh bại Phù Sai, rửa được cái nhục lớn ở núi Cối Kê.', '2015_01/chon-dat-lam-nha-1.jpg', 'phong thuy', 1, 1, '6', 0, 1, 0, 0, 0),
(18, 17, '17', 2, 1, 'Kc', 0, 1422753399, 1422753638, 1, 1422753399, 0, 2, 'Tính toán cấu kiện chịu xoắn theo ACI 318M-08', 'Tinh-toan-cau-kien-chiu-xoan-theo-ACI-318M-08', 'Giới thiệu ngắn gọn (Hiển thị đối với mọi đối tượng kể cả khi không được phân quyền). Số ký tự: 0. Nên nhập tối đa 160 ký tự', '2015_02/message.png', 'Kiểm tra Vách phẳng theo giả thiết vùng biên chịu mô men', 1, 1, '4', 1, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_news_sources`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_news_sources` (
  `sourceid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `link` varchar(255) DEFAULT '',
  `logo` varchar(255) DEFAULT '',
  `weight` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `add_time` int(11) unsigned NOT NULL,
  `edit_time` int(11) unsigned NOT NULL,
  PRIMARY KEY (`sourceid`),
  UNIQUE KEY `title` (`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `kcxd_vi_news_sources`
--

INSERT INTO `kcxd_vi_news_sources` (`sourceid`, `title`, `link`, `logo`, `weight`, `add_time`, `edit_time`) VALUES
(6, 'Kết cấu', 'http://ketcau.com', '', 2, 1421253515, 1421253515),
(5, 'Báo xây dựng', 'http://baoxaydung.com', '', 1, 1421253493, 1421253493);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_news_tags`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_news_tags` (
  `tid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `numnews` mediumint(8) NOT NULL DEFAULT '0',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) DEFAULT '',
  `description` text,
  `keywords` varchar(255) DEFAULT '',
  PRIMARY KEY (`tid`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=58 ;

--
-- Dumping data for table `kcxd_vi_news_tags`
--

INSERT INTO `kcxd_vi_news_tags` (`tid`, `numnews`, `alias`, `image`, `description`, `keywords`) VALUES
(1, 0, 'nguồn-mở', '', '', 'nguồn mở'),
(2, 0, 'quen-thuộc', '', '', 'quen thuộc'),
(3, 0, 'cộng-đồng', '', '', 'cộng đồng'),
(4, 0, 'việt-nam', '', '', 'việt nam'),
(5, 0, 'hoạt-động', '', '', 'hoạt động'),
(6, 0, 'tin-tức', '', '', 'tin tức'),
(7, 0, 'thương-mại-điện', '', '', 'thương mại điện'),
(8, 0, 'điện-tử', '', '', 'điện tử'),
(9, 0, 'nukeviet', '', '', 'nukeviet'),
(10, 0, 'nền-tảng', '', '', 'nền tảng'),
(11, 0, 'xây-dựng', '', '', 'xây dựng'),
(12, 1, 'quản-trị', '', '', 'quản trị'),
(13, 1, 'nội-dung', '', '', 'nội dung'),
(14, 1, 'sử-dụng', '', '', 'sử dụng'),
(15, 1, 'khả-năng', '', '', 'khả năng'),
(16, 1, 'tích-hợp', '', '', 'tích hợp'),
(17, 1, 'ứng-dụng', '', '', 'ứng dụng'),
(18, 0, 'vinades', '', '', 'vinades'),
(19, 0, 'lập-trình-viên', '', '', 'lập trình viên'),
(20, 0, 'chuyên-viên-đồ-họa', '', '', 'chuyên viên đồ họa'),
(21, 0, 'php', '', '', 'php'),
(22, 0, 'mysql', '', '', 'mysql'),
(23, 0, 'khai-trương', '', '', 'khai trương'),
(24, 0, 'khuyến-mại', '', '', 'khuyến mại'),
(25, 0, 'giảm-giá', '', '', 'giảm giá'),
(26, 0, 'siêu-khuyến-mại', '', '', 'siêu khuyến mại'),
(27, 0, 'webnhanh', '', '', 'webnhanh'),
(28, 0, 'thiết-kế-website', '', '', 'thiết kế website'),
(29, 0, 'giao-diện-web', '', '', 'giao diện web'),
(30, 0, 'thiết-kế-web', '', '', 'thiết kế web'),
(31, 0, 'nhân-tài-đất-việt-2011', '', '', 'nhân tài đất việt 2011'),
(32, 0, 'mã-nguồn-mở', '', '', 'mã nguồn mở'),
(33, 1, 'tcxdvn-375', '', '', 'tcxdvn 375'),
(34, 1, 'tính-toán-động-đất', '', '', 'tính toán động đất'),
(35, 1, 'tải-trọng-động-đất', '', '', 'tải trọng động đất'),
(36, 1, 'tính-động-đất-tcxdvn-375', '', '', 'tính động đất tcxdvn 375'),
(37, 2, 'phong-thuỷ', '', '', 'phong thuỷ'),
(38, 1, 'phong-thuỷ-chọn-đất', '', '', 'phong thuỷ chọn đất'),
(39, 1, 'phong-thuỷ-làm-nhà', '', '', 'phong thuỷ làm nhà'),
(40, 1, 'phong-thuỷ-hướng-nhà', '', '', 'phong thuỷ hướng nhà'),
(41, 1, 'phong-thiuym', '', '', 'phong thiuym'),
(42, 1, 'phongg-nthiu', '', '', 'phongg nthiu'),
(43, 1, 'dáldksjadl', '', '', 'dáldksjadl'),
(44, 1, 'faffffffffffffffffff', '', '', 'faffffffffffffffffff'),
(45, 1, 'fffffffffffffffffffffffff', '', '', 'fffffffffffffffffffffffff'),
(46, 1, 'àasffasfs', '', '', 'àasffasfs'),
(47, 1, 'jfjhfjhfjh', '', '', 'jfjhfjhfjh'),
(48, 1, 'jyfjjjjjjjjjjj', '', '', 'jyfjjjjjjjjjjj'),
(49, 1, 'fhddhdhdh', '', '', 'fhddhdhdh'),
(50, 1, 'djdhdjfh', '', '', 'djdhdjfh'),
(51, 1, 'jdfhngdgndn', '', '', 'jdfhngdgndn'),
(52, 1, 'ghhhhhhhhhhh', '', '', 'ghhhhhhhhhhh'),
(53, 1, 'hgfhfj', '', '', 'hgfhfj'),
(54, 1, 'hggkgjk', '', '', 'hggkgjk'),
(55, 1, 'ncncn', '', '', 'ncncn'),
(56, 1, 'dchfjj', '', '', 'dchfjj'),
(57, 1, 'hfjfvfjhj', '', '', 'hfjfvfjhj');

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_news_tags_id`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_news_tags_id` (
  `id` int(11) NOT NULL,
  `tid` mediumint(9) NOT NULL,
  `keyword` varchar(65) NOT NULL,
  UNIQUE KEY `sid` (`id`,`tid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kcxd_vi_news_tags_id`
--

INSERT INTO `kcxd_vi_news_tags_id` (`id`, `tid`, `keyword`) VALUES
(16, 56, 'dchfjj'),
(16, 55, 'ncncn'),
(15, 54, 'hggkgjk'),
(15, 53, 'hgfhfj'),
(15, 52, 'ghhhhhhhhhhh'),
(14, 51, 'jdfhngdgndn'),
(14, 50, 'djdhdjfh'),
(14, 49, 'fhddhdhdh'),
(14, 47, 'jfjhfjhfjh'),
(13, 46, 'àasffasfs'),
(14, 48, 'jyfjjjjjjjjjjj'),
(13, 45, 'fffffffffffffffffffffffff'),
(13, 44, 'faffffffffffffffffff'),
(13, 43, 'dáldksjadl'),
(13, 42, 'phongg nthiu'),
(13, 37, 'phong thuỷ'),
(12, 41, 'phong thiuym'),
(12, 40, 'phong thuỷ hướng nhà'),
(12, 39, 'phong thuỷ làm nhà'),
(12, 38, 'phong thuỷ chọn đất'),
(12, 37, 'phong thuỷ'),
(11, 36, 'tính động đất tcxdvn 375'),
(11, 35, 'tải trọng động đất'),
(11, 34, 'tính toán động đất'),
(11, 33, 'tcxdvn 375'),
(16, 57, 'hfjfvfjhj');

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_news_topics`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_news_topics` (
  `topicid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) DEFAULT '',
  `description` varchar(255) DEFAULT '',
  `weight` smallint(5) NOT NULL DEFAULT '0',
  `keywords` text,
  `add_time` int(11) NOT NULL DEFAULT '0',
  `edit_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`topicid`),
  UNIQUE KEY `title` (`title`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `kcxd_vi_news_topics`
--

INSERT INTO `kcxd_vi_news_topics` (`topicid`, `title`, `alias`, `image`, `description`, `weight`, `keywords`, `add_time`, `edit_time`) VALUES
(2, 'Kết cấu', 'Ket-cau', '', 'Kết cấu', 1, 'Kết cấu', 1421254180, 1421254180),
(3, 'Phong thuỷ', 'Phong-thuy', '', 'Phong thuỷ', 2, 'Phong thuỷ', 1421254382, 1421254382),
(4, 'kkkkkkkkkkk', 'kkkkkkkkkkk', '', 'kkkkkkkkkkk', 3, 'kkkkkkkkkkk', 1421255177, 1421255177);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_page`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_page` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT '',
  `imagealt` varchar(255) DEFAULT '',
  `description` text,
  `bodytext` mediumtext NOT NULL,
  `keywords` text,
  `socialbutton` tinyint(4) NOT NULL DEFAULT '0',
  `activecomm` varchar(255) DEFAULT '',
  `layout_func` varchar(100) DEFAULT '',
  `gid` mediumint(9) NOT NULL DEFAULT '0',
  `weight` smallint(4) NOT NULL DEFAULT '0',
  `admin_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `add_time` int(11) NOT NULL DEFAULT '0',
  `edit_time` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `kcxd_vi_page`
--

INSERT INTO `kcxd_vi_page` (`id`, `title`, `alias`, `image`, `imagealt`, `description`, `bodytext`, `keywords`, `socialbutton`, `activecomm`, `layout_func`, `gid`, `weight`, `admin_id`, `add_time`, `edit_time`, `status`) VALUES
(1, 'Số ký tự&#x3A; 0. Nên nhập tối đa 65 ký tự', 'So-ky-tu-0-Nen-nhap-toi-da-65-ky-tu', 'tap-chi-so-4.jpg', 'Tap chi so 4', 'Số ký tự&#x3A; 0. Nên nhập tối đa 160 ký tự   Số ký tự&#x3A; 0. Nên nhập tối đa 65 ký tự', '<p>Số ký tự: 0. Nên nhập tối đa 160 ký tự&nbsp;&nbsp; Số ký tự: 0. Nên nhập tối đa 65 ký tự Số ký tự: 0. Nên nhập tối đa 160 ký tự&nbsp;&nbsp; Số ký tự: 0. Nên nhập tối đa 65 ký tự</p>\r\n\r\n<p>Số ký tự: 0. Nên nhập tối đa 160 ký tự&nbsp;&nbsp; Số ký tự: 0. Nên nhập tối đa 65 ký tự</p>', 'tối đa', 1, '4', '', 0, 1, 1, 1422293937, 1422293937, 1);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_page_config`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_page_config` (
  `config_name` varchar(30) NOT NULL,
  `config_value` varchar(255) NOT NULL,
  UNIQUE KEY `config_name` (`config_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kcxd_vi_page_config`
--

INSERT INTO `kcxd_vi_page_config` (`config_name`, `config_value`) VALUES
('viewtype', '0'),
('facebookapi', '709044099210291');

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_referer_stats`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_referer_stats` (
  `host` varchar(255) NOT NULL,
  `total` int(11) NOT NULL DEFAULT '0',
  `month01` int(11) NOT NULL DEFAULT '0',
  `month02` int(11) NOT NULL DEFAULT '0',
  `month03` int(11) NOT NULL DEFAULT '0',
  `month04` int(11) NOT NULL DEFAULT '0',
  `month05` int(11) NOT NULL DEFAULT '0',
  `month06` int(11) NOT NULL DEFAULT '0',
  `month07` int(11) NOT NULL DEFAULT '0',
  `month08` int(11) NOT NULL DEFAULT '0',
  `month09` int(11) NOT NULL DEFAULT '0',
  `month10` int(11) NOT NULL DEFAULT '0',
  `month11` int(11) NOT NULL DEFAULT '0',
  `month12` int(11) NOT NULL DEFAULT '0',
  `last_update` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `host` (`host`),
  KEY `total` (`total`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_searchkeys`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_searchkeys` (
  `id` varchar(32) NOT NULL DEFAULT '',
  `skey` varchar(255) NOT NULL,
  `total` int(11) NOT NULL DEFAULT '0',
  `search_engine` varchar(50) NOT NULL,
  KEY `id` (`id`),
  KEY `skey` (`skey`),
  KEY `search_engine` (`search_engine`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_vanban`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_vanban` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `time` int(11) DEFAULT '0',
  `tomtat` mediumtext,
  `chitiet` mediumtext,
  `idcat` int(11) DEFAULT '0',
  `idroom` int(11) DEFAULT '0',
  `idfield` int(11) DEFAULT '0',
  `idorgan` int(11) DEFAULT '0',
  `copyr` varchar(255) DEFAULT NULL,
  `file` varchar(225) DEFAULT NULL,
  `type` varchar(15) DEFAULT NULL,
  `hits` int(20) DEFAULT '0',
  `dows` int(20) DEFAULT '0',
  `active` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `kcxd_vi_vanban`
--

INSERT INTO `kcxd_vi_vanban` (`id`, `name`, `alias`, `time`, `tomtat`, `chitiet`, `idcat`, `idroom`, `idfield`, `idorgan`, `copyr`, `file`, `type`, `hits`, `dows`, `active`) VALUES
(2, 'Giá vật liệu xây dựng Hà Nội tháng 12.2014', 'gia-vat-lieu-xay-dung-ha-noi-thang-12-2014', 1422464400, 'Giá vật liệu xây dựng Hà Nội tháng 12.2014', '', 2, 2, 2, 1, 'Trần Văn Thi', '/lam/uploads/vanban/tcvn-9386-2012-thiet-ke-cong-trinh-chiu-dong-dat.zip', 'zip', 6, 0, 1),
(3, 'Giá vật liệu xây dựng Nam Định tháng 12 năm 2014', 'gia-vat-lieu-xay-dung-nam-dinh-thang-12-nam-2014', 1422464400, 'Giá vật liệu xây dựng Nam Định tháng 12 năm 2014', '', 3, 3, 3, 1, '', '/lam/uploads/vanban/cp_1469_qd_ttg_220814.pdf', 'pdf', 2, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_vanban_cat`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_vanban_cat` (
  `catid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `catname` varchar(255) NOT NULL,
  `catalias` varchar(100) NOT NULL,
  PRIMARY KEY (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `kcxd_vi_vanban_cat`
--

INSERT INTO `kcxd_vi_vanban_cat` (`catid`, `catname`, `catalias`) VALUES
(2, 'Lâm 1', 'lam-1'),
(3, 'Lâm 2', 'lam-2');

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_vanban_field`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_vanban_field` (
  `fieldid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `fieldname` varchar(255) NOT NULL,
  `fieldalias` varchar(100) NOT NULL,
  PRIMARY KEY (`fieldid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `kcxd_vi_vanban_field`
--

INSERT INTO `kcxd_vi_vanban_field` (`fieldid`, `fieldname`, `fieldalias`) VALUES
(2, 'Xi măng', 'xi-mang'),
(3, 'Gạch', 'gach');

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_vanban_organ`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_vanban_organ` (
  `organid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `organname` varchar(255) NOT NULL,
  `organalias` varchar(100) NOT NULL,
  PRIMARY KEY (`organid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `kcxd_vi_vanban_organ`
--

INSERT INTO `kcxd_vi_vanban_organ` (`organid`, `organname`, `organalias`) VALUES
(1, 'Nam Định', 'nam-dinh'),
(2, 'Hà Nội', 'ha-noi'),
(3, 'TP. Hồ Chí Minh', 'tp-ho-chi-minh');

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_vanban_room`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_vanban_room` (
  `roomid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `roomname` varchar(255) NOT NULL,
  `roomalias` varchar(100) NOT NULL,
  PRIMARY KEY (`roomid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `kcxd_vi_vanban_room`
--

INSERT INTO `kcxd_vi_vanban_room` (`roomid`, `roomname`, `roomalias`) VALUES
(2, 'Thi 1', 'thi-1'),
(3, 'Thi 2', 'thi-2');

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_video_clip_clip`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_video_clip_clip` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `tid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `hometext` mediumtext NOT NULL,
  `bodytext` mediumtext NOT NULL,
  `keywords` mediumtext NOT NULL,
  `img` varchar(255) NOT NULL,
  `internalpath` varchar(255) NOT NULL,
  `externalpath` mediumtext NOT NULL,
  `groups_view` varchar(255) NOT NULL,
  `comm` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `addtime` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`),
  KEY `tid` (`tid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `kcxd_vi_video_clip_clip`
--

INSERT INTO `kcxd_vi_video_clip_clip` (`id`, `tid`, `title`, `alias`, `hometext`, `bodytext`, `keywords`, `img`, `internalpath`, `externalpath`, `groups_view`, `comm`, `status`, `addtime`) VALUES
(1, 1, 'Bê tông cốt thép ứng lực trước', 'be-tong-cot-thep-ung-luc-truoc', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaaaaaa', '', '', 'uploads/video-clip/images/sua-loi-text.jpg', '', 'https://www.youtube.com/watch?v=-3ATv5Ms4SE', '6', 1, 1, 1421942974),
(2, 2, 'Bê tông cốt thép ứng lực trước tran van thi', 'be-tong-cot-thep-ung-luc-truoc-tran-van-thi', 'xxxxxxxxxx xxxxxxxxxxxxxxxx', '', '', 'uploads/video-clip/images/happy-new-year-abba.jpg', '', 'https://www.youtube.com/watch?v=5BMrtuEOdCA', '6', 1, 1, 1421943215),
(3, 1, 'Bánh Cupcake Giáng Sinh', 'banh-cupcake-giang-sinh', 'fgggggggggggggg', '', '', 'uploads/video-clip/images/happy-new-year-abba.jpg', '', 'https://www.youtube.com/watch?v=6Ep2fbfqfNw', '6', 1, 1, 1421943263),
(4, 1, 'Bê tông cốt thép ứng lực trước tran van thi', 'be-tong-cot-thep-ung-luc-truoc-tran-van-thi-1', 'kkkkkkkkkkkkkkkkkkkkkkk', '', '', 'uploads/video-clip/images/body-top.jpg', '', 'https://www.youtube.com/watch?v=6Ep2fbfqfNw', '6', 1, 1, 1421947704),
(5, 2, 'Bằng cam kết mang lại giá trị cao nhất cho khách hàng với hỗ trợ', 'bang-cam-ket-mang-lai-gia-tri-cao-nhat-cho-khach-hang-voi-ho-tro', 'kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk', '<p>kkkkjjjjjjjjjjjjjjjjj</p>', '', 'uploads/video-clip/images/sapo_new.jpg', '', 'https://www.youtube.com/watch?v=5BMrtuEOdCA', '6', 1, 1, 1421947800);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_video_clip_comm`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_video_clip_comm` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `cid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `content` mediumtext NOT NULL,
  `posttime` int(11) unsigned NOT NULL DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(15) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `broken` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ischecked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `posttime` (`userid`,`posttime`),
  KEY `cid` (`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_video_clip_hit`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_video_clip_hit` (
  `cid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `view` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `likehit` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `unlikehit` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `comment` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `broken` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cid`),
  KEY `view` (`view`),
  KEY `likehit` (`likehit`),
  KEY `unlikehit` (`unlikehit`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kcxd_vi_video_clip_hit`
--

INSERT INTO `kcxd_vi_video_clip_hit` (`cid`, `view`, `likehit`, `unlikehit`, `comment`, `broken`) VALUES
(1, 1, 0, 0, 0, 0),
(2, 0, 0, 0, 0, 0),
(3, 2, 0, 1, 0, 0),
(4, 2, 0, 0, 0, 0),
(5, 1, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_video_clip_topic`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_video_clip_topic` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` mediumint(8) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `weight` smallint(4) unsigned NOT NULL DEFAULT '0',
  `img` varchar(255) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `keywords` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `kcxd_vi_video_clip_topic`
--

INSERT INTO `kcxd_vi_video_clip_topic` (`id`, `parentid`, `title`, `alias`, `description`, `weight`, `img`, `status`, `keywords`) VALUES
(1, 0, 'Tran Thi', 'tran-thi', '', 1, '', 1, ''),
(2, 0, 'Tran Thi 2', 'tran-thi-2', '', 2, '', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_voting`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_voting` (
  `vid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(255) NOT NULL,
  `link` varchar(255) DEFAULT '',
  `acceptcm` int(2) NOT NULL DEFAULT '1',
  `admin_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `groups_view` varchar(255) DEFAULT '',
  `publ_time` int(11) unsigned NOT NULL DEFAULT '0',
  `exp_time` int(11) unsigned NOT NULL DEFAULT '0',
  `act` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`vid`),
  UNIQUE KEY `question` (`question`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `kcxd_vi_voting`
--

INSERT INTO `kcxd_vi_voting` (`vid`, `question`, `link`, `acceptcm`, `admin_id`, `groups_view`, `publ_time`, `exp_time`, `act`) VALUES
(2, 'Bạn biết gì về NukeViet 3?', '', 1, 1, '6', 1275318563, 0, 1),
(3, 'Bạn quan tâm gì nhất ở mã nguồn mở?', '', 1, 1, '6', 1275318563, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `kcxd_vi_voting_rows`
--

CREATE TABLE IF NOT EXISTS `kcxd_vi_voting_rows` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `vid` smallint(5) unsigned NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) DEFAULT '',
  `hitstotal` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vid` (`vid`,`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `kcxd_vi_voting_rows`
--

INSERT INTO `kcxd_vi_voting_rows` (`id`, `vid`, `title`, `url`, `hitstotal`) VALUES
(5, 2, 'Một bộ sourcecode cho web hoàn toàn mới.', '', 0),
(6, 2, 'Mã nguồn mở, sử dụng miễn phí.', '', 0),
(7, 2, 'Sử dụng xHTML, CSS và hỗ trợ Ajax', '', 0),
(8, 2, 'Tất cả các ý kiến trên', '', 0),
(9, 3, 'Liên tục được cải tiến, sửa đổi bởi cả thế giới.', '', 0),
(10, 3, 'Được sử dụng miễn phí không mất tiền.', '', 0),
(11, 3, 'Được tự do khám phá, sửa đổi theo ý thích.', '', 0),
(12, 3, 'Phù hợp để học tập, nghiên cứu vì được tự do sửa đổi theo ý thích.', '', 0),
(13, 3, 'Tất cả các ý kiến trên', '', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
