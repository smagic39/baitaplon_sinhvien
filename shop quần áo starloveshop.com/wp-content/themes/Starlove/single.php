<?php get_header();?>

	<?php 
		
		if(have_posts()) : while(have_posts()) : the_post();
	?>	
	<?php
          //setPostViews(get_the_ID());
	?>
	<section class="title-breadcrumbs">
		<div class="inside clear">							
		<h1 class="title"><?php the_title();?> </h1>
		<div class="post-info">
			<div class="author_mt hp_meta">Thời gian: <?php the_time('l, jS F, Y') ?> </div>		
		</div>
			<div class="breadcrumbs">
				<div class="breadcrumbs">
				<?php if(function_exists('bcn_display'))
				{
					bcn_display();
				}?>
			</div>
			</div>
			<div id="sb-search" class="sb-search search-icon">
				<form action="/" method="get" id="searchform">
					<input class="sb-search-input" placeholder="Enter Keyword and hit enter..." type="text" value="" id="s" name="s">
					<input class="sb-search-submit" type="submit" value="">
					<span class="sb-icon-search"></span>
				</form>
			</div>
		</div>
	</section>
	<section id="main-content-info">
			<div id="single-product-write-left">
				<?php get_sidebar();?>
			</div>
			<div class="post-content">
				<h2 class="entry-title"><?php the_title();?> </h2>
				<?php the_content();?>
				<?php edit_post_link('Sửa bài viết', '<p>', '</p>'); ?>
				<div class="tags-name"><?php the_tags("<span class='tags-title' style='float: left;'>Từ khóa: </span>","","");?></div>
				<div class="fb-comments" data-href="<?php the_permalink();?>" data-numposts="5" data-colorscheme="light"></div>
				<div class="related-post">
				<h3>Bài viết cũ hơn :</h3>
				<ul>
					<?php
								global $post;
								$tmp_post = $post;
								$GLOBALS[ 'begin_time' ] = date('Y-m-d H:i:s', strtotime($post->post_date));
								$GLOBALS[ 'end_time' ] = date('Y-m-d H:i:s', strtotime($post->post_date.'-1825 days'));

								// Create a new filtering function that will add our where clause to the query
								function filter_where( $where = '' ) {
									// posts in the last 1825 days of current post
									$where .= " AND post_date >= '".$GLOBALS[ 'end_time' ]."' AND post_date < '".$GLOBALS[ 'begin_time' ]."'";
									return $where;
								}

								add_filter( 'posts_where', 'filter_where' );

								$args=array(
									'post__not_in' => array($post->ID),
									'orderby' => 'date',
							'order' => 'DESC',
									'showposts' => 12, // Number of related posts that will be shown.
									'ignore_sticky_posts' => 1
								);
								$my_query = new wp_query($args);
								$i = 1;
								remove_filter( 'posts_where', 'filter_where' );

								if( $my_query->have_posts() ) {
									while ($my_query->have_posts()) {
										$my_query->the_post();
								?>
										<li><?php echo $i;?>. <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
								<?php $i++;
									}
								}

							?>
							<?php $post = $tmp_post; ?>
				 </ul>
				</div>
				
				
				
				<div class="related-post">
				<h3>Bài viết mới hơn :</h3>
				<ul>
					<?php
								global $post;
								$tmp_post = $post;
								$GLOBALS[ 'begin_time' ] = date('Y-m-d H:i:s', strtotime($post->post_date));
								$GLOBALS[ 'end_time' ] = date('Y-m-d H:i:s', strtotime($post->post_date.'-1825 days'));

								// Create a new filtering function that will add our where clause to the query
								function filter_where2( $where = '' ) {
									// posts in the last 1825 days of current post
									$where .= " AND post_date >= '".$GLOBALS[ 'end_time' ]."' AND post_date < '".$GLOBALS[ 'begin_time' ]."'";
									return $where;
								}

								add_filter( 'posts_where', 'filter_where2' );

								$args=array(
									'post__not_in' => array($post->ID),
									'orderby' => 'date',
									'order' => 'ASC',
									'showposts' => 12, // Number of related posts that will be shown.
									'ignore_sticky_posts' => 1
								);
								$my_query = new wp_query($args);
								$i = 1;
								remove_filter( 'posts_where', 'filter_where' );

								if( $my_query->have_posts() ) {
									while ($my_query->have_posts()) {
										$my_query->the_post();
								?>
										<li><?php echo $i;?>. <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
								<?php $i++;
									}
								}
								
							?>
							<?php $post = $tmp_post; ?>
				 </ul>
				</div>
			</div>
			
			
		<?php wp_reset_query();endwhile;endif;?>
		<div id="single-product-write-right">
			<aside class="sidebar-right">
				<div class="widget">
					<h4 class="widget-title">Thời trang nổi bật</h4>
					<div class="widget-content sidebar-column">
						<?php 					
							$args = array(						
								'post_type' => 'product',						
													
								'showposts' => 10
							);					
							$vcl = new WP_Query();					
							$vcl->query($args);					
							while($vcl->have_posts()) : $vcl->the_post();				
						?>
							<div class="sidebar-product"> 
								<a href="<?php the_permalink();?>" title="<?php the_title();?>">
						<img width="100%" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" alt="<?php the_title();?>">
						<div class="spacer">
						  <h3 class="item-title"><?php the_title();?></h3>
						  
						</div>
						
					</a>
					<div class="item-info"> <span class="price"><span class="amount"><?php woocommerce_get_template( 'loop/price.php' ); ?></span><a href="/?add-to-cart=<?php the_ID();?>" rel="nofollow" data-product_id="<?php the_ID();?>" data-product_sku="" class="button add_to_cart_button product_type_simple" style="float: right;margin: 5px 10px 0 0;"><i class="fa fa-shopping-cart"></i> Buy Now</a></span> </div>
							</div>
						<?php 					
						endwhile;					
						wp_reset_postdata();				
						?>
					</div>
				</div>
			</aside>
		</div>
	</section>
	
<?php get_footer();?>