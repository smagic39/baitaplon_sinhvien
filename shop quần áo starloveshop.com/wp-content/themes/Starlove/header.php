<!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7" <?php language_attributes(); ?>>
<![endif]--><!--[if IE 8]><html class="ie ie8" <?php language_attributes(); ?>><![endif]-->
<html <?php language_attributes(); ?>>
<head>	
	<meta charset="<?php bloginfo( 'charset' ); ?>">	
	<meta name="viewport" content="width=device-width">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="p:domain_verify" content="cd1ed57c044a32504ced51491b01ed22"/>
	<meta name="author" content="Nguyễn Thị Thủy">
	<meta name="last-modified" content="2014-09-01">
	<link href="https://plus.google.com/u/0/b/111550713790581615475/" rel="publisher" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="shortcut icon" href="<?php bloginfo('template_directory');?>/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?php bloginfo('template_directory');?>/images/favicon.ico" type="image/x-icon">	
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">	
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('title');?> Feed" href="<?php bloginfo('home');?>/feed/">
	<link rel="stylesheet" id="metrolics_style-css" href="<?php bloginfo('template_directory');?>/style.css" type="text/css" media="all">
	<link rel="stylesheet" id="metrolics_font-awesome-css" href="<?php bloginfo('template_directory');?>/css/font-awesome.min.css" type="text/css" media="all">
	<link rel="stylesheet" id="metrolics_superfish_style-css" href="<?php bloginfo('template_directory');?>/css/superfish.css" type="text/css" media="all">
	<link rel="stylesheet" id="metrolics_quote_style-css" href="<?php bloginfo('template_directory');?>/css/component.css" type="text/css" media="all">
	<link rel="stylesheet" id="metrolics_responsive-css" href="<?php bloginfo('template_directory');?>/css/responsive.css" type="text/css" media="all">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript"></script> 
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/jquery-latest.pack.js"></script>	
	<script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/jcarousellite_1.0.1.js"></script>

	<?php wp_head();?>
</head>

<body>

	<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=654749041215191&version=v2.0";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
	<!--CONTAINER-->
	<div id="container">

		<header>			
			<div class="inside clear">
				<!--SOCIAL-->		
				<div class="social clear">
					<ul>
						<li><a href="#" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a></li>	
						<li><a href="#" target="_blank" class="gplus"><i class="fa fa-google-plus"></i></a></li>	
						<li><a href="#" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a></li>	
						<li><a href="#" target="_blank" class="dribbble"><i class="fa fa-dribbble"></i></a></li>	
						<li><a href="#" target="_blank" class="pinterest"><i class="fa fa-pinterest"></i></a></li>	
						<li><a href="#" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a></li>	
						<li><a href="#" target="_blank" class="rss"><i class="fa fa-rss"></i></a></li>	
						<li><a href="#" target="_blank" class="youtube"><i class="fa fa-youtube"></i></a></li>	
					</ul>
				</div>					

				<!--LOGO-->		
				<aside class="logo clear">
					<a href="<?php bloginfo('home');?>">
						<img width="320" src="<?php bloginfo('template_directory');?>/images/logo.png" alt="thời trang nữ đẹp giá rẻ 2015" title="thời trang nữ đẹp giá rẻ 2015" />
					</a>
					
				</aside>

				<!--MENU-->
				<nav class="navigation">
				<div id="dropdown" class="theme-menu">
					<ul id="main-menu" class="sf-menu sf-js-enabled">
						<li id="menu-item-135" class="menu-green menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children has-menu">
							<a href="http://starloveshop.com/danh-muc/vay-dam-dep" class="sf-with-ul">
								<span class="menu-title">Váy Đầm Đẹp</span>
								<span class="menu-icon">
									<i class="fa fa-home"></i>
								</span>
								<span class="sf-sub-indicator">Váy Đầm Đẹp</span>
							</a>
							<ul class="sub-menu" style="display: none;">
								<li id="menu-item-134" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://starloveshop.com/danh-muc/vay-dam-cong-so"><span class="menu-title">Váy Đầm Công Sở</span></a></li>
								<li id="menu-item-133" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://starloveshop.com/danh-muc/vay-dam-da-hoi"><span class="menu-title">Váy Đầm Dạ Hội</span></a></li>
								<li id="menu-item-132" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://starloveshop.com/danh-muc/vay-dam-dao-pho"><span class="menu-title">Váy Đầm Dạo Phố</span></a></li>
								<li id="menu-item-132" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://starloveshop.com/danh-muc/vay-dam-du-tiec"><span class="menu-title">Váy Đầm Dự Tiệc</span></a></li>
								<li id="menu-item-132" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://starloveshop.com/danh-muc/Vay-dam-ho-lung"><span class="menu-title">Váy Đầm Hở Lưng</span></a></li>
								<li id="menu-item-132" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://starloveshop.com/danh-muc/vay-dam-hoa"><span class="menu-title">Váy Hoa Đầm Hoa</span></a></li>
								<li id="menu-item-132" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://starloveshop.com/danh-muc/vay-dam-maxi"><span class="menu-title">Váy Đầm Maxi</span></a></li>
								<li id="menu-item-132" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://starloveshop.com/danh-muc/vay-dam-om-body"><span class="menu-title">Váy Đầm Ôm Body</span></a></li>
								<li id="menu-item-132" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://starloveshop.com/danh-muc/vay-ren-dam-ren"><span class="menu-title">Váy Ren Đầm Ren</span></a></li>
								<li id="menu-item-132" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://starloveshop.com/danh-muc/dam-suong"><span class="menu-title">Váy Đầm Suông</span></a></li>
								<li id="menu-item-132" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://starloveshop.com/danh-muc/dam-xoe"><span class="menu-title">Váy Đầm Xòe</span></a></li>
								<li id="menu-item-132" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://starloveshop.com/danh-muc/dam-cup-nguc"><span class="menu-title">Váy Đầm Cúp Ngực</span></a></li>
								
							</ul>
						</li>
						<li id="menu-item-22" class="menu-yellow menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-menu">
							<a href="#" class="sf-with-ul"><span class="menu-title">Chân Váy & Áo Nữ</span>
								<span class="menu-icon">
									<i class="fa fa-folder-open"></i>
								</span>
								<span class="sf-sub-indicator"> </span>
							</a>
							<ul class="sub-menu" style="display: none;">
								<li id="menu-item-17" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://starloveshop.com/danh-muc/chan-vay"><span class="menu-title">Chân Váy Đẹp</span></a></li>
								<li id="menu-item-182" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://starloveshop.com/danh-muc/ao-nu-dep"><span class="menu-title">Áo Nữ Đẹp</span></a></li>
							</ul>
						</li>
						<li id="menu-item-189" class="menu-pink menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-menu"><a href="http://starloveshop.com/danh-muc/phu-kien-thoi-trang" class="sf-with-ul"><span class="menu-title">Phụ Kiện Thời Trang</span><span class="menu-icon"><i class="fa fa-bullhorn"></i></span><span class="sf-sub-indicator"> </span></a>
						</li>
						<li id="menu-item-247" class="menu-blue menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-menu"><a href="http://starloveshop.com/tin-thoi-trang" class="sf-with-ul"><span class="menu-title">Tin Thời Trang</span><span class="menu-icon"><i class="fa fa-laptop"></i></span><span class="sf-sub-indicator"> </span></a>
	
						</li>
						<li id="menu-item-19" class="menu-dblue menu-item menu-item-type-post_type menu-item-object-page">
							<a href="#"><span class="menu-title">Hướng Dẫn Chung</span><span class="menu-icon"><i class="fa fa-envelope"></i></span></a>
						<ul class="sub-menu" style="display: none;">
								<li id="menu-item-134" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://starloveshop.com/tuyen-dung"><span class="menu-title">Tuyển cộng tác viên</span></a></li>
								<li id="menu-item-133" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://starloveshop.com/huong-dan-mua-hang"><span class="menu-title">Hướng dẫn mua hàng</span></a></li>
								<li id="menu-item-132" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://starloveshop.com/huong-dan-doi-tra-hang"><span class="menu-title">Hướng dẫn đổi trả hàng</span></a></li>
							</ul>
						
						</li>
					</ul>

					<!--<select class="select-menu sf-menu">
						<option value="#">Navigate to...</option>
						<option value="<?php bloginfo('home');?>">Home</option>
						
					</select>-->
				</div>				

			</nav>
			</div>
			<nav class="sticky">
				<div id="secondary-menu">
					<?php wp_nav_menu( array( 'theme_location' => 'sub-menu', 'container' => false ) ); ?>
				</div>
				<div id="search-cse">
					<script>
					  (function() {
						var cx = '013903025837811952771:82omgfick0o';
						var gcse = document.createElement('script');
						gcse.type = 'text/javascript';
						gcse.async = true;
						gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
							'//www.google.com/cse/cse.js?cx=' + cx;
						var s = document.getElementsByTagName('script')[0];
						s.parentNode.insertBefore(gcse, s);
					  })();
					</script>
				<gcse:search></gcse:search>
				</div>
				<div id="phone-contact">
					<span class="del">Liên hệ tư vấn miễn phí : </span><ins class="phone-number">0963.029.702</ins><span class="del"> (Ms.Thủy)</span>
				</div>
			</nav>
			<script>
				var sticky = document.querySelector('.sticky');
				var origOffsetY = sticky.offsetTop;

				function onScroll(e) {
				  window.scrollY >= origOffsetY ? sticky.classList.add('fixed') :
												  sticky.classList.remove('fixed');
				}

				document.addEventListener('scroll', onScroll);
			</script>
			
			
		</header>	
