<?php get_header();?>
	<div id="main-slider">
	 <?php echo do_shortcode('[rev_slider revolution]');?>
	</div>
	<div id="main-content">
		<?php get_sidebar();?>
		<div id="main-primary">
			<section class="product-list">
				<aside class="product-list-name">
					<h1 class="category-title"><a href="http://starloveshop.com/"style="color: azure;">Thời Trang Nữ Starlove</a></h1>
				</aside>
				<div class="product-list-content" id="product-list-test">
					<div class="tab-show">
						<div id="xuly-1" class="tab">
							<?php 
								$args = array(
									'post_type' => 'product',
									//'category__in' => 287,
									'showposts' => 6
								);
								$mnct = new WP_Query();
								$mnct->query($args);
								$i = 1;
								while($mnct->have_posts()) : $mnct->the_post();
							?>
						  <div class="product-list-item-test">
							<a href="<?php the_permalink();?>" title="<?php the_title();?>">
								<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" alt="<?php the_title();?>">
								<div class="spacer">
								  <h2 class="item-title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h2>
								  
								</div>
								
							</a>
							<div class="item-info"> 
								<span class="price">
									<span class="amount-price"><?php echo $product->get_price_html();?></span>
									<a href="/?add-to-cart=<?php the_ID();?>" rel="nofollow" data-product_id="<?php the_ID();?>" data-product_sku="" class="button add_to_cart_button product_type_simple" style="float: right;"><i class="fa fa-shopping-cart"></i> Buy Now</a>
								</span> 
							</div>
							<div class="index-attribute">Mã SP: <span class="index-attr"><?php echo $product->get_sku();?></span></div>
							<?php 
								$availability = $product->get_availability();
								 
								if ($availability['availability']) :
									echo apply_filters( 'woocommerce_stock_html', '<div class="attribute-stock"><span class="index-attr' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</span></div>', $availability['availability'] );
								endif;
							
							?>
						  </div>
						  <?php 
								endwhile;
								wp_reset_postdata();
						  ?>
						</div>
						<div id="xuly-2" class="tab">
							<?php 
								$args = array(
									'post_type' => 'product',
									//'category__in' => 287,
									'offset' => 7,
									'showposts' => 6
								);
								$mnct = new WP_Query();
								$mnct->query($args);
								$i = 1;
								while($mnct->have_posts()) : $mnct->the_post();
							?>
						  <div class="product-list-item-test">
							<a href="<?php the_permalink();?>" title="<?php the_title();?>">
								<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" alt="<?php the_title();?>">
								<div class="spacer">
								  <h2 class="item-title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h2>
								  
								</div>
								
							</a>
							<div class="item-info"> 
								<span class="price">
									<span class="amount-price"><?php echo $product->get_price_html();?></span>
									<a href="/?add-to-cart=<?php the_ID();?>" rel="nofollow" data-product_id="<?php the_ID();?>" data-product_sku="" class="button add_to_cart_button product_type_simple" style="float: right;"><i class="fa fa-shopping-cart"></i> Buy Now</a>
								</span> 
							</div>
							<div class="index-attribute">Mã SP: <span class="index-attr"><?php echo $product->get_sku();?></span></div>
						  </div>
						  <?php 
								endwhile;
								wp_reset_postdata();
						  ?>
						</div>
						<div id="xuly-3" class="tab">
							<?php 
								$args = array(
									'post_type' => 'product',
									//'category__in' => 287,
									'offset' => 13,
									'showposts' => 6
								);
								$mnct = new WP_Query();
								$mnct->query($args);
								$i = 1;
								while($mnct->have_posts()) : $mnct->the_post();
							?>
						  <div class="product-list-item-test">
							<a href="<?php the_permalink();?>" title="<?php the_title();?>">
								<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" alt="<?php the_title();?>">
								<div class="spacer">
								  <h2 class="item-title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h2>
								  
								</div>
								
							</a>
							<div class="item-info"> 
								<span class="price">
									<span class="amount-price"><?php echo $product->get_price_html();?></span>
									<a href="/?add-to-cart=<?php the_ID();?>" rel="nofollow" data-product_id="<?php the_ID();?>" data-product_sku="" class="button add_to_cart_button product_type_simple" style="float: right;"><i class="fa fa-shopping-cart"></i> Buy Now</a>
								</span> 
							</div>
							<div class="index-attribute">Mã SP: <span class="index-attr"><?php echo $product->get_sku();?></span></div>
						  </div>
						  <?php 
								endwhile;
								wp_reset_postdata();
						  ?>
						</div>
						<div id="xuly-4" class="tab">
							<?php 
								$args = array(
									'post_type' => 'product',
									//'category__in' => 287,
									'offset' => 19,
									'showposts' => 6
								);
								$mnct = new WP_Query();
								$mnct->query($args);
								$i = 1;
								while($mnct->have_posts()) : $mnct->the_post();
							?>
						  <div class="product-list-item-test">
							<a href="<?php the_permalink();?>" title="<?php the_title();?>">
								<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" alt="<?php the_title();?>">
								<div class="spacer">
								  <h2 class="item-title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h2>
								  
								</div>
								
							</a>
							<div class="item-info"> 
								<span class="price">
									<span class="amount-price"><?php echo $product->get_price_html();?></span>
									<a href="/?add-to-cart=<?php the_ID();?>" rel="nofollow" data-product_id="<?php the_ID();?>" data-product_sku="" class="button add_to_cart_button product_type_simple" style="float: right;"><i class="fa fa-shopping-cart"></i> Buy Now</a>
								</span> 
							</div>
							<div class="index-attribute">Mã SP: <span class="index-attr"><?php echo $product->get_sku();?></span></div>
							<?php 
								$availability = $product->get_availability();
								 
								if ($availability['availability']) :
									echo apply_filters( 'woocommerce_stock_html', '<div class="attribute-stock"><span class="index-attr' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</span></div>', $availability['availability'] );
								endif;
							
							?>
						  </div>
						  <?php 
								endwhile;
								wp_reset_postdata();
						  ?>
						</div>
						<div id="xuly-5" class="tab">
							<?php 
								$args = array(
									'post_type' => 'product',
									//'category__in' => 287,
									'offset' => 25,
									'showposts' => 6
								);
								$mnct = new WP_Query();
								$mnct->query($args);
								$i = 1;
								while($mnct->have_posts()) : $mnct->the_post();
							?>
						  <div class="product-list-item-test">
							<a href="<?php the_permalink();?>" title="<?php the_title();?>">
								<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" alt="<?php the_title();?>">
								<div class="spacer">
								  <h2 class="item-title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h2>
								  
								</div>
								
							</a>
							<div class="item-info"> 
								<span class="price">
									<span class="amount-price"><?php echo $product->get_price_html();?></span>
									<a href="/?add-to-cart=<?php the_ID();?>" rel="nofollow" data-product_id="<?php the_ID();?>" data-product_sku="" class="button add_to_cart_button product_type_simple" style="float: right;"><i class="fa fa-shopping-cart"></i> Buy Now</a>
								</span> 
							</div>
							<div class="index-attribute">Mã SP: <span class="index-attr"><?php echo $product->get_sku();?></span></div>
							<?php 
								$availability = $product->get_availability();
								 
								if ($availability['availability']) :
									echo apply_filters( 'woocommerce_stock_html', '<div class="attribute-stock"><span class="index-attr' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</span></div>', $availability['availability'] );
								endif;
							
							?>
						  </div>
						  <?php 
								endwhile;
								wp_reset_postdata();
						  ?>
						</div>
						<div id="xuly-6" class="tab">
							<?php 
								$args = array(
									'post_type' => 'product',
									//'category__in' => 287,
									'offset' => 31,
									'showposts' => 6
								);
								$mnct = new WP_Query();
								$mnct->query($args);
								$i = 1;
								while($mnct->have_posts()) : $mnct->the_post();
							?>
						  <div class="product-list-item-test">
							<a href="<?php the_permalink();?>" title="<?php the_title();?>">
								<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" alt="<?php the_title();?>">
								<div class="spacer">
								  <h2 class="item-title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h2>
								  
								</div>
								
							</a>
							<div class="item-info"> 
								<span class="price">
									<span class="amount-price"><?php echo $product->get_price_html();?></span>
									<a href="/?add-to-cart=<?php the_ID();?>" rel="nofollow" data-product_id="<?php the_ID();?>" data-product_sku="" class="button add_to_cart_button product_type_simple" style="float: right;"><i class="fa fa-shopping-cart"></i> Buy Now</a>
								</span> 
							</div>
							<div class="index-attribute">Mã SP: <span class="index-attr"><?php echo $product->get_sku();?></span></div>
							<?php 
								$availability = $product->get_availability();
								 
								if ($availability['availability']) :
									echo apply_filters( 'woocommerce_stock_html', '<div class="attribute-stock"><span class="index-attr' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</span></div>', $availability['availability'] );
								endif;
							
							?>
						  </div>
						  <?php 
								endwhile;
								wp_reset_postdata();
						  ?>
						</div>
						<div id="xuly-7" class="tab">
							<?php 
								$args = array(
									'post_type' => 'product',
									//'category__in' => 287,
									'offset' => 37,
									'showposts' => 6
								);
								$mnct = new WP_Query();
								$mnct->query($args);
								$i = 1;
								while($mnct->have_posts()) : $mnct->the_post();
							?>
						  <div class="product-list-item-test">
							<a href="<?php the_permalink();?>" title="<?php the_title();?>">
								<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" alt="<?php the_title();?>">
								<div class="spacer">
								  <h2 class="item-title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h2>
								  
								</div>
								
							</a>
							<div class="item-info"> 
								<span class="price">
									<span class="amount-price"><?php echo $product->get_price_html();?></span>
									<a href="/?add-to-cart=<?php the_ID();?>" rel="nofollow" data-product_id="<?php the_ID();?>" data-product_sku="" class="button add_to_cart_button product_type_simple" style="float: right;"><i class="fa fa-shopping-cart"></i> Buy Now</a>
								</span> 
							</div>
							<div class="index-attribute">Mã SP: <span class="index-attr"><?php echo $product->get_sku();?></span></div>
							<?php 
								$availability = $product->get_availability();
								 
								if ($availability['availability']) :
									echo apply_filters( 'woocommerce_stock_html', '<div class="attribute-stock"><span class="index-attr' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</span></div>', $availability['availability'] );
								endif;
							
							?>
						  </div>
						  <?php 
								endwhile;
								wp_reset_postdata();
						  ?>
						</div>
						<div id="xuly-8" class="tab">
							<?php 
								$args = array(
									'post_type' => 'product',
									//'category__in' => 287,
									'offset' => 43,
									'showposts' => 6
								);
								$mnct = new WP_Query();
								$mnct->query($args);
								$i = 1;
								while($mnct->have_posts()) : $mnct->the_post();
							?>
						  <div class="product-list-item-test">
							<a href="<?php the_permalink();?>" title="<?php the_title();?>">
								<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" alt="<?php the_title();?>">
								<div class="spacer">
								  <h2 class="item-title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h2>
								  
								</div>
								
							</a>
							<div class="item-info"> 
								<span class="price">
									<span class="amount-price"><?php echo $product->get_price_html();?></span>
									<a href="/?add-to-cart=<?php the_ID();?>" rel="nofollow" data-product_id="<?php the_ID();?>" data-product_sku="" class="button add_to_cart_button product_type_simple" style="float: right;"><i class="fa fa-shopping-cart"></i> Buy Now</a>
								</span> 
							</div>
							<div class="index-attribute">Mã SP: <span class="index-attr"><?php echo $product->get_sku();?></span></div>
							<?php 
								$availability = $product->get_availability();
								 
								if ($availability['availability']) :
									echo apply_filters( 'woocommerce_stock_html', '<div class="attribute-stock"><span class="index-attr' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</span></div>', $availability['availability'] );
								endif;
							
							?>
						  </div>
						  <?php 
								endwhile;
								wp_reset_postdata();
						  ?>
						</div>
						<div id="xuly-9" class="tab">
							<?php 
								$args = array(
									'post_type' => 'product',
									//'category__in' => 287,
									'offset' => 49,
									'showposts' => 6
								);
								$mnct = new WP_Query();
								$mnct->query($args);
								$i = 1;
								while($mnct->have_posts()) : $mnct->the_post();
							?>
						  <div class="product-list-item-test">
							<a href="<?php the_permalink();?>" title="<?php the_title();?>">
								<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" alt="<?php the_title();?>">
								<div class="spacer">
								  <h2 class="item-title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h2>
								  
								</div>
								
							</a>
							<div class="item-info"> 
								<span class="price">
									<span class="amount-price"><?php echo $product->get_price_html();?></span>
									<a href="/?add-to-cart=<?php the_ID();?>" rel="nofollow" data-product_id="<?php the_ID();?>" data-product_sku="" class="button add_to_cart_button product_type_simple" style="float: right;"><i class="fa fa-shopping-cart"></i> Buy Now</a>
								</span> 
							</div>
							<div class="index-attribute">Mã SP: <span class="index-attr"><?php echo $product->get_sku();?></span></div>
							<?php 
								$availability = $product->get_availability();
								 
								if ($availability['availability']) :
									echo apply_filters( 'woocommerce_stock_html', '<div class="attribute-stock"><span class="index-attr' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</span></div>', $availability['availability'] );
								endif;
							
							?>
						  </div>
						  <?php 
								endwhile;
								wp_reset_postdata();
						  ?>
						</div>
						<div id="xuly-10" class="tab">
							<?php 
								$args = array(
									'post_type' => 'product',
									//'category__in' => 287,
									'offset' => 55,
									'showposts' => 6
								);
								$mnct = new WP_Query();
								$mnct->query($args);
								$i = 1;
								while($mnct->have_posts()) : $mnct->the_post();
							?>
						  <div class="product-list-item-test">
							<a href="<?php the_permalink();?>" title="<?php the_title();?>">
								<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" alt="<?php the_title();?>">
								<div class="spacer">
								  <h2 class="item-title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h2>
								  
								</div>
								
							</a>
							<div class="item-info"> 
								<span class="price">
									<span class="amount-price"><?php echo $product->get_price_html();?></span>
									<a href="/?add-to-cart=<?php the_ID();?>" rel="nofollow" data-product_id="<?php the_ID();?>" data-product_sku="" class="button add_to_cart_button product_type_simple" style="float: right;"><i class="fa fa-shopping-cart"></i> Buy Now</a>
								</span> 
							</div>
							<div class="index-attribute">Mã SP: <span class="index-attr"><?php echo $product->get_sku();?></span></div>
							<?php 
								$availability = $product->get_availability();
								 
								if ($availability['availability']) :
									echo apply_filters( 'woocommerce_stock_html', '<div class="attribute-stock"><span class="index-attr' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</span></div>', $availability['availability'] );
								endif;
							
							?>
						  </div>
						  <?php 
								endwhile;
								wp_reset_postdata();
						  ?>
						</div>
						<div id="xuly-11" class="tab">
							<?php 
								$args = array(
									'post_type' => 'product',
									//'category__in' => 287,
									'offset' => 61,
									'showposts' => 6
								);
								$mnct = new WP_Query();
								$mnct->query($args);
								$i = 1;
								while($mnct->have_posts()) : $mnct->the_post();
							?>
						  <div class="product-list-item-test">
							<a href="<?php the_permalink();?>" title="<?php the_title();?>">
								<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" alt="<?php the_title();?>">
								<div class="spacer">
								  <h2 class="item-title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h2>
								  
								</div>
								
							</a>
							<div class="item-info"> 
								<span class="price">
									<span class="amount-price"><?php echo $product->get_price_html();?></span>
									<a href="/?add-to-cart=<?php the_ID();?>" rel="nofollow" data-product_id="<?php the_ID();?>" data-product_sku="" class="button add_to_cart_button product_type_simple" style="float: right;"><i class="fa fa-shopping-cart"></i> Buy Now</a>
								</span> 
							</div>
							<div class="index-attribute">Mã SP: <span class="index-attr"><?php echo $product->get_sku();?></span></div>
							<?php 
								$availability = $product->get_availability();
								 
								if ($availability['availability']) :
									echo apply_filters( 'woocommerce_stock_html', '<div class="attribute-stock"><span class="index-attr' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</span></div>', $availability['availability'] );
								endif;
							
							?>
						  </div>
						  <?php 
								endwhile;
								wp_reset_postdata();
						  ?>
						</div>
						<div id="xuly-12" class="tab">
							<?php 
								$args = array(
									'post_type' => 'product',
									//'category__in' => 287,
									'offset' => 67,
									'showposts' => 6
								);
								$mnct = new WP_Query();
								$mnct->query($args);
								$i = 1;
								while($mnct->have_posts()) : $mnct->the_post();
							?>
						  <div class="product-list-item-test">
							<a href="<?php the_permalink();?>" title="<?php the_title();?>">
								<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" alt="<?php the_title();?>">
								<div class="spacer">
								  <h2 class="item-title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h2>
								  
								</div>
								
							</a>
							<div class="item-info"> 
								<span class="price">
									<span class="amount-price"><?php echo $product->get_price_html();?></span>
									<a href="/?add-to-cart=<?php the_ID();?>" rel="nofollow" data-product_id="<?php the_ID();?>" data-product_sku="" class="button add_to_cart_button product_type_simple" style="float: right;"><i class="fa fa-shopping-cart"></i> Buy Now</a>
								</span> 
							</div>
							<div class="index-attribute">Mã SP: <span class="index-attr"><?php echo $product->get_sku();?></span></div>
							<?php 
								$availability = $product->get_availability();
								 
								if ($availability['availability']) :
									echo apply_filters( 'woocommerce_stock_html', '<div class="attribute-stock"><span class="index-attr' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</span></div>', $availability['availability'] );
								endif;
							
							?>
						  </div>
						  <?php 
								endwhile;
								wp_reset_postdata();
						  ?>
						</div>
						<div id="xuly-13" class="tab">
							<?php 
								$args = array(
									'post_type' => 'product',
									//'category__in' => 287,
									'offset' => 73,
									'showposts' => 6
								);
								$mnct = new WP_Query();
								$mnct->query($args);
								$i = 1;
								while($mnct->have_posts()) : $mnct->the_post();
							?>
						  <div class="product-list-item-test">
							<a href="<?php the_permalink();?>" title="<?php the_title();?>">
								<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" alt="<?php the_title();?>">
								<div class="spacer">
								  <h2 class="item-title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h2>
								  
								</div>
								
							</a>
							<div class="item-info"> 
								<span class="price">
									<span class="amount-price"><?php echo $product->get_price_html();?></span>
									<a href="/?add-to-cart=<?php the_ID();?>" rel="nofollow" data-product_id="<?php the_ID();?>" data-product_sku="" class="button add_to_cart_button product_type_simple" style="float: right;"><i class="fa fa-shopping-cart"></i> Buy Now</a>
								</span> 
							</div>
							<div class="index-attribute">Mã SP: <span class="index-attr"><?php echo $product->get_sku();?></span></div>
							<?php 
								$availability = $product->get_availability();
								 
								if ($availability['availability']) :
									echo apply_filters( 'woocommerce_stock_html', '<div class="attribute-stock"><span class="index-attr' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</span></div>', $availability['availability'] );
								endif;
							
							?>
						  </div>
						  <?php 
								endwhile;
								wp_reset_postdata();
						  ?>
						</div>
						
					</div>
					<ul class="tab_click">
						<li class="tab"><a rel="xuly-1">Trang 1</a></li>
						<li class="tab"><a rel="xuly-2">Trang 2</a></li>
						<li class="tab"><a rel="xuly-3">Trang 3</a></li>
						<li class="tab"><a rel="xuly-4">Trang 4</a></li>
						<li class="tab"><a rel="xuly-5">Trang 5</a></li>
						<li class="tab"><a rel="xuly-6">Trang 6</a></li>
						<li class="tab"><a rel="xuly-7">Trang 7</a></li>
						<li class="tab"><a rel="xuly-4">Trang 8</a></li>
						<li class="tab"><a rel="xuly-5">Trang 9</a></li>
						<li class="tab"><a rel="xuly-6">Trang 10</a></li>
						<li class="tab"><a rel="xuly-7">Trang 11</a></li>
						<li class="tab"><a rel="xuly-6">Trang 12</a></li>
						<li class="tab"><a rel="xuly-7">Trang 13</a></li>
						   
					</ul>
					<script type="text/javascript">
					$('#product-list-test > ul > li > a').click(function(){ 
						
						var target = $(this).attr('rel');
						
						$('div.tab').hide();
						
						$("div#"+target).show();
					 })

				</script>
				</div>
			</section>
						<section class="product-list">
				<aside class="product-list-name">
					<h2 class="category-title"><a href="http://starloveshop.com/danh-muc/vay-dam-cong-so"style="color: azure;">Đầm công sở đẹp</a></h2>
				</aside>
				<div class="columns">
					<?php 
						$args = array(
							'post_type' => 'product',
							'product_cat' => 'dam-xoe',
							'showposts' => 9
							
						);
						$vcl = new WP_Query();
						$vcl->query($args);
						while($vcl->have_posts()) : $vcl->the_post();
					?> 
				  <div class="product-list-item">
					<a href="<?php the_permalink();?>" title="<?php the_title();?>">
						<img width="100%" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" alt="<?php the_title();?>">
						<div class="spacer">
						  <h3 class="item-title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h3>
						  
						</div>
						
					</a>
					<div class="item-info"> 
						<span class="price">
							<span class="amount-price"><?php echo $product->get_price_html();?></span>
							<a href="/?add-to-cart=<?php the_ID();?>" rel="nofollow" data-product_id="<?php the_ID();?>" data-product_sku="" class="button add_to_cart_button product_type_simple" style="float: right;"><i class="fa fa-shopping-cart"></i> Buy Now</a>
						</span> 
					</div>
					<div class="index-attribute">Mã SP: <span class="index-attr"><?php echo $product->get_sku();?></span></div>
					<?php 
								$availability = $product->get_availability();
								 
								if ($availability['availability']) :
									echo apply_filters( 'woocommerce_stock_html', '<div class="attribute-stock"><span class="index-attr' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</span></div>', $availability['availability'] );
								endif;
							
							?>
				  </div>
				  <?php 
						endwhile;
						wp_reset_postdata();
				  ?>
				  
				</div>
					
			</section>
			<section class="product-list">
				<aside class="product-list-name">
					<h2 class="category-title"><a href="http://starloveshop.com/danh-muc/vay-dam-om-body"style="color: azure;">Mẫu Váy Ôm Body Đẹp</a></h2>
				</aside>
				<div class="columns">
					<?php 
						$args = array(
							'post_type' => 'product',
							'product_cat' => 'vay-dam-om-body',
							'showposts' => 9
							
						);
						$vcl = new WP_Query();
						$vcl->query($args);
						while($vcl->have_posts()) : $vcl->the_post();
					?> 
				  <div class="product-list-item">
					<a href="<?php the_permalink();?>" title="<?php the_title();?>">
						<img width="100%" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" alt="<?php the_title();?>">
						<div class="spacer">
						  <h3 class="item-title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h3>
						  
						</div>
						
					</a>
					<div class="item-info"> 
						<span class="price">
							<span class="amount-price"><?php echo $product->get_price_html();?></span>
							<a href="/?add-to-cart=<?php the_ID();?>" rel="nofollow" data-product_id="<?php the_ID();?>" data-product_sku="" class="button add_to_cart_button product_type_simple" style="float: right;"><i class="fa fa-shopping-cart"></i> Buy Now</a>
						</span> 
					</div>
					<div class="index-attribute">Mã SP: <span class="index-attr"><?php echo $product->get_sku();?></span></div>
					<?php 
								$availability = $product->get_availability();
								 
								if ($availability['availability']) :
									echo apply_filters( 'woocommerce_stock_html', '<div class="attribute-stock"><span class="index-attr' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</span></div>', $availability['availability'] );
								endif;
							
							?>
				  </div>
				  <?php 
						endwhile;
						wp_reset_postdata();
				  ?>
				  
				</div>
					
			</section>
			<section class="product-list">
				<aside class="product-list-name">
					<h2 class="category-title"><a href="http://starloveshop.com/danh-muc/ao-nu-dep"style="color: azure;">Mẫu áo nữ đẹp 2015</a></h2>
				</aside>
				<div class="columns">
					<?php 
						$args = array(
							'post_type' => 'product',
							'product_cat' => 'ao-nu-dep',
							'showposts' => 9
							
						);
						$vcl = new WP_Query();
						$vcl->query($args);
						while($vcl->have_posts()) : $vcl->the_post();
					?> 
				  <div class="product-list-item">
					<a href="<?php the_permalink();?>" title="<?php the_title();?>">
						<img width="100%" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" alt="<?php the_title();?>">
						<div class="spacer">
						  <h3 class="item-title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h3>
						  
						</div>
						
					</a>
					<div class="item-info"> 
						<span class="price">
							<span class="amount-price"><?php echo $product->get_price_html();?></span>
							<a href="/?add-to-cart=<?php the_ID();?>" rel="nofollow" data-product_id="<?php the_ID();?>" data-product_sku="" class="button add_to_cart_button product_type_simple" style="float: right;"><i class="fa fa-shopping-cart"></i> Buy Now</a>
						</span> 
					</div>
					<div class="index-attribute">Mã SP: <span class="index-attr"><?php echo $product->get_sku();?></span></div>
					<?php 
								$availability = $product->get_availability();
								 
								if ($availability['availability']) :
									echo apply_filters( 'woocommerce_stock_html', '<div class="attribute-stock"><span class="index-attr' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</span></div>', $availability['availability'] );
								endif;
							
							?>
				  </div>
				  <?php 
						endwhile;
						wp_reset_postdata();
				  ?>
				  
				</div>
				
			</section>	
			<section class="product-list">
				<aside class="product-list-name">
					<h2 class="category-title"><a href="http://starloveshop.com/danh-muc/chan-vay"style="color: azure;">Chân Váy Đẹp 2015</a></h2>
				</aside>
				<div class="columns">
					<?php 
						$args = array(
							'post_type' => 'product',
							'product_cat' => 'chan-vay',
							'showposts' => 9
							
						);
						$vcl = new WP_Query();
						$vcl->query($args);
						while($vcl->have_posts()) : $vcl->the_post();
					?> 
				  <div class="product-list-item">
					<a href="<?php the_permalink();?>" title="<?php the_title();?>">
						<img width="100%" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" alt="<?php the_title();?>">
						<div class="spacer">
						  <h3 class="item-title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h3>
						  
						</div>
						
					</a>
					<div class="item-info"> 
						<span class="price">
							<span class="amount-price"><?php echo $product->get_price_html();?></span>
							<a href="/?add-to-cart=<?php the_ID();?>" rel="nofollow" data-product_id="<?php the_ID();?>" data-product_sku="" class="button add_to_cart_button product_type_simple" style="float: right;"><i class="fa fa-shopping-cart"></i> Buy Now</a>
						</span> 
					</div>
					<div class="index-attribute">Mã SP: <span class="index-attr"><?php echo $product->get_sku();?></span></div>
					<?php 
								$availability = $product->get_availability();
								 
								if ($availability['availability']) :
									echo apply_filters( 'woocommerce_stock_html', '<div class="attribute-stock"><span class="index-attr' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</span></div>', $availability['availability'] );
								endif;
							
							?>
				  </div>
				  <?php 
						endwhile;
						wp_reset_postdata();
				  ?>
				  
				</div>
				
			</section>
		
		</div>
	</div>
<?php get_footer();?>