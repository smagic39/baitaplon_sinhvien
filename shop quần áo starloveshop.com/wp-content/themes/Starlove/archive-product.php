<?php get_header();?>
<div id="main-content">
	<?php get_sidebar();?>
	<div id="main-primary">

		<section class="product-list">
			<?php
				/**
				 * woocommerce_before_shop_loop hook
				 *
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
				do_action( 'woocommerce_before_shop_loop' );
			?>

			<aside class="product-list-name">
				<h1 class="category-title"><?php echo single_cat_title();?></h1>
			</aside>
			<div class="breadcrumbs" style="margin-top: 15px; font-size: 10px;color: #bb0000;">
				<?php 
					if(function_exists('bcn_display'))
					{
						bcn_display();
					}
				?>
			</div>
			<div class="columns" style="margin-top: 50px;">
			
				<?php 
					//query_posts($query_string. "&order=DESC");
					
					//query_posts($query_string. "&orderby=date");
					while(have_posts()) : the_post();
					global $product;
				?>
				<?php //echo $query_string;?>
			  <div class="product-list-item">
				<a href="<?php the_permalink();?>" title="<?php the_title();?>">
					<img width="100%" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" alt="<?php the_title();?>">
					<div class="spacer">
					  <h2 class="item-title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h2>
					  
					</div>
					
				</a>
				<div class="item-info"> 
					<span class="price">
						<span class="amount-price"><?php woocommerce_get_template( 'loop/price.php' );?></span>
						<a href="/?add-to-cart=<?php the_ID();?>" rel="nofollow" data-product_id="<?php the_ID();?>" data-product_sku="" class="button add_to_cart_button product_type_simple" style="float: right;"><i class="fa fa-shopping-cart"></i> Buy Now</a>
					</span> 
				</div>
				<div class="index-attribute">Mã SP: <span class="index-attr"><?php echo $product->get_sku();?></span></div>
			  </div>
			  <?php 
					endwhile;
					wp_reset_postdata();
			  ?>
			  
			</div>
			<div id="nav-below" class="navigation_number">
				<div class="page_navi">
					<?php wp_simple_pagination(); ?>	
				</div>
			</div><!-- #nav-below -->
			</section>
			
			<div id="category-description">
				<?php echo category_description( get_category_by_slug('category-slug')->term_id ); ?>
			</div>
			  
		</section>
	</div>
</div>
<?php get_footer();?>