<footer>	
	<div class="footer-stripes clear">
		<div class="footer-green-stripe">&nbsp;</div>
		<div class="footer-yellow-stripe">&nbsp;</div>
		<div class="footer-pink-stripe">&nbsp;</div>
		<div class="footer-violet-stripe">&nbsp;</div>
		<div class="footer-blue-stripe">&nbsp;</div>
	</div>

				
	<!--FOOTER WIDGETS-->
	<section class="footer-widgets">
		<div class="inside clear">
			<div class="widget-column widget-green">
				<div id="text-2" class="widget widget_text"><h5>Hỗ Trợ Khách Hàng</h5>
					<div class="textwidget">											<div class="textwidget">																<p style="padding: 5px; border: 2px dashed #EFECEE; /* background-color: #1580D2; */">						* HOTLINE : <span style="font-weight: bold;font-size:20px;color: rgb(247, 74, 166);">0963.029.702</span><br>						* HOTLINE : <span style="font-weight: bold;font-size:20px;color:rgb(247, 74, 166);"> 0937.978.474</span><br>						* LÀM VIỆC: <span style="font-weight: bold;font-size:12px;color:rgb(247, 74, 166);">Thứ 2 - CN: 9h00 - 23h00</span><br>						* EMAIL: <span style="font-weight: bold;font-size:12px;color:rgb(247, 74, 166);">philamhuyen@gmail.com</span><br>						=&gt; <a href="http://starloveshop.com/huong-dan-mua-hang" rel="dofollow" target="_blank" title="Hướng Dẫn Mua Hàng">Hướng Dẫn Mua Hàng</a><br>						=&gt; <a href="http://starloveshop.com/huong-dan-doi-tra-hang" rel="dofollow" target="_blank" title="Hướng Dẫn Đỗi Trả">Hướng Dẫn Đổi Trả</a>						</p></div>
					</div>
				</div>
				<div id="newslettersignupwidget-2" class="widget nsu_widget">
				<h5>Đăng ký nhận thông báo</h5>
				<div class="nsu-text-before-form" style="font-size: 13px;">Nhận Email thông báo các sản phẩm <a href="http://starloveshop.com/danh-muc/vay-dam-dep" title="vay dam moi, váy đầm mới">váy đầm mới</a> hàng tuần</div>
				<?php echo do_shortcode('[nsu_form]');?>
					
			</div>	
			</div>
			
		
			<div class="widget-column widget-blue">
				<div id="categories-3" class="widget widget_categories">
				<h5>Danh mục thời trang</h5>		
					<?php $args = array(
						'show_option_all'    => '',
						'orderby'            => 'count',
						'order'              => 'DESC',
						'style'              => 'list',
						'show_count'         => 1,
						'hide_empty'         => 1,
						'use_desc_for_title' => 1,
						'child_of'           => 0,
						'feed'               => '',
						'feed_type'          => '',
						'feed_image'         => '',
						'exclude'            => '',
						'exclude_tree'       => '',
						'include'            => '',
						'hierarchical'       => 1,
						'title_li'           => 0,
						'show_option_none'   => __( 'No categories' ),
						'number'             => 14,
						'echo'               => 1,
						'depth'              => 0,
						'current_category'   => 0,
						'pad_counts'         => 0,
						'taxonomy'           => 'product_cat',
						'walker'             => null
					); ?>
					<?php wp_list_categories( $args ); ?> 
				</div>					
			</div>
			<div class="widget-column widget-pink">
				
				<div id="tag_cloud-2" class="widget widget_tag_cloud">
					<h5>Danh mục sản phẩm</h5>		
					<?php $args = array(
						'smallest'                  => 15, 
						'largest'                   => 19,
						'unit'                      => 'px', 
						'number'                    => 14,  
						'format'                    => 'flat',
						'separator'                 => "\n",
						'orderby'                   => 'name', 
						'order'                     => 'DESC',
						'exclude'                   => null, 
						'include'                   => null, 
						'topic_count_text_callback' => default_topic_count_text,
						'link'                      => 'view', 
						'taxonomy'                  => 'product_tag', 
						'echo'                      => true,
						'child_of'                  => null, // see Note!
					); ?>
					<?php wp_tag_cloud( $args ); ?>
				</div>
			</div>
			
			<div class="widget-column widget-yellow">
				<div id="facebook-like-box" class="widget widget_tag_cloud">
					<h5>Theo dõi Starlove Fanpage</h5>
					<div class="fb-like-box" data-href="https://www.facebook.com/thoitrangstarlove" data-width="240" data-colorscheme="dark" data-show-faces="true" data-header="false" data-stream="false" data-show-border="true"></div>
				</div>				<p>Bản Quyền Thuộc starlove :</p>						<script src="//images.dmca.com/Badges/DMCABadgeHelper.min.js"></script><a href="http://www.dmca.com/Protection/Status.aspx?ID=bab65e7f-b7cd-4794-b2a5-7a498bbc6ec6" title="DMCA.com Protection Program" class="dmca-badge"> <img src ="//images.dmca.com/Badges/dmca_protected_12_120.png?ID=bab65e7f-b7cd-4794-b2a5-7a498bbc6ec6"  alt="DMCA.com Protection Status" /></a>
			</div>
		</div>
	</section>
</footer>

	<script type='text/javascript' src='<?php bloginfo('template_directory');?>/js/jquery.superfish.min.js'></script>
	
	<script type='text/javascript' src='<?php bloginfo('template_directory');?>/js/jquery.mobilemenu.js'></script>
	<script type='text/javascript' src='<?php bloginfo('template_directory');?>/js/custom.js'></script>
	
	<?php wp_footer();?>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-54948948-1', 'auto');
	  ga('send', 'pageview');

	</script>
	
	<!--Start of Zopim Live Chat Script-->
	<script type="text/javascript">
	window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
	d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
	_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
	$.src='//v2.zopim.com/?2Qd6SzgmfYVT79YYxapktcpX1R7IARwU';z.t=+new Date;$.
	type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
	</script>
	<!--End of Zopim Live Chat Script-->
	
	
</body>
</html>