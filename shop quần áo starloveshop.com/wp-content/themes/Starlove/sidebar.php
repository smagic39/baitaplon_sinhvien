<aside class="sidebar">
	<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar()) : ?>
	<?php endif;?>
	<div class="widget">
		<h4 class="widget-title">Fashion News</h4>
		<div class="widget-content">
			<?php 					
				$args = array(						
					'post_type' => 'post',						
					'category_name' => 'tin-thoi-trang',						
					'showposts' => 10
				);					
				$vcl = new WP_Query();					
				$vcl->query($args);					
				while($vcl->have_posts()) : $vcl->the_post();				
			?>
				<div class="sidebar-post"> 
					<a href="<?php the_permalink();?>" title="<?php the_title();?>"> 
					<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" title="<?php the_title();?>" alt="<?php the_title();?>"/>
					<h3><span><?php the_title();?></span></h3>
					</a>
				</div>
			<?php 					
			endwhile;					
			wp_reset_postdata();				
			?>
		</div>
	</div>

</aside>