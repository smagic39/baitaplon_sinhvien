<div id="single-product-write-right">
			<aside class="sidebar-right">
				<div class="widget">
					<h4 class="widget-title">Thời trang nổi bật</h4>
					<div class="widget-content sidebar-column">
						<?php 					
							$args = array(						
								'post_type' => 'product',						
								'product_cat' => 'vay-dam-dep',						
								'showposts' => 10
							);					
							$vcl = new WP_Query();					
							$vcl->query($args);					
							while($vcl->have_posts()) : $vcl->the_post();				
						?>
							<div class="sidebar-product" style="position: relative;"> 
								<a href="<?php the_permalink();?>" title="<?php the_title();?>">
									<img width="100%" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" alt="<?php the_title();?>">
									<div class="spacer">
									  <h3 class="item-title"><?php the_title();?></h3>
									  
									</div>
									
								</a>
								<div class="item-info"> <span class="price"><span class="amount"><?php woocommerce_get_template( 'loop/price.php' ); ?></span><a href="/?add-to-cart=<?php the_ID();?>" rel="nofollow" data-product_id="<?php the_ID();?>" data-product_sku="" class="button add_to_cart_button product_type_simple" style="float: right;"><i class="fa fa-shopping-cart"></i> Buy Now</a></span> </div>
							</div>
						<?php 					
						endwhile;					
						wp_reset_postdata();				
						?>
					</div>
				</div>
			</aside>
		</div>