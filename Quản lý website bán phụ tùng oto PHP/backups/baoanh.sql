-- phpMyAdmin SQL Dump
-- version 2.10.2
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Nov 20, 2007 at 06:54 PM
-- Server version: 5.0.45
-- PHP Version: 5.2.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `baoanh`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `a`
-- 

CREATE TABLE `a` (
  `a` varchar(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table `a`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `carphoto`
-- 

CREATE TABLE `carphoto` (
  `id_carphoto` bigint(20) unsigned NOT NULL auto_increment,
  `id_product` bigint(20) NOT NULL default '0',
  `name` varchar(255) NOT NULL default '',
  `small_image` varchar(255) NOT NULL default '',
  `normal_image` varchar(255) NOT NULL default '',
  `image` varchar(255) NOT NULL default '',
  `thu_tu` bigint(20) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id_carphoto`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=39 ;

-- 
-- Dumping data for table `carphoto`
-- 

INSERT INTO `carphoto` VALUES (2, 3, 'a', 'thumb_carphoto_1195114449.jpeg', 'normal_carphoto_1195114449.jpeg', 'carphoto_1195114449.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (3, 3, 'abv', 'thumb_carphoto_1195114460.jpeg', 'normal_carphoto_1195114460.jpeg', 'carphoto_1195114460.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (4, 3, '4', 'thumb_carphoto_1195114641.jpeg', 'normal_carphoto_1195114641.jpeg', 'carphoto_1195114641.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (5, 2, 'd', 'thumb_carphoto_1195114859.jpeg', 'normal_carphoto_1195114859.jpeg', 'carphoto_1195114859.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (6, 1, '34', 'thumb_carphoto_1195114972.jpeg', 'normal_carphoto_1195114972.jpeg', 'carphoto_1195114972.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (7, 1, 'dsf', 'thumb_carphoto_1195114979.jpeg', '', 'carphoto_1195114979.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (8, 1, 'd', 'thumb_carphoto_1195114984.jpeg', 'normal_carphoto_1195114984.jpeg', 'carphoto_1195114984.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (9, 2, 'dfsdf', 'thumb_carphoto_1195118909.jpeg', 'normal_carphoto_1195118909.jpeg', 'carphoto_1195118909.jpg', 2, 1);
INSERT INTO `carphoto` VALUES (10, 2, 'dfsdfsdfsd', 'thumb_carphoto_1195119013.jpeg', 'normal_carphoto_1195119013.jpeg', 'carphoto_1195119013.jpg', 3, 1);
INSERT INTO `carphoto` VALUES (11, 2, 'dfs?f fd d', 'thumb_carphoto_1195119061.jpeg', 'normal_carphoto_1195119061.jpeg', 'carphoto_1195119061.jpg', 76, 1);
INSERT INTO `carphoto` VALUES (18, 6, '2', 'thumb_carphoto_1195120250.jpeg', 'normal_carphoto_1195120250.jpeg', 'carphoto_1195120250.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (19, 6, '4', 'thumb_carphoto_1195120255.jpeg', 'normal_carphoto_1195120255.jpeg', 'carphoto_1195120255.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (20, 8, 'anh 1', 'thumb_carphoto_1195197110.jpg', 'normal_carphoto_1195197110.jpg', 'carphoto_1195197110.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (21, 8, 'anh 2', 'thumb_carphoto_1195197119.jpg', 'normal_carphoto_1195197119.jpg', 'carphoto_1195197119.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (22, 8, 'anh 2', 'thumb_carphoto_1195197129.jpg', 'normal_carphoto_1195197129.jpg', 'carphoto_1195197129.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (23, 8, 'anh 4', 'thumb_carphoto_1195197146.jpg', 'normal_carphoto_1195197146.jpg', 'carphoto_1195197146.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (24, 8, 'anh 5', 'thumb_carphoto_1195197157.jpg', 'normal_carphoto_1195197157.jpg', 'carphoto_1195197157.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (25, 8, 'anh 6', 'thumb_carphoto_1195197168.jpg', 'normal_carphoto_1195197168.jpg', 'carphoto_1195197168.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (26, 9, '1', 'thumb_carphoto_1195198091.jpg', 'normal_carphoto_1195198091.jpg', 'carphoto_1195198091.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (27, 9, '2.', 'thumb_carphoto_1195198097.jpg', 'normal_carphoto_1195198097.jpg', 'carphoto_1195198097.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (28, 9, '3', 'thumb_carphoto_1195198104.jpg', 'normal_carphoto_1195198104.jpg', 'carphoto_1195198104.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (29, 9, '4', 'thumb_carphoto_1195198111.jpg', 'normal_carphoto_1195198111.jpg', 'carphoto_1195198111.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (30, 9, '5', 'thumb_carphoto_1195198117.jpg', 'normal_carphoto_1195198117.jpg', 'carphoto_1195198117.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (31, 9, '6', 'thumb_carphoto_1195198127.jpg', 'normal_carphoto_1195198127.jpg', 'carphoto_1195198127.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (32, 9, '7', 'thumb_carphoto_1195198134.jpg', 'normal_carphoto_1195198134.jpg', 'carphoto_1195198134.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (33, 9, '8', 'thumb_carphoto_1195198142.jpg', 'normal_carphoto_1195198142.jpg', 'carphoto_1195198142.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (34, 9, '9', 'thumb_carphoto_1195198152.jpg', 'normal_carphoto_1195198152.jpg', 'carphoto_1195198152.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (35, 9, '10', 'thumb_carphoto_1195198166..jpg', 'normal_carphoto_1195198166..jpg', 'carphoto_1195198166..jpg', 0, 1);
INSERT INTO `carphoto` VALUES (36, 9, '11', 'thumb_carphoto_1195198176.jpg', 'normal_carphoto_1195198176.jpg', 'carphoto_1195198176.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (37, 9, '12', 'thumb_carphoto_1195198183.jpg', 'normal_carphoto_1195198183.jpg', 'carphoto_1195198183.jpg', 0, 1);
INSERT INTO `carphoto` VALUES (38, 9, '13', 'thumb_carphoto_1195198189.jpg', 'normal_carphoto_1195198189.jpg', 'carphoto_1195198189.jpg', 0, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `cart`
-- 

CREATE TABLE `cart` (
  `id_cart` bigint(20) unsigned NOT NULL auto_increment,
  `session` tinytext NOT NULL,
  `quantity` int(10) unsigned NOT NULL default '0',
  `id_prod` bigint(20) unsigned NOT NULL default '0',
  `prod_cat` varchar(10) NOT NULL default '',
  `time` bigint(20) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id_cart`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

-- 
-- Dumping data for table `cart`
-- 

INSERT INTO `cart` VALUES (1, '9c9801f8ca314a633ed87c2583b4a174', 1, 5, 'product', 1193220120);
INSERT INTO `cart` VALUES (2, '7a450107be6723564b49276d205d643b', 100000, 5, 'product', 1193220518);
INSERT INTO `cart` VALUES (3, '9c9801f8ca314a633ed87c2583b4a174', 2, 6, 'product', 1193221158);
INSERT INTO `cart` VALUES (4, 'ca78c20533147d2c7533723021f99607', 4, 6, 'product', 1193221322);
INSERT INTO `cart` VALUES (5, 'ca78c20533147d2c7533723021f99607', 3, 7, 'product', 1193221541);
INSERT INTO `cart` VALUES (6, 'ca78c20533147d2c7533723021f99607', 1, 3, 'product', 1193221656);
INSERT INTO `cart` VALUES (8, 'ca78c20533147d2c7533723021f99607', 1, 4, 'product', 1193222017);
INSERT INTO `cart` VALUES (9, 'b4fab4ad2d8f4068b815b074cb247d7d', 2, 6, 'product', 1193295151);
INSERT INTO `cart` VALUES (10, 'b4fab4ad2d8f4068b815b074cb247d7d', 1, 5, 'product', 1193298203);
INSERT INTO `cart` VALUES (11, 'b4fab4ad2d8f4068b815b074cb247d7d', 1, 3, 'product', 1193298514);
INSERT INTO `cart` VALUES (12, 'b4fab4ad2d8f4068b815b074cb247d7d', 1, 7, 'product', 1193300701);
INSERT INTO `cart` VALUES (18, '5ecee0d9632c323eaa4bd69c1a9c76f2', 2, 4, 'product', 1193996381);
INSERT INTO `cart` VALUES (17, 'b4bb2bbc95f1920b32248c4db50043d8', 1, 5, 'product', 1193643879);
INSERT INTO `cart` VALUES (19, '666aa922f15ebadf18c91f59cc93fd1f', 1, 6, 'product', 1193998197);
INSERT INTO `cart` VALUES (20, '4b511812c5810b2ec358604a973d0597', 1, 3, 'product', 1194061852);
INSERT INTO `cart` VALUES (21, 'b1f4b9ebae9c0926863ee043388a474d', 4, 1, 'product', 1195121179);
INSERT INTO `cart` VALUES (22, 'fdbdf1a8f6b73318d5f462275987088e', 2, 2, 'product', 1195121235);
INSERT INTO `cart` VALUES (23, 'cdea65f31222f9922ab617f5950c7ed4', 1, 2, 'product', 1195175493);
INSERT INTO `cart` VALUES (24, '176981f6598ef072b54631cae49f1134', 1, 1, 'product', 1195175806);
INSERT INTO `cart` VALUES (30, 'd5395ae76fbaa7caa9a8693c211b25f6', 1, 2, 'product', 1195209228);
INSERT INTO `cart` VALUES (31, '612c646a2aefe2967be9d7af3e9c3edb', 1, 8, 'product', 1195268078);

-- --------------------------------------------------------

-- 
-- Table structure for table `cat`
-- 

CREATE TABLE `cat` (
  `id_cat` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(50) character set latin1 collate latin1_general_ci NOT NULL default '',
  `parentid` bigint(20) unsigned NOT NULL default '0',
  `thu_tu` bigint(20) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '1',
  PRIMARY KEY  (`id_cat`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `cat`
-- 

INSERT INTO `cat` VALUES (1, 'TIN THá»Š TRÆ¯á»œNG OTO', 0, 0, 1);
INSERT INTO `cat` VALUES (3, 'TIN Tá»¨C Sá»° KIá»†N', 0, 1, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `catif`
-- 

CREATE TABLE `catif` (
  `id_catif` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(50) character set latin1 collate latin1_general_ci NOT NULL default '',
  `parentid` bigint(20) unsigned NOT NULL default '0',
  `thu_tu` bigint(20) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '1',
  PRIMARY KEY  (`id_catif`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `catif`
-- 

INSERT INTO `catif` VALUES (1, 'Gi&#7899;i thi&#7879;u', 0, 0, 1);
INSERT INTO `catif` VALUES (3, 'B&#7843;o h&#224;nh', 0, 0, 1);
INSERT INTO `catif` VALUES (4, 'ThÃ´ng tin trÃªn trang liÃªn há»‡', 0, 0, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `catlg`
-- 

CREATE TABLE `catlg` (
  `id_catlg` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(50) character set latin1 collate latin1_general_ci NOT NULL default '',
  `parentid` bigint(20) unsigned NOT NULL default '0',
  `thu_tu` bigint(20) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '1',
  PRIMARY KEY  (`id_catlg`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- 
-- Dumping data for table `catlg`
-- 

INSERT INTO `catlg` VALUES (1, 'Banner trang ch&#7911;', 0, 0, 1);
INSERT INTO `catlg` VALUES (2, 'Li&#234;n k&#7871;t website', 0, 0, 1);
INSERT INTO `catlg` VALUES (4, 'Logo tr&#432;&#7907;t', 0, 0, 1);
INSERT INTO `catlg` VALUES (5, 'Tr&#225;i', 4, 0, 1);
INSERT INTO `catlg` VALUES (6, 'Ph&#7843;i', 4, 0, 1);
INSERT INTO `catlg` VALUES (7, 'quáº£ng cÃ¡o trang chá»§', 0, 0, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `catpd`
-- 

CREATE TABLE `catpd` (
  `id_catpd` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(50) character set latin1 collate latin1_general_ci NOT NULL default '',
  `parentid` bigint(20) unsigned NOT NULL default '0',
  `thu_tu` bigint(20) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '1',
  `content` text character set latin1 collate latin1_general_ci NOT NULL,
  `image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  PRIMARY KEY  (`id_catpd`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

-- 
-- Dumping data for table `catpd`
-- 

INSERT INTO `catpd` VALUES (1, 'Xe h&#417;i trong n&#432;&#7899;c', 0, 0, 1, '', '');
INSERT INTO `catpd` VALUES (2, 'Xe h&#417;i nh&#7853;p kh&#7849;u', 0, 1, 1, '', '');
INSERT INTO `catpd` VALUES (3, '&#272;&#7891; ch&#417;i xe h&#417;i', 0, 2, 1, '', '');
INSERT INTO `catpd` VALUES (4, 'Xe hÆ¡i Ã', 0, 3, 0, '', '');
INSERT INTO `catpd` VALUES (6, 'Cadillac', 1, 0, 1, '', 'product_1194945866.gif');
INSERT INTO `catpd` VALUES (7, 'Chevrolet', 1, 0, 1, '', 'product_1195009728.jpg');
INSERT INTO `catpd` VALUES (24, 'Toyota', 2, 0, 1, '', 'product_1195196111.jpg');
INSERT INTO `catpd` VALUES (27, 'Mazda', 3, 0, 1, '', 'product_1195196993.jpg');
INSERT INTO `catpd` VALUES (25, 'Rolls-royce', 3, 0, 1, '', 'product_1195196345.jpg');
INSERT INTO `catpd` VALUES (26, 'Hon Da', 2, 0, 1, '', 'product_1195196954.jpg');
INSERT INTO `catpd` VALUES (14, 'Xe Italia 1', 4, 0, 1, '', 'product_1195012200.jpg');
INSERT INTO `catpd` VALUES (15, 'Xe Italia 2', 4, 0, 1, '', 'product_1195012206.jpg');
INSERT INTO `catpd` VALUES (16, 'Castagna Aznom', 4, 0, 1, '', 'product_1195196803.jpg');
INSERT INTO `catpd` VALUES (18, 'Xe h&#417;i kh&#225;c', 0, 4, 0, '', '');
INSERT INTO `catpd` VALUES (19, 'Fiat', 18, 0, 1, '', '');
INSERT INTO `catpd` VALUES (20, 'dfsfsdf', 14, 0, 1, '', 'product_1195196503.jpg');
INSERT INTO `catpd` VALUES (28, 'Lexus', 3, 0, 1, '', 'product_1195198030.jpg');
INSERT INTO `catpd` VALUES (29, 'Äá»“ chÆ¡i xe', 0, 5, 1, '', '');
INSERT INTO `catpd` VALUES (30, 'Bá»™ giáº£m Ã¢m', 29, 0, 1, '', 'product_1195527659.gif');
INSERT INTO `catpd` VALUES (31, 'Bá»™ ly há»£p', 29, 0, 1, '', 'product_1195527674.gif');
INSERT INTO `catpd` VALUES (32, 'Lá»c nhiÃªn liá»‡u', 29, 0, 1, '', 'product_1195527704.gif');
INSERT INTO `catpd` VALUES (33, 'Äai Ä‘Ã£n Ä‘á»™ng', 29, 0, 1, '', 'product_1195527715.gif');
INSERT INTO `catpd` VALUES (34, 'Phá»¥ tÃ¹ng thÃ¢n xe', 29, 0, 1, '', 'product_1195527812.gif');
INSERT INTO `catpd` VALUES (35, 'Lá»c dáº§u', 29, 0, 1, '', 'product_1195527823.gif');
INSERT INTO `catpd` VALUES (36, 'Lá»c giÃ³', 29, 0, 1, '', 'product_1195527831.gif');
INSERT INTO `catpd` VALUES (37, 'bugi', 29, 0, 1, '', 'product_1195527855.gif');
INSERT INTO `catpd` VALUES (38, 'Bá»™ giáº£m sÃ³c', 29, 0, 1, '', 'product_1195527865.gif');
INSERT INTO `catpd` VALUES (39, 'MÃ¡ phanh', 29, 0, 1, '', 'product_1195527888.gif');
INSERT INTO `catpd` VALUES (40, 'Dáº§u bÃ´i trÆ¡n Ä‘á»ng cÆ¡', 29, 0, 1, '', 'product_1195527905.gif');
INSERT INTO `catpd` VALUES (41, 'dáº§u bÃ´i trÆ¡n há»™p sá»‘', 29, 0, 1, '', 'product_1195527911.gif');

-- --------------------------------------------------------

-- 
-- Table structure for table `catpt`
-- 

CREATE TABLE `catpt` (
  `id_catpt` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(50) character set latin1 collate latin1_general_ci NOT NULL default '',
  `parentid` bigint(20) unsigned NOT NULL default '0',
  `thu_tu` bigint(20) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '1',
  PRIMARY KEY  (`id_catpt`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `catpt`
-- 

INSERT INTO `catpt` VALUES (1, 'B&#7897; S&#432;u t&#7853;p &#7843;nh xe', 0, 0, 1);
INSERT INTO `catpt` VALUES (2, 'Ng&#432;&#7901;i &#273;&#7865;p v&#224; xe', 0, 0, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `catrv`
-- 

CREATE TABLE `catrv` (
  `id_catrv` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(50) character set latin1 collate latin1_general_ci NOT NULL default '',
  `parentid` bigint(20) unsigned NOT NULL default '0',
  `thu_tu` bigint(20) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '1',
  PRIMARY KEY  (`id_catrv`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

-- 
-- Dumping data for table `catrv`
-- 

INSERT INTO `catrv` VALUES (11, 'Cho thuÃª', 0, 2, 1);
INSERT INTO `catrv` VALUES (9, 'Mua', 0, 1, 1);
INSERT INTO `catrv` VALUES (10, 'BÃ¡n', 0, 0, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `contact`
-- 

CREATE TABLE `contact` (
  `id_contact` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `content` longtext character set latin1 collate latin1_general_ci NOT NULL,
  `xem` tinyint(4) NOT NULL default '0',
  `ngay` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`id_contact`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `contact`
-- 

INSERT INTO `contact` VALUES (3, 'From: sdsds date: 18/11/2007', 'H&#7885; t&#234;n: sdsds<br>&#272;&#7883;a ch&#7881;:\r\ndsdsd<br>&#272;i&#7879;n tho&#7841;i: 12222<br>fax. 11111<Br>Email. sda@dfff.vn<br>N&#7897;i dung:Â dsdsdsdsd', 1, '0000-00-00');

-- --------------------------------------------------------

-- 
-- Table structure for table `count`
-- 

CREATE TABLE `count` (
  `value` bigint(20) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table `count`
-- 

INSERT INTO `count` VALUES (3608);

-- --------------------------------------------------------

-- 
-- Table structure for table `country`
-- 

CREATE TABLE `country` (
  `id_country` int(11) NOT NULL auto_increment,
  `name` varchar(100) character set latin1 collate latin1_general_ci default NULL,
  `currency` varchar(50) character set latin1 collate latin1_general_ci default NULL,
  `code` varchar(50) character set latin1 collate latin1_general_ci default NULL,
  PRIMARY KEY  (`id_country`),
  UNIQUE KEY `countryID` (`id_country`),
  KEY `countryID_2` (`id_country`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=214 ;

-- 
-- Dumping data for table `country`
-- 

INSERT INTO `country` VALUES (1, 'United States of America', 'USD', 'US');
INSERT INTO `country` VALUES (2, 'Canada', 'CAD', 'CA');
INSERT INTO `country` VALUES (3, 'Afghanistan', 'AFA', 'AF');
INSERT INTO `country` VALUES (4, 'Albania', 'ALL', 'AL');
INSERT INTO `country` VALUES (5, 'Algeria', 'DZD', 'DZ');
INSERT INTO `country` VALUES (6, 'Andorra', 'EUR', 'AD');
INSERT INTO `country` VALUES (7, 'Angola', 'AOA', 'AO');
INSERT INTO `country` VALUES (8, 'Anguilla', 'XCD', 'AI');
INSERT INTO `country` VALUES (10, 'Antigua and Barbuda', 'XCD', 'AG');
INSERT INTO `country` VALUES (11, 'Argentina', 'ARS', 'AR');
INSERT INTO `country` VALUES (12, 'Armenia', 'AMD', 'AM');
INSERT INTO `country` VALUES (13, 'Aruba', 'AWG', 'AW');
INSERT INTO `country` VALUES (14, 'Australia', 'AUD', 'AU');
INSERT INTO `country` VALUES (15, 'Austria', 'EUR', 'AT');
INSERT INTO `country` VALUES (16, 'Azerbaijan', 'AZM', 'AZ');
INSERT INTO `country` VALUES (17, 'Bahamas', 'BSD', 'BS');
INSERT INTO `country` VALUES (18, 'Bahrain', 'BHD', 'BH');
INSERT INTO `country` VALUES (19, 'Bangladesh', 'BDT', 'BD');
INSERT INTO `country` VALUES (20, 'Barbados', 'BBD', 'BB');
INSERT INTO `country` VALUES (21, 'Belarus', 'BYR', 'BY');
INSERT INTO `country` VALUES (22, 'Belgium', 'EUR', 'BE');
INSERT INTO `country` VALUES (23, 'Belize', 'BZD', 'BZ');
INSERT INTO `country` VALUES (24, 'Benin', 'XOF', 'BJ');
INSERT INTO `country` VALUES (25, 'Bermuda', 'BMD', 'BM');
INSERT INTO `country` VALUES (26, 'Bhutan', 'BTN', 'BT');
INSERT INTO `country` VALUES (27, 'Bolivia', 'BOB', 'BO');
INSERT INTO `country` VALUES (28, 'Bosnia-Herzegovina', 'BAM', 'BA');
INSERT INTO `country` VALUES (29, 'Botswana', 'BWP', 'BW');
INSERT INTO `country` VALUES (30, 'Brazil', 'BRL', 'BR');
INSERT INTO `country` VALUES (31, 'Brunei Darussalam', 'BND', 'BN');
INSERT INTO `country` VALUES (32, 'Bulgaria', 'BGL', 'BG');
INSERT INTO `country` VALUES (33, 'Burkina Faso', 'XOF', 'BF');
INSERT INTO `country` VALUES (34, 'Burundi', 'BIF', 'BI');
INSERT INTO `country` VALUES (35, 'Cambodia', 'KHR', 'KH');
INSERT INTO `country` VALUES (36, 'Cameroon', 'XAF', 'CM');
INSERT INTO `country` VALUES (37, 'Cape Verde', 'CVE', 'CV');
INSERT INTO `country` VALUES (38, 'Cayman Islands', 'KYD', 'KY');
INSERT INTO `country` VALUES (39, 'Central African Republic', 'XAF', 'CF');
INSERT INTO `country` VALUES (40, 'Chad', 'XAF', 'TD');
INSERT INTO `country` VALUES (41, 'Chile', 'CHL', 'CL');
INSERT INTO `country` VALUES (42, 'China', 'CNY', 'CN');
INSERT INTO `country` VALUES (43, 'Colombia', 'COP', 'CO');
INSERT INTO `country` VALUES (44, 'Comoros', 'KMF', 'KM');
INSERT INTO `country` VALUES (45, 'Costa Rica', 'CRI', 'CR');
INSERT INTO `country` VALUES (46, 'Croatia', 'HRK', 'HR');
INSERT INTO `country` VALUES (47, 'Cuba', 'CUP', 'CU');
INSERT INTO `country` VALUES (48, 'Cyprus', 'CYP', 'CY');
INSERT INTO `country` VALUES (49, 'Czech Republic', 'CZK', 'CZ');
INSERT INTO `country` VALUES (50, 'Denmark', 'DKK', 'DK');
INSERT INTO `country` VALUES (51, 'Djibouti', 'DJF', 'DJ');
INSERT INTO `country` VALUES (52, 'Dominica', 'XCD', 'DM');
INSERT INTO `country` VALUES (53, 'Dominican Republic', 'DOP', 'DO');
INSERT INTO `country` VALUES (54, 'East Timor', 'IDR', 'TP');
INSERT INTO `country` VALUES (55, 'Ecuador', 'USD', 'EC');
INSERT INTO `country` VALUES (56, 'Egypt', 'EGP', 'EG');
INSERT INTO `country` VALUES (57, 'El Salvador', 'SVC', 'SV');
INSERT INTO `country` VALUES (58, 'Equatorial Guinea', 'XAF', 'GQ');
INSERT INTO `country` VALUES (59, 'Estonia', 'EEK', 'EE');
INSERT INTO `country` VALUES (60, 'Ethiopia', 'ETB', 'ET');
INSERT INTO `country` VALUES (61, 'Falkland Islands', 'FKP', 'FK');
INSERT INTO `country` VALUES (62, 'Faroe Islands', 'DKK', 'FO');
INSERT INTO `country` VALUES (63, 'Fiji', 'FJD', 'FJ');
INSERT INTO `country` VALUES (64, 'Finland', 'EUR', 'FI');
INSERT INTO `country` VALUES (65, 'France', 'EUR', 'FR');
INSERT INTO `country` VALUES (66, 'French Guiana', 'EUR', 'GF');
INSERT INTO `country` VALUES (67, 'French Polynesia', 'XPF', 'PF');
INSERT INTO `country` VALUES (68, 'Gabon', 'XAF', 'GA');
INSERT INTO `country` VALUES (69, 'Gambia', 'GMD', 'GM');
INSERT INTO `country` VALUES (70, 'Georgia, Republic of', 'GEL', 'GE');
INSERT INTO `country` VALUES (71, 'Germany', 'EUR', 'DE');
INSERT INTO `country` VALUES (72, 'Ghana', 'GHC', 'GH');
INSERT INTO `country` VALUES (73, 'Gibraltar', 'GBP', 'GI');
INSERT INTO `country` VALUES (74, 'Greece', 'EUR', 'GR');
INSERT INTO `country` VALUES (75, 'Greenland', 'DKK', 'GL');
INSERT INTO `country` VALUES (76, 'Grenada', 'XCD', 'GD');
INSERT INTO `country` VALUES (77, 'Guadeloupe', 'EUR', 'GP');
INSERT INTO `country` VALUES (78, 'Guam', 'USD', 'GU');
INSERT INTO `country` VALUES (79, 'Guatemala', 'GTQ', 'GT');
INSERT INTO `country` VALUES (80, 'Guinea', 'GNF', 'GN');
INSERT INTO `country` VALUES (81, 'Guinea-Bissau', 'XOF', 'GW');
INSERT INTO `country` VALUES (82, 'Guyana', 'GYD', 'GY');
INSERT INTO `country` VALUES (83, 'Haiti', 'USD', 'HT');
INSERT INTO `country` VALUES (84, 'Honduras', 'HNL', 'HN');
INSERT INTO `country` VALUES (85, 'Hong Kong', 'HKD', 'HK');
INSERT INTO `country` VALUES (86, 'Hungary', 'HUF', 'HU');
INSERT INTO `country` VALUES (87, 'Iceland', 'ISK', 'IS');
INSERT INTO `country` VALUES (88, 'India', 'INR', 'IN');
INSERT INTO `country` VALUES (89, 'Indonesia', 'IDR', 'ID');
INSERT INTO `country` VALUES (90, 'Iraq', 'IQD', 'IQ');
INSERT INTO `country` VALUES (91, 'Ireland', 'EUR', 'IE');
INSERT INTO `country` VALUES (92, 'Israel', 'ILS', 'IL');
INSERT INTO `country` VALUES (93, 'Italy', 'EUR', 'IT');
INSERT INTO `country` VALUES (94, 'Jamaica', 'JMD', 'JM');
INSERT INTO `country` VALUES (95, 'Japan', 'JPY', 'JP');
INSERT INTO `country` VALUES (96, 'Jordan', 'JOD', 'JO');
INSERT INTO `country` VALUES (97, 'Kazakhstan', 'KZT', 'KZ');
INSERT INTO `country` VALUES (98, 'Kenya', 'KES', 'KE');
INSERT INTO `country` VALUES (99, 'Kiribati', 'AUD', 'KI');
INSERT INTO `country` VALUES (100, 'North Korea', 'KPW', 'KP');
INSERT INTO `country` VALUES (101, 'South Korea', 'KRW', 'KR');
INSERT INTO `country` VALUES (102, 'Kuwait', 'KWD', 'KW');
INSERT INTO `country` VALUES (103, 'Latvia', 'LVL', 'LV');
INSERT INTO `country` VALUES (104, 'Lebanon', 'LBP', 'LB');
INSERT INTO `country` VALUES (105, 'Lesotho', 'LSL', 'LS');
INSERT INTO `country` VALUES (106, 'Liberia', 'LRD', 'LR');
INSERT INTO `country` VALUES (107, 'England', 'GBP', 'EN');
INSERT INTO `country` VALUES (108, 'Liechtenstein', 'CHF', 'LI');
INSERT INTO `country` VALUES (109, 'Lithuania', 'LTL', 'LT');
INSERT INTO `country` VALUES (110, 'Luxembourg', 'EUR', 'LU');
INSERT INTO `country` VALUES (111, 'Macao', 'MOP', 'MO');
INSERT INTO `country` VALUES (112, 'Macedonia, Republic of', 'MKD', 'MK');
INSERT INTO `country` VALUES (113, 'Madagascar', 'MGF', 'MG');
INSERT INTO `country` VALUES (114, 'Malawi', 'MWK', 'MW');
INSERT INTO `country` VALUES (115, 'Malaysia', 'MYR', 'MY');
INSERT INTO `country` VALUES (116, 'Maldives', 'MVR', 'MV');
INSERT INTO `country` VALUES (117, 'Mali', 'XOF', 'ML');
INSERT INTO `country` VALUES (118, 'Malta', 'MTL', 'MT');
INSERT INTO `country` VALUES (119, 'Martinique', 'EUR', 'MQ');
INSERT INTO `country` VALUES (120, 'Mauritania', 'MRO', 'MR');
INSERT INTO `country` VALUES (121, 'Mauritius', 'MUR', 'MU');
INSERT INTO `country` VALUES (122, 'Mexico', 'MXN', 'MX');
INSERT INTO `country` VALUES (123, 'Moldova', 'MDL', 'MD');
INSERT INTO `country` VALUES (124, 'Monaco', 'FRF', 'MC');
INSERT INTO `country` VALUES (125, 'Mongolia', 'MNT', 'MN');
INSERT INTO `country` VALUES (126, 'Montserrat', 'XCD', 'MS');
INSERT INTO `country` VALUES (127, 'Morocco', 'MAD', 'MA');
INSERT INTO `country` VALUES (128, 'Mozambique', 'MZM', 'MZ');
INSERT INTO `country` VALUES (129, 'Myanmar', 'MMK', 'MM');
INSERT INTO `country` VALUES (130, 'Namibia', 'NAD', 'NA');
INSERT INTO `country` VALUES (131, 'Nauru', 'AUD', 'NR');
INSERT INTO `country` VALUES (132, 'Nepal', 'NPR', 'NP');
INSERT INTO `country` VALUES (133, 'Netherlands', 'EUR', 'NL');
INSERT INTO `country` VALUES (134, 'Netherlands Antilles', 'ANG', 'AN');
INSERT INTO `country` VALUES (135, 'New Caledonia', 'XPF', 'NC');
INSERT INTO `country` VALUES (136, 'New Zealand', 'NZD', 'NZ');
INSERT INTO `country` VALUES (137, 'Nicaragua', 'NIO', 'NI');
INSERT INTO `country` VALUES (138, 'Niger', 'XOF', 'NE');
INSERT INTO `country` VALUES (139, 'Nigeria', 'NGN', 'NG');
INSERT INTO `country` VALUES (140, 'Niue', 'NZD', 'NU');
INSERT INTO `country` VALUES (141, 'Norfolk Island', 'AUD', 'NF');
INSERT INTO `country` VALUES (142, 'Northern Ireland', 'GBP', 'NB');
INSERT INTO `country` VALUES (143, 'Norway', 'NOK', 'NO');
INSERT INTO `country` VALUES (144, 'Oman', 'OMR', 'OM');
INSERT INTO `country` VALUES (145, 'Pakistan', 'PKR', 'PK');
INSERT INTO `country` VALUES (146, 'Panama', 'PAB', 'PA');
INSERT INTO `country` VALUES (147, 'Papua New Guinea', 'PGK', 'PG');
INSERT INTO `country` VALUES (148, 'Paraguay', 'PYG', 'PY');
INSERT INTO `country` VALUES (149, 'Peru', 'PEN', 'PE');
INSERT INTO `country` VALUES (150, 'Philippines', 'PHP', 'PH');
INSERT INTO `country` VALUES (151, 'Pitcairn Island', 'NZD', 'PN');
INSERT INTO `country` VALUES (152, 'Poland', 'PLN', 'PL');
INSERT INTO `country` VALUES (153, 'Portugal', 'EUR', 'PT');
INSERT INTO `country` VALUES (154, 'Qatar', 'QAR', 'QA');
INSERT INTO `country` VALUES (155, 'Reunion', 'EUR', 'RE');
INSERT INTO `country` VALUES (156, 'Romania', 'ROL', 'RO');
INSERT INTO `country` VALUES (157, 'Russia', 'RUR', 'RU');
INSERT INTO `country` VALUES (158, 'Rwanda', 'RWF', 'RW');
INSERT INTO `country` VALUES (159, 'Saint Kitts', 'XCD', 'KN');
INSERT INTO `country` VALUES (160, 'Saint Lucia', 'XCD', 'LC');
INSERT INTO `country` VALUES (161, 'Saint Vincent and the Grenadines', 'XCD', 'VC');
INSERT INTO `country` VALUES (162, 'Western Samoa', 'WST', 'WS');
INSERT INTO `country` VALUES (163, 'San Marino', 'EUR', 'SM');
INSERT INTO `country` VALUES (164, 'Sao Tome and Principe', 'STD', 'ST');
INSERT INTO `country` VALUES (165, 'Saudi Arabia', 'SAR', 'SA');
INSERT INTO `country` VALUES (166, 'Senegal', 'XOF', 'SN');
INSERT INTO `country` VALUES (167, 'Seychelles', 'SCR', 'SC');
INSERT INTO `country` VALUES (168, 'Sierra Leone', 'SLL', 'SL');
INSERT INTO `country` VALUES (169, 'Singapore', 'SGD', 'SG');
INSERT INTO `country` VALUES (170, 'Slovak Republic', 'SKK', 'SK');
INSERT INTO `country` VALUES (171, 'Slovenia', 'SIT', 'SI');
INSERT INTO `country` VALUES (172, 'Solomon Islands', 'SBD', 'SB');
INSERT INTO `country` VALUES (173, 'Somalia', 'SOS', 'SO');
INSERT INTO `country` VALUES (174, 'South Africa', 'ZAR', 'ZA');
INSERT INTO `country` VALUES (175, 'Spain', 'EUR', 'ES');
INSERT INTO `country` VALUES (176, 'Sri Lanka', 'LKR', 'LK');
INSERT INTO `country` VALUES (177, 'Saint Helena', 'SHP', 'SH');
INSERT INTO `country` VALUES (178, 'Saint Pierre and Miquelon', 'EUR', 'PM');
INSERT INTO `country` VALUES (179, 'Sudan', 'SDD', 'SD');
INSERT INTO `country` VALUES (180, 'Suriname', 'SRG', 'SR');
INSERT INTO `country` VALUES (181, 'Swaziland', 'SZL', 'SZ');
INSERT INTO `country` VALUES (182, 'Sweden', 'SEK', 'SE');
INSERT INTO `country` VALUES (183, 'Switzerland', 'CHF', 'CH');
INSERT INTO `country` VALUES (184, 'Syrian Arab Republic', 'SYP', 'SY');
INSERT INTO `country` VALUES (185, 'Taiwan', 'TWD', 'TW');
INSERT INTO `country` VALUES (186, 'Tajikistan', 'TJS', 'TJ');
INSERT INTO `country` VALUES (187, 'Tanzania', 'TZS', 'TZ');
INSERT INTO `country` VALUES (188, 'Thailand', 'THB', 'TH');
INSERT INTO `country` VALUES (189, 'Togo', 'XOF', 'TG');
INSERT INTO `country` VALUES (190, 'Tokelau', 'NZD', 'TK');
INSERT INTO `country` VALUES (191, 'Tonga', 'TOP', 'TO');
INSERT INTO `country` VALUES (192, 'Trinidad and Tobago', 'TTD', 'TT');
INSERT INTO `country` VALUES (193, 'Tunisia', 'TND', 'TN');
INSERT INTO `country` VALUES (194, 'Turkey', 'TRL', 'TR');
INSERT INTO `country` VALUES (195, 'Turkmenistan', 'TMM', 'TM');
INSERT INTO `country` VALUES (196, 'Turks and Caicos Islands', 'USD', 'TC');
INSERT INTO `country` VALUES (197, 'Tuvalu', 'TVD', 'TV');
INSERT INTO `country` VALUES (198, 'Uganda', 'UGX', 'UG');
INSERT INTO `country` VALUES (199, 'Ukraine', 'UAH', 'UA');
INSERT INTO `country` VALUES (200, 'United Arab Emirates', 'AED', 'AE');
INSERT INTO `country` VALUES (201, 'Great Britain and Northern Ireland', 'GBP', 'GB');
INSERT INTO `country` VALUES (202, 'Uruguay', 'UYU', 'UY');
INSERT INTO `country` VALUES (203, 'Uzbekistan', 'UZS', 'UZ');
INSERT INTO `country` VALUES (204, 'Vanuatu', 'VUV', 'VU');
INSERT INTO `country` VALUES (205, 'Vatican City', 'ITL', 'VA');
INSERT INTO `country` VALUES (206, 'Venezuela', 'VEB', 'VE');
INSERT INTO `country` VALUES (207, 'Vietnam', 'VND', 'VN');
INSERT INTO `country` VALUES (208, 'British Virgin Islands', 'USD', 'VG');
INSERT INTO `country` VALUES (209, 'Wallis and Futuna Islands', 'XPF', 'WF');
INSERT INTO `country` VALUES (210, 'Yemen', 'YER', 'YE');
INSERT INTO `country` VALUES (211, 'Zambia', 'ZMK', 'ZM');
INSERT INTO `country` VALUES (212, 'Zimbabwe', 'ZWD', 'ZW');
INSERT INTO `country` VALUES (213, 'Iran', 'IRR', 'IR');

-- --------------------------------------------------------

-- 
-- Table structure for table `feedback`
-- 

CREATE TABLE `feedback` (
  `id_feedback` bigint(20) unsigned NOT NULL auto_increment,
  `id_user` bigint(20) unsigned NOT NULL default '0',
  `id_product` bigint(20) unsigned NOT NULL default '0',
  `comment` longtext NOT NULL,
  PRIMARY KEY  (`id_feedback`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

-- 
-- Dumping data for table `feedback`
-- 

INSERT INTO `feedback` VALUES (1, 3, 1, 'dsfsdafsdfsadfsdafsdfsdf');
INSERT INTO `feedback` VALUES (2, 3, 1, 'sdfsdfsdfsdfsdafdsafsdafsddsfsd');
INSERT INTO `feedback` VALUES (3, 3, 1, 'sadsadsadsadsadsadsa');
INSERT INTO `feedback` VALUES (4, 3, 1, 'sadsadsadsadsadsadsa');
INSERT INTO `feedback` VALUES (5, 3, 1, 'sadsadsadsadsadsadsa');
INSERT INTO `feedback` VALUES (6, 3, 1, 'fxdfgdfgdfgdf');
INSERT INTO `feedback` VALUES (7, 3, 2, 'xczxCxZCxz');
INSERT INTO `feedback` VALUES (8, 3, 2, 'xczxCxZCxz');
INSERT INTO `feedback` VALUES (9, 3, 2, 'xczxCxZCxz');
INSERT INTO `feedback` VALUES (10, 3, 2, 'xczxCxZCxz');
INSERT INTO `feedback` VALUES (11, 3, 2, 'xczxCxZCxz');
INSERT INTO `feedback` VALUES (12, 3, 2, 'xczxCxZCxz');
INSERT INTO `feedback` VALUES (13, 3, 2, 'xczxCxZCxz');
INSERT INTO `feedback` VALUES (14, 3, 2, 'xczxCxZCxz');
INSERT INTO `feedback` VALUES (15, 3, 2, 'xczxCxZCxz');
INSERT INTO `feedback` VALUES (16, 3, 2, 'xczxCxZCxz');
INSERT INTO `feedback` VALUES (17, 3, 2, 'xczxCxZCxz');
INSERT INTO `feedback` VALUES (18, 3, 2, 'xczxCxZCxz');
INSERT INTO `feedback` VALUES (19, 3, 2, 'xczxCxZCxz');
INSERT INTO `feedback` VALUES (20, 3, 2, 'xczxCxZCxz');
INSERT INTO `feedback` VALUES (21, 3, 8, 'Ki&#7875;u d&#225;ng tuy&#7879;t v&#7901;i');
INSERT INTO `feedback` VALUES (22, 3, 8, 'Ki&#7875;u d&#225;ng tuy&#7879;t v&#7901;i');

-- --------------------------------------------------------

-- 
-- Table structure for table `info`
-- 

CREATE TABLE `info` (
  `id_info` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `content` longtext character set latin1 collate latin1_general_ci NOT NULL,
  `thu_tu` bigint(20) default '0',
  `id_catif` bigint(11) NOT NULL default '0',
  `id_user` bigint(20) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '0',
  `ngay_dang` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`id_info`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `info`
-- 

INSERT INTO `info` VALUES (1, 'Giá»›i thiá»‡u vá» KYLIN - GX 668', '<p><font color="#ff0000" size="3"><strong><em><font color="#ff6600">C&ocirc;ng ty CP TM KYLIN - GX668 gá»­i lá»i ch&agrave;o tr&acirc;n trá»ng v&agrave; lá»i ch&uacute;c sá»©c khoáº» tá»›i Qu&yacute; kh&aacute;ch h&agrave;ng c&ugrave;ng gia Ä‘&igrave;nh !<br />\r\n</font></em><img style="WIDTH: 244px; HEIGHT: 174px" height="188" alt="" width="250" align="left" border="0" src="http://images4.dantri.com.vn/Uploaded/lanlt/Thang11-2007/6-161107.jpg" />&nbsp;&nbsp;&nbsp; </strong></font><font color="#ff0000" size="3"><font color="#000000" size="2">Vinh dá»± c&oacute; máº·t vá»›i tÆ° c&aacute;ch l&agrave; nh&agrave; cung cáº¥p h&agrave;ng Ä‘áº§u c&aacute;c loáº¡i xe &ocirc;t&ocirc; nháº­pkháº©u táº¡i thá»‹ trÆ°á»ng Viá»‡t Nam, l&agrave; cáº§u ná»‘i giá»¯a thá»‹ trÆ°á»ng &Ocirc;t&ocirc; trong nÆ°á»›c v&agrave; tháº¿ giá»›i,&nbsp;Kylin mang Ä‘áº¿n cho Qu&yacute; kh&aacute;ch&nbsp; h&agrave;ng th&ecirc;m nhiá»u sá»± lá»±a chá»n má»›i,</font></font><font color="#ff0000" size="3"><font color="#000000" size="2">Ä‘&oacute; l&agrave; nhá»¯ng chiáº¿c </font></font>MERCEDES, BMW, LEXUS h&agrave;o nho&aacute;ng, sang trá»ng; Ä‘&oacute; l&agrave; nhá»¯ng chiáº¿c INFINITI vá»›i thiáº¿t káº¿ máº¡nh máº½ Ä‘áº§y quyáº¿n rÅ©; nhá»¯ng chiáº¿c NISSAN viá»‡t d&atilde; thá»ƒ thao áº¥n tÆ°á»£ng; TOYOTA c&aacute; t&iacute;nh v&agrave; nhiá»u kh&aacute;m ph&aacute;; hay vá»›i nhá»¯ng MATIZ, KIA MORNING nhá» gá»n m&agrave; tao nh&atilde;... ph&ugrave; há»£p vá»›i c&aacute;c c&aacute; nh&acirc;n v&agrave; doanh nghiá»‡p th&agrave;nh Ä‘áº¡t. Táº¡i Ä‘&acirc;y Qu&yacute; kh&aacute;ch h&agrave;ng sáº½ Ä‘Æ°á»£c Ä‘áº£m báº£o to&agrave;n bá»™ c&aacute;c ti&ecirc;u ch&iacute;:<br />\r\n<br />\r\n&nbsp;&nbsp; Cháº¥t lÆ°á»£ng cá»§a sáº£n pháº©m Ä‘áº£m báº£o Ä‘&uacute;ng theo nhá»¯ng g&igrave; m&agrave;&nbsp;Kylin Ä‘&atilde; ch&agrave;o b&aacute;n. </p>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; *&nbsp;&nbsp;&nbsp; Gi&aacute; cáº£ cá»§a sáº£n pháº©m Ä‘Æ°á»£c Ä‘áº£m báº£o trong to&agrave;n l&atilde;nh thá»• Viá»‡t Nam. </p>\r\n<ul>\r\n    <li>Sáº£n pháº©m Ä‘Æ°á»£c báº£o h&agrave;nh v&agrave; báº£o tr&igrave; theo Ä‘&uacute;ng ti&ecirc;u chuáº©n cá»§a nh&agrave; sáº£n xuáº¥t. </li>\r\n</ul>\r\n<p>S&aacute;t c&aacute;nh c&ugrave;ng Ban l&atilde;nh Ä‘áº¡o giá»i, d&agrave;y dáº¡n kinh nghiá»‡m trong ng&agrave;nh kinh doanh &ocirc;t&ocirc; l&agrave; má»™t Ä‘á»™i ngÅ© c&aacute;n bá»™, nh&acirc;n vi&ecirc;n tráº», nÄƒng Ä‘á»™ng, lá»‹ch thiá»‡p v&agrave; Ä‘áº§y nhiá»‡t huyáº¿t, b&ecirc;n cáº¡nh Ä‘&oacute; l&agrave; má»™t m&ocirc;i trÆ°á»ng l&agrave;m viá»‡c chuy&ecirc;n nghiá»‡p, má»™t cÆ¡ sá»Ÿ váº­t cháº¥t trang bá»‹ tiá»‡n nghi, hiá»‡n Ä‘áº¡i,&nbsp;Kylin mong muá»‘n mang Ä‘áº¿n cho Qu&yacute; kh&aacute;ch h&agrave;ng nhá»¯ng sáº£n pháº©m Ä‘a dáº¡ng, Ä‘áº£m báº£o cháº¥t lÆ°á»£ng k&egrave;m theo nhá»¯ng dá»‹ch vá»¥ ho&agrave;n háº£o:<br />\r\n</p>\r\n<ul>\r\n    <li>Dá»‹ch vá»¥ cung cáº¥p v&agrave; nháº­p kháº©u uá»· th&aacute;c c&aacute;c d&ograve;ng xe &ocirc; t&ocirc; Ä‘Æ°á»£c ph&eacute;p nháº­p kháº©u vá» Viá»‡t Nam. </li>\r\n    <li>Dá»‹ch vá»¥ Ä‘á»•i xe, sang t&ecirc;n Ä‘á»•i biá»ƒn sá»‘ c&aacute;c d&ograve;ng xe Ä‘ang lÆ°u h&agrave;nh táº¡i Viá»‡t Nam. </li>\r\n    <li>Dá»‹ch vá»¥ Ä‘Äƒng tin b&aacute;n xe miá»…n ph&iacute; cho má»i Ä‘á»‘i tÆ°á»£ng kh&aacute;ch h&agrave;ng tr&ecirc;n Website cá»§a Kylin. </li>\r\n    <li>Dá»‹ch vá»¥ tÆ° váº¥n, láº­p phÆ°Æ¡ng &aacute;n ho&agrave;n thiá»‡n thá»§ tá»¥c vay vá»‘n tráº£ g&oacute;p, ná»™p thuáº¿ v&agrave; Ä‘Äƒng kiá»ƒm xe trong thá»i gian nhanh nháº¥t. </li>\r\n</ul>\r\n<p><font color="#ff6600" size="3"><em><strong>Ch&uacute;ng t&ocirc;i h&acirc;n háº¡nh k&iacute;nh má»i Qu&yacute; kh&aacute;ch h&agrave;ng gh&eacute; thÄƒm Showroom Kylin</strong></em></font></p>', 0, 1, 7, 1, 1194949079);
INSERT INTO `info` VALUES (4, 'ThÃ´ng tin trÃªn trang liÃªn há»‡', '<p align="center">&nbsp;<font face="Arial">C&Ocirc;NG TY TNHH THI&Ecirc;N Báº¢O ANH<br />\r\nÄC: 33-43 L&ecirc; VÄƒn LÆ°Æ¡ng - Trung H&ograve;a - Cáº§u Giáº¥y - H&agrave; ná»™i<br />\r\nÄT: 04. 210 3228&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; *&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; FAX: 04. 556 7322<br />\r\nEmail: 123@yahoo.com&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; *&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Website: thienbaoanh.com.vn</font></p>', 0, 4, 7, 1, 1195031593);
INSERT INTO `info` VALUES (3, 'CÆ¡ cháº¿ báº£o hÃ nh', '<p align="center"><font color="#ff0000" size="1">Dá»¯ liá»‡u Ä‘ang Ä‘Æ°á»£c cáº­p nháº­t !</font></p>', 0, 3, 7, 1, 1194949202);

-- --------------------------------------------------------

-- 
-- Table structure for table `logo`
-- 

CREATE TABLE `logo` (
  `id_logo` bigint(20) unsigned NOT NULL auto_increment,
  `id_catlg` bigint(20) unsigned NOT NULL default '0',
  `name` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `link` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `ngay_dang` bigint(20) unsigned NOT NULL default '0',
  `thu_tu` bigint(20) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '1',
  `small_image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `normal_image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  PRIMARY KEY  (`id_logo`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

-- 
-- Dumping data for table `logo`
-- 

INSERT INTO `logo` VALUES (1, 2, 'Ho&#224;ng gia auto', '', 'http://hoanggiaauto.com', 1195030069, 0, 1, '', '');
INSERT INTO `logo` VALUES (2, 2, 'Vnexpress.net', '', 'http://vnexpress.net', 1195030089, 0, 1, '', '');
INSERT INTO `logo` VALUES (10, 1, 'abc', 'logo_1195267259.swf', '', 1195267259, 0, 1, '', '');
INSERT INTO `logo` VALUES (6, 6, 'phai', 'logo_1195269348.gif', '', 1195181590, 0, 1, 'thumb_logo_1195181589.jpeg', 'normal_logo_1195181589.jpeg');
INSERT INTO `logo` VALUES (7, 5, 'sd', 'logo_1195269377.gif', '', 1195181599, 0, 1, 'thumb_logo_1195181598.jpeg', 'normal_logo_1195181598.jpeg');
INSERT INTO `logo` VALUES (12, 7, 'Quáº£ng cÃ¡o pháº§n trÃªn trang chá»§', 'logo_1195526169.gif', '', 1195526169, 0, 1, '', '');
INSERT INTO `logo` VALUES (13, 7, 'Quáº£ng cÃ¡o pháº§n giá»¯a trang chá»§', 'logo_1195526212.gif', '', 1195526213, 0, 1, '', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `maillist`
-- 

CREATE TABLE `maillist` (
  `id_maillist` bigint(20) unsigned NOT NULL auto_increment,
  `subject` varchar(255) NOT NULL default '',
  `message` longtext NOT NULL,
  `creat_date` bigint(20) NOT NULL default '0',
  `id_users` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`id_maillist`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `maillist`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `module`
-- 

CREATE TABLE `module` (
  `id_module` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `gia_tri` varchar(255) character set latin7 NOT NULL default '',
  `thu_tu` bigint(20) unsigned NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id_module`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=69 ;

-- 
-- Dumping data for table `module`
-- 

INSERT INTO `module` VALUES (5, 'Module manager', 'module.php', 0, 1);
INSERT INTO `module` VALUES (9, 'C&#7845;u h&#236;nh h&#7879; th&#7889;ng', 'settings.php', 0, 1);
INSERT INTO `module` VALUES (45, 'Qu&#7843;n l&#253; database', 'dbadmin.php', 0, 1);
INSERT INTO `module` VALUES (26, 'Th&#244;ng tin li&#234;n h&#7879; th&#7915; kh&#225;ch h&#224;ng', 'contact.php', 0, 1);
INSERT INTO `module` VALUES (27, 'Qu&#7843;n l&#253; s&#7843;n ph&#7849;m', 'catpd.php', 0, 1);
INSERT INTO `module` VALUES (48, 'Qu&#7843;n l&#253; tin t&#7913;c', 'cat.php', 0, 1);
INSERT INTO `module` VALUES (36, 'Quáº£n lÃ½ logo - banner', 'catlg.php', 0, 1);
INSERT INTO `module` VALUES (53, 'H&#7895; tr&#7907; tr&#7921;c tuy&#7871;n', 'yahoo.php', 0, 1);
INSERT INTO `module` VALUES (52, 'Qu&#7843;n l&#253; trang n&#7897;i dung', 'catif.php', 0, 1);
INSERT INTO `module` VALUES (42, 'Qu&#7843;n l&#253; ng&#432;&#7901;i s&#7917; d&#7909;ng', 'users.php', 0, 1);
INSERT INTO `module` VALUES (43, 'Th&#244;ng tin c&#225; nh&#226;n', 'myadmin.php', 0, 1);
INSERT INTO `module` VALUES (56, 'shortcut', 'short.php', 0, 1);
INSERT INTO `module` VALUES (57, 'B&#7897; s&#432;u t&#7853;p &#7843;nh', 'catpt.php', 0, 1);
INSERT INTO `module` VALUES (65, 'Qu&#7843;n l&#253; th&#224;nh vi&#234;n', 'users_post.php', 0, 1);
INSERT INTO `module` VALUES (64, 'Th&#244;ng tin &#273;&#7863;t h&#224;ng', 'order.php', 0, 1);
INSERT INTO `module` VALUES (66, 'Quáº£n lÃ½ &#7843;nh sáº£n pháº©m', 'carphoto.php', 0, 1);
INSERT INTO `module` VALUES (67, 'Qu&#7843;n l&#253; maillist', 'maillist.php', 0, 1);
INSERT INTO `module` VALUES (68, 'Rao v&#7863;t', 'catrv.php', 0, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `news`
-- 

CREATE TABLE `news` (
  `id_news` bigint(20) unsigned NOT NULL auto_increment,
  `id_cat` bigint(20) unsigned NOT NULL default '0',
  `name` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `title` varchar(255) NOT NULL default '',
  `image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `intro` mediumtext character set latin1 collate latin1_general_ci NOT NULL,
  `content` text character set latin1 collate latin1_general_ci NOT NULL,
  `ngay_dang` bigint(20) unsigned NOT NULL default '0',
  `thu_tu` bigint(20) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '1',
  `small_image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `normal_image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `id_user` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`id_news`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

-- 
-- Dumping data for table `news`
-- 

INSERT INTO `news` VALUES (12, 3, '&#39;SiÃªu cáº·p&#39; cá»§a há»c sinh tiá»ƒu há»c náº·ng gáº§n 5 kg', '', 'news_1195200940.jpg', 'S&aacute;ng 13/11, l&atilde;nh Ä‘áº¡o Vá»¥ Gi&aacute;o dá»¥c tiá»ƒu há»c, Bá»™ GD&amp;ÄT v&agrave; Ä‘áº¡i diá»‡n Sá»Ÿ GD&amp;ÄT H&agrave; Ná»™i Ä‘&atilde; chá»n ngáº«u nhi&ecirc;n má»™t sá»‘ trÆ°á»ng tiá»ƒu há»c táº¡i thá»§ Ä‘&ocirc; Ä‘á»ƒ kiá»ƒm tra cáº·p s&aacute;ch cá»§a há»c sinh.<br />', '<p class="Normal" align="left">Äá»£t kiá»ƒm tra n&agrave;y theo chá»‰ Ä‘áº¡o cá»§a Ph&oacute; thá»§ tÆ°á»›ng, Bá»™ trÆ°á»Ÿng GD&amp;ÄT Nguyá»…n Thiá»‡n Nh&acirc;n.</p>\r\n<p class="SubTitle" align="left">Cha máº¹ g&oacute;p pháº§n l&agrave;m cáº·p náº·ng th&ecirc;m</p>\r\n<p class="Normal" align="left">Má»™t há»c sinh lá»›p 4G Tiá»ƒu há»c T&acirc;y SÆ¡n, (quáº­n Hai B&agrave; TrÆ°ng) c&oacute; cáº·p náº·ng nháº¥t lá»›p Ä‘&atilde; mang theo s&aacute;ch vá»Ÿ quy Ä‘á»‹nh trong thá»i kho&aacute; biá»ƒu v&agrave; má»™t sá»‘ váº­t dá»¥ng kh&aacute;c nhÆ° Ä‘áº¥t náº·n, m&agrave;u s&aacute;p, b&aacute;o, nÆ°á»›c uá»‘ng, &aacute;o mÆ°a, truyá»‡n... C&ocirc; b&eacute; n&agrave;y há»“n nhi&ecirc;n cho biáº¿t: &quot;Mang th&ecirc;m (c&aacute;c váº­t dá»¥ng, s&aacute;ch vá»Ÿ kh&ocirc;ng c&oacute; trong thá»i kho&aacute; biá»ƒu) Ä‘á»ƒ dá»± ph&ograve;ng!&quot;.</p>\r\n<p class="Normal" align="left">C&ograve;n táº¡i lá»›p 2 Tiá»ƒu há»c Nguyá»…n Tri PhÆ°Æ¡ng, c&oacute; chiáº¿c cáº·p s&aacute;ch náº·ng 4,2 kg. NhÆ°ng trong Ä‘&oacute;, sá»‘ s&aacute;ch, vá»Ÿ pháº£i mang theo quy Ä‘á»‹nh chá»‰ náº·ng 2 kg, ri&ecirc;ng chiáº¿c cáº·p Ä‘&atilde; náº·ng 1,2 kg, c&ograve;n vá»Ÿ v&agrave; c&aacute;c váº­t dá»¥ng kh&aacute;c náº·ng 1 kg. Má»™t em á»Ÿ lá»›p n&agrave;y c&oacute; cáº·p s&aacute;ch náº·ng 4 kg cho biáº¿t: &quot;Ch&aacute;u Ä‘&atilde; n&oacute;i vá»›i bá»‘ máº¹ nhiá»u láº§n l&agrave; cáº·p náº·ng qu&aacute;, con mang kh&ocirc;ng ná»•i. NhÆ°ng bá»‘ máº¹ ch&aacute;u báº£o cá»© cá»‘ m&agrave; mang Ä‘i&quot;. Trong cáº·p cá»§a em n&agrave;y c&ograve;n c&oacute; cáº£ sá»¯a, nÆ°á»›c uá»‘ng.</p>\r\n<p class="Normal" align="left">&Ocirc;ng Pháº¡m Xu&acirc;n Tiáº¿n, TrÆ°á»Ÿng ph&ograve;ng Gi&aacute;o dá»¥c tiá»ƒu há»c, Sá»Ÿ GD&amp;ÄT H&agrave; Ná»™i, cho biáº¿t, th&aacute;ng 9/2005, Sá»Ÿ Ä‘&atilde; c&oacute; c&ocirc;ng vÄƒn hÆ°á»›ng dáº«n dáº¡y 2 buá»•i má»™t ng&agrave;y v&agrave; quy Ä‘á»‹nh c&aacute;c loáº¡i vá»Ÿ cá»§a há»c sinh tiá»ƒu há»c. Qua kiá»ƒm tra, nhá»¯ng em mang s&aacute;ch vá»Ÿ Ä‘&uacute;ng y&ecirc;u cáº§u th&igrave; cáº·p s&aacute;ch chá»‰ náº·ng khoáº£ng 2,7 - 3 kg. </p>\r\n<p class="Normal" align="left">NhÆ°ng Ä‘a pháº§n há»c sinh kh&ocirc;ng mang s&aacute;ch vá»Ÿ theo Ä‘&uacute;ng quy Ä‘á»‹nh v&agrave; phá»¥ huynh, gi&aacute;o vi&ecirc;n chá»§ nhiá»‡m cÅ©ng kh&ocirc;ng thÆ°á»ng xuy&ecirc;n nháº¯c nhá»Ÿ, kiá»ƒm tra. Tiá»ƒu há»c Nguyá»…n Tri PhÆ°Æ¡ng c&ograve;n y&ecirc;u cáº§u há»c sinh mua th&ecirc;m vá»Ÿ luyá»‡n chá»¯. Nhiá»u em Ä‘Æ°á»£c cha máº¹ trang bá»‹ cho nhá»¯ng chiáº¿c cáº·p lá»›n qu&aacute; cá»¡, chá»‰ ri&ecirc;ng c&acirc;n náº·ng cá»§a cáº·p Ä‘&atilde; Ä‘áº¿n 1,5 kg. </p>\r\n<p class="SubTitle" align="left">Cáº§n quy Ä‘á»‹nh chuáº©n vá» c&acirc;n náº·ng cáº·p s&aacute;ch</p>\r\n<p class="Normal" align="left">Táº¡i Tiá»ƒu há»c Tráº§n Quá»‘c Toáº£n, b&agrave; Lan Anh, Hiá»‡u trÆ°á»Ÿng trÆ°á»ng cho biáº¿t, 100% tr&ograve; Ä‘Æ°á»£c há»c 2 buá»•i má»™t ng&agrave;y v&agrave; má»—i lá»›p c&oacute; 1 ph&ograve;ng há»c ri&ecirc;ng. Há»c sinh c&oacute; thá»ƒ Ä‘á»ƒ láº¡i lá»›p má»™t sá»‘ s&aacute;ch, vá»Ÿ, Ä‘á»“ d&ugrave;ng há»c táº­p. C&aacute;c em lá»›p 1, 2, 3 gáº§n nhÆ° kh&ocirc;ng pháº£i mang s&aacute;ch vá»Ÿ vá» nh&agrave; do trÆ°á»ng cháº¥p h&agrave;nh chá»§ trÆ°Æ¡ng kh&ocirc;ng giao b&agrave;i táº­p vá» nh&agrave;. Tuy nhi&ecirc;n á»Ÿ H&agrave; Ná»™i, nháº¥t l&agrave; c&aacute;c quáº­n ná»™i th&agrave;nh, kh&ocirc;ng máº¥y trÆ°á»ng c&oacute; Ä‘á»§ cÆ¡ sá»Ÿ váº­t cháº¥t cho c&aacute;c em Ä‘Æ°á»£c há»c ng&agrave;y 2 buá»•i ngay táº¡i má»™t ph&ograve;ng há»c. </p>\r\n<p class="Normal" align="left">Má»™t sá»‘ trÆ°á»ng váº«n c&ograve;n chung cÆ¡ sá»Ÿ váº­t cháº¥t, nhÆ° TrÆ°á»ng Tiá»ƒu há»c L&ecirc; Ngá»c H&acirc;n, Báº¿ VÄƒn Ä&agrave;n, c&oacute; trÆ°á»ng váº«n tá»“n táº¡i nhiá»u Ä‘iá»ƒm láº» nhÆ° Tiá»ƒu há»c Quang Trung, ThÄƒng Long. Nhiá»u trÆ°á»ng pháº£i há»c buá»•i hai á»Ÿ nh&agrave; d&acirc;n, há»c sinh pháº£i Ä‘i tá»« trÆ°á»ng Ä‘áº¿n nÆ¡i há»c buá»•i 2 kh&aacute; xa... </p>\r\n<p class="Normal" align="left">Táº¡i láº§n &quot;c&acirc;n cáº·p&quot; c&aacute;ch Ä‘&acirc;y 4 nÄƒm, l&atilde;nh Ä‘áº¡o Vá»¥ Gi&aacute;o dá»¥c tiá»ƒu há»c ph&aacute;t biá»ƒu ráº±ng, ch&uacute;ng ta má»›i chá»‰ c&oacute; chuáº©n vá» b&agrave;n gháº¿ há»c sinh, ph&ograve;ng há»c, váº­y n&ecirc;n tiáº¿n tá»›i cÅ©ng cáº§n c&oacute; &quot;chuáº©n&quot; vá» trá»ng lÆ°á»£ng cáº·p s&aacute;ch. </p>\r\n<p class="Normal" align="left">Váº­y nhÆ°ng sau má»™t thá»i gian d&agrave;i, chiáº¿c cáº·p há»c sinh chÆ°a nháº¹ Ä‘i l&agrave; bao v&agrave; &quot;chuáº©n&quot; trá»ng lÆ°á»£ng cáº·p s&aacute;ch th&igrave; váº«n chá»‰ l&agrave; &yacute; tÆ°á»Ÿng. ChÆ°a c&oacute; má»™t nghi&ecirc;n cá»©u n&agrave;o vá» viá»‡c cáº·p há»c há»c tr&ograve; náº·ng bao nhi&ecirc;u l&agrave; vá»«a. </p>\r\n<p class="Normal" align="left">Táº¡i c&aacute;c trÆ°á»ng tiá»ƒu há»c T&acirc;y SÆ¡n, Nguyá»…n Tri PhÆ°Æ¡ng, gi&aacute;o vi&ecirc;n chá»§ nhiá»‡m c&aacute;c lá»›p 1, 2 cho biáº¿t, c&oacute; em chá»‰ náº·ng 14 - 15 kg. Vá»›i v&oacute;c d&aacute;ng áº¥y, c&aacute;c em pháº£i Ä‘eo nhá»¯ng chiáº¿c cáº·p náº·ng khoáº£ng 4kg.</p>\r\n<p class="Normal" align="right">(Theo <em>Lao Äá»™ng</em>)</p>', 1195200940, 0, 1, 'thumb_news_1195200940.jpg', '', 7);
INSERT INTO `news` VALUES (8, 1, 'Bá»™ sÆ°u táº­p siÃªu xe táº¡i SÃ i GÃ²n', '', 'news_1195200310.jpg', 'Hai ngá»±a chiáº¿n Ferrari F360 v&agrave; F430, b&ograve; t&oacute;t Lamborghini Gallardo v&agrave; Audi R8 tá»¥ há»p b&ecirc;n cáº¡nh &quot;h&agrave;ng Ä‘á»™c&quot; LS600hL c&ugrave;ng Hummer H3 limounsine.<br />', '<p class="Normal">H&ocirc;m qua 15/11, nhá»¯ng si&ecirc;u xe ná»•i Ä‘&igrave;nh Ä‘&aacute;m nháº¥t Viá»‡t Nam Ä‘&atilde; xuáº¥t hiá»‡n trong lá»… khá»Ÿi c&ocirc;ng Auto Kingdom, do c&ocirc;ng ty cá»• pháº§n S&oacute;ng Tháº§n x&acirc;y dá»±ng táº¡i Khu c&ocirc;ng nghiá»‡p S&oacute;ng Tháº§n II, B&igrave;nh DÆ°Æ¡ng.</p>\r\n<p class="Normal">Vá»›i diá»‡n t&iacute;ch 70.000 m&eacute;t vu&ocirc;ng giai Ä‘oáº¡n 1 v&agrave; tá»•ng vá»‘n Ä‘áº§u tÆ° 25 triá»‡u USD, Auto Kingdom l&agrave; nÆ¡i táº­p trung háº§u háº¿t c&aacute;c dá»‹ch vá»¥ li&ecirc;n quan Ä‘áº¿n xe hÆ¡i nhÆ° kinh doanh, sá»­a chá»¯a, tÆ° váº¥n v&agrave; Ä‘&agrave;o táº¡o l&aacute;i xe.</p>\r\n<table cellspacing="0" cellpadding="3" width="1" align="center" border="0">\r\n    <tbody>\r\n        <tr>\r\n            <td class="Image"><img height="275" width="380" border="1" alt="" src="http://vnexpress.net/Vietnam/Oto-Xe-may/2007/11/3B9FC5BA/Audi-R8.jpg" /></td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image"><img height="285" width="380" border="1" alt="" src="http://vnexpress.net/Vietnam/Oto-Xe-may/2007/11/3B9FC5BA/R8.jpg" /></td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image"><img height="285" width="380" border="1" alt="" src="http://vnexpress.net/Vietnam/Oto-Xe-may/2007/11/3B9FC5BA/ntR8.jpg" /></td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image">Chiáº¿c Audi R8 vá» TP HCM há»“i giá»¯a th&aacute;ng 9. Audi R8 l&agrave; Ä‘á»‘i thá»§ cá»§a Porsche 911 Carrera 4S, Aston Martin V8 Vantage hay BMW M6. Äiá»u áº¥n tÆ°á»£ng vá»›i d&acirc;n chÆ¡i xe Viá»‡t Nam l&agrave; &iacute;t ai ngá» R8 láº¡i xuáº¥t hiá»‡n nhanh Ä‘áº¿n váº­y, bá»Ÿi vá»›i sáº£n lÆ°á»£ng chá»‰ khoáº£ng 15 chiáº¿c má»—i ng&agrave;y, viá»‡c táº­u n&oacute; á»Ÿ ngay c&aacute;c thá»‹ trÆ°á»ng lá»›n l&agrave; Ä‘iá»u kh&ocirc;ng dá»…. R8 máº¥t 4,6 gi&acirc;y Ä‘á»ƒ tÄƒng tá»‘c 0-100 km/h, váº­n tá»‘c tá»‘i Ä‘a 301 km/h. </td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image"><img height="275" width="380" border="1" alt="" src="http://vnexpress.net/Vietnam/Oto-Xe-may/2007/11/3B9FC5BA/lLamborghini.jpg" /></td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image"><img height="285" width="380" border="1" alt="" src="http://vnexpress.net/Vietnam/Oto-Xe-may/2007/11/3B9FC5BA/lamborghini-va-Ferarri-F430.jpg" /></td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image"><img height="268" width="380" border="1" alt="" src="http://vnexpress.net/Vietnam/Oto-Xe-may/2007/11/3B9FC5BA/Lam.jpg" /></td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image"><img height="285" width="380" border="1" alt="" src="http://vnexpress.net/Vietnam/Oto-Xe-may/2007/11/3B9FC5BA/lamborghini.jpg" /></td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image">Lamborghini Gallardo, chiáº¿c si&ecirc;u xe Ä‘&atilde; l&agrave;m n&ecirc;n cÆ¡n &quot;Ä‘á»‹a cháº¥n&quot; trong l&agrave;ng chÆ¡i &ocirc;t&ocirc; khi xuáº¥t hiá»‡n Ä‘á»™t ngá»™t táº¡i Viá»‡t Nam v&agrave;o Ä‘áº§u th&aacute;ng 5. Gallardo Ä‘Æ°á»£c Lamborghini sáº£n xuáº¥t tá»« nÄƒm 2003, trang bá»‹ Ä‘á»™ng cÆ¡ V10 dung t&iacute;ch 5.0 l&iacute;t. Chá»‰ sau 3 nÄƒm c&oacute; máº·t, c&oacute; tá»›i 5.000 chiáº¿c Gallardo Ä‘Æ°á»£c ra l&ograve; v&agrave; l&agrave; máº«u b&aacute;n cháº¡y nháº¥t trong lá»‹ch sá»­ cá»§a Lamborghini. N&oacute; l&agrave; Ä‘á»‘i thá»§ cá»§a Ferrari 360 Modena</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image"><img height="278" width="380" border="1" alt="" src="http://vnexpress.net/Vietnam/Oto-Xe-may/2007/11/3B9FC5BA/1.jpg" /></td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image">Bá»™ ba Audi R8, Ferrari F360 Spider v&agrave; Lexus LS600hL trong ng&agrave;y khá»Ÿi c&ocirc;ng Auto Kingdom.</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image"><img height="285" width="380" border="1" alt="" src="http://vnexpress.net/Vietnam/Oto-Xe-may/2007/11/3B9FC5BA/Lexus-LS600hL.jpg" /></td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image"><img height="285" width="380" border="1" alt="" src="http://vnexpress.net/Vietnam/Oto-Xe-may/2007/11/3B9FC5BA/noithat-LS600hL.jpg" /></td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image">LS600hL kh&ocirc;ng xáº¿p v&agrave;o d&ograve;ng si&ecirc;u xe nhÆ°ng n&oacute; ná»•i tiáº¿ng nhá» kháº£ nÄƒng chÆ¡i xe thá»i thÆ°á»£ng cá»§a c&aacute;c Ä‘áº¡i gia S&agrave;i G&ograve;n. Má»›i chá»‰ ra máº¯t há»“i th&aacute;ng 5 nhÆ°ng LS600hL nhanh ch&oacute;ng c&oacute; máº·t táº¡i Viá»‡t Nam. Ä&acirc;y l&agrave; chiáº¿c hybrid xÄƒng-Ä‘iá»‡n Ä‘áº¯t nháº¥t tháº¿ giá»›i vá»›i gi&aacute; táº¡i Má»¹ khoáº£ng 124.000 USD.</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image"><img height="275" width="380" border="1" alt="" src="http://vnexpress.net/Vietnam/Oto-Xe-may/2007/11/3B9FC5BA/Ferarri-F-360-Spider.jpg" /></td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image">F360 Spider, chiáº¿c Ferrari Ä‘áº§u ti&ecirc;n c&oacute; máº·t táº¡i Viá»‡t Nam v&agrave;o cuá»‘i th&aacute;ng 5. Ban Ä‘áº§u F360 Spider c&oacute; m&agrave;u báº¡c nhÆ°ng chá»§ nh&acirc;n cá»§a n&oacute; Ä‘&atilde; thu&ecirc; thá»£ sÆ¡n láº¡i th&agrave;nh m&agrave;u Ä‘á» cho Ä‘á»“ng bá»™ vá»›i F430 nháº­p vá» há»“i th&aacute;ng 10. Gi&aacute; má»™t chiáº¿c F360 Spider Ä‘á»i 2004 táº¡i Má»¹ v&agrave;o khoáº£ng 140.000 USD.</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image"><img height="285" width="380" border="1" alt="" src="http://vnexpress.net/Vietnam/Oto-Xe-may/2007/11/3B9FC5BA/3.jpg" />&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image">F430, máº«u xe m&agrave; Ferrari thay tháº¿ cho F360. Ferrari F430 sá»­ dá»¥ng Ä‘á»™ng cÆ¡ xÄƒng V8 dung t&iacute;ch 4,3L, c&ocirc;ng suáº¥t 483 m&atilde; lá»±c, tÄƒng tá»‘c tá»« 0 l&ecirc;n 100 km/h trong 4 gi&acirc;y v&agrave; Ä‘áº¡t tá»‘c Ä‘á»™ cá»±c Ä‘áº¡i 315 km/h. Gi&aacute; cá»§a F430 táº¡i Má»¹ v&agrave;o khoáº£ng 170.000 USD.</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image"><img height="267" width="380" border="1" alt="" src="http://vnexpress.net/Vietnam/Oto-Xe-may/2007/11/3B9FC5BA/H3-Limo.jpg" /></td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image">Chiáº¿c Hummer H3 limounsine Ä‘áº§u ti&ecirc;n táº¡i Viá»‡t Nam.</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<p class="Normal" align="right"><em designtimesp="28189">B&agrave;i v&agrave; áº£nh</em>: <strong designtimesp="28190">Minh Anh</strong></p>', 1195200311, 0, 1, 'thumb_news_1195200310.jpg', 'normal_news_1195200310.jpg', 7);
INSERT INTO `news` VALUES (9, 1, 'TÄƒng háº¡ng giáº¥y phÃ©p lÃ¡i xe pháº£i cÃ³ xÃ¡c nháº­n sá»‘ km an toÃ n', '', 'news_1195201011.jpg', 'Ngo&agrave;i viá»‡c dá»± thi l&yacute; thuyáº¿t tr&ecirc;n m&aacute;y t&iacute;nh, thi thá»±c h&agrave;nh Ä‘á»ƒ tÄƒng háº¡ng báº±ng thiáº¿t bá»‹ cháº¥m Ä‘iá»ƒm tá»± Ä‘á»™ng, ngÆ°á»i dá»± s&aacute;t háº¡ch giáº¥y ph&eacute;p l&aacute;i xe c&ograve;n pháº£i c&oacute; giáº¥y x&aacute;c nháº­n sá»‘ km l&aacute;i xe an to&agrave;n.', '<p class="Normal">Bá»™ GTVT vá»«a ban h&agrave;nh 3 quyáº¿t Ä‘á»‹nh li&ecirc;n quan Ä‘áº¿n c&ocirc;ng t&aacute;c Ä‘&agrave;o táº¡o, s&aacute;t háº¡ch v&agrave; cáº¥p giáº¥y ph&eacute;p l&aacute;i xe nháº±m n&acirc;ng cao cháº¥t lÆ°á»£ng v&agrave; tÄƒng cÆ°á»ng an to&agrave;n giao th&ocirc;ng. Theo &ocirc;ng Nguyá»…n VÄƒn Quyá»n, Ph&oacute; cá»¥c trÆ°á»Ÿng ÄÆ°á»ng bá»™ Viá»‡t Nam, c&aacute;c quyáº¿t Ä‘á»‹nh má»›i Ä‘Æ°á»£c sá»­a Ä‘á»•i theo hÆ°á»›ng tÄƒng c&acirc;u há»i l&yacute; thuyáº¿t khi Ä‘&agrave;o táº¡o xe m&aacute;y v&agrave; chuáº©n ho&aacute;, n&acirc;ng cao cháº¥t lÆ°á»£ng táº¡i c&aacute;c cÆ¡ sá»Ÿ Ä‘&agrave;o táº¡o, s&aacute;t háº¡ch &ocirc;t&ocirc;.</p>\r\n<p class="Normal">Theo Ä‘&oacute;, ngÆ°á»i thi l&yacute; thuyáº¿t xe m&aacute;y sáº½ thá»±c hiá»‡n tráº¯c nghiá»‡m tr&ecirc;n giáº¥y hoáº·c tr&ecirc;n m&aacute;y vi t&iacute;nh. NgÆ°á»i s&aacute;t háº¡ch l&aacute;i &ocirc;t&ocirc; sáº½ thá»±c hiá»‡n táº¡i trung t&acirc;m c&oacute; gáº¯n thiáº¿t bá»‹ cháº¥m Ä‘iá»ƒm tá»± Ä‘á»™ng, kh&ocirc;ng c&oacute; ngÆ°á»i cháº¥m thi c&ugrave;ng tr&ecirc;n xe s&aacute;t háº¡ch. </p>\r\n<p class="Normal">B&ecirc;n cáº¡nh Ä‘&oacute;, má»™t quy Ä‘á»‹nh kh&aacute; ngáº·t ngh&egrave;o l&agrave; ngÆ°á»i Ä‘Æ°á»£c ph&eacute;p tham gia s&aacute;t háº¡ch l&aacute;i xe khi muá»‘n tÄƒng háº¡ng pháº£i c&oacute; giáº¥y x&aacute;c nháº­n thá»i gian l&aacute;i xe v&agrave; sá»‘ kilomet l&aacute;i xe an to&agrave;n theo giáº¥y ph&eacute;p do thá»§ trÆ°á»Ÿng cÆ¡ quan, tá»• chá»©c, Ä‘Æ¡n vá»‹ x&aacute;c nháº­n. TrÆ°á»ng há»£p l&aacute;i xe ch&iacute;nh l&agrave; chá»§ xe hoáº·c xe cá»§a há»™ gia Ä‘&igrave;nh th&igrave; pháº£i c&oacute; báº£n cam káº¿t báº£o Ä‘áº£m Ä‘á»§ thá»i gian h&agrave;nh nghá» v&agrave; sá»‘ kilomet l&aacute;i xe an to&agrave;n. </p>\r\n<p class="Normal">Cá»¥ thá»ƒ, náº¿u n&acirc;ng tá»« háº¡ng B1 l&ecirc;n B2 pháº£i c&oacute; thá»i gian l&aacute;i xe &iacute;t nháº¥t l&agrave; 1 nÄƒm v&agrave; c&oacute; 12.000 km l&aacute;i xe an to&agrave;n. Tá»« B2 l&ecirc;n háº¡ng C, tá»« háº¡ng C l&ecirc;n háº¡ng D, tá»« háº¡ng D l&ecirc;n háº¡ng E v&agrave; tá»« c&aacute;c háº¡ng GPLX l&ecirc;n háº¡ng F tÆ°Æ¡ng á»©ng pháº£i c&oacute; thá»i gian l&aacute;i xe &iacute;t nháº¥t 3 nÄƒm v&agrave; c&oacute; 50.000 km an to&agrave;n. Tá»« B2 l&ecirc;n háº¡ng D, tá»« háº¡ng C l&ecirc;n háº¡ng E pháº£i c&oacute; thá»i gian l&aacute;i xe &iacute;t nháº¥t Ä‘á»§ 5 nÄƒm v&agrave; c&oacute; 100.000 km l&aacute;i xe an to&agrave;n; N&acirc;ng l&ecirc;n c&aacute;c háº¡ng D, E pháº£i c&oacute; tr&igrave;nh Ä‘á»™ vÄƒn ho&aacute; tá»‘t nghiá»‡p tá»« Trung há»c cÆ¡ sá»Ÿ trá»Ÿ l&ecirc;n. </p>\r\n<p class="Normal">Ngo&agrave;i ra, ngÆ°á»i há»c thi láº¥y báº±ng l&aacute;i xe sáº½ Ä‘Æ°á»£c tÄƒng thá»i gian há»c l&yacute; thuyáº¿t v&agrave; thá»±c h&agrave;nh tá»« c&aacute;c háº¡ng B1 - F; tÄƒng sá»‘ tiáº¿t cá»§a bá»™ m&ocirc;n Äáº¡o Ä‘á»©c ngÆ°á»i l&aacute;i xe l&ecirc;n gáº¥p Ä‘&ocirc;i. </p>\r\n<table cellspacing="0" cellpadding="3" width="100%" bgcolor="#e2effc" border="1">\r\n    <tbody>\r\n        <tr>\r\n            <td class="Normal"><strong designtimesp="25547"><font color="#5f5f5f">Háº¡ng A1:</font></strong>&nbsp;Cho ph&eacute;p Ä‘iá»u khiá»ƒn xe m&ocirc;t&ocirc; 2 b&aacute;nh c&oacute; dung t&iacute;ch xi-lanh tá»« 50cc Ä‘áº¿n dÆ°á»›i 175cc.<br />\r\n            <strong designtimesp="25550"><font color="#5f5f5f">Háº¡ng A2:</font></strong> Cho ph&eacute;p Ä‘iá»u khiá»ƒn xe m&ocirc;t&ocirc; 2 b&aacute;nh n&oacute;i chung, kh&ocirc;ng giá»›i háº¡n dung t&iacute;ch xi-lanh.<br />\r\n            <strong designtimesp="25553"><font color="#5f5f5f">Háº¡ng A3:</font></strong> Cho ph&eacute;p Ä‘iá»u khiá»ƒn m&ocirc;t&ocirc; 3 b&aacute;nh, xe lam, x&iacute;ch l&ocirc; m&aacute;y v&agrave; c&aacute;c loáº¡i xe háº¡ng A1, kh&ocirc;ng &aacute;p dá»¥ng vá»›i phÆ°Æ¡ng tiá»‡n háº¡ng A2.<br />\r\n            <strong designtimesp="25556"><font color="#5f5f5f">Háº¡ng B1:</font></strong> D&ugrave;ng cho l&aacute;i xe kh&ocirc;ng chuy&ecirc;n nghiá»‡p, Ä‘Æ°á»£c quyá»n Ä‘iá»u khiá»ƒn: &Ocirc;t&ocirc; dÆ°á»›i 9 chá»—, ká»ƒ cáº£ ngÆ°á»i l&aacute;i; Xe táº£i, xe chuy&ecirc;n d&ugrave;ng c&oacute; trá»ng táº£i thiáº¿t káº¿ dÆ°á»›i 3.500 kg<br />\r\n            <strong designtimesp="25559"><font color="#5f5f5f">Háº¡ng B2:</font></strong> Cáº¥p cho l&aacute;i xe chuy&ecirc;n nghiá»‡p, quy Ä‘á»‹nh quyá»n Ä‘iá»u khiá»ƒn c&aacute;c phÆ°Æ¡ng tiá»‡n háº¡ng B1 v&agrave; c&aacute;c xe cáº©u b&aacute;nh lá»‘p c&oacute; sá»©c n&acirc;ng thiáº¿t káº¿ dÆ°á»›i 3.500 kg.<br />\r\n            <strong designtimesp="25562"><font color="#5f5f5f">Háº¡ng C:</font></strong> Cáº¥p cho l&aacute;i xe chuy&ecirc;n nghiá»‡p, quy Ä‘á»‹nh quyá»n Ä‘iá»u khiá»ƒn: &Ocirc;t&ocirc; táº£i v&agrave; xe chuy&ecirc;n d&ugrave;ng c&oacute; táº£i trá»ng thiáº¿t káº¿ tá»« 3.500 kg trá»Ÿ l&ecirc;n. Äáº§u k&eacute;o, m&aacute;y k&eacute;o 1 rÆ¡-mo&oacute;c hoáº·c sÆ¡-mi rÆ¡-mo&oacute;c c&oacute; táº£i trá»ng thiáº¿t káº¿ tá»« 3.500 kg trá»Ÿ l&ecirc;n. Cáº§n cáº©u b&aacute;nh lá»‘p c&oacute; sá»©c n&acirc;ng thiáº¿t káº¿ tá»« 3.500 kg trá»Ÿ l&ecirc;n.<br />\r\n            <strong designtimesp="25565"><font color="#5f5f5f">Háº¡ng D:</font></strong> Cáº¥p cho l&aacute;i xe chuy&ecirc;n nghiá»‡p, quy Ä‘á»‹nh quyá»n Ä‘iá»u khiá»ƒn: &Ocirc;t&ocirc; chá»Ÿ ngÆ°á»i tá»« 10-30 chá»—, t&iacute;nh cáº£ gháº¿ l&aacute;i; c&aacute;c loáº¡i xe quy Ä‘á»‹nh trong háº¡ng B1, B2, C.<br />\r\n            <strong designtimesp="25568"><font color="#5f5f5f">Háº¡ng E:</font></strong> Cáº¥p cho l&aacute;i xe chuy&ecirc;n nghiá»‡p, quy Ä‘á»‹nh quyá»n Ä‘iá»u khiá»ƒn: &Ocirc;t&ocirc; chá»Ÿ ngÆ°á»i tr&ecirc;n 30 chá»— ngá»“i, t&iacute;nh cáº£ gháº¿ l&aacute;i; C&aacute;c loáº¡i xe quy Ä‘á»‹nh trong háº¡ng B1, B2, C, D.<br />\r\n            <strong designtimesp="25571"><font color="#5f5f5f">Háº¡ng F:</font></strong> Cáº¥p cho ngÆ°á»i Ä‘&atilde; c&oacute; giáº¥y ph&eacute;p l&aacute;i xe háº¡ng B2, C, D, E Ä‘á»ƒ Ä‘iá»u khiá»ƒn c&aacute;c loáº¡i xe tÆ°Æ¡ng á»©ng c&oacute; k&eacute;o rÆ¡-mo&oacute;c trá»ng táº£i thiáº¿t káº¿ lá»›n hÆ¡n 750 kg.</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<p class="Normal" align="right"><strong designtimesp="25575">Äo&agrave;n Loan</strong></p>', 1195200389, 0, 1, 'thumb_news_1195201011.jpg', 'normal_news_1195201011.jpg', 7);
INSERT INTO `news` VALUES (10, 1, 'áº¢nh chiáº¿c Rolls-Royce Phantom Ä‘áº§u tiÃªn táº¡i Viá»‡t Nam', '', 'news_1195200512.jpg', '<p class="Lead">M&agrave;u s&aacute;ng báº¡c &aacute;nh kim, lÆ°á»›i táº£n nhiá»‡t phong c&aacute;ch Rolls-Royce v&agrave; ná»™i tháº¥t sang trá»ng l&agrave; nhá»¯ng g&igrave; l&agrave;m n&ecirc;n má»™t Phantom áº¥n tÆ°á»£ng.</p>', '<p class="Normal">Phantom l&agrave; d&ograve;ng xe duy nháº¥t hiá»‡n nay cá»§a Rolls-Royce, náº¿u kh&ocirc;ng ká»ƒ tá»›i phi&ecirc;n báº£n mui má»m Drophead Coup&eacute; m&agrave; h&atilde;ng xe Anh quá»‘c má»›i tr&igrave;nh l&agrave;ng. Phantom trong tiáº¿ng Anh l&agrave; &quot;b&oacute;ng ma&quot;, má»™t kiá»ƒu Ä‘áº·t t&ecirc;n huyá»n b&iacute; cá»§a Rolls-Royce.</p>\r\n<table cellspacing="7" cellpadding="7" width="201" align="right" border="0">\r\n    <tbody>\r\n        <tr>\r\n            <td><a class="" href="http://vnexpress.net/Vietnam/Oto-Xe-may/2007/06/3B9F77E6/">*''Si&ecirc;u pháº©m'' Rolls-Royce Phantom Drophead Coupe</a> </td>\r\n        </tr>\r\n        <tr>\r\n            <td><a class="" href="http://vnexpress.net/Vietnam/Oto-Xe-may/2007/02/3B9F2E4F/">*Rolls-Royce - l&atilde;nh Ä‘á»‹a cá»§a t&agrave; tháº§n v&agrave; &aacute;c quá»·</a></td>\r\n        </tr>\r\n        <tr>\r\n            <td><a class="" href="http://vnexpress.net/Vietnam/Oto-Xe-may/2006/11/3B9EF85B/">*Nhá»¯ng b&iacute; máº­t trong Ä‘áº¡i báº£n doanh Rolls-Royce</a></td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<p class="Normal">Trong tháº¿ giá»›i xe si&ecirc;u sang, Phantom Ä‘Æ°á»£c coi l&agrave; Ä‘á»‘i thá»§ cá»§a Maybach 57 v&agrave; Maybach 62. Tuy nhi&ecirc;n, x&eacute;t tr&ecirc;n kh&iacute;a cáº¡nh lá»‹ch sá»­, máº«u xe cá»§a Rolls-Royce váº«n Ä‘áº³ng cáº¥p hÆ¡n. C&aacute;i t&ecirc;n Phantom láº§n Ä‘áº§u ti&ecirc;n xuáº¥t hiá»‡n c&aacute;ch Ä‘&acirc;y Ä‘&atilde; hÆ¡n 80 nÄƒm trong khi 62 v&agrave; 57 cá»§a Maybach tá»“n táº¡i chÆ°a Ä‘áº§y má»™t tháº­p ká»·.</p>\r\n<p class="Normal">So vá» c&ocirc;ng nghá»‡, Maybach l&agrave; Ä‘á»‰nh cao, nhÆ°ng vá»›i nhá»¯ng ngÆ°á»i Æ°a sá»± thuáº§n khiáº¿t v&agrave; lá»‹ch l&atilde;m th&igrave; Rolls-Royce m&agrave; cá»¥ thá»ƒ l&agrave; Phantom kh&ocirc;ng c&oacute; Ä‘á»‘i thá»§. Ch&iacute;nh v&igrave; tháº¿ m&agrave; vá»›i chá»‰ má»™t sáº£n pháº©m, h&atilde;ng xe Anh quá»‘c váº«n Äƒn n&ecirc;n l&agrave;m ra trong nhiá»u nÄƒm nay. Tá»« 2003 Ä‘áº¿n th&aacute;ng 3/2007, tá»•ng sá»‘ c&oacute; 3.000 chiáº¿c Phantom b&aacute;n ra tr&ecirc;n to&agrave;n tháº¿ giá»›i.</p>\r\n<p class="Normal">Táº¡i nh&agrave; m&aacute;y cá»§a Rolls-Royce á»Ÿ Goodwood, Anh, vá» Phantom Ä‘Æ°a tá»« Äá»©c sang, nhÆ°ng c&ocirc;ng Ä‘oáº¡n sÆ¡n cáº§u ká»³ vá»›i 15 lá»›p Ä‘Æ°á»£c thá»±c hiá»‡n ho&agrave;n to&agrave;n táº¡i Ä‘&acirc;y. C&aacute;c nghá»‡ nh&acirc;n chá»‹u tr&aacute;ch nhiá»‡m sáº£n xuáº¥t khoáº£ng 56 bá»™ pháº­n báº±ng gá»— kh&aacute;c nhau vá»›i m&agrave;u sáº¯c, h&igrave;nh d&aacute;ng t&ugrave;y thuá»™c v&agrave;o tá»«ng y&ecirc;u cáº§u cá»§a kh&aacute;ch h&agrave;ng. </p>\r\n<p class="Normal">Gá»— sá»­ dá»¥ng tr&ecirc;n Phantom l&agrave; loáº¡i Walnut nháº­p tá»« California (Má»¹) hoáº·c Elm cá»§a Anh c&oacute; k&iacute;ch thÆ°á»›c khoáº£ng 10 m&eacute;t vu&ocirc;ng v&agrave; d&agrave;y 0,6 mm. Walnut l&agrave; gá»— cá»§a c&acirc;y &oacute;c ch&oacute; thÆ°á»ng Ä‘Æ°á»£c d&ugrave;ng Ä‘á»ƒ cháº¿ táº¡o máº·t Ä‘á»“ng há»“, c&ograve;n Elm l&agrave; gá»— c&acirc;y Ä‘u c&oacute; Ä‘á»™ bá»n cao v&agrave; chá»‘ng láº¡i tá»‘t nhá»¯ng va cháº¡m máº¡nh. Má»—i chiáº¿c Phantom Ä‘Æ°á»£c táº¡o n&ecirc;n tá»« khoáº£ng 400 bá»™ pháº­n da nháº­p tá»« Bavaria (Äá»©c) hay Argentina.</p>\r\n<table cellspacing="0" cellpadding="3" width="1" align="center" border="0">\r\n    <tbody>\r\n        <tr>\r\n            <td class="Image"><img height="262" alt="" width="380" border="1" src="http://vnexpress.net/Vietnam/Oto-Xe-may/2007/08/3B9F97F2/RR5.jpg" /></td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image">Náº¯p ca-p&ocirc; d&agrave;i v&agrave; lÆ°á»›i táº£n nhiá»‡t kh&ocirc;ng thá»ƒ láº«n cá»§a Rolls-Royce. Biá»ƒu tÆ°á»£ng &quot;Spirit of Ecstasy&quot; c&oacute; thá»ƒ thá»¥t v&agrave;o trong khi kh&ocirc;ng cáº§n thiáº¿t v&agrave; Ä‘á»ƒ tr&aacute;nh trá»™m. To&agrave;n bá»™ náº¯p ca-p&ocirc; Ä‘Æ°á»£c l&agrave;m tá»« nh&ocirc;m nháº¹ v&agrave; c&oacute; kháº£ nÄƒng chá»‘ng xÆ°á»›c. Phantom m&agrave; Ho&agrave;ng Trá»ng nháº­p vá» Ä‘Æ°á»£c sáº£n xuáº¥t nÄƒm 2006.</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image"><img height="251" alt="" width="380" border="1" src="http://vnexpress.net/Vietnam/Oto-Xe-may/2007/08/3B9F97F2/RR6.jpg" /></td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image">Phantom c&oacute; chiá»u d&agrave;i tá»•ng thá»ƒ 5.834 mm, ngáº¯n hÆ¡n Maybach 62 (d&agrave;i 6,2 m). </td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image"><img height="285" alt="" width="380" border="1" src="http://vnexpress.net/Vietnam/Oto-Xe-may/2007/08/3B9F97F2/RR4.jpg" /></td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image">Äá»™ng cÆ¡ 6,75 l&iacute;t V12 (12 xi-lanh xáº¿p h&igrave;nh chá»¯ V), c&ocirc;ng suáº¥t 453 m&atilde; lá»±c, m&ocirc;-men xoáº¯n 720 Nm. Sá»©c máº¡nh n&agrave;y gi&uacute;p chiáº¿c xe náº·ng gáº§n 2,5 táº¥n Ä‘áº¡t váº­n tá»‘c 100 km/h trong 5,7 gi&acirc;y.</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image"><img height="299" alt="" width="380" border="1" src="http://vnexpress.net/Vietnam/Oto-Xe-may/2007/08/3B9F97F2/RR8.jpg" /></td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image">Báº£ng Ä‘iá»u khiá»ƒn n&agrave;y cá»§a Phantom l&agrave; Ä‘áº·c trÆ°ng kh&ocirc;ng thá»ƒ láº«n cá»§a Rolls-Royce. C&aacute;c chi tiáº¿t tr&ecirc;n xe Ä‘Æ°á»£c á»‘p gá»— qu&yacute;. Ä&acirc;y l&agrave; nÆ¡i Rolls-Royce Ä‘á»ƒ kh&aacute;ch h&agrave;ng t&ugrave;y chá»n theo &yacute; th&iacute;ch cá»§a m&igrave;nh. C&aacute;c nghá»‡ nh&acirc;n c&oacute; thá»ƒ Ä‘&aacute;p á»©ng má»i y&ecirc;u cáº§u cho tá»«ng chi tiáº¿t nhá» nháº¥t.</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image"><img height="285" alt="" width="380" border="1" src="http://vnexpress.net/Vietnam/Oto-Xe-may/2007/08/3B9F97F2/RR2.jpg" /></td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image">NhÆ° c&aacute;c máº«u xe si&ecirc;u sang kh&aacute;c, h&agrave;ng gháº¿ sau l&agrave; nÆ¡i táº­p trung nhá»¯ng thiáº¿t bá»‹ tá»‘i t&acirc;n. Cá»­a sau trang bá»‹ r&egrave;m cho Ä‘iá»u khiá»ƒn Ä‘iá»‡n v&agrave; gáº¯n quáº¡t gi&oacute; lá»c m&ugrave;i. Hai bá»™ g&aacute; á»‘p gá»— sang trá»ng. Giá»¯a hai gháº¿ sau l&agrave; cá»¥m Ä‘iá»u khiá»ƒn Ä‘iá»u h&ograve;a, d&agrave;n &acirc;m thanh, chá»©c nÄƒng gháº¿ v&agrave; nhiá»u thá»© kh&aacute;c. Gháº¿ ngá»“i tr&ecirc;n Phantom á»Ÿ vá»‹ tr&iacute; cao Ä‘á»ƒ táº¡o sá»± tá»± tin v&agrave; thoáº£i m&aacute;i cho nhá»¯ng &ocirc;ng chá»§ cá»§a n&oacute;.</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image"><img height="285" alt="" width="380" border="1" src="http://vnexpress.net/Vietnam/Oto-Xe-may/2007/08/3B9F97F2/RR1.jpg" /></td>\r\n        </tr>\r\n        <tr>\r\n            <td class="Image">M&agrave;n h&igrave;nh áº©n sau gháº¿ trÆ°á»›c. Khi kh&ocirc;ng sá»­ dá»¥ng, n&oacute; náº±m gá»n v&agrave;o b&ecirc;n trong.</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<p class="Normal" align="right"><strong>Há»“ng Anh<br />\r\náº¢nh: </strong><em>H.T.</em></p>\r\n<em></em>&nbsp;', 1195200512, 0, 1, 'thumb_news_1195200512.jpg', 'normal_news_1195200512.jpg', 7);
INSERT INTO `news` VALUES (11, 3, 'TÃ¢n PhÃ³ thá»§ tÆ°á»›ng &#39;má»Ÿ hÃ ng&#39; phiÃªn cháº¥t váº¥n', '', 'news_1195200855.gif', 'Chiá»u nay, Ph&oacute; thá»§ tÆ°á»›ng ki&ecirc;m Bá»™ trÆ°á»Ÿng GD&amp;ÄT Nguyá»…n Thiá»‡n Nh&acirc;n sáº½ Ä‘Äƒng Ä‘&agrave;n tráº£ lá»i Quá»‘c há»™i vá» Ä‘á» &aacute;n há»c ph&iacute;, Ä‘á» &aacute;n Ä‘á»•i má»›i thi tuyá»ƒn sinh, cháº¥t lÆ°á»£ng thi THPT Ä‘á»£t 2 v&agrave; hiá»‡u quáº£ mua sáº¯m trang thiáº¿t bá»‹ dáº¡y há»c.<br />', '<p class="Normal">Trong sá»‘ hÆ¡n 230 c&acirc;u há»i cháº¥t váº¥n táº¡i ká»³ há»p n&agrave;y, &ocirc;ng Nh&acirc;n cÅ©ng l&agrave; má»™t trong nhá»¯ng th&agrave;nh vi&ecirc;n Ch&iacute;nh phá»§ nháº­n Ä‘Æ°á»£c nhiá»u quan t&acirc;m nháº¥t (gáº§n 20 c&acirc;u há»i). Phi&ecirc;n cháº¥t váº¥n trá»±c tiáº¿p sáº½ k&eacute;o d&agrave;i hÆ¡n 2 ng&agrave;y.</p>\r\n<p class="Normal">Trao Ä‘á»•i vá»›i <em>VnExpress</em>, &ocirc;ng Nguyá»…n Thiá»‡n Nh&acirc;n cho biáº¿t, th&aacute;ng 12 tá»›i Ä‘á» &aacute;n há»c ph&iacute; v&agrave; lÆ°Æ¡ng gi&aacute;o vi&ecirc;n sáº½ Ä‘Æ°á»£c tr&igrave;nh Bá»™ Ch&iacute;nh trá»‹. Táº¡i phi&ecirc;n cháº¥t váº¥n, &ocirc;ng muá»‘n tráº£ lá»i c&aacute;c Ä‘áº¡i biá»ƒu Ä‘áº·c Ä‘iá»ƒm chi ph&iacute; cá»§a gi&aacute;o dá»¥c, trong Ä‘&oacute; bao gá»“m t&iacute;nh hiá»‡u quáº£ cá»§a c&aacute;c dá»± &aacute;n gi&aacute;o dá»¥c.</p>\r\n<p class="Normal">Th&aacute;ng 11 nÄƒm ngo&aacute;i, trong láº§n Ä‘áº§u ti&ecirc;n Ä‘Äƒng Ä‘&agrave;n Quá»‘c há»™i tr&ecirc;n cÆ°Æ¡ng vá»‹ Bá»™ trÆ°á»Ÿng GD&amp;ÄT, &ocirc;ng Nh&acirc;n Ä‘&atilde; Ä‘Æ°á»£c nhiá»u Ä‘áº¡i biá»ƒu Ä‘&aacute;nh gi&aacute;o cao vá»›i nhá»¯ng quyáº¿t s&aacute;ch t&aacute;o báº¡o. </p>\r\n<p class="Normal"><font color="#0f0f0f"><strong>Ngay sau cháº¥t váº¥n Bá»™ trÆ°á»Ÿng GD&amp;ÄT, Bá»™ trÆ°á»Ÿng T&agrave;i ch&iacute;nh VÅ© VÄƒn Ninh</strong></font> sáº½ Ä‘Äƒng Ä‘&agrave;n giáº£i tr&igrave;nh vá» t&igrave;nh tráº¡ng gi&aacute; cáº£ tÄƒng, cháº­m tiáº¿n Ä‘á»™ giáº£i ng&acirc;n nguá»“n vá»‘n ng&acirc;n s&aacute;ch nh&agrave; nÆ°á»›c v&agrave; vá»‘n tr&aacute;i phiáº¿u Ch&iacute;nh phá»§. cÆ¡ sá»Ÿ giao chá»‰ ti&ecirc;u thu tiá»n chuyá»ƒn quyá»n sá»­ dá»¥ng Ä‘áº¥t cho c&aacute;c Ä‘á»‹a phÆ°Æ¡ng nÄƒm sau cao hÆ¡n nÄƒm trÆ°á»›c.</p>\r\n<p class="Normal">Tiáº¿p Ä‘&oacute;, Bá»™ trÆ°á»Ÿng C&ocirc;ng thÆ°Æ¡ng VÅ© Huy Ho&agrave;ng, Bá»™ trÆ°á»Ÿng N&ocirc;ng nghiá»‡p ph&aacute;t triá»ƒn N&ocirc;ng th&ocirc;n Cao Äá»©c Ph&aacute;t, Bá»™ trÆ°á»Ÿng Giao th&ocirc;ng Váº­n táº£i Há»“ NghÄ©a DÅ©ng, Bá»™ trÆ°á»Ÿng Y táº¿ Nguyá»…n Quá»‘c Triá»‡u v&agrave; Bá»™ trÆ°á»Ÿng Ná»™i vá»¥ Tráº§n VÄƒn Tuáº¥n sáº½ Ä‘Äƒng Ä‘&agrave;n. </p>\r\n<p class="Normal">Trao Ä‘á»•i vá»›i b&aacute;o ch&iacute; chiá»u 15/11, Ph&oacute; chá»§ tá»‹ch Quá»‘c há»™i Nguyá»…n Äá»©c Ki&ecirc;n cho biáº¿t, Thá»§ tÆ°á»›ng Nguyá»…n Táº¥n DÅ©ng sáº¯p c&oacute; chuyáº¿n c&ocirc;ng t&aacute;c nÆ°á»›c ngo&agrave;i, n&ecirc;n c&oacute; thá»ƒ sáº½ á»§y quyá»n Ph&oacute; thá»§ tÆ°á»›ng thÆ°á»ng trá»±c Nguyá»…n Sinh H&ugrave;ng &quot;chá»‘t&quot; láº¡i phi&ecirc;n cháº¥t váº¥n v&agrave;o ng&agrave;y 19/11. </p>\r\n<p class="Normal">Ä&acirc;y l&agrave; phi&ecirc;n cháº¥t váº¥n v&agrave; tráº£ lá»i cháº¥t váº¥n Ä‘áº§u ti&ecirc;n cá»§a Quá»‘c há»™i kho&aacute; 12, trong Ä‘&oacute; c&oacute; 3 bá»™ trÆ°á»Ÿng láº§n Ä‘áº§u ti&ecirc;n Ä‘Äƒng Ä‘&agrave;n tráº£ lá»i cháº¥t váº¥n QH l&agrave; &ocirc;ng Nguyá»…n Quá»‘c Triá»‡u, Tráº§n VÄƒn Tuáº¥n v&agrave; VÅ© Huy Ho&agrave;ng. </p>\r\n<p class="Normal" align="right"><strong>Viá»‡t Anh</strong> </p>', 1195200855, 0, 1, 'thumb_news_1195200855.gif', '', 7);
INSERT INTO `news` VALUES (13, 3, 'Nhiá»u nhÃ  nháº­p kháº©u Ã´ tÃ´ cÃ´ng bá»‘ má»©c giÃ¡ má»›i', '', 'news_1195270717.jpg', 'Ng&agrave;y 16/11, thá»i Ä‘iá»ƒm quyáº¿t Ä‘á»‹nh giáº£m thuáº¿ nháº­p kháº©u &ocirc; t&ocirc; xuá»‘ng 60% ch&iacute;nh thá»©c c&oacute; hiá»‡u lá»±c, má»™t sá»‘ c&ocirc;ng ty báº¯t Ä‘áº§u c&ocirc;ng bá»‘ gi&aacute; xe nháº­p kháº©u má»›i, trong khi má»™t sá»‘ kh&aacute;c kháº©n trÆ°Æ¡ng tiáº¿n h&agrave;nh thá»§ tá»¥c th&ocirc;ng quan cho nhá»¯ng xe nháº­p kháº©u Ä‘ang &ldquo;gÄƒm&rdquo; táº¡i cáº£ng.', '<span class="story_body" id="lbBody">&nbsp;\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt">&nbsp;</p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt"><span style="FONT-SIZE: 12pt"><font face="Times New Roman">Trao Ä‘á»•i vá»›i ph&oacute;ng&nbsp;vi&ecirc;n&nbsp;<em style="mso-bidi-font-style: normal">D&acirc;n tr&iacute;</em>, &ocirc;ng Nguyá»…n Trung Hiáº¿u, Gi&aacute;m Ä‘á»‘c kinh doanh cá»§a C&ocirc;ng ty Cá»• pháº§n Hyundai Viá»‡t Nam cho biáº¿t há» Ä‘&atilde; ch&iacute;nh thá»©c<span style="mso-spacerun: yes">&nbsp; </span>th&ocirc;ng b&aacute;o gi&aacute; b&aacute;n má»›i c&aacute;c d&ograve;ng xe Hyundai nháº­p kháº©u nguy&ecirc;n chiáº¿c má»›i 100%. <o:p></o:p></font></span></p>\r\n<p>&nbsp;</p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt"><span style="FONT-SIZE: 12pt"><o:p><font face="Times New Roman">&nbsp;</font></o:p></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt"><span style="FONT-SIZE: 12pt"><font face="Times New Roman">Cá»¥ thá»ƒ, xe Veracrus phi&ecirc;n báº£n m&aacute;y xÄƒng 3.8L V6 giáº£m 5.000 USD xuá»‘ng c&ograve;n 63.500 USD/chiáº¿c, phi&ecirc;n báº£n m&aacute;y dáº§u 3.0L giáº£m 3.000 USD xuá»‘ng c&ograve;n 69.500 USD/chiáº¿c. Xe <span style="mso-spacerun: yes">&nbsp;</span>Santa Fe 2.7L m&aacute;y xÄƒng giáº£m 1.500 USD xuá»‘ng 43.400 USD/chiáº¿c, m&aacute;y dáº§u 2.2L giáº£m 1.500 USD xuá»‘ng 45.400 USD. Ngo&agrave;i ra, xe Elantra m&aacute;y xÄƒng 1.6L gi&aacute; 30.500 USD, giáº£m 1.400 USD/xe.<o:p></o:p></font></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt"><span style="FONT-SIZE: 12pt"><o:p><font face="Times New Roman">&nbsp;</font></o:p></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt"><span style="FONT-SIZE: 12pt"><font face="Times New Roman">Má»™t nh&agrave; nháº­p kháº©u kh&aacute;c l&agrave; C&ocirc;ng ty Thi&ecirc;n Báº£o Anh cÅ©ng ch&iacute;nh thá»©c c&ocirc;ng bá»‘ giáº£m gi&aacute; táº¥t cáº£ c&aacute;c xe nháº­p kháº©u, vá»›i má»©c giáº£m tá»« 1.000-2.000 USD/xe.<o:p></o:p></font></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt"><span style="FONT-SIZE: 12pt"><o:p><font face="Times New Roman">&nbsp;</font></o:p></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt"><span style="FONT-SIZE: 12pt"><font face="Times New Roman">Giáº£i th&iacute;ch vá» viá»‡c b&acirc;y giá» má»›i c&ocirc;ng bá»‘ gi&aacute;, &ocirc;ng Nguyá»…n Ho&agrave;ng Anh, Gi&aacute;m Ä‘á»‘c C&ocirc;ng ty Thi&ecirc;n Báº£o Anh cho biáº¿t: &ldquo;Sau khi Bá»™ T&agrave;i ch&iacute;nh c&oacute; quyáº¿t Ä‘á»‹nh giáº£m thuáº¿ nháº­p kháº©u xe má»›i nguy&ecirc;n chiáº¿c, thá»‹ trÆ°á»ng gáº§n nhÆ° chá»¯ng láº¡i. Ráº¥t nhiá»u kh&aacute;ch h&agrave;ng c&oacute; nhu cáº§u mua xe chá»‰ Ä‘i tham kháº£o rá»“i chá» khi thuáº¿ má»›i c&oacute; hiá»‡u lá»±c má»›i mua. Ch&iacute;nh v&igrave; t&acirc;m l&yacute; n&agrave;y m&agrave; nhiá»u Ä‘Æ¡n vá»‹ Ä‘&atilde; nháº­p nhiá»u h&agrave;ng vá» nhÆ°ng chÆ°a th&ocirc;ng quan ngay. Thi&ecirc;n Báº£o Anh cÅ©ng c&oacute; khoáº£ng 30 xe Ä‘&atilde; vá» tá»›i Má»¹ Ä&igrave;nh, chá»‰ chá» thuáº¿ suáº¥t má»›i c&oacute; hiá»‡u lá»±c l&agrave; th&ocirc;ng quan&rdquo;. <o:p></o:p></font></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt"><span style="FONT-SIZE: 12pt"><o:p><font face="Times New Roman">&nbsp;</font></o:p></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt"><span style="FONT-SIZE: 12pt"><font face="Times New Roman">TrÆ°á»›c Ä‘&oacute;, nháº­p kháº©u v&agrave; ph&acirc;n phá»‘i thÆ°Æ¡ng hiá»‡u xe Porsche táº¡i Viá»‡t Nam l&agrave; C&ocirc;ng ty TNHH Xe hÆ¡i Thá»ƒ thao Uy t&iacute;n (PSC) v&agrave; nh&agrave; ph&acirc;n phá»‘i ch&iacute;nh thá»©c xe BMW l&agrave; C&ocirc;ng ty Cá»• pháº§n &Ocirc;t&ocirc; &Acirc;u Ch&acirc;u (Euro Auto) Ä‘&atilde; ch&iacute;nh thá»©c giáº£m gi&aacute; xe ngay tá»« thá»i Ä‘iá»ƒm Bá»™ T&agrave;i ch&iacute;nh quyáº¿t Ä‘á»‹nh giáº£m thuáº¿ nháº­p kháº©u Ä‘Æ°á»£c Ä‘Äƒng c&ocirc;ng b&aacute;o (ng&agrave;y 1/11) m&agrave; kh&ocirc;ng chá» Ä‘áº¿n khi thuáº¿ má»›i ch&iacute;nh thá»©c c&oacute; hiá»‡u lá»±c.<o:p></o:p></font></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt"><span style="FONT-SIZE: 12pt"><o:p><font face="Times New Roman">&nbsp;</font></o:p></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt"><span style="FONT-SIZE: 12pt"><font face="Times New Roman">Trong khi Ä‘&oacute;, nhiá»u máº«u xe sáº£n xuáº¥t trong nÆ°á»›c váº«n &ldquo;khan h&agrave;ng&rdquo; v&agrave; kh&ocirc;ng giáº£m gi&aacute; b&aacute;n.<o:p></o:p></font></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt"><span style="FONT-SIZE: 12pt"><o:p><font face="Times New Roman">&nbsp;</font></o:p></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; TEXT-ALIGN: right" align="right"><strong style="mso-bidi-font-weight: normal"><span style="FONT-SIZE: 12pt"><font face="Times New Roman">Máº¡nh H&ugrave;ng<o:p></o:p></font></span></strong></p>\r\n</span>', 1195270717, 0, 1, 'thumb_news_1195270717.jpg', 'normal_news_1195270717.jpg', 7);
INSERT INTO `news` VALUES (14, 3, 'Nhá»¯ng chiáº¿c xe Ä‘áº¯t nháº¥t tháº¿ giá»›i nÄƒm 2007', '', 'news_1195270858.jpg', '&nbsp;Báº¡n cho ráº±ng Bugatti Veyron gi&aacute; 1,4 triá»‡u USD l&agrave; xe &ocirc; t&ocirc; Ä‘áº¯t nháº¥t tháº¿ giá»›i? Ä&uacute;ng nhÆ° váº­y náº¿u chá»‰ Ä‘á» cáº­p Ä‘áº¿n xe má»›i, c&ograve;n náº¿u t&iacute;nh cáº£ xe cá»• th&igrave; si&ecirc;u xe 1001 m&atilde; lá»±c n&agrave;y c&ograve;n &ldquo;ráº»&rdquo; hÆ¡n chiáº¿c xe Ä‘áº¯t nháº¥t tháº¿ giá»›i hiá»‡n nay gáº§n 8 triá»‡u USD.&nbsp;Báº¡n cho ráº±ng Bugatti Veyron gi&aacute; 1,4 triá»‡u USD l&agrave; xe &ocirc; t&ocirc; Ä‘áº¯t nháº¥t tháº¿ giá»›i? Ä&uacute;ng nhÆ° váº­y náº¿u chá»‰ Ä‘á» cáº­p Ä‘áº¿n xe má»›i, c&ograve;n náº¿u t&iacute;nh cáº£ xe cá»• th&igrave; si&ecirc;u xe 1001 m&atilde; lá»±c n&agrave;y c&ograve;n &ldquo;ráº»&rdquo; hÆ¡n chiáº¿c xe Ä‘áº¯t nháº¥t tháº¿ giá»›i hiá»‡n nay gáº§n 8 triá»‡u USD.', '<span class="story_body" id="lbBody">&nbsp;\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><span style="COLOR: black; mso-bidi-font-weight: bold"><font size="3"><font face="Times New Roman">Táº¡p ch&iacute; <em style="mso-bidi-font-style: normal">Business Week</em> Ä‘&atilde; tá»•ng há»£p danh s&aacute;ch 15 chiáº¿c xe Ä‘áº¯t nháº¥t nÄƒm 2007, táº¥t cáº£ Ä‘á»u Ä‘Æ°á»£c b&aacute;n trong nÄƒm, vá»›i gi&aacute; tá»« 2,8 triá»‡u USD Ä‘áº¿n 9,3 triá»‡u USD.<o:p></o:p></font></font></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><strong><span style="COLOR: black; mso-font-kerning: 18.0pt"><o:p><font face="Times New Roman" size="3">&nbsp;</font></o:p></span></strong></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><strong><span style="COLOR: black; mso-font-kerning: 18.0pt"><font size="3"><font face="Times New Roman">1962 Ferrari 330 TRI/LM<o:p></o:p></font></font></span></strong></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><strong><span style="COLOR: black"><o:p><font face="Times New Roman" size="3">&nbsp;</font></o:p></span></strong></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><span style="COLOR: black; mso-bidi-font-weight: bold"><font face="Times New Roman" size="3">Gi&aacute; b&aacute;n:</font></span><font size="3"><font face="Times New Roman"><span style="COLOR: black"> 9,3 triá»‡u USD<br />\r\n<span style="mso-bidi-font-weight: bold">Ng&agrave;y b&aacute;n:</span> </span><st1:date year="2007" day="20" month="5"><span style="COLOR: black">20/5/2007</span></st1:date></font></font><span style="COLOR: black"><br />\r\n<font face="Times New Roman" size="3">Äá»‹a Ä‘iá»ƒm<span style="mso-bidi-font-weight: bold">:</span> </font></span><font size="3"><font face="Times New Roman"><st1:place><st1:city><span style="COLOR: black">Maranello</span></st1:city><span style="COLOR: black">, </span><st1:country-region><span style="COLOR: black">Italy</span></st1:country-region></st1:place><span style="COLOR: black"><o:p></o:p></span></font></font></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; TEXT-ALIGN: center; mso-layout-grid-align: none" align="center"><span style="COLOR: black"><o:p><font face="Times New Roman" size="3">&nbsp;\r\n<table align="middle">\r\n    <tbody>\r\n        <tr>\r\n            <td><img src="http://images4.dantri.com.vn/Uploaded/lanlt/Thang11-2007/1-161107.jpg" align="middle" border="0" alt="" /></td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n&nbsp;</font></o:p></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><font size="3"><font face="Times New Roman"><span style="COLOR: black">Chiáº¿c </span><strong><u><span style="COLOR: blue; mso-font-kerning: 18.0pt"><a href="http://www.dantri.com.vn/otoxemay/2007/5/180586.vip">Ferrari 330 TRI/LM</a></span></u><span style="COLOR: black; mso-font-kerning: 18.0pt"> </span></strong><span style="COLOR: black">trang bá»‹ Ä‘á»™ng cÆ¡ V12 n&agrave;y Ä‘&atilde; tá»«ng gi&agrave;nh chiáº¿n tháº¯ng trong giáº£i Ä‘ua Le Mans 1962 dÆ°á»›i sá»± Ä‘iá»u khiá»ƒn cá»§a tay l&aacute;i huyá»n thoáº¡i <span style="mso-bidi-font-weight: bold">Phil Hill v&agrave; Oliver Gendebien</span>. D&ugrave; kh&oacute; tin nhÆ°ng nh&agrave; Ä‘áº¥u gi&aacute; RM Auctions cho biáº¿t thá»±c táº¿ l&agrave; chiáº¿c xe Ä‘ua n&agrave;y Ä‘&atilde; Ä‘</span><span lang="VI" style="COLOR: black; mso-ansi-language: VI">Æ°á»£c d&ugrave;ng </span><span style="COLOR: black">Ä‘á»ƒ cháº¡y thÆ°á»ng ng&agrave;y tr&ecirc;n Ä‘Æ°á»ng phá»‘ </span><st1:state><st1:place><span style="COLOR: black">New York</span></st1:place></st1:state><span style="COLOR: black"> trong thá»i gian tá»« nÄƒm 1965 Ä‘áº¿n 1974. Th&ecirc;m má»™t th&ocirc;ng tin th&uacute; vá»‹ b&ecirc;n lá» ná»¯a l&agrave; ch&iacute;nh chiáº¿c xe n&agrave;y Ä‘&atilde; tá»«ng Ä‘Æ°á»£c ch&agrave;o b&aacute;n vá»›i gi&aacute; 8 triá»‡u USD v&agrave;o nÄƒm 2005 nhÆ°ng kh&ocirc;ng ai mua.<o:p></o:p></span></font></font></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><span style="COLOR: black"><o:p><font face="Times New Roman" size="3">&nbsp;</font></o:p></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><strong><span style="COLOR: black; mso-font-kerning: 18.0pt"><font size="3"><font face="Times New Roman">1953 Ferrari 340/375 MM<o:p></o:p></font></font></span></strong></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><strong><span style="COLOR: black"><o:p><font face="Times New Roman" size="3">&nbsp;</font></o:p></span></strong></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><span style="COLOR: black; mso-bidi-font-weight: bold"><font size="3"><font face="Times New Roman">Gi&aacute; b&aacute;n: 5,7 triá»‡u USD<o:p></o:p></font></font></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><font size="3"><font face="Times New Roman"><span style="COLOR: black; mso-bidi-font-weight: bold">Ng&agrave;y b&aacute;n: </span><st1:date year="2007" day="20" month="5"><span style="COLOR: black; mso-bidi-font-weight: bold">20/5/2007</span></st1:date><span style="COLOR: black; mso-bidi-font-weight: bold"><o:p></o:p></span></font></font></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><font size="3"><font face="Times New Roman"><span style="COLOR: black; mso-bidi-font-weight: bold">Äá»‹a Ä‘iá»ƒm: </span><st1:place><st1:city><span style="COLOR: black">Maranello</span></st1:city><span style="COLOR: black">, </span><st1:country-region><span style="COLOR: black">Italy</span></st1:country-region></st1:place><span style="COLOR: black; mso-bidi-font-weight: bold"><o:p></o:p></span></font></font></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; TEXT-ALIGN: center; mso-layout-grid-align: none" align="center"><span style="COLOR: black"><o:p><font face="Times New Roman" size="3">&nbsp;\r\n<table align="middle">\r\n    <tbody>\r\n        <tr>\r\n            <td><img src="http://images4.dantri.com.vn/Uploaded/lanlt/Thang11-2007/2-161107.jpg" align="middle" border="0" alt="" /></td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n&nbsp;</font></o:p></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><span style="COLOR: black"><font size="3"><font face="Times New Roman">Xe Ä‘ua Ferrari Ä‘á»™ng cÆ¡ V15 dung t&iacute;ch 5.0L n&agrave;y thá»±c sá»± l&agrave; &ldquo;cá»§a hiáº¿m&rdquo; v&igrave; n&oacute; l&agrave; má»™t trong hai chiáº¿c duy nháº¥t loáº¡i n&agrave;y c&ograve;n tá»“n táº¡i cho Ä‘áº¿n ng&agrave;y nay. HÆ¡n ná»¯a, cÅ©ng chá»‰ c&oacute; 3 chiáº¿c nhÆ° tháº¿ Ä‘Æ°á»£c sáº£n xuáº¥t. <o:p></o:p></font></font></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><span style="COLOR: black"><o:p><font face="Times New Roman" size="3">&nbsp;</font></o:p></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><font size="3"><font face="Times New Roman"><strong><span style="COLOR: black; mso-font-kerning: 18.0pt">1966 </span></strong><st1:city><st1:place><strong><span style="COLOR: black; mso-font-kerning: 18.0pt">Shelby</span></strong></st1:place></st1:city><strong><span style="COLOR: black; mso-font-kerning: 18.0pt"> Cobra<o:p></o:p></span></strong></font></font></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><strong><span style="COLOR: black"><o:p><font face="Times New Roman" size="3">&nbsp;</font></o:p></span></strong></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><span style="COLOR: black; mso-bidi-font-weight: bold"><font size="3"><font face="Times New Roman">Gi&aacute; b&aacute;n: 5,5 triá»‡u USD<o:p></o:p></font></font></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><font size="3"><font face="Times New Roman"><span style="COLOR: black; mso-bidi-font-weight: bold">Ng&agrave;y b&aacute;n: </span><st1:date year="2007" day="18" month="1"><span style="COLOR: black; mso-bidi-font-weight: bold">18/1/2007</span></st1:date><span style="COLOR: black; mso-bidi-font-weight: bold"><o:p></o:p></span></font></font></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><font size="3"><font face="Times New Roman"><span style="COLOR: black; mso-bidi-font-weight: bold">Äá»‹a Ä‘iá»ƒm: </span><st1:state><st1:place><span style="COLOR: black">Arizona</span></st1:place></st1:state><span style="COLOR: black">, Má»¹<span style="mso-bidi-font-weight: bold"><o:p></o:p></span></span></font></font></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; TEXT-ALIGN: center; mso-layout-grid-align: none" align="center"><span style="COLOR: black"><o:p><font face="Times New Roman" size="3">&nbsp;\r\n<table align="middle">\r\n    <tbody>\r\n        <tr>\r\n            <td><img src="http://images4.dantri.com.vn/Uploaded/lanlt/Thang11-2007/3-161107.jpg" align="middle" border="0" alt="" /></td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n&nbsp;</font></o:p></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><span style="COLOR: black"><font size="3"><font face="Times New Roman">Chiáº¿c Shelby Cobra n&agrave;y láº¯p Ä‘á»™ng cÆ¡ V8 tÄƒng &aacute;p c&ocirc;ng suáº¥t 800 m&atilde; lá»±c, ban Ä‘áº§u Ä‘Æ°á»£c thiáº¿t káº¿ Ä‘á»ƒ l&agrave;m xe Ä‘ua, nhÆ°ng sau Ä‘&oacute; c&oacute; má»™t sá»‘ thay Ä‘á»•i Ä‘á»ƒ c&oacute; thá»ƒ cháº¡y há»£p ph&aacute;p tr&ecirc;n Ä‘Æ°á»ng phá»‘. &ldquo;Huyá»n thoáº¡i&rdquo; Carroll Shelby Ä‘&atilde; tá»«ng sá»­ dá»¥ng chiáº¿c xe n&agrave;y.<o:p></o:p></font></font></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><span style="COLOR: black"><o:p><font face="Times New Roman" size="3">&nbsp;</font></o:p></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><strong><span style="COLOR: black; mso-font-kerning: 18.0pt"><font size="3"><font face="Times New Roman">1959 Ferrari 250 GT<o:p></o:p></font></font></span></strong></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><strong><span style="COLOR: black; mso-font-kerning: 18.0pt"><o:p><font face="Times New Roman" size="3">&nbsp;</font></o:p></span></strong></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><span style="COLOR: black; mso-bidi-font-weight: bold"><font size="3"><font face="Times New Roman">Gi&aacute; b&aacute;n: 4,94 triá»‡u USD<o:p></o:p></font></font></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><font size="3"><font face="Times New Roman"><span style="COLOR: black; mso-bidi-font-weight: bold">Ng&agrave;y b&aacute;n: </span><st1:date year="2007" day="17" month="8"><span style="COLOR: black; mso-bidi-font-weight: bold">17/8/2007</span></st1:date><span style="COLOR: black; mso-bidi-font-weight: bold"><o:p></o:p></span></font></font></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><font size="3"><font face="Times New Roman"><span style="COLOR: black; mso-bidi-font-weight: bold">Äá»‹a Ä‘iá»ƒm: </span><st1:state><st1:place><span style="COLOR: black; mso-bidi-font-weight: bold">California</span></st1:place></st1:state><span style="COLOR: black; mso-bidi-font-weight: bold">, Má»¹<o:p></o:p></span></font></font></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; TEXT-ALIGN: center; mso-layout-grid-align: none" align="center"><span style="COLOR: black"><o:p><font face="Times New Roman" size="3">&nbsp;\r\n<table align="middle">\r\n    <tbody>\r\n        <tr>\r\n            <td><img src="http://images4.dantri.com.vn/Uploaded/lanlt/Thang11-2007/4-161107.jpg" align="middle" border="0" alt="" /></td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n&nbsp;</font></o:p></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><font size="3"><font face="Times New Roman"><span style="COLOR: black">Lá»‹ch sá»­ lÄƒn b&aacute;nh tr&ecirc;n c&aacute;c giáº£i Ä‘ua lá»›n khiáº¿n chiáº¿c xe n&agrave;y c&oacute; gi&aacute; trá»‹ hÆ¡n nhiá»u so vá»›i c&aacute;c xe tÆ°Æ¡ng tá»± kh&aacute;c. Ná»•i tiáº¿ng nháº¥t c&oacute; láº½ pháº£i ká»ƒ Ä‘áº¿n giáº£i </span><st1:city><st1:place><span style="COLOR: black">Le Mans</span></st1:place></st1:city><span style="COLOR: black">, vá»›i tay Ä‘ua lá»«ng danh Bob Grossman ngÆ°á»i Má»¹.<o:p></o:p></span></font></font></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><span style="COLOR: black"><o:p><font face="Times New Roman" size="3">&nbsp;</font></o:p></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><strong><span style="COLOR: black; mso-font-kerning: 18.0pt"><font size="3"><font face="Times New Roman">1931 Bentley 4-Liter<o:p></o:p></font></font></span></strong></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><strong><span style="COLOR: black"><o:p><font face="Times New Roman" size="3">&nbsp;</font></o:p></span></strong></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><span style="COLOR: black; mso-bidi-font-weight: bold"><font size="3"><font face="Times New Roman">Gi&aacute; b&aacute;n: 4,51 triá»‡u USD<o:p></o:p></font></font></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><font size="3"><font face="Times New Roman"><span style="COLOR: black; mso-bidi-font-weight: bold">Ng&agrave;y b&aacute;n: </span><st1:date year="2007" day="18" month="8"><span style="COLOR: black; mso-bidi-font-weight: bold">18/8/2007</span></st1:date><span style="COLOR: black; mso-bidi-font-weight: bold"><o:p></o:p></span></font></font></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><font size="3"><font face="Times New Roman"><span style="COLOR: black; mso-bidi-font-weight: bold">Äá»‹a Ä‘iá»ƒm:</span><span style="COLOR: black"> </span><st1:place><st1:city><span style="COLOR: black">Pebble Beach</span></st1:city><span style="COLOR: black">, </span><st1:state><span style="COLOR: black">Calif.</span></st1:state></st1:place><span style="COLOR: black">, Má»¹<o:p></o:p></span></font></font></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; TEXT-ALIGN: center; mso-layout-grid-align: none" align="center"><span style="COLOR: black"><o:p><font face="Times New Roman" size="3">\r\n<table align="middle">\r\n    <tbody>\r\n        <tr>\r\n            <td><img src="http://images4.dantri.com.vn/Uploaded/lanlt/Thang11-2007/5-161107.jpg" align="middle" border="0" alt="" /></td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n&nbsp;</font></o:p></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><span style="COLOR: black"><font size="3"><font face="Times New Roman">Ä&acirc;y l&agrave; má»™t trong&nbsp;43 chiáº¿c c&ograve;n tá»“n táº¡i&nbsp;hiá»‡n nay.&nbsp;N&oacute; Ä‘Æ°á»£c b&aacute;n vá»›i gi&aacute;&nbsp;hÆ¡n&nbsp;4,5 triá»‡u&nbsp;USD v&agrave;o m&ugrave;a h&egrave; nÄƒm nay d&ugrave;&nbsp;chuy&ecirc;n trang <em>Sports Car Market</em> Ä‘&atilde; m&ocirc; táº£ nhÆ° sau:&nbsp;&quot;Ä‘á»™ng cÆ¡&nbsp;bá»‹ thá»§ng, gháº¿ r&aacute;ch, sÆ¡n báº¡c m&agrave;u,&nbsp;v&agrave; mui xe&nbsp;á»p&nbsp;áº¹p.&quot; Tuy nhi&ecirc;n, c&oacute; láº½ ch&iacute;nh n&eacute;t nguy&ecirc;n báº£n Ä‘&oacute; khiáº¿n chiáº¿c xe&nbsp;Ä‘Æ°á»£c sÄƒn Ä‘&oacute;n hÆ¡n l&agrave; khi Ä‘&atilde; Ä‘Æ°á»£c phá»¥c cháº¿&nbsp;Ä‘áº¿n &quot;b&oacute;ng mÆ°á»£t&quot;.<span style="mso-bidi-font-weight: bold"><o:p></o:p></span></font></font></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><span style="COLOR: black"><o:p><font face="Times New Roman" size="3">&nbsp;</font></o:p></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><strong><span style="COLOR: black; mso-font-kerning: 18.0pt"><font size="3"><font face="Times New Roman">1959 Ferrari 250 GT<o:p></o:p></font></font></span></strong></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><strong><span style="COLOR: black"><o:p><font face="Times New Roman" size="3">&nbsp;</font></o:p></span></strong></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><font size="3"><font face="Times New Roman"><span style="COLOR: black; mso-bidi-font-weight: bold">Gi&aacute; b&aacute;n: </span><span style="COLOR: black">4.455.000 USD<span style="mso-bidi-font-weight: bold"><o:p></o:p></span></span></font></font></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><font size="3"><font face="Times New Roman"><span style="COLOR: black; mso-bidi-font-weight: bold">Ng&agrave;y b&aacute;n: </span><st1:date year="2007" day="18" month="8"><span style="COLOR: black; mso-bidi-font-weight: bold">18/8/2007</span></st1:date><span style="COLOR: black; mso-bidi-font-weight: bold"><o:p></o:p></span></font></font></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><font size="3"><font face="Times New Roman"><span style="COLOR: black; mso-bidi-font-weight: bold">Äá»‹a Ä‘iá»ƒm: </span><st1:place><st1:city><span style="COLOR: black">Pebble Beach</span></st1:city><span style="COLOR: black">, </span><st1:state><span style="COLOR: black">Calif.</span></st1:state></st1:place><span style="COLOR: black">, Má»¹<span style="mso-bidi-font-weight: bold"><o:p></o:p></span></span></font></font></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; TEXT-ALIGN: center; mso-layout-grid-align: none" align="center"><strong><span style="COLOR: black"><o:p><font face="Times New Roman" size="3">&nbsp;\r\n<table align="middle">\r\n    <tbody>\r\n        <tr>\r\n            <td><img src="http://images4.dantri.com.vn/Uploaded/lanlt/Thang11-2007/6-161107.jpg" align="middle" border="0" alt="" /></td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n&nbsp;</font></o:p></span></strong></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><span style="COLOR: black"><font size="3"><font face="Times New Roman">Nhá» thá»i gian 3 nÄƒm phá»¥c cháº¿, chiáº¿c Ferrari Ä‘á»™ng cÆ¡ V12 m&agrave;u Ä‘en n&agrave;y giá» Ä‘&acirc;y tr&ocirc;ng &ldquo;ho&agrave;n háº£o&rdquo; Ä‘áº¿n tá»«ng Ä‘Æ°á»ng n&eacute;t. Chá»‰ c&oacute; Ä‘iá»u n&oacute; kh&ocirc;ng mang má»™t lá»‹ch sá»­ Ä‘ua xe n&agrave;o.<o:p></o:p></font></font></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><span style="COLOR: black"><o:p><font face="Times New Roman" size="3">&nbsp;</font></o:p></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><strong><span style="COLOR: black; mso-font-kerning: 18.0pt"><font size="3"><font face="Times New Roman">1935 Duesenberg SJ<o:p></o:p></font></font></span></strong></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><strong><span style="COLOR: black"><o:p><font face="Times New Roman" size="3">&nbsp;</font></o:p></span></strong></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><span style="COLOR: black; mso-bidi-font-weight: bold"><font size="3"><font face="Times New Roman">Gi&aacute; b&aacute;n: 4,4 triá»‡u USD<o:p></o:p></font></font></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><font size="3"><font face="Times New Roman"><span style="COLOR: black; mso-bidi-font-weight: bold">Ng&agrave;y b&aacute;n: </span><st1:date year="2007" day="17" month="8"><span style="COLOR: black; mso-bidi-font-weight: bold">17/8/2007</span></st1:date><span style="COLOR: black; mso-bidi-font-weight: bold"><o:p></o:p></span></font></font></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><font size="3"><font face="Times New Roman"><span style="COLOR: black; mso-bidi-font-weight: bold">Äá»‹a Ä‘iá»ƒm: </span><st1:state><st1:place><span style="COLOR: black">California</span></st1:place></st1:state><span style="COLOR: black">, Má»¹<span style="mso-bidi-font-weight: bold"><o:p></o:p></span></span></font></font></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; TEXT-ALIGN: center; mso-layout-grid-align: none" align="center"><strong><span style="COLOR: black"><o:p><font face="Times New Roman" size="3">&nbsp;\r\n<table align="middle">\r\n    <tbody>\r\n        <tr>\r\n            <td><img src="http://images4.dantri.com.vn/Uploaded/lanlt/Thang11-2007/7-161107.jpg" align="middle" border="0" alt="" /></td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n&nbsp;</font></o:p></span></strong></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><span style="COLOR: black"><font size="3"><font face="Times New Roman">Chiáº¿c Duesenberg n&agrave;y Ä‘Æ°á»£c giá»›i thiá»‡u l&agrave; thiáº¿t káº¿ d&agrave;nh cho ná»¯ minh tinh Mae West, biá»ƒu tÆ°á»£ng sex cá»§a nÆ°á»›c Má»¹ há»“i tháº­p ni&ecirc;n 30, nhÆ°ng c&ocirc; chÆ°a bao giá» nháº­n chiáº¿c xe. Nh&agrave; Ä‘áº¥u gi&aacute; RM Auctions cho biáº¿t Ä‘&acirc;y l&agrave; má»™t trong 36 chiáº¿c duy nháº¥t Ä‘Æ°á»£c sáº£n xuáº¥t.<o:p></o:p></font></font></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><span style="COLOR: black"><o:p><font face="Times New Roman" size="3">&nbsp;</font></o:p></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><strong><span style="COLOR: black; mso-font-kerning: 18.0pt"><font size="3"><font face="Times New Roman">1933 Delage D8S<o:p></o:p></font></font></span></strong></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><strong><span style="COLOR: black"><o:p><font face="Times New Roman" size="3">&nbsp;</font></o:p></span></strong></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><font size="3"><font face="Times New Roman"><span style="COLOR: black; mso-bidi-font-weight: bold">Gi&aacute; b&aacute;n: </span><span style="COLOR: black">3,74 triá»‡u USD<span style="mso-bidi-font-weight: bold"><o:p></o:p></span></span></font></font></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><font size="3"><font face="Times New Roman"><span style="COLOR: black; mso-bidi-font-weight: bold">Ng&agrave;y b&aacute;n: </span><st1:date year="2007" day="17" month="8"><span style="COLOR: black; mso-bidi-font-weight: bold">17/8/2007</span></st1:date><span style="COLOR: black; mso-bidi-font-weight: bold"><o:p></o:p></span></font></font></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><font size="3"><font face="Times New Roman"><span style="COLOR: black; mso-bidi-font-weight: bold">Äá»‹a Ä‘iá»ƒm: </span><st1:state><st1:place><span style="COLOR: black">California</span></st1:place></st1:state><span style="COLOR: black">, Má»¹<span style="mso-bidi-font-weight: bold"><o:p></o:p></span></span></font></font></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; TEXT-ALIGN: center; mso-layout-grid-align: none" align="center"><strong><span style="COLOR: black"><o:p><font face="Times New Roman" size="3">&nbsp;\r\n<table align="middle">\r\n    <tbody>\r\n        <tr>\r\n            <td><img src="http://images4.dantri.com.vn/Uploaded/lanlt/Thang11-2007/8-161107.jpg" align="middle" border="0" alt="" /></td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n&nbsp;</font></o:p></span></strong></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><font size="3"><font face="Times New Roman"><span style="COLOR: black">Chiáº¿c xe Ph&aacute;p n&agrave;y Ä‘Æ°á»£c giá»›i thiá»‡u táº¡i Triá»ƒn l&atilde;m &ocirc; t&ocirc; </span><st1:city><st1:place><span style="COLOR: black">Paris</span></st1:place></st1:city><span style="COLOR: black"> v&agrave;o nÄƒm 1933. Chiáº¿c xe c&oacute; kháº£ nÄƒng tÄƒng tá»‘c tá»« 0 l&ecirc;n 100km/h trong khoáº£ng 15 gi&acirc;y, nhanh hÆ¡n má»™t chiáº¿c Bentley Ä‘á»™ng cÆ¡ tÄƒng &aacute;p c&ugrave;ng thá»i.</span></font></font></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><font size="3"><font face="Times New Roman"><span style="COLOR: black"></span></font></font>&nbsp;</p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; mso-layout-grid-align: none"><font size="3"><font face="Times New Roman"><span style="COLOR: black"><em>(C&ograve;n tiáº¿p)</em></span></font></font><span style="COLOR: black"><o:p><font face="Times New Roman" size="3">&nbsp;</font></o:p></span></p>\r\n<p class="MsoNormal" style="MARGIN: 0in 0in 0pt; TEXT-ALIGN: right; mso-layout-grid-align: none" align="right"><strong style="mso-bidi-font-weight: normal"><span style="COLOR: black"><font size="3"><font face="Times New Roman">Sá»¹ Ho&agrave;ng<o:p></o:p></font></font></span></strong></p>\r\n</span>', 1195270858, 0, 1, 'thumb_news_1195270858.jpg', 'normal_news_1195270858.jpg', 7);

-- --------------------------------------------------------

-- 
-- Table structure for table `orders`
-- 

CREATE TABLE `orders` (
  `id_order` bigint(20) unsigned NOT NULL auto_increment,
  `session` tinytext NOT NULL,
  `time` bigint(20) unsigned NOT NULL default '0',
  `name` varchar(16) NOT NULL default '',
  `tel` varchar(16) NOT NULL default '',
  `email` varchar(16) NOT NULL default '',
  `address` tinytext NOT NULL,
  `addinfo` text,
  PRIMARY KEY  (`id_order`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

-- 
-- Dumping data for table `orders`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `photo`
-- 

CREATE TABLE `photo` (
  `id_photo` bigint(20) unsigned NOT NULL auto_increment,
  `id_catpt` bigint(20) unsigned NOT NULL default '0',
  `name` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `intro` mediumtext character set latin1 collate latin1_general_ci NOT NULL,
  `ngay_dang` bigint(20) unsigned NOT NULL default '0',
  `thu_tu` bigint(20) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '1',
  `small_image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `normal_image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `id_user` bigint(20) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id_photo`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=46 ;

-- 
-- Dumping data for table `photo`
-- 

INSERT INTO `photo` VALUES (1, 1, 'Rolls-royce phantom', 'photo_1195202558.jpg', '', 1195026578, 0, 1, 'thumb_photo_1195202558.jpg', 'normal_photo_1195202558.jpg', 7);
INSERT INTO `photo` VALUES (2, 1, 'Rolls-royce', 'photo_1195202534.jpg', '', 1195026730, 0, 1, 'thumb_photo_1195202534.jpg', 'normal_photo_1195202534.jpg', 7);
INSERT INTO `photo` VALUES (3, 1, 'Rolls-royce phantom', 'photo_1195202525.jpg', '', 1195026752, 0, 1, 'thumb_photo_1195202525.jpg', 'normal_photo_1195202525.jpg', 7);
INSERT INTO `photo` VALUES (4, 2, '01', 'photo_1195089317.jpg', '', 1195089318, 0, 1, 'thumb_photo_1195089317.jpeg', 'normal_photo_1195089317.jpeg', 7);
INSERT INTO `photo` VALUES (5, 2, '02', 'photo_1195089324.jpg', '', 1195089325, 0, 1, 'thumb_photo_1195089324.jpeg', 'normal_photo_1195089324.jpeg', 7);
INSERT INTO `photo` VALUES (6, 2, '03', 'photo_1195089331.jpg', '', 1195089331, 0, 1, 'thumb_photo_1195089331.jpeg', 'normal_photo_1195089331.jpeg', 7);
INSERT INTO `photo` VALUES (7, 2, '04', 'photo_1195089339.jpg', '', 1195089340, 0, 1, 'thumb_photo_1195089339.jpeg', 'normal_photo_1195089339.jpeg', 7);
INSERT INTO `photo` VALUES (8, 2, '05', 'photo_1195089347.jpg', '', 1195089347, 0, 1, 'thumb_photo_1195089347.jpeg', 'normal_photo_1195089347.jpeg', 7);
INSERT INTO `photo` VALUES (9, 2, '06', 'photo_1195089354.jpg', '', 1195089354, 0, 1, 'thumb_photo_1195089354.jpeg', 'normal_photo_1195089354.jpeg', 7);
INSERT INTO `photo` VALUES (10, 2, '07', 'photo_1195089364.jpg', '', 1195089364, 0, 1, 'thumb_photo_1195089364.jpeg', 'normal_photo_1195089364.jpeg', 7);
INSERT INTO `photo` VALUES (11, 2, '08', 'photo_1195089371.jpg', '', 1195089371, 0, 1, 'thumb_photo_1195089371.jpeg', 'normal_photo_1195089371.jpeg', 7);
INSERT INTO `photo` VALUES (12, 2, '09', 'photo_1195089378.jpg', '', 1195089378, 0, 1, 'thumb_photo_1195089378.jpeg', 'normal_photo_1195089378.jpeg', 7);
INSERT INTO `photo` VALUES (13, 2, '10', 'photo_1195089831.jpg', '', 1195089567, 0, 1, 'thumb_photo_1195089831.jpeg', 'normal_photo_1195089831.jpeg', 7);
INSERT INTO `photo` VALUES (14, 2, '11', 'photo_1195089824.jpg', '', 1195089574, 0, 1, 'thumb_photo_1195089824.jpeg', 'normal_photo_1195089824.jpeg', 7);
INSERT INTO `photo` VALUES (15, 2, '12', 'photo_1195089818.jpg', '', 1195089583, 0, 1, 'thumb_photo_1195089818.jpeg', 'normal_photo_1195089818.jpeg', 7);
INSERT INTO `photo` VALUES (16, 2, '13', 'photo_1195089811.jpg', '', 1195089588, 0, 1, 'thumb_photo_1195089811.jpeg', 'normal_photo_1195089811.jpeg', 7);
INSERT INTO `photo` VALUES (17, 2, '14', 'photo_1195089805.jpg', '', 1195089594, 0, 1, 'thumb_photo_1195089805.jpeg', 'normal_photo_1195089805.jpeg', 7);
INSERT INTO `photo` VALUES (18, 2, '15', 'photo_1195089798.jpg', '', 1195089598, 0, 1, 'thumb_photo_1195089798.jpeg', 'normal_photo_1195089798.jpeg', 7);
INSERT INTO `photo` VALUES (19, 2, '16', 'photo_1195089790.jpg', '', 1195089607, 0, 1, 'thumb_photo_1195089790.jpeg', 'normal_photo_1195089790.jpeg', 7);
INSERT INTO `photo` VALUES (25, 2, '22', 'photo_1195090270.jpg', '', 1195090270, 0, 1, 'thumb_photo_1195090270.jpeg', 'normal_photo_1195090270.jpeg', 7);
INSERT INTO `photo` VALUES (21, 2, '18', 'photo_1195089782.jpg', '', 1195089622, 0, 1, 'thumb_photo_1195089782.jpeg', 'normal_photo_1195089782.jpeg', 7);
INSERT INTO `photo` VALUES (22, 2, '19', 'photo_1195089774.jpg', '', 1195089629, 0, 1, 'thumb_photo_1195089774.jpeg', 'normal_photo_1195089774.jpeg', 7);
INSERT INTO `photo` VALUES (23, 2, '20', 'photo_1195089765.jpg', '', 1195089637, 0, 1, 'thumb_photo_1195089765.jpeg', 'normal_photo_1195089765.jpeg', 7);
INSERT INTO `photo` VALUES (24, 2, '21', 'photo_1195089750.jpg', '', 1195089643, 0, 1, 'thumb_photo_1195089750.jpeg', 'normal_photo_1195089750.jpeg', 7);
INSERT INTO `photo` VALUES (26, 2, '23', 'photo_1195090277.jpg', '', 1195090278, 0, 1, 'thumb_photo_1195090277.jpeg', 'normal_photo_1195090277.jpeg', 7);
INSERT INTO `photo` VALUES (27, 2, '24', 'photo_1195090282.jpg', '', 1195090283, 0, 1, 'thumb_photo_1195090282.jpeg', 'normal_photo_1195090282.jpeg', 7);
INSERT INTO `photo` VALUES (28, 2, '25', 'photo_1195090288.jpg', '', 1195090288, 0, 1, 'thumb_photo_1195090288.jpeg', 'normal_photo_1195090288.jpeg', 7);
INSERT INTO `photo` VALUES (29, 2, '26', 'photo_1195090294.jpg', '', 1195090295, 0, 1, 'thumb_photo_1195090294.jpeg', 'normal_photo_1195090294.jpeg', 7);
INSERT INTO `photo` VALUES (30, 2, '27', 'photo_1195090301.jpg', '', 1195090301, 0, 1, 'thumb_photo_1195090301.jpeg', 'normal_photo_1195090301.jpeg', 7);
INSERT INTO `photo` VALUES (31, 2, '28', 'photo_1195090310.jpg', '', 1195090311, 0, 1, 'thumb_photo_1195090310.jpeg', 'normal_photo_1195090310.jpeg', 7);
INSERT INTO `photo` VALUES (32, 2, '29', 'photo_1195090318.jpg', '', 1195090318, 0, 1, 'thumb_photo_1195090318.jpeg', 'normal_photo_1195090318.jpeg', 7);
INSERT INTO `photo` VALUES (33, 2, '30', 'photo_1195090326.jpg', '', 1195090327, 0, 1, 'thumb_photo_1195090326.jpeg', 'normal_photo_1195090326.jpeg', 7);
INSERT INTO `photo` VALUES (34, 2, '31', 'photo_1195090332.jpg', '', 1195090333, 0, 1, 'thumb_photo_1195090332.jpeg', 'normal_photo_1195090332.jpeg', 7);
INSERT INTO `photo` VALUES (35, 2, '32', 'photo_1195090340.jpg', '', 1195090340, 0, 1, 'thumb_photo_1195090340.jpeg', 'normal_photo_1195090340.jpeg', 7);
INSERT INTO `photo` VALUES (36, 2, '33', 'photo_1195090348.jpg', '', 1195090349, 0, 1, 'thumb_photo_1195090348.jpeg', 'normal_photo_1195090348.jpeg', 7);
INSERT INTO `photo` VALUES (37, 1, 'Lexus', 'photo_1195202613.jpg', '', 1195202613, 0, 1, 'thumb_photo_1195202613.jpg', 'normal_photo_1195202613.jpg', 7);
INSERT INTO `photo` VALUES (38, 1, '1', 'photo_1195202648.jpg', '', 1195202648, 0, 1, 'thumb_photo_1195202648.jpg', 'normal_photo_1195202648.jpg', 7);
INSERT INTO `photo` VALUES (39, 1, 'BMW X5', 'photo_1195202851.jpg', '', 1195202851, 0, 1, 'thumb_photo_1195202851.jpg', 'normal_photo_1195202851.jpg', 7);
INSERT INTO `photo` VALUES (40, 1, 'Mazda', 'photo_1195202869.jpg', '', 1195202869, 0, 1, 'thumb_photo_1195202869.jpg', 'normal_photo_1195202869.jpg', 7);
INSERT INTO `photo` VALUES (41, 1, 'Rolls-royce', 'photo_1195203056.jpg', '', 1195203056, 0, 1, 'thumb_photo_1195203056.jpg', 'normal_photo_1195203056.jpg', 7);
INSERT INTO `photo` VALUES (42, 1, 'Rolls-royce', 'photo_1195203070.jpg', '', 1195203070, 0, 1, 'thumb_photo_1195203070.jpg', 'normal_photo_1195203070.jpg', 7);
INSERT INTO `photo` VALUES (43, 1, 'Rolls-royce', 'photo_1195203099.jpg', '', 1195203099, 0, 1, 'thumb_photo_1195203099.jpg', 'normal_photo_1195203099.jpg', 7);
INSERT INTO `photo` VALUES (44, 1, 'Rolls-royce', 'photo_1195203111.jpg', '', 1195203112, 0, 1, 'thumb_photo_1195203111.jpg', 'normal_photo_1195203111.jpg', 7);
INSERT INTO `photo` VALUES (45, 1, 'Rolls-royce', 'photo_1195203127.jpg', '', 1195203128, 0, 1, 'thumb_photo_1195203127.jpg', 'normal_photo_1195203127.jpg', 7);

-- --------------------------------------------------------

-- 
-- Table structure for table `product`
-- 

CREATE TABLE `product` (
  `id_product` bigint(20) unsigned NOT NULL auto_increment,
  `id_catpd` bigint(20) unsigned NOT NULL default '0',
  `name` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `price` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `don_vi` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `intro` mediumtext character set latin1 collate latin1_general_ci NOT NULL,
  `content` longtext character set latin1 collate latin1_general_ci NOT NULL,
  `ngay_dang` bigint(20) unsigned NOT NULL default '0',
  `thu_tu` bigint(20) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '1',
  `small_image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `normal_image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `id_user` tinyint(3) unsigned NOT NULL default '0',
  `moi` tinyint(4) NOT NULL default '0',
  `sologan` text NOT NULL,
  PRIMARY KEY  (`id_product`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

-- 
-- Dumping data for table `product`
-- 

INSERT INTO `product` VALUES (1, 6, 'Kia New Morning', 'product_1195014175.jpg', '', '', '<font face="Arial">\r\n<table cellspacing="2" cellpadding="2" width="100%">\r\n    <tbody>\r\n        <tr>\r\n            <td class="link_title" width="30%">M&atilde; xe</td>\r\n            <td class="link_title" width="70%">HGB-SEC-08</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="content" width="30%">S? km</td>\r\n            <td class="content">6,968 miles&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="content" width="30%">??i xe</td>\r\n            <td class="content">2007</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="content" width="30%">Xu?t x?</td>\r\n            <td class="content">M?</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="content" width="30%">N?i b&aacute;n</td>\r\n            <td class="content">H&agrave; N?i</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="content" width="30%">T&igrave;nh tr?ng</td>\r\n            <td class="content">Xe c&oacute; t?i Showroom (KH ?&atilde; ??t)</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n</font>', '<font face="Arial"><font face="Arial"><font face="Arial">\r\n<table cellspacing="8" cellpadding="0" width="100%" border="0">\r\n    <tbody>\r\n        <tr>\r\n            <td class="link_title" colspan="3">Th&ocirc;ng s? c? b?n</td>\r\n        </tr>\r\n        <tr>\r\n            <td colspan="3">&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td width="19%">&nbsp;</td>\r\n            <td class="content" width="30%">H&atilde;ng xe</td>\r\n            <td class="content" width="65%">BMW</td>\r\n        </tr>\r\n        <tr>\r\n            <td width="19%">&nbsp;</td>\r\n            <td class="content" width="30%">M?u xe</td>\r\n            <td class="content" width="65%">X5</td>\r\n        </tr>\r\n        <tr>\r\n            <td width="10%">&nbsp;</td>\r\n            <td class="content" nowrap="nowrap" width="30%">N?m sxu?t</td>\r\n            <td class="content" width="55%">2006</td>\r\n        </tr>\r\n        <tr>\r\n            <td width="10%">&nbsp;</td>\r\n            <td class="content" nowrap="nowrap" width="30%">M&agrave;u ngo?i th?t</td>\r\n            <td class="content" width="55%">M&agrave;u b?c</td>\r\n        </tr>\r\n        <tr>\r\n            <td width="10%">&nbsp;</td>\r\n            <td class="content" nowrap="nowrap" width="30%">M&agrave;u n?i th?t</td>\r\n            <td class="content" width="55%">M&agrave;u ghi s&aacute;ng</td>\r\n        </tr>\r\n        <tr>\r\n            <td width="10%">&nbsp;</td>\r\n            <td class="content" nowrap="nowrap" width="30%">Lo?i nhi&ecirc;n li?u</td>\r\n            <td class="content" width="55%">X?ng</td>\r\n        </tr>\r\n        <tr>\r\n            <td width="10%">&nbsp;</td>\r\n            <td class="content" nowrap="nowrap" width="30%">S? ch? ng?i</td>\r\n            <td class="content" width="55%">7 ch?</td>\r\n        </tr>\r\n        <tr>\r\n            <td width="10%">&nbsp;</td>\r\n            <td class="content" nowrap="nowrap" width="30%">Dung t&iacute;ch xi lanh</td>\r\n            <td class="content" width="55%">3.0 L</td>\r\n        </tr>\r\n        <tr>\r\n            <td width="10%">&nbsp;</td>\r\n            <td class="content" nowrap="nowrap" width="30%">H?p s?</td>\r\n            <td class="content" width="55%">S? t? ??ng</td>\r\n        </tr>\r\n        <tr>\r\n            <td width="10%">&nbsp;</td>\r\n            <td class="content" nowrap="nowrap" width="30%">La r?ng</td>\r\n            <td class="content" width="55%">255/55 R18</td>\r\n        </tr>\r\n        <tr>\r\n            <td width="10%">&nbsp;</td>\r\n            <td class="content" nowrap="nowrap" width="30%">S? VIN</td>\r\n            <td class="content" width="55%">4USFE43567LY76293</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n</font></font></font>', 1194917367, 0, 1, 'thumb_product_1195014175.jpeg', 'normal_product_1195014175.jpeg', 7, 1, 'TrÆ°á»ng tá»“n vá»›i thá»i gian');
INSERT INTO `product` VALUES (2, 6, 'Innova', 'product_1195003493.gif', '', '', 'fd sfsdfsd fsd fsd fsdf sdfsdf', 'df sdfsd', 1194919298, 0, 1, 'thumb_product_1195003493.gif', 'normal_product_1195003493.gif', 7, 1, 'LÆ°á»›t cÃ¹ng thá»i Ä‘áº¡i');
INSERT INTO `product` VALUES (3, 6, 'Messedes Ben', 'product_1195204699.jpg', '', '', '<table cellspacing="2" cellpadding="2" width="100%">\r\n    <tbody>\r\n        <tr>\r\n            <td class="link_title" width="30%">M&atilde; xe</td>\r\n            <td class="link_title" width="70%">HGB-SEC-08</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="content" width="30%">S? km</td>\r\n            <td class="content">6,968 miles&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="content" width="30%">??i xe</td>\r\n            <td class="content">2007</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="content" width="30%">Xu?t x?</td>\r\n            <td class="content">M?</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="content" width="30%">N?i b&aacute;n</td>\r\n            <td class="content">H&agrave; N?i</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="content" width="30%">T&igrave;nh tr?ng</td>\r\n            <td class="content">Xe c&oacute; t?i Showroom (KH ?&atilde; ??t)</td>\r\n        </tr>\r\n    </tbody>\r\n</table>', 'df sdfsd', 1194919310, 0, 1, 'thumb_product_1195204699.jpg', 'normal_product_1195204699.jpg', 7, 1, '??nh cao c?a ch?t l??ng');
INSERT INTO `product` VALUES (6, 7, 'Chevrolet', 'product_1195202701.php.jpg', '', 'USD', '<table cellspacing="2" cellpadding="2" width="100%">\r\n    <tbody>\r\n        <tr>\r\n            <td class="link_title" width="30%">M&atilde; xe</td>\r\n            <td class="link_title" width="70%">HGT-NEW-03</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="content" width="30%">Sá»‘ km</td>\r\n            <td class="content">100% New&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="content" width="30%">Äá»i xe</td>\r\n            <td class="content">2007</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="content" width="30%">Xuáº¥t xá»©</td>\r\n            <td class="content">Má»¹</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="content" width="30%">NÆ¡i b&aacute;n</td>\r\n            <td class="content">H&agrave; Ná»™i</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="content" width="30%">T&igrave;nh tráº¡ng</td>\r\n            <td class="content">Xe Ä‘&atilde; b&aacute;n</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<tr>\r\n</tr>\r\n<td colspan="2">&nbsp;</td>\r\n&nbsp; &nbsp; &nbsp;', '', 1195027790, 0, 1, 'thumb_product_1195202701.php.jpg', 'normal_product_1195202701.php.jpg', 7, 1, '');

-- --------------------------------------------------------

-- 
-- Table structure for table `raovat`
-- 

CREATE TABLE `raovat` (
  `id_raovat` bigint(20) unsigned NOT NULL auto_increment,
  `id_catrv` bigint(20) unsigned NOT NULL default '0',
  `name` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `price` decimal(10,0) NOT NULL default '0',
  `don_vi` varchar(255) NOT NULL,
  `title` text NOT NULL,
  `image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `intro` mediumtext character set latin1 collate latin1_general_ci NOT NULL,
  `content` text character set latin1 collate latin1_general_ci NOT NULL,
  `ngay_dang` bigint(20) unsigned NOT NULL default '0',
  `thu_tu` bigint(20) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '1',
  `small_image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `normal_image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `id_user` bigint(20) NOT NULL default '0',
  `id_user_post` bigint(20) NOT NULL default '0',
  `hit` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`id_raovat`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

-- 
-- Dumping data for table `raovat`
-- 

INSERT INTO `raovat` VALUES (16, 9, 'abdcdd  ', 150000, '$', '', 'raovat_1195467208.jpg', '<font face="Arial">abdcdd&nbsp; <font face="Arial">abdcdd&nbsp; <font face="Arial">abdcdd&nbsp; <font face="Arial">abdcdd&nbsp; <font face="Arial">abdcdd&nbsp; <font face="Arial">abdcdd&nbsp; </font></font></font></font></font></font>', '', 1195467208, 0, 1, 'thumb_raovat_1195467208.jpeg', 'normal_raovat_1195467208.jpeg', 7, 0, 2);
INSERT INTO `raovat` VALUES (17, 10, 'oto cu 1', 130000, '$', '', 'raovat_1195467380.jpg', 'xem', '', 1195467380, 0, 1, 'thumb_raovat_1195467380.jpeg', 'normal_raovat_1195467380.jpeg', 7, 0, 3);
INSERT INTO `raovat` VALUES (18, 10, 'OTO 3', 200000, '$', '', 'raovat_1195468412.jpg', '', '', 1195468413, 0, 1, 'thumb_raovat_1195468412.jpeg', 'normal_raovat_1195468412.jpeg', 7, 0, 2);
INSERT INTO `raovat` VALUES (19, 11, 'OTO 4', 140000, '$', 'KhÃ¡ch hÃ ng mua xe Ä‘Æ°á»£c há»— trá»£ Ä‘Äƒng kÃ½, Ä‘Äƒng kiá»ƒm (láº¥y sá»• ngay), tÆ° váº¥n mua xe tráº£ gÃ³p nhanh chÃ³ng. Má»i nhu cáº§u xin liÃªn há»‡: Honda Ã” tÃ´ Giáº£i PhÃ³ng \r<br><br><br><br>', 'raovat_1195468429.jpg', '<p>\r\n<table cellspacing="0" cellpadding="5" width="100%" border="0">\r\n    <tbody>\r\n        <tr>\r\n            <td colspan="2"><em><strong>&raquo; Th&ocirc;ng sá»‘ ká»¹ thuáº­t </strong></em></td>\r\n        </tr>\r\n        <tr>\r\n            <td align="left" width="350">- Há»™p sá»‘ tá»± Ä‘á»™ng<br />\r\n            - Radio Cassette<br />\r\n            - CD chá»©a nhiá»u Ä‘Ä©a<br />\r\n            - Phanh ABS<br />\r\n            - Ä&egrave;n sÆ°Æ¡ng m&ugrave;<br />\r\n            - Äiá»u h&ograve;a trÆ°á»›c<br />\r\n            - Larang Ä‘&uacute;c<br />\r\n            - 4 cá»­a<br />\r\n            - Gháº¿ da<br />\r\n            </td>\r\n            <td align="left">- T&uacute;i kh&iacute; ngÆ°á»i l&aacute;i<br />\r\n            - T&uacute;i kh&iacute; h&agrave;nh kh&aacute;ch trÆ°á»›c<br />\r\n            - CD chá»©a 1 Ä‘Ä©a<br />\r\n            - Trá»£ lá»±c tay l&aacute;i<br />\r\n            - V&ocirc; lÄƒng Ä‘iá»u chá»‰nh Ä‘Æ°á»£c<br />\r\n            - Äiá»u khiá»ƒn xa Ä‘&oacute;ng má»Ÿ cá»­a<br />\r\n            - B&aacute;o Ä‘á»™ng Ä‘á» ná»•<br />\r\n            - Cá»­a k&iacute;nh Ä‘iá»‡n<br />\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n</p>', '<table>\r\n    <tbody>\r\n        <tr>\r\n            <td><em><strong>&raquo; Th&ocirc;ng tin kh&aacute;c</strong></em></td>\r\n        </tr>\r\n        <tr>\r\n            <td>\r\n            <p align="justify">Kh&aacute;ch h&agrave;ng mua xe Ä‘Æ°á»£c há»— trá»£ Ä‘Äƒng k&yacute;, Ä‘Äƒng kiá»ƒm (láº¥y sá»• ngay), tÆ° váº¥n mua xe tráº£ g&oacute;p nhanh ch&oacute;ng. Má»i nhu cáº§u xin li&ecirc;n há»‡: Honda &Ocirc; t&ocirc; Giáº£i Ph&oacute;ng, <br />\r\n            <strong>Nguyá»…n B&aacute;ch Viá»‡t, ÄT: 097 6226 898</strong><br />\r\n            <br />\r\n            Honda Civic tháº¿ há»‡ thá»© 8 sáº£n pháº©m to&agrave;n cáº§u, kiá»ƒu d&aacute;ng sang trá»ng v&agrave; thanh lá»‹ch. Ch&uacute;ng t&ocirc;i xin giá»›i thiá»‡u tá»›i Qu&iacute; vá»‹ 3 sáº£n pháº©m cá»§a Honda Civic.<br />\r\n            Honda civic2.0/at: Vá»›i ná»™i tháº¥t sang trá»ng, rá»™nh r&atilde;i v&agrave; hiá»‡n Ä‘áº¡i, báº³ng Ä‘á»“ng há»“ Ä‘a chá»©c nÄƒng Multiplex, d&agrave;n &acirc;m thanh cháº¥t lÆ°á»£ng tuyá»‡t háº£o vá»›i há»‡ thá»‘ng Ä‘áº§u Ä‘á»c 6 Ä‘Ä©a v&agrave; m&agrave;n h&igrave;nh LCD c&oacute; chá»©c nÄƒng nhÆ° 1 chiáº¿c Ä‘á»“ng há»“ ká»¹ thuáº­t sá»‘. Civic 2.0 c&ograve;n c&oacute; t&iacute;nh nÄƒng trá»£ lá»±c l&aacute;i Ä‘iá»‡n tá»­ th&ocirc;ng minh (EPS), cá»­a n&oacute;c, gháº¿ da, Ä‘iá»u ho&agrave; tá»± Ä‘á»™ng, há»‡ thá»‘ng chá»‘ng b&oacute; cá»©ng phanh ABS vá»›i EBD, t&uacute;i kh&iacute; Ä‘&ocirc;i SRS v&agrave; c&aacute;c d&acirc;y Ä‘ai an to&agrave;n tá»± cÄƒng.<br />\r\n            - Honda Civic 1.8/at v&agrave; MT: Kiá»ƒu xe Ä‘Æ°á»£c ph&aacute;t triá»ƒn vá»›i ná»™i tháº¥t sang trá»ng v&agrave; rá»™ng r&atilde;i gi&uacute;p báº¡n ráº¥t tá»± tin v&agrave; thoáº£i m&aacute;i khi ngá»“i trong xe. Vá»›i kho&aacute; Ä‘iá»‡n Ä‘iá»u khiá»ƒn tá»« xa, gÆ°Æ¡ng chiáº¿u háº­u gáº­p Ä‘iá»‡n, Ä‘á»“ng há»“ Ä‘a táº§ng&hellip;v&agrave; má»™t sá»‘ c&aacute;c thiáº¿t bá»‹ tiá»‡n nghi v&agrave; hiá»‡n Ä‘áº¡i kh&aacute;c.<br />\r\n            - Qu&iacute; vá»‹ chá»‰ pháº£i tráº£ trÆ°á»›c 150.000.000vnd l&agrave; sá»Ÿ há»¯u ngay chiáº¿c xe Æ°ng &yacute; nháº¥t.<br />\r\n            Äá»ƒ biáº¿t th&ecirc;m th&ocirc;ng tin chi tiáº¿t cÅ©ng nhÆ° h&igrave;nh áº£nh cá»§a chiáº¿c xe m&agrave; Qu&iacute; vá»‹ Ä‘ang quan t&acirc;m. Xin vui l&ograve;ng li&ecirc;n há»‡:<br />\r\n            <strong>Nguyá»…n B&aacute;ch Viá»‡t &ndash; TÆ° váº¥n sáº£n pháº©m<br />\r\n            Mobile: 0976. 22.68.98</strong><br />\r\n            Xem chi tiáº¿t th&ocirc;ng sá»‘ ká»¹ thuáº­t táº¡i : http://hondacivic.com.vn <br />\r\n            <br />\r\n            ----------- Äiá»‡n thoáº¡i Ä‘á»ƒ biáº¿t gi&aacute; tá»‘t nháº¥t. -----------<br />\r\n            <br />\r\n            <strong>Miá»n báº¯c : Nguyá»…n B&aacute;ch viá»‡t : (0976. 22.68.98)</strong> - Cháº¯c cháº¯n c&oacute; gi&aacute; b&aacute;n v&agrave; c&aacute;c cháº¿ Ä‘á»™ háº­u m&atilde;i tá»‘t nháº¥t. H&igrave;nh thá»©c tráº£ tháº³ng, tráº£ g&oacute;p, thu&ecirc; mua... thá»§ tá»¥c nhanh , l&atilde;i suáº¥t tháº¥p. Há»— trá»£ má»i thá»§ tá»¥c Ä‘Äƒng k&yacute; , Ä‘Äƒng kiá»ƒm, Ä‘á»“ chÆ¡i. Mua b&aacute;n trao Ä‘á»•i xe cÅ©...(20/11) <br />\r\n            <br />\r\n            Miá»n trung : -------------<br />\r\n            <br />\r\n            Miá»n nam : --------------<br />\r\n            <br />\r\n            H&agrave;ng ng&agrave;y ch&uacute;ng t&ocirc;i nháº­n Ä‘Æ°á»£c h&agrave;ng trÄƒm cuá»™c Ä‘iá»‡n thoáº¡i v&agrave; email há»i mua &ocirc;t&ocirc; v&agrave; c&aacute;c th&ocirc;ng tin kh&aacute;c li&ecirc;n quan Ä‘áº¿n &ocirc;t&ocirc;. Äá»ƒ phá»¥c vá»¥ h&agrave;ng váº¡n ngÆ°á»i c&oacute; nhu cáº§u mua b&aacute;n &ocirc;t&ocirc; táº¡i www.MUABANOTO.VN , ch&uacute;ng t&ocirc;i Ä‘ang t&igrave;m c&aacute;c Ä‘á»‘i t&aacute;c c&oacute; kháº£ nÄƒng cung cáº¥p &ocirc;t&ocirc; trong nÆ°á»›c v&agrave; nháº­p kháº©u vá»›i gi&aacute; b&aacute;n v&agrave; c&aacute;c cháº¿ Ä‘á»™ háº­u m&atilde;i tá»‘t nháº¥t táº¡i 3 miá»n Báº¯c, Trung, Nam Ä‘á»ƒ Ä‘Äƒng táº£i tr&ecirc;n www.MUABANOTO.VN Ä‘á»ƒ ngÆ°á»i mua li&ecirc;n há»‡ trá»±c tiáº¿p, qu&iacute; vá»‹ n&agrave;o c&oacute; kháº£ nÄƒng nhÆ° tr&ecirc;n má»i li&ecirc;n há»‡ : 04.552.1980 - 0913.510033 ; Email : carvina@gmail.com </p>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<!-- BEGIN: File: /themes/Default/footer.php -->', 1195468430, 0, 1, 'thumb_raovat_1195468429.jpeg', 'normal_raovat_1195468429.jpeg', 7, 0, 14);
INSERT INTO `raovat` VALUES (20, 10, 'Hon da Civic', 20000, '$', 'Mua hon da civic Tang 2 nguoi dep', 'raovat_1195532245.jpg', '<br />\r\n<br />\r\n<br />\r\n<p>sdf sdf sdf</p>', 'df sdf sd', 0, 0, 1, 'thumb_raovat_1195532245.jpeg', 'normal_raovat_1195532245.jpeg', 0, 3, 12);
INSERT INTO `raovat` VALUES (21, 10, 'Toyota Vios', 12000, '$', 'Mua con nay tang den 3 nguoi dep', 'raovat_1195532232.jpg', 'd fsd f', 'd fsd d', 0, 0, 1, 'thumb_raovat_1195532232.jpeg', 'normal_raovat_1195532232.jpeg', 0, 3, 8);
INSERT INTO `raovat` VALUES (22, 10, 'Mazda 6', 23000, '$', 'Xe dep cho nhung nguoi it xien', 'raovat_1195532218.jpg', 'd fsd', '&nbsp;df sd', 0, 0, 1, 'thumb_raovat_1195532218.jpeg', 'normal_raovat_1195532218.jpeg', 0, 3, 131);
INSERT INTO `raovat` VALUES (23, 10, 'BMW X5', 90000, '$', 'Hop voi di tan gai', 'raovat_1195527656.jpg', 'df sdfsd', 'd fsd f', 0, 0, 1, 'thumb_raovat_1195527656.jpeg', 'normal_raovat_1195527656.jpeg', 0, 3, 2);
INSERT INTO `raovat` VALUES (24, 9, 'dsfasd', 34000, '$', 'sdfsd', 'raovat_1195528336.jpg', 'df sdf', 'd sfsd', 0, 0, 1, 'thumb_raovat_1195528336.jpeg', 'normal_raovat_1195528336.jpeg', 0, 3, 1);
INSERT INTO `raovat` VALUES (25, 9, 'dsfasd', 34000, '$', 'sdfsd', 'raovat_1195528355.jpg', 'df sdf', 'd sfsd', 0, 0, 1, 'thumb_raovat_1195528355.jpeg', 'normal_raovat_1195528355.jpeg', 0, 3, 2);
INSERT INTO `raovat` VALUES (26, 11, 'Innova', 21000, '$', 'f df sdf sdf sd fsd', 'raovat_1195530652.jpg', 'd fsdf', 'd fsd fds', 0, 0, 1, 'thumb_raovat_1195530652.jpeg', 'normal_raovat_1195530652.jpeg', 0, 3, 2);
INSERT INTO `raovat` VALUES (27, 11, 'Matiz1', 8001, '$', 'fd fsd 1', 'raovat_1195532079.jpg', 'fsd fsd1', 'd fsd fsd1', 1195530731, 0, 1, 'thumb_raovat_1195532079.jpeg', 'normal_raovat_1195532079.jpeg', 0, 3, 184);
INSERT INTO `raovat` VALUES (28, 10, 'Rolls-royce phantom', 1000000, '$', 'f sd fsdf ', 'raovat_1195532389.jpg', 'dsf sdf sd', 'fsdfs', 1195532356, 0, 1, 'thumb_raovat_1195532389.jpeg', 'normal_raovat_1195532389.jpeg', 0, 0, 20);

-- --------------------------------------------------------

-- 
-- Table structure for table `settings`
-- 

CREATE TABLE `settings` (
  `setting_name` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `setting_value` mediumtext character set latin1 collate latin1_general_ci NOT NULL,
  PRIMARY KEY  (`setting_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table `settings`
-- 

INSERT INTO `settings` VALUES ('site_name', 'CÃ”NG TY TNHH THIÃŠN Báº¢O ANH');
INSERT INTO `settings` VALUES ('dir_path', '/baoanh');
INSERT INTO `settings` VALUES ('site_email', 'binh_mczk@yahoo.com');
INSERT INTO `settings` VALUES ('use_smtp', '0');
INSERT INTO `settings` VALUES ('smtp_host', '');
INSERT INTO `settings` VALUES ('smtp_username', '');
INSERT INTO `settings` VALUES ('smtp_password', '');
INSERT INTO `settings` VALUES ('template_dir', 'default');
INSERT INTO `settings` VALUES ('language_dir', 'english');
INSERT INTO `settings` VALUES ('date_format', 'd/m/Y');
INSERT INTO `settings` VALUES ('time_format', 'H:i');
INSERT INTO `settings` VALUES ('convert_tool', 'gd');
INSERT INTO `settings` VALUES ('convert_tool_path', '');
INSERT INTO `settings` VALUES ('gz_compress', '1');
INSERT INTO `settings` VALUES ('gz_compress_level', '');
INSERT INTO `settings` VALUES ('upload_mode', '2');
INSERT INTO `settings` VALUES ('allowed_mediatypes', 'jpg,gif,png,bmp,aif,au,avi,mid,mov,mp3,mpg,swf,wav,rar,ra,rm,zip,pdf,txt,xls,doc,swf');
INSERT INTO `settings` VALUES ('max_thumb_width', '200');
INSERT INTO `settings` VALUES ('max_thumb_height', '300');
INSERT INTO `settings` VALUES ('max_image_height', '1024');
INSERT INTO `settings` VALUES ('max_media_size', '5000');
INSERT INTO `settings` VALUES ('upload_notify', '0');
INSERT INTO `settings` VALUES ('upload_emails', '');
INSERT INTO `settings` VALUES ('auto_thumbnail', '1');
INSERT INTO `settings` VALUES ('auto_thumbnail_dimension', '135');
INSERT INTO `settings` VALUES ('auto_thumbnail_resize_type', '1');
INSERT INTO `settings` VALUES ('auto_thumbnail_quality', '100');
INSERT INTO `settings` VALUES ('id_country', '207');
INSERT INTO `settings` VALUES ('paging_range', '5');
INSERT INTO `settings` VALUES ('watermark_text', '');
INSERT INTO `settings` VALUES ('upload_media_path', 'upload/medias/');
INSERT INTO `settings` VALUES ('upload_image_path', 'upload/images/');
INSERT INTO `settings` VALUES ('session_timeout', '15');
INSERT INTO `settings` VALUES ('max_image_width', '200');
INSERT INTO `settings` VALUES ('time_offset', '0');
INSERT INTO `settings` VALUES ('http_host', 'localhost:801');
INSERT INTO `settings` VALUES ('document_root', 'D:/AppServ/www');
INSERT INTO `settings` VALUES ('site_keywords', 'Nguyen Binh');
INSERT INTO `settings` VALUES ('site_description', 'Ha Noi');
INSERT INTO `settings` VALUES ('bao_gia', 'upload/medias/bao_gia_1154146521.xls');
INSERT INTO `settings` VALUES ('diachi_cty', '<p><font face="Arial">C&Ocirc;NG TY TNHH THI&Ecirc;N Báº¢O ANH<br />\r\nÄC: 33-43 L&ecirc; VÄƒn LÆ°Æ¡ng - Trung H&ograve;a - Cáº§u Giáº¥y - H&agrave; ná»™i<br />\r\nÄT: 04. 210 3228&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; *&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; FAX: 04. 556 7322<br />\r\nEmail: 123@yahoo.com&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; *&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Website: thienbaoanh.com.vn</font></p>');
INSERT INTO `settings` VALUES ('active_site', '1');
INSERT INTO `settings` VALUES ('content_active_site', '<p><span class="style1"><font color="#ff0000">Website n&agrave;y Ä‘ang táº¡m ngá»«ng hoáº¡t Ä‘á»™ng Ä‘á»ƒ n&acirc;ng cáº¥p, xin qu&yacute; kh&aacute;ch vui l&ograve;ng gh&eacute; thÄƒm sau!</font></span></p>');
INSERT INTO `settings` VALUES ('sl_image', '10');

-- --------------------------------------------------------

-- 
-- Table structure for table `shortcut`
-- 

CREATE TABLE `shortcut` (
  `id_shortcut` tinyint(4) NOT NULL auto_increment,
  `title` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `image` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `link` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `active` tinyint(4) NOT NULL default '0',
  `thu_tu` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id_shortcut`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

-- 
-- Dumping data for table `shortcut`
-- 

INSERT INTO `shortcut` VALUES (2, 'S&#7843;n ph&#7849;m', 'shortcut_1184358087.gif', '?act=catpd&code=00', 1, 5);
INSERT INTO `shortcut` VALUES (1, 'Th&#244;ng tin li&#234;n h&#7879;', 'shortcut_1184358104.png', '?act=contact&code=00', 1, 7);
INSERT INTO `shortcut` VALUES (8, 'Tin t&#7913;c', 'shortcut_1184358095.gif', '?act=cat&code=00', 1, 6);
INSERT INTO `shortcut` VALUES (9, 'Database', 'shortcut_1184358064.gif', '?act=dbadmin', 1, 2);
INSERT INTO `shortcut` VALUES (10, 'Th&#244;ng tin c&#225; nh&#226;n', 'shortcut_1184358071.png', '?act=myadmin', 1, 4);
INSERT INTO `shortcut` VALUES (11, 'QL ng&#432;&#7901;i s&#7917; d&#7909;ng', 'shortcut_1184358078.png', '?act=users&code=00', 1, 3);
INSERT INTO `shortcut` VALUES (12, 'C&#7845;u h&#236;nh h&#7879; th&#7889;ng', 'shortcut_1184358054.gif', '?act=settings&code=00', 0, 1);
INSERT INTO `shortcut` VALUES (14, 'Logo qu?ng c', 'shortcut_1184358118.gif', '?act=catlg&code=00', 1, 8);
INSERT INTO `shortcut` VALUES (15, 'Trang n?i dung', 'shortcut_1184358149.gif', '?act=catif&code=00', 1, 10);
INSERT INTO `shortcut` VALUES (17, 'Qu&#7843;n l&#253; n&#7897;i dung', 'shortcut_1184803969.png', '?act=catif&code=00', 1, 11);

-- --------------------------------------------------------

-- 
-- Table structure for table `ttlienhe`
-- 

CREATE TABLE `ttlienhe` (
  `id_ttlienhe` bigint(20) NOT NULL auto_increment,
  `id_raovat` bigint(20) NOT NULL default '0',
  `ttlienhe` text NOT NULL,
  PRIMARY KEY  (`id_ttlienhe`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `ttlienhe`
-- 

INSERT INTO `ttlienhe` VALUES (1, 25, 'ten');
INSERT INTO `ttlienhe` VALUES (2, 0, 'dfsd fsd fsd sd');
INSERT INTO `ttlienhe` VALUES (3, 28, 'thong tin lien he');

-- --------------------------------------------------------

-- 
-- Table structure for table `useronline`
-- 

CREATE TABLE `useronline` (
  `id` int(10) NOT NULL auto_increment,
  `ip` varchar(15) NOT NULL default '',
  `timestamp` varchar(15) NOT NULL default '',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43263 ;

-- 
-- Dumping data for table `useronline`
-- 

INSERT INTO `useronline` VALUES (42897, '127.0.0.1', '1195559118');
INSERT INTO `useronline` VALUES (42899, '127.0.0.1', '1195559121');
INSERT INTO `useronline` VALUES (42919, '127.0.0.1', '1195559144');
INSERT INTO `useronline` VALUES (42920, '127.0.0.1', '1195559146');
INSERT INTO `useronline` VALUES (42967, '127.0.0.1', '1195559207');
INSERT INTO `useronline` VALUES (43068, '127.0.0.1', '1195559345');
INSERT INTO `useronline` VALUES (43082, '127.0.0.1', '1195559367');
INSERT INTO `useronline` VALUES (43084, '127.0.0.1', '1195559369');
INSERT INTO `useronline` VALUES (43100, '127.0.0.1', '1195559393');
INSERT INTO `useronline` VALUES (43101, '127.0.0.1', '1195559394');
INSERT INTO `useronline` VALUES (43103, '127.0.0.1', '1195559397');
INSERT INTO `useronline` VALUES (43041, '127.0.0.1', '1195559311');
INSERT INTO `useronline` VALUES (43045, '127.0.0.1', '1195559315');
INSERT INTO `useronline` VALUES (43044, '127.0.0.1', '1195559313');
INSERT INTO `useronline` VALUES (43049, '127.0.0.1', '1195559321');
INSERT INTO `useronline` VALUES (42994, '127.0.0.1', '1195559248');
INSERT INTO `useronline` VALUES (42995, '127.0.0.1', '1195559249');
INSERT INTO `useronline` VALUES (42996, '127.0.0.1', '1195559251');
INSERT INTO `useronline` VALUES (43008, '127.0.0.1', '1195559268');
INSERT INTO `useronline` VALUES (43009, '127.0.0.1', '1195559268');
INSERT INTO `useronline` VALUES (43010, '127.0.0.1', '1195559269');
INSERT INTO `useronline` VALUES (43011, '127.0.0.1', '1195559269');
INSERT INTO `useronline` VALUES (43012, '127.0.0.1', '1195559270');
INSERT INTO `useronline` VALUES (43014, '127.0.0.1', '1195559273');
INSERT INTO `useronline` VALUES (43015, '127.0.0.1', '1195559274');
INSERT INTO `useronline` VALUES (43016, '127.0.0.1', '1195559276');
INSERT INTO `useronline` VALUES (43018, '127.0.0.1', '1195559279');
INSERT INTO `useronline` VALUES (43020, '127.0.0.1', '1195559282');
INSERT INTO `useronline` VALUES (42970, '127.0.0.1', '1195559212');
INSERT INTO `useronline` VALUES (42968, '127.0.0.1', '1195559209');
INSERT INTO `useronline` VALUES (42969, '127.0.0.1', '1195559210');
INSERT INTO `useronline` VALUES (43042, '127.0.0.1', '1195559312');
INSERT INTO `useronline` VALUES (43046, '127.0.0.1', '1195559316');
INSERT INTO `useronline` VALUES (43047, '127.0.0.1', '1195559318');
INSERT INTO `useronline` VALUES (43059, '127.0.0.1', '1195559331');
INSERT INTO `useronline` VALUES (43060, '127.0.0.1', '1195559333');
INSERT INTO `useronline` VALUES (43048, '127.0.0.1', '1195559319');
INSERT INTO `useronline` VALUES (43051, '127.0.0.1', '1195559324');
INSERT INTO `useronline` VALUES (43058, '127.0.0.1', '1195559331');
INSERT INTO `useronline` VALUES (43062, '127.0.0.1', '1195559336');
INSERT INTO `useronline` VALUES (43089, '127.0.0.1', '1195559376');
INSERT INTO `useronline` VALUES (43091, '127.0.0.1', '1195559379');
INSERT INTO `useronline` VALUES (43094, '127.0.0.1', '1195559384');
INSERT INTO `useronline` VALUES (43095, '127.0.0.1', '1195559385');
INSERT INTO `useronline` VALUES (43096, '127.0.0.1', '1195559388');
INSERT INTO `useronline` VALUES (43099, '127.0.0.1', '1195559391');
INSERT INTO `useronline` VALUES (43128, '127.0.0.1', '1195559435');
INSERT INTO `useronline` VALUES (43136, '127.0.0.1', '1195559447');
INSERT INTO `useronline` VALUES (43140, '127.0.0.1', '1195559453');
INSERT INTO `useronline` VALUES (43142, '127.0.0.1', '1195559456');
INSERT INTO `useronline` VALUES (43177, '127.0.0.1', '1195559499');
INSERT INTO `useronline` VALUES (42924, '127.0.0.1', '1195559152');
INSERT INTO `useronline` VALUES (42923, '127.0.0.1', '1195559150');
INSERT INTO `useronline` VALUES (42926, '127.0.0.1', '1195559156');
INSERT INTO `useronline` VALUES (42901, '127.0.0.1', '1195559124');
INSERT INTO `useronline` VALUES (42905, '127.0.0.1', '1195559130');
INSERT INTO `useronline` VALUES (42912, '127.0.0.1', '1195559134');
INSERT INTO `useronline` VALUES (42913, '127.0.0.1', '1195559135');
INSERT INTO `useronline` VALUES (43223, '127.0.0.1', '1195559557');
INSERT INTO `useronline` VALUES (43227, '127.0.0.1', '1195559563');
INSERT INTO `useronline` VALUES (43233, '127.0.0.1', '1195559568');
INSERT INTO `useronline` VALUES (43234, '127.0.0.1', '1195559568');
INSERT INTO `useronline` VALUES (42831, '127.0.0.1', '1195559017');
INSERT INTO `useronline` VALUES (42944, '127.0.0.1', '1195559181');
INSERT INTO `useronline` VALUES (42945, '127.0.0.1', '1195559182');
INSERT INTO `useronline` VALUES (42946, '127.0.0.1', '1195559182');
INSERT INTO `useronline` VALUES (42947, '127.0.0.1', '1195559182');
INSERT INTO `useronline` VALUES (42948, '127.0.0.1', '1195559184');
INSERT INTO `useronline` VALUES (42915, '127.0.0.1', '1195559138');
INSERT INTO `useronline` VALUES (42917, '127.0.0.1', '1195559141');
INSERT INTO `useronline` VALUES (42933, '127.0.0.1', '1195559165');
INSERT INTO `useronline` VALUES (42934, '127.0.0.1', '1195559167');
INSERT INTO `useronline` VALUES (42938, '127.0.0.1', '1195559173');
INSERT INTO `useronline` VALUES (42939, '127.0.0.1', '1195559174');
INSERT INTO `useronline` VALUES (43197, '127.0.0.1', '1195559528');
INSERT INTO `useronline` VALUES (43199, '127.0.0.1', '1195559529');
INSERT INTO `useronline` VALUES (43221, '127.0.0.1', '1195559554');
INSERT INTO `useronline` VALUES (43235, '127.0.0.1', '1195559569');
INSERT INTO `useronline` VALUES (42816, '127.0.0.1', '1195558996');
INSERT INTO `useronline` VALUES (42818, '127.0.0.1', '1195558999');
INSERT INTO `useronline` VALUES (42846, '127.0.0.1', '1195559046');
INSERT INTO `useronline` VALUES (42852, '127.0.0.1', '1195559053');
INSERT INTO `useronline` VALUES (42855, '127.0.0.1', '1195559055');
INSERT INTO `useronline` VALUES (42845, '127.0.0.1', '1195559044');
INSERT INTO `useronline` VALUES (42851, '127.0.0.1', '1195559052');
INSERT INTO `useronline` VALUES (42850, '127.0.0.1', '1195559052');
INSERT INTO `useronline` VALUES (42853, '127.0.0.1', '1195559054');
INSERT INTO `useronline` VALUES (42854, '127.0.0.1', '1195559054');
INSERT INTO `useronline` VALUES (42875, '127.0.0.1', '1195559085');
INSERT INTO `useronline` VALUES (42862, '127.0.0.1', '1195559065');
INSERT INTO `useronline` VALUES (42863, '127.0.0.1', '1195559067');
INSERT INTO `useronline` VALUES (42865, '127.0.0.1', '1195559071');
INSERT INTO `useronline` VALUES (42868, '127.0.0.1', '1195559074');
INSERT INTO `useronline` VALUES (42869, '127.0.0.1', '1195559077');
INSERT INTO `useronline` VALUES (42872, '127.0.0.1', '1195559080');
INSERT INTO `useronline` VALUES (42880, '127.0.0.1', '1195559092');
INSERT INTO `useronline` VALUES (43072, '127.0.0.1', '1195559351');
INSERT INTO `useronline` VALUES (43167, '127.0.0.1', '1195559488');
INSERT INTO `useronline` VALUES (43173, '127.0.0.1', '1195559496');
INSERT INTO `useronline` VALUES (43181, '127.0.0.1', '1195559505');
INSERT INTO `useronline` VALUES (43175, '127.0.0.1', '1195559497');
INSERT INTO `useronline` VALUES (43183, '127.0.0.1', '1195559508');
INSERT INTO `useronline` VALUES (43182, '127.0.0.1', '1195559505');
INSERT INTO `useronline` VALUES (43185, '127.0.0.1', '1195559511');
INSERT INTO `useronline` VALUES (43187, '127.0.0.1', '1195559514');
INSERT INTO `useronline` VALUES (43200, '127.0.0.1', '1195559530');
INSERT INTO `useronline` VALUES (43202, '127.0.0.1', '1195559531');
INSERT INTO `useronline` VALUES (43204, '127.0.0.1', '1195559533');
INSERT INTO `useronline` VALUES (43205, '127.0.0.1', '1195559536');
INSERT INTO `useronline` VALUES (43213, '127.0.0.1', '1195559542');
INSERT INTO `useronline` VALUES (43214, '127.0.0.1', '1195559544');
INSERT INTO `useronline` VALUES (43243, '127.0.0.1', '1195559575');
INSERT INTO `useronline` VALUES (43244, '127.0.0.1', '1195559575');
INSERT INTO `useronline` VALUES (43245, '127.0.0.1', '1195559577');
INSERT INTO `useronline` VALUES (43250, '127.0.0.1', '1195559581');
INSERT INTO `useronline` VALUES (43251, '127.0.0.1', '1195559581');
INSERT INTO `useronline` VALUES (43252, '127.0.0.1', '1195559581');
INSERT INTO `useronline` VALUES (43253, '127.0.0.1', '1195559581');
INSERT INTO `useronline` VALUES (43254, '127.0.0.1', '1195559583');
INSERT INTO `useronline` VALUES (42820, '127.0.0.1', '1195559002');
INSERT INTO `useronline` VALUES (42822, '127.0.0.1', '1195559004');
INSERT INTO `useronline` VALUES (42819, '127.0.0.1', '1195559000');
INSERT INTO `useronline` VALUES (42823, '127.0.0.1', '1195559005');
INSERT INTO `useronline` VALUES (42825, '127.0.0.1', '1195559008');
INSERT INTO `useronline` VALUES (42828, '127.0.0.1', '1195559013');
INSERT INTO `useronline` VALUES (42844, '127.0.0.1', '1195559043');
INSERT INTO `useronline` VALUES (42911, '127.0.0.1', '1195559134');
INSERT INTO `useronline` VALUES (42916, '127.0.0.1', '1195559140');
INSERT INTO `useronline` VALUES (42906, '127.0.0.1', '1195559132');
INSERT INTO `useronline` VALUES (42904, '127.0.0.1', '1195559128');
INSERT INTO `useronline` VALUES (42910, '127.0.0.1', '1195559133');
INSERT INTO `useronline` VALUES (42907, '127.0.0.1', '1195559132');
INSERT INTO `useronline` VALUES (42908, '127.0.0.1', '1195559132');
INSERT INTO `useronline` VALUES (42909, '127.0.0.1', '1195559133');
INSERT INTO `useronline` VALUES (42984, '127.0.0.1', '1195559233');
INSERT INTO `useronline` VALUES (42985, '127.0.0.1', '1195559234');
INSERT INTO `useronline` VALUES (43002, '127.0.0.1', '1195559260');
INSERT INTO `useronline` VALUES (43004, '127.0.0.1', '1195559263');
INSERT INTO `useronline` VALUES (42955, '127.0.0.1', '1195559195');
INSERT INTO `useronline` VALUES (42950, '127.0.0.1', '1195559187');
INSERT INTO `useronline` VALUES (42951, '127.0.0.1', '1195559188');
INSERT INTO `useronline` VALUES (42943, '127.0.0.1', '1195559180');
INSERT INTO `useronline` VALUES (42965, '127.0.0.1', '1195559204');
INSERT INTO `useronline` VALUES (42966, '127.0.0.1', '1195559206');
INSERT INTO `useronline` VALUES (42962, '127.0.0.1', '1195559203');
INSERT INTO `useronline` VALUES (42960, '127.0.0.1', '1195559202');
INSERT INTO `useronline` VALUES (42961, '127.0.0.1', '1195559202');
INSERT INTO `useronline` VALUES (43071, '127.0.0.1', '1195559348');
INSERT INTO `useronline` VALUES (43152, '127.0.0.1', '1195559467');
INSERT INTO `useronline` VALUES (43153, '127.0.0.1', '1195559468');
INSERT INTO `useronline` VALUES (43067, '127.0.0.1', '1195559342');
INSERT INTO `useronline` VALUES (43070, '127.0.0.1', '1195559348');
INSERT INTO `useronline` VALUES (43087, '127.0.0.1', '1195559373');
INSERT INTO `useronline` VALUES (43085, '127.0.0.1', '1195559370');
INSERT INTO `useronline` VALUES (43079, '127.0.0.1', '1195559360');
INSERT INTO `useronline` VALUES (43080, '127.0.0.1', '1195559363');
INSERT INTO `useronline` VALUES (43075, '127.0.0.1', '1195559354');
INSERT INTO `useronline` VALUES (43076, '127.0.0.1', '1195559358');
INSERT INTO `useronline` VALUES (43191, '127.0.0.1', '1195559521');
INSERT INTO `useronline` VALUES (43189, '127.0.0.1', '1195559517');
INSERT INTO `useronline` VALUES (43193, '127.0.0.1', '1195559524');
INSERT INTO `useronline` VALUES (43220, '127.0.0.1', '1195559553');
INSERT INTO `useronline` VALUES (43241, '127.0.0.1', '1195559574');
INSERT INTO `useronline` VALUES (43238, '127.0.0.1', '1195559573');
INSERT INTO `useronline` VALUES (42833, '127.0.0.1', '1195559020');
INSERT INTO `useronline` VALUES (42834, '127.0.0.1', '1195559022');
INSERT INTO `useronline` VALUES (43134, '127.0.0.1', '1195559444');
INSERT INTO `useronline` VALUES (42817, '127.0.0.1', '1195558996');
INSERT INTO `useronline` VALUES (43258, '127.0.0.1', '1195559589');
INSERT INTO `useronline` VALUES (43259, '127.0.0.1', '1195559590');
INSERT INTO `useronline` VALUES (42930, '127.0.0.1', '1195559161');
INSERT INTO `useronline` VALUES (42929, '127.0.0.1', '1195559159');
INSERT INTO `useronline` VALUES (42936, '127.0.0.1', '1195559170');
INSERT INTO `useronline` VALUES (43179, '127.0.0.1', '1195559502');
INSERT INTO `useronline` VALUES (43133, '127.0.0.1', '1195559442');
INSERT INTO `useronline` VALUES (43256, '127.0.0.1', '1195559586');
INSERT INTO `useronline` VALUES (43257, '127.0.0.1', '1195559587');
INSERT INTO `useronline` VALUES (43260, '127.0.0.1', '1195559593');
INSERT INTO `useronline` VALUES (43261, '127.0.0.1', '1195559593');
INSERT INTO `useronline` VALUES (43262, '127.0.0.1', '1195559596');
INSERT INTO `useronline` VALUES (42921, '127.0.0.1', '1195559147');
INSERT INTO `useronline` VALUES (42922, '127.0.0.1', '1195559149');
INSERT INTO `useronline` VALUES (43166, '127.0.0.1', '1195559487');
INSERT INTO `useronline` VALUES (43178, '127.0.0.1', '1195559499');
INSERT INTO `useronline` VALUES (43172, '127.0.0.1', '1195559495');
INSERT INTO `useronline` VALUES (43170, '127.0.0.1', '1195559493');
INSERT INTO `useronline` VALUES (43171, '127.0.0.1', '1195559495');
INSERT INTO `useronline` VALUES (43169, '127.0.0.1', '1195559491');
INSERT INTO `useronline` VALUES (43180, '127.0.0.1', '1195559502');
INSERT INTO `useronline` VALUES (43184, '127.0.0.1', '1195559508');
INSERT INTO `useronline` VALUES (43188, '127.0.0.1', '1195559514');
INSERT INTO `useronline` VALUES (43186, '127.0.0.1', '1195559511');
INSERT INTO `useronline` VALUES (43209, '127.0.0.1', '1195559540');
INSERT INTO `useronline` VALUES (43201, '127.0.0.1', '1195559530');
INSERT INTO `useronline` VALUES (43196, '127.0.0.1', '1195559527');
INSERT INTO `useronline` VALUES (43195, '127.0.0.1', '1195559526');
INSERT INTO `useronline` VALUES (43190, '127.0.0.1', '1195559517');
INSERT INTO `useronline` VALUES (43192, '127.0.0.1', '1195559521');
INSERT INTO `useronline` VALUES (43194, '127.0.0.1', '1195559524');
INSERT INTO `useronline` VALUES (43203, '127.0.0.1', '1195559532');
INSERT INTO `useronline` VALUES (43211, '127.0.0.1', '1195559542');
INSERT INTO `useronline` VALUES (43212, '127.0.0.1', '1195559542');
INSERT INTO `useronline` VALUES (43218, '127.0.0.1', '1195559551');
INSERT INTO `useronline` VALUES (43216, '127.0.0.1', '1195559547');
INSERT INTO `useronline` VALUES (43217, '127.0.0.1', '1195559548');
INSERT INTO `useronline` VALUES (43225, '127.0.0.1', '1195559560');
INSERT INTO `useronline` VALUES (43239, '127.0.0.1', '1195559574');
INSERT INTO `useronline` VALUES (43219, '127.0.0.1', '1195559551');
INSERT INTO `useronline` VALUES (43222, '127.0.0.1', '1195559556');
INSERT INTO `useronline` VALUES (42925, '127.0.0.1', '1195559154');
INSERT INTO `useronline` VALUES (42927, '127.0.0.1', '1195559156');
INSERT INTO `useronline` VALUES (42928, '127.0.0.1', '1195559158');
INSERT INTO `useronline` VALUES (43248, '127.0.0.1', '1195559580');
INSERT INTO `useronline` VALUES (43255, '127.0.0.1', '1195559584');
INSERT INTO `useronline` VALUES (43249, '127.0.0.1', '1195559580');
INSERT INTO `useronline` VALUES (43247, '127.0.0.1', '1195559580');
INSERT INTO `useronline` VALUES (43246, '127.0.0.1', '1195559578');
INSERT INTO `useronline` VALUES (42824, '127.0.0.1', '1195559007');
INSERT INTO `useronline` VALUES (42826, '127.0.0.1', '1195559010');
INSERT INTO `useronline` VALUES (42827, '127.0.0.1', '1195559011');
INSERT INTO `useronline` VALUES (42829, '127.0.0.1', '1195559014');
INSERT INTO `useronline` VALUES (42837, '127.0.0.1', '1195559028');
INSERT INTO `useronline` VALUES (42847, '127.0.0.1', '1195559047');
INSERT INTO `useronline` VALUES (43224, '127.0.0.1', '1195559559');
INSERT INTO `useronline` VALUES (43226, '127.0.0.1', '1195559562');
INSERT INTO `useronline` VALUES (43230, '127.0.0.1', '1195559567');
INSERT INTO `useronline` VALUES (43236, '127.0.0.1', '1195559571');
INSERT INTO `useronline` VALUES (43228, '127.0.0.1', '1195559565');
INSERT INTO `useronline` VALUES (43232, '127.0.0.1', '1195559568');
INSERT INTO `useronline` VALUES (43229, '127.0.0.1', '1195559566');
INSERT INTO `useronline` VALUES (43231, '127.0.0.1', '1195559567');
INSERT INTO `useronline` VALUES (43237, '127.0.0.1', '1195559572');
INSERT INTO `useronline` VALUES (43240, '127.0.0.1', '1195559574');
INSERT INTO `useronline` VALUES (43104, '127.0.0.1', '1195559399');
INSERT INTO `useronline` VALUES (43109, '127.0.0.1', '1195559406');
INSERT INTO `useronline` VALUES (43107, '127.0.0.1', '1195559403');
INSERT INTO `useronline` VALUES (43111, '127.0.0.1', '1195559409');
INSERT INTO `useronline` VALUES (43121, '127.0.0.1', '1195559424');
INSERT INTO `useronline` VALUES (43119, '127.0.0.1', '1195559421');
INSERT INTO `useronline` VALUES (43138, '127.0.0.1', '1195559450');
INSERT INTO `useronline` VALUES (43130, '127.0.0.1', '1195559438');
INSERT INTO `useronline` VALUES (43131, '127.0.0.1', '1195559439');
INSERT INTO `useronline` VALUES (43132, '127.0.0.1', '1195559441');
INSERT INTO `useronline` VALUES (43123, '127.0.0.1', '1195559427');
INSERT INTO `useronline` VALUES (43125, '127.0.0.1', '1195559430');
INSERT INTO `useronline` VALUES (43162, '127.0.0.1', '1195559482');
INSERT INTO `useronline` VALUES (43165, '127.0.0.1', '1195559486');
INSERT INTO `useronline` VALUES (43168, '127.0.0.1', '1195559490');
INSERT INTO `useronline` VALUES (43174, '127.0.0.1', '1195559497');
INSERT INTO `useronline` VALUES (43154, '127.0.0.1', '1195559469');
INSERT INTO `useronline` VALUES (43156, '127.0.0.1', '1195559472');
INSERT INTO `useronline` VALUES (43159, '127.0.0.1', '1195559476');
INSERT INTO `useronline` VALUES (43158, '127.0.0.1', '1195559475');
INSERT INTO `useronline` VALUES (43176, '127.0.0.1', '1195559497');
INSERT INTO `useronline` VALUES (43198, '127.0.0.1', '1195559528');
INSERT INTO `useronline` VALUES (43242, '127.0.0.1', '1195559574');
INSERT INTO `useronline` VALUES (43215, '127.0.0.1', '1195559545');
INSERT INTO `useronline` VALUES (43207, '127.0.0.1', '1195559538');
INSERT INTO `useronline` VALUES (43206, '127.0.0.1', '1195559536');
INSERT INTO `useronline` VALUES (43208, '127.0.0.1', '1195559539');
INSERT INTO `useronline` VALUES (43210, '127.0.0.1', '1195559541');
INSERT INTO `useronline` VALUES (43163, '127.0.0.1', '1195559483');
INSERT INTO `useronline` VALUES (43164, '127.0.0.1', '1195559484');
INSERT INTO `useronline` VALUES (43160, '127.0.0.1', '1195559478');
INSERT INTO `useronline` VALUES (43150, '127.0.0.1', '1195559466');
INSERT INTO `useronline` VALUES (43030, '127.0.0.1', '1195559297');
INSERT INTO `useronline` VALUES (43023, '127.0.0.1', '1195559286');
INSERT INTO `useronline` VALUES (43024, '127.0.0.1', '1195559288');
INSERT INTO `useronline` VALUES (43029, '127.0.0.1', '1195559295');
INSERT INTO `useronline` VALUES (43032, '127.0.0.1', '1195559300');
INSERT INTO `useronline` VALUES (43034, '127.0.0.1', '1195559303');
INSERT INTO `useronline` VALUES (43108, '127.0.0.1', '1195559405');
INSERT INTO `useronline` VALUES (43113, '127.0.0.1', '1195559412');
INSERT INTO `useronline` VALUES (43110, '127.0.0.1', '1195559408');
INSERT INTO `useronline` VALUES (43115, '127.0.0.1', '1195559415');
INSERT INTO `useronline` VALUES (43127, '127.0.0.1', '1195559433');
INSERT INTO `useronline` VALUES (43139, '127.0.0.1', '1195559451');
INSERT INTO `useronline` VALUES (43141, '127.0.0.1', '1195559454');
INSERT INTO `useronline` VALUES (43006, '127.0.0.1', '1195559266');
INSERT INTO `useronline` VALUES (43022, '127.0.0.1', '1195559285');
INSERT INTO `useronline` VALUES (43021, '127.0.0.1', '1195559284');
INSERT INTO `useronline` VALUES (43017, '127.0.0.1', '1195559277');
INSERT INTO `useronline` VALUES (43025, '127.0.0.1', '1195559290');
INSERT INTO `useronline` VALUES (43027, '127.0.0.1', '1195559293');
INSERT INTO `useronline` VALUES (43026, '127.0.0.1', '1195559291');
INSERT INTO `useronline` VALUES (43028, '127.0.0.1', '1195559294');
INSERT INTO `useronline` VALUES (43038, '127.0.0.1', '1195559309');
INSERT INTO `useronline` VALUES (43043, '127.0.0.1', '1195559312');
INSERT INTO `useronline` VALUES (43031, '127.0.0.1', '1195559298');
INSERT INTO `useronline` VALUES (43033, '127.0.0.1', '1195559301');
INSERT INTO `useronline` VALUES (43053, '127.0.0.1', '1195559327');
INSERT INTO `useronline` VALUES (43057, '127.0.0.1', '1195559330');
INSERT INTO `useronline` VALUES (43066, '127.0.0.1', '1195559342');
INSERT INTO `useronline` VALUES (43054, '127.0.0.1', '1195559329');
INSERT INTO `useronline` VALUES (43064, '127.0.0.1', '1195559339');
INSERT INTO `useronline` VALUES (42890, '127.0.0.1', '1195559107');
INSERT INTO `useronline` VALUES (42892, '127.0.0.1', '1195559110');
INSERT INTO `useronline` VALUES (42893, '127.0.0.1', '1195559112');
INSERT INTO `useronline` VALUES (42903, '127.0.0.1', '1195559128');
INSERT INTO `useronline` VALUES (42902, '127.0.0.1', '1195559125');
INSERT INTO `useronline` VALUES (43073, '127.0.0.1', '1195559351');
INSERT INTO `useronline` VALUES (43074, '127.0.0.1', '1195559354');
INSERT INTO `useronline` VALUES (43078, '127.0.0.1', '1195559360');
INSERT INTO `useronline` VALUES (43161, '127.0.0.1', '1195559479');
INSERT INTO `useronline` VALUES (43157, '127.0.0.1', '1195559473');
INSERT INTO `useronline` VALUES (43086, '127.0.0.1', '1195559372');
INSERT INTO `useronline` VALUES (43088, '127.0.0.1', '1195559375');
INSERT INTO `useronline` VALUES (43143, '127.0.0.1', '1195559457');
INSERT INTO `useronline` VALUES (43144, '127.0.0.1', '1195559459');
INSERT INTO `useronline` VALUES (43155, '127.0.0.1', '1195559470');
INSERT INTO `useronline` VALUES (43151, '127.0.0.1', '1195559467');
INSERT INTO `useronline` VALUES (43148, '127.0.0.1', '1195559465');
INSERT INTO `useronline` VALUES (43147, '127.0.0.1', '1195559463');
INSERT INTO `useronline` VALUES (43149, '127.0.0.1', '1195559466');
INSERT INTO `useronline` VALUES (42884, '127.0.0.1', '1195559098');
INSERT INTO `useronline` VALUES (42914, '127.0.0.1', '1195559137');
INSERT INTO `useronline` VALUES (42918, '127.0.0.1', '1195559143');
INSERT INTO `useronline` VALUES (42864, '127.0.0.1', '1195559068');
INSERT INTO `useronline` VALUES (42857, '127.0.0.1', '1195559058');
INSERT INTO `useronline` VALUES (42858, '127.0.0.1', '1195559059');
INSERT INTO `useronline` VALUES (42859, '127.0.0.1', '1195559063');
INSERT INTO `useronline` VALUES (42861, '127.0.0.1', '1195559064');
INSERT INTO `useronline` VALUES (42876, '127.0.0.1', '1195559087');
INSERT INTO `useronline` VALUES (42886, '127.0.0.1', '1195559101');
INSERT INTO `useronline` VALUES (42889, '127.0.0.1', '1195559106');
INSERT INTO `useronline` VALUES (42885, '127.0.0.1', '1195559100');
INSERT INTO `useronline` VALUES (42874, '127.0.0.1', '1195559084');
INSERT INTO `useronline` VALUES (42860, '127.0.0.1', '1195559063');
INSERT INTO `useronline` VALUES (42871, '127.0.0.1', '1195559079');
INSERT INTO `useronline` VALUES (42866, '127.0.0.1', '1195559072');
INSERT INTO `useronline` VALUES (42870, '127.0.0.1', '1195559077');
INSERT INTO `useronline` VALUES (42867, '127.0.0.1', '1195559073');
INSERT INTO `useronline` VALUES (42873, '127.0.0.1', '1195559083');
INSERT INTO `useronline` VALUES (42879, '127.0.0.1', '1195559091');
INSERT INTO `useronline` VALUES (42877, '127.0.0.1', '1195559089');
INSERT INTO `useronline` VALUES (42878, '127.0.0.1', '1195559089');
INSERT INTO `useronline` VALUES (42882, '127.0.0.1', '1195559095');
INSERT INTO `useronline` VALUES (42881, '127.0.0.1', '1195559094');
INSERT INTO `useronline` VALUES (42883, '127.0.0.1', '1195559097');
INSERT INTO `useronline` VALUES (42887, '127.0.0.1', '1195559103');
INSERT INTO `useronline` VALUES (42888, '127.0.0.1', '1195559104');
INSERT INTO `useronline` VALUES (42896, '127.0.0.1', '1195559116');
INSERT INTO `useronline` VALUES (42895, '127.0.0.1', '1195559115');
INSERT INTO `useronline` VALUES (42894, '127.0.0.1', '1195559113');
INSERT INTO `useronline` VALUES (42891, '127.0.0.1', '1195559109');
INSERT INTO `useronline` VALUES (42898, '127.0.0.1', '1195559119');
INSERT INTO `useronline` VALUES (42900, '127.0.0.1', '1195559122');
INSERT INTO `useronline` VALUES (42942, '127.0.0.1', '1195559180');
INSERT INTO `useronline` VALUES (42941, '127.0.0.1', '1195559178');
INSERT INTO `useronline` VALUES (42932, '127.0.0.1', '1195559164');
INSERT INTO `useronline` VALUES (42931, '127.0.0.1', '1195559162');
INSERT INTO `useronline` VALUES (42935, '127.0.0.1', '1195559168');
INSERT INTO `useronline` VALUES (42937, '127.0.0.1', '1195559171');
INSERT INTO `useronline` VALUES (42940, '127.0.0.1', '1195559177');
INSERT INTO `useronline` VALUES (42949, '127.0.0.1', '1195559185');
INSERT INTO `useronline` VALUES (42952, '127.0.0.1', '1195559190');
INSERT INTO `useronline` VALUES (42953, '127.0.0.1', '1195559191');
INSERT INTO `useronline` VALUES (42954, '127.0.0.1', '1195559193');
INSERT INTO `useronline` VALUES (42956, '127.0.0.1', '1195559196');
INSERT INTO `useronline` VALUES (42957, '127.0.0.1', '1195559198');
INSERT INTO `useronline` VALUES (42963, '127.0.0.1', '1195559204');
INSERT INTO `useronline` VALUES (43019, '127.0.0.1', '1195559280');
INSERT INTO `useronline` VALUES (42958, '127.0.0.1', '1195559200');
INSERT INTO `useronline` VALUES (42973, '127.0.0.1', '1195559216');
INSERT INTO `useronline` VALUES (42975, '127.0.0.1', '1195559219');
INSERT INTO `useronline` VALUES (42978, '127.0.0.1', '1195559224');
INSERT INTO `useronline` VALUES (42981, '127.0.0.1', '1195559228');
INSERT INTO `useronline` VALUES (42993, '127.0.0.1', '1195559246');
INSERT INTO `useronline` VALUES (43037, '127.0.0.1', '1195559307');
INSERT INTO `useronline` VALUES (43035, '127.0.0.1', '1195559305');
INSERT INTO `useronline` VALUES (43040, '127.0.0.1', '1195559310');
INSERT INTO `useronline` VALUES (42959, '127.0.0.1', '1195559201');
INSERT INTO `useronline` VALUES (42964, '127.0.0.1', '1195559204');
INSERT INTO `useronline` VALUES (43001, '127.0.0.1', '1195559258');
INSERT INTO `useronline` VALUES (42976, '127.0.0.1', '1195559221');
INSERT INTO `useronline` VALUES (42992, '127.0.0.1', '1195559245');
INSERT INTO `useronline` VALUES (43036, '127.0.0.1', '1195559306');
INSERT INTO `useronline` VALUES (43039, '127.0.0.1', '1195559310');
INSERT INTO `useronline` VALUES (42971, '127.0.0.1', '1195559213');
INSERT INTO `useronline` VALUES (42972, '127.0.0.1', '1195559215');
INSERT INTO `useronline` VALUES (43005, '127.0.0.1', '1195559264');
INSERT INTO `useronline` VALUES (43013, '127.0.0.1', '1195559271');
INSERT INTO `useronline` VALUES (43000, '127.0.0.1', '1195559257');
INSERT INTO `useronline` VALUES (43003, '127.0.0.1', '1195559261');
INSERT INTO `useronline` VALUES (43007, '127.0.0.1', '1195559267');
INSERT INTO `useronline` VALUES (42988, '127.0.0.1', '1195559239');
INSERT INTO `useronline` VALUES (42989, '127.0.0.1', '1195559240');
INSERT INTO `useronline` VALUES (42974, '127.0.0.1', '1195559218');
INSERT INTO `useronline` VALUES (42999, '127.0.0.1', '1195559255');
INSERT INTO `useronline` VALUES (42997, '127.0.0.1', '1195559252');
INSERT INTO `useronline` VALUES (42998, '127.0.0.1', '1195559254');
INSERT INTO `useronline` VALUES (42987, '127.0.0.1', '1195559237');
INSERT INTO `useronline` VALUES (42979, '127.0.0.1', '1195559225');
INSERT INTO `useronline` VALUES (42982, '127.0.0.1', '1195559230');
INSERT INTO `useronline` VALUES (42986, '127.0.0.1', '1195559236');
INSERT INTO `useronline` VALUES (43050, '127.0.0.1', '1195559322');
INSERT INTO `useronline` VALUES (42991, '127.0.0.1', '1195559243');
INSERT INTO `useronline` VALUES (42983, '127.0.0.1', '1195559231');
INSERT INTO `useronline` VALUES (42977, '127.0.0.1', '1195559222');
INSERT INTO `useronline` VALUES (42980, '127.0.0.1', '1195559227');
INSERT INTO `useronline` VALUES (42990, '127.0.0.1', '1195559242');
INSERT INTO `useronline` VALUES (43052, '127.0.0.1', '1195559325');
INSERT INTO `useronline` VALUES (43055, '127.0.0.1', '1195559329');
INSERT INTO `useronline` VALUES (43056, '127.0.0.1', '1195559330');
INSERT INTO `useronline` VALUES (43098, '127.0.0.1', '1195559390');
INSERT INTO `useronline` VALUES (43106, '127.0.0.1', '1195559402');
INSERT INTO `useronline` VALUES (43093, '127.0.0.1', '1195559382');
INSERT INTO `useronline` VALUES (43097, '127.0.0.1', '1195559388');
INSERT INTO `useronline` VALUES (43102, '127.0.0.1', '1195559396');
INSERT INTO `useronline` VALUES (43105, '127.0.0.1', '1195559400');
INSERT INTO `useronline` VALUES (43116, '127.0.0.1', '1195559417');
INSERT INTO `useronline` VALUES (43081, '127.0.0.1', '1195559364');
INSERT INTO `useronline` VALUES (43061, '127.0.0.1', '1195559333');
INSERT INTO `useronline` VALUES (43063, '127.0.0.1', '1195559336');
INSERT INTO `useronline` VALUES (43092, '127.0.0.1', '1195559382');
INSERT INTO `useronline` VALUES (43090, '127.0.0.1', '1195559378');
INSERT INTO `useronline` VALUES (43083, '127.0.0.1', '1195559367');
INSERT INTO `useronline` VALUES (43077, '127.0.0.1', '1195559358');
INSERT INTO `useronline` VALUES (43069, '127.0.0.1', '1195559345');
INSERT INTO `useronline` VALUES (43065, '127.0.0.1', '1195559339');
INSERT INTO `useronline` VALUES (43112, '127.0.0.1', '1195559411');
INSERT INTO `useronline` VALUES (43120, '127.0.0.1', '1195559423');
INSERT INTO `useronline` VALUES (43145, '127.0.0.1', '1195559460');
INSERT INTO `useronline` VALUES (43146, '127.0.0.1', '1195559463');
INSERT INTO `useronline` VALUES (43114, '127.0.0.1', '1195559414');
INSERT INTO `useronline` VALUES (43118, '127.0.0.1', '1195559420');
INSERT INTO `useronline` VALUES (42856, '127.0.0.1', '1195559056');
INSERT INTO `useronline` VALUES (42848, '127.0.0.1', '1195559049');
INSERT INTO `useronline` VALUES (42849, '127.0.0.1', '1195559050');
INSERT INTO `useronline` VALUES (42843, '127.0.0.1', '1195559041');
INSERT INTO `useronline` VALUES (43117, '127.0.0.1', '1195559418');
INSERT INTO `useronline` VALUES (43135, '127.0.0.1', '1195559445');
INSERT INTO `useronline` VALUES (43137, '127.0.0.1', '1195559448');
INSERT INTO `useronline` VALUES (42842, '127.0.0.1', '1195559040');
INSERT INTO `useronline` VALUES (42841, '127.0.0.1', '1195559038');
INSERT INTO `useronline` VALUES (43122, '127.0.0.1', '1195559426');
INSERT INTO `useronline` VALUES (42839, '127.0.0.1', '1195559034');
INSERT INTO `useronline` VALUES (42840, '127.0.0.1', '1195559037');
INSERT INTO `useronline` VALUES (42835, '127.0.0.1', '1195559023');
INSERT INTO `useronline` VALUES (42836, '127.0.0.1', '1195559025');
INSERT INTO `useronline` VALUES (42838, '127.0.0.1', '1195559031');
INSERT INTO `useronline` VALUES (43129, '127.0.0.1', '1195559436');
INSERT INTO `useronline` VALUES (42832, '127.0.0.1', '1195559019');
INSERT INTO `useronline` VALUES (42830, '127.0.0.1', '1195559016');
INSERT INTO `useronline` VALUES (42821, '127.0.0.1', '1195559002');
INSERT INTO `useronline` VALUES (43126, '127.0.0.1', '1195559432');
INSERT INTO `useronline` VALUES (43124, '127.0.0.1', '1195559429');

-- --------------------------------------------------------

-- 
-- Table structure for table `users`
-- 

CREATE TABLE `users` (
  `id_users` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `username` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `password` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `email` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `telephone` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `lastvisit` bigint(20) unsigned NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '0',
  `super` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id_users`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- 
-- Dumping data for table `users`
-- 

INSERT INTO `users` VALUES (7, 'Nguy&#7877;n V&#259;n B&#236;nh', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'binh_mczk@yahoo.com', '0912512449', 1195556337, 1, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `user_module`
-- 

CREATE TABLE `user_module` (
  `id_user_module` bigint(20) unsigned NOT NULL auto_increment,
  `id_user` bigint(20) unsigned NOT NULL default '0',
  `id_module` bigint(20) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id_user_module`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `user_module`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `user_post`
-- 

CREATE TABLE `user_post` (
  `id_user` bigint(20) unsigned NOT NULL auto_increment,
  `name` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `username` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `password` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `email` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `telephone` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `lastvisit` bigint(20) unsigned NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '0',
  `address` text character set latin1 collate latin1_general_ci NOT NULL,
  `other` text character set latin1 collate latin1_general_ci NOT NULL,
  `area` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `nhanmail` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

-- 
-- Dumping data for table `user_post`
-- 

INSERT INTO `user_post` VALUES (3, 'Nguyá»…n VÄƒn BÃ¬nh', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'binh_mczk@yahoo.com', '091-251-2449', 0, 1, 'Ha noi', 'khong co gi', '0', 1);
INSERT INTO `user_post` VALUES (4, 'nguyen van binh', 'nguyenbinh', 'a745715b2ade66190a65e1710b7830c7', 'suongmumc@yahoo.com', '091-251-2449', 0, 1, 'cau giay ha noi', 'chang co thong tin gi them', '0', 0);
INSERT INTO `user_post` VALUES (7, 'Nguyá»…n VÄƒn BÃ¬nh', 'admin1', 'e00cf25ad42683b3df678c61f42c6bda', 'suongmumc@gmail.com', '091-251-2449', 0, 1, 'df sdÃ ', 'd fsd f', '', 0);
INSERT INTO `user_post` VALUES (8, 'kkkk', 'cuoglm', '1', 'cuoglm@gmail.com', '1233333', 0, 1, 'mmm', '', '', 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `yahoo`
-- 

CREATE TABLE `yahoo` (
  `id_yahoo` tinyint(4) NOT NULL auto_increment,
  `nic` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `name` varchar(255) character set latin1 collate latin1_general_ci NOT NULL default '',
  `thu_tu` tinyint(4) NOT NULL default '0',
  `active` tinyint(4) NOT NULL default '0',
  `sky` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id_yahoo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `yahoo`
-- 

