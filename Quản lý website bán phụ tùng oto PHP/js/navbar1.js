//NavBar
function navBar( tableCellRef, hoverFlag, navStyle ) {
	if ( hoverFlag ) {
		switch ( navStyle ) {
			case 1:
//This defines rollover color
				tableCellRef.style.backgroundColor = '#EBEBEB';
				break;
			default:
//tableCellRef.style.backgroundColor = '#ccc';
				if ( document.getElementsByTagName ) {
					tableCellRef.getElementsByTagName( 'a' )[0].style.color = '#fff000';
				}
		}
	} else {
		switch ( navStyle ) {
			case 1:
//This defines return color
				tableCellRef.style.backgroundColor = '';
				break;
			default:
//tableCellRef.style.backgroundColor = '#ddd';
				if ( document.getElementsByTagName ) {
					tableCellRef.getElementsByTagName( 'a' )[0].style.color = '#00ff89';
				}
		}
	}
}
