﻿<?php
function show_order_list_h()
{
	echo "<table border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse; font-family: Arial; font-size: 10pt; color: #000099' bordercolor='#9999FF' width='100%' id='AutoNumber1'>
			  
			  <tr>
			  	<td>
			  ";
	echo "<table border='1' cellpadding='0' cellspacing='0' style='border-collapse: collapse; font-family: Arial; font-size: 10pt; color: #000080' bordercolor='#9999FF' width='100%' id='AutoNumber1'>";
	echo "
	<tr>
				<td width='100%' class='header' background='imagesaddm/title2.png' height='25' colspan='10'>&nbsp;<img src='imagesaddm/book.gif' width='18' height='15'>&nbsp;
				Danh s&#225;ch &#273;&#417;n &#273;&#7863;t h&#224;ng
			  </tr>
	<tr>
	<td width='15' class='header2' align='center'><strong>STT</strong></td>
	<td class='header2'width='70%'>&nbsp;<strong>Ng&#432;&#7901;i &#273;&#7863;t h&#224;ng</strong></td>
	<td width='90' align='center' class='header2'><strong>Ng&#224;y</strong></td>
	<td width='50' class='header2'>&nbsp;</td>
	</tr>
	";
	
}

function show_order_list_f()
{
	echo "</table></td></tr></table>";
}
function show_order_cell($info)
{
	echo "
	<tr onmouseover=navBar(this,1,1) onmouseout=navBar(this,0,1)>
	<td align='center'>".$info['stt']."</td>
	<td>&nbsp;<a href='main.php?act=order&code=01&id=".$info['id_order']."'>".$info['name']."</a></td>
	<td width='90' align='center'>".$info['time']."</td>
	<td width='50' align='center'><a href='javascript:confirmdelete(".$info['id_order'].")'>Delete</a></td>
	</tr>
	";
	
}
function show_order_info($info)
{
	return "
	<table border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse; font-family: Arial; font-size: 10pt; color: #000099' bordercolor='#9999FF' width='100%' id='AutoNumber1'>
			  
			  <tr>
			  	<td>
	<table border='1' cellpadding='0' cellspacing='0' style='border-collapse: collapse; font-family: Arial; font-size: 10pt; color: #000080' bordercolor='#9999FF' width='100%' id='AutoNumber1'>
	<tr>
				<td width='100%' class='header' background='imagesaddm/title2.png' height='25'>&nbsp;<img src='imagesaddm/book.gif' width='18' height='15'>&nbsp;
				Chi ti&#7871;t &#273;&#417;n &#273;&#7863;t h&#224;ng
			  </tr>
	<tr>
		<td align='right'><a href='javascript:confirmdelete(".$info['id_order'].")'>Delete</a></td>
	</tr>
	<tr>
		<td class='header3'><b>Th&#244;ng tin kh&#225;ch h&#224;ng</b></td>
	</tr>
	<tr>
		<td align='center'><br>
			<table border='1' cellpadding='5' cellspacing='0' style='border-collapse: collapse; font-family: Arial; font-size: 10pt; color: #000080' bordercolor='#9999FF' width='95%'>
				<tr>
					<td width='100'>Họ tên:</td>
					<td>".$info['cusname']."</td>
				</tr>
				<tr>
					<td>Điện thoại:</td>
					<td>".$info['custel']."</td>
				</tr>	
				<tr>
					<td>Email:</td>
					<td>".$info['cusemail']."</td>
				</tr>	
				<tr>
					<td>Địa chỉ:</td>
					<td>".$info['cusaddress']."</td>
				</tr>	
				<tr>
					<td valign='top'>Thông tin thêm:</td>
					<td>".$info['cusaddinfo']."</td>
				</tr>	
			</table>
			<br>
		</td>
	</tr>
	<tr>
		<td class='header3'><b>Đặt hàng:</b></td>
	</tr>
	<tr>
		<td><br>
		  <table border='0' cellspacing='0' cellpadding='0' width='100%' align='center'>
			<tr> 
			  <td align='center'>		
				<table border='1' cellpadding='5' cellspacing='0' style='border-collapse: collapse; font-family: Arial; font-size: 10pt; color: #000080' bordercolor='#9999FF' width='95%'>
				  <tr>
					
					<td class='header2'><b>Sản phẩm</b></td>
					<td class='header2' width='14%' align='center'><b>Đơn giá</b></td>
					<td class='header2' width='14%' align='center'><b>Số lượng</b></td>
					<td class='header2' width='14%' align='center'><b>Tổng tiền</b></td>
				  </tr>		
				".$info['cart']."
				  <tr>
								<td>&nbsp;</td>
								<td bgcolor='#FFFFFF' align='right' colspan='2'><b>Tổng cộng:</b></td>
					<td bgcolor='#FFFFFF' align='right'>".number_format($info['grandtotal'])."</td>
				  </tr>
				</table>
			  </td>
			</tr>
		  </table>
			<br>
		</td>
	</tr>
	</table> 
	</td></tr></table><br><br>
	";
}
function show_cart_row_order($info)
{
	return "
              <tr height='30'>
			    <td bgcolor='#FFFFFF'>".$info['prod_name']."</td>
				<td bgcolor='#FFFFFF' align='right'>".number_format($info['price'])."</td>
				<td bgcolor='#FFFFFF' align='center'>".$info['quantity']."</td>
				<td bgcolor='#FFFFFF' align='right'>".number_format($info['total'])."</td>
			  </tr>
			";
}

?>