<?php 
defined( '_VALID_NVB' ) or die( 'Direct Access to this location is not allowed.' );

$id=intval($_GET['id']);
$idc=intval($_GET['idc']);

include("lib/news.php");
$tpl=new TemplatePower("template/news.htm");
$tpl->prepare();
if(!$_GET['code']){
	news_default(0);
}elseif($_GET['code']=='show_cat'){
	Show_cat($idc);
}elseif($_GET['code']=='detail'){
	Detail($id,$idc);
}


$tpl->printToScreen();

?>