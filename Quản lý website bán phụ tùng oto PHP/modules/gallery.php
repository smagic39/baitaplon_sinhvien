<?php 
defined( '_VALID_NVB' ) or die( 'Direct Access to this location is not allowed.' );
$id=intval($_GET['id']);
$idc=intval($_GET['idc']);

if(!$_GET['code']){
	if(!$_GET['idc']) $idc=0;
	gallery_default($idc);
} elseif($_GET['code']=='show_cat'){
	show_cat($idc,18);
} elseif($_GET['code']=='detail'){
	detail($id);
}

function gallery_default($idc=0){
	global $DB, $CONFIG;
	$tpl=new TemplatePower("template/gallery.htm");
	$tpl->prepare();
	$sql="SELECT * FROM catpt WHERE active=1 AND parentid=$idc ORDER BY thu_tu DESC, name ASC";
	$db=$DB->query($sql);
	$count=mysql_num_rows($db);
	if($count>1){
		while($rs=mysql_fetch_array($db)){
			$tpl->newBlock("cat_gallery");
			$tpl->assign("cat_name",$rs['name']);
			$tpl->assign("link_cat","?page=gallery&code=show_cat&idc=$rs[id_catpt]");
			$sql1="SELECT * FROM photo WHERE active=1 AND id_catpt=$rs[id_catpt] ORDER BY thu_tu DESC, name ASC LIMIT 0,3";
			$db1=$DB->query($sql1);
			while($rs1=mysql_fetch_array($db1)){
				$tpl->newBlock("gallery");
				$tpl->assign("photo_name",$rs1['name']);
				$tpl->assign("id",$rs1['id_photo']);
				if ($rs1['small_image']){ 
					if ($rs1['small_image'] && file_exists($CONFIG['upload_image_path'].$rs1['small_image'])){
						$size=getimagesize($CONFIG['upload_image_path'].$rs1['small_image']);
						if($size[0]>$size['1']){
							$tpl->assign("image","<img hspace='5' vspace='5' width='130' src='".$CONFIG['upload_image_path'].$rs1['small_image']."' align='center'  border='0'>");
						} elseif($size[1]>$size[0]){
							$tpl->assign("image","<img hspace='5' vspace='5' height='130' src='".$CONFIG['upload_image_path'].$rs1['small_image']."' align='center'  border='0'>");
						}
					}
				}
				$tpl->assign("full",$CONFIG['upload_image_path'].$rs1['image']);
				$tpl->assign("content",$rs1['intro']);
			}
			
			
			
		}
	}else {
		$sql="SELECT * FROM catpt WHERE active=1 AND parentid=$idc";
		$db=$DB->query($sql);
		if($rs=mysql_fetch_array($db)){
			show_cat($rs['id_catpt'],24,"?page=gallery&idc=$idc");
		}else {
			show_cat($idc);
		}
	}
	$tpl->printToScreen();
}

function show_cat($idc,$max=24){
	global $DB, $CONFIG;
	
	$tpl=new TemplatePower("template/gallery_cat.htm");
	$tpl->prepare();
	
	$sql="SELECT * FROM catpt WHERE active=1 AND id_catpt=$idc";
	$db=$DB->query($sql);
	if($rs1=mysql_fetch_array($db)){
		$tpl->assign("cat_name",$rs1['name']);
	}
	
	$sql="SELECT * FROM photo WHERE active=1 AND id_catpt=$idc ORDER BY thu_tu ASC, id_photo DESC";
	$link="?page=gallery&code=show_cat&idc=$idc";
	$maxp=$max;
	include("lib/mos_nava.php");
	$i=0;
	while($rs=mysql_fetch_array($rs_list)){
		$i++;
		$tpl->newBlock("show_cat");
		$tpl->assign("photo_name",$rs['name']);
		$tpl->assign("intro",$rs['intro']);
		$tpl->assign("anh",$rs['image']);
		$tpl->assign("link_detail","?page=gallery&code=detail&idc=$rs[id_catpt]&id=$rs[id_photo]");
		$tpl->assign("id",$rs['id_photo']);
		if ($rs['small_image']){ 
			if ($rs['small_image'] && file_exists($CONFIG['upload_image_path'].$rs['small_image'])){
				$size=getimagesize($CONFIG['upload_image_path'].$rs['small_image']);
				if($size[0]>$size['1']){
					$tpl->assign("image","<img hspace='5' vspace='5' width='130' src='".$CONFIG['upload_image_path'].$rs['small_image']."' align='center'  border='0'>");
					$tpl->assign("url",$CONFIG['upload_image_path'].$rs['image']);
				} elseif($size[1]>$size[0]){
					$tpl->assign("image","<img hspace='5' vspace='5' height='130' src='".$CONFIG['upload_image_path'].$rs['small_image']."' align='center'  border='0'>");
					$tpl->assign("url",$CONFIG['upload_image_path'].$rs['image']);
				} else {
					$tpl->assign("image","<img hspace='5' vspace='5' src='".$CONFIG['upload_image_path'].$rs['small_image']."' align='center'  border='0'>");
					$tpl->assign("url",$CONFIG['upload_image_path'].$rs['image']);
				}
			}
		}
		
		if($i>=3){
			$tpl->assign("br","<tr><td height='10'></td></tr>");
			$i=0;
		}
	}
	$tpl->printToScreen();
}

function detail($id){
	global $DB, $CONFIG;

	$tpl=new TemplatePower("template/gallery_detail.htm");
	$tpl->prepare();
	$sql="SELECT * FROM photo WHERE id_photo=$id";
	$db=$DB->query($sql);
	if($rs=mysql_fetch_array($db)){
		$tpl->newBlock("detail");
		$tpl->assign("photo_name",$rs['name']);
		$tpl->assign("intro",$rs['intro']);
		/*
		if ($rs['normal_image']){ 
			if ($rs['normal_image'] && file_exists($CONFIG['upload_image_path'].$rs['normal_image'])){
				$tpl->assign("image","<img hspace='5' vspace='0'  src='".$CONFIG['upload_image_path'].$rs['normal_image']."' align='left'  border='0'>");
			}
		}
		if ($rs['small_image']){ 
			if ($rs['small_image'] && file_exists($CONFIG['upload_image_path'].$rs['small_image'])){
				$tpl->assign("image","<img hspace='5' vspace='0'  src='".$CONFIG['upload_image_path'].$rs['small_image']."' align='left'  border='0'>");
			}
		}*/
		if ($rs['image']){ 
			if ($rs['image'] && file_exists($CONFIG['upload_image_path'].$rs['image'])){
				$size=getimagesize($CONFIG['upload_image_path'].$rs['image']);
				if($size[0]>500){
					$tpl->assign("image","<img hspace='0' vspace='0'  src='".$CONFIG['upload_image_path'].$rs['image']."' align='center'  border='0' width='500'>");
				}else {
					$tpl->assign("image","<img hspace='0' vspace='0'  src='".$CONFIG['upload_image_path'].$rs['image']."' align='center'  border='0'>");
				}
			}
		}
	}
	$tpl->printToScreen();
}

?>