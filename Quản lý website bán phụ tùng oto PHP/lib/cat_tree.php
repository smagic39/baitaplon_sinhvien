<?php
defined( '_VALID_NVB' ) or die( 'Direct Access to this location is not allowed.' );
class cat_tree{
	var $n;
	var $current_idcat;
	var $navcat;
	var $subcatlist;
	function cat_tree($current_idcat=0)
	{
		$this->current_idcat=$current_idcat;
		$this->navcat=array();
		$this->subcatlist=array();
	}
	function get_cat_tree($parent = 0)
	{
		global $DB,$tree; // add $cat_old
		$raw = $DB->query("select * from cat where parentid='$parent' order by thu_tu asc,id_cat desc");
		// add -- if it has childs
		if ($DB->get_affected_rows() > 0) {
			$this->n++;
		} else {
			return;
		} while ($result = mysql_fetch_array($raw)) {
		/*
			if ($result['pcatid'] == $childcat_old['pcatid']) {
				continue; // remove  cats from list
			}
			*/
			for($i = 0;$i < $this->n;$i++) {
				$tree[$result['id_cat']]['name'] .= '-- ';
			}
			$tree[$result['id_cat']]['name'] .= $result['name'];
			$this->get_cat_tree($result['id_cat']);
		}
		// all childs listed, remove --
		$this->n--;
	}
	function get_cat_string($id_cat)
	{
		global $DB,$catstring;
		if ($id_cat==0)
			return;
		else
		{
			$sql="select * from cat where id_cat=".$id_cat;
			$a=$DB->query($sql);
			if ($b=mysql_fetch_array($a))
			{
				$catstring = $b['name']." > ".$catstring;
				$this->get_cat_string($b['parentid']);
			}
		}
	
	}
	function get_cat_string_admin($id_cat)
	{
		global $DB,$catstring2;
		if ($id_cat==0)
			return;
		else
		{
			if ($this->current_idcat==$id_cat)
			{
				$showclass="class='currentcat'";
			}
			else
			{
				$showclass="";
			}
			$sql="select * from cat where id_cat=".$id_cat;
			$a=$DB->query($sql);
			if ($b=mysql_fetch_array($a))
			{
				$catstring2 = "<a href='main.php?act=cat&pid=".$id_cat."' $showclass>".$b['name']."</a> > ".$catstring2;
				$this->get_cat_string_admin($b['parentid']);
			}
		}
	
	}
	function get_cat_nav($id_cat)
	{
		global $DB;
		if ($id_cat==0)
		{
			ksort($this->navcat);
			return;
		}
		else
		{
			$sql="select * from cat where id_cat=".$id_cat;
			$a=$DB->query($sql);
			if ($b=mysql_fetch_array($a))
			{
				$this->navcat[$b['id_cat']]['name']=$b['name'];
				$this->get_cat_nav($b['parentid']);
			}
		}
	
	}
	function get_sub_cat_list($id_cat)	
	{
		global $DB;
		$raw = $DB->query("select * from cat where parentid='$id_cat' order by thu_tu asc,id_cat desc");
		if ($DB->get_affected_rows() == 0) {
			return;
		} 
		while ($result = mysql_fetch_array($raw)) {
			$this->subcatlist[$result['id_cat']]['name'] = $result['name'];
			$this->get_sub_cat_list($result['id_cat']);
		}
	}


}
class catpt_tree{
	var $n;
	var $current_idcatpt;
	function catpt_tree($current_idcatpt=0)
	{
		$this->current_idcatpt=$current_idcatpt;
	}
	function get_catpt_tree($parent = 0)
	{
		global $DB,$tree; // add $catpt_old
		$raw = $DB->query("select * from catpt where parentid='$parent' order by id_catpt asc");
		// add -- if it has childs
		if ($DB->get_affected_rows() > 0) {
			$this->n++;
		} else {
			return;
		} while ($result = mysql_fetch_array($raw)) {
		/*
			if ($result['pcatptid'] == $childcatpt_old['pcatptid']) {
				continue; // remove  catpts from list
			}
			*/
			for($i = 0;$i < $this->n;$i++) {
				$tree[$result['id_catpt']]['name'] .= '-- ';
			}
			$tree[$result['id_catpt']]['name'] .= $result['name'];
			$this->get_catpt_tree($result['id_catpt']);
		}
		// all childs listed, remove --
		$this->n--;
	}
	function get_catpt_string($id_catpt)
	{
		global $DB,$catptstring;
		if ($id_catpt==0)
			return;
		else
		{
			$sql="select * from catpt where id_catpt=".$id_catpt;
			$a=$DB->query($sql);
			if ($b=mysql_fetch_array($a))
			{
				$catptstring = $b['name']." > ".$catptstring;
				$this->get_catpt_string($b['parentid']);
			}
		}
	
	}
	function get_catpt_string_admin($id_catpt)
	{
		global $DB,$catptstring2;
		if ($id_catpt==0)
			return;
		else
		{
			if ($this->current_idcatpt==$id_catpt)
			{
				$showclass="class='currentcat'";
			}
			else
			{
				$showclass="";
			}
			$sql="select * from catpt where id_catpt=".$id_catpt;
			$a=$DB->query($sql);
			if ($b=mysql_fetch_array($a))
			{
				$catptstring2 = "<a href='main.php?act=catpt&pid=".$id_catpt."' $showclass>".$b['name']."</a> > ".$catptstring2;
				$this->get_catpt_string_admin($b['parentid']);
			}
		}
	
	}	


}
class catrv_tree{
	var $n;
	var $current_idcatrv;
	var $navcatrv;
	var $subcatrvlist;
	function catrv_tree($current_idcatrv=0)
	{
		$this->current_idcatrv=$current_idcatrv;
		$this->navcatrv=array();
		$this->subcatrvlist=array();
	}
	function get_catrv_tree($parent = 0)
	{
		global $DB,$tree; // add $catrv_old
		$raw = $DB->query("select * from catrv where parentid='$parent' order by thu_tu asc,id_catrv desc");
		// add -- if it has childs
		if ($DB->get_affected_rows() > 0) {
			$this->n++;
		} else {
			return;
		} while ($result = mysql_fetch_array($raw)) {
		/*
			if ($result['pcatrvid'] == $childcatrv_old['pcatrvid']) {
				continue; // remove  catrvs from list
			}
			*/
			for($i = 0;$i < $this->n;$i++) {
				$tree[$result['id_catrv']]['name'] .= '-- ';
			}
			$tree[$result['id_catrv']]['name'] .= $result['name'];
			$this->get_catrv_tree($result['id_catrv']);
		}
		// all childs listed, remove --
		$this->n--;
	}
	function get_catrv_string($id_catrv)
	{
		global $DB,$catrvstring;
		if ($id_catrv==0)
			return;
		else
		{
			$sql="select * from catrv where id_catrv=".$id_catrv;
			$a=$DB->query($sql);
			if ($b=mysql_fetch_array($a))
			{
				$catrvstring = $b['name']." > ".$catrvstring;
				$this->get_catrv_string($b['parentid']);
			}
		}
	
	}
	function get_catrv_string_admin($id_catrv)
	{
		global $DB,$catrvstring2;
		if ($id_catrv==0)
			return;
		else
		{
			if ($this->current_idcatrv==$id_catrv)
			{
				$showclass="class='currentcatrv'";
			}
			else
			{
				$showclass="";
			}
			$sql="select * from catrv where id_catrv=".$id_catrv;
			$a=$DB->query($sql);
			if ($b=mysql_fetch_array($a))
			{
				$catrvstring2 = "<a href='main.php?act=catrv&pid=".$id_catrv."' $showclass>".$b['name']."</a> > ".$catrvstring2;
				$this->get_catrv_string_admin($b['parentid']);
			}
		}
	
	}
	function get_catrv_nav($id_catrv)
	{
		global $DB;
		if ($id_catrv==0)
		{
			ksort($this->navcatrv);
			return;
		}
		else
		{
			$sql="select * from catrv where id_catrv=".$id_catrv;
			$a=$DB->query($sql);
			if ($b=mysql_fetch_array($a))
			{
				$this->navcatrv[$b['id_catrv']]['name']=$b['name'];
				$this->get_catrv_nav($b['parentid']);
			}
		}
	
	}
	function get_sub_catrv_list($id_catrv)	
	{
		global $DB;
		$raw = $DB->query("select * from catrv where parentid='$id_catrv' order by thu_tu asc,id_catrv desc");
		if ($DB->get_affected_rows() == 0) {
			return;
		} 
		while ($result = mysql_fetch_array($raw)) {
			$this->subcatrvlist[$result['id_catrv']]['name'] = $result['name'];
			$this->get_sub_catrv_list($result['id_catrv']);
		}
	}


}
?>