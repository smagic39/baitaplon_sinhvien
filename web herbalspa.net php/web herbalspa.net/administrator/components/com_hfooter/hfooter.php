<?php
/*------------------------------------------------------------------------
# hfooter.php - herbal footer Component
# ------------------------------------------------------------------------
# author    vuguyen
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   ruahinh.vn
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_hfooter')){
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
};

// require helper files
JLoader::register('HfooterHelper', dirname(__FILE__) . DS . 'helpers' . DS . 'hfooter.php');

// import joomla controller library
jimport('joomla.application.component.controller');

// Add CSS file for all pages
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_hfooter/assets/css/hfooter.css');
$document->addScript('components/com_hfooter/assets/js/hfooter.js');

// Get an instance of the controller prefixed by Hfooter
$controller = JController::getInstance('Hfooter');

// Perform the Request task
$controller->execute(JRequest::getCmd('task'));

// Redirect if set by the controller
$controller->redirect();

?>