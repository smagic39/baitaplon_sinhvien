<?php
/*------------------------------------------------------------------------
# hbannerzero.php - hbanner Component
# ------------------------------------------------------------------------
# author    VuNguyen
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   ruahinh.vn
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import the list field type
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');

/**
 * banner Form Field class for the Hbanner component
 */
class JFormFieldhbannerzero extends JFormFieldList
{
	/**
	 * The banner field type.
	 *
	 * @var		string
	 */
	protected $type = 'hbannerzero';

	/**
	 * Method to get a list of options for a list input.
	 *
	 * @return	array		An array of JHtml options.
	 */
	protected function getOptions()
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('#__hbanner_edit.id as id, #__hbanner_edit.banner as banner');
		$query->from('#__hbanner_edit');
		$db->setQuery((string)$query);
		$items = $db->loadObjectList();
		$options = array();
		if($items){
			foreach($items as $item){
				$options[] = JHtml::_('select.option', $item->id, ucwords($item->banner));
			};
		};
		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}
?>