<?php
/*------------------------------------------------------------------------
# default.php - san pham Component
# ------------------------------------------------------------------------
# author    VuNguyen
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   ruahinh.vn
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

?>
<div id="hsanpham-content">
	<p><strong>Name</strong>: <?php echo $this->item->name; ?></p>
	<p><strong>Category</strong>: <?php echo $this->item->category; ?></p>
	<p><strong>Description</strong>: <?php echo $this->item->description; ?></p>
	<p><strong>Images</strong>: <?php echo $this->item->images; ?></p>
	<p><strong>Price</strong>: <?php echo $this->item->price; ?></p>
</div>