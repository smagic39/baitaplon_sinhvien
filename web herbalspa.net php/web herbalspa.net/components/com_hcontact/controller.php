<?php
/*------------------------------------------------------------------------
# controller.php - hcontact Component
# ------------------------------------------------------------------------
# author    VuNguyen
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   ruahinh.vn
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controller library
jimport('joomla.application.component.controller');

/**
 * Hcontact Component Controller
 */
class HcontactController extends JController
{
	
	function sendMail(){
		$name =$_POST["name"]; 
		$subject=$_POST["subject"]; 
		$phone=$_POST["phone"]; 
		$content=$_POST["content"];
		$address=$_POST["address"]; 
		$email =$_POST["email"]; 
		$receipt = $_POST["receipt"];
		
		$mailer = JFactory::getMailer();
		$config = JFactory::getConfig();
		$sender = array( 
		    $config->getValue( 'config.mailfrom' ),
		    $config->getValue( 'config.fromname' ) );
		echo "a"; 
		$mailer->setSender($sender);
		$recipient = $receipt;
		
		$mailer->addRecipient($recipient); 
		$body   = "Tên: $name \n Địa chỉ: $address \n Điện thoại: $phone \n Email: $email \n Nội Dung:$content";
		$mailer->setSubject($subject);
		$mailer->setBody($body); 
		$send = $mailer->Send(); 
		if ( $send !== true ) {  
		    echo 'Error sending email: ' . $send->__toString();
			JFactory::getApplication()->redirect("index.php/lien-he","Lỗi Gửi mail !");
		} else {
		   JFactory::getApplication()->redirect("index.php/lien-he","Gửi mail thành công !");
		}
		
	}

}
?>