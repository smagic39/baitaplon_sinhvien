<?php
/*------------------------------------------------------------------------
# hdichvucategory.php - dich vu Component
# ------------------------------------------------------------------------
# author    VuNguyen
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   ruahinh.vn
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import the Joomla modellist library
jimport('joomla.application.component.modellist');

/**
 * Hdichvu Cateory Model for Hdichvu Component
 */
class HdichvuModelhdichvucategory extends JModelList
{
	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
		$pk = JRequest::getInt('id');

		// Create a new query object.
		$db = JFactory::getDBO();
		$query	= $db->getQuery(true);
		// Select some fields
		$query->select('*');
		// From the products_product table
		$query->from('#__hdichvu_edit');
		$query->where('category="' . $pk . '"');

		return $query;
	}
	
	function getItems(){
		$pk = JRequest::getInt('id');
        $page = JRequest::getInt('page');
		if(!$page){
			$page =1;
		}
		$from = ($page-1)*6;
		$to = $page*6 + 1;
		// Create a new query object.
		$db = JFactory::getDBO();
		$query ="select * from #__hdichvu_edit where category = $pk limit $from, 6"; 
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	
	function getMItems(){
	
		// Create a new query object.
		$db = JFactory::getDBO();
		$query ="select * from #__hdichvu_edit where mdv = 1"; 
		$db->setQuery($query);
		return $db->loadObjectList();
	}
}
?>