<?php
/*------------------------------------------------------------------------
# default.php - herbal footer Component
# ------------------------------------------------------------------------
# author    vuguyen
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   ruahinh.vn
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
jimport('joomla.filter.output');
?>
<div id="hfooter-hfooter">
	<?php foreach($this->items as $item){ ?>
		<?php
		if(empty($item->alias)){
			$item->alias = $item->name;
		};
		$item->alias = JFilterOutput::stringURLSafe($item->alias);
		$item->linkURL = JRoute::_('index.php?option=com_hfooter&view=edit&id='.$item->id.':'.$item->alias);
		?>
		<p><strong>Name</strong>: <a href="<?php echo $item->linkURL; ?>"><?php echo $item->name; ?></a></p>
		<p><strong>Title</strong>: <?php echo $item->title; ?></p>
		<p><strong>Content</strong>: <?php echo $item->content; ?></p>
		<p><strong>Link URL</strong>: <a href="<?php echo $item->linkURL; ?>">Go to page</a> - <?php echo $item->linkURL; ?></p>
		<br /><br />
	<?php }; ?>
</div>
