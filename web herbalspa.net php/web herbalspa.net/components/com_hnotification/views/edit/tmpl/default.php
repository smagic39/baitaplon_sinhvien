<?php
/*------------------------------------------------------------------------
# default.php - hnotification Component
# ------------------------------------------------------------------------
# author    VuNguyen
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   ruahinh.vn
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

?>
<div id="hnotification-content">
	<p><strong>Name</strong>: <?php echo $this->item->name; ?></p>
	<p><strong>Content</strong>: <?php echo $this->item->content; ?></p>
</div>