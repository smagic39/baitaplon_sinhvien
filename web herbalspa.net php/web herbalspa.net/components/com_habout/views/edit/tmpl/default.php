<?php
/*------------------------------------------------------------------------
# default.php - habout Component
# ------------------------------------------------------------------------
# author    VuNguyen
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   ruahinh.vn
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

?>
<style>
	#habout-content{
		background:#f6f4e5;
		float:left;
	}
	#habout-content h3.title{
		margin:0px 10px 0px 10px;
		font-size:40px;
		border-bottom:#bdb7ad solid 2px;
		text-transform: uppercase;
		line-height:20px;
		padding-top:40px;
		display:none;
	}
	#habout-content div.content{
		 float: left;
    padding: 0 30px;
    width: 444px;
    text-align:justify;
	}
	
	#habout-content div.image{
	float: left;
    padding: 0;
    width: 476px;
	}
</style>
<div id="habout-content">
	<h3 class="title">&nbsp;&nbsp;<?php echo $this->item->title; ?></h3>
	<div class="content"> <?php echo $this->item->content; ?></div>
	<div class="image"> <?php echo $this->item->image; ?></div>
</div>