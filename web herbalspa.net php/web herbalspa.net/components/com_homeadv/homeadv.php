<?php
/*------------------------------------------------------------------------
# homeadv.php - home adv Component
# ------------------------------------------------------------------------
# author    VuNguyen
# copyright Copyright (C) 2014. All Rights Reserved
# license   GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
# website   ruahinh.vn
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Set the component css/js
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_homeadv/assets/css/homeadv.css');

// Require helper file
JLoader::register('HomeadvHelper', dirname(__FILE__) . DS . 'helpers' . DS . 'homeadv.php');

// import joomla controller library
jimport('joomla.application.component.controller');

// Get an instance of the controller prefixed by Homeadv
$controller = JController::getInstance('Homeadv');

// Perform the request task
$controller->execute(JRequest::getCmd('task'));

// Redirect if set by the controller
$controller->redirect();
?>