<?php
/*-----------------------------------------------
| SOME PANEL THAT WILL BE SHOWED ON PAGE
+------------------------------------------------*/	
// Check for Security
if ( !defined('HCR') )
 {
	print "<h1>Incorrect Access</h1>";
	exit();
 }

class panel
{	
	
	function getCRMInfos()
	{
		global $db;
		$crm = array();
		$sql_crm = 'SELECt * FROM crm ORDER BY id ASC';
		$result_crm = mysql_query($sql_crm);
		$crm = array();
		while ($row_crm = mysql_fetch_assoc($result_crm))
		{
			$crm[] = $row_crm;
		}
		return $crm;
	}

	// Add product to cart
	function addToCart($arr)	
	{
		// Set default quantity is 1
		//$arr['quantity'] = 1;
		// Have already product in cart
		if (isset($_SESSION['cart']) && count($_SESSION['cart']) >0)
		{
			$notin = true;
			// Check for the the duplication
			foreach($_SESSION['cart'] as $k=>$v)
			{
				if ($k == $arr['product'])
				{
					$notin = false;
					break;
				}
			}
			// Product is not in cart yet
			if ($notin)
			{
				$_SESSION['cart'][$arr['product']] = $arr;
			}
		
		}
		else
		{
			$_SESSION['cart'][$arr['product']] = $arr;
		}
	}
	
	// Sending simple email
	function sendEmail($to,$content,$title='',$from='')
	{
		global $db;
		$message  = "<h2>EMAIL THÔNG TIN TỪ ".URL."</h2>\r\n";
		$message .= $content;		
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		$headers .= 'To: ' . $to . "\r\n";
		if (strlen($from) > 0)
		{
			$fromname = substr($from,0,strpos($from,'@'));
			$headers .= 'From: '.$fromname.' <'.$from.'>' . "\r\n";	
		}
		else
		{
			// Get admin info
			$adinfo = $db->getData('SELECT * FROM adminfo');
			$headers .= 'From: '.$adinfo['name'].' <'.$adinfo['email'].'>'."\r\n";
		}
		
		if (!$title){ $title = 'THONG TIN TU '.WSITE.' <'.URL.'>'; }
			
		@mail($to, $title, $message, $headers);
	}
	

	

	/*------------------------------------
	| SHOW ACCESS INFOS
	--------------------------------------*/
	function showAccessInfo()
	{
		 global $ln, $db, $lg;
		 $text = '';
		 $accessInfo = array();
	     $sql = $db->simple_select('*','statistics','keyword IN("hitcounter","online_now")');
	     $query = $db->query($sql);
	     while ( $row = mysql_fetch_array($query) )
	     {
	         $accessInfo[$row['keyword']] = $row['value'] ;
	     }
		
		 $text .= '<table align="center" class="table_portion" cellpadding="0" cellspacing="0">
						<tr>
						<td class="td_portion"" align="center">
						'.$lang['access_info'].'
						</td>
						</tr>
						<tr><td height="4px"></td></tr>
						<tr>
						<td>';
				
		$text .= '				
						<div style="padding:5px;"><img src="'.DIR_IMG.'dot.gif" border="0" align="absmiddle"/>&nbsp;'.$lang['online_now'].' '.$accessInfo['online_now'].'</div>
						<div style="padding:5px;"><img src="'.DIR_IMG.'dot.gif" border="0" align="absmiddle"/>&nbsp;'.$lang['hit_counter'].' '.$accessInfo['hitcounter'].'</div>';
						
						
		 $text .= '   </td>
		  			  </tr>
					  <tr><td height="4px"></td></tr>
					  </table>';	
					   
		 return $text;
					   
	}	
	
	
	
	
	
} // end class


?>
