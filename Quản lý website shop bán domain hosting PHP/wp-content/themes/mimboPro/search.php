<?php get_header(); ?>
<div id="breadcrumbs"></div>

<div class="clearfloat stripes">
<?php get_sidebar(); ?>


	<div id="content">

	<?php if (have_posts()) : ?>

		<h4 class="pagetitle">Search Results</h4>

		<?php while (have_posts()) : the_post(); ?>

			<div class="cat-excerpt clearfloat bluebox" id="post-<?php the_ID(); ?>">
				
				<?php 
				//Checks for custom images
				$values = get_post_custom_values("Image");
	if (isset($values[0])) {						
	?>
	<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><img src="<?php echo bloginfo('template_url'); ?>/scripts/timthumb.php?src=/<?php
			$values = get_post_custom_values("Image"); echo $values[0]; ?>&amp;w=70&amp;h=70&amp;zc=1" alt="<?php the_title(); ?>" /></a>
			<?php } ?>
			
			
			<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
			<?php the_excerpt(); ?>
			</div>
			
		<?php endwhile; ?>

		<div class="navigation pagination">
			<div class="left"><?php next_posts_link('&laquo; Older Entries') ?></div>
			<div class="right"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
		</div>

	<?php else : ?>

		<h4 class="pagetitle">No posts found. Try a different search?</h4>

	<?php endif; ?>

	</div>
	</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>