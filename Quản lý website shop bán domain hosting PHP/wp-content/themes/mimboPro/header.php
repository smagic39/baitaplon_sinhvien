<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	
	<title><?php 
	//Smart and SEO-friendly titles!
	if ( is_single() ) {
	      wp_title('');
	      echo (' | ');
	      bloginfo('name');
	 
	} else if ( is_page() || is_paged() ) {
	      bloginfo('name');
	      wp_title('|');
	 
	} else if ( is_author() ) {
	      bloginfo('name');
	      wp_title(' | Archive for ');	  
		  
	} else if ( is_archive() ) {
	      bloginfo('name');
	      echo (' | Archive for ');
	      wp_title('');
	 
	} else if ( is_search() ) {
	      bloginfo('name');
	      echo (' | Search Results');
	 
	} else if ( is_404() ) {
	      bloginfo('name');
	      echo (' | 404 Error (Page Not Found)');
		  
	} else if ( is_home() ) {
	      bloginfo('name');
	 
	} else {
	      bloginfo('name');
	      echo (' | ');
	      echo (''.$blog_longd.'');
	}
	 ?></title>
		
	<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" /> 
	<?php if ( is_single() ) { ?>
	<meta name="description" content="<?php wp_title(''); ?>" />
	<meta name="keywords" content="<?php wp_title(''); ?>" />
	<?php } ?>
	<?php if ( ! is_single() ) { ?>
	<meta name="description" content="<?php bloginfo('description'); ?>" />
	<meta name="keywords" content="<?php bloginfo('description'); ?>" />
	<?php } ?>
	
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css" type="text/css" media="screen" title="default" />
	
	<?php 
	//Include homepage-specific files
	if (is_home()) { ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/home.css" type="text/css" media="screen" />
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-1.2.1.pack.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.jcarousel.pack.js"></script>
	<script type="text/javascript">
	jQuery(document).ready(function() {
	    jQuery('#mycarousel').jcarousel();
	});
	</script>
	<?php } ?>
	
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/nav.css" type="text/css" media="screen" title="default" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/tabs.css" type="text/css" media="screen" title="default"/>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/carousel.css" type="text/css" media="screen" title="default"/>
	<link rel="alternate stylesheet" href="<?php bloginfo('template_url'); ?>/css/print-preview.css" type="text/css" media="screen" title="Print Preview"/>
	<?php $bmValue = get_settings( "bm_style" );
	if( $bmValue != "" ) { ?>
	<link href="<?php bloginfo('template_url'); ?>/skins/<?php echo $bmValue; ?>/styles.css" type="text/css" rel="stylesheet" title="default" />
	<?php } ?>
	
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/print.css" type="text/css" media="print" />
	
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/dropdowns.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/printPreview.js"></script>	
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/commenteditor.js"></script>	
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/domtab.js"></script>	
	
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	
	<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico" />
	
	<?php if( get_option( "bm_logo" ) == 1 ) { ?>
	<style type="text/css">
	#masthead h1, a#logo {
		text-indent:-9999px;
		width:320px;
		height:108px;
		background:url( http://megahost.vn/logo_megahost.png ) no-repeat;
		margin:0 auto;
		display:block;
		color:#FFF;
	}
	#masthead h2 {
		display:none;
	}
	</style>
	<?php }

	if( get_option( "bm_useCustomHeader" ) == 1 ) {
		$newHeader = get_header_image();
		if( $newHeader != "" ) { ?>
	<style type="text/css">
	#masthead {
		background-image: url(<?php echo $newHeader; ?>);	
	}	
	</style>
	<?php			
		}
		
	}
	?>
	
	<?php wp_head(); ?>

</head>

<body<?php if (is_home()) { ?> id="home"<?php } else { ?> class="page-<?php echo $post->post_name; ?>" id="interior"<?php } ?>>

<div id="printMessage"></div>

<div id="page">

<div id="masthead">
	<?php if (is_home()) { ?>
		<h1><?php bloginfo('name'); ?></h1>
	<?php } else { ?>
		<a id="logo" href="<?php bloginfo('home'); ?>" title="Back Home"><?php bloginfo('name'); ?></a>
	<?php } ?>
	<h2 id="description"><?php bloginfo('description'); ?></h2>
</div>

<div id="navbar" class="clearfloat">

<ul id="nav" class="left clearfloat">
<li <?php if (is_home()) { ?>class="current-cat"<?php } ?>><a href="<?php echo get_option('home'); ?>/">Home</a></li> 
<?php

	$bm_navigation = get_settings( "bm_navigation" );
	if( $bm_navigation == "" || $bm_navigation == 0 ) {
		wp_list_categories('title_li=');
	} else {
		wp_list_pages('title_li=');
	}
	 
	
?>
</ul>



</div><!--END NAVBAR-->



