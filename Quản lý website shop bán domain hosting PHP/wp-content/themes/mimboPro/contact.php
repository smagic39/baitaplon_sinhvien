<?php
/*
Template Name: contact
*/
?>

<?php get_header(); ?>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/contact.js"></script>
<div id="breadcrumbs"></div>

<div class="clearfloat stripes">
<?php get_sidebar(); ?>

	<div id="content">
	
<?php
//Here is where you'd put your address, contact policies or any other contact info
if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="post" id="post-<?php the_ID(); ?>">
		<h2><?php the_title(); ?></h2>
			<div class="entry">
				<?php the_content(); ?>
			</div>
		</div>
		<?php endwhile; endif; ?>
	
	
	<!--FORM GOES HERE-->

<?php 
//Contact script created by Tim McDaniels and Darren Hoyt for the Mimbo Pro theme
//Developer's license allows for re-use of contact form, if credits are intact
//Copyright 2008

if($_REQUEST['submit']): ?>
<?php
		$admin_email = get_settings( "mim_contactEmail" );
		$admin_subject = get_settings( "mim_contactSubject" );
		$admin_success = get_settings( "mim_contactSuccess" );
		$headers = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: ' . $admin_email . "\r\n";
		$body = "<blockquote>
	Name: " . $_REQUEST['Name'] . "<br/>
	Email: " . $_REQUEST['Email'] . "<br/>
	Message:<br/>" . $_REQUEST['Message'] . "<br/>
	<hr/>
	Remote Address: " . $_SERVER['REMOTE_ADDR'] . "<br/>
	Browser: " . $_SERVER['HTTP_USER_AGENT'] . "
	<hr/>
	</blockquote>";
                mail ($admin_email, $admin_subject, $body, $headers);
?>

			<div class="bluebox"><?php echo $admin_success; ?></div>

<?php endif; ?>
<?php if(!$_REQUEST['submit']): ?>
	<form onsubmit="return(submitContactForm(this));" id="contactform" action="">
		<fieldset>
			<legend>Contact</legend>
			<label for="contact-name">Your Name</label>
			<input type="text" id="contact-name" name="Name" class="field" size="35" />
			<label for="contact-email">Your Email</label>
			<input type="text" id="contact-email" name="Email" class="field" size="35" />
			<label for="contact-message">Your Message</label>
			<textarea id="contact-message" name="Message" rows="7" cols="80" class="field"></textarea>
			<input type="hidden" name="submit" value="1" />
			<input type="submit" value="Submit" id="submit" />
		</fieldset>
	</form>
	<!--END FORM-->
	
	
<?php endif; ?>
	</div>
</div>

<?php get_footer(); ?>
