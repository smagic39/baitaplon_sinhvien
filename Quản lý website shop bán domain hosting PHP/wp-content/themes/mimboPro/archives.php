<?php
/*
Template Name: archives
*/
?>


<?php get_header(); ?>
<div id="breadcrumbs"></div>

<div class="clearfloat stripes">
<?php get_sidebar(); ?>

	<div id="content" class="entry">
	
	<h2>Archives</h2>

<br />
	<h4>Browse by Month</h4>
				<ul class="bullets">
				<?php wp_get_archives('type=monthly'); ?>
				</ul>
					
					<br />
				<h4>Browse by Year</h4>
				<ul class="bullets">
				<?php wp_get_archives('type=yearly'); ?>
				</ul>
				
				<br />
				<h4>Browse Last 50 Posts</h4>
				<ul class="bullets">
		<?php query_posts('showposts=50'); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<li>
<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>">
<?php the_title(); ?></a><span class="commentcount">(<?php comments_number('0', '1', '%'); ?>)</span>
</li>
<?php endwhile; else: endif; ?>
</ul>				
						
	</div>
</div><!--END FLOATS-->

<?php get_footer(); ?>
