	function addLoadEvent (func) {
        	var oldonload = window.onload;
        	if(typeof window.onload != 'function') {
                	window.onload = func;
        	}
        	else {
                	window.onload = function() {
                        	oldonload();
                        	func();
                	}
        	}
	}

	function showDocumentTabs () {
		var links = document.getElementsByTagName('a');
		var first = 1;
		for(var i = 0; i < links.length; i++) {
			myregexp = /(.*)_document_(.*)/;
			mymatch = myregexp.exec(links[i].id);
			if(!(mymatch == null)) {
				if(mymatch[2] == 'tab') {

					// default first tab to selected and all others to unselected

					if(first) {
						eval('document.getElementById("'+mymatch[1]+'_document_tab").className="selected_tab";');
						first = 0;
					}
					else {
						eval('document.getElementById("'+mymatch[1]+'_document_tab").className="unselected_tab";');
					}
					eval('links[i].onclick = function() {'
						+'var target = document.getElementById("'+mymatch[1]+'_document_div");'
						+'target.style.display = target.style.display == \'none\' ? \'block\': \'block\';'
						+'document.getElementById("'+mymatch[1]+'_document_tab").className="selected_tab";'
						+'hideInstallationDivs("'+mymatch[1]+'_document_div");'
					+'}');
				}
			}
		}
	}

	function hideInstallationDivs (item) {
		var divs = document.getElementsByTagName('div');
		for(var i = 0; i < divs.length; i++) {
			myregexp = /(.*)_document_(.*)/; 
                        mymatch = myregexp.exec(divs[i].id);
                        if(!(mymatch == null)) {
                                if(mymatch[2] == 'div' && mymatch[1]+"_document_div" != item) {
					eval('var '+mymatch[1]+'_document_div = document.getElementById("'+mymatch[1]+'_document_div");');
					eval(mymatch[1]+'_document_div.style.display = \'none\';');
					eval('document.getElementById("'+mymatch[1]+'_document_tab").className = "unselected_tab";');
                                }
			}
		}
	}

	function showDocumentSubFiles () {
		var uls = document.getElementsByTagName('ul');
		for(var i = 0; i < uls.length; i++) {
			myregexp = /(.*)_sub_doc_(.*)/;
			mymatch = myregexp.exec(uls[i].id);
			if(!(mymatch == null)) {
				eval('var '+mymatch[1]+'_sub_doc_list = document.getElementById("'+mymatch[1]+'_sub_doc_list");');
				eval(mymatch[1]+'_sub_doc_list.style.display = \'none\';');
				eval('var '+mymatch[1]+'_sub_doc_link = document.getElementById("'+mymatch[1]+'_sub_doc_link");');
				eval(mymatch[1]+'_sub_doc_link.style.background = \'url(/images/plus.png) no-repeat\';');
				eval(mymatch[1]+'_sub_doc_link.style.paddingBottom = \'2px\';');
				eval(mymatch[1]+'_sub_doc_link.style.textDecoration = \'none\';');
				eval(mymatch[1]+'_sub_doc_link.onclick = function() {'
					+mymatch[1]+'_sub_doc_list.style.display = '+mymatch[1]+'_sub_doc_list.style.display == \'none\' ? \'block\': \'none\';'
					//+'var '+mymatch[1]+'_sub_doc_img = document.getElementById("'+mymatch[1]+'_sub_doc_img");'
					+'var test_str = new String('+mymatch[1]+'_sub_doc_link.style.background);'
					+mymatch[1]+'_sub_doc_link.style.background = test_str.match(\'images/plus.png\') ? \'url(/images/minus.png) no-repeat\' : \'url(/images/plus.png) no-repeat\';'
                                        +'}');
			}
		}
	}

addLoadEvent(showDocumentTabs);
addLoadEvent(showDocumentSubFiles);
