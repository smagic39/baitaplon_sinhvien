<?php get_header(); ?>
<div id="breadcrumbs"></div>

<div class="clearfloat stripes">
<?php get_sidebar(); ?>
<div id="content">



<!-- This sets the $curauth variable -->
<?php
if(isset($_GET['author_name'])) {
	$curauth = get_userdatabylogin($author_name);
} else {
	$curauth = get_userdata(intval($author));
}
?>

<h2>Author Archive <a href="<?php echo get_author_posts_url( $author, ""); ?>feed/"><img src="<?php bloginfo('template_url'); ?>/images/rss.gif" alt="rss" /></a></h2>
<br />

<div class="bluebox clearfloat" id="author">
<img src="<?php bloginfo('template_url'); ?>/scripts/timthumb.php?src=<? echo str_replace( get_bloginfo('url'), "", get_bloginfo('stylesheet_directory') ); ?>/images/<?php echo $curauth->first_name; ?>.jpg&amp;w=100&amp;h=100&amp;zc=1&amp;q=90" alt="author" /> 
			

<p class="left"><strong><?php echo $curauth->first_name; ?> <?php echo $curauth->last_name; ?></strong> is <?php echo $curauth->user_description; ?>
<br />
<span><a href="mailto:<?php echo $curauth->user_email; ?>" title="Email this author">Email this author</a></span>
</p>
</div>

<h4>Posts by <?php echo $curauth->first_name; ?>:</h4>
<div class="entry">
<ul class="bullets">
<?php query_posts('showposts=1000&author=' . $author ); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<li>
<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>">
<?php the_title(); ?></a><span class="commentcount">(<?php comments_number('0', '1', '%'); ?>)</span>
</li>
<?php endwhile; else: ?>
<p><?php _e('No posts by this author.'); ?></p>
<?php endif; ?>
</ul>
</div>
</div>
</div>

<?php get_footer(); ?>
