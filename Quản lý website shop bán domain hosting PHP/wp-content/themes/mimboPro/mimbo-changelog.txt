v1.2
----

- added - scripts/.htaccess to try and improve timthumb.php caching
- fix - functions.php, multi select style in wp 2.5
- fix - timthumb.php, fixed and improved caching features. Hppefully it will play nicely with everyone!
- fix - sidebar.php, tweak widget display to look nicer and validate
- fix - custom header image in for wordpress 2.5

v1.1
----
fix - timthumb.php, replace quit with die (doh!)
fix - timthumb.php, "better" mimetype support
fix - timthumb.php, support for pngs (quality value fix)
fix - header.php, moved wp-head to improve support for plugins
fix - author.php, hardcoded author image path now dynamic 
add - index.php, clearfloat div to improve display for small excerpts
add - index.php, carousel count customisation
add - header.php, navigation customisation
add - functions.php, carousel and navigation options  

v1
--
initial release
