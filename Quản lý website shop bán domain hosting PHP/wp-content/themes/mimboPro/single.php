<?php get_header(); ?>

<div id="breadcrumbs">You are here: <a href="<?php bloginfo('home'); ?>">Home</a> -> <?php the_category(', ') ?> -> <?php the_title(''); ?></div>


		<?php
		//Gets category and author info
		
		global $wp_query;

		$bm_cats = get_the_category();		
		$postAuthor = $wp_query->post->post_author;
		$tempQuery = $wp_query;
		
		// related author posts
		$newQuery = "posts_per_page=4&author=" . $postAuthor;
		query_posts( $newQuery ); 

		$bm_authorPosts = "";

		if (have_posts()) {
			while (have_posts()) {
				
				the_post();
				$bm_authorPosts .= '<li><a href="' . get_permalink() . '">' . the_title( "", "", false ) . '</a></li>';
					 
			}
		}

		// related category posts
		$bm_clist = ""; 
		forEach( $bm_cats as $bm_c ) {
			if( $bm_clist != "" ) { $bm_clist .= ","; }
			$bm_clist .= $bm_c->cat_ID;
		}

		$newQuery = "posts_per_page=4&cat=" . $bm_clist;
		query_posts( $newQuery );
		
		$bm_categoryPosts = "";

		if (have_posts()) {
			while (have_posts()) {
				
				the_post();
				$bm_categoryPosts .= '<li><a href="' . get_permalink() . '">' . the_title( "", "", false ) . '</a></li>'				
				;
			}
		}
		
		$wp_query = $tempQuery; 
		
		?>

		


		<div class="clearfloat stripes">
		<?php get_sidebar(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


		<div id="content">

			<div class="post" id="post-<?php the_ID(); ?>">
				<h2><?php the_title(); ?></h2>
				
				<div class="postmetadata">Posted by <?php the_author_posts_link(); ?> on <?php the_time('F jS, Y') ?>  <span class="commentcount"><a href="<?php the_permalink(); ?>#discussion"><?php comments_number('No Comments', '1 Comment', '% Comments'); ?></a></span>
				<span id="print"><a href="#printpreview" onclick="printPreview();">Printer-Friendly</a></span>
				
				</div>
				
				<div class="clearfloat">
				
				
				<div class="entry narrow clearfloat bigger">
				<?php the_content(); ?>
				</div>
				
				<div id="more-menu">
				
				
				<h4>More from this category</h4>
				<ul>
				<?php echo $bm_categoryPosts; ?>
				</ul>
				
				<h4>More from this author</h4>
				<ul>
				<?php echo $bm_authorPosts; ?>
				</ul>
				
				
				
				
				</div><!--END MENU-->
				
				
				</div><!--END ENTRY-->
	  
	  

<?php if (get_the_tags()) { ?>

<?php } ?>
	
				<?php comments_template(); ?>

	<?php endwhile; else: ?>

				<p>Sorry, no posts matched your criteria.</p>

<?php endif; ?>

			</div>
		</div><!--END CONTENT-->
</div><!--END FLOATS-->


<?php get_footer(); ?>
