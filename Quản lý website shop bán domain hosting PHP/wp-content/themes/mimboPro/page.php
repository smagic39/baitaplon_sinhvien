<?php get_header(); ?>
<div id="breadcrumbs"></div>

<div class="clearfloat stripes">
<?php get_sidebar(); ?>

	<div id="content">
	
	<?php 
	//This establishes if the page has children
	if($post->post_parent)
  $children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0"); else
  $children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
  if ($children) { ?><?php } else { ?><?php } ?>


<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="post clearfloat" id="post-<?php the_ID(); ?>">
		<h2><?php the_title(); ?></h2>
	
	<div class="entry bigger">

 <?php 
 // This provides a submenu if there are child pages
 if ($children) { ?>
<div id="more-menu">
<h4>More in this section:</h4>
<ul>
<?php echo $children; ?>
</ul>

</div><!--END SUBNAV-->
<?php } else { ?>
<?php } ?>



<?php the_content(''); ?>
			</div><!--END ENTRY-->
			
		</div><!--END POST-->
		<?php endwhile; endif; ?>
		
	<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>

	</div>
</div><!--END FLOATS-->

<?php get_footer(); ?>
