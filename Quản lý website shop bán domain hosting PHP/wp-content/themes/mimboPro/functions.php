<?php

$bmTheme = "Mimbo Pro";

// sidebar stuff
if ( function_exists('register_sidebar') ) {
    register_sidebar(
		array(
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
        'name' => 'Left Sidebar'        
        )
   	);
   	register_sidebar(
   		array(
		'name' => 'Right Sidebar'  
		)
	);
   	unregister_sidebar_widget ( "search" );
}

// custom header stuff
define( 'HEADER_TEXTCOLOR', '' );
define( 'HEADER_IMAGE', '%s/images/header_bg.jpg' ); // %s is theme dir uri
define( 'HEADER_IMAGE_WIDTH', 946 );
define( 'HEADER_IMAGE_HEIGHT', 108 );
define( 'HEADER_IMG_DIR', 'mimbopro' );
define( 'NO_HEADER_TEXT', true );

add_custom_image_header( 'bm_adminHeaderStyle', 'bm_adminHeaderStyle' );

function bm_adminHeaderStyle() {}

// the rest of the stuff....

function bm_addThemePage() {

	global $bmTheme;

	if ( $_GET['page'] == basename(__FILE__) ) {
	
	    // save settings
		if ( 'save' == $_REQUEST['action'] ) {
		
			check_admin_referer( 'save-theme-properties' );

			// text input
			update_option( 'bm_carouselCount', $_REQUEST[ 'bm_carouselCount' ] );
			update_option( 'bm_navigation', $_REQUEST[ 'bm_navigation' ] );			
			update_option( 'bm_feedlinkURL', $_REQUEST[ 'bm_feedlinkURL' ] );
			update_option( 'bm_feedlinkComments', $_REQUEST[ 'bm_feedlinkComments' ] );
			update_option( 'bm_analytics', $_REQUEST[ 'bm_analytics' ] );
			update_option( 'bm_style', $_REQUEST[ 'bm_style' ] );
			
			update_option( 'mim_homeCat', $_REQUEST[ "mim_homeCat" ] );	
			update_option( 'mim_carousel', $_REQUEST[ 'mim_carousel' ] );
			update_option( 'mim_feature', $_REQUEST[ 'mim_feature' ] );

            update_option( 'mim_contactEmail', $_REQUEST[ 'mim_contactEmail' ] );
            update_option( 'mim_contactSubject', $_REQUEST[ 'mim_contactSubject' ] );
            update_option( 'mim_contactSuccess', $_REQUEST[ 'mim_contactSuccess' ] );
						
			update_option( 'bm_adImage_1', $_REQUEST[ 'bm_adImage_1' ] );
			update_option( 'bm_adURL_1', $_REQUEST[ 'bm_adURL_1' ] );
			update_option( 'bm_adImage_2', $_REQUEST[ 'bm_adImage_2' ] );
			update_option( 'bm_adURL_2', $_REQUEST[ 'bm_adURL_2' ] );
			update_option( 'bm_adImage_3', $_REQUEST[ 'bm_adImage_3' ] );
			update_option( 'bm_adURL_3', $_REQUEST[ 'bm_adURL_3' ] );
			update_option( 'bm_adImage_4', $_REQUEST[ 'bm_adImage_4' ] );
			update_option( 'bm_adURL_4', $_REQUEST[ 'bm_adURL_4' ] );
			
			// checkboxes
			if( isset( $_REQUEST[ 'bm_logo' ] ) ) { update_option( 'bm_logo', 1 ); } else { delete_option( 'bm_logo' ); }
			if( isset( $_REQUEST[ 'bm_title' ] ) ) { update_option( 'bm_title', 1 ); } else { delete_option( 'bm_title' ); }
			if( isset( $_REQUEST[ 'bm_useCustomHeader' ] ) ) { update_option( 'bm_useCustomHeader', 1 ); } else { delete_option( 'bm_useCustomHeader' ); }

			// goto theme edit page
			header("Location: themes.php?page=functions.php&saved=true");
			die;

  		// reset settings
		} else if( 'reset' == $_REQUEST['action'] ) {

			delete_option( 'bm_carouselCount' );
			delete_option( 'bm_navigation' );
			delete_option( 'bm_feedlinkURL' );
			delete_option( 'bm_feedlinkComments' );
			delete_option( 'bm_analytics' );
			delete_option( 'bm_style' );
			
            delete_option( 'mim_contactEmail' );
            delete_option( 'mim_contactSubject' );
            delete_option( 'mim_contactSuccess' );
						
			delete_option( 'mim_homeCat' );
			delete_option( 'mim_carousel' );
			delete_option( 'mim_feature' );
			
			delete_option( 'bm_adImage_1' );
			delete_option( 'bm_adURL_1' );		
			delete_option( 'bm_adImage_2' );
			delete_option( 'bm_adURL_2' );
			delete_option( 'bm_adImage_3' );
			delete_option( 'bm_adURL_3' );
			delete_option( 'bm_adImage_4' );
			delete_option( 'bm_adURL_4' );

			// goto theme edit page
			header("Location: themes.php?page=functions.php&reset=true");
			die;

		}
	}

	add_theme_page( $bmTheme . ' Theme Options', $bmTheme . ' Options', 'edit_themes', basename(__FILE__), 'bm_themePage' );

}

function bm_themePage() {

	global $bmTheme;
	
	include( "scripts/bm_helpers.php" );
	
	if ( $_REQUEST[ 'saved' ] ) echo '<div id="message" class="updated fade"><p><strong>Settings saved</strong></p></div>';
	if ( $_REQUEST[ 'reset' ] ) echo '<div id="message" class="updated fade"><p><strong>Settings reset</strong></p></div>';
	
	?>
	
	<div class="wrap">
	
	<h2><?php echo $bmTheme; ?></h2>
	
	<form method="post">
	
	<?php if ( function_exists('wp_nonce_field') ) { wp_nonce_field( 'save-theme-properties' ); } ?>
	
	<h3>Blog Settings</h3>
	<p>Some general blog settings to set up the layout of your site</p>

	<table width="100%" cellspacing="2" cellpadding="5" class="editform form-table">
	<?php

		// ------------
		// Theme Styles
		// ------------
		bm_th( 'Style' );
		
		// get styles
		$bm_styleDir = opendir( TEMPLATEPATH . "/skins/" );

		$bm_styles[] = array( "", "None (default)" );
		// Open a known directory, and proceed to read its contents
	    while (false !== ( $bm_styleFolder = readdir( $bm_styleDir ) ) ) {
	    	if( $bm_styleFolder != "." && $bm_styleFolder != ".." ) {
	    		$bm_styleName = $bm_styleFolder;
	    		$bm_styleName = str_replace( "-", " ", $bm_styleName );
	    		$bm_styleName = str_replace( "_", " ", $bm_styleName );
				$bm_styles[] = array( $bm_styleFolder, $bm_styleName );
    		}
		}
		
		bm_select( "bm_style", $bm_styles, get_settings( "bm_style" ), "" );
    	
    	closedir( $bm_styleDir );
    	
    	bm_cth();
    	
		bm_th( "Header Properties" );
    	bm_input( "bm_logo", "checkbox", "Show Logo?", "1", get_settings( 'bm_logo' ) );
    	bm_input( "bm_useCustomHeader", "checkbox", "User custom header image?", "1", get_settings( 'bm_useCustomHeader' ) );
		bm_cth();
		
		bm_th( "Carousel Quantity" );
		bm_input( "bm_carouselCount", "text", "Number of Carousel items to show on the homepage. Defaults to 12, recommended greater than 4", get_settings( "bm_carouselCount" ) );
		bm_cth();
		
		$bm_nav[] = array( 0, "Categories (Default)" );
		$bm_nav[] = array( 1, "Pages" );

		bm_th( "Main Navigation" );
		bm_select( "bm_navigation", $bm_nav, get_settings( "bm_navigation" ), "" );
		bm_cth();

		// --------------		
		// Other settings
		// --------------	

		bm_th( "Feedburner URL" );
		bm_input( "bm_feedlinkURL", "text", "Optional Feedburner URL. Will replace rss feed links if used", get_settings( "bm_feedlinkURL" ) );
		bm_cth();
		
		bm_th( "Feedburner Comments URL" );
		bm_input( "bm_feedlinkComments", "text", "Optional Feedburner id. Will replace rss comment feed links if used", get_settings( "bm_feedlinkComments" ) );
		bm_cth();
		
		bm_th( "Google Analytics ID" );
		bm_input( "bm_analytics", "text", "Optional Google Analytics ID", get_settings( "bm_analytics" ) );
		bm_cth();		
		
	?>
	</table>

	
	<h3>Homepage Settings</h3>	
	<p>Homepage specific settings. These control the content shown on the first page of your site.</p>

	<table width="100%" cellspacing="2" cellpadding="5" class="editform form-table">
	<?php 

		$bm_categories = get_categories('hide_empty=0');
		foreach ( $bm_categories as $b ) {
			$bm_cat[] = array( $b->cat_ID, $b->cat_name );
		}

		bm_th( "Lead Category" );
		bm_select( "mim_feature", $bm_cat, get_settings( "mim_feature" ), "" );		
		bm_cth();

		bm_th( "Carousel Category" );
		bm_select( "mim_carousel", $bm_cat, get_settings( "mim_carousel" ), "" );		
		bm_cth();
		
		bm_th( "Category Summaries" );
		bm_multiSelect( "mim_homeCat[]", $bm_cat, get_settings( "mim_homeCat" ), "", "Hold down Control to select multiple categories" );
		bm_cth();
		
	?>
	</table>	


	<h3>Banner Ads</h3>
	<p>The Ads that will appear on the righthand sidebar of the homepage. To set up a new ad simply <strong>upload the image to the ads folder in your theme folder</strong></p>

	<table width="100%" cellspacing="2" cellpadding="5" class="editform form-table">
	<?php
		// get styles
		$bm_adsDir = opendir( TEMPLATEPATH . "/ads/" );

		$bm_ads[] = array( "", "None (default)" );
		// Open a known directory, and proceed to read its contents
	    while (false !== ( $bm_adsFolder = readdir( $bm_adsDir ) ) ) {
	    	if( $bm_adsFolder != "." && $bm_adsFolder != ".." ) {
	    		$bm_adName = $bm_adsFolder;
	    		$bm_adName = str_replace( "-", " ", $bm_adName );
	    		$bm_adName = str_replace( ".jpg", "", $bm_adName );
	    		$bm_adName = str_replace( ".gif", "", $bm_adName );
	    		$bm_adName = str_replace( "_", " ", $bm_adName );
				$bm_ads[] = array( $bm_adsFolder, $bm_adName );
    		}
		}
		
		closedir( $bm_adsDir );
		
		bm_th( "Advert 1" );
		bm_select( "bm_adImage_1", $bm_ads, get_settings( "bm_adImage_1" ), "" );
		echo "<br />";
		bm_input( "bm_adURL_1", "text", "Advert 1 click URL", get_settings( "bm_adURL_1" ) );
		bm_cth();
		
		bm_th( "Advert 2" );
		bm_select( "bm_adImage_2", $bm_ads, get_settings( "bm_adImage_2" ), "" );
		echo "<br />";
		bm_input( "bm_adURL_2", "text", "Advert 2 click URL", get_settings( "bm_adURL_2" ) );
		bm_cth();
		
		bm_th( "Advert 3" );
		bm_select( "bm_adImage_3", $bm_ads, get_settings( "bm_adImage_3" ), "" );
		echo "<br />";
		bm_input( "bm_adURL_3", "text", "Advert 3 click URL", get_settings( "bm_adURL_3" ) );
		bm_cth();
		
		bm_th( "Advert 4" );
		bm_select( "bm_adImage_4", $bm_ads, get_settings( "bm_adImage_4" ), "" );
		echo "<br />";
		bm_input( "bm_adURL_4", "text", "Advert 4 click URL", get_settings( "bm_adURL_4" ) );
		bm_cth();
    	
    	
	?>
	</table>


	<h3>Contact Page properties</h3>
	<p>Some general settings for your contact form</p>

	<table width="100%" cellspacing="2" cellpadding="5" class="editform form-table">
	<?php
			
		bm_th( "Email Address" );
		bm_input( "mim_contactEmail", "text", "contact email address", get_settings( "mim_contactEmail" ) );
		bm_cth();
    	
		bm_th( "Contact Form Email Subject" );
		bm_input( "mim_contactSubject", "text", "Enter a subject for the contact submission", get_settings( "mim_contactSubject" ) );
		bm_cth();
    	
		bm_th( "Contact Form Email Success Message" );
		bm_input( "mim_contactSuccess", "text", "Enter a success message that will be displayed to the user after their submission.", get_settings( "mim_contactSuccess" ) );
		bm_cth();


	?>
	</table>

	<input type="hidden" name="action" value="save" />
	
	<?php bm_input( "save", "submit", "", "Save Settings" ); ?>
	
	</form>
	
	</div>
	<?php



}

function bm_getProperty( $property ) {

	global $bmTheme;

	$value = get_settings( "bmTheme_" . $property );
	if( $value == "1" ) { return 1; } else { return 0; }	

}

$bm_trackbacks	= array();
$bm_comments	= array();

function bm_splitComments( $source ) {

	if ( $source ) {

		global $bm_trackbacks;
		global $bm_comments;
	 
		foreach ( $source as $comment ) {
	
			if ( $comment->comment_type == 'trackback' || $comment->comment_type == 'pingback' ) {	
				$bm_trackbacks[] = $comment;
	  		} else {
	  			$bm_comments[] = $comment;
	  		}

		}
	}

}

function bm_catProperties( $id ) {

	global $bm_categories;

	forEach( $bm_categories as $bC ) {
	
		if( $bC->cat_ID == $id ) {
			return $bC;
			exit;
		}
	
	}

}

// based upon the work done by Steve Smith - http://orderedlist.com/wordpress-plugins/feedburner-plugin/ and feedburner - http://www.feedburner.com/fb/a/help/wordpress_quickstart
function feed_redirect() {

	global $wp, $feed, $withcomments;
	
	$newURL1 = trim( get_settings( "bm_feedlinkURL" ) );
	$newURL2 = trim( get_settings( "bm_feedlinkComments" ) );
	
	if( is_feed() ) {

		if ( $feed != 'comments-rss2' 
				&& !is_single() 
				&& $wp->query_vars[ 'category_name' ] == ''
				&& !is_author() 
				&& ( $withcomments != 1 )
				&& $newURL1 != '' ) {
		
			if ( function_exists( 'status_header' ) ) { status_header( 302 ); }
			header( "Location:" . $newURL1 );
			header( "HTTP/1.1 302 Temporary Redirect" );
			exit();
			
		} elseif ( ( $feed == 'comments-rss2' || $withcomments == 1 ) && $newURL2 != '' ) {
	
			if ( function_exists( 'status_header' ) ) { status_header( 302 ); }
			header( "Location:" . $newURL2 );
			header( "HTTP/1.1 302 Temporary Redirect" );
			exit();
			
		}
	
	}

}

function feed_check_url() {

	switch ( basename( $_SERVER[ 'PHP_SELF' ] ) ) {
		case 'wp-rss.php':
		case 'wp-rss2.php':
		case 'wp-atom.php':
		case 'wp-rdf.php':
		
			$newURL = trim( get_settings( "bm_feedlinkURL" ) );
			
			if ( $newURL != '' ) {
				if ( function_exists('status_header') ) { status_header( 302 ); }
				header( "Location:" . $newURL );
				header( "HTTP/1.1 302 Temporary Redirect" );
				exit();
			}
			
			break;
			
		case 'wp-commentsrss2.php':
		
			$newURL = trim( get_settings( "bm_feedlinkComments" ) );
			
			if ( $newURL != '' ) {
				if ( function_exists('status_header') ) { status_header( 302 ); }
				header( "Location:" . $newURL );
				header( "HTTP/1.1 302 Temporary Redirect" );
				exit();
			}
			
			break;
	}
}

if (!preg_match("/feedburner|feedvalidator/i", $_SERVER['HTTP_USER_AGENT'])) {
	add_action('template_redirect', 'feed_redirect');
	add_action('init','feed_check_url');
}
add_action( 'admin_menu', 'bm_addThemePage' );

?>
