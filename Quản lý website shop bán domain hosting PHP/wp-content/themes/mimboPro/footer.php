<?php include("meta.php"); ?>

</div><!--END PAGE-->
<div id="footer">
  <?php wp_footer(); ?>
 
 <div class="left">Copyright &#169; <?php echo date('Y'); ?> Megahost &bull; 
  Powered by WordPress
  
  
  
<!--The following is optional address and contact information using Microformats:
http://microformats.org/wiki/adr#Examples-->

<!--
<div class="adr">
<span class="street-address">
Address Line #1</span>, <span class="locality">City</span>, <span class="region">State</span>, <span class="postal-code">22901</span> <br />
<span class="tel">Ph: <span class="value">(555) 555-5555</span></span>
</div>-->
</div> 



</div><!--END FOOTER-->

<?php
	$bm_analytics = get_settings( "bm_analytics" );
	if( $bm_analytics != "" ) { 
?>

<!--GOOGLE ANALYTICS-->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>

<script type="text/javascript">
var pageTracker = _gat._getTracker("<?php echo $bm_analytics; ?>");
pageTracker._initData();
pageTracker._trackPageview();
</script>
	
<?php } ?>

<script type="text/javascript">if( location.hash == "#printpreview" ) { printPreview(); }</script>

</body>
</html>
