<?php get_header(); ?>

<div id="upper" class="clearfloat">

	<div id="lead" class="clearfloat">
	
		<div class="left">
		
		<?php
		//Interacts with control panel to determine lead category
			$bm_categories = get_categories('hide_empty=0');
			$mim_feature = get_settings( "mim_feature" );
			if( $mim_feature == 0 ) { $mim_feature = $bm_categories[ 0 ]->cat_ID; }
			query_posts( 'showposts=1&cat=' . $mim_feature );
		 	while (have_posts()) : the_post(); 
		?>
	
	
	<?php
	
	$values = get_post_custom_values("Image");
	
	if (isset($values[0])) {						
	?>
		<a href="<?php the_permalink() ?>" rel="bookmark"><img src="<?php echo bloginfo('template_url'); ?>/scripts/timthumb.php?src=/<?php
			$values = get_post_custom_values("Image"); echo $values[0]; ?>&amp;w=200&amp;h=210&amp;zc=1" alt="<?php the_title(); ?>" /></a>
	<?php } ?>
	
	
		</div>
	
		<div class="right">
			<h3><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a>
			<span class="commentcount">(<?php comments_popup_link('0', '1', '%'); ?>)</span></h3>
			<div class="date"><?php the_date(); ?> &bull; Category: <?php the_category(', ') ?></div>
			<div class="bigger"><?php the_excerpt(); ?></div>
			<a href="<?php the_permalink() ?>" rel="bookmark" id="fullstory"><?php the_title(); ?></a>
			<?php endwhile; ?>
		</div>
		
	</div><!--END LEAD-->


	<div id="recent">
		<h3>Recent <span>Stories</span></h3>
		<ul>
		<?php query_posts('showposts=5'); ?>
		<?php while (have_posts()) : the_post(); ?>
			<li><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></li>
		<?php endwhile; ?>
		</ul>
	</div>
</div><!--END UPPER-->


<?php
	// Interacts with control panel to determine carousel category
	$bm_carousel = get_settings( "mim_carousel" );
	$bm_carouselCount = get_settings( "bm_carouselCount" );
	if( $bm_carouselCount == "" ) { $bm_carouselCount = 12; }
	 
	if( $bm_carousel > 0 ) {
?>

<div id="carousel">

	<ul id="mycarousel" class="jcarousel-skin-tango">
    <?php
		query_posts('showposts=' . $bm_carouselCount . '&cat=' . $bm_carousel );
		while (have_posts()) {
			the_post();
		?>
		<li>
			<a href="<?php the_permalink() ?>" rel="bookmark"><img src="<?php bloginfo('template_url'); ?>/scripts/timthumb.php?src=/<?php $values = get_post_custom_values("Image"); echo $values[0]; ?>&amp;w=155&amp;h=82&amp;zc=1" alt="<?php the_title(); ?>" /><?php the_title(); ?></a>
		</li>
		<?php } ?>
		
	</ul>
  
</div>
 
<?php } ?>

<div class="stripes clearfloat">

<?php get_sidebar(); ?>

<div id="content">



<?php 
// Determines which categories will display in middle column

$bm_boxColor[] = "green";
$bm_boxColor[] = "red";
$bm_boxColor[] = "grey";

$bm_currentColor = -1;

$postcat = get_settings( "mim_homeCat" );

if( ! is_array( $postcat ) ) {
	foreach ( $bm_categories as $b ) {
		$postcat[] = $b->cat_ID;
	}	
}

$bm_baseURL = get_bloginfo('url');

forEach( $postcat as $bm_pC ) {

	$bm_currentColor ++;
	if( $bm_currentColor >= count( $bm_boxColor ) ) { $bm_currentColor = 0; }
	
	$catDetails = bm_catProperties( $bm_pC );	
	
	//Choose how many total headlines you want in each category box
	query_posts('showposts=4&cat=' . $bm_pC ); ?>
	
	<div class="whitebox catnews">
	
	<h3 class="<?php echo $bm_boxColor[ $bm_currentColor ]; ?>"><a href="<?php echo get_category_link( $catDetails->cat_ID ); ?>"><?php echo $catDetails->cat_name ?>&raquo;</a></h3>
	
		<?php
		$count = 0;
		while (have_posts()) {
		
			the_post();
			 	
			if( $count == 0 ) { ?>
			
				<div class="clearfloat">
				
		  		<h4><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a> 
				<span class="commentcount">(<?php comments_popup_link('0', '1', '%'); ?>)</span></h4>
		 		<?php $values = get_post_custom_values("Image");
				if (isset($values[0])) {						
				?>
					<a href="<?php the_permalink() ?>" rel="bookmark"><img src="<?php echo bloginfo('template_url'); ?>/scripts/timthumb.php?src=/<?php
						$values = get_post_custom_values("Image"); echo $values[0]; ?>&amp;w=70&amp;h=70&amp;zc=1" alt="<?php the_title(); ?>" /></a>
				<?php } ?>
				
				<?php the_excerpt(); ?>
				
				</div>
				 		
				<ul class="headlines">

				<?php 
			} else { 
				?>
				
	  			<li><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a> </li>	
			
				<?php	
			}
				?>
				
 				
 				
				<?php
				
			$count ++;
		}
		
		if( $count > 0 ) { echo "</ul>"; }
		?>
 	
	 </div>
 		
<?php } ?>
 

</div><!--END CONTENT-->

<?php include("ads.php"); ?>
</div><!--END STRIPES-->
	

<?php get_footer(); ?>

