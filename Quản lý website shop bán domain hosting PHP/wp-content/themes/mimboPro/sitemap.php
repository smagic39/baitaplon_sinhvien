<?php
/*
Template Name: Sitemap
*/
?>

<?php get_header(); ?>
<div id="breadcrumbs"></div>

<div class="clearfloat stripes">
<?php get_sidebar(); ?>

	
		<div id="content">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
			<h2><?php the_title(); ?></h2>	
			<div class="entry">
			<br />		
				<h4>Pages</h4>
				<ul class="bullets">
					<?php wp_list_pages('title_li='); ?>
				</ul>
				<h4>Browse Last 50 Posts</h4>
				<ul class="bullets">
					<?php $archive_query = new WP_Query('showposts=50');
						while ($archive_query->have_posts()) : $archive_query->the_post(); ?>
					<li><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a> <span class="commentcount">(<?php comments_number('0', '1', '%'); ?>)</span>
					
				</li>
					<?php endwhile; ?>
				</ul>
				<h4>Monthly archives</h4>
				<ul class="bullets">
					<?php wp_get_archives('type=monthly'); ?>
				</ul>
				<h4>Categories</h4>
				<ul class="bullets">
					<?php wp_list_categories('title_li=0'); ?>
				</ul>
				<h4>Feeds &amp; Syndication</h4>
				<ul class="bullets">
					<li><a href="<?php bloginfo('rdf_url'); ?>" title="RDF/RSS 1.0 feed"><acronym title="Resource Description Framework">RDF</acronym>/<acronym title="Really Simple Syndication">RSS</acronym> 1.0 feed</a></li>
					<li><a href="<?php bloginfo('rss_url'); ?>" title="RSS 0.92 feed"><acronym title="Really Simple Syndication">RSS</acronym> 0.92 feed</a></li>
					<li><a href="<?php bloginfo('rss2_url'); ?>" title="RSS 2.0 feed"><acronym title="Really Simple Syndication">RSS</acronym> 2.0 feed</a></li>
					<li><a href="<?php bloginfo('atom_url'); ?>" title="Atom feed">Atom feed</a></li>
				</ul>
			</div>
		
				
			<?php endwhile; endif; ?>

		</div>

</div>
	

<?php get_footer(); ?>