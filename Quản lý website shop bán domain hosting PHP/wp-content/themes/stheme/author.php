<?php get_header(); ?>

<div id="content_top">
<?php include (TEMPLATEPATH . '/lsidebar.php'); ?>

<div id="blog">

<?php
if(isset($_GET['author_name'])) :
$curauth = get_userdatabylogin(get_the_author_login());
else :
$curauth = get_userdata(intval($author));
endif;
?>

<div class="box">
<div id="author">
<?php echo get_avatar( $curauth->ID , 36 ); ?>
<div class="posttitle">
<h2><?php echo $curauth->display_name; ?><?php _e('\'s page', 'stheme') ?></h2>
<p class="postmeta"><span class="author"><a href="<?php $curauth->user_url; ?>"><?php _e('Homepage', 'stheme') ?></a></span><span class="edit"><a href="mailto:<?php echo antispambot($curauth->user_email); ?>"><?php _e('E-mail', 'stheme') ?></a></span></p></div>
</div>
<?php echo $curauth->description; ?>
</div>

	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
 
<div class="box">
<div class="entry" id="post-<?php the_ID(); ?>">
<div class="posttime"><div class="d"><?php the_time("d"); ?></div><div class="m"><?php the_time("M"); ?></div></div>
<div class="posttitle">
<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
<p class="postmeta"><span class="comment"><?php comments_popup_link(__('No comments', 'stheme'), __('1 Comment', 'stheme'), __('% Comments', 'stheme')); ?></span><span class="category"><?php the_category(', ') ?></span><span class="date"><?php the_time(__('F jS, Y', 'stheme')) ?></span><span class="author"><?php the_author() ?> </span><?php edit_post_link(__('Edit' ,'stheme') , '<span class="edit">', '</span>'); ?></p></div>
		
			<div class="post"><?php the_content(__('More &raquo;' ,'stheme')) ?></div>

<p class="tags"><?php the_tags(' ', ', ', ' '); ?></p></div></div>
		<?php endwhile; ?>

	<?php if (function_exists('wp_pagenavi')) : ?>
		<div id="pagenavi">
			<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
		</div>
	<?php else : ?>
		<div class="navigation">
			<div class="alignleft"><?php next_posts_link(__('&laquo; Older Entries')) ?></div>
			<div class="alignright"><?php previous_posts_link(__('Newer Entries &raquo;')) ?></div>
		</div>
	<?php endif; ?>

	<?php else : ?>

		<h2 class="center"><?php _e('Not Found', 'stheme') ?></h2>
		<p class="center"><?php _e('Sorry, but you are looking for something that is not here.', 'stheme') ?></p>
	<?php endif; ?>

</div>

<?php get_sidebar(); ?>
		</div>
<div id="content_foot"></div>
	</div>	
<div class="clear"></div>	
</div>

<?php get_footer(); ?>