<?php get_header(); ?>

<div id="content_top">
		<?php include (TEMPLATEPATH . '/lsidebar.php'); ?>
<div id="blog">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div class="box">
<div class="entry" id="post-<?php the_ID(); ?>">
<div class="posttime"><div class="d"><?php the_time("d"); ?></div><div class="m"><?php the_time("M"); ?></div></div>
<div class="posttitle">
<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
<p class="postmeta"><span class="comment"><a href="#respond" title="<?php _e('Leave a reply', 'stheme') ?>"><?php _e('Reply', 'stheme') ?></a></span><span class="category"><?php the_category(', ') ?></span><span class="date"><?php the_time(__('F jS, Y', 'stheme')) ?></span><span class="author"><?php the_author_posts_link(); ?></span><?php edit_post_link(__('Edit'), '<span class="edit">', '</span>'); ?></p></div>

		<div class="post"><?php the_content(__('More &raquo;' ,'stheme')); ?></div>
<?php wp_link_pages(array('before' => __('<p><strong>Pages:</strong>' ,'stheme'), 'after' => '</p>', 'next_or_number' => 'number')); ?>
<P class="tags"><?php the_tags(' ', ', ', ' '); ?></P></div></div> 
	<div class="clear"></div>

<div class="box">
	<div class="postmetadata">
	<small>
	<?php _e('This entry was posted on', 'stheme') ?> <?php the_time('M jS, Y') ?><?php _e(' at ', 'stheme') ?><?php the_time() ?><?php _e(' and is filed under ', 'stheme') ?><?php the_category(', ') ?><?php _e('.', 'stheme') ?>
<?php _e('You can follow any responses to this entry through the ', 'stheme') ?>
	<?php comments_rss_link('RSS 2.0'); ?><?php _e('.', 'stheme') ?>
	<?php if (('open' == $post-> comment_status) && ('open' == $post->ping_status)) {
							// Both Comments and Pings are open ?>
	<?php _e('You can ', 'stheme') ?><a href="#respond"><?php _e('Leave a response', 'stheme') ?></a><?php _e(', or ', 'stheme') ?><a href="<?php trackback_url(); ?>" rel="trackback"><?php _e('Trackback', 'stheme') ?></a><?php _e('.', 'stheme') ?>
						<?php } elseif (!('open' == $post-> comment_status) && ('open' == $post->ping_status)) {
							// Only Pings are Open ?>
		<?php _e('Responses are currently closed, but you can ', 'stheme') ?><a href="<?php trackback_url(); ?> " rel="trackback"><?php _e('Trackback.', 'stheme') ?></a><?php _e('.', 'stheme') ?>
						<?php } elseif (('open' == $post-> comment_status) && !('open' == $post->ping_status)) {
							// Comments are open, Pings are not ?>
		<?php _e('You can ', 'stheme') ?><a href="#respond"><?php _e('Leave a response', 'stheme') ?></a><?php _e('.', 'stheme') ?>
						<?php } elseif (!('open' == $post-> comment_status) && !('open' == $post->ping_status)) {
							// Neither Comments, nor Pings are open ?>
		Both comments and pings are currently closed.
						<?php }  ?>
					</small>
	</div>
</div>
<div class="clear"></div>

<div class="box">
	<div id="comments"><?php comments_template('', true); ?>
</div>
</div>

	<?php endwhile; else: ?>

<div class="box"><p><?php _e('Sorry, no posts matched your criteria.', 'stheme') ?></p></div>

<?php endif; ?>
</div>

<?php get_sidebar(); ?>

		</div>
<div id="content_foot"></div>
	</div>
<div class="clear"></div>	
</div>

<?php get_footer(); ?>