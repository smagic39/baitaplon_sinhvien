VERSION DATE       TYPE   CHANGES
2.2.8.4 2010/04/16 FIXED  Fix post paragraph margin.
				   NEW	  Add Belarusian supported.
2.2.8.3 2009/05/10 FIXED  Fix footer style in CSS.
2.2.8.2 2009/05/01 FIXED  Fix comment style at Single page.
2.2.8.1 2009/05/01 FIXED  Fix some width.
				   FIXED  Fix pagelist code in "header.php".
2.2.7   2009/04/22 FIXED  Fix some width.
				   NEW    Added update tip at Admin Option Page.
				   NEW    Added Roundborder option at Admin Option Page.
				   NEW    Added Logo image option at Admin Option Page.
				   NEW    Added Logo image and PSD file.
2.2.5   2009/04/17 FIXED  Long catelogy bug.
				   FIXED  Some translate problems in  Chinese.
				   NEW    Added H1,H2,Table,Pre CSS style
2.2.0   2009/04/13 FIXED  Posttitle Code.
				   FIXED  Some bugs in Safari.
2.1.0   2009/04/07 FIXED  RSS image link.
				   FIXED  Fixed logo image instead of blog title and tag linesome.
				   DELETE Remove Twitter Code,install Twitter in Admin Option Page.
				   DELETE Remove logo.gif and logo.psd.
                   NEW    Added Admin Option Page and Notice option.
                   NEW    Created this CHANGLOG file.