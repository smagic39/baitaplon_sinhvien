<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<?php $the_title = wp_title(' - ', false); if ($the_title != '') : ?>
    <title><?php echo wp_title('',false); ?> | <?php bloginfo('name'); ?></title>
<?php else : ?>
    <title><?php bloginfo('name'); ?></title>
<?php endif; ?>
<?php if (is_home()){
    $description = " ";
} elseif (is_single()){
    if ($post->post_excerpt) {
        $description     = $post->post_excerpt;
    } else {
        $description = substr(strip_tags($post->post_content),0,220);
    }
}
?>
<meta name="description" content="<?php echo $description; ?>" />
<meta http-equiv="content-type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<!--[if IE 6]>
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/IE6.css" type="text/css" media="screen" />
<![endif]-->
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.1/jquery.min.js"></script>
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_head(); ?>
<?php $options = get_option('classic_options'); ?>
<?php if($options['roundborder']) : ?>
<style type="text/css" media="screen">
#rssList { -moz-border-radius:9px; -khtml-border-radius:9px; -webkit-border-radius:9px; border-radius:9px; }
.box { -moz-border-radius:9px; -khtml-border-radius:9px; -webkit-border-radius:9px; border-radius:9px; }
</style>
<?php endif; ?>
<?php if($options['logo']) : ?>
<style type="text/css" media="screen">
#logo h1 { text-indent:-999em; margin:0px; }
#logo h1 a { display:block; background:url(<?php bloginfo('template_directory'); ?>/images/megahost.gif) no-repeat; height:140px; width:938px; outline:none; }
#logo .description { display:none; }
</style>
<?php endif; ?>
</head>
<body>
<div id="main"></div>
<div class="top_bg"></div>
<div id="header">
<div id="logo">
	<h1><a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a></h1>
	<div class="description"><?php bloginfo('description'); ?></div>
	</div>
</div>
<ul id="subnav">
<li class="<?php if (((is_home()) && !(is_paged())) or (is_archive()) or (is_single()) or (is_paged()) or (is_search())) { ?>current_page_item<?php } else { ?>page_item<?php } ?> active">
<a href="<?php echo get_settings('home'); ?>"><em><?php _e('Home', 'stheme') ?></em><?php echo $langblog;?></a></li>
<?php wp_list_pages('depth=1&title_li=&link_before=<em class="">&link_after=</em>'); ?>
<?php if(!$_COOKIE['show_sidebar']=='no'):?>
<li class="close-sidebar" title=""><a href="#"><em><?php _e('Sidebar Off', 'stheme') ?></em></a></li>
<li class="show-sidebar" style="display:none;" title=""><a href="#"><em><?php _e('Sidebar On', 'stheme') ?></em></a></li>
<?php else: ?>
<li class="close-sidebar" style="display:none;" title=""><a href="#"><em><?php _e('Sidebar Off', 'stheme') ?></em></a></li>
<li class="show-sidebar" title=""><a href="#"><em><?php _e('Sidebar On', 'stheme') ?></em></a></li>
<?php endif;?>
<?php if($_COOKIE['show_sidebar']=='no'):?>
<style type="text/css">
#blog {width:770px;}
.box {width:760px;}
#rsidebar {display:none;}
</style>
<?php endif;?>
</ul>
<script type="text/javascript" language="javascript">
/* <![CDATA[ */
$(document).ready(function(){
(function(){
function SetCookie(c_name,value,expiredays){
var exdate=new Date();
exdate.setDate(exdate.getDate()+expiredays);
document.cookie=c_name+"="+escape(value)+((expiredays==null)?"":";expires="+exdate.toGMTString())+";path=/"; 
}
window['RootCookies'] = {};
window['RootCookies']['SetCookie'] = SetCookie;
})();
$('.close-sidebar').click(function() { 
RootCookies.SetCookie('show_sidebar', 'no', 1);
$('.close-sidebar').hide();
$('.show-sidebar').show();
$('#rsidebar').fadeOut(1000);
$('#blog').animate({width: "770px"}, 1000);
$('.box').animate({width: "760px"}, 1000);
});
$('.show-sidebar').click(function() {  
RootCookies.SetCookie('show_sidebar', 'no', -1);       
$('.show-sidebar').hide();
$('.close-sidebar').show();
$('#blog').animate({width: "550px"}, 1000);
$('.box').animate({width: "530px"}, 1000);
$('#rsidebar').animate({opacity: 1.0}, 500).fadeIn('slow');}); 
});
/* ]]> */
</script>
<script type="text/javascript" language="javascript">
/* <![CDATA[ */
$(document).ready(function(){
$('#rssfeed, #rssList').mouseover(function() {
if(window.willhide) clearTimeout(window.willhide);
$('#rssList').fadeIn(500);
});
$('#rssfeed,#rssList').mouseout(function() {
window.willhide = setTimeout(function() {
$('#rssList').fadeOut(500);
}, 750);
});
});
/* ]]> */
</script>
<div id="wrapper">
<div id="stheme_left">
<div class="sblk1"><div class="sblk2">
<a href="<?php echo get_settings('home'); ?>" title="<?php _e('Home', 'stheme') ?>"><img src="<?php bloginfo('template_directory'); ?>/images/home.gif" alt=""></img></a></div></div>
<div class="sblk1"><div class="sblk2">
<a href="mailto:<?php echo get_option('admin_email'); ?>" title="<?php _e('E-mail Me', 'stheme') ?>"><img src="<?php bloginfo('template_directory'); ?>/images/smg.gif" alt=""></img></a></div></div>
<div class="sblk1"><div class="sblk2">
<a href="<?php echo get_option('siteurl'); ?>/wp-admin/" title="<?php _e('Admin Login', 'stheme') ?>"><img src="<?php bloginfo('template_directory'); ?>/images/slogout.gif" alt=""></img></a></div></div>
</div>

<div id="content">