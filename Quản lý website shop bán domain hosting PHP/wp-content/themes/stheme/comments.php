<?php // Do not delete these lines
	if ('comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

	if (!empty($post->post_password)) { // if there's a password
		if ($_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) {  // and it doesn't match the cookie
			?>

			<p class="nocomments"><?php _e('This post is password protected. Enter the password to view comments.', 'stheme') ?></p>

			<?php
			return;
		}
	}

	/* This variable is for alternating comment background */
	$oddcomment = 'class="alt" ';
?>

<!-- You can start editing here. -->

<?php if ( have_comments() ) : ?>
	<h3><?php comments_number(__('No Responses' ,'stheme'), __('One Response' ,'stheme'),__('% Responses' ,'stheme'));?> <?php _e('to', 'stheme') ?> &#8220;<?php the_title(); ?>&#8221;</h3>
<div class="navigation">
<div class="alignleft"><?php previous_comments_link() ?></div>
<div class="alignright"><?php next_comments_link() ?></div>
</div>
<ul class="commentlist">
    <?php wp_list_comments('type=comment&callback=mytheme_comment'); ?>
</ul>
<ul class="trackback">
			<?php wp_list_comments('type=pings'); ?>
</ul>

 <?php else : // this is displayed if there are no comments so far ?>

	<?php if ('open' == $post->comment_status) : ?>
		<!-- If comments are open, but there are no comments. -->

	 <?php else : // comments are closed ?>
		<!-- If comments are closed. -->
		<p class="nocomments"><?php _e('Comments are closed.', 'stheme') ?></p>

	<?php endif; ?>
<?php endif; ?>


<?php if ('open' == $post->comment_status) : ?>

<div id="respond">
<h3><?php comment_form_title( __('Add reply' ,'stheme'), __('to %s' ,'stheme')); ?>
</h3>

<div id="cancel-comment-reply"> 
	<small><?php cancel_comment_reply_link() ?></small>
</div>

<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
<p><?php _e('You must be ', 'stheme') ?><a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>"><?php _e('logged in', 'stheme') ?></a><?php _e(' to post a comment.', 'stheme') ?></p>
<?php else : ?>

<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">

<?php if ( $user_ID ) : ?>

<p><?php _e('Logged in as ', 'stheme') ?><a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a><?php _e('.', 'stheme') ?> <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?action=logout" title="Log out of this account"><?php _e('Logout &raquo;', 'stheme') ?></a></p>

<?php else : ?>

<?php if ( $comment_author != "" ) : ?>
	<script type="text/javascript">function setStyleDisplay(id, status){document.getElementById(id).style.display = status;}</script>
	<div class="form_row small">
		<?php printf(__('Welcome back <strong>%s</strong>.', 'stheme'), $comment_author) ?>
		<span id="show_c"><a href="javascript:setStyleDisplay('c','');setStyleDisplay('show_c','none');setStyleDisplay('hide_c','');"><?php _e('Change &raquo;', 'stheme'); ?></a></span>
 		<span id="hide_c"><a href="javascript:setStyleDisplay('c','none');setStyleDisplay('show_c','');setStyleDisplay('hide_c','none');"><?php _e('Close &raquo;', 'stheme'); ?></a></span>
	</div>
<?php endif; ?>

<div id="c">
<div id="cleft"><p><input type="text" name="author" id="author" value="<?php echo $comment_author; ?>" size="22" tabindex="1" />
<label for="author"><small><?php _e('Name ', 'stheme') ?><?php if ($req) echo "(*)"; ?></small></label></p>

<p><input type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" size="22" tabindex="2" />
<label for="email"><small><?php _e('Mail (will not be published)', 'stheme') ?> <?php if ($req) echo "(*)"; ?></small></label></p></div>
<div id="cright">
<p><input type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" size="22" tabindex="3" />
<label for="url"><small><?php _e('Website', 'stheme') ?></small></label></p></div>
</div>

<?php if ( $comment_author != "" ) : ?>
	<script type="text/javascript">setStyleDisplay('hide_c','none');setStyleDisplay('c','none');</script>
<?php endif; ?>

<?php endif; ?>

<p><textarea name="comment" id="comment" cols="100%" rows="10" tabindex="4"></textarea></p>

<p><input name="submit" type="submit" id="submit" tabindex="5" value="<?php _e('Submit', 'stheme') ?>(Ctrl+Enter)" />
<input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" /><?php comment_id_fields(); ?>
</p>
<?php do_action('comment_form', $post->ID); ?>

</form>

<script type="text/javascript" language="javascript">
document.getElementById("comment").onkeydown = function (moz_ev)
        {
                var ev = null;
                if (window.event){
                        ev = window.event;
                }else{
                        ev = moz_ev;
                }
                if (ev != null && ev.ctrlKey && ev.keyCode == 13)
                {
                        document.getElementById("submit").click();
                }
        }
</script>

<?php endif; // If registration required and not logged in ?>
</div>

<?php endif; // if you delete this the sky will fall on your head ?>
