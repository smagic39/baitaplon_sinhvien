﻿<?php
/*------------------------------------------
| CLASS FOR DB HANDLE
-------------------------------------------*/
	
// Check for Security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}

class database
{

    private $obj = array ( 
							"sql_database"     => MYSQL_DATABASE,
						    "sql_user"         => MYSQL_USERNAME,
						    "sql_pass"         => MYSQL_PASSWORD,
						    "sql_host"         => MYSQL_HOST,
						    "sql_port"         => MYSQL_PORT,
						    "persistent"       => MYSQL_PERSISTENT,
						    "sql_tbl_prefix"   => MYSQL_PREFIX,
						    "cached_queries"   => array(),
						    "shutdown_queries" => array() ,
						    "debug"            => 0,
						    "use_shutdown"     => 1,
						 );
     
     private $error             = "";
     private $sql               = "";
     private $cur_query         = "";
     private $query_id          = "";
     private $connection_id     = "";
     private $query_count       = 0;
     private $return_die        = 0; // return error or not
     private $manual_addslashes = 0;
     private $is_shutdown       = 0;
     private $prefix_changed    = 0;
     private $record_row        = array();
     private $loaded_classes    = array();
     private $connect_vars      = array();
     private $force_data_type   = array();
	 
	 private $failed            = 0;
    
	/*--------------------------
	| CONNECT TO DB
	-----------------------------*/
	function database()
	{
     	$this->connect_vars = array();
		$this->connect();
	}
     
	function connect()
	{
     	//--------------------------
     	// Connect
     	//--------------------------
    	if ($this->obj['persistent'])
    	{
    	    $this->connection_id = mysql_pconnect( $this->obj['sql_host'] ,
												   $this->obj['sql_user'] ,
												   $this->obj['sql_pass'] 
												);
        }
        else
        {
			$this->connection_id = mysql_connect( $this->obj['sql_host'] ,
												  $this->obj['sql_user'] ,
												  $this->obj['sql_pass'] 
												);
		}
		
		if (!$this->connection_id)
		{
			$this->fatal_error();
			return FALSE;
		}
		
        if (!mysql_select_db($this->obj['sql_database'], $this->connection_id))
        {
        	$this->fatal_error();
        	return FALSE;
        }
        
        return TRUE;
    }
	
	/*-----------------------------
	| INFORM ERRORS
	-------------------------------*/
    function fatal_error($the_error="")
    {
		// Are we simply returning the error?
    	if ($this->return_die == 1)
    	{
    		$this->error    = mysql_error();
    		$this->error_no = mysql_errno();
    		$this->failed   = 1;
    		return;
    	}
		
    	/*--------------------------------------------------------------
		| SHOULD REMOVE WHEN PUT IN LIVE
		+-------------------------------------------------------------*/
    	$the_error .= "\n\nMySQL Error: ".mysql_error()."\n";
    	$the_error .= "Error Code: ".$this->error_no."\n";
    	$the_error .= "Date: ".date("l dS of F Y h:i:s A");
    	
    	$out = "<html><head><title>HOANGCN02 FRAMEWORK: Database Error</title>
    		   <style>P,BODY{ font-family:arial,sans-serif; font-size:11px; }</style></head><body>
    		   &nbsp;<br><br><blockquote><b>Database Error.</b><br>
    		   <br><br><b>Error Return:</b><br>
    		   <form name='mysql'><textarea rows=\"15\" cols=\"60\">".htmlspecialchars($the_error)."</textarea></form><br>Please try late!</blockquote></body></html>";
        echo($out);
		
		
        die();
    }
	
	/*-------------------------
	| CLOSE CONNECT TO DB
	---------------------------*/
    function close()
    { 
    	if ($this->connection_id) return @mysql_close($this->connection_id);
    }
	
	/*------------------------
	| SELECT DB
	--------------------------*/
    function simple_select($get, $table, $where="", $order="", $limit="")
    {
    	$this->cur_query = "SELECT $get FROM ".$this->obj['sql_tbl_prefix']."$table";
    	
    	if ($where) $this->cur_query .= " WHERE ".$where;
		if ($order) $this->cur_query .= " ORDER BY ".$order;
		if ($limit) $this->cur_query .= " LIMIT ".$limit;
		
		return $this->cur_query;
    }
	
	/*---------------------
	| DELETE
	---------------------*/
    function do_delete($tbl, $where)
    {
    	$this->cur_query = "DELETE FROM ".$this->obj['sql_tbl_prefix']."$tbl";
    	
    	if ($where) $this->cur_query .= " WHERE $where";
		
		return $this->query($this->cur_query);
    }
	
	/*-----------------
	| INSERT
	--------------------*/
	function do_insert($tbl, $arr, $raw = FALSE)
    {
		if ($raw)
		{
			$dba = $this->compile_insert_string_raw($arr);
		}
		else
		{
    		$dba = $this->compile_insert_string($arr);
		}
		
		if ($raw)
		{
			return mysql_query("INSERT INTO ".$this->obj['sql_tbl_prefix']."$tbl ({$dba['FIELD_NAMES']}) VALUES({$dba['FIELD_VALUES']})", $this->connection_id);
		}
		else
		{
    		return $this->query("INSERT INTO ".$this->obj['sql_tbl_prefix']."$tbl ({$dba['FIELD_NAMES']}) VALUES({$dba['FIELD_VALUES']})");
		}
    }
    
	/*------------------------
	|  PARSE DATA FOR INSERT
	--------------------------*/
    function compile_insert_string($data)
	{
		$field_names  = "";
		$field_values = "";
		
		foreach ($data as $k => $v)
		{
			if (!$this->manual_addslashes) $v = preg_replace( "/'/", "\\'", $v );
			
			$field_names  .= "$k,";
			
			//-----------------------------------------
			// Forcing data type?
			//-----------------------------------------
			if ( $this->force_data_type[$k] )
			{
				if ($this->force_data_type[ $k ] == 'string')
				{
					$field_values .= "'$v',";
				}
				else if ($this->force_data_type[ $k ] == 'int')
				{
					$field_values .= intval($v).",";
				}
				else if ($this->force_data_type[ $k ] == 'float')
				{
					$field_values .= floatval($v).",";
				}
			}
			
			//-----------------------------------------
			// No? best guess it is then..
			//-----------------------------------------
			else
			{
				//if (is_numeric($v) and intval($v) == $v)
				//{
					//$field_values .= $v.",";
				//}
				//else
				//{
					$field_values .= "'$v',";
				//}
			}
		}
		
		$field_names  = preg_replace( "/,$/" , "" , $field_names  );
		$field_values = preg_replace( "/,$/" , "" , $field_values );
		
		return array( 'FIELD_NAMES'  => $field_names,
					  'FIELD_VALUES' => $field_values,
					);
	}
	
	
	/*---------------------------------------------------
	|  PARSE DATA FOR INSERT - NO CLEAN SPECIAL CHARS
	----------------------------------------------------*/
    function compile_insert_string_raw($data)
	{
		$field_names  = "";
		$field_values = "";
		
		foreach ($data as $k => $v)
		{
			$field_names  .= "$k,";
			$field_values .= "'$v',";			
		}
		$field_names  = preg_replace( "/,$/" , "" , $field_names  );
		$field_values = preg_replace( "/,$/" , "" , $field_values );
		return array( 
					    'FIELD_NAMES'  => $field_names,
					    'FIELD_VALUES' => $field_values,
					);
	}
	
	
	
	/*-------------------
	| UPDATE
	--------------------*/
	function do_update($tbl, $arr, $where="", $raw = FALSE)
    {
		if ($raw)
		{
			$dba = $this->compile_db_update_string_raw($arr);
		}
		else
		{
    		$dba = $this->compile_db_update_string($arr);
    	}
    	$query = "UPDATE ".$this->obj['sql_tbl_prefix']."$tbl SET $dba";
    	
    	if ($where) $query .= " WHERE ".$where;
		
		if ($raw)
		{
			return mysql_query($query, $this->connection_id);
		}
		else
		{
    		return $this->query( $query );
		}
    }
	
	/*---------------------------------
	| COMPILE DATA TO UPDATE
	----------------------------------*/
	function compile_db_update_string($data)
	{
		$return_string = "";
		
		foreach ($data as $k => $v)
		{
			if (!$this->manual_addslashes) $v = preg_replace( "/'/", "\\'", $v );
			
			//-----------------------------------------
			// Forcing data type?
			//-----------------------------------------
			if ( $this->force_data_type[$k] )
			{
				if ($this->force_data_type[$k] == 'string')
				{
					$return_string .= $k . "='".$v."',";
				}
				else if ($this->force_data_type[ $k ] == 'int')
				{
					$return_string .= $k . "=".intval($v).",";
				}
				else if ($this->force_data_type[ $k ] == 'float')
				{
					$return_string .= $k . "=".floatval($v).",";
				}
			}
			
			//-----------------------------------------
			// No? best guess it is then..
			//-----------------------------------------
			else
			{
				//if (is_numeric($v) and intval($v) == $v)
				//{
					//$return_string .= $k . "=".$v.",";
				//}
				//else
				//{
					$return_string .= $k . "='".$v."',";
				//}
			}
		}
		
		$return_string = preg_replace( "/,$/" , "" , $return_string );
		
		return $return_string;
	}
	
	
		/*---------------------------------
	| COMPILE DATA TO UPDATE
	----------------------------------*/
	function compile_db_update_string_raw($data)
	{
		$return_string = "";
		
		foreach ($data as $k => $v)
		{
			$return_string .= $k . "='".$v."',";
		}
		$return_string = preg_replace( "/,$/" , "" , $return_string );				
		return $return_string;
	}
	
	
	/*------------------------------
	| QUERY DB
	--------------------------------*/
    function query($the_query, $bypass=0)
	{
        $this->query_id = mysql_query($the_query, $this->connection_id);
      	
      	//--------------------------------------
      	// Reset array...
      	//--------------------------------------
      	$this->force_data_type = array();
      	
        if (!$this->query_id) $this->fatal_error("MySQL Query: $the_query");
		
		$this->query_count++;
        
        $this->obj['cached_queries'][] = $the_query;
        
        return $this->query_id;
    }
	
	
	/*-------------------------------
	| GET ARRAY RESULT OF QUERY 
	--------------------------------*/
    function fetch_array($query_id = "")
	{
    	if ($query_id == "")
		{
			$query_id = $this->query_id;
    	}
		
		$this->record_row = array();
        
		if ($query_id)
		{
			$this->record_row = mysql_fetch_array($query_id, MYSQL_ASSOC);
		}
        
        return $this->record_row;
    }
	
	/*-----------------------------------
	| GET ARRAY OF ROWS RESULT OF QUERY 
	-------------------------------------*/
    function fetch_row($query_id = "")
	{
    	if ($query_id == "")
		{
			$query_id = $this->query_id;
    	}
		
		$record_row = array();
       
	    if ($query_id)
		{
			$record_row = mysql_fetch_row($query_id);        
		}
        
		return $record_row;
    }
	
	
	/*-----------------------------------
	| GET ARRAY OF ASSOC RESULT OF QUERY 
	-------------------------------------*/
    function fetch_assoc($query_id = "")
	{
    	if ($query_id == "")
		{
			$query_id = $this->query_id;    	
		}
		
		$record_row = array();		
       
	    if ($query_id)
		{
			$record_row = mysql_fetch_assoc($query_id);        
		}
		
        return $record_row;
    }
	
	
	 /*--------------------------------------------------
	* FUNCTION GET DATA FROM DB BASES ON INPUT QUERY SENTENCE
	* Params :
				+ $sql : query sentence
				+ $ftype : kind of result should be fetched
				           1 : fetch_array
				           2 : fetch_rows
						   0 : fetch_assoc
						   default is fetch_assoc
	* Returns : array of data from DB
	----------------------------------------------------*/
	function getData($sql, $ftype=0)
	{
		$result = array();
		$query = $this->query($sql);
		switch ($ftype)
		{
			case 0 : $result = $this->fetch_assoc($query); break;
			case 1 : $result = $this->fetch_array($query); break;
			case 2 : $result = $this->fetch_row($query); break;
			default : $result = $this->fetch_assoc($query);
		}
		return $result;
	}
	
	
	/*-------------------------------------------------
	* FUNCTION CHECK IF THERE'S ANY DATA IN TABLE
	* Params :
				(string) $table : name of table-exclude prefix
				(string) $condition : would be placed in where of sql
	* Return : number of records that is available
	----------------------------------------------------------*/				
	function isDataAvailable($table,$condition='1')
	{   
		$isAvailable = '';
		$queryCount = $this->simple_select("COUNT(*) AS numofrow", $table, $condition);
		$resultCount = $this->query($queryCount);
		$rowCount = $this->fetch_assoc($resultCount);
		$isAvailable = $rowCount['numofrow'];
		return $isAvailable;
	}
	
	
		
}


?>