<?php
/*--------------------------------------
| CLASS FOR TIME HANDLE
----------------------------------------*/

// Check for Security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}


class timer
{
	/*--------------------------------------------
	| CONVERT UNIX TIME TO DD/MM/YYYYY
	---------------------------------------------*/
	function unixtime_2_ddmmyyyy($unix_time, $separate="/")
	{
		$date = date("dmY",$unix_time);
		
		$day = substr($date, 0, 2);
		$month = substr($date, 2, 2);
		$year = substr($date, 4, 4);
		
		return $day.$separate.$month.$separate.$year;
	}
	
	
	/*------------------------------------
	| CONVERT UNIX TIME TO MM/YYYYY
	-------------------------------------*/
	function unixtime_2_mmyyyy($unix_time, $separate="/")
	{
		$date = date("mY",$unix_time);

		$month = substr($date, 0, 2);
		$year = substr($date, 2, 4);
		
		return $month.$separate.$year;
	}
	
	
	/*----------------------------------------
	| CONVERT DD/MM/YYYYY TO UNIX TIME
	-----------------------------------------*/
	function ddmmyyyy_2_unixtime($ddmmyyyy, $hhmmss="00:00:00")
	{
		$day = intval(substr($ddmmyyyy, 0, 2));
		$mon = intval(substr($ddmmyyyy, 3, 2));
		$year = intval(substr($ddmmyyyy, 6, 4));
		
		$hour = intval(substr($hhmmss, 0, 2));
		$min = intval(substr($hhmmss, 3, 2));
		$sec = intval(substr($hhmmss, 6, 2));
		
		if ( ($mon <= 0) || ($mon > 12) ) $mon = 1;
		if ( ($year <= 0) || ($day > 9999) ) $year = 1970;
		
		switch ($mon)
		{
			case 1 or 3 or 5 or 7 or 8 or 10 or 12:
				if ( ($day < 1) || ($day > 31) ) $day = 1;
				break;
			case 4 or 6 or 9 or 11:
				if ( ($day < 1) || ($day > 30) ) $day = 1;
				break;
			case 2:
				if ( ($year % 4 == 0) && ($year % 100 == 0) && ($year % 400) ) // Nam nhuan
				{
					if ( ($day <= 0) || ($day > 29) ) $day = 1;
				}
				
				if ( ($year % 4 == 0) && ($year % 100 == 0) ) // Nam thuong
				{
					if ( ($day <= 0) || ($day > 28) ) $day = 1;
				}
				
				if ($year % 4 == 0) // Nam nhuan
				{
					if ( ($day <= 0) || ($day > 29) ) $day = 1;
				}
				
				if ( ($day <= 0) || ($day > 28) ) $day = 1; // Nam thuong
				break;
		}
		
		return mktime($hour, $min, $sec, $mon, $day, $year);
	}
	
	
	/*-------------------------------------
	| SHOW DATE TIME IN VIETNAMESE
	---------------------------------------*/
	function show_date($unix_time)	
	 {  global $lg;
	    $date_string='';
	   if($lg->lang_current=='en')
	    {  
			$date_string=date("D, d/m/Y",$unix_time);
		 }
		else
		 {   
		 	$str_search = array ( 
									"Mon", 
									"Tue", 
									"Wed", 
									"Thu", 
									"Fri", 
									"Sat", 
									"Sun", 
									
									); 
			$str_replace = array ( 
										"Thứ hai", 
										"Thứ ba", 
										"Thứ tư", 
										"Thứ năm", 
										"Thứ sáu", 
										"Thứ bảy", 
										"Chủ nhật", 										
									); 
				$timenow = date("D, d/m/Y",$unix_time); 
				$date_string = str_replace( $str_search, $str_replace, $timenow); 
	
		   }
	 		return $date_string;
	   }
	   
	   
	   /*------------------------------------------------------------------
	   * CONVERT DATETIME FROM YYYY-MM-DD HH:MM:SS TO HH:MM:SS DD-MM-YYYY
	   -------------------------------------------------------------------*/
	   function showNormalDateTime($dbdatetime)
	   {
	   		return substr($dbdatetime,11,2).':'.substr($dbdatetime,14,2).':'.substr($dbdatetime,17,2).'&nbsp;'.substr($dbdatetime,8,2).'-'.substr($dbdatetime,5,2).'-'.substr($dbdatetime,0,4);
	   }
	   
	   /*------------------------------------------------
	    * CONVERT FROM YYYY-MM-DD TO DD-MM-YYYY
		-----------------------------------------------*/
		function showNormalDate($dbdate)
		{
			ereg ("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", $dbdate, $regs);
			return  "$regs[3].$regs[2].$regs[1]";
		}
	   
	   
}
?>