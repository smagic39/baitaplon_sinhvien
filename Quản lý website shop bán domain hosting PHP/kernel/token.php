<?php
/*-------------------------------------------------------------------
| CLASS TO HANDLE THE MAIN CONTENT OF SITE
| BASE ON TOKEN THAT GET FROM URL, RETURN THE CORRESPONDING CONTENT
---------------------------------------------------------------------*/

// Check for Security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}

class token
{
	public $tk_arr = array();
	public $tk_start = 0;
	private $path = "";
	public $process = "";
	public $body = "";
	// This param will be used to show/hide corresponding menu on admin panel
	public $folder = "";
	
	/*-----------------------------------------------------------
	* Get name of corresponding params base on MOD param on URL
	-------------------------------------------------------------*/
	function token()
	{
		
		$this->path = "http://";
		$this->path .= $_SERVER['HTTP_HOST'];
		$this->path .= $_SERVER['REQUEST_URI'];
		$this->tk_arr = explode("/", $this->path);
		
		
		$slash = explode("/", URL);
		$this->tk_start = count($slash);
		
		// This is use in mod wrewrite case as index/token/....
		//$this->process = $this->tk_start < count($this->tk_arr) ? $this->tk_arr[$this->tk_start] : "home";
		
		// Use normal url and base on mod to define module
		if (isset($_GET['mod']) && $_GET['mod'] != '')
		{
			$this->process = $_GET['mod'];
		}
		else
		{
			$this->process = 'home';
		}
		//echo $this->process;
	}
	
	/*-----------------------------------------------
	| DEFINE RETURN TOKEN-FRONTPAGE
	-------------------------------------------------*/
	function act()
	{
		global $sess;
		
		switch ($this->process)
		{  
			// Domain checking
			case "checkDomain":
				$this->body = "whois/whois.php";
				break;
			 
			// Service type
			case "st":
				$this->body = "services/st.php";
				break;
			// Service
			case "service":
				$this->body = "services/service.php";
				break;
			// Register service guide
			case "regi":
				$this->body = "regi/regi.php";
				break;
			// Support information
			case "support":
				$this->body = "support/support.php";
				break;
			
			// Referen information
			case "rinfo":
				$this->body = "rinfo/rinfo.php";
				break;
			
			// Contact
			case "contact":
				$this->body = "contact/contact.php";
				break;
				
			// Service introduction
			case "serpromo":
				$this->body = "services/serpromo.php";
				break;
				
			// Service plan
			case "sp":
				$this->body = "services/sp.php";
				break;
			
			// News
			case "news":
				$this->body = "news/news.php";
				break;
			case "newsView":
				$this->body = "news/newsView.php";
				break;
			
			// FAQ
			case "faq":
				$this->body = "faq/faq.php";
				break;
			case "faqView":
				$this->body = "faq/faqView.php";
				break;
				
			// Order
			case "order":
				$this->body = "order/order.php";
				break;				
					
			/*
			case "lang":
				$sess->language = $this->tk_arr[ $this->tk_start+1 ];	
			*/
			case "home":
			default:
				$this->process = "home";
				$this->body = "homepage.php";
		}
	}
	
	
	/*-----------------------------------------------
	| GET RETURN PROCCESS FOR ADMIN CONTROL PANEL
	------------------------------------------------*/
	function admin()
	{
		switch ($this->process)
		{
			case "login":
				return "login.php";
				
			case "logout":
				return "logout.php";
			
			case "changepass":
				return "changepass.php";
			
			// Optional
			case "setting":
				$this->folder = "setting";
				return "setting.php";			
			case 'pagingSetting':
				$this->folder = "setting";
				return "pagingSetting.php";	
										
			// Support information
			case "supportList":
				$this->folder = "support";
				return "support/supportList.php";
			case "supportAdd":
				$this->folder = "support";
				return "support/supportAdd.php";
			case "supportEdit":
				$this->folder = "support";
				return "support/supportEdit.php";
				
			// Referen information
			case "infosList":
				$this->folder = "infos";
				return "infos/infosList.php";
			case "infosAdd":
				$this->folder = "infos";
				return "infos/infosAdd.php";
			case "infosEdit":
				$this->folder = "infos";
				return "infos/infosEdit.php";
				
			// Register guide
			case "regList":
				$this->folder = "reg";
				return "reg/regList.php";
			case "regAdd":
				$this->folder = "reg";
				return "reg/regAdd.php";
			case "regEdit":
				$this->folder = "reg";
				return "reg/regEdit.php";

			
				
			
			// Customer contact
			case "contactList":
				$this->folder = "homeinfo";
				return "contact/contactList.php";
			case "contactView":
				$this->folder = "homeinfo";
				return  "contact/contactView.php";
			
			// Link back to CRM
			case "crmList":
				$this->folder = "homeinfo";
				return "crm/crmList.php";
			case "crmEdit":
				$this->folder = "homeinfo";
				return "crm/crmEdit.php";
				
			
			// Information at home
			case "homeInfos":
				$this->folder = "homeinfo";
				return "homeinfos/homeInfos.php";
			// Edit information at home
			case "homeInfosEdit":
				$this->folder = "homeinfo";
				return "homeinfos/homeInfosEdit.php";
			
			// Service introduction
			case "siList":
				$this->folder = "si";
				return "si/siList.php";
			case "siAdd":
				$this->folder = "si";
				return "si/siAdd.php";
			case "siEdit":
				$this->folder = "si";
				return "si/siEdit.php";
			
			// Faq
			case "faqList":
				$this->folder = "faq";
				return "faq/faqList.php";
			case "faqAdd":
				$this->folder = "faq";
				return "faq/faqAdd.php";
			case "faqEdit";
				$this->folder = "faq";
				return "faq/faqEdit.php";
				
				
			// News
			case "newsList":
				$this->folder = "news";
				return "news/newsList.php";
			case "newsAdd":
				$this->folder = "news";
				return "news/newsAdd.php";
			case "newsEdit":
				$this->folder = "news";
				return "news/newsEdit.php";
			
			// Service type
			case "stList":
				$this->folder = "sc";
				return "service/stList.php";
			case "stAdd":
				$this->folder = "sc";
				return "service/stAdd.php";
			case "stEdit":
				$this->folder = "sc";
				return "service/stEdit.php";
			
			// Service 
			case "serviceList":
				$this->folder = "sc";
				return "service/serviceList.php";
			case "serviceAdd":
				$this->folder = "sc";
				return "service/serviceAdd.php";
			case "serviceEdit":
				$this->folder = "sc";
				return "service/serviceEdit.php";
				
			// Service plan
			case "spList":
				$this->folder = "sc";
				return "service/spList.php";
			case "spAdd":
				$this->folder = "sc";
				return "service/spAdd.php";
			case "spEdit":
				$this->folder = "sc";
				return "service/spEdit.php";
				
			// Order informatin
			case "orderList":
				$this->folder = "od";
				return "order/orderList.php";
			case "orderView":
				$this->folder = "od";
				return "order/orderView.php";
			
			// Banners on top
			case "bannerList":
				$this->folder = "homeinfo";
				return "advert/bannerList.php";
			case "bannerAdd":
				$this->folder = "homeinfo";
				return "advert/bannerAdd.php";
						
			default:
				$this->process = "home";
				return "homepage.php";break;
		
		}
	}
}
?>
