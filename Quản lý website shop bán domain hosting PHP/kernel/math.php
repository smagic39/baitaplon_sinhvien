<?php
/*---------------------------------------------------
| CLASS FOR MATCH FUNCTIONS
-----------------------------------------------------*/
// Check for Security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}

class math
{
	/* ------------------------------------------------------------------------------------------
	| LAM TRON LEN
	|
	| VD: 4.1 => 5
	+ -----------------------------------------------------------------------------------------*/
	function round_high($number)
	{
		return round($number) >= $number ? round($number) : round($number) + 1;
	}
	
	/* ------------------------------------------------------------------------------------------
	| LAM TRON XUONG
	|
	| VD: 4.9 => 4
	+ -----------------------------------------------------------------------------------------*/
	function round_low($number)
	{
		return round($number) <= $number ? round($number) : round($number) - 1;
	}
}
?>