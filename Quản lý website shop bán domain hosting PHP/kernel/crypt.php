<?php
/*----------------------------------------
| CLASS FOR CRYPT AND ENCRYPT
----------------------------------------*/
// Check for Security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}

class crypter
{
	private $secu_code_arr = array(
										"start" => "a",
										"end" => "z",
									);
	private $pass_min_length = 6;				
	private $key_length = 32;
	private $encode_key = "e9c495f4b4271113dc1e546cbf04224b";	//Not change
	private $encode_arr = array(								//Not change
							" ", "0", "1", "2", "3",
							"4", "5", "6", "7", "8",
							"9", "a", "b", "c", "d",
							"e", "f", "g", "h", "i",
							"j", "k", "l", "m", "n",
							"o", "p", "q", "r", "s",
							"t", "u", "v", "w", "x",
							"y", "z", "A", "B", "C",
							"D", "E", "F", "G", "H",
							"I", "J", "K", "L", "M",
							"N", "O", "P", "Q", "R",
							"S", "T", "U", "V", "W",
							"X", "Y", "Z", "@",
						);
						
	/*-----------------------------------------------------
	| CREATE RANDOM KEY
	+ ---------------------------------------------------*/
	function random_key($number)
	{
		$arr_sum = array();
		$arr_num = array();
		$arr_char = array();
		
		$arr_num = range("0", "9");
		$arr_char = range($this->secu_code_arr['start'], $this->secu_code_arr['end']);
		
		$arr_sum = array_merge($arr_num, $arr_char);

		$rand = '';
		for($i = 0; $i < $number; $i++)
		{
			$key = array_rand($arr_sum);
			$rand .= $arr_sum[$key];
		}
		
		return $rand;
	}
	
	/*-----------------------------------------------------
	| CREATE A SECURITY CODE
	+ ---------------------------------------------------*/
	function security_code($number)
	{
		// Lay ngau nhien tu 0-9, A-Z
		$this->secu_code_arr['start'] = "A";
		$this->secu_code_arr['end'] = "Z";
		
		return $this->random_key($number);
	}
	
	/*-----------------------------------------------------
	| COMPILE A SECURITY CODE TO PRINT IN SCREEN
	+ ---------------------------------------------------*/
	function print_screen($code)
	{
		$str_print = "";
		for($i = 0; $i < strlen($code); $i++)
		{
			$str = substr($code, $i,1);
			$str_print .= $str.'';
		}
		
		return $str_print;
	}
	
	/*-----------------------------------------------------
	| CREATE KEY FOR CRYPT A PASSWORD
	+ ---------------------------------------------------*/
	function set_key()
	{
		// Lay ngau nhien tu 0-9, a-z
		$this->secu_code_arr['start'] = "a";
		$this->secu_code_arr['end'] = "z";
		
		return $this->random_key($this->key_length);
	}
	
	/*-----------------------------------------------------
	| FILL IN MISSING CHAR BY RANDOM CHAR
	|
	| This help Vernam encrypt be true
	+ ---------------------------------------------------*/
	function get_true_vernam($text)
	{
		// Just encrypt chars in encode_arr
		$text_arr = str_split($text);
		$text = "";
		foreach ($text_arr as $k => $v)
		{
			$yes = false;
			foreach ($this->encode_arr as $value)
			{
				if ($v == $ $value)
				{
					$yes = true;
					break;
				}
			}
			
			if ($yes) $text .= $text_arr[$k];
		}
		
		// strlen($key) = strlen($password) = key_length
		if ( strlen($text) < $this->key_length)
		{
			$text .= "@";
			
			while ( strlen($text) < $this->key_length) $text .= $this->random_key(1);
			
			if ( strlen($text) > $this->key_length) $text = substr($text,0,$this->key_length);
		}
		
		return $text;
	}
	
	/*-----------------------------------------------------
	| ENCODE TEXT USING VERNAM ALGORITHM
	+ ---------------------------------------------------*/
	function vernam($text, $key)
	{
		// Be sure $text and $key are valid
		$text = $this->get_true_vernam($text);
		$key = $this->get_true_vernam($key);
		
		$text_arr = str_split($text);
		$key_arr = str_split($key);
		
		$code_text = "";
		
		
		for ($i = 0; $i < $this->key_length; $i++)
		{
			$text_int = 0;
			while ( ($text_arr[$i] != $this->encode_arr[$text_int]) && ($text_int < count($this->encode_arr)) ) $text_int++;
			
			$key_int = 0;
			while ( ($key_arr[$i] != $this->encode_arr[$key_int]) && ($key_int < count($this->encode_arr)) ) $key_int++;
			
	
			$code_int =  $text_int + $key_int;
			
			if ( $code_int > count($this->encode_arr) ) $code_int -= count($this->encode_arr);
			
			$code_text .= $this->encode_arr[$code_int];
		}
		
		return $code_text;
	}
	
	/*-----------------------------------------------------
	| DECODE TEXT HAD BEEN ENCODE WITH VERNAM ALGORITHM
	+ ---------------------------------------------------*/
	function devernam($text, $key)
	{
		
		$text = $this->get_true_vernam($text);
		$key = $this->get_true_vernam($key);
		
		$text_arr = str_split($text);
		$key_arr = str_split($key);
		
		$code_text = "";
		
		
		for ($i = 0; $i < $this->key_length; $i++) 
		{			
			$text_int = 0;
			while ( ($text_arr[$i] != $this->encode_arr[$text_int]) && ($text_int < count($this->encode_arr)) ) $text_int++;
			
			
			$key_int = 0;
			while ( ($key_arr[$i] != $this->encode_arr[$key_int]) && ($key_int < count($this->encode_arr)) ) $key_int++;
			
			
			$code_int =  $text_int - $key_int;
			
			if ( $code_int < 0 ) $code_int += count($this->encode_arr);
			
			$code_text .= $this->encode_arr[$code_int];
		}
		
		return $code_text;
	}
	
	/*-----------------------------------------------------
	| ENCODE PASSWORD BY VERNAM ALGORITHM
	+ ---------------------------------------------------*/
	function crypt_password($password, $key)
	{
		// Encode keyword
		$key = $this->vernam($key, $this->encode_key);
		
		// Encode password
		return $this->vernam($password, $key);
	}
	
	/*-----------------------------------------------------
	| DECODE PASSWORD HAD BEEN ENCODE BY VERNAM
	+ ---------------------------------------------------*/
	function decrypt_password($password, $key)
	{
		$key = $this->vernam($key, $this->encode_key);		
		$text = $this->devernam($password, $key);		
		$password_arr = explode("@", $text);
		return $password_arr[0];
	}
}
?>