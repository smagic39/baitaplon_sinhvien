<?php
/*--------------------------------------
| HANDLE BASIC DISPLAY
--------------------------------------*/

// Check for Security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}

class display
{

	////////////////////////////////////
	// SHOW LIST OF ADVERT POSITION
	////////////////////////////////////
	function showlist_Pos($sName, $selectValue, $onChange= '', $all=FALSE)
	{
		global $lang, $str, $db, $frm;
		$returnStr = '';
		$returnStr = '<select  name="'. $sName .'" size="1" onchange="'. $onChange .'">';
		if ($all)
		{
        	$returnStr .= $frm->draw_option('0','All',true);
		}
		$returnStr .= $frm->draw_option(1, 'Vị trí 1', ($selectValue == 1 ? true : false));
		$returnStr .= $frm->draw_option(2, 'Vị trí 2', ($selectValue == 2 ? true : false));
		$returnStr .= $frm->draw_option(3, 'Vị trí 3', ($selectValue == 3 ? true : false));
		$returnStr .= $frm->draw_option(4, 'Vị trí 4', ($selectValue == 4 ? true : false));
		$returnStr .= $frm->draw_option(5, 'Vị trí 5', ($selectValue == 5 ? true : false));
		$returnStr .= '</select>';
		
		return $returnStr;
		
	}

	/*
	* FUNCTION TO SHOW LIST OF CATEGORIES IN 2 LEVELS
	*/
	/*-----------------------------
	| DRAW DYNAMIC SELECT LIST 
	+-----------------------------*/
	function showServicesList($CboBoxName, $SelectValue='', $OnchangeFunction='', $all=TRUE)
	{
		global $lang, $str, $db, $frm;
		$returnStr = '';
		$returnStr = '<select  name="'. $CboBoxName .'" size="1" onchange="'. $OnchangeFunction .'">';
		$sqlType = 'SELECT id,name FROM service_type WHERE active=1 ORDER BY name ASC';
		$query = $db->query($sqlType);
		if ($all)
		{
        	$returnStr = $returnStr . $frm->draw_option('0','All',true);
		}
		
		while ( $row = mysql_fetch_row($query) )
		{
		   	$row1 = $row[1] ; 
			$returnStr .= $frm->draw_option('T'.$row[0],$row[1]);
			
			// show list of category from this type
			$qr_cat = 'SELECT id,title FROM service WHERE type="'.$row[0].'"';
			$rs_cat = $db->query($qr_cat);
			while ($cat = mysql_fetch_row($rs_cat))
			{
				if ( $SelectValue == $cat[0] )
				{
					$returnStr .= $frm->draw_option($cat[0],'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$cat[1],true);
				}
				else
				{
					$returnStr .= $frm->draw_option($cat[0],'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$cat[1]);
				}
			}
			
		}
		$returnStr .= '</select>';
		return $returnStr;
	}	 
	
	
	
	/*---------------------------------------------
	| GET VALID SIZE TO FOR AN IMAGE
	---------------------------------------------*/
	function getFixSize($url, $width=200)
	{   
		$arrReturn = array();
		$arrSize = array();
		$arrSize = @getimagesize($url);
		// If image width is too large, resize the image
		if ($arrSize[0])
		{
			if ($arrSize[0] <= $width)
			{
				$arrReturn[0] = $arrSize[0];
				$arrReturn[1] = $arrSize[1];
			}
			else
			{
				$arrReturn[0] = $width;
				$arrReturn[1] = (($width/$arrSize[0])*$arrSize[1]);
			}
		}
		else
		{
			$arrReturn[0] = $width;
			$arrReturn[1] = $width*80/100;
		}
		return $arrReturn;
		
	}
	
	
	
	/*--------------------------------------
	| DRAW LIT OF SERVICES FOR ORDER PAGE
	+---------------------------------------*/
	function showServices($CboBoxName, $SelectValue='', $OnchangeFunction='', $all=TRUE)
	{
		global $str, $db, $frm;
		$returnStr = '';
		$returnStr = '<select  name="'. $CboBoxName .'" size="1" onchange="'. $OnchangeFunction .'">';
		$sqlType = 'SELECT id,name FROM service_type WHERE active=1 ORDER BY name ASC';
		$query = $db->query($sqlType);
		if ($all)
		{
        	$returnStr = $returnStr . $frm->draw_option('0','Vui lòng chọn',true);
		}
		
		while ( $row = mysql_fetch_row($query) )
		{
		   	$row1 = $row[1] ; 
			$returnStr .= $frm->draw_option('ST'.$row[0],$row[1]);
			
			// show list of category from this type
			$qr_cat = 'SELECT id,title FROM service WHERE allow_order = 1 AND type="'.$row[0].'"';
			$rs_cat = $db->query($qr_cat);
			while ($cat = mysql_fetch_row($rs_cat))
			{
				if ( $SelectValue == $cat[0] )
				{
					$returnStr .= $frm->draw_option($cat[0],'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_'.$cat[1],true);
				}
				else
				{
					$returnStr .= $frm->draw_option($cat[0],'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_'.$cat[1]);
				}
			}
			
		}
		$returnStr .= '</select>';
		return $returnStr;
	}
	
	/*--------------------------------------
	| DRAW LIT OF SERVICES FOR ORDER PAGE
	+---------------------------------------*/
	function showServicesPlan($CboBoxName, $Service, $SelectValue='', $OnchangeFunction='', $all=TRUE)
	{
		global $str, $db, $frm;
		$returnStr = '';
		$returnStr = '<select  name="'. $CboBoxName .'" size="1" onchange="'. $OnchangeFunction .'" class="input-list1">';
		if ($all)
		{
        	$returnStr = $returnStr . $frm->draw_option('0','Vui lòng chọn',true);
		}
		
		$qr_sp = 'SELECT id,title FROM service_plan WHERE service_id="'.$Service.'"';
		$rs_sp = $db->query($qr_sp);
		while ($spr = mysql_fetch_row($rs_sp))
		{
			if ( $SelectValue == $spr[0] )
			{
				$returnStr .= $frm->draw_option($spr[0], $spr[1], TRUE);
			}
			else
			{
				$returnStr .= $frm->draw_option($spr[0], $spr[1]);
			}
		}
			
		$returnStr .= '</select>';
		return $returnStr;
	}	 	 
	
	
}
?>