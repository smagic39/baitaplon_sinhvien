<?php
// Check for Security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}

class order
{
	public $order_code = 0;
	
	function set_order_code()
	{
		global $sess;
		
		$this->order_code = $this->check_order_code($sess->now);
		
		return $this->order_code;
	}
	
	function display_code($code)
	{
		global $math;
		
		$first = $math->round_low($code / 10000000);
		$second = $math->round_low(($code % 10000000) / 10000);
		$third = $code % 10000;
		
		$first = $first < 10 ? '00'.$first : ($first < 100 ? '0'.$first : $first);
		$second = $second < 10 ? '00'.$second : ($second < 100 ? '0'.$second : $second);
		$third = $third < 10 ? '000'.$third : ($third < 100 ? '00'.$third : ($third < 1000 ? '0'.$third : $third));
		
		return $first . ' ' . $second . ' ' . $third;
	}
	
	function check_order_code($code)
	{
		global $db, $str;
		
		$query = $db->simple_select("code", "order", "code=".$code);
		$result = $db->query($query);
		$row = $db->fetch_array($result);
		
		if ($row['code'] == $code) $code = $this->check_order_code($code+1);
		
		return $code;
	}
	
	function show_hiden_part1($frmValue)
	{
		global $frm;
		
		$content = "";
		
		// Part 1
		$content .= $frm->draw_hidden('kh_name',$frmValue['kh_name']);
		$content .= $frm->draw_hidden('kh_address',$frmValue['kh_address']);
		$content .= $frm->draw_hidden('kh_tel',$frmValue['kh_tel']);
		$content .= $frm->draw_hidden('kh_fax',$frmValue['kh_fax']);
		$content .= $frm->draw_hidden('kh_website',$frmValue['kh_website']);
		$content .= $frm->draw_hidden('kh_email',$frmValue['kh_email']);
		
		$content .= $frm->draw_hidden('ndd_name',$frmValue['ndd_name']);
		$content .= $frm->draw_hidden('ndd_jobtitle',$frmValue['ndd_jobtitle']);
		$content .= $frm->draw_hidden('ndd_address',$frmValue['ndd_address']);
		$content .= $frm->draw_hidden('ndd_tel',$frmValue['ndd_tel']);
		$content .= $frm->draw_hidden('ndd_fax',$frmValue['ndd_fax']);
		$content .= $frm->draw_hidden('ndd_website',$frmValue['ndd_website']);
		$content .= $frm->draw_hidden('ndd_email',$frmValue['ndd_email']);
		
		return $content;
	}
	
	function show_hiden_part2($frmValue)
	{
		global $frm;
		
		$content = "";
		
		// Part 2
		$content .= $frm->draw_hidden('domain_free',$frmValue['domain_free']);
		$content .= $frm->draw_hidden('email_account',$frmValue['email_account']);
		
		$content .= $frm->draw_hidden('design_skin',$frmValue['design_skin']);
		$content .= $frm->draw_hidden('design_logo',$frmValue['design_logo']);
		$content .= $frm->draw_hidden('design_flashintro',$frmValue['design_flashintro']);
		$content .= $frm->draw_hidden('design_flashbanner',$frmValue['design_flashbanner']);
		$content .= $frm->draw_hidden('design_sound',$frmValue['design_sound']);
		$content .= $frm->draw_hidden('design_images',$frmValue['design_images']);
		
		$content .= $frm->draw_hidden('functions_html',$frmValue['functions_html']);
		$content .= $frm->draw_hidden('functions_news',$frmValue['functions_news']);
		$content .= $frm->draw_hidden('functions_search',$frmValue['functions_search']);
		$content .= $frm->draw_hidden('functions_searchadvanced',$frmValue['functions_searchadvanced']);
		$content .= $frm->draw_hidden('functions_contacts',$frmValue['functions_contacts']);
		$content .= $frm->draw_hidden('functions_newsletter',$frmValue['functions_newsletter']);
		
		$content .= $frm->draw_hidden('functions_hitcounter',$frmValue['functions_hitcounter']);
		$content .= $frm->draw_hidden('functions_faq',$frmValue['functions_faq']);
		$content .= $frm->draw_hidden('functions_advbanner',$frmValue['functions_advbanner']);
		$content .= $frm->draw_hidden('functions_files',$frmValue['functions_files']);
		$content .= $frm->draw_hidden('functions_polls',$frmValue['functions_polls']);
		$content .= $frm->draw_hidden('functions_careers',$frmValue['functions_careers']);
		
		$content .= $frm->draw_hidden('functions_members',$frmValue['functions_members']);
		$content .= $frm->draw_hidden('functions_admin',$frmValue['functions_admin']);
		$content .= $frm->draw_hidden('functions_gallery',$frmValue['functions_gallery']);
		$content .= $frm->draw_hidden('functions_album',$frmValue['functions_album']);
		$content .= $frm->draw_hidden('functions_ecommerce',$frmValue['functions_ecommerce']);
		$content .= $frm->draw_hidden('functions_creditcard',$frmValue['functions_creditcard']);
		
		$content .= $frm->draw_hidden('functions_chat',$frmValue['functions_chat']);
		$content .= $frm->draw_hidden('functions_forum',$frmValue['functions_forum']);
		$content .= $frm->draw_hidden('functions_blog',$frmValue['functions_blog']);
		$content .= $frm->draw_hidden('functions_guestbook',$frmValue['functions_guestbook']);
		$content .= $frm->draw_hidden('functions_buysell',$frmValue['functions_buysell']);
		$content .= $frm->draw_hidden('functions_webmail',$frmValue['functions_webmail']);
		
		$content .= $frm->draw_hidden('functions_backup',$frmValue['functions_backup']);
		$content .= $frm->draw_hidden('functions_adminlog',$frmValue['functions_adminlog']);
		
		return $content;
	}
	
	function show_hiden_part3($frmValue)
	{
		global $frm;
		
		$content = "";
		
		// Part 3
		$content .= $frm->draw_hidden('pay_method',$frmValue['pay_method']);
		
		return $content;
	}
}
?>