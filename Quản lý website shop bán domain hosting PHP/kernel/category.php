<?php
/*---------------------------------------------
| SOME FUNCTION USED FOR CATEGORIES VISUALIZE
-------------------------------------------------*/
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}


class catVisualize
{  

	// Show list of category
	function showCatList($SQLStr, $CboBoxName, $SelectValue='', $OnchangeFunction='', $all=false)
	{
		global $str ;
		$returnStr = '';
		$returnStr = '<select  name="'. $CboBoxName .'" size="1" onchange="'. $OnchangeFunction .'">';
		
		$query = mysql_query($SQLStr);
		if ($all)
		{
        	$returnStr = $returnStr .$this->draw_option('0','All',true);
		}
		
		while ( $row = mysql_fetch_row($query) )
		{
		    $row1 = $row[1] ; 
			if ($row[2] > 0)
			{
				$row1 = '--'.$row[1];
			}
			
			if ( $SelectValue == $row[0] )
			{
				$returnStr = $returnStr .$this->draw_option($row[0],$row[1],true);
			}
			else
			{
				$returnStr = $returnStr .$this->draw_option($row[0],$row[1]);
			}
		}
		$returnStr = $returnStr .'</select>';
		return $returnStr;
	}


}