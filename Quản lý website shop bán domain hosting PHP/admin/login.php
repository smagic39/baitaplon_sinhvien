<?php
/*------------------------------------
| LOGIN TO ADMIN PANEL
 ------------------------------------*/
 
// Check for Security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}

$cnt = new content;

class content
{
	public $text = "";
	public $title = "WEBSITE CONTROL PANEL";	
	private $process = "";
	private $frmValue = array(
								'password' => '',
								'username' => '',
						  	 );
	private $error = array(
								'username' => 0,
								'password' => 0,
							);
	private $warning = "";
	
	
	function __construct()
	{
		global $str, $sess, $lang, $panel;		
		if ($sess->member_id) $sess->destroy();		
		$this->get_input();
		$check_input = $this->check_input( $this->frmValue );
		
		if ( $this->process == "login" )
		{
			if ($check_input)
			{
				 if ($this->login($this->frmValue))
				 {
				  	  $str->goto_url('admp.php');
				 }
				 else
				 {
					  $this->warning = " <span class='span_err'>". $lang['login_notmatch'] ."</span><br/><br/>";
				 }
			}
			else
			{
				$this->warning = "<span class='span_err'>". $lang['login_error'] ."</span><br/><br/>";
			}
		}else
		{
			//Reset Error Array
			$this->error['username'] = 0;
			$this->error['password'] = 0;
		}
		
		$this->text = $this->warning ? "<div id='warning'><b><u>Lỗi nhập liệu</u></b> : " . $this->warning . "</div>" . $this->box_login() : $this->box_login();
	}
	
	
	/*---------------------------------------------
	 | GET INPUT DATA	
	+----------------------------------------------*/
	function get_input()
	{
		global $str;		
		$this->process = isset($_POST['process']) ? $str->input($_POST['process']) : "";		
		$this->frmValue['username'] = isset($_POST['username']) ? $str->input($_POST['username']) : "";
		$this->frmValue['password'] = isset($_POST['password']) ? $_POST['password'] : "";
	 }
	
	
	/*--------------------------------------------------
	 | CHECK INPUT DATA
	+--------------------------------------------------*/
	function check_input($frmValue)
	{
		global $frm, $main, $sess, $db, $str;		
		$no_error = true;		
		if ( !$frm->check_username($frmValue['username'], 3) )
		{
			$this->error['username'] = 1;
			$no_error = false;
		}
		
		if ( !$frm->check_password($frmValue['password'], 6, 32) )
		{
			$this->error['password'] = 1;
			$no_error = false;
		}
		
		return $no_error;
	}
	
	
	/*-------------------------------------------
	  | CHECK INFOS AND AUTHENTICATE FOR LOGIN
	+--------------------------------------------*/
	function login($frmValue)
	{
		global $str, $frm, $db, $sess, $lang, $crypt, $main;
		
		if ( $sess->member_id ) $sess->destroy();
		
		$tmp_username = stripslashes($frmValue['username']);
		$tmp_password = stripslashes($frmValue['password']);
				
		
		$query = $db->simple_select("username, password, login_key, id, groups", "adm_members", "username = '". mysql_real_escape_string($tmp_username) ."' AND active = 1");
		$result = $db->query($query);
		$row = $db->fetch_array($result);
		
		if ( !$row['password'] ) return false;		
		$password = $crypt->decrypt_password($row['password'], $row['login_key'], 32);						
		if ($password != $tmp_password) return false;
		if (strcmp($row['username'], $tmp_username) <> 0 ) return false;
		
		// Update to session
		$sess->member_id = $row['id'];
		$sess->username = $frmValue['username'];
		$sess->groups_id = $row['groups'];
				
		$arr = array( 
						'username' => $sess->username,
						'member_id' => $sess->member_id,
						'groups_id' => $sess->groups_id,
					);
		$db->do_update("session", $arr, "id = '". $sess->sess_id ."'");
						
		// Update last visit
		$arr = array( 'last_visit'  => $sess->now );
		$db->do_update("adm_members", $arr, "id = ". $sess->member_id);
		
		return true;
	}
	
	/*------------------------------------------------
	 | SHOW LOGIN BOX
	--------------------------------------------------*/
	function box_login()
	{
		global $lang, $frm, $dsp;
		
		$text  = $frm->draw_form("", "", 2, "POST");
		$text .= $frm->draw_hidden("process", "login");
			$text .= "<center><fieldset style='border:#0066FF solid 1px; width:300px;'>
<legend style='font-weight:bold; font-size:0.9em; color:#333333'><img src='". ADMIN_IMG ."lock.png' align='absmiddle' width='20' />&nbsp;Đăng nhập hệ thống</legend>";
			$text .= "<table border='0' width='300'>";
				$text .= "<tr><td height='5' colspan='2'></td></tr>";
				$text .= "<tr>";
					$text .= "<td width='70'>" . $lang['login_user'] . "</td>";
					$text .= "<td align='left'>" . $frm->draw_textfield("username", "", "field", "", "32") . "</td>";
				$text .= "</tr>";
				$text .= "<tr><td height='2' colspan='2'></td></tr>";
				$text .= "<tr>";
					$text .= "<td>" . $lang['login_pass'] . "</td>";
					$text .= "<td align='left'>" . $frm->draw_password("password", "field", "", "32") . "</td>";
				$text .= "</td></tr>";
				$text .= "<tr><td height='5' colspan='2'></td></tr>";
				$text .= "<tr><td colspan='2' align='center'>";
					$text .= "<input type='submit' value='Đăng nhập' style='background-color:#0F4B5D; color:#FFFFFF; border:#CCC solid 1x;' />";
				$text .= "</td></tr>";
				$text .= "<tr><td height='5' colspan='2'></td></tr>";
			$text .= "</table></fieldset></center>";
		$text .= '</form>';
		
		return $text;
	}
}

?>