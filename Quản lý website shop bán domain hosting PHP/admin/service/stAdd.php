<?php
/*----------------------------------------
| ADD NEW SERVICE TYPE
------------------------------------------*/

// Check for security
if (!defined('HCR'))
{
	print '<h1>Incorrect Access!</h1>';
	exit();
}

$cnt = new content;

class content{

	public  $title = "Thêm danh mục dịch vụ mới";
	public  $text = "";	
	private $process = "";
	private $inserted_id = 0;
	private $frmValue = array(
								 'name' => '',
								 'active'  => ''
							 );
	private $warning = "";
	private $err = "";
			
			
	function __construct()
	{
		global $str, $sess;		
		$this->get_input();
		$check_input = $this->check_input($this->frmValue);
		
		if ($this->process == "addST")
		{ 
			if ($check_input)
			{
				$this->insert_field($this->frmValue);
				$this->warning = "<span class='span_ok'>Thêm danh mục dịch vụ mới thành công !!</span><ul>";
				$this->warning .= "<ol><img src='". ADMIN_IMG ."red.gif' align='absmiddle' /> Nhấp <a href='?mod=stEdit&id=". $this->inserted_id ."' class='topadd'>vào đây</a> để hiệu chỉnh.</ol>";
				$this->warning .= "<ol><img src='". ADMIN_IMG ."red.gif' align='absmiddle'/> Nhấp <a href='?mod=stList' class='topadd'>vào đây</a> để xem danh sách.<ol></ul><br/>";
				        
						$this->frmValue['name'] = "";
						$this->err = "";
			
			}
			else
			{
				$this->warning = "<b><u>Lỗi nhập liệu</u>:</b>&nbsp;<span class='span_err'><ul>". $this->err ."</ul></span>";
			}
		}		
		
		$this->text = ($this->warning) ? "<div id='warning'>" . $this->warning . "</div>" . $this->show_form($this->frmValue) : $this->show_form($this->frmValue);
	}	
	
	/*--------------------------------
	 | SHOW FORM 
	 ---------------------------------*/
	function show_form( $frmValue )
	{
		global $frm, $db, $sess, $time;
		
		$text = $frm->draw_form("", "", 2, "POST", "frmCategory" );
		$text .= $frm->draw_hidden("process", "addST");
		$text .= "<table cellspacing='0' cellspadding='4' class='tbl_add'>
				  <tr><td colspan='2' height='10'></td></tr>";
					
		$text .= "<tr>";
		$text .= "<td>Tên danh mục : </td>";
		$text .= "<td>". $frm->draw_textfield("name", $frmValue["name"], "", "50", "255") ."</td>
				  </tr>
				  <tr><td colspan='2' height='6'></td></tr>	";
		
		$text .= "<tr>";
		$text .=   "<td colspan='2' align=center>";
		$text .= $frm->draw_submit(" Thêm ", "");
		$text .= "&nbsp;&nbsp;<input type='reset' value=' Hủy '></td>";
		$text .= "</tr>";
			
		$text .= "</table>";		
		$text .= "</form>";			
		
		return $text;
	}
	
	
	/*----------------------------------------------------
	 | GET INPUT DATA
	+-----------------------------------------------------*/
	function get_input()
	{
		global $str;
		
		$this->process = isset( $_POST['process']) ? $str->input($_POST['process'] ) : "";				
		$this->frmValue['name'] = isset($_POST['name']) ? $str->input($_POST['name']) : "";
				
	}
	
		
	/*-----------------------------------------------------
	 | CHECK FOR INPUT DATA
	+------------------------------------------------------*/
	function check_input($frmValue)
	{
		global $frm;		
		$no_error = true;
		
		if ( !$frm->check_input($frmValue['name'], 1) )
		{

			$no_error = false;
			$this->err .= "<li>Hãy nhập tên danh mục</li>";
		}
			
		return $no_error;
	}
	
	
	/*------------------------------------------------
	 | INSERT INTO DATA
	+------------------------------------------------*/
	function insert_field($frmValue)
	{
		global $db, $time;
		       
		// Ghi vao CSDL
		$arr = array(
						'name'    => $frmValue['name'],
						'active' 	 => 1
					);
		$db->do_insert("service_type", $arr);		
		$this->inserted_id = mysql_insert_id();
		return true;
	}
	
}

?>
