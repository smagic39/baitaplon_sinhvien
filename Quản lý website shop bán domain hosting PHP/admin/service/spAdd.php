<?php
/*----------------------------------------
| ADD NEW SERVICE PLAN
------------------------------------------*/

// Check for security
if (!defined('HCR'))
{
	print '<h1>Incorrect Access!</h1>';
	exit();
}

$cnt = new content;

class content{

	public  $title = "Thêm gói dịch vụ mới";
	public  $text = "";	
	private $process = "";
	private $inserted_id = 0;
	private $frmValue = array(
								 'title'    => '',
								 'type'     => '',
								 'intro'    => '',
								 'content'  => '',
								 'showhome' => '',
								 'price' 	=> '',
								 'allow_order' => '',								
							 );
	private $warning = "";
	private $err = "";
			
			
	function __construct()
	{
		global $str, $sess;		
		$this->get_input();
		$check_input = $this->check_input($this->frmValue);
		
		if ($this->process == "addSP")
		{ 
			if ($check_input)
			{
				$this->insert_field($this->frmValue);
				$this->warning = "<span class='span_ok'>Thêm gói dịch vụ mới thành công !!</span><ul>";
				$this->warning .= "<ol><img src='". ADMIN_IMG ."red.gif' align='absmiddle' /> Nhấp <a href='?mod=spEdit&id=". $this->inserted_id ."' class='topadd'>vào đây</a> để hiệu chỉnh.</ol>";
				$this->warning .= "<ol><img src='". ADMIN_IMG ."red.gif' align='absmiddle'/> Nhấp <a href='?mod=spList' class='topadd'>vào đây</a> để xem danh sách.<ol></ul><br/>";
				        
						$this->frmValue['title'] = "";
						$this->frmValue['intro'] = "";
						$this->frmValue['content'] = "";
						$this->frmValue['price'] = "";
						$this->err = "";
			
			}
			else
			{
				$this->warning = "<b><u>Lỗi nhập liệu</u>:</b>&nbsp;<span class='span_err'><ul>". $this->err ."</ul></span>";
			}
		}		
		
		$this->text = ($this->warning) ? "<div id='warning'>" . $this->warning . "</div>" . $this->show_form($this->frmValue) : $this->show_form($this->frmValue);
	}	
	
	/*---------------------------------------------------
	 | SHOW FORM 
	 ---------------------------------------------------*/
	function show_form( $frmValue )
	{
		global $frm, $db, $sess, $time, $dsp;
		
		$text = $frm->draw_form("", "", 2, "POST", "frmSP");
		$text .= $frm->draw_hidden("process", "addSP");
		$text .= "<table cellspacing='0' cellspadding='4' class='tbl_add'>
				  <tr><td colspan='2' height='10'></td></tr>";
					
		// Show list of product type
		$text .= "<tr><td>Loại dịch vụ : </td>
		          <td>". $dsp->showServicesList('type', $frmValue['type'], '', FALSE) ."</td>";	
		$text .= "</tr>
				  <tr><td colspan='2' height='6'></td></tr>"; 
				
		$text .= "<tr>";
		$text .= "<td>Tên gói dịch vụ : </td>";
		$text .= "<td>". $frm->draw_textfield("title", $frmValue["title"], "", "50", "255") ."</td>
				  </tr>
				  <tr><td colspan='2' height='6'></td></tr>	";
				  
	    $text .= "<tr>";
		$text .= "<td>Hiện tại trang chủ : </td>";
		$text .= "<td>". $frm->draw_checkbox("showhome","1","",($frmValue['showhome'] == 1 ? 'checked':'')) ."</td>";
		$text .= "</tr>
				  <tr><td colspan='2' height='6'><font color='#0000FF'>* Nếu hiển thị gói dịch vụ tại trang chủ. Vui lòng nhập thông tin giới thiệu bên dưới</font></td></tr>";	
		  
				  
		$text .= "<tr>";
		$text .= "<td>Thông tin giá: </td>";
		$text .= "<td>". $frm->draw_textfield("price", $frmValue["price"], "", "30", "255") ."</td>
				  </tr>
				  <tr><td colspan='2' height='6'></td></tr>	";
		
		$text .= "<tr>";
		$text .= "<td>Cho phép đăng kí: </td>";
		$text .= "<td>". $frm->draw_checkbox("allow_order", "1","", "checked") ."</td></tr>
				<tr><td colspan='2' height='6'><font color='#0000FF'>* Nếu chọn - gói dịch vụ sẽ được hiển thị để đăng kí tại trang đăng kí</font></td></tr>";
		
		// Content
		$text .= "<tr>";
		$text .= "<td>Giới thiệu : </td><td>";
			// Embed FCKEditor
			$oFCKeditor = new FCKeditor('intro') ;
			$oFCKeditor->BasePath = DIR_LIB_EDITOR;
			$oFCKeditor->Value = $frmValue['intro'];
			$oFCKeditor->Width  = '650' ;
			$oFCKeditor->Height = '400' ;					
		$text .= $oFCKeditor->Create() ."</td></tr>";
		$text .= "<tr><td colspan='2' height='6'></td></tr>";
				  
		// Content
		$text .= "<tr>";
		$text .= "<td>Nội dung : </td><td>";
			// Embed FCKEditor
			$oFCKeditor = new FCKeditor('content') ;
			$oFCKeditor->BasePath = DIR_LIB_EDITOR;
			$oFCKeditor->Value = $frmValue['content'];
			$oFCKeditor->Width  = '650' ;
			$oFCKeditor->Height = '600' ;					
		$text .= $oFCKeditor->Create() ."</td></tr>";
		$text .= "<tr><td colspan='2' height='6'></td></tr>";
		
		$text .= "<tr>";
		$text .=   "<td colspan='2' align=center>";
		$text .= $frm->draw_submit(" Thêm ", "");
		$text .= "&nbsp;&nbsp;<input type='reset' value=' Hủy '></td>";
		$text .= "</tr>";
			
		$text .= "</table>";		
		$text .= "</form>";			
		
		return $text;
	}
	
	
	/*----------------------------------------------------
	 | GET INPUT DATA
	+-----------------------------------------------------*/
	function get_input()
	{
		global $str;
		
		$this->process = isset( $_POST['process']) ? $str->input($_POST['process'] ) : "";				
		$this->frmValue['title'] = isset($_POST['title']) ? $str->input($_POST['title']) : "";
		$this->frmValue['type'] = isset($_POST['type']) ? $str->input($_POST['type']) : "";
		$this->frmValue['showhome'] = isset($_POST['showhome']) ? 1 : 0;
		$this->frmValue['intro'] = isset($_POST['intro']) ? $str->input_html($_POST['intro']) : "";
		$this->frmValue['content'] = isset($_POST['content']) ? $str->input_html($_POST['content']) : "";
		$this->frmValue['price'] = isset($_POST['price']) ? $str->input($_POST['price']) : "";
		$this->frmValue['allow_order'] = isset($_POST['allow_order']) ? 1 : 0;
				
	}
	
		
	/*-----------------------------------------------------
	 | CHECK FOR INPUT DATA
	+------------------------------------------------------*/
	function check_input($frmValue)
	{
		global $frm;		
		$no_error = true;
		
		if ($frmValue['type'] <= 0)
		{
			 $no_error = false;
			 $this->err .= "<li>Hãy chọn loại dịch vụ</li>";
		}
		
		if ( !$frm->check_input($frmValue['title'], 1) )
		{
			$no_error = false;
			$this->err .= "<li>Hãy nhập tên gói dịch vụ</li>";
		}
		
		if ($frmValue['showhome'] == 1)
		{
			if (!$frm->check_input($frmValue['intro'], 1))
			{
				$no_error = false;
				$this->err .= "<li>Hãy nhập thông tin giới thiệu gói dịch vụ</li>";
			}
			if (!$frm->check_input($frmValue['price'], 1))
			{
				$no_error = false;
				$this->err .= "<li>Hãy nhập thông tin giá</li>";
			}
		}
		
		if (!$frm->check_input($frmValue['content'], 1))
		{
			$no_error = false;
			$this->err .= "<li>Hãy nhập nội dung gói dịch vụ</li>";
		}
			
		return $no_error;
	}
	
	
	/*------------------------------------------------
	 | INSERT INTO DATA
	+------------------------------------------------*/
	function insert_field($frmValue)
	{
		global $db, $time;
		$str_replace = array("á","à","ã","â","é","è","ê","í","ì","ý","ú","ù","ó","ò","õ","ô","Á","À","Ã","Â","É","È","Ê","Í","Ì","Ý","Ú","Ù","Ó","Ò","Õ","Ô");
		$str = array("&aacute;","&agrave;","&atilde;","&acirc;","&eacute;","&egrave;","&ecirc;","&iacute;","&igrave;","&yacute;","&uacute;","&ugrave;","&oacute;","&ograve;","&otilde;","&ocirc;","&Aacute;","&Agrave;","&Atilde;","&Acirc;","&Eacute;","&Egrave;","&Ecirc;","&Iacute;","&Igrave;","&Yacute;","&Uacute;","&Ugrave;","&Oacute;","&Ograve;","&Otilde;","&Ocirc;");
		$frmValue['content'] = str_replace($str, $str_replace, $frmValue['content']);       
		$frmValue['intro'] = str_replace($str, $str_replace, $frmValue['intro']);       
		// Add to db
		$arr = array(
						'title'	      => $frmValue['title'],
						'service_id'  => intval($frmValue['type']),
						'showhome'    => intval($frmValue['showhome']),
						'intro'       => $frmValue['intro'],
						'content'     => $frmValue['content'],
						'price'       => $frmValue['price'],
						'allow_order' => $frmValue['allow_order']
					);
		$db->do_insert("service_plan", $arr);		
		$this->inserted_id = mysql_insert_id();
		return true;
	}
	
}

?>