<?php
/*----------------------------------------
| ADD NEW SUPPORT INFORMATION
------------------------------------------*/

// Check for security
if (!defined('HCR'))
{
	print '<h1>Incorrect Access!</h1>';
	exit();
}

$cnt = new content;

class content{

	public $title = "Thêm thông tin hỗ trợ mới";
	public $text = "";	
	private $process = "";
	private $inserted_id = 0;
	private $frmValue = array(
								'title'    => '',
								'content'  => '',
								'rank'     => '',
							 );
							 
	private $file = array(
							  'name' => '',
							  'type' => '',
							  'size' => '',
							  'tmp'  => ''
						  );
					  
	
	private $warning = '';
	private $err = '';
		
		
	function __construct()
	{
		global $str, $sess, $db;				
		 
		$this->get_input();
		$check_input = $this->check_input( $this->frmValue );	

		if ($this->process == "addSupport")
		{ 
			if ($check_input)
			{ 
				$this->insert_field( $this->frmValue );
				$this->warning = "<span class='span_ok'>Thêm thông tin hỗ trợ mới thành công !!</span>";
				$this->warning .= "<ul><ol><img src='". ADMIN_IMG ."red.gif' align='absmiddle' /> Nhấp <a href='?mod=supportEdit&id=". $this->inserted_id ."' class='topadd'>vào đây</a> để hiệu chỉnh.</ol>";
				$this->warning .= "<ol><img src='". ADMIN_IMG ."red.gif' align='absmiddle'/> Nhấp <a href='?mod=supportList' class='topadd'>vào đây</a> để xem danh sách.</ol></ul><br/>";
				
				$this->frmValue['title'] = "";
				$this->frmValue['content'] = "";
				$this->frmValue['rank'] = "";
				$this->err = "";
				
			}
			else
			{
				$this->warning = "<b><u>Lỗi nhập liệu</u>:</b>&nbsp;<span class='span_err'><ul>". $this->err ."</ul></span>";
			}
		} 
		
		$this->text = ($this->warning) ? "<div id='warning'>" . $this->warning . "</div>" . $this->show_form($this->frmValue) : $this->show_form($this->frmValue);
	}	
	
	/*-------------------------------------------
	  SHOW FORM FOR INPUT DATA
	 -------------------------------------------*/
	function show_form($frmValue)
	{
		global $frm, $db, $sess, $time;
		
		$text = $frm->draw_form( "", "", 2, "POST", "frm_addSupport" );
		$text .= $frm->draw_hidden("process", "addSupport");		

		$text .= "<table cellspacing='0' cellpadding='4' class='tbl_add'>";
			
		$text .= "<tr>";
		$text .= "<td>Tiêu đề : </td>";
		$text .= "<td>". $frm->draw_textfield("title", $frmValue["title"], "", "60", "255" ) ."</td>";
		$text .= "</tr>
				  <tr><td colspan='2' height='6'></td></tr>";
				  
		$text .= "<tr>";
		$text .= "<td>Thứ tự : </td>";
		$text .= "<td>". $frm->draw_textfield("rank", $frmValue["rank"], "", "10", "10" ) ."</td>";
		$text .= "</tr>
				  <tr><td colspan='2' height='6'></td></tr>";
				
		$text .= "<tr>";
		$text .= "<td>Nội dung : </td><td>";
			// Embed FCKEditor
			$oFCKeditor = new FCKeditor('content') ;
			$oFCKeditor->BasePath = DIR_LIB_EDITOR;
			$oFCKeditor->Value = $frmValue['content'];
			$oFCKeditor->Width  = '650' ;
			$oFCKeditor->Height = '600' ;					
		$text .= $oFCKeditor->Create() ."</td></tr>";
		$text .= "<tr><td colspan='2' height='6'></td></tr>";

		
		$text .= "<tr>";
		$text .= "<td colspan='2' align=center>";
		$text .= $frm->draw_submit(" Thêm ", "");
		$text .= "&nbsp;&nbsp;<input type='reset' value=' Hủy '></td>";
		$text .= "</tr>";
					
		$text .= "</table>";		
		$text .= "</form>";
				
		return $text;
	}
	
	/*----------------------------------------------
	 | GET INPUT DATA
	+-----------------------------------------------*/
	function get_input()
	{
		global $str;
		
	  	$this->process = isset($_POST['process']) ? $str->input($_POST['process']) : "";		
	  	$this->frmValue['title'] = isset($_POST['title']) ? $str->input($_POST['title']) : "";
		$this->frmValue['rank'] = isset($_POST['rank']) ? $str->input($_POST['rank']) : "";
		$this->frmValue['content'] = isset($_POST['content']) ? $str->input_html($_POST['content']) : "";		
		
	}
	
	
	/*----------------------------------
	| CHECK FOR INPUT DATA	
	+-----------------------------------*/
	function check_input($frmValue)
	{
		global $frm, $time, $db, $str;
		
		$no_error = true;
		if ( !$frm->check_input($frmValue['title'], 1) )
		{
			 $no_error = false;
			 $this->err .= "<li>Hãy nhập tiêu đề</li>";
		}
		

		if ( !$frm->check_input($frmValue['content'], 1) )
		{
			$no_error = false;
			$this->err .= "<li>Hãy nhập nội dung</li>";
		}
		
		return $no_error;
	}
	
	/*---------------------------------------------
	| ADD INFORMATIONS TO DB
	+----------------------------------------------*/
	function insert_field($frmValue)
	{
		global $db, $time, $sess;
				
		$str_replace = array("á","à","ã","â","é","è","ê","í","ì","ý","ú","ù","ó","ò","õ","ô","Á","À","Ã","Â","É","È","Ê","Í","Ì","Ý","Ú","Ù","Ó","Ò","Õ","Ô");
		$str = array("&aacute;","&agrave;","&atilde;","&acirc;","&eacute;","&egrave;","&ecirc;","&iacute;","&igrave;","&yacute;","&uacute;","&ugrave;","&oacute;","&ograve;","&otilde;","&ocirc;","&Aacute;","&Agrave;","&Atilde;","&Acirc;","&Eacute;","&Egrave;","&Ecirc;","&Iacute;","&Igrave;","&Yacute;","&Uacute;","&Ugrave;","&Oacute;","&Ograve;","&Otilde;","&Ocirc;");
		$frmValue['content'] = str_replace($str, $str_replace, $frmValue['content']);

		$arr = array(
						'title' 	=> $frmValue['title'],
						'content' 	=> $frmValue['content'],
						'rank' 		=> $frmValue['rank']
						
					);
		
		$db->do_insert("support", $arr);		
		$this->inserted_id = mysql_insert_id();
		return true;
	}
}

?>