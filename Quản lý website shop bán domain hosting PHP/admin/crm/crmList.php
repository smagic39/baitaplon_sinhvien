<?php
/*-----------------------------------
* LIST OF SERVICE INTRODUTION
-------------------------------------*/

// Check for security
if (!defined('HCR'))
{
	print '<h1>Incorrect Access!</h1>';
	exit();
}

$cnt = new content;

class content{

	public $title = "Các liên kết đến CRM";
	public $text = "";	
	private $process = "";
	private $frmValue = array('update' => '');
	private $page = "";
	
  	function __construct()
    {  
		 global $str, $sess, $db;
		
	 	// Show form content	  
		$this->text = $this->show_form($this->frmValue);
	}
	
	
	/*----------------------------------------
	 | SHOW FORM
	+ ----------------------------------------*/
	function show_form($frmValue)
	{
		global $frm, $db, $str, $sess, $time, $token, $hpaging;
		
		$query = $db->simple_select('*', 'crm');			 	
		$result = $db->query($query);		
		$count = 0;		

		// Prepare the form
		$text .= $frm->draw_form("", "", 2, "POST", "frmCRM");
								
		$text .=  "<table cellspacing='0' cellpadding='6' class='tbl_main' align='left'>";
		
		$text .=   "<tr class='trc'>					
						<td>Tiêu đề liên kết</td>
						<td>Link</td>
						<td>Cập nhật</td>
					</tr>";
					
		while ($row = $db->fetch_assoc($result))
		{  
		    $count += 1; 
			switch ($row['id'])
			{
				case 1 : $title = 'Liên hệ kinh doanh'; break;
				case 2 : $title = 'Hỗ trợ kỹ thuật'; break;
				case 3 : $title = 'Than phiền dịch vụ'; break;
				case 4 : $title = 'Góp ý chất lượng'; break;
				default : $title = 'Liên hệ kinh doanh';
			}
			$text .= "<tr class=".($count%2? "ho" : "hr").">
						<td>". $title ."</td>
					    <td><a href='".$row['blink']."' class='external' target='_blank'>". $row['blink'] ."</a></td>";	
							
			$text .= "<td>
					  <a href='?mod=crmEdit&id=".$row['id']."' class='style10'>Cập nhật</a>&nbsp;
					  </td>
					</tr>";
		   }	
		 
		$text .=  "</table>";		
		$text .=   "</form>";			  
					
		return $text;
	}
	
	/*---------------------------------------------
	 | GET INPUT DATA AND ACTIONS
	+----------------------------------------------*/
	function get_input()
	{
		global $str;
		$this->page = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$this->process = isset($_POST['process']) ? $str->input($_POST['process']) : "";
		$this->frmValue['update'] = isset($_POST['Update']) ? $str->input($_POST['Update']) : "";
		
    }		

}

?>