<?php
/*--------------------------------------------------------
 | ADMIN INFORMATION
 --------------------------------------------------------*/

//  Check for security
if ( !defined('HCR'))
  {
  	print '<h1>Incorrect Access</h1>';
	exit;
  }

$cnt = new content;

class content
{
		
	public $title = "Thông tin người quản trị";
	public $text = "";
	public $warning = "";
	public $frmValue = array(
								'name'     => '',
								'email'    => ''
							 );
	private $process = "";
		
	function __construct()
		{  
		   $this->get_input();
		   if ( $this->process == "update" )	
		   {  
			 	$this->update_field($this->frmValue);
			    $this->warning = "<span class='span_ok'>Cập nhật thông tin thành công !!</span>";
			}
			
		   $this->text = $this->warning ? "<div style='padding:5px'>". $this->warning ."</div>". $this->show_form($this->frmValue) : $this->show_form($this->frmValue);		
		  }

	/*------------------------------------------------
	| SHOW FORM 
	 ------------------------------------------------*/
	 function show_form($frmValue)
	 { 
	  	global $db , $frm, $str;
	  
		$sql = $db->simple_select("*","adminfo");
		$query = $db->query($sql);
		$row = @mysql_fetch_array($query);
			
		 
	    $text = '<form method="post" name="frmadmInfo" action="" onsubmit="return checkInput()">';
		$text .= $frm->draw_hidden("process","update");
		$text .= "<table cellspacing='0' cellspadding='4' class='tbl_edit'>";

		$text .= "<tr>";
		$text .= "<td>Tên người quản trị : </td>";
		$text .= "<td>".$frm->draw_textfield("name", $frmValue["name"] ? $frmValue["name"] : $row['name'],"","50","255")."</td>";
		$text .=   "</tr>
					<tr><td colspan='2' height='6'></td></tr>";	
		
		$text .= "<tr>";
		$text .= "<td>Email người quản trị : </td>";	
		$text .= "<td>".$frm->draw_textfield("email", $frmValue["email"] ? $frmValue["email"] : $row['email'],"","50","255")."</td>";
		$text .=   "</tr>
					<tr><td colspan='2' height='6'></td></tr>";	
		
		    						
		$text .=  "<tr>";
		$text .=  "<td colspan='2' align=center>";
		$text .=  $frm->draw_submit(" Cập nhật ", "");
		$text .=  "</td>";
		$text .=  "</tr>";


		$text .= "</table>";
		$text .= "</form>";
		
		// JS
		$text .= '<script language="javascript">
					function checkInput()
					{
						if (document.frmadmInfo.name.value == "")
						{
							alert("Hãy nhập tên người quản trị");
							document.frmadmInfo.name.focus();
							return false;
						}
						
						if (!validateEmail(document.frmadmInfo.email.value))
						{
							alert("Email không hợp lệ");
							document.frmadmInfo.email.focus();
							return false;
						}
						return true;
					}
					
					function validateEmail(elementValue)
					{  
						 var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;  
						 return emailPattern.test(elementValue);  
					}  
					</script>
		         ';
		
		return $text ;
	}	   
		 
	
	/*--------------------------------------------
	| GET INPUT DATA
	---------------------------------------------*/
	function get_input()
	{ 
		global $str;
	    $this->process = isset($_POST['process']) ? $_POST['process'] : "";
	  	$this->frmValue['email'] = isset($_POST['email']) ? $str->input($_POST['email']) : "";
		$this->frmValue['name'] = isset($_POST['name']) ? $str->input($_POST['name']) : "";   
	}
	
	
	/*-------------------------------------------
	| UPDATE DATA
	 ------------------------------------------*/
	 function update_field($frmValue)
	 {
	   	 global $db;
	     $arr = array(
		 				   "name" => $frmValue['name'],
						   "email" => $frmValue['email'],
		 			  );
		$db->do_update("adminfo",$arr);
		return true;
	   
	  }
 } // end class


?>
