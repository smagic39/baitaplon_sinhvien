<?php
/*----------------------------------------
 * EDIT FAQ INFORMATION
------------------------------------------*/

// Check for security
if (!defined('HCR'))
{
	print '<h1>Incorrect Access!</h1>';
	exit();
}

$cnt = new content;

class content{

	public $title = 'Hiệu chỉnh thông tin hỏi đáp';
	public $text = '';	
	private $process = '';
	private $fID = '';
	private $frmValue = array(
								 'question'  => '',
								 'answer' 	 => ''
						     );
							  
	private $file = array(
							 'name' => '',
							 'type' => '',
							 'size' => '',
							 'tmp' => ''
						 );
					
	private $warning = '';
	private $err = '';
		
		
	function __construct()
	{
		global $str, $sess, $token;
		
		$this->fID = isset($_GET['id']) ? $_GET['id'] : 0;
		$this->get_input();
		$check_input = $this->check_input($this->frmValue);

		if ($this->process == "editFaq")
		{ 
			if ($check_input)
			{				
				$this->update_field($this->frmValue);
				if (isset($_GET['r']) && strlen($_GET['r']) > 0)				
				{
					$str->goto_url(urldecode($_GET['r']));	
				}
				$str->goto_url('?mod=faqList');				
			}
			else
			{
				$this->warning = "<b><u>Lỗi nhập liệu</u>:&nbsp;</b><span class='span_err'><ul>". $this->err ."</ul></span>";
			}
		} 
		
		$this->text = ($this->warning) ? "<div id='warning'>" . $this->warning . "</div>" . $this->show_form($this->frmValue) : $this->show_form($this->frmValue);
	}	
	
	
	/*---------------------------
	 | SHOW FORM 
	+ ---------------------------
	*/
	function show_form($frmValue)
	{
		global $frm, $db, $sess, $time;
		
		$query = $db->simple_select('*', 'faq', 'id = "'. $this->fID .'"');
		$result = $db->query($query);
		$row = $db->fetch_assoc($result);
		$text = '';
		
	
		$text .= $frm->draw_form("", "", 2, "POST", "frm_editFaq");
		$text .= $frm->draw_hidden("process", "editFaq");
		
		$text .= '<table cellpadding="4" cellspacing="0" class="tbl_edit">';			
			
		$text .= "<tr>";
		$text .= "<td>Câu hỏi : </td>";
		$text .= "<td><textarea name='question' rows='3' cols='50'>".($frmValue['question'] ? $frmValue['question'] : $row['question'])."</textarea></td>";
		$text .= "</tr>
				  <tr><td colspan='2' height='6'></td></tr>";	
				  
		
		$text .= "<tr>";
		$text .= "<td>Trả lời : </td><td>";		
	        // Embed FCKEditor
			$oFCKeditor = new FCKeditor('answer');
			$oFCKeditor->BasePath = DIR_LIB_EDITOR;
			$oFCKeditor->Value = $frmValue['answer'] ? $frmValue['answer'] : $row['answer'];
			$oFCKeditor->Width  = '650' ;
			$oFCKeditor->Height = '500' ;				
		$text .= $oFCKeditor->Create() ."</td></tr>";
		$text .= "<tr><td colspan='2' height='6'></td></tr>";
		
		
			
		$text .= "<tr>";
		$text .= "<td colspan='2' align='center'>";
		$text .= $frm->draw_submit(" Cập nhật ", "");
		$text .=  "&nbsp;&nbsp;<input type='reset' value=' Hủy '></td>";
		$text .=  "</tr>";
			
		$text .=  "</table>";		
		$text .=  "</form>";
		
		return $text;
	}
	
	
	/*-------------------------
	 | GET INPUT DATA
	+--------------------------*/
	function get_input()
	{
		global $str;
		
		$this->process = isset($_POST['process']) ? $str->input($_POST['process']) : "";		
	   	$this->frmValue['question'] = isset($_POST['question']) ? $str->input($_POST['question']) : "";
		$this->frmValue['answer'] = isset($_POST['answer']) ? $str->input_html($_POST['answer']) : "";
	}
	
	
	/*----------------------------
	 | CHECK FOR INPUT DATA
	+-----------------------------*/
	function check_input($frmValue)
	{
		global $frm, $str, $db;		
		$no_error = true;
				
		if (!$frm->check_input($frmValue['question'], 1))
		{
			$no_error = false; 
			$this->err .= '<li>Hãy nhập câu hỏi</li>';
		}
		
		
		if (!$frm->check_input($frmValue['answer'], 1))
		{
			$no_error = false; 
			$this->err .= '<li>Hãy nhập trả lời</li>';
		}
		
		return $no_error;
	}
	
	
	
	// UPDATE DATA TO DB
	function update_field($frmValue)
	{
		global $db, $time, $sess;		
		
		$str_replace = array("á","à","ã","â","é","è","ê","í","ì","ý","ú","ù","ó","ò","õ","ô","Á","À","Ã","Â","É","È","Ê","Í","Ì","Ý","Ú","Ù","Ó","Ò","Õ","Ô");
		$str = array("&aacute;","&agrave;","&atilde;","&acirc;","&eacute;","&egrave;","&ecirc;","&iacute;","&igrave;","&yacute;","&uacute;","&ugrave;","&oacute;","&ograve;","&otilde;","&ocirc;","&Aacute;","&Agrave;","&Atilde;","&Acirc;","&Eacute;","&Egrave;","&Ecirc;","&Iacute;","&Igrave;","&Yacute;","&Uacute;","&Ugrave;","&Oacute;","&Ograve;","&Otilde;","&Ocirc;");
		
		$frmValue['answer'] = str_replace($str, $str_replace, $frmValue['answer']);	
		$arr = array(
						'question' 	 => $frmValue['question'],
						'answer' 	 => $frmValue['answer'],
					);	
		
		$db->do_update("faq", $arr, "id = '". $this->fID ."'" );		
		return true;
	}
	
 
}

?>
