<?php
/*--------------------------------------------------------
 | GET INFORMATION SETTINGS FOR PAGING
 --------------------------------------------------------*/

//  Check for security
if ( !defined('HCR'))
  {
  	print '<h1>Incorrect Access</h1>';
	exit;
  }

$cnt = new content;

class content
{
		
	public $title = "Thiết lập thông số phân trang";
	public $text = "";
	public $warning = "";
	public $frmValue = array(
								'item'     => '',
								'bitem'    => '',
								'segment'  => '',
								'bsegment' => '',
							 );
	private $process = "";
		
	function __construct()
		{  
		   $this->get_input();
		   if ( $this->process == "update" )	
		   {  
			 	$this->update_field($this->frmValue);
			    $this->warning = "<span class='span_ok'>Cập nhật thông tin thành công !!</span>";
			}
			
		   $this->text = $this->warning ? "<div style='padding:5px'>". $this->warning ."</div>". $this->show_form($this->frmValue) : $this->show_form($this->frmValue);		
		  }

	/*------------------------------------------------
	| SHOW FORM 
	 ------------------------------------------------*/
	 function show_form($frmValue)
	 { 
	  	global $db , $frm, $str;
	  
		$sql = $db->simple_select("*","paging_setting");
		$query = $db->query($sql);
		$row = @mysql_fetch_array($query);
		
		$item = "<select name='item'>";
		$bitem = "<select name='bitem'>";
		for ( $i=1 ; $i<10 ; $i++ )
		{  $va = $i*5;
		    $selected =  ($frmValue['item']==$va) || ($row['item']==$va)  ? "selected" : ""; 
			$bselected =  ($frmValue['bitem']==$va) || ($row['bitem']==$va)  ? "selected" : ""; 
		 	$item .= "<option value='". $va ."' ". $selected ." >&nbsp;". $va ."&nbsp;</option>";
			$bitem .= "<option value='". $va ."' ". $bselected ." >&nbsp;". $va ."&nbsp;</option>";
		}
		$item .= "</select>";
		$bitem .= "</select>";
		 
		 
	
		$page = "<select name='segment'>";
		$bpage = "<select name='bsegment'>";
		for ( $i=2 ; $i<10 ; $i++ )
		{ 
			$selected = ($frmValue['segment']==$i) || ($row['segment']==$i) ? "selected" : "";
			$bselected = ($frmValue['bsegment']==$i) || ($row['bsegment']==$i) ? "selected" : "";
		 	$page .= "<option value='". $i ."' ". $selected ." >&nbsp;". $i ."&nbsp;</option>";
			$bpage .= "<option value='". $i ."' ". $bselected ." >&nbsp;". $i ."&nbsp;</option>";
		}
		$page .= "</select>";
		$bpage .= "</select>";
		 
	    $text = $frm->draw_form("","",2,"POST");
		$text .= $frm->draw_hidden("process","update");
		$text .= "<div class='pagemain'>";
			$text .= "<div class='paco'>";
				$text .= "<div class='pageset'>Item/Trang &nbsp;</div>";
				$text .= '<div>'. $item .'</div>';
			$text .= "</div>";
			
			$text .= "<div class='paco'>";
				$text .= "<div class='pageset'>Item/Trang(Phần quản trị) &nbsp;</div>";
				$text .= '<div>'. $bitem .'</div>';
			$text .= "</div>";
			
			$text .= "<div class='paco'>";
				$text .= "<div class='pageset'>Trang/Phân đoạn &nbsp;</div>";
				$text .= '<div>'. $page .'</div>';
			$text .= "</div>";
		    
			$text .= "<div class='paco'>";
				$text .= "<div class='pageset'>Trang/Phân đoạn(Phần quản trị) &nbsp;</div>";
				$text .= '<div>'. $bpage .'</div>';
			$text .= "</div>";
			
			$text .= "<div class='pagebr'>";
				$text .= $frm->draw_submit('Cập nhật');
			$text.= "</div>";
		$text .= "</div>";
		
		$text .= "</form";
		
		return $text ;
	}	   
		 
	
	/*--------------------------------------------
	| GET INPUT DATA
	---------------------------------------------*/
	function get_input()
	{ 
	    $this->process = isset($_POST['process']) ? $_POST['process'] : "";
	  	$this->frmValue['item'] = isset($_POST['item']) ? intval($_POST['item']) : "";
		$this->frmValue['bitem'] = isset($_POST['bitem']) ? intval($_POST['bitem']) : "";
		$this->frmValue['segment'] = isset($_POST['segment']) ? intval($_POST['segment']) : "";
		$this->frmValue['bsegment'] = isset($_POST['bsegment']) ? intval($_POST['bsegment']) : "";	   
	}
	
	
	/*-------------------------------------------
	| UPDATE DATA
	 ------------------------------------------*/
	 function update_field($frmValue)
	 {
	   	 global $db;
	     $arr = array(
		 				   "item" => $frmValue['item'],
						   "bitem" => $frmValue['bitem'],
						   "segment" => $frmValue['segment'],
						   "bsegment" => $frmValue['bsegment'],
		 			  );
		$db->do_update("paging_setting",$arr);
		return true;
	   
	  }
 } // end class


?>
