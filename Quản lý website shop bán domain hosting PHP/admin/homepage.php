<?php
/*------------------------------------------
 | HOMEPAGE FOR ADMIN PANEL
 ------------------------------------------*/
 
// Check for Security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}

$cnt = new content;

class content
{
	public $text = '';
	public $title = "WEBSITE CONTROL PANEL";	
	
	function __construct()
	{
		$this->text = "<font color='#0000FF'>Chào mừng đến phần quản trị website. Trong quá trình truy cập và quản trị thông tin, xin lưu ý</font>";
		$this->text .= '						
						<ul style="color:#FF0000">
						<li style="line-height:22px;">Nhập thông tin đầy đủ và chính xác khi thêm & cập nhật nội dung</li>
						<li style="line-height:22px;">Kiểm tra kỹ thông tin trước khi chọn xóa, ngừng kích hoạt</li>
						<li style="line-height:22px;">Item được sắp xếp theo thứ tự tăng dần</li>
						<li style="line-height:22px;">Kiểm tra kích thước của các thẻ HTML khi sử dụng editor để không ảnh hưởng đến frontpage</li>
						<li style="line-height:22px;">Upload hình ảnh đúng định dạng(jpg,gif,png), đúng kích cỡ và kích thước quy định</li>
						<li style="line-height:22px;">Không chọn chức năng lưu mật khẩu của trình duyệt và log out sau khi đã hoàn thành việc cập nhật thông tin</li>
						</ul>';
	}

 }

?>
