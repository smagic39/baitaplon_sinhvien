<?php
/*----------------------------------------
| ADD NEW SERVICE INFORMATION
------------------------------------------*/

// Check for security
if (!defined('HCR'))
{
	print '<h1>Incorrect Access!</h1>';
	exit();
}

$cnt = new content;

class content{

	public $title = "Thêm thông tin giới thiệu dịch vụ";
	public $text = "";	
	private $process = "";
	private $inserted_id = 0;
	private $frmValue = array(
								'name' 	  => '',
								'intro'   => '',
								'content' => '',
								'rank'    => '',
							 );
							 
	private $file = array(
							  'name' => '',
							  'type' => '',
							  'size' => '',
							  'tmp'  => ''
						  );
					  
	
	private $warning = '';
	private $err = '';
		
		
	function __construct()
	{
		global $str, $sess, $db;				
		 
		$this->get_input();
		$check_input = $this->check_input( $this->frmValue );	

		if ($this->process == "addSI")
		{ 
			if ($check_input)
			{ 
				$this->insert_field( $this->frmValue );
				$this->warning = "<span class='span_ok'>Thêm thông tin dịch vụ thành công !!</span>";
				$this->warning .= "<ul><ol><img src='". ADMIN_IMG ."red.gif' align='absmiddle' /> Nhấp <a href='?mod=siEdit&id=". $this->inserted_id ."' class='topadd'>vào đây</a> để hiệu chỉnh.</ol>";
				$this->warning .= "<ol><img src='". ADMIN_IMG ."red.gif' align='absmiddle'/> Nhấp <a href='?mod=siList' class='topadd'>vào đây</a> để xem danh sách.</ol></ul><br/>";
				
				$this->frmValue['name'] = "";
				$this->frmValue['intro'] = "";
				$this->frmValue['content'] = "";
				$this->frmValue['rank'] = "";
				$this->err = "";
				
			}
			else
			{
				$this->warning = "<b><u>Lỗi nhập liệu</u>:</b>&nbsp;<span class='span_err'><ul>". $this->err ."</ul></span>";
			}
		} 
		
		$this->text = ($this->warning) ? "<div id='warning'>" . $this->warning . "</div>" . $this->show_form($this->frmValue) : $this->show_form($this->frmValue);
	}	
	
	/*-------------------------------------------
	  SHOW FORM FOR INPUT DATA
	 -------------------------------------------*/
	function show_form($frmValue)
	{
		global $frm, $db, $sess, $time;
		
		$text = $frm->draw_form( "", "", 2, "POST", "frm_addSI" );
		$text .= $frm->draw_hidden("process", "addSI");		

		$text .= "<table cellspacing='0' cellpadding='4' class='tbl_add'>";
			
		$text .= "<tr>";
		$text .= "<td>Tên dịch vụ : </td>";
		$text .= "<td>". $frm->draw_textfield("name", $frmValue["name"], "", "60", "255" ) ."</td>";
		$text .= "</tr>
				  <tr><td colspan='2' height='6'></td></tr>";
				
		$text .= "<tr>";
		$text .= "<td>Giới thiệu : </td><td>";
			// Embed FCKEditor
			$oFCKeditor = new FCKeditor('intro') ;
			$oFCKeditor->BasePath = DIR_LIB_EDITOR;
			$oFCKeditor->Value = $frmValue['intro'];
			$oFCKeditor->Width  = '650' ;
			$oFCKeditor->Height = '300' ;					
		$text .= $oFCKeditor->Create() ."</td></tr>";
		$text .= "<tr><td colspan='2' height='6'></td></tr>";
		
		$text .= "<tr>";
		$text .= "<td>Nội dung : </td><td>";
			// Embed FCKEditor
			$oFCKeditor = new FCKeditor('content') ;
			$oFCKeditor->BasePath = DIR_LIB_EDITOR;
			$oFCKeditor->Value = $frmValue['content'];
			$oFCKeditor->Width  = '650' ;
			$oFCKeditor->Height = '600' ;					
		$text .= $oFCKeditor->Create() ."</td></tr>";
		$text .= "<tr><td colspan='2' height='6'></td></tr>";
		
		$text .= "<tr>";
		$text .= "<td>Thứ tự : </td>";
		$text .= "<td>". $frm->draw_textfield("rank", $frmValue["rank"], "", "10", "20" ) ."</td>";
		$text .= "</tr>
				  <tr><td colspan='2' height='6'></td></tr>";

		$text .= "<tr>";
		$text .= "<td>Hình ảnh : </td>";
		$text .= "<td>". $frm->draw_file("file") ."&nbsp;<font color='#0000FF'>289x82 px</font>&nbsp;(*.jpg,*.gif,*.png &amp; size<=1MB)</td>";
		$text .= "</tr>
				  <tr><td colspan='2' height='6'></td></tr>";
		
		$text .= "<tr>";
		$text .= "<td colspan='2' align=center>";
		$text .= $frm->draw_submit(" Thêm ", "");
		$text .= "&nbsp;&nbsp;<input type='reset' value=' Hủy '></td>";
		$text .= "</tr>";
					
		$text .= "</table>";		
		$text .= "</form>";
				
		return $text;
	}
	
	/*----------------------------------------------
	 | GET INPUT DATA
	+-----------------------------------------------*/
	function get_input()
	{
		global $str;
		
	  	$this->process = isset($_POST['process']) ? $str->input($_POST['process']) : "";		
	  	$this->frmValue['name'] = isset($_POST['name']) ? $str->input($_POST['name']) : "";
		$this->frmValue['intro'] = isset($_POST['intro']) ? $str->input_html($_POST['intro']) : "";
		$this->frmValue['content'] = isset($_POST['content']) ? $str->input_html($_POST['content']) : "";	
		$this->frmValue['rank'] = isset($_POST['rank']) ? $str->input($_POST['rank']) : "";
		
		if (isset($_FILES['file']['name']))
		{
			$this->file['name'] = $_FILES['file']['name'];
			$this->file['type'] = $_FILES['file']['type'];
			$this->file['size'] = $_FILES['file']['size'];
			$this->file['tmp'] = $_FILES['file']['tmp_name'];
		}
		
		
	}
	
	
	/*----------------------------------
	| CHECK FOR INPUT DATA	
	+-----------------------------------*/
	function check_input($frmValue)
	{
		global $frm, $time, $db, $str;
		
		$no_error = true;
		if ( !$frm->check_input($frmValue['name'], 1) )
		{
			 $no_error = false;
			 $this->err .= "<li>Hãy nhập tên</li>";
		}
		
		
		if ( !$frm->check_input($frmValue['intro'], 1) )
		{
			$no_error = false;
			$this->err .= "<li>Hãy nhập giới thiệu</li>";
		}
		

		if ( !$frm->check_input($frmValue['content'], 1) )
		{
			$no_error = false;
			$this->err .= "<li>Hãy nhập nội dung</li>";
		}
		
		
		// Check for upload file		
		if ( $this->file['name'] )
		{	 
			 if (!is_uploaded_file($this->file['tmp']))
			 {
			 	$no_error = false;
				$this->err .= "<li>Lỗi trong quá trình upload ảnh lên server</li>";
			 }
		
			 if ( ( $this->file['type'] != "image/jpeg" ) && ( $this->file['type'] != "image/gif" ) && ( $this->file['type'] != "image/x-png" ) && ( $this->file['type'] != "image/pjpeg" ) && ( $this->file['type'] != "image/png" ) )
		     {
				$no_error = false;
				$this->err .= "<li>Loại file ảnh không hợp lệ</li>";
			 }
			 			 			 
			 if ($this->file['size'] > 1048576)
			 {
			 	$no_error = false;
				$this->err .= "<li>Kích thước file ảnh quá lớn(<=1MB)</li>";
			 }
		}
		 

		return $no_error;
	}
	
	/*---------------------------------------------
	| ADD INFORMATIONS TO DB
	+----------------------------------------------*/
	function insert_field($frmValue)
	{
		global $db, $time, $sess;
				
		$str_replace = array("á","à","ã","â","é","è","ê","í","ì","ý","ú","ù","ó","ò","õ","ô","Á","À","Ã","Â","É","È","Ê","Í","Ì","Ý","Ú","Ù","Ó","Ò","Õ","Ô");
		$str = array("&aacute;","&agrave;","&atilde;","&acirc;","&eacute;","&egrave;","&ecirc;","&iacute;","&igrave;","&yacute;","&uacute;","&ugrave;","&oacute;","&ograve;","&otilde;","&ocirc;","&Aacute;","&Agrave;","&Atilde;","&Acirc;","&Eacute;","&Egrave;","&Ecirc;","&Iacute;","&Igrave;","&Yacute;","&Uacute;","&Ugrave;","&Oacute;","&Ograve;","&Otilde;","&Ocirc;");
		$frmValue['intro'] = str_replace($str, $str_replace, $frmValue['intro']);
		$frmValue['content'] = str_replace($str, $str_replace, $frmValue['content']);

		
		$arr = array(
						'name' 		 => $frmValue['name'],
						'intro' 	 => $frmValue['intro'],
						'content' 	 => $frmValue['content'],
						'rank' 	     => $frmValue['rank'],						
					);
		
		// Upload file thumbnail
		if ($this->file['name'])
		{
			// Generate image name
			$img_name = rand(9,999999);
			$img_name .= '_'.$this->file['name'];
			$arr['thumb'] = $img_name;
			@move_uploaded_file( $this->file['tmp'], SI_IMG . $img_name );
		}		
		
		$db->do_insert("service_intro", $arr);		
		$this->inserted_id = mysql_insert_id();
		return true;
	}
}

?>