<?php
/*----------------------------------------
 * EDIT SERVICE INFORMATION
------------------------------------------*/

// Check for security
if (!defined('HCR'))
{
	print '<h1>Incorrect Access!</h1>';
	exit();
}

$cnt = new content;

class content{

	public $title = 'Hiệu chỉnh thông tin giới thiệu dịch vụ';
	public $text = '';	
	private $process = '';
	private $siID = '';
	private $frmValue = array(
								 'name' 	 => '',
								 'intro' 	 => '',
								 'content'   => '',
 								 'rank'      => '',								 
						     );
							  
	private $file = array(
							 'name' => '',
							 'type' => '',
							 'size' => '',
							 'tmp' => ''
						 );
					
	private $warning = '';
	private $err = '';
		
		
	function __construct()
	{
		global $str, $sess, $token;
		
		$this->siID = isset($_GET['id']) ? $_GET['id'] : 0;
		$this->get_input();
		$check_input = $this->check_input($this->frmValue);

		if ($this->process == "editSI")
		{ 
			if ($check_input)
			{				
				$this->update_field($this->frmValue);	
				if (isset($_GET['r']) && strlen($_GET['r']) > 0)			
				{
					$str->goto_url(urldecode($_GET['r']));
				}
				$str->goto_url('?mod=siList');				
			}
			else
			{
				$this->warning = "<b><u>Lỗi nhập liệu</u>:&nbsp;</b><span class='span_err'><ul>". $this->err ."</ul></span>";
			}
		} 
		
		$this->text = ($this->warning) ? "<div id='warning'>" . $this->warning . "</div>" . $this->show_form($this->frmValue) : $this->show_form($this->frmValue);
	}	
	
	
	/*---------------------------
	 | SHOW FORM 
	+ ---------------------------
	*/
	function show_form($frmValue)
	{
		global $frm, $db, $sess, $time;
		
		$query = $db->simple_select('*', 'service_intro', 'id = "'. $this->siID .'"');
		$result = $db->query($query);
		$row = $db->fetch_assoc($result);
		$text = '';
		
		// JS for lightbox
		
		$text = '<link rel="stylesheet" href="'. DIR_LIGHTBOX .'css/lightbox.css" type="text/css" media="screen" />
				  <script src="'. DIR_LIGHTBOX .'js/prototype.js" type="text/javascript"></script>
				  <script src="'. DIR_LIGHTBOX .'js/scriptaculous.js?load=effects" type="text/javascript"></script>
				  <script src="'. DIR_LIGHTBOX .'js/lightbox.js" type="text/javascript"></script>';

		$text .= $frm->draw_form("", "", 2, "POST", "frm_editSI");
		$text .= $frm->draw_hidden("process", "editSI");
		
		$text .= '<table cellpadding="4" cellspacing="0" class="tbl_edit">';			
			
	
			
		$text .= "<tr>";
		$text .= "<td>Tên dịch vụ : </td>";
		$text .= "<td>". $frm->draw_textfield("name", $frmValue["name"] ? $frmValue["name"] : $row['name'], "", "60", "255") ."</td>";
		$text .= "</tr>
				  <tr><td colspan='2' height='6'></td></tr>";	
		
		$text .= "<tr>";
		$text .= "<td>Thứ tự : </td>";
		$text .= "<td>". $frm->draw_textfield("rank", $frmValue["rank"] ? $frmValue["rank"] : $row['rank'], "", "10", "20") ."</td>";
		$text .= "</tr>
				  <tr><td colspan='2' height='6'></td></tr>";	
				  
		
		$text .= "<tr>";
		$text .= "<td>Hình hiện tại : </td>";
		$image = $row['thumb'] ? "<a href='". SI_IMG . $row['thumb'] ."' rel='lightbox'><img src='". SI_IMG . $row['thumb'] ."' class='thumbnail' title='Nhấp để xem kích thước thật' /></a>" : "<font color='red'>Chưa có hình</font>";
		
		$text .= "<td>". $image ."</td>";
		$text .= "</tr>
				  <tr><td colspan='2' height='6'></td></tr>";
			
		$text .= "<tr>";
		$text .= "<td>Đổi hình : </td>";
		$text .= "<td>". $frm->draw_file("file") ."&nbsp;<font color='#0000FF'>289x82 px</font>&nbsp;(*.jpg, *.gif, *.png & <=1MB)</td>
		          </tr>
				  <tr><td colspan='2' align='center'><font color='blue'>Nếu không muốn thay đổi,hãy để trống</font></td></tr>
		    	   <tr><td colspan='2' height='6'></td></tr>";

				  
	
		$text .= "<tr>";
		$text .= "<td>Giới thiệu : </td><td>";		
	        // Embed FCKEditor
			$oFCKeditor = new FCKeditor('intro');
			$oFCKeditor->BasePath = DIR_LIB_EDITOR;
			$oFCKeditor->Value = $frmValue['intro'] ? $frmValue['intro'] : $row['intro'];
			$oFCKeditor->Width  = '650' ;
			$oFCKeditor->Height = '300' ;				
		$text .= $oFCKeditor->Create() ."</td></tr>";
		$text .= "<tr><td colspan='2' height='6'></td></tr>";
		
		
		$text .= "<tr>";
		$text .= "<td>Nội dung : </td><td>";		
	        // Embed FCKEditor
			$oFCKeditor = new FCKeditor('content');
			$oFCKeditor->BasePath = DIR_LIB_EDITOR;
			$oFCKeditor->Value = $frmValue['content'] ? $frmValue['content'] : $row['content'];
			$oFCKeditor->Width  = '650' ;
			$oFCKeditor->Height = '600' ;				
		$text .= $oFCKeditor->Create() ."</td></tr>";
		$text .= "<tr><td colspan='2' height='6'></td></tr>";
		
		
			
		$text .= "<tr>";
		$text .= "<td colspan='2' align='center'>";
		$text .= $frm->draw_submit(" Cập nhật ", "");
		$text .=  "&nbsp;&nbsp;<input type='reset' value=' Hủy '></td>";
		$text .=  "</tr>";
			
		$text .=  "</table>";		
		$text .=  "</form>";
		
		return $text;
	}
	
	
	/*-------------------------
	 | GET INPUT DATA
	+--------------------------*/
	function get_input()
	{
		global $str;
		
		$this->process = isset($_POST['process']) ? $str->input($_POST['process']) : "";		
	   	$this->frmValue['name'] = isset($_POST['name']) ? $str->input($_POST['name']) : "";
		$this->frmValue['intro'] = isset($_POST['intro']) ? $str->input_html($_POST['intro']) : "";
		$this->frmValue['content'] = isset($_POST['content']) ? $str->input_html($_POST['content']) : "";
		$this->frmValue['rank'] = isset($_POST['rank']) ? $str->input($_POST['rank']) : "";		
	
		if (isset($_FILES['file']['name']))
		{
			$this->file['name'] = $_FILES['file']['name'];
			$this->file['type'] = $_FILES['file']['type'];
			$this->file['size'] = $_FILES['file']['size'];
			$this->file['tmp']  = $_FILES['file']['tmp_name'];
		}
	    
		
	}
	
	
	/*----------------------------
	 | CHECK FOR INPUT DATA
	+-----------------------------*/
	function check_input($frmValue)
	{
		global $frm, $str, $db;		
		$no_error = true;
				
		if (!$frm->check_input($frmValue['name'], 1))
		{
			$no_error = false; 
			$this->err .= '<li>Hãy nhập tên</li>';
		}
		
		
		if (!$frm->check_input($frmValue['intro'], 1))
		{
			$no_error = false; 
			$this->err .= '<li>Hãy nhập giới thiệu</li>';
		}
		

		if (!$frm->check_input($frmValue['content'], 1))
		{
			$no_error = false; 
			$this->err .= '<li>Hãy nhập nội dung</li>';
		}
		
		
		if ($this->file['name'])
		{
			if (!is_uploaded_file($this->file['tmp']))
			{
				$no_error = false;
				$this->err .= '<li>Lỗi trong quá trình upload ảnh lên server</li>';
			}
		
		    if ( ( $this->file['type'] != "image/jpeg" ) && ( $this->file['type'] != "image/gif" ) && ( $this->file['type'] != "image/x-png" ) && ( $this->file['type'] != "image/pjpeg" ) && ( $this->file['type'] != "image/png" ) )
		    {
				$no_error = false; 
				$this->err .= '<li>Loại file ảnh không hợp lệ</li>';
		    }
						
			if ($this->file['size'] > 1048576)
			{
				$no_error = false;
				$this->err .= '<li>Kích thước file quá lớn(<=1MB)</li>';
			}

		}	
		
		return $no_error;
	}
	
	
	
	// UPDATE DATA TO DB
	function update_field($frmValue)
	{
		global $db, $time, $sess;		
		
		$str_replace = array("á","à","ã","â","é","è","ê","í","ì","ý","ú","ù","ó","ò","õ","ô","Á","À","Ã","Â","É","È","Ê","Í","Ì","Ý","Ú","Ù","Ó","Ò","Õ","Ô");
		$str = array("&aacute;","&agrave;","&atilde;","&acirc;","&eacute;","&egrave;","&ecirc;","&iacute;","&igrave;","&yacute;","&uacute;","&ugrave;","&oacute;","&ograve;","&otilde;","&ocirc;","&Aacute;","&Agrave;","&Atilde;","&Acirc;","&Eacute;","&Egrave;","&Ecirc;","&Iacute;","&Igrave;","&Yacute;","&Uacute;","&Ugrave;","&Oacute;","&Ograve;","&Otilde;","&Ocirc;");
		
		$frmValue['intro'] = str_replace($str, $str_replace, $frmValue['intro']);
		$frmValue['content'] = str_replace($str, $str_replace, $frmValue['content']);
	
		$arr = array(
						'name' 	 	 => $frmValue['name'],
						'intro' 	 => $frmValue['intro'],
						'content'    => $frmValue['content'],
						'rank'       => $frmValue['rank']
					);
		
		if ($this->file['name'])
		{
			// Generate image name
			$img_name = rand(9,999999);
			$img_name .= '_'.$this->file['name'];
			$arr['thumb'] = $img_name;	
			@move_uploaded_file( $this->file['tmp'], SI_IMG . $img_name );		
		}
		
		
		$db->do_update("service_intro", $arr, "id = '". $this->siID ."'" );		
		return true;
	}
	
 
}

?>