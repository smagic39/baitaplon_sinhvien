<!--LIST OF MENU THAT IS USED FOR ADMIN PANEL-->

<div class="adm_cap" onClick="showHide('dhtml');">
		<img src="admin/images/red.gif"/>THÔNG TIN CẦN BIẾT
</div>
<div id="dhtml" style="display:<?=($token->folder == 'homeinfo') ? '':'none'?>">
	<div class="adm_lmenu"><a href="?mod=homeInfos" class="style3">Dịch vụ tiêu biểu</a></div>
	<div class="adm_lmenu"><a href="?mod=crmList" class="style3">Các link đến CRM</a></div>
	<div class="adm_lmenu"><a href="?mod=bannerList" class="style3">Banner Top</a></div>
	<div class="adm_lmenu"><a href="?mod=contactList" class="style3">Khách hàng liên hệ</a></div>
</div>


<div class="adm_cap" onClick="showHide('dinfos');">
		<img src="admin/images/red.gif"/>THÔNG TIN THAM KHẢO
</div>
<div id="dinfos" style="display:<?=($token->folder == 'infos') ? '':'none'?>">
	<div class="adm_lmenu"><a href="?mod=infosList" class="style3">Các thông tin tham khảo</a></div>
	<div class="adm_lmenu"><a href="?mod=infosAdd" class="style3">Thêm thông tin tham khảo</a></div>
</div>


<div class="adm_cap" onClick="showHide('dsupport');">
		<img src="admin/images/red.gif"/>HỖ TRỢ
</div>
<div id="dsupport" style="display:<?=($token->folder == 'support') ? '':'none'?>">
	<div class="adm_lmenu"><a href="?mod=supportList" class="style3">Các thông tin hỗ trợ</a></div>
	<div class="adm_lmenu"><a href="?mod=supportAdd" class="style3">Thêm thông tin hỗ trợ</a></div>
</div>


<div class="adm_cap" onClick="showHide('dreg');">
		<img src="admin/images/red.gif"/>ĐĂNG KÍ
</div>
<div id="dreg" style="display:<?=($token->folder == 'reg') ? '':'none'?>">
	<div class="adm_lmenu"><a href="?mod=regList" class="style3">Các hướng dẫn đăng kí</a></div>
	<div class="adm_lmenu"><a href="?mod=regAdd" class="style3">Thêm hướng dẫn đăng kí</a></div>
</div>


<div class="adm_cap" onClick="showHide('service_intro');">
		<img src="admin/images/red.gif"/>GIỚI THIỆU CÁC DỊCH VỤ
</div>
<div id="service_intro" style="display:<?=($token->folder == 'si') ? '':'none'?>">
	<div class="adm_lmenu"><a href="?mod=siList" class="style3">Các thông tin dịch vụ</a></div>
	<div class="adm_lmenu"><a href="?mod=siAdd" class="style3">Thêm thông tin dịch vụ</a></div>
</div>


<div class="adm_cap" onClick="showHide('dservice');">
		<img src="admin/images/red.gif"/>DỊCH VỤ CUNG CẤP
</div>
<div id="dservice" style="display:<?=($token->folder == 'sc') ? '':'none'?>">
	<div class="adm_lmenu"><a href="?mod=stList" class="style3">Các danh mục</a></div>
	<div class="adm_lmenu"><a href="?mod=stAdd" class="style3">Thêm danh mục</a></div>
	<div class="adm_lmenu"><a href="?mod=serviceList" class="style3">Danh sách loại dịch vụ</a></div>
	<div class="adm_lmenu"><a href="?mod=serviceAdd" class="style3">Thêm loại dịch vụ</a></div>
	<div class="adm_lmenu"><a href="?mod=spList" class="style3">Danh sách gói dịch vụ</a></div>
	<div class="adm_lmenu"><a href="?mod=spAdd" class="style3">Thêm gói dịch vụ</a></div>

</div>


<div class="adm_cap" onClick="showHide('dod');">
		<img src="admin/images/red.gif"/>ĐĂNG KÍ DỊCH VỤ
</div>
<div id="dod" style="display:<?=($token->folder == 'od') ? '':'none'?>">
<div class="adm_lmenu"><a href="?mod=orderList" class="style3">Danh sách đăng kí</a></div>	
</div>


<div class="adm_cap" onClick="showHide('dfaq');">
		<img src="admin/images/red.gif"/>HỎI ĐÁP
</div>
<div id="dfaq" style="display:<?=($token->folder == 'faq') ? '':'none'?>">
	<div class="adm_lmenu"><a href="?mod=faqList" class="style3">Các hỏi đáp</a></div>
	<div class="adm_lmenu"><a href="?mod=faqAdd" class="style3">Thêm hỏi đáp</a></div>
</div>



<div class="adm_cap" onClick="showHide('dnews');">
		<img src="admin/images/red.gif"/>TIN TỨC
</div>
<div id="dnews" style="display:<?=($token->folder == 'news') ? '':'none'?>">
	<div class="adm_lmenu"><a href="?mod=newsList" class="style3">Danh sách tin tức</a></div>	
	<div class="adm_lmenu"><a href="?mod=newsAdd" class="style3">Thêm tin tức</a></div>
</div>


<script type="text/javascript">
function showHide(obj)
{
	if(document.getElementById(obj).style.display == 'none')
	{
		document.getElementById(obj).style.display = 'block';			
	}
	else
	{
		document.getElementById(obj).style.display = 'none';	
	}
}
</script>
