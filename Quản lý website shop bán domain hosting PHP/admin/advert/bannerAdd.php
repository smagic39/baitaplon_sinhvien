<?php
/*----------------------
| ADD NEW BANNER
------------------------*/

// Check for security
if (!defined('HCR'))
{
	print '<h1>Incorrect Access!</h1>';
	exit();
}

$cnt = new content;

class content{


	public $title = "Upload banner mới";
	public $text = "";	
	private $process = "";
	
	private $warning = '';
	private $err = '';
	
	private $file = array(
							'name' => '',
							'size' => '',
							'type' => '',
							'tmp'  => '',
						 );
		
		
	function __construct()
	{
		global $str, $sess, $db;
		$this->get_input();
		$check_input = $this->check_input( $this->frmValue );	

		if ($this->process == "addBanner")
		{ 
			if ($check_input)
			{ 
				// UPLOAD BANNER
				// Generate banner name
				$ban_name = rand(9,999);
				$ban_name .= '_'.$this->file['name'];
				@move_uploaded_file($this->file['tmp'], B_IMG . $ban_name);
				// Update xml file for banner show on top
				require_once 'fw.php';				
								
				$this->warning = "<span class='span_ok'>Upload banner thành công !!</span>";
				$this->warning .= "<ol><img src='". ADMIN_IMG ."red.gif' align='absmiddle'/> Nhấp <a href='?mod=bannerList' class='topadd'>vào đây</a> để xem danh sách banner.</ol></ul><br/>";
				
			}
			else
			{
				$this->warning = "<b><u>Lỗi upload</u>:</b>&nbsp;<span class='span_err'><ul>". $this->err ."</ul></span>";
			}
		} 
		
		$this->text = ($this->warning) ? "<div id='warning'>" . $this->warning . "</div>" . $this->show_form($this->frmValue) : $this->show_form($this->frmValue);
	}	
	
	
	/*------------------------------------
	 | SHOW FORM FOR INPUT DATA
	+ ------------------------------------*/
	function show_form($frmValue)
	{
		global $frm, $db, $sess, $time;
		
		
		$text .= $frm->draw_form( "", "", 2, "POST", "frm_addBanner");
		$text .= $frm->draw_hidden("process", "addBanner");		

		$text .= "<table cellspacing='0' cellpadding='4' class='tbl_add'>";
		$text .= "<tr>";
		$text .= "<td>Banner : </td>";
		$text .= "<td>". $frm->draw_file("banner") ."&nbsp;<font color='#0000FF'>1000x277 px</font>&nbsp;(*.jpg &amp; <= 2MB)</td>";
		$text .= "</tr>
				  <tr><td colspan='2' height='6'></td></tr>";
				
		$text .= "<tr>";
		$text .= "<td colspan='2' align=center>";
		$text .= $frm->draw_submit(" Upload ", "");
		$text .= "&nbsp;&nbsp;<input type='reset' value=' Hủy '></td>";
		$text .= "</tr>";
					
		$text .= "</table>";		
		$text .= "</form>";
		
				
		
		return $text;
	}
	
	/*-----------------------------------
	 | GET INPUT DATA
	+------------------------------------*/
	function get_input()
	{
		global $str;
		
	  	$this->process = isset($_POST['process']) ? $str->input($_POST['process']) : "";		
	    if ( isset($_FILES['banner']['name']) )
	    {
		   $this->file['name'] = $_FILES['banner']['name'];
		   $this->file['type'] = $_FILES['banner']['type'];
		   $this->file['size'] = $_FILES['banner']['size'];
		   $this->file['tmp'] = $_FILES['banner']['tmp_name'];
	    }
		
	}
	
	
	/*-----------------------------------------------------
	| CHECK FOR INPUT DATA	
	+------------------------------------------------------*/
	function check_input($frmValue)
	{
		global $frm, $time, $db, $str;
		
		$no_error = true;
		if (!is_uploaded_file($this->file['tmp']))
		{
		 	$no_error = false;
			$this->err .= "<li>Lỗi upload file lên server</li>";
		}
		// Allow jpg, jpeg and gif	 
		if ($this->file['type'] != "image/jpeg")
		{
			$no_error = false;
			$this->err .= "<li>Loại file không hợp lệ(*jpg)</li>";
		}
			 
		if ($this->file['size'] > 2097152)
		{
		 	$no_error = false;
			$this->err .= "<li>Kích thước file quá lớn(<=2MB)</li>";
		 }
		
		 return $no_error;
	}
	

}

?>