<?php
/*------------------------------------
 |	LOG OUT
-------------------------------------*/
// Check for Security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}

$cnt = new content;

class content
 {
	var $text = "";
	var $title = "BAOPHUONGSHOP.COM CONTROL PANEL";
	
	function content()
	 {
		global $sess, $str;		
		$sess->destroy();
       $str->goto_url('admp.php');
	 }
	 
  }
?>