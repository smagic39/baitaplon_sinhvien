<?php
/*---------------------------------------------
| EDIT INFOS ABOUT SPECIFIED LANGUAGE
 ----------------------------------------------*/

// Check for Security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
}

$cnt = new content;

class content
{
	public $title = "Hiệu chỉnh các thiết lập ngôn ngữ";
	public $text = "";
	
	private $process = "";
	private $lang_code = "";
	private $frmValue = array(
								  'name'   => "",
								  'icon'   => "",
								  'code'   => "",
								  'rank'   => "",
								  'active' => "",
							  );
	private $file = array(
							  'name' => "",
							  'type' => "",
							  'size' => "",
							  'tmp'  => "",
					     );
	private $warning = "";
	private $err = "";
		
	function __construct()
	{
		global $sess, $db, $lang, $str, $token;		
		$this->lang_code = isset($_GET['id']) ? $_GET['id'] : "";
		$this->get_input();
		$check_input = $this->check_input($this->frmValue);
		
		if ($this->process == "edit_language")
		{
			if ($check_input)
			{  
				$this->update_field($this->frmValue);
				$str->goto_url(FILE_ADMIN.'?mod=languageList');
			}
			else
			{
				$this->warning = "<b><u>Lỗi nhập liệu</u>:</b>&nbsp;<span class='span_err'>". $this->err ."</span>";
			}
		} 
		
		$this->text = ($this->warning) ? "<div class='warning'>" . $this->warning . "</div>" . $this->show_form($this->frmValue) : $this->show_form($this->frmValue);
	}	
	
	
	/*--------------------------------
	| SHOW FORM
	+ --------------------------------*/
	function show_form($frmValue)
	{
		global $frm, $db, $sess, $time;
		
		$query = $db->simple_select("*" , "language" , "code = '". $this->lang_code ."'");
		$result = $db->query($query);
		$row = $db->fetch_assoc($result);
		$option = "";
		$text = $frm->draw_form( "", "", 2, "POST" );
		$text .= "<table cellspacing='0' cellspadding='0' class='tbl_edit'>";
		$text .= $frm->draw_hidden("process", "edit_language");
		$text .= "<tr>";
		$text .= "<td>Tên ngôn ngữ: </td>";
		$text .= "<td>";
		$text .= $frm->draw_textfield("name", $frmValue['name'] ? $frmValue['name'] : $row['name'], "", "30", "50");
		$text .= "</td></tr>
				  <tr><td colspan='2' height='6'></td></tr>";
		
		$text .= "<tr>";
		$text .= "<td>Icon: </td>";
		$image = $row['icon'] ? "<img src='". DIR_IMG . $row['icon'] ."' border='0' width='30' alt='Icon hiện tại'>" : "<font color='red'>Chưa có icon</font>";
		
		$text .= "<td>". $image ."</td>";
		$text .= "</tr>
				  <tr><td colspan='2' height='6'></td></tr>";
		/*		
		$text .=   "<tr>";
		$text .= "<td>Đổi icon: </td>";
		$text .= "<td>".$frm->draw_file("file")."&nbsp;(*.jpg,*.gif,*.png)</td>";
		$text .=   "</tr>
					<tr><td colspan='2' height='6'></td></tr>";			
		
		$text .= "<tr>";
		$text .= "<td>Code:</td>";
		$text .= "<td>". $frm->draw_textfield( "code" , $row['code'], "", "5", "10", "", "readonly='true'") ."</td>";
		$text .= "</tr>
				  <tr><td colspan='2' height='6'></td></tr>";	
		*/
		
		// Show this if this is not a default language
		if ($row['isdefault'] != 1)			
		{
			$choose = $frmValue['active'] ? $frmValue['active']:$row['active'];
			$text .= "<tr>";
			$text .= "<td>Kích hoạt: </td>";
			$text .= "<td>". $frm->draw_checkbox("active", "1","", ($choose==1 ? "checked" : "")) ."</td>";
			$text .= "</tr>
					  <tr><td colspan='2' height='6'></td></tr>";
		}
		else
		{
			$text .= "<input type='hidden' name='active' value='1'/>";
		}
			
		$text .= "<tr>";
		$text .= "<td colspan='2' align=center>";
		$text .= $frm->draw_submit(" Cập nhật ", "");
		$text .= "&nbsp;&nbsp;<input type='reset' value=' Hủy '></td>";
		
		$text .= "</table>";		
		$text .= "</form>";
		
		return $text;
	}
	
	/*--------------------------------
	| GET INPUT DATA
	+---------------------------------*/
	function get_input()
	{
		global $str;
		
		$this->process = isset($_POST['process']) ? $str->input($_POST['process']) : "";
		
		$this->frmValue['name'] = isset($_POST['name']) ? $str->input($_POST['name']) : "";
		//$this->frmValue['code'] = isset($_POST['code']) ? $str->input($_POST['code']) : "";
		//$this->frmValue['rank'] = isset($_POST['rank']) ? $str->input($_POST['rank']) : 0;
		$this->frmValue['active'] = isset($_POST['active']) ? 1 : 0;
		/*
		if (isset($_FILES['file']['name']))
		{
			$this->file['name'] = $_FILES['file']['name'];
			$this->file['type'] = $_FILES['file']['type'];
			$this->file['size'] = $_FILES['file']['size'];
			$this->file['tmp'] = $_FILES['file']['tmp_name'];
		}
		*/
	}
	
	/*------------------------------------------------------
	| CHECK FOR INPUT DATA
	+-------------------------------------------------------*/
	function check_input($frmValue)
	{
		global $frm;
		$this->err = "";
		$no_error = true;
		
		if (!$frm->check_input($frmValue['name'], 3))
		{
			$no_error = false;
			$this->err .= " Tên ngôn ngữ phải có ít nhất 3 kí tự . ";
		}
		/*
		if ( $this->file['name'] )
		{ if ( ($this->file['type'] != "image/jpeg" ) && ( $this->file['type'] != "image/gif" ) && ( $this->file['type'] != "image/x-png" ) && ( $this->file['type'] != "image/pjpeg" ) && ( $this->file['type'] != "image/png" ) )
		   {
				$no_error = false;
				$this->err .= " Đuôi file ảnh không hợp lệ . ";
		    }
		   if ( file_exists('images/'.$this->file['name'] ) )
		    {
			   $no_error = false;
			   $this->err .= " File ảnh đã tồn tại . ";
		    }
		}
		*/	
		
		return $no_error;
	}
	
	/*------------------------------------------
	| UPDATE DATA TO DB
	+--------------------------------------------*/
	function update_field($frmValue, $img = false)
	{
		global $db, $time;	
		
		// Ghi vao CSDL
		$arr = array(
						'name' => $frmValue['name'],						
						//'code' => $frmValue['code'],
						'active' => intval($frmValue['active']),
					);
		
		/*			
		if($img)
		{
			$arr['icon'] = $this->file['name'];
			move_uploaded_file($this->file['tmp'], "images/".$this->file['name']);
		}
		*/
		$db->do_update("language" , $arr , "code = '". $this->lang_code ."'");		
		return true;
	}
	

}
?>