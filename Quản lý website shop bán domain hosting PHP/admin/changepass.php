<?php
/*-------------------------------------
| PAGE FOR PASSWORD CHANGING
-------------------------------------*/

// Check for Security
if ( !defined('HCR') )
{
	print "<h1>Incorrect Access</h1>";
	exit();
 }

$cnt = new content;

class content
 {
	public $title = "Đổi mật khẩu quản trị";
	public $text = "";	
	
	private $process = "";
	private $frmValue = array(
								'password_old' => "",
								'password' => "",
								'password_confirm' => "",
								
						   	 );
	private $error = array(	'security_code' => false );
	private $warning = "";
	
	
	function __construct()
	 {
		global $str, $sess, $lang, $panel, $token, $dsp;		
		//if (!$sess->member_id) $str->redirect(URL .'admin.php');		
		$this->get_input();
		$check_input = $this->check_input( $this->frmValue );
		
		if ( $this->process == "submit" )
		{
			if ( $check_input )
			{
				$this->changepass( $this->frmValue );
				$this->warning = "<span class='span_ok'>Đổi mật khẩu thành công!</span><br/><br/>";
			
			}
			else
			{
				$this->warning = "<b><u>Lỗi nhập liệu </u></b> : <span class='span_err'>Dữ liệu nhập vào không hợp lệ hoặc(và) mật khẩu cũ không đúng</span><br/><br/>";
	
			}
		}
		
			
			$this->text = ($this->warning) ? "<div id='warning'>" . $this->warning . "</div>" . $this->show_form($this->frmValue, $this->error) : $this->show_form($this->frmValue, $this->error);
		
	}
	
	
	/*--------------------------------------------
	 | SHOW FORMs
	+ --------------------------------------------*/
	function show_form( $frmValue, $error )
	{
		global $frm, $db, $sess, $lang, $crypt, $dsp, $main;		
		$text = $frm->draw_form("", "", 2, "POST");
		$text .=  "<table width='400' border='0' cellspacing='0' cellspadding='0' class='tabl_edit'>";
			$text .= $frm->draw_hidden("process", "submit");

			$text .=  "<tr>";
				$text .=  "<td nowrap='nowrap'><strong>Mật khẩu cũ&nbsp;</strong></td>";
				$text .=  "<td align=left>";
					$text .= $frm->draw_password("password_old");
					$text .= "&nbsp". $lang['contact_require'];
				$text .= "</td>";
			$text .= "</tr>";
			
			$text .= "<tr>";
				$text .= "<td nowrap='nowrap'><strong>Mật khẩu mới&nbsp;</strong></td>";
				$text .= "<td align=left>";
					$text .= $frm->draw_password("password");
					$text .= "&nbsp".$lang['contact_require'];
				$text .= "</td>";
			$text .= "</tr>";
			
			$text .=  "<tr>";
				$text .= "<td nowrap='nowrap'><strong>Nhập lại mật khẩu mới:&nbsp;</strong></td>";
				$text .= "<td align=left>";
					$text .= $frm->draw_password("password_confirm");
					$text .= "&nbsp".$lang['contact_require'];;
				$text .= "</td>";
			$text .= "</tr>";
			
			$text .= "<tr><td colspan='2' height='6'></td></tr>";
			$text .= "<tr>";
				$text .= "<td colspan='2' align=center>";
					$text .= $frm->draw_submit("Đổi mật khẩu", "button");
				$text .= "</td>";
			$text .= "</tr>";
			
		$text .= "</table>";		
		$text .= "</form>";
		
		return $text;
	}
	
	
	/*-------------------------------
	 | GET INPUT DATA
	+--------------------------------*/
	function get_input()
	{
		global $str;
		
		$this->process = isset($_POST['process']) ? $str->input($_POST['process']) : "";
		$this->frmValue['password_old'] = isset($_POST['password_old']) ? $_POST['password_old'] : "";
		$this->frmValue['password'] = isset($_POST['password']) ? $_POST['password'] : "";
		$this->frmValue['password_confirm'] = isset($_POST['password_confirm']) ? $_POST['password_confirm'] : "";
		
	}
	
	
	/*---------------------------------
	 | CHECK FOR INPUT DATA
	+----------------------------------*/
	function check_input( $frmValue )
	{
		global $frm, $main, $sess, $db, $str, $crypt;						
		$no_error = true;		
		if ( !$frm->check_password($frmValue['password_old'], 6, 32) )
		{
			$no_error = false;
		}
		else
		{	
			// Kiem tra password co trung khop
			$query = $db->simple_select("password, login_key", "adm_members", "id = '". $sess->member_id ."' AND active = 1");
			$result = $db->query($query);
			$row = $db->fetch_array($result);
			
			if ( !$row['password'] )
			{
				$no_error = false;
			}
			else
			{
				$password = $crypt->decrypt_password($row['password'], $row['login_key'], 32);
				if ($password != $frmValue['password_old']) $no_error = false;
			}
		}
				
		if ( !$frm->check_password($frmValue['password'], 6, 32) )
		{
			$no_error = false;
		}
		else if ( !$frm->check_password_confirm($frmValue['password'], $frmValue['password_confirm'], 6, 32) )
		{
			$no_error = false;
		}
		
	 return $no_error;
	}
	
	
	/*------------------------------------
	  | UPDATE NEW PASSWORD
	+-------------------------------------*/
	function changepass( $frmValue )
	{
		global $db, $sess, $crypt, $main, $lang, $lg, $time, $str;
		
		// Encrypt password
		$login_key = $crypt->set_key(32);			
		$frmValue['password'] = $crypt->crypt_password($frmValue['password'], $login_key, 32);
		
		// Prepare data
		$arr = array(
						'password' => $frmValue['password'],
						'login_key' => $login_key,
					);
		$db->do_update("adm_members", $arr, "id = ". $sess->member_id);
	}
	
 }

?>
