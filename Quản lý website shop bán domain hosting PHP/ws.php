<?php
///////////////////////////////////////
// SPECIFIC PAGE FOR DOMAIN CHECKING
//////////////////////////////////////
ob_start(); // Start output buffering
				
define("HCR",true); // Check for security
	
require_once "configure.php"; // COnfiguration file
	

/*=========================================
| GET ALL KERNEL CLASS
+==========================================*/
require_once DIR_CLASS."string.php";
$str = new str;
require_once DIR_CLASS."time.php";
$time = new timer;
require_once DIR_CLASS."form.php";
$frm = new form;
require_once DIR_CLASS."display.php";
$dsp = new display;
require_once DIR_CLASS."crypt.php";
$crypt = new crypter;
require_once DIR_CLASS."db_mysql.php";
$db = new database;
require_once DIR_CLASS."session.php";
$sess = new session;
require_once DIR_CLASS."token.php";
$token = new token;
require_once DIR_CLASS."paging.php";
$pagings = new hpaging;
require_once DIR_CLASS."dynamic_form.php";
$dfrm = new dynamic_form;
//require_once DIR_CLASS."paging_information.php";
//$paging_info = new paging_info;
require_once DIR_CLASS."mail.php";
$email = new email;


// Execute token to get content of site
$body = $token->act();
require(DIR_LANG."vn.php");


/*=================================================
| Get file that hold all needed box for site
+==================================================*/
require_once DIR_SOURCE."panel.php";
$panel = new panel;

/*======================================
| Define content for current token
+=======================================*/
require_once DIR_SOURCE.$token->body;	




/*=================================================================
| SOME FUNCTIONS FOR PARSE INFORMATION TO GET WHOIS INFORS
+==================================================================*/

		// Decode html chars
		function ht_unhtmlchars($str)
		{
		return str_replace(array('&lt;', '&gt;', '&quot;', '&amp;', '&#92;', '&#39'), array('<', '>', '"', '&', chr(92), chr(39)), $str);
		}
		// Ping to address where we can find and parse whois information
		function pingDomain($domain)
		{
				$starttime = microtime(true);
				$InternetAddress      = fsockopen ($domain, 80, $errno, $errstr, 10);
				$stoptime  = microtime(true);
				$status    = 0;
			
				if (!$InternetAddress) $status = -1;
				else {
					fclose($InternetAddress);
					$status = ($stoptime - $starttime) * 1000;
					$status = floor($status);
				}
				return $status;
		}
		
		// Request to corresponding server and parse needed information
		function ht_whois($domain,$ext,$showInfo=0)
		{
				$requestURI1 = 'http://www.matbao.vn/whoisXML.aspx?domain='.$domain.'.'.$ext;
				$requestURI2 = 'http://whois.pavietnam.vn/whois.php?domain='.$domain.'.'.$ext;
				$svStatus = array(pingDomain('www.matbao.vn'),pingDomain('whois.pavietnam.vn'));
				$contents = '';
			
				if($svStatus[0] != -1){
					$get_contents = file_get_contents($requestURI1);
					//find <item> tag to get description
					$itemPattern = '/<item>(.*)<\/item>/s';
					if(preg_match($itemPattern,$get_contents,$itemMatches)){
						$availablePattern = '/<avaiable>(.*)<\/avaiable>/';
						if($showInfo){
							$descriptionPattern = '/<description>(.*)<\/description>/s';
							if(preg_match($descriptionPattern,$itemMatches[1],$matches)){
								$contents .= $matches[1];
								echo ht_unhtmlchars($contents);
								exit();
							}	 
						}
						if(preg_match($availablePattern, $itemMatches[1], $matches)) {                 
							if($matches[1] === 'True') {
								echo $domain.$ext.'.';
							}
							else{
								echo $domain.$ext.'';
							}
						}
						
					}
				}elseif($svStatus[1] != -1){
					$get_contents = file_get_contents($requestURI2);
				}else{
					echo 'Server quá tải, vui lòng thử lại!';
				}
		}
			
			
			
		/////////////////////////////////////////////////
		// Using ajax to get needed params and check
		//////////////////////////////////////////////////
		if (isset($_GET['act']) && $_REQUEST['act']=='check')
		{
				$domain = $_REQUEST['domain'];
				$ext = $_REQUEST['ext'];
				ht_whois($domain,$ext).'<br />';
		}
		elseif($_REQUEST['act']=='info')
		{
				$domain = $_REQUEST['domain'];
				$ext = $_REQUEST['ext'];
				ht_whois($domain,$ext,1);
		}
		else
		{
			


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Kiểm tra tên miền - MegaHost.Vn - Hosting share, lưu trữ website, VPS, Dedicated Server, Tên miền .Vn, Tên miền Quốc Tế .Com, .Net, Host sinh viên</title>
<meta name="description" content="Kiểm tra tên miền - Đăng Ký Tên miền - Domain – Hosting - Email Hosting – VPS - Virtual Private Server – Thuê Máy Chủ Riêng - Dedicated Server – Chỗ Đặt Máy Chủ - Colocation - Hàng Đầu Việt Nam"> 
<meta name="keywords" content="Kiểm tra tên miền - Server riêng, Máy chủ Email, damg ky ten mien quoc te, Đăng ký tên miền Quốc tế, dns manage, dns control, lay lai ten mien, quan ly dns, domain forwarding, an thong tin ten mien, can bang tai may chu, cân bằng tải máy chủ, load balancing, mien phi quan ly dich vu dns, chuyen ten mien, transfer domain, tu van lua chon ten mien, quan ly ten mien">

<link href="css/style.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="ws/css/common.css" rel="stylesheet" type="text/css" />
<!--<link href="ws/css/style.css" rel="stylesheet" type="text/css" />-->
<link type="text/css" href="ws/css/ui-lightness/jquery-ui-1.7.2.custom.css" rel="stylesheet" />	
<script type="text/javascript" src="ws/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="ws/js/jquery-ui-1.7.2.custom.min.js"></script>
<script type="text/javascript" src="ws/js/jquery-ui-1.7.2.custom.min.js"></script>
<script type="text/javascript" src="ws/js/common.js"></script>
<script type="text/javascript">
// JS for auto binding - force to click Check Domain button automatically
$(document).ready ( function () {
	if ($('#haction').val ().length > 0)
		$('#check').click ();
});
</script>
</head>

<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div class="nen">
<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="106" align="left" valign="top"><table width="1000" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="625" height="106" align="left" valign="top"><img src="images/megahost_02.gif" width="625" height="106" alt="" /></td>
        <td width="375" height="106" align="left" valign="top"><table width="375" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="31" align="left" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td height="48" align="left" valign="top"><table width="375" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="147" align="left" valign="top"><a href="http://crm.megahost.vn/index.php"><img src="images/megahost_05.gif" alt="" width="147" height="48" border="0" /></a></td>
                <td width="228" align="left" valign="top"><img src="images/megahost_06.gif" width="228" height="48" alt="" /></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="36" align="left" valign="top" background="images/megahost_10.gif"><table width="1000" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="36" align="left" valign="top" class="top-menu"><a href="index.php">TRANG CHỦ</a></td>
        <td width="1" height="36" align="left" valign="top"><img src="images/megahost_09.gif" width="1" height="36" alt="" /></td>
        <td height="36" align="left" valign="top" class="top-menu"><a href="index.php?mod=st&id=3">TÊN MIỀN</a></td>
        <td width="1" height="36" align="left" valign="top"><img src="images/megahost_09.gif" width="1" height="36" alt="" /></td>
        <td height="36" align="left" valign="top" class="top-menu"><a href="index.php?mod=st&id=2">HOSTING</a></td>
        <td width="1" height="36" align="left" valign="top"><img src="images/megahost_09.gif" width="1" height="36" alt="" /></td>
        <td height="36" align="left" valign="top" class="top-menu"><a href="index.php?mod=st&id=1">MÁY CHỦ</a></td>
        <td width="1" height="36" align="left" valign="top"><img src="images/megahost_09.gif" width="1" height="36" alt="" /></td>
        <td height="36" align="left" valign="top" class="top-menu"><a href="index.php?mod=regi">ĐĂNG KÝ</a></td>
        <td width="1" height="36" align="left" valign="top"><img src="images/megahost_09.gif" width="1" height="36" alt="" /></td>
        <td height="36" align="left" valign="top" class="top-menu"><a href="index.php?mod=support">HỖ TRỢ</a></td>
        <td width="1" height="36" align="left" valign="top"><img src="images/megahost_09.gif" width="1" height="36" alt="" /></td>
        <td height="36" align="left" valign="top" class="top-menu"><a href="index.php?mod=rinfo">THÔNG TIN</a></td>
        <td width="1" height="36" align="left" valign="top"><img src="images/megahost_09.gif" width="1" height="36" alt="" /></td>
        <td height="36" align="left" valign="top" class="top-menu"><a href="index.php?mod=contact">LIÊN HỆ</a></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="310" align="left" valign="top">
    <iframe src="libbanner/index.html" width="1000" height="287" frameborder="0" scrolling="no" style="z-index:-100;"></iframe>
    </td>
  </tr>
  <!--Start Main content-->
  
  
  
  
  <?php 
  $crm = $panel->getCRMInfos();
				$crm = $panel->getCRMInfos();
	?>
					<!--MIDDLE-->
					<tr>
					<td align="left" valign="top" height="7"></td>
				  </tr>
					  <tr>
						<td align="left" valign="top"><table width="1000" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td width="277" align="left" valign="top"><table width="277" border="0" cellspacing="0" cellpadding="0">
							  <tr>
								<td align="left" valign="top">
								<!--SUPPORT-->
								<table width="277" border="0" cellspacing="0" cellpadding="0">
								  <tr>
									<td height="38" align="left" valign="top" background="images/detail_12.gif" class="danhmuc">THÔNG TIN HỖ TRỢ</td>
								  </tr>
								  <tr>
									<td height="39" align="left" valign="top" background="images/detail_130.gif" class="main"><table width="241" border="0" cellspacing="0" cellpadding="0">
									  <tr>
										<td width="45" align="left" valign="top"><a href="<?=$crm[0]['blink']?>"><img src="images/megahost_25.gif" alt="" width="43" height="45" border="0" /></a></td>
										<td width="81" align="left" valign="top"><a href="<?=$crm[0]['blink']?>">Liên hệ<br />
										  Kinh doanh</a></td>
										<td width="45" align="left" valign="top"><a href="<?=$crm[1]['blink']?>"><img src="images/megahost_27.gif" alt="" width="39" height="45" border="0" /></a></td>
										<td width="74" align="left" valign="top"><a href="<?=$crm[1]['blink']?>">Hỗ trợ <br />
										  Kỹ Thuật</a></td>
									  </tr>
										<tr>
										<td width="45" height="15" align="left" valign="top">&nbsp;</td>
										<td width="81" height="15" align="left" valign="top">&nbsp;</td>
										<td width="45" height="15" align="left" valign="top">&nbsp;</td>
										<td width="74" height="15" align="left" valign="top">&nbsp;</td>
										</tr>
										<tr>
										<td width="45" align="left" valign="top"><a href="<?=$crm[2]['blink']?>"><img src="images/megahost_29.gif" alt="" width="41" height="45" border="0" /></a></td>
										<td width="81" align="left" valign="top"><a href="<?=$crm[2]['blink']?>">Than phiền<br />
										  Dịch vụ</a></td>
										<td width="45" align="left" valign="top"><a href="<?=$crm[3]['blink']?>"><img src="images/megahost_31.gif" alt="" width="47" height="45" border="0" /></a></td>
										<td width="74" align="left" valign="top"><a href="<?=$crm[3]['blink']?>">Góp ý<br />
										Chất lượng</a></td>
										</tr>
									</table></td>
								  </tr>
								  <tr>
									<td align="left" valign="top"><img src="images/detail_10.gif" width="277" height="11" alt="" /></td>
								  </tr>
								</table>
								<!--SUPPORT-->
								</td>
							  </tr>
							  <tr>
								<td align="left" valign="top">&nbsp;</td>
							  </tr>
							</table></td>
							<td width="723" align="left" valign="top"><table width="723" border="0" cellspacing="0" cellpadding="0">							  	
							  <tr>
								<td align="left" valign="top">
		
		

						
			<div id="resultInfo" style="z-index:100;">
			</div>
			
			<?php
				
				// Define list of checked domain from homepage
				$from_home = array();
				$dm_name = '';
				$carr = array(
								'.vn' => '',
								'.com.vn' => '',
								'.net.vn' => '',
								'.edu.vn' => '',
								'.us' => '',
								'.com' => '',
								'.net' => '',
								'.info' => '',
								'.org' => ''
				              );
				// User chose domain name from homepage and submit
				if (isset($_POST['haction']) && $_POST['haction'] == 'checkdomain')
				{
					$from_home = $_POST['ext'];
					$dm_name = isset($_POST['domainId']) ? 'value="'.$_POST['domainId'].'"' : '';
					if (in_array('.vn', $from_home))
					{
						$carr['.vn'] = 'checked="checked"';
					}
					
					if (in_array('.com.vn', $from_home))
					{
						$carr['.com.vn'] = 'checked="checked"';
					}
					
					if (in_array('.net.vn', $from_home))
					{
						$carr['.net.vn'] = 'checked="checked"';
					}
					
					if (in_array('.edu.vn', $from_home))
					{
						$carr['.edu.vn'] = 'checked="checked"';
					}
					
					if (in_array('.us', $from_home))
					{
						$carr['.us'] = 'checked="checked"';
					}
					
					if (in_array('.com', $from_home))
					{
						$carr['.com'] = 'checked="checked"';
					}
					
					if (in_array('.net', $from_home))
					{
						$carr['.net'] = 'checked="checked"';
					}
					
					if (in_array('.info', $from_home))
					{
						$carr['.info'] = 'checked="checked"';
					}
					
					if (in_array('.org', $from_home))
					{
						$carr['.org'] = 'checked="checked"';
					}
				}
			?>
			
			<!--WHOIS-->								
			<table width="723" border="0" cellspacing="0" cellpadding="0">
			<tr>
			<td height="43" align="left" valign="top" background="images/detail_04.gif" class="danhmuc">TÌM KIẾM TÊN MIỀN</td>
			</tr>
			<tr>
			<td align="center" valign="top" background="images/detail_15.gif">
												
						
			
			<table width="700" border="0" cellspacing="0" cellpadding="0">
			<tr>
			<td align="left" valign="top">
			<table width="445" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="60" align="left" valign="top" class="text-table">
					<img src="images/megahost_18.gif" width="34" height="28" alt="" />
				</td>
				<td width="255" align="left" valign="top" class="text-table">
					<input type="text" class="input-email" name="domainId" id="domainId" <?=$dm_name?>/>
					<input type="hidden" id="haction" name="haction" value="<?=$_POST['haction']?>" />
				</td>
				<td width="130" align="left" valign="top" class="text-table">
					<input type="image" name="check" id="check" src="images/megahost_22.gif" alt="" width="77" height="24" border="0" />
				</td>
			</tr>
			</table>
			</td>
			</tr>
																	 
																	 
																	 
			<!--LIST OF DOMAIN EXTENTION--> 
			<tr>
			<td align="left" valign="top">
				<table width="700" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="vn" value=".vn" <?=$carr['.vn']?>/>&nbsp;.vn
					</td>
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="com.vn" value=".com.vn" <?=$carr['.com.vn']?>/>&nbsp;.com.vn
					</td>
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="net.vn" value=".net.vn" <?=$carr['.net.vn']?>/>&nbsp;.net.vn
					</td>
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="org.vn" value=".org.vn"/>&nbsp;.org.vn
					</td>
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="info.vn" value=".info.vn"/>&nbsp;.info.vn
					</td>
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="gov.vn" value=".gov.vn"/>&nbsp;.gov.vn
					</td>
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="edu.vn" value=".edu.vn" <?=$carr['.edu.vn']?>/>&nbsp;.edu.vn
					</td>
					<td width="60" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="com" value=".com" <?=$carr['.com']?>/>&nbsp;.com
					</td>       
				</tr> 
															
				<tr>		  
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="net" value=".net" <?=$carr['.net']?>/>&nbsp;.net
					</td>
					<td width="85" align="left" valign="top" class="text-table1">
					<input type="checkbox" name="ext" id="org" value=".org" <?=$carr['.org']?>/>&nbsp;.org
					</td>
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="info" value=".info" <?=$carr['.info']?>/>&nbsp;.info
					</td>
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="us" value=".us" <?=$carr['.us']?>/>&nbsp;.us
					</td>
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="biz" value=".biz"/>&nbsp;.biz
					</td>
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="cc" value=".cc"/>&nbsp;.cc
					</td>
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="asia" value=".asia"/>&nbsp;.asia
					</td>       
					<td width="60" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="eu" value=".eu"/>&nbsp;.eu
					</td>
				</tr> 
															
				<tr>		  
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="me" value=".me"/>&nbsp;.me
					</td>
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="tel" value=".tel"/>&nbsp;.tel
					</td>
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="ws" value=".ws"/>&nbsp;.ws
					</td>
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="name" value=".name"/>&nbsp;.name
					</td>
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="tv" value=".tv"/>&nbsp;.tv
					</td>
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="mobi" value=".mobi"/>&nbsp;.mobi
					</td>
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="bz" value=".bz"/>&nbsp;.bz
					</td>       
					<td width="60" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="mn" value=".mn"/>&nbsp;.mn
					</td>
				</tr>
															
				<tr>		  
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="in" value=".in"/>&nbsp;.in
					</td>
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="co.uk" value=".co.uk"/>&nbsp;.co.uk
					</td>
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="biz.vn" value=".biz.vn"/>&nbsp;.biz.vn
					</td>
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="name.vn" value=".name.vn"/>&nbsp;.name.vn
					</td>
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="pro.vn" value=".pro.vn"/>&nbsp;.pro.vn
					</td>
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="health.vn" value=".health.vn"/>&nbsp;.health.vn
					</td>
					<td width="85" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="ext" id="co" value=".co"/>&nbsp;.co
					</td>       
					<td width="60" align="left" valign="top" class="text-table1">
						<input type="checkbox" name="chkall" id="chkall" value="all"/>&nbsp;Tất cả
					</td>
				</tr>  
			</table>
			</td>
			</tr>
			<!--LIST OF DOMAIN EXTENTION--> 
													
													
			<tr><td height="30"></td></tr>
			
			<!--WHOIS RESULT SECTION-->
			<tr>
			<td align="center" valign="top">
			
			<table width="680" cellspacing="0" cellpadding="4" style="border:#999999 solid 1px;">
				<tr class="wr_trc">
					<td width="290" align="center" valign="top" class="wr_tdc">
						Tên miền
					</td>
					<td width="220" align="center" class="wr_tdc">
						Kết quả
					</td>
					<td width="150" align="center" class="wr_tdc" style="border:none">
						Xem thông tin
					</td>
				</tr>				
				<tr>
				<td colspan="3" align="left" valign="top" style="padding-left:5px;">
				<div id="rowResult"></div>
				</td>
			</table>
			</td>
			</tr>
			<!--RESULT-->
													
													
													
			</table>

												
												
			<br/><br/><br/><br/><br/>						
			</td>
			</tr>
			<tr>
			<td align="left" valign="top"><img src="images/detail_16.gif" width="723" height="12" alt="" /></td>
			</tr>
			</table>								
			<!--WHOIS-->
				
			
			
					
  
        </td>
							  </tr>
							</table></td>
						  </tr>
						</table></td>
					</tr>
					<!--MIDDLE-->
  
  
  
  
  
  
  
  
  
  
  <!--End Main content-->
  <tr>
    <td height="7" align="left" valign="top"></td>
  </tr>
    <tr>
    <td height="72" align="left" valign="top" class="icon"><img src="images/icon-control.jpg" width="850" height="29" /></td>
  </tr>
  <tr>
    <td height="109" align="left" valign="top"><table width="1000" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="109" align="left" valign="top" class="copyright"><br />          <strong>Công Ty TNHH Phần Mềm Hoàn Mỹ</strong> <br />
          Địa Chỉ: 98/6  Thích Quảng Đức, P.5, Q.Phú Nhuận, HCM
          <br />
          Copyright © 2008 -2010 megahost.vn </td>
        <td height="109" align="left" valign="top" class="copyright"><strong><br />
          Phòng Kinh Doanh </strong><br />
          Email: <a href="mailto:duc@megahost.vn">duc@megahost.vn</a><br />
          Mobile: 0903339009</td>
        <td height="109" align="left" valign="top" class="copyright"><br />
          <strong>Phòng Kỹ Thuật </strong><br />
          Email:<a href="mailto:contact@megahost.vn"> contact@megahost.vn</a><br />
          Mobile: 0985661672</td>
          <td height="109" align="left" valign="top" class="copyright1"><br />
                       </td>
      </tr>
    </table></td>
  </tr>
</table>
</div>
</body>
</html>


<?php
}
$db->close(); // Close connect to db
ob_end_flush(); // clean output buffering

?>
