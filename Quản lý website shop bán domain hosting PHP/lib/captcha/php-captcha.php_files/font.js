var fontSizer = {
	min: 8,
	max: 18,
	el: Object,
	size: 11,
	init: function() {
		var fs = document.createElement('div');
		fs.innerHTML='<p>Select your font size<br /><span style="margin-left:10px;"><a href="javascript:fontSizer.decrease();"><img src="/images/font-dec.gif" alt="Decrease Font Size" style="border:0px;" /></a><a href="javascript:fontSizer.increase();"><img src="/images/font-inc.gif" alt="Increase Font Size" style="border:0px;" /></a></span></p>';
		$('left').appendChild(fs);
		this.el = this.collectionToArray($('right').getElementsByTagName("p"));
		this.el = this.el.concat(this.collectionToArray($('right').getElementsByTagName("li")));
		this.el = this.el.concat(this.collectionToArray($('right').getElementsByTagName("td")));
	},
	collectionToArray: function(col) {
		a = new Array();
		for (i = 0; i < col.length; i++)
			a[a.length] = col[i];
		return a;
	},
	increase: function() {
		if(this.size!=this.max) {
			this.size++;
		}
		for(i=0;i<this.el.length;i++) {
			this.el[i].style.fontSize = this.size+"px"
		}
	},
	decrease: function() {
		if(this.size!=this.min) {
			this.size--;
		}
		for(i=0;i<this.el.length;i++) {
			this.el[i].style.fontSize = this.size+"px"
		}	
	}
};
addEvent(window,'load',function() { fontSizer.init(); });