addEvent(window,'load',function() {
	var b = document.body;
	var c = document.getElementById('container');
	var d = document.createElement('DIV');
	d.id = 'bookmarks';
	d.appendChild(document.createElement('DIV'));
	b.insertBefore(d,c);

	var title = encodeURIComponent(document.title);
	var url = encodeURIComponent(window.location);

	var link = Array()
	link['digg'] = 'http://digg.com/submit?phase=2&url='+url+'&title='+title;
	link['slashdot'] = 'http://slashdot.org/bookmark.pl?url='+url+'&title='+title;
	link['reddit'] = 'http://reddit.com/submit?url='+url+'&title='+title;
	link['netscape'] = 'http://www.netscape.com/submit/?U='+url+'&T='+title;
	link['furl'] = 'http://furl.net/storeIt.jsp?u='+url+'&t='+title;
	link['delicious'] = 'http://del.icio.us/post?v=2&url='+url+'&title='+title;
	link['stumbleupon'] = 'http://www.stumbleupon.com/submit?url='+url+'&title='+title;
	link['technorati'] = 'http://www.technorati.com/faves?add='+url;
	link['squidoo'] = 'http://www.squidoo.com/lensmaster/bookmark?'+url;
	link['swik'] = 'http://stories.swik.net/?submitUrl&url='+url;
	link['yahoomyweb'] = 'http://myweb2.search.yahoo.com/myresults/bookmarklet?u='+url+'&t='+title;
	link['googlebookmarks'] = 'http://www.google.com/bookmarks/mark?op=edit&bkmk='+url+'&title='+title;
	link['windowslive'] = 'https://favorites.live.com/quickadd.aspx?url='+url+'&title='+title;
	link['rss'] = 'http://www.white-hat-web-design.co.uk/feed.rss';

	for( service in link ) {
		var b = document.getElementById('bookmarks').firstChild;
		var a = document.createElement('A');
		a.href = link[service];
		//a.target = '_blank';
		addEvent(a,'click',function(event) { 
			var t = (event.target) ? event.target : event.srcElement;
			window.location = '/recordclick.php?link='+escape(t.parentNode);
		});
		var i = document.createElement('IMG');
		i.src = '/images/'+service+'.gif';
		i.alt = service;
		i.title = service;
		a.appendChild(i);
		b.appendChild(a);
	}
});