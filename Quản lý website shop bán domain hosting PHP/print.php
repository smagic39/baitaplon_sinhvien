<?php
/*====================================================
| HOANGCN02 FRAMEWORK - FRAMEWORK BASE ON OOP
| DEVELOPED BY HOANGCN02 
+=====================================================*/


ob_start(); // Start output buffering
				
define("HCR",true); // Check for security
	
require("configure.php"); // COnfiguration file
	
/*=========================================
| GET ALL KERNEL CLASS
+==========================================*/
require(DIR_CLASS."string.php");
$str = new str;
require(DIR_CLASS."time.php");
$time = new timer;
require(DIR_CLASS."form.php");
$frm = new form;
require(DIR_CLASS."display.php");
$dsp = new display;
require(DIR_CLASS."db_mysql.php");
$db = new database;
require(DIR_CLASS."session.php");
$sess = new session;
require(DIR_CLASS."token.php");
$token = new token;

require(DIR_CLASS."dynamic_form.php");
$dfrm = new dynamic_form;
require(DIR_CLASS."mail.php");
$email = new email;


/*----------------------------------------------------------------
* A optimize regarding this site : When user click to change language
* We must keep the position of user on site. So we must change session
of language by a GET param named lang. Not use token class as usual
------------------------------------------------------------------*/
if ($_GET['lang'] != '')
{   
	// Check if lang param is in language list in DB
	$langcode = array('vn','en');
	// Check if lang param is in list of possible language
	if (in_array($_GET['lang'], $langcode))
	{   
		if ($_GET['lang'] != $sess->language)
		{
			$sess->language = $_GET['lang'];
		}

		$urlReturn = $_SERVER['REQUEST_URI'];
		// Remove lang param on url
		$urlReturn = ereg_replace('[&|\?]lang=[vnen]{1,2}','',$urlReturn);
		//echo $urlReturn;
		header("Location:". $urlReturn);
		
	}
	else // In case user trang to put an invalid lang param point to home page
	{
		header("Location:". FILE_INDEX);
	}
}



// Execute token to get content of site
$body = $token->act();

/*================================
| Get language
+=================================*/
require(DIR_LANG."language.php");
$lg = new language($sess->language);
require(DIR_LANG.$lg->lang_current.".php");



/*======================================
| Define content for current token
+=======================================*/
require(DIR_SOURCE.$token->body);	

/*===================
| DISPLAY CONTENT
+====================*/
?>

<!--Echo content of corresponding content-->
<?=$cnt->text; ?>


<?php

$db->close(); // End connect to db
ob_end_flush(); // Clean the output buffering

?>
