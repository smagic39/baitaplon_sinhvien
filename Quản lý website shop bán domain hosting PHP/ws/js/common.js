/*
 * DOHOAVN.INFO
 * http://dohoavn.info/
 *
 * Copyright (c) 2009 DOHOAVN.INFO
 * Powered by HT™ ( YM: phan_huutam )
 */
function template(num,dm,ex){
	if(num==2){
		//return '<div id="'+replacedot((dm+ex))+'"><label>Domain <strong>'+dm+'.'+ex+'</strong> đang được kiểm tra...<img src="ws/img/loading2.gif" /></label></div>';
		return '<div id="'+replacedot((dm+ex))+'" class="wrd_holder"><div class="wrd_domain">'+dm+'.'+ex+'</div><div class="wrd_result">đang được kiểm tra...<img src="ws/img/loading2.gif" /></div><div class="wrd_reg"></div></div><div class="wrd_clear"></div>';
	}
	if(num==3){
		//return '<label id="registered"><span>'+dm+'.'+ex+'</span> | <img src="ws/img/notavailable.gif" /> | <a href="javascript:ht_whois(\''+dm+'\',\''+ex+'\')">đã được đăng kí</a></label>';
		return '<div class="wrd_holder"><div class="wrd_domain">'+dm+'.'+ex+'</div><div class="wrd_result"><img src="ws/img/notavailable.gif" /> đã được đăng kí</div><div class="wrd_reg">&nbsp;</div></div><div class="wrd_clear"></div>';
	}
	if(num==4){
		//return '<label id="available"><strong><font color="green">'+dm+'.'+ex+'</font></strong> | <img src="ws/img/OK.gif" /> | chưa đăng kí</a></label>';
		return '<div class="wrd_holder"><div class="wrd_domain">'+dm+'.'+ex+'</div><div class="wrd_result"><img src="ws/img/ok.gif" /> chưa đăng kí</a></div><div class="wrd_reg"><a href="index.php?mod=order"><img src="ws/img/dk.gif" border="0" align="absmiddle"/></a></div></div><div class="wrd_clear"></div>';
	}
}
function replacedot(str){
	return str.replace(/[.]/gi,'');
}
$(function(){	
	$('#check').click(function(){
		$('#rowResult').html('');
		var domain = $('#domainId').val();
		var ext = $('input[name=ext]');
		var errlogs = '';
		
		if(domain.length < 2){
			errlogs += '+ Tên domain không hợp lệ \n';			
		}
		if(!validateDomain(domain)){
			errlogs += '+ Tên miền bao gồm các kí tự A-Z, 0-9 và dấu (-) \n';			
		}
		if(domain.indexOf('--') != -1){
			errlogs += '+ Tên miền không thể có kí tự: -- \n';
		}
		if(domain.indexOf('-')==0 || domain.lastIndexOf('-')==domain.length-1){
			errlogs += '+ Tên domain không thể bắt đầu với: - \n';
		}
		if(errlogs != ''){
			alert(errlogs);
			return false;
		}
		else {
			var hasChecked = false;
			ext.each(function(){
				if(this.checked){
					hasChecked=true;
					$(template(2,domain,this.id)).appendTo('#rowResult');
				}
			});
			if(!hasChecked){
				alert("Vui lòng chọn đuôi tên miền.");
				return false;
			}else{
				ext.each(function(){
					if(this.checked){
						$.ajax({
							type:'GET',
							url: 'ws.php',
							data: 'act=check&domain='+domain+'&ext='+this.id,							
							success:function(html){
								html = $.trim(html);
								ext = new Array(html.slice(domain.length,(html.length -1)),html.slice(domain.length,(html.length)))
								if(html){
									if(html.lastIndexOf('.') == (html.length -1)){
										$('#'+replacedot(domain+ext[0])+'').html(template(3,domain,ext[0]));
									}
									else{
										$('#'+replacedot(domain+ext[1])+'').html(template(4,domain,ext[1]));
									}
								}else{
									$('#rowResult').html('Không thể kết nối đến server.');								
								}																
							}
						});
					}
				});
			}
		}
	});			
	$('#chkall').click(function(){
		var checked_status = this.checked;
		$('input[name=ext]').each(function(){
			this.checked = checked_status;
		});
	});
});
function ht_whois(domain,ext){
	$('#resultInfo').dialog('open');
	$('#resultInfo').dialog({	
		width: 600,
		height: 500,
		modal:true
	});
	$('#resultInfo').dialog('option', 'title', 'whois'+' '+domain+'.'+ext);
	$('#resultInfo').html('<div style="text-align:center;margin-top:200px"><img src="ws/img/loading1.gif" /></div>');
	$.ajax({
		type: 'GET',
		url: 'ws.php',
		data: 'act=info&domain='+domain+'&ext='+ext,
		success: function(html){
		 	$('#resultInfo').html(html);
		}
	});
}
function validateDomain(elementValue){    
   var domainPattern = /^[a-zA-Z0-9-]+$/ ;
   return domainPattern.test(elementValue); 
}