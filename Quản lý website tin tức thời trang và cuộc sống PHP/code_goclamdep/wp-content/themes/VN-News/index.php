﻿<?php get_header(); ?>

<div id="contentleft">
	<?php if (get_settings( 'show_feature' )) { ?>
	<div id="my-glider">
		<div class="scroller">
			<div class="content">
				<?php $display_categories = get_settings("feature_cat"); ?>
				<?php $post_cat = get_option('feature_count'); if (($post_cat==0) || ($post_cat>20)) { $post_cat=5; } ?>
				<?php $result = '';  $i=0; ?>
				<?php query_posts('showposts='.$post_cat.'&cat='.$display_categories); ?>
				<?php while (have_posts()) : the_post(); ?>
					<div class="section" id="section-<?php the_ID(); ?>">
						<?php if ( get_settings( 'show_featured_thumb' ) && get_post_meta($post->ID, 'Image', true) ) { $img_url = get_post_meta($post->ID, 'Image', true); ?>
						<div class="featured-post-thumbnail">
							<?php if ( get_settings( 'thumb_resizer' ) ) { ?>
							<a href="<?php the_permalink(); ?>" rel="bookmark"><img width="<?php echo get_settings( 'featured_thumb_width' ); ?>" height="<?php echo get_settings( 'featured_thumb_height' ); ?>" src="<?php bloginfo('template_url'); ?>/scripts/timthumb.php?src=<?php echo str_replace(get_bloginfo('wpurl').'/','',$img_url); ?>&amp;w=<?php echo get_settings( 'featured_thumb_width' ); ?>&amp;h=<?php echo get_settings( 'featured_thumb_height' ); ?>&amp;zc=1&amp;q=100" alt="<?php the_title_attribute(); ?>" /></a>
							<?php } elseif ((strpos($img_url,'wp-content') == 0) && file_exists($img_url)) { ?>
							<a href="<?php the_permalink(); ?>" rel="bookmark"><img width="<?php echo get_settings( 'featured_thumb_width' ); ?>" height="<?php echo get_settings( 'featured_thumb_height' ); ?>" src="<?php echo get_bloginfo('wpurl') . '/' . $img_url; ?>" alt="<?php the_title_attribute(); ?>" /></a>	
							<?php } else { ?>
							<a href="<?php the_permalink(); ?>" rel="bookmark"><img width="<?php echo get_settings( 'featured_thumb_width' ); ?>" height="<?php echo get_settings( 'featured_thumb_height' ); ?>" src="<?php echo $img_url; ?>" alt="<?php the_title_attribute(); ?>" /></a>	
							<?php } ?>
						</div>
						<?php } ?>					
						<div class="feature-entry">
							<h2><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title_attribute(); ?></a></h2>
							<div class="post-info">
								<span class="date"><?php the_time(__('F jS, Y','vn-news')) ?></span> | 
								<span class="author"><?php the_author_posts_link(); ?></span> | 
								<?php if(function_exists('the_views')) { ?>
								<span class="views"><?php the_views(); ?></span> | 
								<?php } ?>
								<span class="comments"><a href="<?php the_permalink() ?>#respond" title="<?php _e('Leave a comment','vn-news'); ?>" ><?php comments_number(__('0 Comments &#187;','vn-news'), __('1 Comment &#187;','vn-news'), __('% Comments &#187;','vn-news')); ?></a></span>
								<?php edit_post_link(__('Edit','vn-news'), '[', ']'); ?>
							</div>
							<div class="content"><?php the_excerpt(); ?></div>
							<?php $i++; ?>
							<?php $result .= '<li><a href="#section-' .$post->ID. '">'.$i.'</a></li>'."\n"; ?>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		</div>

		<div class="controls">
			<ul>
				<?php echo $result; ?>
			</ul>
			<div class="clearfix"></div>
		</div>
	</div>

	<script type="text/javascript" charset="utf-8">
		var my_glider = new Glider('my-glider', {duration:0.5, autoGlide:true, frequency:6});
	</script>


	<div style="clear:both;"></div>
	<?php } ?>
	
	<div id="main-content" style="width:645px;">
		<?php $display_categories = get_settings("home_cat"); ?>
		<?php if (is_array($display_categories)) { ?>
			<?php $post_cat = get_option('home_cat_count'); if ($post_cat==0) { $post_cat=3; } ?>
			<?php 
				$catColor[] = "blue";
				$catColor[] = "green";
				$catColor[] = "cyan";
				$catColor[] = "orange";
				$currentColor = -1;
			?>
			<?php foreach ($display_categories as $category) { ?>
				<?php $currentColor ++;	if( $currentColor >= count( $catColor ) ) { $currentColor = 0; } ?>
				<?php query_posts("showposts=1&cat=$category");	?>
				<?php $cat_details = get_category($category); ?>
				<?php while (have_posts()) : the_post(); ?>
					<div class="content-block">
						<div class="box-title">
							<div class="<?php echo $catColor[ $currentColor ]; ?>">
								<h3><a href="<?php echo get_category_link( $cat_details->cat_ID ); ?>"><?php echo $cat_details->cat_name ?> &raquo;</a></h3>
							</div>
						</div>
						<div class="post-content clearfix" style="padding: 0px;">
						<table style="width:100%;margin: 0px;" valign="top"><tr><td style="width:390px;" valign="top">
							<h3><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title_attribute(); ?></a></h3>
							<div class="post-info">
								<span class="date"><?php the_time(__('F jS, Y','vn-news')) ?></span> | 
								<span class="author"><?php the_author_posts_link(); ?></span> | 
								<?php if(function_exists('the_views')) { ?>
								<span class="views"><?php the_views(); ?></span> | 
								<?php } ?>
								<span class="comments"><a href="<?php the_permalink() ?>#respond" title="<?php _e('Leave a comment','vn-news'); ?>" ><?php comments_number(__('0 Comments &#187;','vn-news'), __('1 Comment &#187;','vn-news'), __('% Comments &#187;','vn-news')); ?></a></span>
								<?php edit_post_link(__('Edit','vn-news'), '[', ']'); ?>
							</div>
							<?php if ( get_settings( 'show_thumb' ) && get_post_meta($post->ID, 'Image', true) ) { $img_url = get_post_meta($post->ID, 'Image', true); ?>
							<div class="post-thumbnail">
								<?php if ( get_settings( 'thumb_resizer' ) ) { ?>
								<a href="<?php the_permalink(); ?>" rel="bookmark"><img width="<?php echo get_settings( 'thumb_width' ); ?>" height="<?php echo get_settings( 'thumb_height' ); ?>" src="<?php bloginfo('template_url'); ?>/scripts/timthumb.php?src=<?php echo str_replace(get_bloginfo('wpurl').'/','',$img_url); ?>&amp;w=<?php echo get_settings( 'thumb_width' ); ?>&amp;h=<?php echo get_settings( 'thumb_height' ); ?>&amp;zc=1&amp;q=100" alt="<?php the_title_attribute(); ?>" /></a>
								<?php } elseif ((strpos($img_url,'wp-content') == 0) && file_exists($img_url)) { ?>
								<a href="<?php the_permalink(); ?>" rel="bookmark"><img width="<?php echo get_settings( 'thumb_width' ); ?>" height="<?php echo get_settings( 'thumb_height' ); ?>" src="<?php echo get_bloginfo('wpurl') . '/' . $img_url; ?>" alt="<?php the_title_attribute(); ?>" /></a>	
								<?php } else { ?>
								<a href="<?php the_permalink(); ?>" rel="bookmark"><img width="<?php echo get_settings( 'thumb_width' ); ?>" height="<?php echo get_settings( 'thumb_height' ); ?>" src="<?php echo $img_url; ?>" alt="<?php the_title_attribute(); ?>" /></a>	
								<?php } ?>
							</div>
							<?php } ?>
							<div class="content clearfix"><?php the_excerpt(); ?></div>
							<?php $recent = new WP_Query('cat='.$category.'&showposts='.$post_cat.'&offset=1'); ?> 
							<?php if ($recent->have_posts()) : ?>
								<div style="clear:both;"></div>
								</td><td style="width: 225px;">
								<div class="post-summary">
																	<ul>
									<?php while($recent->have_posts()) : $recent->the_post();?>
										<li><strong><a href="<?php the_permalink() ?>" title="Chuyển đến bài viết <?php the_title(); ?>" rel="bookmark"><?php the_title() ?></a></strong></li>
									<?php endwhile; ?>
									</ul>
								</div>
								</td></tr></table>
							<?php endif; ?>
						</div>
					</div>
				<?php endwhile; ?>
			<?php } ?>
		<?php } else { ?>
			<div class="content-block">
				<div class="box-title">
					<div class="blue">
						<h3><?php _e('Home Page Error!','vn-news'); ?></h3>
					</div>
				</div>
				<div class="post-content clearfix">
					<p><?php _e('You have not selected which Categories will be shown on Home Page.','vn-news'); ?></p>
					<?php _e('Please select them in WP-Admin -> Appearance -> VN-News Options.','vn-news'); ?>
				</div>
			</div>
		<?php } ?>
	</div>
</div>

<div id="contentright">
	<div id="sidebar1">
		<?php include (TEMPLATEPATH . '/ads/banner300.php'); ?>
		<?php if (get_settings("show_subscribe")) include (TEMPLATEPATH . '/subscribe-form.php'); ?>
		<ul>
		<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar(1) ) : else : ?>
			<?php include (TEMPLATEPATH . '/sidebar-1.php'); ?>
		<?php endif; ?>
		</ul>
	</div>
</div>

<?php get_footer(); ?>



