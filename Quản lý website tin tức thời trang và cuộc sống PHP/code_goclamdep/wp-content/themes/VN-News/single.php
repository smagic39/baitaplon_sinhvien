<?php get_header(); ?>

<div id="contentleft">
	<div id="main-content" style="width:100%;">
		<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
				<div class="content-block">
					<div class="box-title">
						<div class="blue">
							<h3><a href="<?php bloginfo('home'); ?>"><?php _e('Home','vn-news'); ?></a> &raquo; <?php the_category(', ') ?></h3>
						</div>
					</div>
					<div class="post-content clearfix" id="post-<?php the_ID(); ?>">
						<h1><?php the_title_attribute(); ?></h1>
						<div class="post-info">
							<span class="date"><?php the_time(__('F jS, Y','vn-news')) ?></span> | 
							<span class="author"><?php the_author_posts_link(); ?></span> | 
							<?php if(function_exists('the_views')) { ?>
							<span class="views"><?php the_views(); ?></span> | 
							<?php } ?>
							<span class="comments"><a href="<?php the_permalink(); ?>#respond" title="<?php _e('Leave a comment','vn-news'); ?>" ><?php comments_number(__('0 Comments &#187;','vn-news'), __('1 Comment &#187;','vn-news'), __('% Comments &#187;','vn-news')); ?></a></span>
							<?php edit_post_link(__('Edit','vn-news'), '[', ']'); ?>
						</div>
						<div class="entry clearfix">
							<div class="content clearfix"><?php the_content(); ?></div>
							<?php wp_link_pages(array('before' => '<p><strong>'.__('Pages:','vn-news').'</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
							<?php if(function_exists('st_related_posts')) : ?>
							<div id="related_posts">
								<h2><?php _e('Related Posts:','vn-news'); ?></h2>
								<?php st_related_posts('title='); ?>
							</div>
							<?php endif; ?>
						</div>
						<?php the_tags('<div class="tags" style="font-size:12px;padding-bottom:0px;margin-top:10px;">'.__('Tags:','vn-news').' ',' &middot; ','</div>'); ?>
						<?php 	$permalink 	 = urlencode(get_permalink($post->ID));
								$title 		 = urlencode($post->post_title);
								$title 		 = str_replace('+','%20',$title);
						?>
						<div class="sociable">
							<a href="http://digg.com/submit?phase=2&amp;url=<?php echo $permalink; ?>&amp;title=<?php echo $title; ?>" rel="nofollow" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/sociable/digg.png" title="Digg this!" alt="digg" class="sociable-hovers" /></a>
							<a href="http://del.icio.us/post?url=<?php echo $permalink; ?>&amp;title=<?php echo $title; ?>" rel="nofollow" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/sociable/delicious.png" title="Add to del.icio.us!" alt="delicious" class="sociable-hovers" /></a>
							<a href="http://www.stumbleupon.com/submit?url=<?php echo $permalink; ?>&amp;title=<?php echo $title; ?>" rel="nofollow" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/sociable/stumbleupon.png" title="Stumble this!" alt="stumbleupon" class="sociable-hovers" /></a>
							<a href="http://technorati.com/faves?add=<?php echo $permalink; ?>" rel="nofollow" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/sociable/technorati.png" title="Add to Techorati!" alt="technorati" class="sociable-hovers" /></a>
							<a href="http://www.google.com/bookmarks/mark?op=edit&amp;bkmk=<?php echo $permalink; ?>&amp;title=<?php echo $title; ?>"><img src="<?php bloginfo('template_directory'); ?>/images/sociable/googlebookmark.png" alt="Google" title="Google" class="sociable-hovers" /></a>
							<a href="https://favorites.live.com/quickadd.aspx?marklet=1&amp;url=<?php echo $permalink; ?>&amp;title=<?php echo $title; ?>"><img src="<?php bloginfo('template_directory'); ?>/images/sociable/windows.gif" alt="live" title="Live" class="sociable-hovers" /></a>
							<a href="http://www.facebook.com/sharer.php?u=<?php echo $permalink; ?>&amp;t=<?php echo $title; ?>" rel="nofollow" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/sociable/facebook.png" title="Share on Facebook!" alt="facebook" class="sociable-hovers" /></a>
							<a href="http://sphinn.com/submit.php?url=<?php echo $permalink; ?>&amp;title=<?php echo $title; ?>" rel="nofollow" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/sociable/sphinn.png" title="Sphinn" alt="Sphinn" class="sociable-hovers" /></a>
							<a href="http://www.mixx.com/submit?page_url=<?php echo $permalink; ?>&amp;title=<?php echo $title; ?>" rel="nofollow" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/sociable/mixx.png" title="Mixx" alt="Mixx" class="sociable-hovers" /></a>
							<a href="http://www.newsvine.com/_tools/seed&amp;save?u=<?php echo $permalink; ?>&amp;h=<?php echo $title; ?>" rel="nofollow" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/sociable/newsvine.png" title="Seed Newsvine!" alt="newsvine" class="sociable-hovers" /></a>
							<a href="http://reddit.com/submit?url=<?php echo $permalink; ?>&amp;title=<?php echo $title; ?>" rel="nofollow" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/sociable/reddit.png" title="Reddit!" alt="reddit" class="sociable-hovers" /></a>
							<a href="http://myweb2.search.yahoo.com/myresults/bookmarklet?u=<?php echo $permalink; ?>&amp;=<?php echo $title; ?>" rel="nofollow" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/sociable/yahoomyweb.png" title="Add to Yahoo!" alt="yahoomyweb" class="sociable-hovers" /></a>
						</div>
						<?php if(function_exists('the_ratings')) { echo '<div style="float:right;margin-top:10px;">'; the_ratings(); echo '</div>'; } ?>
					</div>
					<div style="border-top:1px solid #ccc;padding:0px;margin:0px;"></div>
					<div class="author-info">
						<span class="author-avatar"><?php echo get_avatar(get_the_author_email(),64,get_bloginfo('template_directory') . '/images/avatar.gif'); ?></span>
						<h3><?php _e('This post was written by:','vn-news'); ?></h3>                							
						<p><strong><?php the_author_posts_link(); ?></strong> - <?php printf(__('who has written %1$s posts on ',"vn-news"), get_the_author_posts()); ?> <strong><a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a></strong>.</p>
						<p><?php the_author_description(); ?></p>
						<span class="author-email"><a href="mailto:<?php the_author_email(); ?>"><?php _e('Email This Author','vn-news'); ?></a></span> | 
						<span class="feed"><a href="<?php echo get_author_posts_url( get_the_author_id(), ""); ?>feed/" title="<?php printf(__('Subscribe to %1$s','vn-news'), get_the_author_nickname()); ?>"><?php the_author_nickname(); ?>'s RSS</a></span>
					</div>
				</div>
				<?php comments_template(); ?>
			<?php endwhile; else: ?>
				<div class="content-block">
					<div class="box-title">
						<div class="blue">
							<h3><?php _e('404 Page Not Found','vn-news'); ?></h3>
						</div>
					</div>
					<div class="post-content clearfix">
						<p><?php _e("Sorry, but you are looking for something that isn't here.","vn-news"); ?></p>
						<?php _e('You could try going <a href="javascript:history.back()">&laquo; Back</a> or maybe you can find what your looking for below:','vn-news'); ?>
						<?php include (TEMPLATEPATH . "/searchform.php"); ?>
					</div>
				</div>
		<?php endif; ?>
	</div>
</div>

<div id="contentright">
	<div id="sidebar1">
		<?php include (TEMPLATEPATH . '/ads/banner300.php'); ?>
		<?php if (get_settings("show_subscribe")) include (TEMPLATEPATH . '/subscribe-form.php'); ?>
		<ul>
			<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar(1) ) : else : ?>
				<?php include (TEMPLATEPATH . '/sidebar-1.php'); ?>
			<?php endif; ?>
		</ul>
	</div>
</div>

<?php get_footer(); ?>
