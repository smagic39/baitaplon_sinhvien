=== Auto-tags ===
Contributors: Jonathan Foucher
Donate link: http://jfoucher.com/
Tags: automatic,tag,tags,tagging
Requires at least: 2.5
Tested up to: 3.0
Stable tag: 0.4.6

Automatically add relevant tags to new posts.
== Description ==
This plugin uses the Yahoo.com and tagthe.net APIs to find the most relevant keywords from the content of your post, and then adds them as tags. 
**New** for version 0.2: an options page allows to choose how many tags are retrieved from each service
The tag adding is **fully automatic**, so if you're using a plugin like feedwordpress to display RSS feeds on your blog as posts, everything will get done as the feed posts are published. **No user intervention necessary!**

== Installation ==


1. Upload the auto-tag directory to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Go to Options/Auto Tags to configure.
4. That's it. Every new post will now be automatically tagged.

== Frequently Asked Questions ==

= What are the configuration options ? =

You can choose how many tags to retrieve from each service and whether the tags are overwritten or appended to existing tags.

= Wow, is this great or what? =

Yes, but thanks for saying so in the form of a question!

== Screenshots ==

1. This is what the tag area looks like when this plugin did its job. Nice tags!

== ChangeLog ==
V. 0.4.3 Bug fix release
V. 0.4 Added item to configuration page to choose whether the tags are appended to preexisting ones or overwrite them
Numerous bug fixes.

V. 0.2 Added configuration page to choose number of tags retrieved from each service.

