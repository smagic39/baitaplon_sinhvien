<?php
/************************************************************************************
#	Plugin Name: URL Friendly For Vietnamese										#
#	Plugin URI: http://nhanweb.com/2009/07/xu-ly-dau-tieng-viet-trong-url-wordpress/#
#	Description: Make you Permalink friendly: no space, no accents, no dot, no @...	#
#	Author: Nguyen Duy Nhan															#
#	Version: 1.0																	#
#	Author URI: http://nhanweb.com													#
************************************************************************************/

function url_friendly_for_vietnamese($title) {
 	$url_pattern = array('`&(amp;|#)?[a-z0-9]+;`i', '`[^a-z0-9]`i');
	$title = htmlentities($title, ENT_COMPAT, 'utf-8');
	$title = preg_replace( '`&([a-z]+)(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig);`i', "\\1", $title );
	$title = preg_replace('`\[.*\]`U','',$title);
	$title = strtolower(trim($title, '-'));
	
	$title = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $title);  
   	$title = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $title);  
   	$title = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $title);  
   	$title = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $title);  
   	$title = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $title);  
   	$title = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $title);  
  	$title = preg_replace("/(đ)/", 'd', $title);  
  	$title = preg_replace($url_pattern , '-', $title);
	return $title;
}

add_filter('sanitize_title', 'url_friendly_for_vietnamese', 1);

?>