<?php

if (version_compare(PHP_VERSION, '5.0.0.', '<'))
{
	die("RobotVN yêu cầu phiên bản từ PHP 5 trở lên để có thể hoạt động ổn định.");
}

if (!defined('WP_CONTENT_URL')) {
   define('WP_CONTENT_URL', get_option('siteurl') . '/wp-content');
}

	// Modules
	@include_once("modules/vnexpress.php");	
	@include_once("modules/tuoitre.php");	
	
	// Global Variables
	$rbvn_core_version = "1.0";
	$rbvn_debug = 0; // 1 to debug
	$rbvn_dbtable = $wpdb->prefix . "robotvn";
	$rbvnlink_dbtable = $wpdb->prefix . "robotvn_link";	
	
function rbvn_activate() {
   global $wpdb;

   	$rbvn_dbtable = $wpdb->prefix . "robotvn";
	$rbvnlink_dbtable = $wpdb->prefix . "robotvn_link";	
   if ($wpdb->get_var("SHOW TABLES LIKE '" . $rbvn_dbtable . "'") != $rbvn_dbtable) {

      $sql = "CREATE TABLE " . $rbvn_dbtable . " (
        `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		`postspan` VARCHAR(255) NOT NULL DEFAULT '1days',
		`keyword` VARCHAR(255) character set utf8 collate utf8_unicode_ci NOT NULL,
		`vnexcat` VARCHAR(255) NOT NULL DEFAULT '',
		`tuoitrecat` VARCHAR(255) NOT NULL DEFAULT '',
		`category` BIGINT NOT NULL DEFAULT -1 ,
		`num_total` BIGINT NOT NULL DEFAULT 0,
		`num_vnexpress` BIGINT NOT NULL DEFAULT 0,
		`num_tuoitre` BIGINT NOT NULL DEFAULT 0
		)";

      require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
      dbDelta($sql);
   }
   
   if ($wpdb->get_var("SHOW TABLES LIKE '" . $rbvnlink_dbtable . "'") != $rbvnlink_dbtable) {

      $sql = "CREATE TABLE " . $rbvnlink_dbtable . " (
        `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		`fromlink` VARCHAR(255) NOT NULL DEFAULT ''
		)";

      require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
      dbDelta($sql);
   }
	
	// GIA TRI MAC DINH
	//general
	add_option('rbvn_poststatus','published');
	add_option('rbvn_autotag','Yes');
	add_option('rbvn_randomstart1','0');
	add_option('rbvn_resetcount','no');
	add_option('rbvn_badwords','');
	add_option('rbvn_randomstart2','0');
	add_option('rbvn_addlinkback','1');
	add_option('rbvn_linkbackislink','1');
	
	
	wp_schedule_event( time(), 'hourly', "rbvncheckhook");
}


function rbvn_reset_options() {
	update_option('rbvn_poststatus','published');
	update_option('rbvn_autotag','Yes');
	update_option('rbvn_resetcount','no');
	update_option('rbvn_randomstart1','0');
	update_option('rbvn_badwords','');
	update_option('rbvn_randomstart2','0');		
	update_option('rbvn_addlinkback','1');
	update_option('rbvn_linkbackislink','1');
}
?>