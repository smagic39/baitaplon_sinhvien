<?php
/**
 Plugin Name: Robot VN
 Plugin URI: http://www.vnwebmaster.com/
 Version: 1.0
 Description: Tự động đăng bài từ các từ khóa do bạn lựa chọn lên Blog Wordpress. Thực hiện theo ý tưởng và code WPRobot - tác giá Lunatic Studios (phiên bản 1.31).
 Author: Nguyen Duy Nhan.
 Author URI: http://www.vnwebmaster.com/
 License: Vui lòng để lại copyright bên dưới footer.
*/
/*  Copyright 2010 Nguyễn Duy Nhân
*/

require_once("rbvninstall.php");
require_once("general.php");
		
function robotvn_add_menu_admin() {
    add_menu_page('Robot VN', 'Robot VN', 8, __FILE__, 'rbvn_mainpage');
	add_submenu_page(__FILE__, 'Cấu hình', 'Cấu hình', 8, 'rbvn-options', 'rbvn_sub_options');
    add_submenu_page(__FILE__, '', '', 8, 'rbvn-editkw', 'rbvn_editkw');	
}

//Run khi trang khoi dong
function rbvn_mainpage() {
   global $wpdb, $rbvn_dbtable;

if($_POST['rbvn_post']){
	rbvn_add_keyword();
}

if($_POST['rbvn_deleteit']){
	if($_POST["delete"] == "" || $_POST["delete"] == 0 || $_POST["delete"] == null) {
		echo '<div class="updated"><p>Hãy chọn ít nhất một từ khóa!</p></div>';				
	} else {
		rbvn_delete_keyword();
	}	
}

if($_POST['rbvn_runnow']) {
	if($_POST["delete"] == "" || $_POST["delete"] == 0 || $_POST["delete"] == null) {
		echo '<div class="updated"><p>Hãy chọn ít nhất một từ khóa!</p></div>';				
	} else {
		$bulk = $_POST["rbvn_bulk"];
		$delete = $_POST["delete"];
		$array = implode(",", $delete);
		
		if($bulk == "" || $bulk == 0 || $bulk == null) {$bulk = 1;}
		
		for($i=1; $i <= $bulk; $i++) { 
			foreach ($_POST['delete']  as $key => $value) {
				rbvn_post($value);
			}	
		}
	}
}
if($_POST['rbvn_resetred']) {
	rbvn_resetred();
}
?>
<div class="wrap">
<h2>RobotVN</h2>
	<h3>Các keyword đang hoạt động</h3>	
	<form id="rbvn_delete" method="post">
      <?php
		$red = 0;
         $records = $wpdb->get_results("SELECT * FROM " . $rbvn_dbtable . " ORDER BY id ASC");
		 
		 foreach ($records as $record) { ?>
            <div style="padding:5px;margin-top: 5px;border:1px solid #e3e3e3;-moz-border-radius:5px;background-color:#f1f1f1;">
			
            <table width="100%">
                <tr>
                    <td style="border-bottom: 1px dotted #e3e3e3;" class="check-column"><input type="checkbox" value="<?php echo $record->id; ?>" name="delete[]"/></td>
                    <td style="border-bottom: 1px dotted #e3e3e3;"><b><?php echo $record->keyword; ?></b></td>
                    <td style="border-bottom: 1px dotted #e3e3e3;">Mục: <?php echo get_cat_name($record->category); ?></td>
                    <td style="border-bottom: 1px dotted #e3e3e3;">Bài tiếp lúc: <?php echo date('jS \of F Y H:i:s',wp_next_scheduled("rbvnposthook",array($record->id)));?></td>
                </tr>
                <tr>
                    <td><a href="admin.php?page=rbvn-editkw&kw=<?php echo $record->id; ?>" title="Edit Keyword">edit</a></td>
                    <td colspan="3">Đã tạo: 
                    <?php echo $record->num_total . ' (';
                    if($record->num_vnexpress > -1) {echo ' VNExpress.NET:' . $record->num_vnexpress;} elseif($record->num_vnexpress < -1) {$red = 1;$nnum = $record->num_vnexpress * -1 -2;echo ' VNExpress.NET:' . $nnum .'<b style="color:#cc0000;">!</b>';}
                    if($record->num_tuoitre > -1) {echo ' TuoiTre.COM:' . $record->num_tuoitre;} elseif($record->num_tuoitre < -1) {$red = 1;$nnum = $record->num_tuoitre * -1 -2;echo ' TuoiTre:' . $nnum .'<b style="color:#cc0000;">!</b>';}
                    echo ' )'; ?></td>
    
                </tr>
            </table>
		</div>	
		<?php }
        
		 if (!$records) {echo 'Hiện chưa có từ khóa nào!';} else { ?>	
	<p class="submit"><input type="submit" name="rbvn_deleteit" value="Xóa các từ khóa đã chọn"/> <input size="3" style="background:#fff;" name="rbvn_bulk" type="text" id="rbvn_bulk" value="1"/> <input type="submit" name="rbvn_runnow" value="Đăng bài ngay"/> <?php if($red==1){ ?><input type="submit" name="rbvn_resetred" value="Reset All '!'"/><?php } ?></p>
	<?php } ?>
	</form>
	
	<h3>Thêm từ khóa</h3>
	<form method="post" id="rbvn_post_options">
	
		<div style="padding:8px;margin-top: 4px;border:1px solid #e3e3e3;-moz-border-radius:5px;width:650px;background-color:#f1f1f1;">	
		<table>
		
		  <tr>
		    <td width="220px"><b>Thiết lập chính</b></td>
		    <td width="200px"><b>Nguồn bài</b></td>
		    <td><b>Thiết lập mở rộng</b></td>
		  </tr>		
		
		  <tr>
		    <td rowspan="2">
			Từ khóa:<br/><input name="rbvn_keyword" type="text" id="rbvn_keyword" value=""/>
			</td>
		    <td><input name="rbvn_post_vne" type="checkbox" id="rbvn_post_vne" value="yes" <?php if(function_exists('rbvn_vnexpresspost')) {echo "checked";} else {echo "disabled";} ?> /> VNExpress.NET</td>
		    <td rowspan="2">
				 <!-- VNEXPRESS DEPARTMENT -->
                VNExpress Category:<br />
                <select name="rbvn_vnc_vnccat" id="rbvn_vnc_vnccat">
                    <option value="">Toàn bộ các mục</option>
                    <option value="">Xã hội</option>
    
                    <option value="2">Thế giới</option>
                    <option value="3">Kinh doanh</option>
                    <option value="51">Văn hóa</option>
                    <option value="9">Thể thao</option>
                    <option value="47">Pháp luật</option>
                    <option value="110">Đời sống</option>
    
                    <option value="83">Khoa học</option>
                    <option value="89">Vi tính</option>
                    <option value="38">Ô tô xe máy</option>
                    <option value="109">Bạn đọc viết</option>
                    <option value="127">Tâm sự</option>
                    <option value="105">Cười</option>
                </select>
    
                <!--END VNEXPRESS DEPARTMENT-->
			</td>
		  </tr>
		  <tr>
		    <td><input name="rbvn_post_tuoitre" type="checkbox" id="rbvn_post_tuoitre" value="yes" <?php if(function_exists('rbvn_tuoitrepost')) {echo "checked";} else {echo "disabled";} ?> /> TuoiTre</td>
		  </tr>	  
		  <tr>
		    <td>
			Category:<br/>
				<select name="rbvn_category" id="rbvn_category">				
				<?php
				   				   $categories = get_categories('type=post&hide_empty=0');
				   				   foreach($categories as $category)
				   				   {
				   				   echo '<option value="'.$category->cat_ID.'">'.$category->cat_name.'</option>';
				   				   }				
				?>				
				</select>				
			</td>
		    <td></td>
		    <td>
				 <!-- TUOITRE DEPARTMENT -->
                TuoiTre Category:<br />
                <select name="rbvn_tuoitrecat" id="rbvn_tuoitrecat">
                    <option value="-1" <?php if($tuoitrecat == "-1") {echo "selected";} ?>>To&#224;n bộ c&#225;c chuyện mục</option>
                        <option value="3" <?php if($tuoitrecat == "3") {echo "selected";} ?>>Ch&#237;nh trị - X&#227; hội</option>
                        <option value="17" <?php if($tuoitrecat == "17") {echo "selected";} ?>>--M&#244;i trường</option>
                        <option value="302" <?php if($tuoitrecat == "302") {echo "selected";} ?>>----&#221; kiến - Khoa học</option>
                        <option value="220" <?php if($tuoitrecat == "220") {echo "selected";} ?>>----Ph&#225;t minh mới</option>
                        <option value="235" <?php if($tuoitrecat == "235") {echo "selected";} ?>>----Vũ trụ</option>
                        <option value="232" <?php if($tuoitrecat == "232") {echo "selected";} ?>>----Khảo cổ</option>
                
                        <option value="326" <?php if($tuoitrecat == "326") {echo "selected";} ?>>--Nghe v&#224; nghĩ</option>
                        <option value="447" <?php if($tuoitrecat == "447") {echo "selected";} ?>>--Thư viện ph&#225;p luật</option>
                        <option value="293" <?php if($tuoitrecat == "293") {echo "selected";} ?>>--Văn Kiện</option>
                        <option value="347" <?php if($tuoitrecat == "347") {echo "selected";} ?>>--Tiếp thị h&#236;nh ảnh Việt Nam</option>
                        <option value="343" <?php if($tuoitrecat == "343") {echo "selected";} ?>>--Thời cơ v&#224;ng</option>
                
                        <option value="327" <?php if($tuoitrecat == "327") {echo "selected";} ?>>--Mỗi ng&#224;y một con số</option>
                        <option value="239" <?php if($tuoitrecat == "239") {echo "selected";} ?>>--Hỏi &amp; đ&#225;p xuất nhập cảnh</option>
                        <option value="397" <?php if($tuoitrecat == "397") {echo "selected";} ?>>--Vươn ra biển lớn</option>
                        <option value="22" <?php if($tuoitrecat == "22") {echo "selected";} ?>>--Ch&#237;nh Trị</option>
                
                        <option value="289" <?php if($tuoitrecat == "289") {echo "selected";} ?>>--&#221; kiến - CTXH</option>
                        <option value="6" <?php if($tuoitrecat == "6") {echo "selected";} ?>>--Ph&#225;p luật</option>
                
                        <option value="268" <?php if($tuoitrecat == "268") {echo "selected";} ?>>----Luật xứ người</option>
                        <option value="133" <?php if($tuoitrecat == "133") {echo "selected";} ?>>----Vụ &#225;n</option>
                        <option value="266" <?php if($tuoitrecat == "266") {echo "selected";} ?>>----K&#253; Sự Ph&#225;p Đ&#236;nh</option>
                
                        <option value="300" <?php if($tuoitrecat == "300") {echo "selected";} ?>>----&#221; kiến - Ph&#225;p luật</option>
                        <option value="79" <?php if($tuoitrecat == "79") {echo "selected";} ?>>----Tư vấn ph&#225;p luật</option>
                        <option value="12" <?php if($tuoitrecat == "12") {echo "selected";} ?>>--Sống khỏe</option>
                        <option value="455" <?php if($tuoitrecat == "455") {echo "selected";} ?>>----Thắc mắc biết hỏi ai ?</option>
                        <option value="241" <?php if($tuoitrecat == "241") {echo "selected";} ?>>----Giới t&#237;nh</option>
                
                        <option value="231" <?php if($tuoitrecat == "231") {echo "selected";} ?>>----Ph&#242;ng mạch Online</option>
                        <option value="243" <?php if($tuoitrecat == "243") {echo "selected";} ?>>----Mẹo vặt cho cuộc sống</option>
                        <option value="448" <?php if($tuoitrecat == "448") {echo "selected";} ?>>----Cảnh b&#225;o</option>
                        <option value="198" <?php if($tuoitrecat == "198") {echo "selected";} ?>>----Những điều cần biết</option>
                        <option value="197" <?php if($tuoitrecat == "197") {echo "selected";} ?>>----Sức khỏe mẹ v&#224; b&#233;</option>

                
                        <option value="150" <?php if($tuoitrecat == "150") {echo "selected";} ?>>----C&#250;m g&#224;: Th&#244;ng tin cần biết</option>
                        <option value="59" <?php if($tuoitrecat == "59") {echo "selected";} ?>>------Truyền H&#236;nh</option>
                        <option value="301" <?php if($tuoitrecat == "301") {echo "selected";} ?>>----&#221; kiến - Sức khỏe</option>
                        <option value="89" <?php if($tuoitrecat == "89") {echo "selected";} ?>>--Ph&#243;ng sự - K&#253; sự</option>
                
                        <option value="87" <?php if($tuoitrecat == "87") {echo "selected";} ?>>--Thời sự suy nghĩ</option>
                        <option value="88" <?php if($tuoitrecat == "88") {echo "selected";} ?>>--Chuyện thường ng&#224;y</option>
                        <option value="2" <?php if($tuoitrecat == "2") {echo "selected";} ?>>Thế giới</option>
                        <option value="331" <?php if($tuoitrecat == "331") {echo "selected";} ?>>--Việt Nam v&#224; những người bạn</option>
                        <option value="291" <?php if($tuoitrecat == "291") {echo "selected";} ?>>--&#221; kiến - Thế Giới</option>
                
                        <option value="94" <?php if($tuoitrecat == "94") {echo "selected";} ?>>--Quan s&#225;t &amp; B&#236;nh luận</option>
                        <option value="396" <?php if($tuoitrecat == "396") {echo "selected";} ?>>--C&#226;u chuyện cuối tuần</option>
                        <option value="442" <?php if($tuoitrecat == "442") {echo "selected";} ?>>--Thế giới mu&#244;n m&#224;u</option>
                        <option value="20" <?php if($tuoitrecat == "20") {echo "selected";} ?>>--Hồ sơ</option>
                
                        <option value="312" <?php if($tuoitrecat == "312") {echo "selected";} ?>>--Người Việt xa qu&#234;</option>
                        <option value="328" <?php if($tuoitrecat == "328") {echo "selected";} ?>>----Diễn đ&#224;n người xa xứ</option>
                        <option value="340" <?php if($tuoitrecat == "340") {echo "selected";} ?>>----G&#243;c tự cảm</option>
                        <option value="341" <?php if($tuoitrecat == "341") {echo "selected";} ?>>----Du học sinh</option>
                        <option value="342" <?php if($tuoitrecat == "342") {echo "selected";} ?>>------Ứng xử v&#224; Kinh nghiệm</option>
                
                        <option value="314" <?php if($tuoitrecat == "314") {echo "selected";} ?>>----T&#224;i tr&#237; Việt</option>
                        <option value="315" <?php if($tuoitrecat == "315") {echo "selected";} ?>>----Hộp thư Kiều b&#224;o</option>
                        <option value="316" <?php if($tuoitrecat == "316") {echo "selected";} ?>>----Bản sắc Việt</option>
                        <option value="329" <?php if($tuoitrecat == "329") {echo "selected";} ?>>----Thư phương xa</option>
                        <option value="7" <?php if($tuoitrecat == "7") {echo "selected";} ?>>Nhịp sống trẻ</option>
                
                        <option value="348" <?php if($tuoitrecat == "348") {echo "selected";} ?>>--Sống v&#236; cộng đồng</option>
                        <option value="98" <?php if($tuoitrecat == "98") {echo "selected";} ?>>--Cửa sổ t&#226;m hồn</option>
                        <option value="193" <?php if($tuoitrecat == "193") {echo "selected";} ?>>--Khi ta lớn</option>
                        <option value="365" <?php if($tuoitrecat == "365") {echo "selected";} ?>>--L&#224;m đẹp - Thời trang</option>
                        <option value="366" <?php if($tuoitrecat == "366") {echo "selected";} ?>>----Mỹ phẩm</option>
                
                        <option value="367" <?php if($tuoitrecat == "367") {echo "selected";} ?>>----Đẹp c&#249;ng sao</option>
                        <option value="364" <?php if($tuoitrecat == "364") {echo "selected";} ?>>----Tư vấn Khỏe Đẹp</option>
                        <option value="362" <?php if($tuoitrecat == "362") {echo "selected";} ?>>----Th&#244;ng tin</option>
                        <option value="363" <?php if($tuoitrecat == "363") {echo "selected";} ?>>----Bộ sưu tập</option>
                        <option value="96" <?php if($tuoitrecat == "96") {echo "selected";} ?>>--Sinh vi&#234;n</option>
                
                        <option value="298" <?php if($tuoitrecat == "298") {echo "selected";} ?>>--&#221; kiến - NS trẻ</option>
                        <option value="370" <?php if($tuoitrecat == "370") {echo "selected";} ?>>--Tư vấn học đường</option>
                        <option value="194" <?php if($tuoitrecat == "194") {echo "selected";} ?>>--T&#236;nh y&#234;u - Lối sống</option>
                        <option value="257" <?php if($tuoitrecat == "257") {echo "selected";} ?>>----Chia sẻ</option>
                        <option value="92" <?php if($tuoitrecat == "92") {echo "selected";} ?>>----Từ tr&#225;i tim đến tr&#225;i tim</option>
                
                
                        <option value="57" <?php if($tuoitrecat == "57") {echo "selected";} ?>>--Điện ảnh - Truyền h&#236;nh</option>
                        <option value="62" <?php if($tuoitrecat == "62") {echo "selected";} ?>>--S&#226;n khấu</option>
                        <option value="61" <?php if($tuoitrecat == "61") {echo "selected";} ?>>--Văn học</option>
                        <option value="158" <?php if($tuoitrecat == "158") {echo "selected";} ?>>----Thơ V&#224; Tuổi Trẻ</option>
                
                
                        <option value="128" <?php if($tuoitrecat == "128") {echo "selected";} ?>>--Giải tr&#237; h&#244;m nay</option>
                        <option value="349" <?php if($tuoitrecat == "349") {echo "selected";} ?>>----&#212; số</option>
                        <option value="350" <?php if($tuoitrecat == "350") {echo "selected";} ?>>----&#212; chữ</option>
                        <option value="385" <?php if($tuoitrecat == "385") {echo "selected";} ?>>----Chương Tr&#236;nh Truyền H&#236;nh</option>
                
                        <option value="175" <?php if($tuoitrecat == "175") {echo "selected";} ?>>----Nhạc mới</option>
                        <option value="173" <?php if($tuoitrecat == "173") {echo "selected";} ?>>----Phim mới</option>
                        <option value="176" <?php if($tuoitrecat == "176") {echo "selected";} ?>>----S&#226;n chơi</option>
                        <option value="407" <?php if($tuoitrecat == "407") {echo "selected";} ?>>----Qu&#224; tặng bạn đọc</option>
                        <option value="296" <?php if($tuoitrecat == "296") {echo "selected";} ?>>--&#221; kiến - VHGT</option>
                
                        <option value="16" <?php if($tuoitrecat == "16") {echo "selected";} ?>>Nhịp sống số</option>
                        <option value="65" <?php if($tuoitrecat == "65") {echo "selected";} ?>>--Bảo mật</option>
                        <option value="536" <?php if($tuoitrecat == "536") {echo "selected";} ?>>----Virus</option>
                        <option value="537" <?php if($tuoitrecat == "537") {echo "selected";} ?>>----Hacker</option>
                        <option value="184" <?php if($tuoitrecat == "184") {echo "selected";} ?>>--Mobile Games 2005</option>
                        <option value="185" <?php if($tuoitrecat == "185") {echo "selected";} ?>>----Hướng dẫn J2ME</option>
                
                        <option value="186" <?php if($tuoitrecat == "186") {echo "selected";} ?>>----C&#244;ng cụ viết game</option>
                        <option value="74" <?php if($tuoitrecat == "74") {echo "selected";} ?>>--Thế giới phần cứng</option>
                        <option value="337" <?php if($tuoitrecat == "337") {echo "selected";} ?>>----PCs</option>
                        <option value="335" <?php if($tuoitrecat == "335") {echo "selected";} ?>>----ĐTDĐ &amp; PDA</option>
                        <option value="66" <?php if($tuoitrecat == "66") {echo "selected";} ?>>--Thế giới games</option>
                
                        <option value="444" <?php if($tuoitrecat == "444") {echo "selected";} ?>>----Game online</option>
                        <option value="544" <?php if($tuoitrecat == "544") {echo "selected";} ?>>----Trailer</option>
                        <option value="545" <?php if($tuoitrecat == "545") {echo "selected";} ?>>----Wallpaper</option>
                        <option value="67" <?php if($tuoitrecat == "67") {echo "selected";} ?>>--Thủ thuật</option>
                        <option value="533" <?php if($tuoitrecat == "533") {echo "selected";} ?>>----Phần mềm</option>
                        <option value="534" <?php if($tuoitrecat == "534") {echo "selected";} ?>>----M&#225;y t&#237;nh</option>
                
                        <option value="535" <?php if($tuoitrecat == "535") {echo "selected";} ?>>----Di động</option>
                        <option value="297" <?php if($tuoitrecat == "297") {echo "selected";} ?>>--Nhịp cầu số h&#243;a</option>
                        <option value="540" <?php if($tuoitrecat == "540") {echo "selected";} ?>>----NSS-Tư vấn</option>
                        <option value="541" <?php if($tuoitrecat == "541") {echo "selected";} ?>>----NSS-Hỏi đ&#225;p</option>
                        <option value="542" <?php if($tuoitrecat == "542") {echo "selected";} ?>>----NSS-Thường thức</option>
                
                        <option value="431" <?php if($tuoitrecat == "431") {echo "selected";} ?>>--Tư vấn ti&#234;u d&#249;ng</option>
                        <option value="529" <?php if($tuoitrecat == "529") {echo "selected";} ?>>--Viễn Th&#244;ng</option>
                        <option value="430" <?php if($tuoitrecat == "430") {echo "selected";} ?>>----Điện thoại</option>
                        <option value="551" <?php if($tuoitrecat == "551") {echo "selected";} ?>>------Smartphone</option>
                        <option value="552" <?php if($tuoitrecat == "552") {echo "selected";} ?>>------PDA</option>
                
                        <option value="553" <?php if($tuoitrecat == "553") {echo "selected";} ?>>------Phụ kiện</option>
                        <option value="530" <?php if($tuoitrecat == "530") {echo "selected";} ?>>--Thiết bị số</option>
                        <option value="531" <?php if($tuoitrecat == "531") {echo "selected";} ?>>----Phần cứng</option>
                        <option value="336" <?php if($tuoitrecat == "336") {echo "selected";} ?>>----Thiết bị giải tr&#237;</option>
                        <option value="554" <?php if($tuoitrecat == "554") {echo "selected";} ?>>----PC</option>
                        <option value="555" <?php if($tuoitrecat == "555") {echo "selected";} ?>>----Camera</option>
                
                
                        <option value="421" <?php if($tuoitrecat == "421") {echo "selected";} ?>>--Chuẩn bị v&#224;o đời</option>
                        <option value="420" <?php if($tuoitrecat == "420") {echo "selected";} ?>>--Trao đổi</option>
                        <option value="419" <?php if($tuoitrecat == "419") {echo "selected";} ?>>--Văn</option>
                        <option value="415" <?php if($tuoitrecat == "415") {echo "selected";} ?>>--Thơ</option>
                        <option value="119" <?php if($tuoitrecat == "119") {echo "selected";} ?>>Tuổi Trẻ Cuối tuần</option>
                        <option value="277" <?php if($tuoitrecat == "277") {echo "selected";} ?>>--Truyện ngắn</option>
                
                        <option value="123" <?php if($tuoitrecat == "123") {echo "selected";} ?>>--Chuy&#234;n đề</option>
                        <option value="122" <?php if($tuoitrecat == "122") {echo "selected";} ?>>--Khoa Học- Kỹ thuật</option>
                        <option value="121" <?php if($tuoitrecat == "121") {echo "selected";} ?>>--Mỗi tuần một từ</option>
                        <option value="120" <?php if($tuoitrecat == "120") {echo "selected";} ?>>--Người đương thời</option>
                        <option value="126" <?php if($tuoitrecat == "126") {echo "selected";} ?>>--Ph&#243;ng sự &amp; Hồ sơ</option>
                
                        <option value="125" <?php if($tuoitrecat == "125") {echo "selected";} ?>>--Thể Thao Cuối Tuần</option>
                        <option value="127" <?php if($tuoitrecat == "127") {echo "selected";} ?>>--Thư gi&#227;n</option>
                        <option value="172" <?php if($tuoitrecat == "172") {echo "selected";} ?>>--Thế giới s&#225;ch</option>
                        <option value="103" <?php if($tuoitrecat == "103") {echo "selected";} ?>>Tuổi Trẻ Cười</option>
                        <option value="372" <?php if($tuoitrecat == "372") {echo "selected";} ?>>--Ch&#237;nh trị - X&#227; hội cười</option>
                        <option value="373" <?php if($tuoitrecat == "373") {echo "selected";} ?>>--Chuy&#234;n đề cười</option>
                
                        <option value="374" <?php if($tuoitrecat == "374") {echo "selected";} ?>>--Văn nghệ cười</option>
                        <option value="375" <?php if($tuoitrecat == "375") {echo "selected";} ?>>--Thể thao cười</option>
                        <option value="376" <?php if($tuoitrecat == "376") {echo "selected";} ?>>--Vui... vui...</option>
                        <option value="377" <?php if($tuoitrecat == "377") {echo "selected";} ?>>--Thơ ch&#226;m biếm</option>
                        <option value="378" <?php if($tuoitrecat == "378") {echo "selected";} ?>>--Bạn đọc cười</option>
                        <option value="380" <?php if($tuoitrecat == "380") {echo "selected";} ?>>--Qu&#225;n mắc cỡ</option>
                
                        <option value="115" <?php if($tuoitrecat == "115") {echo "selected";} ?>>--Đời Cười</option>
                        <option value="117" <?php if($tuoitrecat == "117") {echo "selected";} ?>>--Thế Giới Cười</option>
                        <option value="116" <?php if($tuoitrecat == "116") {echo "selected";} ?>>--Tranh h&#224;i hước</option>
                        <option value="100" <?php if($tuoitrecat == "100") {echo "selected";} ?>>Du Lịch</option>
                        <option value="303" <?php if($tuoitrecat == "303") {echo "selected";} ?>>--&#221; kiến - Du lịch</option>
                
                        <option value="481" <?php if($tuoitrecat == "481") {echo "selected";} ?>>--Kh&#225;m ph&#225;</option>
                        <option value="333" <?php if($tuoitrecat == "333") {echo "selected";} ?>>--Những miền đất lạ</option>
                        <option value="217" <?php if($tuoitrecat == "217") {echo "selected";} ?>>--Qu&#225;n cuối tuần</option>
                        <option value="218" <?php if($tuoitrecat == "218") {echo "selected";} ?>>--Ẩm thực</option>
                        <option value="319" <?php if($tuoitrecat == "319") {echo "selected";} ?>>--Du lịch Việt Nam</option>
                
                        <option value="219" <?php if($tuoitrecat == "219") {echo "selected";} ?>>--Chuyến đi của bạn</option>
                        <option value="480" <?php if($tuoitrecat == "480") {echo "selected";} ?>>--Văn h&#243;a</option>
                        <option value="204" <?php if($tuoitrecat == "204") {echo "selected";} ?>>Địa ốc</option>
                        <option value="451" <?php if($tuoitrecat == "451") {echo "selected";} ?>>--Nh&#224; đất</option>
                        <option value="452" <?php if($tuoitrecat == "452") {echo "selected";} ?>>--Thế giới kiến tr&#250;c</option>
                
                        <option value="450" <?php if($tuoitrecat == "450") {echo "selected";} ?>>--Đ&#244; thị</option>
                        <option value="560" <?php if($tuoitrecat == "560") {echo "selected";} ?>>TEEN</option>
                        <option value="561" <?php if($tuoitrecat == "561") {echo "selected";} ?>>--TEEN-Thế giới Teen</option>
                        <option value="569" <?php if($tuoitrecat == "569") {echo "selected";} ?>>----TEEN-Nhịp sống</option>
                        <option value="571" <?php if($tuoitrecat == "571") {echo "selected";} ?>>----TEEN-G&#243;c s&#226;n trường</option>
                
                        <option value="572" <?php if($tuoitrecat == "572") {echo "selected";} ?>>----TEEN-T&#244;i v&#224;o đời</option>
                        <option value="573" <?php if($tuoitrecat == "573") {echo "selected";} ?>>----TEEN-Bốn phương</option>
                        <option value="562" <?php if($tuoitrecat == "562") {echo "selected";} ?>>--TEEN-Văn học</option>
                        <option value="574" <?php if($tuoitrecat == "574") {echo "selected";} ?>>----TEEN-S&#225;ng t&#225;c</option>
                        <option value="575" <?php if($tuoitrecat == "575") {echo "selected";} ?>>----TEEN-Truyện ngắn</option>
                
                        <option value="576" <?php if($tuoitrecat == "576") {echo "selected";} ?>>----TEEN-Thơ</option>
                        <option value="577" <?php if($tuoitrecat == "577") {echo "selected";} ?>>----TEEN-S&#225;ch ơi! mở ra</option>
                        <option value="563" <?php if($tuoitrecat == "563") {echo "selected";} ?>>--TEEN-Sống Hi-tech</option>
                        <option value="578" <?php if($tuoitrecat == "578") {echo "selected";} ?>>----TEEN-S&#224;nh điệu</option>
                        <option value="579" <?php if($tuoitrecat == "579") {echo "selected";} ?>>----TEEN-Game</option>
                
                        <option value="580" <?php if($tuoitrecat == "580") {echo "selected";} ?>>----TEEN-Thế giới số</option>
                        <option value="564" <?php if($tuoitrecat == "564") {echo "selected";} ?>>--TEEN-Khỏe-Điệu</option>
                        <option value="581" <?php if($tuoitrecat == "581") {echo "selected";} ?>>----TEEN-Chuyện ch&#250;ng m&#236;nh</option>
                        <option value="582" <?php if($tuoitrecat == "582") {echo "selected";} ?>>----TEEN-L&#224;m đẹp</option>
                        <option value="583" <?php if($tuoitrecat == "583") {echo "selected";} ?>>----TEEN-Thời trang</option>
                
                        <option value="584" <?php if($tuoitrecat == "584") {echo "selected";} ?>>----TEEN-Khỏe hơn</option>
                        <option value="565" <?php if($tuoitrecat == "565") {echo "selected";} ?>>--TEEN-Y&#234;u</option>
                        <option value="585" <?php if($tuoitrecat == "585") {echo "selected";} ?>>----TEEN-Thư t&#236;nh trong cặp</option>
                        <option value="586" <?php if($tuoitrecat == "586") {echo "selected";} ?>>----TEEN-Gửi gi&#243;</option>
                        <option value="587" <?php if($tuoitrecat == "587") {echo "selected";} ?>>----TEEN-Vườn y&#234;u</option>
                
                        <option value="588" <?php if($tuoitrecat == "588") {echo "selected";} ?>>----TEEN-Trắc nghiệm</option>
                        <option value="566" <?php if($tuoitrecat == "566") {echo "selected";} ?>>--TEEN-Giải tr&#237;</option>
                        <option value="589" <?php if($tuoitrecat == "589") {echo "selected";} ?>>----TEEN-Sao</option>
                        <option value="590" <?php if($tuoitrecat == "590") {echo "selected";} ?>>----TEEN-&#194;m nhạc</option>
                        <option value="591" <?php if($tuoitrecat == "591") {echo "selected";} ?>>----TEEN-Phim</option>
                        <option value="592" <?php if($tuoitrecat == "592") {echo "selected";} ?>>----TEEN-Giọng ca v&#224;ng</option>
                
                        <option value="593" <?php if($tuoitrecat == "593") {echo "selected";} ?>>----TEEN-Điểm hẹn</option>
                        <option value="567" <?php if($tuoitrecat == "567") {echo "selected";} ?>>--TEEN-N&#243;i với Teen</option>
                        <option value="568" <?php if($tuoitrecat == "568") {echo "selected";} ?>>--TEEN-G&#243;c của Teen</option>
                
                        <option value="308" <?php if($tuoitrecat == "308") {echo "selected";} ?>>Đọc g&#236; h&#244;m nay</option>
                        <option value="318" <?php if($tuoitrecat == "318") {echo "selected";} ?>>T&#226;m điểm Kiều b&#224;o</option>
                        <option value="411" <?php if($tuoitrecat == "411") {echo "selected";} ?>>T&#236;m hiểu tư tưởng, đạo đức Hồ Ch&#237; Minh</option>
                        <option value="177" <?php if($tuoitrecat == "177") {echo "selected";} ?>>Kham Pha Cuoc Song</option>
                
                        <option value="441" <?php if($tuoitrecat == "441") {echo "selected";} ?>>Gi&#225;o dục</option>
                        <option value="445" <?php if($tuoitrecat == "445") {echo "selected";} ?>>Thể thao</option>
                        <option value="503" <?php if($tuoitrecat == "503") {echo "selected";} ?>>Phim truyện</option>
                        <option value="440" <?php if($tuoitrecat == "440") {echo "selected";} ?>>Khoa học</option>
                
                        <option value="439" <?php if($tuoitrecat == "439") {echo "selected";} ?>>Nhịp sống số</option>
                        <option value="438" <?php if($tuoitrecat == "438") {echo "selected";} ?>>Kinh tế</option>
                        <option value="80" <?php if($tuoitrecat == "80") {echo "selected";} ?>>Khởi Sự Doanh Nghiệp</option>
                        <option value="167" <?php if($tuoitrecat == "167") {echo "selected";} ?>>Ng&#224;y t&#236;nh y&#234;u</option>
                        <option value="436" <?php if($tuoitrecat == "436") {echo "selected";} ?>>Văn h&#243;a - Giải tr&#237;</option>
                        <option value="136" <?php if($tuoitrecat == "136") {echo "selected";} ?>>--Dịch Vụ-Du Lịch</option>
                        <option value="437" <?php if($tuoitrecat == "437") {echo "selected";} ?>>$ Nhịp sống trẻ</option>
                        <option value="81" <?php if($tuoitrecat == "81") {echo "selected";} ?>>Thế Giới Trẻ</option>
                        <option value="168" <?php if($tuoitrecat == "168") {echo "selected";} ?>>Giải tr&#237;</option>

                </select>
    
                <!--END TUOITRE DEPARTMENT-->
			</td>
		  </tr>
		  
		  
		  <tr>
		    <td>
				Post every:<br/>
				<input size="5" name="rbvn_postevery" type="text" id="rbvn_postevery" value=""/>
				<select name="rbvn_period" id="rbvn_period">
					<option value="hours">Hours</option>
					<option value="days">Days</option>
				</select>
			</td>
		    <td></td>
		    <td><p class="submit" style="margin:0;padding: 10px 0;"><input type="submit" name="rbvn_post" value="Thêm từ khóa" /></p></td>
		  </tr>
          
		</table>

		
		</div>
</form>
<h3>Versions</h3>
<?php
rbvn_get_versions();
?>
</div>
	<?php
	
}//End function
function rbvn_get_versions() {
   global $rbvn_core_version, $rbvn_vnexpress_ver, $rbvn_tuoitre_ver;
	$server_url = "http://vnwebmaster.com/services/robotvn_versions.php";
	
	if ( function_exists('curl_init') ) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (compatible; Konqueror/4.0; Microsoft Windows) KHTML/4.0.80 (like Gecko)");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 'Content-type: application/x-www-form-urlencoded;charset=UTF-8');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $server_url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		$versions = curl_exec($ch);
		curl_close($ch);
	} else { 				
		$versions = @file_get_contents($server_url);
	}
	$versions = explode(";", $versions);
	?>
	<table>
		<tr>
			<td><b>RobotVN Core</b></td>
			<td style="padding-left:10px;">Phiên bản hiện tại của bạn: <?php echo $rbvn_core_version; ?></td>
			<td style="padding-left:10px;<?php if($rbvn_core_version != $versions[0]) {echo "color: #cc0000;font-weight:bold;";} ?>">Phiên bản mới: <?php echo $versions[0]; ?></td>
		</tr>	
		<?php if(function_exists('rbvn_vnexpresspost')) { ?>
		<tr>
			<td><b>VNExpress Robot</b></td>
			<td style="padding-left:10px;">Phiên bản hiện tại của bạn: <?php echo $rbvn_vnexpress_ver; ?></td>
			<td style="padding-left:10px;<?php if($rbvn_vnexpress_ver != $versions[1]) {echo "color: #cc0000;font-weight:bold;";} ?>">Phiên bản mới: <?php echo $versions[1]; ?></td>
		</tr>	
		<?php } if(function_exists('rbvn_tuoitrepost')) { ?>		
		<tr>
			<td><b>TuoiTre Robot</b></td>
			<td style="padding-left:10px;">Phiên bản hiện tại của bạn: <?php echo $rbvn_tuoitre_ver; ?></td>
			<td style="padding-left:10px;<?php if($rbvn_tuoitre_ver != $versions[2]) {echo "color: #cc0000;font-weight:bold;";} ?>">Phiên bản mới: <?php echo $versions[2]; ?></td>
		</tr>
		<?php }?>		
	</table>
	<ul style="list-style-type: disc; margin-left: 15px;">
	<li><b><a href="http://vnwebmaster.com">Xem và cập nhật phiên bản mới tại đây!</a></b></li>
	</ul>
	<?php
}
function rbvn_resetred() {
	global $wpdb,$rbvn_dbtable;
	$sql = "UPDATE " . $rbvn_dbtable . " SET `num_vnexpress` = '0' WHERE `num_vnexpress` < '-1'";
	$update = $wpdb->query($sql);
	$sql = "UPDATE " . $rbvn_dbtable . " SET `num_tuoitre` = '0' WHERE `num_tuoitre` < '-1'";
	$update = $wpdb->query($sql);	
}

function rbvn_editkw() {
   global $wpdb, $rbvn_dbtable, $modules;
   
	$which =$_GET['kw'];
		 
	$sql = "SELECT * FROM " . $rbvn_dbtable . " WHERE id = '$which'";
	$result = $wpdb->get_row($sql);	

	
	$vnnum = $result->num_vnexpress;
	$ttnum = $result->num_tuoitre;
	
	$keyword = $result->keyword;
	$cat = $result->category;
	$vnexcat = $result->vnexcat;
	$tuoitrecat = $result->tuoitrecat;
	
	$postspanorig = $result->postspan;	
	$time = rbvn_get_string_between($postspanorig, "RBVN_", "_");
	
					$findMich   = 'days';
					$pos = strpos($postspanorig, $findMich);
					if ($pos === false) {
						$span = "hours";
					} else {
					    $span = "days";
					}	   
   
   	if($_POST['rbvn_update']){
	
	$id = $_POST['rbvn_id'];
	$postevery = $_POST['rbvn_postevery'];
	$cr_period = $_POST['rbvn_period'];
	
	
	
	$vnex_cat= $_POST['rbvn_vnc_vnccat'];
	$ttcat= $_POST['rbvn_tuoitrecat'];
	$postspan = "RBVN_" . $postevery . "_" . $cr_period;	

	if($postspan != $postspanorig) {		
		rbvn_set_schedule($postevery, $cr_period);	
		wp_reschedule_event( time(), $postspan, "rbvnposthook", array($id) );
	}
	
	$keyword = $_POST['rbvn_keyword'];
	$category = $_POST['rbvn_category'];

    $update = "UPDATE " . $rbvn_dbtable . " SET postspan = '$postspan', keyword = '$keyword', vnexcat = '$vnex_cat', tuoitrecat = '$ttcat', category = '$category'";
 
	//Update
	if ($_POST['rbvn_post_vne'] != 'yes') {$update .= ", num_vnexpress = '-1'";} elseif($vnnum < 0 && $_POST['rbvn_post_vne'] == 'yes') {$update .= ", num_vnexpress = '0'";}
	if ($_POST['rbvn_post_tuoitre'] != 'yes') {$update .= ", num_tuoitre = '-1'";} elseif($ttnum < 0 && $_POST['rbvn_post_tuoitre'] == 'yes') {$update .= ", num_tuoitre = '0'";}
	$update .= " WHERE id = $id";

	$results = $wpdb->query($update);

	if ($results) {
		echo '<div class="updated"><p>Keyword '.$keyword.' đã được cập nhật! <a href="admin.php?page=RobotVN/robotvn.php">Go back</a></p></div>';		
			if($postspan != $postspanorig) {rbvn_delete_schedule($time, $span);}
	} else {
		echo '<div class="updated"><p>Keyword không được cập nhật thành công! <a href="admin.php?page=RobotVN/robotvn.php">Go back</a></p></div>';			
	}	
	
	} elseif($_GET['kw'] != "") {
   
?>
	<div class="wrap">
	<h2>Chỉnh sửa từ khóa</h2>
	<form method="post" id="rbvn_editkw">	
		<div style="padding:8px;margin-top: 4px;border:1px solid #e3e3e3;-moz-border-radius:5px;width:650px;background-color:#f1f1f1;">	
		<table>
			<input style="display:none;" name="rbvn_id" type="text" id="rbvn_id" value="<?php echo $which;?>"/>
		  <tr>
		    <td width="220px"><b>Thiết lập chính</b></td>
		    <td width="200px"><b>Nguồn bài</b></td>
		    <td><b>Thiết lập mở rộng</b></td>
		  </tr>		
		
		  <tr>
		    <td rowspan="2">
			Từ khóa:<br/><input name="rbvn_keyword" type="text" id="rbvn_keyword" value="<?php echo $keyword;?>"/>
			</td>
		    <td><input name="rbvn_post_vne" type="checkbox" id="rbvn_post_vne" value="yes" <?php if(function_exists('rbvn_vnexpresspost') && $vnnum != -1) {echo "checked";} elseif($vnnum == -1) {} else {echo "disabled";} ?> /> VNExpress.NET</td>
		    <td rowspan="2">
				  <!-- VNEXPRESS DEPARTMENT -->
                    VNExpress Category:<br />
                    <select name="rbvn_vnc_vnccat" id="rbvn_vnc_vnccat">
                        <option value="" <?php if($vnexcat == "") {echo "selected";} ?>>Toàn bộ các mục</option>
                        <option value="" <?php if($vnexcat == "") {echo "selected";} ?>>Xã hội</option>
        
                        <option value="2" <?php if($vnexcat == "2") {echo "selected";} ?>>Thế giới</option>
                        <option value="3" <?php if($vnexcat == "3") {echo "selected";} ?>>Kinh doanh</option>
                        <option value="51" <?php if($vnexcat == "51") {echo "selected";} ?>>Văn hóa</option>
                        <option value="9" <?php if($vnexcat == "9") {echo "selected";} ?>>Thể thao</option>
                        <option value="47" <?php if($vnexcat == "47") {echo "selected";} ?>>Pháp luật</option>
                        <option value="110" <?php if($vnexcat == "110") {echo "selected";} ?>>Đời sống</option>
        
                        <option value="83" <?php if($vnexcat == "83") {echo "selected";} ?>>Khoa học</option>
                        <option value="89" <?php if($vnexcat == "89") {echo "selected";} ?>>Vi tính</option>
                        <option value="38" <?php if($vnexcat == "38") {echo "selected";} ?>>Ô tô xe máy</option>
                        <option value="109" <?php if($vnexcat == "109") {echo "selected";} ?>>Bạn đọc viết</option>
                        <option value="127" <?php if($vnexcat == "127") {echo "selected";} ?>>Tâm sự</option>
                        <option value="105" <?php if($vnexcat == "105") {echo "selected";} ?>>Cười</option>
                    </select>
        
                    <!--END VNEXPRESS DEPARTMENT-->
			</td>
		  </tr>
		  <tr>
		    <td>
            <input name="rbvn_post_tuoitre" type="checkbox" id="rbvn_post_tuoitre" value="yes" <?php if(function_exists('rbvn_tuoitrepost') && $ttnum != -1) {echo "checked";} elseif($ttnum == -1) {} else {echo "disabled";} ?> /> TuoiTre</td>
		  </tr>	  
		  <tr>
		    <td>
			Category:<br/>
				<select name="rbvn_category" id="rbvn_category">				
				<?php
				   				   $categories = get_categories('type=post&hide_empty=0');
				   				   foreach($categories as $category)
				   				   {
				   				   echo '<option value="'.$category->cat_ID.'">'.$category->cat_name.'</option>';
				   				   }				
				?>				
				</select>				
			</td>
		    <td></td>
		    <td>
				 <!-- TUOITRE DEPARTMENT -->
                TuoiTre Category:<br />
                <select name="rbvn_tuoitrecat" id="rbvn_tuoitrecat">
                    <option value="" <?php if($tuoitrecat == "") {echo "selected";} ?>>Toàn bộ các mục</option>
                        <option value="-1" <?php if($tuoitrecat == "-1") {echo "selected";} ?>>To&#224;n bộ c&#225;c chuyện mục</option>
                        <option value="3" <?php if($tuoitrecat == "3") {echo "selected";} ?>>Ch&#237;nh trị - X&#227; hội</option>
                        <option value="17" <?php if($tuoitrecat == "17") {echo "selected";} ?>>--M&#244;i trường</option>
                        <option value="302" <?php if($tuoitrecat == "302") {echo "selected";} ?>>----&#221; kiến - Khoa học</option>
                        <option value="220" <?php if($tuoitrecat == "220") {echo "selected";} ?>>----Ph&#225;t minh mới</option>
                        <option value="235" <?php if($tuoitrecat == "235") {echo "selected";} ?>>----Vũ trụ</option>
                        <option value="232" <?php if($tuoitrecat == "232") {echo "selected";} ?>>----Khảo cổ</option>
                
                        <option value="326" <?php if($tuoitrecat == "326") {echo "selected";} ?>>--Nghe v&#224; nghĩ</option>
                        <option value="447" <?php if($tuoitrecat == "447") {echo "selected";} ?>>--Thư viện ph&#225;p luật</option>
                        <option value="293" <?php if($tuoitrecat == "293") {echo "selected";} ?>>--Văn Kiện</option>
                        <option value="347" <?php if($tuoitrecat == "347") {echo "selected";} ?>>--Tiếp thị h&#236;nh ảnh Việt Nam</option>
                        <option value="343" <?php if($tuoitrecat == "343") {echo "selected";} ?>>--Thời cơ v&#224;ng</option>
                
                        <option value="327" <?php if($tuoitrecat == "327") {echo "selected";} ?>>--Mỗi ng&#224;y một con số</option>
                        <option value="239" <?php if($tuoitrecat == "239") {echo "selected";} ?>>--Hỏi &amp; đ&#225;p xuất nhập cảnh</option>
                        <option value="397" <?php if($tuoitrecat == "397") {echo "selected";} ?>>--Vươn ra biển lớn</option>
                        <option value="22" <?php if($tuoitrecat == "22") {echo "selected";} ?>>--Ch&#237;nh Trị</option>
                
                        <option value="289" <?php if($tuoitrecat == "289") {echo "selected";} ?>>--&#221; kiến - CTXH</option>
                        <option value="6" <?php if($tuoitrecat == "6") {echo "selected";} ?>>--Ph&#225;p luật</option>
                
                        <option value="268" <?php if($tuoitrecat == "268") {echo "selected";} ?>>----Luật xứ người</option>
                        <option value="133" <?php if($tuoitrecat == "133") {echo "selected";} ?>>----Vụ &#225;n</option>
                        <option value="266" <?php if($tuoitrecat == "266") {echo "selected";} ?>>----K&#253; Sự Ph&#225;p Đ&#236;nh</option>
                
                        <option value="300" <?php if($tuoitrecat == "300") {echo "selected";} ?>>----&#221; kiến - Ph&#225;p luật</option>
                        <option value="79" <?php if($tuoitrecat == "79") {echo "selected";} ?>>----Tư vấn ph&#225;p luật</option>
                        <option value="12" <?php if($tuoitrecat == "12") {echo "selected";} ?>>--Sống khỏe</option>
                        <option value="455" <?php if($tuoitrecat == "455") {echo "selected";} ?>>----Thắc mắc biết hỏi ai ?</option>
                        <option value="241" <?php if($tuoitrecat == "241") {echo "selected";} ?>>----Giới t&#237;nh</option>
                
                        <option value="231" <?php if($tuoitrecat == "231") {echo "selected";} ?>>----Ph&#242;ng mạch Online</option>
                        <option value="243" <?php if($tuoitrecat == "243") {echo "selected";} ?>>----Mẹo vặt cho cuộc sống</option>
                        <option value="448" <?php if($tuoitrecat == "448") {echo "selected";} ?>>----Cảnh b&#225;o</option>
                        <option value="198" <?php if($tuoitrecat == "198") {echo "selected";} ?>>----Những điều cần biết</option>
                        <option value="197" <?php if($tuoitrecat == "197") {echo "selected";} ?>>----Sức khỏe mẹ v&#224; b&#233;</option>
                
                        <option value="150" <?php if($tuoitrecat == "150") {echo "selected";} ?>>----C&#250;m g&#224;: Th&#244;ng tin cần biết</option>
                        <option value="59" <?php if($tuoitrecat == "59") {echo "selected";} ?>>------Truyền H&#236;nh</option>
                        <option value="301" <?php if($tuoitrecat == "301") {echo "selected";} ?>>----&#221; kiến - Sức khỏe</option>
                        <option value="89" <?php if($tuoitrecat == "89") {echo "selected";} ?>>--Ph&#243;ng sự - K&#253; sự</option>
                
                        <option value="87" <?php if($tuoitrecat == "87") {echo "selected";} ?>>--Thời sự suy nghĩ</option>
                        <option value="88" <?php if($tuoitrecat == "88") {echo "selected";} ?>>--Chuyện thường ng&#224;y</option>
                        <option value="2" <?php if($tuoitrecat == "2") {echo "selected";} ?>>Thế giới</option>
                        <option value="331" <?php if($tuoitrecat == "331") {echo "selected";} ?>>--Việt Nam v&#224; những người bạn</option>
                        <option value="291" <?php if($tuoitrecat == "291") {echo "selected";} ?>>--&#221; kiến - Thế Giới</option>
                
                        <option value="94" <?php if($tuoitrecat == "94") {echo "selected";} ?>>--Quan s&#225;t &amp; B&#236;nh luận</option>
                        <option value="396" <?php if($tuoitrecat == "396") {echo "selected";} ?>>--C&#226;u chuyện cuối tuần</option>
                        <option value="442" <?php if($tuoitrecat == "442") {echo "selected";} ?>>--Thế giới mu&#244;n m&#224;u</option>
                        <option value="20" <?php if($tuoitrecat == "20") {echo "selected";} ?>>--Hồ sơ</option>
                
                        <option value="312" <?php if($tuoitrecat == "312") {echo "selected";} ?>>--Người Việt xa qu&#234;</option>
                        <option value="328" <?php if($tuoitrecat == "328") {echo "selected";} ?>>----Diễn đ&#224;n người xa xứ</option>
                        <option value="340" <?php if($tuoitrecat == "340") {echo "selected";} ?>>----G&#243;c tự cảm</option>
                        <option value="341" <?php if($tuoitrecat == "341") {echo "selected";} ?>>----Du học sinh</option>
                        <option value="342" <?php if($tuoitrecat == "342") {echo "selected";} ?>>------Ứng xử v&#224; Kinh nghiệm</option>
                
                        <option value="314" <?php if($tuoitrecat == "314") {echo "selected";} ?>>----T&#224;i tr&#237; Việt</option>
                        <option value="315" <?php if($tuoitrecat == "315") {echo "selected";} ?>>----Hộp thư Kiều b&#224;o</option>
                        <option value="316" <?php if($tuoitrecat == "316") {echo "selected";} ?>>----Bản sắc Việt</option>
                        <option value="329" <?php if($tuoitrecat == "329") {echo "selected";} ?>>----Thư phương xa</option>
                        <option value="7" <?php if($tuoitrecat == "7") {echo "selected";} ?>>Nhịp sống trẻ</option>
                
                        <option value="348" <?php if($tuoitrecat == "348") {echo "selected";} ?>>--Sống v&#236; cộng đồng</option>
                        <option value="98" <?php if($tuoitrecat == "98") {echo "selected";} ?>>--Cửa sổ t&#226;m hồn</option>
                        <option value="193" <?php if($tuoitrecat == "193") {echo "selected";} ?>>--Khi ta lớn</option>
                        <option value="365" <?php if($tuoitrecat == "365") {echo "selected";} ?>>--L&#224;m đẹp - Thời trang</option>
                        <option value="366" <?php if($tuoitrecat == "366") {echo "selected";} ?>>----Mỹ phẩm</option>
                
                        <option value="367" <?php if($tuoitrecat == "367") {echo "selected";} ?>>----Đẹp c&#249;ng sao</option>
                        <option value="364" <?php if($tuoitrecat == "364") {echo "selected";} ?>>----Tư vấn Khỏe Đẹp</option>
                        <option value="362" <?php if($tuoitrecat == "362") {echo "selected";} ?>>----Th&#244;ng tin</option>
                        <option value="363" <?php if($tuoitrecat == "363") {echo "selected";} ?>>----Bộ sưu tập</option>
                        <option value="96" <?php if($tuoitrecat == "96") {echo "selected";} ?>>--Sinh vi&#234;n</option>
                
                        <option value="298" <?php if($tuoitrecat == "298") {echo "selected";} ?>>--&#221; kiến - NS trẻ</option>
                        <option value="370" <?php if($tuoitrecat == "370") {echo "selected";} ?>>--Tư vấn học đường</option>
                        <option value="194" <?php if($tuoitrecat == "194") {echo "selected";} ?>>--T&#236;nh y&#234;u - Lối sống</option>
                        <option value="257" <?php if($tuoitrecat == "257") {echo "selected";} ?>>----Chia sẻ</option>
                        <option value="92" <?php if($tuoitrecat == "92") {echo "selected";} ?>>----Từ tr&#225;i tim đến tr&#225;i tim</option>
                
                
                        <option value="57" <?php if($tuoitrecat == "57") {echo "selected";} ?>>--Điện ảnh - Truyền h&#236;nh</option>
                        <option value="62" <?php if($tuoitrecat == "62") {echo "selected";} ?>>--S&#226;n khấu</option>
                        <option value="61" <?php if($tuoitrecat == "61") {echo "selected";} ?>>--Văn học</option>
                        <option value="158" <?php if($tuoitrecat == "158") {echo "selected";} ?>>----Thơ V&#224; Tuổi Trẻ</option>
                
                
                        <option value="128" <?php if($tuoitrecat == "128") {echo "selected";} ?>>--Giải tr&#237; h&#244;m nay</option>
                        <option value="349" <?php if($tuoitrecat == "349") {echo "selected";} ?>>----&#212; số</option>
                        <option value="350" <?php if($tuoitrecat == "350") {echo "selected";} ?>>----&#212; chữ</option>
                        <option value="385" <?php if($tuoitrecat == "385") {echo "selected";} ?>>----Chương Tr&#236;nh Truyền H&#236;nh</option>
                
                        <option value="175" <?php if($tuoitrecat == "175") {echo "selected";} ?>>----Nhạc mới</option>
                        <option value="173" <?php if($tuoitrecat == "173") {echo "selected";} ?>>----Phim mới</option>
                        <option value="176" <?php if($tuoitrecat == "176") {echo "selected";} ?>>----S&#226;n chơi</option>
                        <option value="407" <?php if($tuoitrecat == "407") {echo "selected";} ?>>----Qu&#224; tặng bạn đọc</option>
                        <option value="296" <?php if($tuoitrecat == "296") {echo "selected";} ?>>--&#221; kiến - VHGT</option>
                
                        <option value="16" <?php if($tuoitrecat == "16") {echo "selected";} ?>>Nhịp sống số</option>
                        <option value="65" <?php if($tuoitrecat == "65") {echo "selected";} ?>>--Bảo mật</option>
                        <option value="536" <?php if($tuoitrecat == "536") {echo "selected";} ?>>----Virus</option>
                        <option value="537" <?php if($tuoitrecat == "537") {echo "selected";} ?>>----Hacker</option>
                        <option value="184" <?php if($tuoitrecat == "184") {echo "selected";} ?>>--Mobile Games 2005</option>
                        <option value="185" <?php if($tuoitrecat == "185") {echo "selected";} ?>>----Hướng dẫn J2ME</option>
                
                        <option value="186" <?php if($tuoitrecat == "186") {echo "selected";} ?>>----C&#244;ng cụ viết game</option>
                        <option value="74" <?php if($tuoitrecat == "74") {echo "selected";} ?>>--Thế giới phần cứng</option>
                        <option value="337" <?php if($tuoitrecat == "337") {echo "selected";} ?>>----PCs</option>
                        <option value="335" <?php if($tuoitrecat == "335") {echo "selected";} ?>>----ĐTDĐ &amp; PDA</option>
                        <option value="66" <?php if($tuoitrecat == "66") {echo "selected";} ?>>--Thế giới games</option>
                
                        <option value="444" <?php if($tuoitrecat == "444") {echo "selected";} ?>>----Game online</option>
                        <option value="544" <?php if($tuoitrecat == "544") {echo "selected";} ?>>----Trailer</option>
                        <option value="545" <?php if($tuoitrecat == "545") {echo "selected";} ?>>----Wallpaper</option>
                        <option value="67" <?php if($tuoitrecat == "67") {echo "selected";} ?>>--Thủ thuật</option>
                        <option value="533" <?php if($tuoitrecat == "533") {echo "selected";} ?>>----Phần mềm</option>
                        <option value="534" <?php if($tuoitrecat == "534") {echo "selected";} ?>>----M&#225;y t&#237;nh</option>
                
                        <option value="535" <?php if($tuoitrecat == "535") {echo "selected";} ?>>----Di động</option>
                        <option value="297" <?php if($tuoitrecat == "297") {echo "selected";} ?>>--Nhịp cầu số h&#243;a</option>
                        <option value="540" <?php if($tuoitrecat == "540") {echo "selected";} ?>>----NSS-Tư vấn</option>
                        <option value="541" <?php if($tuoitrecat == "541") {echo "selected";} ?>>----NSS-Hỏi đ&#225;p</option>
                        <option value="542" <?php if($tuoitrecat == "542") {echo "selected";} ?>>----NSS-Thường thức</option>
                
                        <option value="431" <?php if($tuoitrecat == "431") {echo "selected";} ?>>--Tư vấn ti&#234;u d&#249;ng</option>
                        <option value="529" <?php if($tuoitrecat == "529") {echo "selected";} ?>>--Viễn Th&#244;ng</option>
                        <option value="430" <?php if($tuoitrecat == "430") {echo "selected";} ?>>----Điện thoại</option>
                        <option value="551" <?php if($tuoitrecat == "551") {echo "selected";} ?>>------Smartphone</option>
                        <option value="552" <?php if($tuoitrecat == "552") {echo "selected";} ?>>------PDA</option>
                
                        <option value="553" <?php if($tuoitrecat == "553") {echo "selected";} ?>>------Phụ kiện</option>
                        <option value="530" <?php if($tuoitrecat == "530") {echo "selected";} ?>>--Thiết bị số</option>
                        <option value="531" <?php if($tuoitrecat == "531") {echo "selected";} ?>>----Phần cứng</option>
                        <option value="336" <?php if($tuoitrecat == "336") {echo "selected";} ?>>----Thiết bị giải tr&#237;</option>
                        <option value="554" <?php if($tuoitrecat == "554") {echo "selected";} ?>>----PC</option>
                        <option value="555" <?php if($tuoitrecat == "555") {echo "selected";} ?>>----Camera</option>
                
                
                        <option value="421" <?php if($tuoitrecat == "421") {echo "selected";} ?>>--Chuẩn bị v&#224;o đời</option>
                        <option value="420" <?php if($tuoitrecat == "420") {echo "selected";} ?>>--Trao đổi</option>
                        <option value="419" <?php if($tuoitrecat == "419") {echo "selected";} ?>>--Văn</option>
                        <option value="415" <?php if($tuoitrecat == "415") {echo "selected";} ?>>--Thơ</option>
                        <option value="119" <?php if($tuoitrecat == "119") {echo "selected";} ?>>Tuổi Trẻ Cuối tuần</option>
                        <option value="277" <?php if($tuoitrecat == "277") {echo "selected";} ?>>--Truyện ngắn</option>
                
                        <option value="123" <?php if($tuoitrecat == "123") {echo "selected";} ?>>--Chuy&#234;n đề</option>
                        <option value="122" <?php if($tuoitrecat == "122") {echo "selected";} ?>>--Khoa Học- Kỹ thuật</option>
                        <option value="121" <?php if($tuoitrecat == "121") {echo "selected";} ?>>--Mỗi tuần một từ</option>
                        <option value="120" <?php if($tuoitrecat == "120") {echo "selected";} ?>>--Người đương thời</option>
                        <option value="126" <?php if($tuoitrecat == "126") {echo "selected";} ?>>--Ph&#243;ng sự &amp; Hồ sơ</option>
                
                        <option value="125" <?php if($tuoitrecat == "125") {echo "selected";} ?>>--Thể Thao Cuối Tuần</option>
                        <option value="127" <?php if($tuoitrecat == "127") {echo "selected";} ?>>--Thư gi&#227;n</option>
                        <option value="172" <?php if($tuoitrecat == "172") {echo "selected";} ?>>--Thế giới s&#225;ch</option>
                        <option value="103" <?php if($tuoitrecat == "103") {echo "selected";} ?>>Tuổi Trẻ Cười</option>
                        <option value="372" <?php if($tuoitrecat == "372") {echo "selected";} ?>>--Ch&#237;nh trị - X&#227; hội cười</option>
                        <option value="373" <?php if($tuoitrecat == "373") {echo "selected";} ?>>--Chuy&#234;n đề cười</option>
                
                        <option value="374" <?php if($tuoitrecat == "374") {echo "selected";} ?>>--Văn nghệ cười</option>
                        <option value="375" <?php if($tuoitrecat == "375") {echo "selected";} ?>>--Thể thao cười</option>
                        <option value="376" <?php if($tuoitrecat == "376") {echo "selected";} ?>>--Vui... vui...</option>
                        <option value="377" <?php if($tuoitrecat == "377") {echo "selected";} ?>>--Thơ ch&#226;m biếm</option>
                        <option value="378" <?php if($tuoitrecat == "378") {echo "selected";} ?>>--Bạn đọc cười</option>
                        <option value="380" <?php if($tuoitrecat == "380") {echo "selected";} ?>>--Qu&#225;n mắc cỡ</option>
                
                        <option value="115" <?php if($tuoitrecat == "115") {echo "selected";} ?>>--Đời Cười</option>
                        <option value="117" <?php if($tuoitrecat == "117") {echo "selected";} ?>>--Thế Giới Cười</option>
                        <option value="116" <?php if($tuoitrecat == "116") {echo "selected";} ?>>--Tranh h&#224;i hước</option>
                        <option value="100" <?php if($tuoitrecat == "100") {echo "selected";} ?>>Du Lịch</option>
                        <option value="303" <?php if($tuoitrecat == "303") {echo "selected";} ?>>--&#221; kiến - Du lịch</option>
                
                        <option value="481" <?php if($tuoitrecat == "481") {echo "selected";} ?>>--Kh&#225;m ph&#225;</option>
                        <option value="333" <?php if($tuoitrecat == "333") {echo "selected";} ?>>--Những miền đất lạ</option>
                        <option value="217" <?php if($tuoitrecat == "217") {echo "selected";} ?>>--Qu&#225;n cuối tuần</option>
                        <option value="218" <?php if($tuoitrecat == "218") {echo "selected";} ?>>--Ẩm thực</option>
                        <option value="319" <?php if($tuoitrecat == "319") {echo "selected";} ?>>--Du lịch Việt Nam</option>
                
                        <option value="219" <?php if($tuoitrecat == "219") {echo "selected";} ?>>--Chuyến đi của bạn</option>
                        <option value="480" <?php if($tuoitrecat == "480") {echo "selected";} ?>>--Văn h&#243;a</option>
                        <option value="204" <?php if($tuoitrecat == "204") {echo "selected";} ?>>Địa ốc</option>
                        <option value="451" <?php if($tuoitrecat == "451") {echo "selected";} ?>>--Nh&#224; đất</option>
                        <option value="452" <?php if($tuoitrecat == "452") {echo "selected";} ?>>--Thế giới kiến tr&#250;c</option>
                
                        <option value="450" <?php if($tuoitrecat == "450") {echo "selected";} ?>>--Đ&#244; thị</option>
                        <option value="560" <?php if($tuoitrecat == "560") {echo "selected";} ?>>TEEN</option>
                        <option value="561" <?php if($tuoitrecat == "561") {echo "selected";} ?>>--TEEN-Thế giới Teen</option>
                        <option value="569" <?php if($tuoitrecat == "569") {echo "selected";} ?>>----TEEN-Nhịp sống</option>
                        <option value="571" <?php if($tuoitrecat == "571") {echo "selected";} ?>>----TEEN-G&#243;c s&#226;n trường</option>
                
                        <option value="572" <?php if($tuoitrecat == "572") {echo "selected";} ?>>----TEEN-T&#244;i v&#224;o đời</option>
                        <option value="573" <?php if($tuoitrecat == "573") {echo "selected";} ?>>----TEEN-Bốn phương</option>
                        <option value="562" <?php if($tuoitrecat == "562") {echo "selected";} ?>>--TEEN-Văn học</option>
                        <option value="574" <?php if($tuoitrecat == "574") {echo "selected";} ?>>----TEEN-S&#225;ng t&#225;c</option>
                        <option value="575" <?php if($tuoitrecat == "575") {echo "selected";} ?>>----TEEN-Truyện ngắn</option>
                
                        <option value="576" <?php if($tuoitrecat == "576") {echo "selected";} ?>>----TEEN-Thơ</option>
                        <option value="577" <?php if($tuoitrecat == "577") {echo "selected";} ?>>----TEEN-S&#225;ch ơi! mở ra</option>
                        <option value="563" <?php if($tuoitrecat == "563") {echo "selected";} ?>>--TEEN-Sống Hi-tech</option>
                        <option value="578" <?php if($tuoitrecat == "578") {echo "selected";} ?>>----TEEN-S&#224;nh điệu</option>
                        <option value="579" <?php if($tuoitrecat == "579") {echo "selected";} ?>>----TEEN-Game</option>
                
                        <option value="580" <?php if($tuoitrecat == "580") {echo "selected";} ?>>----TEEN-Thế giới số</option>
                        <option value="564" <?php if($tuoitrecat == "564") {echo "selected";} ?>>--TEEN-Khỏe-Điệu</option>
                        <option value="581" <?php if($tuoitrecat == "581") {echo "selected";} ?>>----TEEN-Chuyện ch&#250;ng m&#236;nh</option>
                        <option value="582" <?php if($tuoitrecat == "582") {echo "selected";} ?>>----TEEN-L&#224;m đẹp</option>
                        <option value="583" <?php if($tuoitrecat == "583") {echo "selected";} ?>>----TEEN-Thời trang</option>
                
                        <option value="584" <?php if($tuoitrecat == "584") {echo "selected";} ?>>----TEEN-Khỏe hơn</option>
                        <option value="565" <?php if($tuoitrecat == "565") {echo "selected";} ?>>--TEEN-Y&#234;u</option>
                        <option value="585" <?php if($tuoitrecat == "585") {echo "selected";} ?>>----TEEN-Thư t&#236;nh trong cặp</option>
                        <option value="586" <?php if($tuoitrecat == "586") {echo "selected";} ?>>----TEEN-Gửi gi&#243;</option>
                        <option value="587" <?php if($tuoitrecat == "587") {echo "selected";} ?>>----TEEN-Vườn y&#234;u</option>
                
                        <option value="588" <?php if($tuoitrecat == "588") {echo "selected";} ?>>----TEEN-Trắc nghiệm</option>
                        <option value="566" <?php if($tuoitrecat == "566") {echo "selected";} ?>>--TEEN-Giải tr&#237;</option>
                        <option value="589" <?php if($tuoitrecat == "589") {echo "selected";} ?>>----TEEN-Sao</option>
                        <option value="590" <?php if($tuoitrecat == "590") {echo "selected";} ?>>----TEEN-&#194;m nhạc</option>
                        <option value="591" <?php if($tuoitrecat == "591") {echo "selected";} ?>>----TEEN-Phim</option>
                        <option value="592" <?php if($tuoitrecat == "592") {echo "selected";} ?>>----TEEN-Giọng ca v&#224;ng</option>
                
                        <option value="593" <?php if($tuoitrecat == "593") {echo "selected";} ?>>----TEEN-Điểm hẹn</option>
                        <option value="567" <?php if($tuoitrecat == "567") {echo "selected";} ?>>--TEEN-N&#243;i với Teen</option>
                        <option value="568" <?php if($tuoitrecat == "568") {echo "selected";} ?>>--TEEN-G&#243;c của Teen</option>
                
                        <option value="308" <?php if($tuoitrecat == "308") {echo "selected";} ?>>Đọc g&#236; h&#244;m nay</option>
                        <option value="318" <?php if($tuoitrecat == "318") {echo "selected";} ?>>T&#226;m điểm Kiều b&#224;o</option>
                        <option value="411" <?php if($tuoitrecat == "411") {echo "selected";} ?>>T&#236;m hiểu tư tưởng, đạo đức Hồ Ch&#237; Minh</option>
                        <option value="177" <?php if($tuoitrecat == "177") {echo "selected";} ?>>Kham Pha Cuoc Song</option>
                
                        <option value="441" <?php if($tuoitrecat == "441") {echo "selected";} ?>>Gi&#225;o dục</option>
                        <option value="445" <?php if($tuoitrecat == "445") {echo "selected";} ?>>Thể thao</option>
                        <option value="503" <?php if($tuoitrecat == "503") {echo "selected";} ?>>Phim truyện</option>
                        <option value="440" <?php if($tuoitrecat == "440") {echo "selected";} ?>>Khoa học</option>
                
                        <option value="439" <?php if($tuoitrecat == "439") {echo "selected";} ?>>Nhịp sống số</option>
                        <option value="438" <?php if($tuoitrecat == "438") {echo "selected";} ?>>Kinh tế</option>
                        <option value="80" <?php if($tuoitrecat == "80") {echo "selected";} ?>>Khởi Sự Doanh Nghiệp</option>
                        <option value="167" <?php if($tuoitrecat == "167") {echo "selected";} ?>>Ng&#224;y t&#236;nh y&#234;u</option>
                        <option value="436" <?php if($tuoitrecat == "436") {echo "selected";} ?>>Văn h&#243;a - Giải tr&#237;</option>
                        <option value="136" <?php if($tuoitrecat == "136") {echo "selected";} ?>>--Dịch Vụ-Du Lịch</option>
                        <option value="437" <?php if($tuoitrecat == "437") {echo "selected";} ?>>$ Nhịp sống trẻ</option>
                        <option value="81" <?php if($tuoitrecat == "81") {echo "selected";} ?>>Thế Giới Trẻ</option>
                        <option value="168" <?php if($tuoitrecat == "168") {echo "selected";} ?>>Giải tr&#237;</option>
                    
                </select>
    
                <!--END TUOITRE DEPARTMENT-->
			</td>
		  </tr>
		  
		  
		  <tr>
		    <td>
				Post every:<br/>
				<input size="5" name="rbvn_postevery" type="text" id="rbvn_postevery" value="<?php echo $time;?>"/>
				<select name="rbvn_period" id="rbvn_period">
					<option value="hours" <?php if($span == "hours") {echo "selected";} ?>>Hours</option>
					<option value="days" <?php if($span == "days") {echo "selected";} ?>>Days</option>
				</select>
			</td>
		    <td></td>
		    <td><p class="submit" style="margin:0;"><input type="submit" name="rbvn_update" value="Lưu thay đổi" /></p></td>
		  </tr>
          
		</table>

		
		</div>
</form>
</div>
<?php	
	} else {
	?>
	<div class="wrap">
	<h2>Edit Keyword</h2>
		<p>No keyword specified. Go back and select one!</p>
	</div>	
	<?php
	}	 
}
function rbvn_sub_options() {

	if($_POST['rbvn_reset']){
		rbvn_reset_options();
		echo '<div class="updated"><p>Cấu hình đã được reset lại!</p></div>';
	}
	if($_POST['rbvn_save']){
			update_option('rbvn_poststatus',$_POST['rbvn_poststatus']);
			update_option('rbvn_resetcount',$_POST['rbvn_resetcount']);
			update_option('rbvn_badwords',$_POST['rbvn_badwords']);
			update_option('rbvn_autotag',$_POST['rbvn_autotag']);
			update_option('rbvn_randomstart1',$_POST['rbvn_randomstart1']);
			update_option('rbvn_randomstart2',$_POST['rbvn_randomstart2']);
			update_option('rbvn_addlinkback',$_POST['rbvn_addlinkback']);
			update_option('rbvn_linkbackislink',$_POST['rbvn_linkbackislink']);
			
			echo '<div class="updated"><p>Cấu hình đã được cập nhật!</p></div>';
		}
	
?>
	<div class="wrap">
	<h2>Robot VN</h2>
	<form method="post" id="rbvn_options">	
	
	<h3>Cấu hình</h3>	
		<table width="100%" cellspacing="2" cellpadding="5" class="editform"> 			
			<tr valign="top"> 
				<td width="30%" scope="row">Trạng thái đăng bài viết:</td>
				<td>
				<select name="rbvn_poststatus" id="rbvn_poststatus">
					<option <?php if (get_option('rbvn_poststatus')=='published') {echo 'selected';} ?>>published</option>
					<option <?php if (get_option('rbvn_poststatus')=='draft') {echo 'selected';} ?>>draft</option>
				</select>
				</td> 
			</tr>
			<tr valign="top"> 
				<td width="30%" scope="row">Reset Post Count:</td>
				<td>
				<select name="rbvn_resetcount" id="rbvn_resetcount">
					<option <?php if (get_option('rbvn_resetcount')=='no') {echo 'selected';} ?>>no</option>
					<option <?php if (get_option('rbvn_resetcount')=='25') {echo 'selected';} ?>>25</option>
					<option <?php if (get_option('rbvn_resetcount')=='50') {echo 'selected';} ?>>50</option>					
					<option <?php if (get_option('rbvn_resetcount')=='75') {echo 'selected';} ?>>75</option>					
					<option <?php if (get_option('rbvn_resetcount')=='100') {echo 'selected';} ?>>100</option>
					<option <?php if (get_option('rbvn_resetcount')=='150') {echo 'selected';} ?>>150</option>
					<option <?php if (get_option('rbvn_resetcount')=='200') {echo 'selected';} ?>>200</option>					
				</select>
				</td> 
			</tr>	
			<tr valign="top"> 
				<td width="30%" scope="row">Tự động random bài:</td> 
				<td>
				giữa <input id="rbvn_randomstart1" size="7" value="<?php echo get_option('rbvn_randomstart1'); ?>" name="rbvn_randomstart1"/> và <input id="rbvn_randomstart2" size="7" value="<?php echo get_option('rbvn_randomstart2'); ?>" name="rbvn_randomstart2"/> giờ tính từ bây giờ.</td> 
			</tr>				
			<tr valign="top"> 
				<td width="30%" scope="row">Tự động tạo tag:</td>
				<td>
				<select name="rbvn_autotag" id="rbvn_autotag">
					<option value="Yes" <?php if (get_option('rbvn_autotag')=='Yes') {echo 'selected';} ?>>Có</option>
					<option value="No" <?php if (get_option('rbvn_autotag')=='No') {echo 'selected';} ?>>Không</option>
				</select>
				</td> 
			</tr>
			<tr valign="top"> 
				<td width="30%" scope="row">Bỏ qua các tag sau:<br/><small>Các từ có ít hơn 3 ký tự sẽ tự động được bỏ qua.</small></td> 
				<td>			
			<textarea name="rbvn_badwords" rows="3" cols="30"><?php echo get_option('rbvn_badwords');?></textarea>	
				</td> 
			</tr>
            <tr valign="top"> 
				<td width="30%" scope="row">Hiển thị nguồn:</td>
				<td>
				<select name="rbvn_addlinkback" id="rbvn_addlinkback">
					<option value="1" <?php if (get_option('rbvn_addlinkback')=='1') {echo 'selected';} ?>>Cho phép</option>
					<option value="0" <?php if (get_option('rbvn_addlinkback')=='0') {echo 'selected';} ?>>Không</option>
				</select>
				</td> 
			</tr>	
            <tr valign="top"> 
				<td width="30%" scope="row">Hiển thị nguồn dạng liên kết:</td>
				<td>
				<select name="rbvn_linkbackislink" id="rbvn_linkbackislink">
					<option value="1" <?php if (get_option('rbvn_linkbackislink')=='1') {echo 'selected';} ?>>Cho phép</option>
					<option value="0" <?php if (get_option('rbvn_linkbackislink')=='0') {echo 'selected';} ?>>Không</option>
				</select>
				</td> 
			</tr>						
		</table>
		<p class="submit"><input type="submit" name="rbvn_save" value="Lưu cấu hình" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="submit" name="rbvn_reset" value="Sử dụng mặc định" /></p>
	</form>	


<h3>Versions</h3>
<?php
rbvn_get_versions();
?>
<h3>Liên kết hữu ích cho bạn</h3>
<p><a href="http://nhanweb.com">Blog</a> - <a href="http://vnwebmaster.com">Hỗ trợ</a></p>

	</div>
<?php		
}
//Function
function rbvn_toplevel() {
	

}
function rbvn_deactivate() {
	wp_clear_scheduled_hook("rbvncheckhook");	
}
add_action( "rbvncheckhook", 'rbvn_check_schedules' );
add_action( "rbvnposthook", 'rbvn_post' );
register_deactivation_hook(__FILE__, 'rbvn_deactivate');
register_activation_hook(__FILE__, 'rbvn_activate');
add_action('admin_menu', 'robotvn_add_menu_admin');
?>