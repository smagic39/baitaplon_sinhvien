<?
$rbvn_vnexpress_ver = "1.0";

function rbvn_vnexpresspost($keyword,$cat,$num,$which, $vnexcat){
	global $wpdb;
	
	debug_log('-VNExpress');
	if (empty($vnexcat)){$vnexcat="";}
	
	
	$links = rbvn_vnex_run_search($keyword, $vnexcat);
	
	if(!empty($links)){
			
			foreach($links as $link){
				$url = "http://vnexpress.net".$link;
				$check = rbvn_preventduplicates($url);
				if($check == null || $check==""){
					$post = rbvn_vnex_get_post($url);
					$title = $post["title"];
					
					$content = $post["intro"] . "<!--more-->".$post["content"];
					
					if(get_option('rbvn_addlinkback')==1){
						if(get_option('rbvn_linkbackislink')!=1)
							$content .= "<p class=\"link_source\">Nguồn bài viết: $url</p>";
						else
							$content .= "<p class=\"link_source\">Nguồn bài viết: <a href=\"{$url}\" target=\"_blank\">$url</a></p>";
					}
					
					$insert = rbvn_insertpost($content,$title, $cat, $url);
					
					if ($insert != "false"){
						debug_log('-VNExpress: Insert successed: '.$url);
						return true;
					}
				}
			}
			return false;
			
	}
}
function rbvn_vnex_get_post($url){
	$html = rbvn_vnex_run_browser($url);

	//Format data
	//Bo doan dau
	$html = preg_replace('#(.*)<div style="overflow:hidden" cpms_content="true">#is','',$html);
	//Bo doan cuoi
	$html = preg_replace('#<div(.*)style="margin-top:5px;margin-bottom:5px;">(.*)#is','',$html);
	
	//Bo div du thua
	$html=str_replace('</div>' ,'' ,$html);
	$html=str_replace(' align=left' ,'' ,$html);
	$html=str_replace('/GL' ,'http://vnexpress.net/GL' ,$html);
	$html=str_replace('/Files' , 'http://www.vnexpress.net/Files' ,$html);
	
	$post["title"] = rbvn_vnex_get_title($html);
	$post["intro"] = rbvn_vnex_get_introduction($html);
	$post["content"] = rbvn_vnex_get_content($html);
	
	return $post;
}
//Tra ve cac link
function rbvn_vnex_run_search($keyword, $cat){
	
		$keyword = urlencode($keyword);

		$url = "http://search.vnexpress.net/news?s=$keyword&butS=T%C3%ACm+ki%E1%BA%BFm&optInput=1&sd=&ed=&ct=$cat&top=all&ds=1&num=1000";
		$html = rbvn_vnex_run_browser($url);
		
		
		//Format data to get search Item
		$html = preg_replace('#<b>Warning(.*)#is','',$html);
		$html = preg_replace('#(.*)encoding="utf-8"\?\>#is','',$html);
		$html = preg_replace('#<div class="frm-page fl">(.*)#is','',$html);
		//Get link
		
		if (!empty($html)){
		 preg_match_all('/<a(.*?) href=\"([^>]+)\"\>(.*?)\<\/a\>/i', $html, $links);
		}

		if(!empty($links[2]))
			return $links[2];
		else
			return FALSE;
		
}
function rbvn_vnex_run_browser($url){
		
	if ( function_exists('curl_init') ) {
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (compatible; Konqueror/4.0; Microsoft Windows) KHTML/4.0.80 (like Gecko)");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 'Content-type: application/x-www-form-urlencoded;charset=UTF-8');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		$response = curl_exec($ch);
		curl_close($ch);
	} else { 				
		$response = @file_get_contents($url);
	}
	
	return $response;
}
function rbvn_vnex_get_title($data){
		$pattern = "/<P class=Title>(.*?)<\/P>/";
		 preg_match($pattern, $data, $matches);
		 return $matches[1];
}
	
function rbvn_vnex_get_introduction($data){
		$pattern = "/<P class=Lead>(.*?)<\/P>/";
		 preg_match($pattern, $data, $matches);
		 return $matches[1];
}
	
function rbvn_vnex_get_content($data){
		$data = preg_replace('#<P class=Title>(.*)</P>#i','',$data);
		$data = preg_replace('#<P class=Lead>(.*)</P>#i','',$data);
		return $data;
}

?>