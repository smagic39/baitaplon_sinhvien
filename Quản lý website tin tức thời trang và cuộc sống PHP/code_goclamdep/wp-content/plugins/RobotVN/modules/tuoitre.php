<?
$rbvn_tuoitre_ver = "1.0";


function rbvn_tuoitrepost($keyword,$cat,$num,$which, $tuoitrecat){
	global $wpdb;
	
	debug_log('-TuoiTre');
	if (empty($tuoitrecat)){$tuoitrecat="";}
	$startat = $num;
	if ($startat == 0) {
		$startpage = 1;
		$sk = 1;
	} else {
		$xz = $startat / 10;
		$startpage = ceil($xz);
		$sk = $startat - ( $startpage -1 ) * 10;
	}
	$l = $startpage;
	$sk = $sk -1;
	
	$url = "http://tuoitre.vn/Tim-kiem/Index.html?channel=$tuoitrecat&page=$l&keyword=".urlencode($keyword);
	
	$html = tuoitre_run_browser($url);
	
	
	$dom = new DOMDocument();
	@$dom->loadHTML($html);
	// Grab Links 
	$xpath = new DOMXPath($dom);
	$count = $xpath->query("//*[@id='divContent']/div/div//div[@class='fontsize14 bold']/a")->length;
	$paras = $xpath->query("//*[@id='divContent']/div/div//div[@class='fontsize14 bold']/a");
	
	
	for($i=0;$i < $count;$i++ ){
		$para = $paras->item($i);
		$url = $para->getAttribute('href');
		$check = rbvn_preventduplicates($url);
		if($check == null || $check==""){
			
			$html = tuoitre_run_browser($url);
			//Convert UTF-8
			$html = mb_convert_encoding($html, 'HTML-ENTITIES', "UTF-8");
			
			
			$urldata = parse_url($url);
			
			if($urldata["host"]!="tuoitre.vn"){
				// parse the html into a DOMDocument 
				$dom = new DOMDocument();
				@$dom->loadHTML($html);
				
		
				// Grab Article Title 
				$xpath = new DOMXPath($dom);
				$paras = $xpath->query("//div[@id='divContent']//p[@class='pTitle']");			
				$para = $paras->item(0);
				$title = strip_tags($para->textContent);
				
				// Grab Article Intro 
				$xpath = new DOMXPath($dom);
				$paras = $xpath->query("//div[@id='divContent']//p[@class='pHead']"); 
				$para = $paras->item(0);		
				$intro = @$dom->saveXml($para);
			
				// Grab Article content
				$xpath = new DOMXPath($dom);
				$paras = $xpath->query("//div[@id='divContent']"); 
				$para = $paras->item(0);
				$content = @$dom->saveXml($para);
				
				//Boc tach du lieu
				$content = preg_replace('#<div id="divContent"(.*)>#i','',$content);
				$content = preg_replace('#<script>(.*)</script>#i','',$content);
				$content = preg_replace('#</div>#i','',$content);
				$content = preg_replace('#<p class="pTitle">(.*)</p>#i','',$content);
				$content = preg_replace('#<p class="pHead">(.*)</p>#i','',$content);
				$content = str_replace('&#13;','',$content);
				$content = str_replace('ImageView.aspx?' ,"http://".$urldata["host"].'/ImageView.aspx?' ,$content);
				$content = trim($content,'&#13;');
				if(get_option('rbvn_addlinkback')==1){
					if(get_option('rbvn_linkbackislink')!=1)
						$content .= "<p class=\"link_source\">Nguồn bài viết: $url</p>";
					else
						$content .= "<p class=\"link_source\">Nguồn bài viết: <a href=\"{$url}\" target=\"_blank\">$url</a></p>";
				}
				$article = $intro . "<!--more-->".$content;
			}else{
				//Bo doan dau
				$html = preg_replace('#(.*)<!-- MAIN CONTENT -->#is','',$html);
				$html = preg_replace('#<!-- CAC TIN LIEN QUAN -->(.*)#is','',$html);
				
				$data = $html;
				
				$pattern = "/<P class=pTitle>(.*?)<\/P>/";
				 preg_match($pattern, $data, $matches);
				 $title = $matches[1];
				 
				$pattern = "/<P class=pSuperTitle>(.*?)<\/P>/";
				 preg_match($pattern, $data, $matches);
				 $PHead = $matches[1];
				 
				$pattern = "/<P class=pHead>(.*?)<\/P>/";
				preg_match($pattern, $data, $matches);
				$intro = $matches[1];
				
				$content = $data;
				
				$content = preg_replace('#<P class=pTitle>(.*?)</P>#i','',$content);
				$content = preg_replace('#<P class=pSuperTitle>(.*?)</P>#i','',$content);
				$content = preg_replace('#<P class=pHead>(.*?)</P>#i','',$content);
				$content = preg_replace('#<div style="clear: both; margin: 0px; padding: 0px 8px" id="contentfiximg">#i','',$content);
				$content = preg_replace('#</div>#i','',$content);
				
				if(get_option('rbvn_addlinkback')!=0){
					if(get_option('rbvn_linkbackislink')!=1)
						$content .= "<p class=\"link_source\">Nguồn bài viết: $url</p>";
					else
						$content .= "<p class=\"link_source\">Nguồn bài viết: <a href=\"{$url}\" target=\"_blank\">$url</a></p>";
				}
				
				if($PHead!="")
					$article = "<p<strong>$PHead</strong><p>";
					
			 	$article .= $intro . "<!--more-->".$content;
			}
			
		
			$insert = rbvn_insertpost($article,$title, $cat, $url);
			
			if ($insert != "false"){
				debug_log('-TuoiTre: Insert successed: '.$url);
				return true;
			}
		}
	}
	
	return false;
}
//Return content
function tuoitre_run_browser($url){
	if ( function_exists('curl_init') ) {
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (compatible; Konqueror/4.0; Microsoft Windows) KHTML/4.0.80 (like Gecko)");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 'Content-type: application/x-www-form-urlencoded;charset=UTF-8');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		$response = curl_exec($ch);
		curl_close($ch);
	} else { 				
		$response = @file_get_contents($url);
	}
	
	return $response;
}
?>