<?php
	ob_start ();
	
	define("DS", DIRECTORY_SEPARATOR);
	set_include_path("include_path");
	
	require_once "library".DS."@library.php";
	require_once SRC_BACKEND.DS."daos".DS."@daos.php";
	
	/**
	 * @note:	function write logs
	 * 
	 * @param 	String $msg
	 * @author 	ChienKV
	 * @version 1.0
	 */
	function logErrorToFile($msg) {
		//if ($fp = @fopen ( "services/logs/myBicErrors.txt", "ab+" )) @fwrite ( $fp, $msg . "\r\n" );
	}
	
	$file 		= explode ( "?", basename ( $_REQUEST ["action"] ) );
	$php_class 	= $file [0];
	$file 		= $file [0] . ".php";

	if (is_file( "services/". $file )) {
		include_once ("services/" . $file);
		$xmlhttp_response = new $php_class ( $_REQUEST );
		if ($xmlhttp_response->is_authorized ()) $response = $xmlhttp_response->return_response ();
		else {
			//logErrorToFile ("Authorization Failed- IP:". $_SERVER["REMOTE_ADDR"] ." QueryVars:". serialize($_REQUEST));
			//echo "ajax_msg_failed|notauth";
		}
		
		if (isset($_REQUEST ["json"]) && $_REQUEST["json"] === "false") echo $response;
		else {
			require_once ("services/json.php");
			$JSON = new Services_JSON ( );
			echo $JSON->encode ( $response );
		}
	} else {
		//logErrorToFile ("No PHP Class Found for Action: ". $_REQUEST["action"] .". Failed- IP:". $_SERVER["REMOTE_ADDR"]);
		echo "ajax_msg_failed|No Action Found that matches query string: ". $_REQUEST["action"] .". This means the server cannot find your PHP class file, check your paths";
	}
?>