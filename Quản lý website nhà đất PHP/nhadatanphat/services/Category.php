<?php
	require_once BACKEND_PAGE_PATH.DS."include".DS."categories.php";
	
	class Category {
		var $queryVars;
		var $resp = array ( );
		
		function Category($queryVars) {
			$this->queryVars = $queryVars;
			switch ($queryVars["act"]) {
				case "SelectParent" :
					$id = (int)$queryVars["category_id"];
					$this->SelectParent ( $id );
					break;
				default :
					return null;
			}
		}
	
		function SelectParent($id) {
			if (empty ($id)) $this->resp ["result"] = "You are select category";
			else {
				$ChildDao	= new ChildDao();
				$categories = new Categories();
				$parentList = $categories->ShowCategories($ChildDao, $id, 0, null, Status::active);
	
				$this->resp ["result"] = "1";
				$i = 0;
				foreach ($parentList as $value) {
					$this->resp ["data"]["id"][$i] 		= $value["child_id"];
					$this->resp ["data"]["title"][$i] 	= stripslashes($value["child_name"]);
					$i ++;
				}
			}
		}
		
		/**
		 * Method to return the status of the AJAX transaction
		 *
		 * @return  string A string of raw HTML fetched from the Server
		 */
		function return_response() {
			return $this->resp;
		}
		
		function is_authorized() {
			return true;
		}
	}
?>