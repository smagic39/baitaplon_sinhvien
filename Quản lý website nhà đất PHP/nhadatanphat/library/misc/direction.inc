<?php
/*****************************************************************
 * @project_name: localframe
 * @package: package_name
 * @file_name: direction.inc
 * @descr:
 * 
 * @author 	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 *****************************************************************/
class Direction {
	const none 			= 100;
	const east 			= 1;
	const west			= 2;
	const south			= 3;
	const north			= 4;
	const northeastern	= 5;
	const southeast		= 6;
	const northwest		= 7;
	const southwest		= 8;
	
	const direction01		= 9;//Nam dong nam
	const direction02		= 10;//Nam Tay Nam
	const direction03		= 11;//Bac dong bac
	const direction04		= 12;//Bac tay bac

	

	/**
	 * @note:	function get all ball game
	 * @return	List	: Direction
	 * @version 1.0
	 */
	public static function getList($textlang = null) {
		$result = array(
						self::east 			=> array("value"=>self::east, 			"text_en"=>"East", 				"text_vn"=>"Đông"),
						self::west 			=> array("value"=>self::west, 			"text_en"=>"West", 				"text_vn"=>"Tây"),
						self::north 		=> array("value"=>self::north,			"text_en"=>"North", 			"text_vn"=>"Bắc"),
						self::south 		=> array("value"=>self::south, 			"text_en"=>"South", 			"text_vn"=>"Nam"),
						self::northeastern 	=> array("value"=>self::northeastern,	"text_en"=>"Northeastern", 		"text_vn"=>"Đông bắc"),
						self::southeast 	=> array("value"=>self::southeast,		"text_en"=>"South-east", 		"text_vn"=>"Đông nam"),
						self::northwest 	=> array("value"=>self::northwest,		"text_en"=>"Northwest", 		"text_vn"=>"Tây bắc"),
						self::southwest 	=> array("value"=>self::southwest,		"text_en"=>"Southwest", 		"text_vn"=>"Tây nam"),
						self::direction01	=> array("value"=>self::direction01, 	"text_en"=>"direction01", 		"text_vn"=>"Nam đông nam"),
						self::direction02	=> array("value"=>self::direction02, 	"text_en"=>"direction02", 		"text_vn"=>"Nam tây nam"),
						self::direction03	=> array("value"=>self::direction03,	"text_en"=>"direction03", 		"text_vn"=>"Bắc đông bắc"),
						self::direction04	=> array("value"=>self::direction04,	"text_en"=>"direction04", 		"text_vn"=>"Bắc tây bắc"),
						self::none 			=> array("value"=>self::none, 			"text_en"=>"No direction", 		"text_vn"=>"Chưa có")
		);

		$returnslist = array();
		if (empty($textlang)) $text = "text_en";
		else $text = "text_vn";
		foreach ($result as $key => $values) {
			$returnslist[$key]["value"] = $values["value"];
			$returnslist[$key]["text"] 	= $values[$text];
		}
		return $returnslist;
	}

	/**
	 * @note: 	function get text of Direction
	 * @param	Int		: value
	 * @param	Int		: textlang	[0: English, 1: Vietname]
	 * 
	 * @return	String	: Text of Direction value
	 * @version 1.0
	 */
	public function getToText($value, $textlang = null) {
		if (empty($textlang)) $text = "text_en";
		else $text = "text_vn";

		$returnText = "";
		$list = self::getList();
		 
		switch($value){
			case self::none;
				$returnText = $list[self::none][$text];
			case self::east;
				$returnText = $list[self::east][$text];
				break;
			case self::west;
				$returnText = $list[self::west][$text];
				break;
			case self::south;
				$returnText = $list[self::south][$text];
				break;
			case self::north;
				$returnText = $list[self::north][$text];
				break;
			case self::northeastern;
				$returnText = $list[self::northeastern][$text];
				break;
			case self::southeast;
				$returnText = $list[self::southeast][$text];
				break;
			case self::northwest;
				$returnText = $list[self::northwest][$text];
				break;
			case self::southwest;
				$returnText = $list[self::southwest][$text];
				break;
			case self::direction01;
				$returnText = $list[self::direction01][$text];
				break;
			case self::direction02;
				$returnText = $list[self::direction02][$text];
				break;
			case self::direction03;
				$returnText = $list[self::direction03][$text];
				break;
			case self::direction04;
				$returnText = $list[self::direction04][$text];
				break;
			default:
				break;
		}
		return $returnText;
	}
}
?>
