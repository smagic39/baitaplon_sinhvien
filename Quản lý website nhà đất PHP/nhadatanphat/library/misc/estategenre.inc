<?php
/*****************************************************************
 * @project_name: localframe
 * @package: package_name
 * @file_name: newsgenre.inc
 * @descr:
 * 
 * @author 	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 *****************************************************************/
class EstateGenre {
	const estatedef		= 0;
	const estatevip		= 1;
	const estateenhance	= 2;
//	const estatehot		= 3;

	/**
	 * @note:	function get all NewsGenre
	 * @return	List	: NewsGenre
	 * @version 1.0
	 */
	public static function getList($textlang = null) {
		$result = array(
						self::estatedef 	=> array("value"=>'0', "text_en"=>"Estate default", "text_vn"=>"Tin thường"),
						self::estatevip 	=> array("value"=>'1', "text_en"=>"Estate VIP", 	"text_vn"=>"Tin VIP"),
						self::estateenhance => array("value"=>'2', "text_en"=>"Estate enhance", 	"text_vn"=>"Tin nổi bật"),
//						self::estatehot		=> array("value"=>'3', "text_en"=>"Estate HOt", 	"text_vn"=>"Bán gấp")
		);

		$returnslist = array();
		if (empty($textlang)) $text = "text_en";
		else $text = "text_vn";
		foreach ($result as $key => $values) {
			$returnslist[$key]["value"] = $values["value"];
			$returnslist[$key]["text"] 	= $values[$text];
		}
		return $returnslist;
	}

	/**
	 * @note: 	function get text of NewsGenre
	 * @param	Int		: value
	 * @return	String	: Text of NewsGenre value
	 * @version 1.0
	 */
	public function getToText($value, $textlang = null) {
		if (empty($textlang)) 
			$text = "text_en";
		else 
			$text = "text_vn";

		$returnText = "";
		$list = self::getList();
		 
		switch($value){
			case self::estatedef;
				$returnText = $list[self::estatedef][$text];
				break;
			case self::estatevip;
				$returnText = $list[self::estatevip][$text];
				break;
//			case self::estateenhance;
//				$returnText = $list[self::estateenhance][$text];
//				break;
				
			default:
				break;
		}

		return $returnText;
	}
}
?>
