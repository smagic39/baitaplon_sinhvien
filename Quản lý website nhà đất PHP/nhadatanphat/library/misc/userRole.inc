<?php
/*****************************************************************
 * @project_name: estate
 * @package: package_name
 * @file_name: userRole.inc
 * @descr:
 * 
 * @author Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 *****************************************************************/
class UserRole {
	const admin 	= 0;
	const admin1	= 1;
	const admin2	= 2;

	/**
	 * @note:	function get all role
	 * @return	List	: SupportGenre
	 * @version 1.0
	 */
	public static function getList($textlang = null) {
		$result = array(
						self::admin => array("value"=>self::admin, "text_en"=>"System Admin", "text_vn"=>"Quản trị hệ thống"),
						self::admin1 => array("value"=>self::admin1, "text_en"=>"Estate admin", "text_vn"=>"Admin tin rao"),
						self::admin2 => array("value"=>self::admin2, "text_en"=>"News admin", "text_vn"=>"Admin tin tức")
		);

		$returnslist = array();
		if (empty($textlang)) $text = "text_en";
		else $text = "text_vn";
		foreach ($result as $key => $values) {
			$returnslist[$key]["value"] = $values["value"];
			$returnslist[$key]["text"] 	= $values[$text];
		}
		return $returnslist;
	}

	/**
	 * @note: 	function get text of vertical value
	 * @param	Int		: value
	 * @param	Int		: textlang	[0: English, 1: Vietname]
	 * 
	 * @return	String	: Text of vertical value
	 * @version 1.0
	 */
	public function getToText($value, $textlang = null) {
		if (empty($textlang)) $text = "text_en";
		else $text = "text_vn";

		$returnText = "";
		$list = self::getList();
		 
		switch($value){
			case self::admin;
				$returnText = $list[self::admin][$text];
				break;
			case self::admin1;
				$returnText = $list[self::admin1][$text];
				break;
			case self::admin2;
				$returnText = $list[self::admin2][$text];
				break;
			default:
				break;
		}
		return $returnText;
	}
	
	public static function getRoleList(){
		$returnVal = array();
		$returnVal[self::admin] = array();//empty array allow user with full control.
		$returnVal[self::admin1] = array("login", "logout", "index", "auth/detail", 
										"auth/changepass", "estate/list", "estate/regist", 
										"estate/post", "inform/list", "inform/regist");//with news only

		$returnVal[self::admin2] = array("login", "logout", "index", "auth/detail", 
										"auth/changepass", "news/list", "news/regist", 
										"interior/list", "interior/regist", 
										"interior/categories_list", "interior/categories_edit",
										"inform/list", "inform/regist"
									);//with estate news and interior...

		return $returnVal;
	}
}
?>
