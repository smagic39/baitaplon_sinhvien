<?php
/*****************************************************************
 * @project_name: localframe
 * @package: package_name
 * @file_name: status.inc
 * @descr:
 * 
 * @author 	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 *****************************************************************/
class Status {
	const active 	= 1;
	const unactive	= 0;
	
	/**
	 * @note:	function get all status
	 * @return	List	: Status
	 * @version 1.0
	 */
	public static function getList($textlang = null) {
		$result = array(
						self::active 	=> array("value"=>self::active, 	"text_en"=>"Is active",	"text_vn"=>"Hiển thị"),
						self::unactive 	=> array("value"=>self::unactive, 	"text_en"=>"Unactive",	"text_vn"=>"Không hiển thị")
		);

		$returnslist = array();
		if (empty($textlang)) $text = "text_en";
		else $text = "text_vn";
		foreach ($result as $key => $values) {
			$returnslist[$key]["value"] = $values["value"];
			$returnslist[$key]["text"] 	= $values[$text];
		}
		return $returnslist;
	}
	
	/**
	 * @note: 	function get text of status value
	 * @param	Int		: value
	 * @param	Int		: textlang	[0: English, 1: Vietname]
	 * 
	 * @return	String	: Text of status value
	 * @version 1.0
	 */
	public function getToText($value, $textlang = null) {
		if (empty($textlang)) $text = "text_en";
		else $text = "text_vn";

		$returnText = "";
		$list = self::getList();
		 
		switch($value){
			case self::active;
				$returnText = $list[self::active][$text];
				break;
			case self::unactive;
				$returnText = $list[self::unactive][$text];
				break;
			default:
				break;
		}
		return $returnText;
	}
}
?>
