<?php
/*****************************************************************
 * @project_name: localframe
 * @package: package_name
 * @file_name: position.inc
 * @descr:
 * 
 * @author 	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 *****************************************************************/
class Position {
	const positionone 	= 1;
	const positiontwo	= 2;
	const positionthree	= 3;
	const positionfour	= 4;
	const positionfive	= 5;
	const positionsix	= 6;
	const positionseven	= 7;
	const positioneight	= 8;

	/**
	 * @note:	function get all ball game
	 * @return	List	: BallGame
	 * @version 1.0
	 */
	public static function getList($textlang = null) {
		$result = array(
						self::positionone 	=> array("value"=>self::positionone, 	"text_en"=>"Position one",	
																						"text_vn"=>"Trên cùng - Ở giữa"),
																						
						self::positiontwo 	=> array("value"=>self::positiontwo, 	"text_en"=>"Position two",	
																						"text_vn"=>"Trên cùng - Bên phải"),
						
						self::positionseven => array("value"=>self::positionseven, 	"text_en"=>"Position seven - Big banner",	
																						"text_vn"=>"Big Banner"),
						
																						
						self::positionthree => array("value"=>self::positionthree, 	"text_en"=>"Position three",
																						"text_vn"=>"Menu bên trái"),
																						
						self::positionfour 	=> array("value"=>self::positionfour, 	"text_en"=>"Position four",	
																						"text_vn"=>"Menu bên phải"),
																						
						self::positionfive 	=> array("value"=>self::positionfive, 	"text_en"=>"Position five",	
																						"text_vn"=>"Ở giữa trang"),
																						
						self::positionsix 	=> array("value"=>self::positionsix, 	"text_en"=>"Banner Below News detail",
																						"text_vn"=>"Ở giữa (trang chi tiết)"),/*
																						
						self::positionseven 	=> array("value"=>self::positionseven, 	"text_en"=>"Banner center",
																							"text_vn"=>"Banner giữa"),
																							
						self::positioneight 	=> array("value"=>self::positioneight, 	"text_en"=>"Banner right",
																							"text_vn"=>"Banner phải")*/
		);

		$returnslist = array();
		if (empty($textlang)) $text = "text_en";
		else $text = "text_vn";
		foreach ($result as $key => $values) {
			$returnslist[$key]["value"] = $values["value"];
			$returnslist[$key]["text"] 	= $values[$text];
		}
		return $returnslist;
	}

	/**
	 * @note: 	function get text of genre game
	 * @param	Int		: value
	 * @return	String	: Text of genre game value
	 * @version 1.0
	 */
	public function getToText($value, $textlang = null) {
		if (empty($textlang)) $text = "text_en";
		else $text = "text_vn";
		$returnValue = "";
		$list = self::getList();
		 
		switch($value){
			case self::positionone;
				$returnValue = $list[self::positionone][$text];
				break;
			case self::positiontwo;
				$returnValue = $list[self::positiontwo][$text];
				break;
			case self::positionthree;
				$returnValue = $list[self::positionthree][$text];
				break;
			case self::positionfour;
				$returnValue = $list[self::positionfour][$text];
				break;
			case self::positionfive;
				$returnValue = $list[self::positionfive][$text];
				break;
			case self::positionsix;
				$returnValue = $list[self::positionsix][$text];
				break;
			case self::positionseven;
				$returnValue = $list[self::positionseven][$text];
				break;
			case self::positioneight;
				$returnValue = $list[self::positioneight][$text];
				break;
			default:
				break;
		}
		return $returnValue;
	}
}
?>
