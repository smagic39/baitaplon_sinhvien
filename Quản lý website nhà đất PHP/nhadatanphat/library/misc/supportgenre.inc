<?php
/*****************************************************************
 * @project_name: localframe
 * @package: package_name
 * @file_name: gamegenre.inc
 * @descr:
 * 
 * @author 	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 *****************************************************************/
class SupportGenre {
	const yahoo = 0;
	const skype	= 1;

	/**
	 * @note:	function get all genre
	 * @return	List	: SupportGenre
	 * @version 1.0
	 */
	public static function getList($textlang = null) {
		$result = array(
						self::yahoo => array("value"=>self::yahoo, "text_en"=>"Yahoo Nick", "text_vn"=>"Tài khoản Yahoo"),
						self::skype => array("value"=>self::skype, "text_en"=>"Skype Nick", "text_vn"=>"Tài khoản Skype")
		);

		$returnslist = array();
		if (empty($textlang)) $text = "text_en";
		else $text = "text_vn";
		foreach ($result as $key => $values) {
			$returnslist[$key]["value"] = $values["value"];
			$returnslist[$key]["text"] 	= $values[$text];
		}
		return $returnslist;
	}

	/**
	 * @note: 	function get text of genre
	 * @param	Int		: value
	 * @return	String	: Text of genre value
	 * @version 1.0
	 */
	public function getToText($value, $textlang = null) {
		if (empty($textlang)) $text = "text_en";
		else $text = "text_vn";

		$returnText = "";
		$list = self::getList();

		switch($value){
			case self::yahoo;
				$returnText = $list[self::yahoo][$text];
				break;
			case self::skype;
				$returnText = $list[self::skype][$text];
				break;
			default:
				break;
		}
		return $returnText;
	}
}
?>
