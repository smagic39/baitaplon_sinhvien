<?php
/*
 * Smarty plugin
 *
-------------------------------------------------------------
 * File:     modifier.money_format.php
 * Type:     modifier
 * Name:     money_format
 * Version:  1.0
 * Date:     Feb 8th, 2003
 * Purpose:  pass value to PHP money_format() and return result
 * Install:  Drop into the plugin directory.
 * Author:   Michael L. Fladischer <mfladischer@abis.co.at>
 *
-------------------------------------------------------------
 */
function smarty_modifier_money_format($string, $decimals=2, $dec_point=".", $thousands_sep=".")
{
if (is_numeric($string)) // check if it's a number
{
return number_format($string, $decimals, $dec_point, $thousands_sep);
}
else
{
return $string;
}
} 
/* vim: set expandtab: */

?>
