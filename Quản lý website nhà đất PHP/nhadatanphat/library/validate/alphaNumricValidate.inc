<?php
/**
 * @note:	
 * @author	ChienKV - khuongchien@gmail.com
 * @version 1.0
 */
if(!defined("ALPHANUMRIC_VALIDATE_INC")) {
	define("ALPHANUMRIC_VALIDATE_INC", 1);
	class alphaNumricValidate extends normalizeValidate {

		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		function __construct() {}
	
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		public function execute($value, $setting) {
			if(isset($setting['charonly']) && $setting['charonly'] == 1) {
				$boolean = $this->normalize($value,"^[0-9a-zA-Z]+$");
			} else {
				$boolean = $this->normalize($value,"^[0-9a-zA-Z_.?/-]+$");
			}
			
			if($boolean) {
				return;
			} else {
				if(isset($setting['normalizeerrormsg'])) {
					$error = $setting['normalizeerrormsg'];
				} else {
					$error = "%name%";
				}
				return $error;
			}
		}
	}
}
?>