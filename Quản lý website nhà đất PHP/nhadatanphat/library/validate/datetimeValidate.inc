<?php
/**
 * @note:	
 * @author	ChienKV - khuongchien@gmail.com
 * @version 1.0
 */
if(!defined("DATETIME_VALIDATE_INC")){
define("DATETIME_VALIDATE_INC",1);
	
	class datetimeValidate {
		
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		function __construct()
		{
		}
	
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		public function execute($value,$setting)
		{
			$date = DateUtils::splitDatetime($value);
			if(!checkdate($date['month'],$date['day'],$date['year'])){
				if($setting['dateerrormsg']){
					$errormsg = $setting['dateerrormsg'];
				}else{
					$errormsg = "%name%";
				}
				return $errormsg;
			}

			if($setting['gettime']){
				if($date['hour'] < 0 || 23 < $date['hour']){
					if($setting['timeerrormsg']){
						$errormsg = $setting['timeerrormsg'];
					}else{
						$errormsg = "%name%";
					}
					return $errormsg;
				}
				if($date['minute'] < 0 || 59 < $date['minute'] || $date['second'] < 0 || 59 < $date['second']){
					if($setting['timeerrormsg']){
						$errormsg = $setting['timeerrormsg'];
					}else{
						$errormsg = "%name%";
					}
					return $errormsg;
				}

				if($setting['area'] == "future"){
					if(mktime($date['hour'],$date['minute'],$date['second'],$date['month'],$date['day'],$date['year']) < time()){
						if($setting['timeareaerrormsg']){
							$errormsg = $setting['timeareaerrormsg'];
						}else{
							$errormsg = "%name%";
						}
						return $errormsg;
					}
				} else if($setting['area'] == "past"){
					if(mktime($date['hour'],$date['minute'],$date['second'],$date['month'],$date['day'],$date['year']) > time() || $date['year'] > date("Y")){
						if($setting['timeareaerrormsg']){
							$errormsg = $setting['timeareaerrormsg'];
						}else{
							$errormsg = "%name%";
						}
						return $errormsg;
					}
				}
			}
			return;
		}
	}
}
?>