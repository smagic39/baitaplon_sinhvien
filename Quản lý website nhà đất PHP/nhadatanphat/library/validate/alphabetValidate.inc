<?php
/**
 * @note:	
 * @author	ChienKV - khuongchien@gmail.com
 * @version 1.0
 */
if(!defined("ALPHABET_VALIDATE_INC")){
	define("ALPHABET_VALIDATE_INC",1);
	
	class alphabetValidate extends normalizeValidate {
		
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		function __construct()
		{
			
		}
	
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		public function execute($value,$setting)
		{
			$boolean = $this->normalize($value,"^[a-zA-Z_.?/-]+$");
			if($boolean){
				return;
			}else{
				if($setting['normalizeerrormsg']){
					$error = $setting['normalizeerrormsg'];
				}else{
					$error = "%name%";
				}
				return $error;
			}
		}
	}
}
?>