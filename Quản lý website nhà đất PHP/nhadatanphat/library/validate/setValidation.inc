<?php
if(!defined("SET_VALIDATION_INC")){

define("SET_VALIDATION_INC",1);

class setValidation{
	
	private $iniFileName;
	private $status = 0;	
	private $counter = 0;
	public $valueNum;
	public $returnType = "message";
	public $setting = array();
    public $fp = "";

	function __construct($identify,$iniFileName = "")
	{
		$this->iniFileName = $iniFileName;
		if(!file_exists($this->iniFileName)){
			trigger_error("There is not '{$this->iniFileName}' validation setting File.\n",E_USER_ERROR );
			exit(0);
		}
		
		$this->open();
		$this->read($identify);
		$this->close();
	}

	public function getSetting()
	{
		return $this->setting;
	}

	/**
	 * @note:	
	 * @author	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	private function open()
	{
		if($this->fp){
			return 0;
		}
		$this->fp = fopen($this->iniFileName,"r");
	}

	
	private function read($identify)
	{
		
		$whileRoopBreak = 0;
		$i = 0;
		while ( !feof ($this->fp) ){
			$i++;
   			$line = fgets($this->fp, 4096);
   			$line = $this->escapeComment($line);
   			$line = $this->removeTab($line);
   			if(trim($line) == ""){
   				continue;
   			}
   			switch($this->status){
   			case 0:
   				
   				if(!ereg("\[",$line)){
   					break;
   				}
   				
   				$tmp = $tmp2 = array();
   				$tmp  = split("\[",$line);
   				$tmp2 = split("\]",$tmp[1]);
   				if(trim($tmp2[0]) == $identify){
	   				$this->status = 1;
   				}
   				break;
   			case 1:
   				if(ereg("\[",$line)){
   					trigger_error("Validation Setting Error. The number of arguments is not set. In file: {$this->iniFileName} on Line {$i} ",E_USER_ERROR );
   					exit(0);
   				}
   				if(ereg("return",$line)){
	   				$tmp = $tmp2 = array();
	   				$tmp = split("=", $line);
   					$this->returnType = trim($this->quoteEscape($tmp[1]));
   					break;
   				}
   				if(ereg("=",$line)){
	   				$tmp = $tmp2 = array();
   					$tmp = split("=", $line);
   					if(strtolower(trim($tmp[0])) == "argc"){
   						$this->valueNum = trim($tmp[1]);
		 				if(!is_numeric($this->valueNum)){
							trigger_error("Validation Setting Error.The number of arguments is not int types. In file: {$this->iniFileName} on Line {$i} ",E_USER_ERROR );
						}
						$this->status = 2;
   					}
   				}	
   				break;
   			case 2:
   				if(ereg("return",$line)){
	   				$tmp = $tmp2 = array();
	   				$tmp = split("=", $line);
   					$this->returnType = trim($this->quoteEscape($tmp[1]));
   					break;
   				}
				if(ereg("BEGIN;",$line)){
	   				$this->status = 3;
	   				break;
				}
   				if(ereg("\[",$line)){
   					trigger_error("Validation Setting Error.BEGIN; is not found. In file: {$this->iniFileName} on Line {$i} ",E_USER_ERROR );
   					exit(0);
   				}
				break;
			case 3:
				if(ereg("END;",$line)){
					$this->counter++;
					if($this->counter == $this->valueNum){
						$whileRoopBreak = 1;
						break;
					}
	   				$this->status = 2;
	   				break;
				}
   				if(ereg("\[",$line)){
   					
   				}
   				$tmp = $tmp2 = array();
   				$tmp = split("=", $line);
   				$this->setting[$this->counter][strtolower(trim($tmp[0]))] = trim($this->quoteEscape($tmp[1]));
				break;
   			}
   			if($whileRoopBreak == 1){
   				break;
   			}
		}
		if($whileRoopBreak == 0){
			trigger_error("Validation Setting Error.Specified identifier information was not able to be acquired. In file: {$this->iniFileName} on Line {$i} ",E_USER_ERROR );
		}
	}
	
	/*
	
	 */
	private function removeTab($line)
	{
		$line = ereg_replace("\t","",$line);
		return $line;
	}
	
	/*
	
	 */
	private function escapeComment($line)
	{
		$tmp = split("#",$line);
		return $tmp[0];
	}

	/*
	
	 */
	private function close()
	{
		if($this->fp){
			fclose($this->fp);
		}
	}

	private function quoteEscape($val)
	{
	
		$val = ereg_replace("\"","",$val);
	
		$val = ereg_replace("\'","",$val);
		return $val;
	}

}

}
?>