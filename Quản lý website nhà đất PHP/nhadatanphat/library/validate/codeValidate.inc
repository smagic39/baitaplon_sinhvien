<?php
/**
 * @note:	
 * @author	ChienKV - khuongchien@gmail.com
 * @version 1.0
		 */
if(!defined("ALPHABET_VALIDATE_INC")){
	define("ALPHABET_VALIDATE_INC",1);
	
	class codeValidate extends normalizeValidate {
		
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		function __construct()
		{
			
		}
	
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		public function execute($value,$setting)
		{
			$replace=array("(", ")"," ","_","-","#",".","=");
			$value=str_replace($replace,"", $value);
			$value=str_replace("/","", $value);
			$value=str_replace("\\","", $value);
			if(!ereg("^[0-9a-zA-Z]*$", $value)){
				$error=$setting['normalizeerrormsg'];
				return $error;
			}
			return ;
		}
	}
}
?>