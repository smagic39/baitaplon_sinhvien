<?php
/**
 * @note:	
 * @author	ChienKV - khuongchien@gmail.com
 * @version 1.0
 */
if(!defined("NUMBER_VALIDATE_INC")){
	define("NUMBER_VALIDATE_INC",1);
	
	/**
	 * @note:	
	 * @author	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	class numberValidate extends normalizeValidate {
		
		private $val;
		
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		function __construct()
		{
			$this->val['max'] = "NotSet";
			$this->val['min'] = "NotSet";
		}
	
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		public function execute($value,$setting)
		{
			if(!is_numeric($value)){
				if(isset($setting['normalizeerrormsg']) && $setting['normalizeerrormsg']!=""){
					return $setting['normalizeerrormsg'];
				}else{
					return "%name%";
				}
			}
			$this->val['max'] = $setting['valmax'];
			$this->val['min'] = $setting['valmin'];
			$this->nullok = $setting['nullok'];
	
			if(is_numeric($this->val['min']) || is_numeric($this->val['max'])){
				if(!is_numeric($this->val['max'])){
					if($value < $this->val['min']){
						$msg.= "%name%";
					}
				}else if(!is_numeric($this->val['min'])){
					if($value > $this->val['max']){
						$msg.= "%name%";
					}
				}else{
					if($value < $this->val['min'] && $value > $this->val['max']){
						$msg.= "%name%";
					}
				}
			}		
		}
	}
}
?>