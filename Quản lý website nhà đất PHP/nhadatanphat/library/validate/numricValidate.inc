<?php
/**
 * @note:	
 * @author	ChienKV - khuongchien@gmail.com
 * @version 1.0
 */
if(!defined("NUMRIC_VALIDATE_INC")){
	define("NUMRIC_VALIDATE_INC",1);
	
	class numricValidate extends normalizeValidate {
		
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		function __construct()
		{
			
		}
	
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		public function execute($value,$setting)
		{
			if($setting['charonly'] == 1) {
				$boolean = $this->normalize($value,"^[0-9]+$");
			} else {
				$boolean = $this->normalize($value,"^[0-9]+$");
			}
			if($boolean){
				return;
			}else{
				if(isset($setting['numbererrormsg']) && $setting['numbererrormsg']!=""){
					$error = $setting['numbererrormsg'];
				}else{
					$error = "%name%�͔��p�p���œ�͂��Ă��������B                 ";
				}
				return $error;
			}
		}
	}
}
?>