<?php
/**
 * @note:	
 * @author	ChienKV - khuongchien@gmail.com
 * @version 1.0
 */
class telValidate extends normalizeValidate {

	/**
	 * @note:	
	 * @author	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	function __construct()
	{
	}

	/**
	 * @note:	
	 * @author	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	public function execute($value,$setting)
	{
		$boolean = $this->normalize($value,"^[0-9()-_' '+.]+$");
		if($boolean) return;
		if(isset($setting['normalizeerrormsg']) && $setting['normalizeerrormsg']!="")
			$error=$setting['normalizeerrormsg'];
		else 
			$error="%name% không hợp lệ";
		return $error;
	}
}
?>