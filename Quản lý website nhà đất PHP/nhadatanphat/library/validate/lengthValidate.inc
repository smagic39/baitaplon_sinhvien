<?php
/**
 * @note:	
 * @author	ChienKV - khuongchien@gmail.com
 * @version 1.0
 */
if(!defined("LENGTH_VALIDATE_INC")){
	define("LENGTH_VALIDATE_INC",1);
	
	class lengthValidate{
	
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		function __construct()
		{
			
		}
	
		/**
		 * @note:	
		 * @author	ChienKV - khuongchien@gmail.com
		 * @version 1.0
		 */
		public static function execute($value, $setting)
		{
			if(isset($setting['lenmin']) && $setting['lenmin']!=""){
				$len['min'] = $setting['lenmin'];
			}else{
				$len['min'] = 0;
			}
			if(isset($setting['lenmax']) && $setting['lenmax']!=""){
				$len['max'] = $setting['lenmax'];
			}else{
				$len['max'] = 0;
			}
			
			if(isset($setting['mb_len'])&&($setting['mb_len'] == 1)){
				$length = mb_strlen($value);
			}else{
				$length = strlen($value);
			}
			if($length == 0 && $setting['nullok'] == 1){
				return;
			}
			if( $len['max']>0 && $len['min']>0 ){
				if($len['max'] == $len['min'] ){
					if($length != $len['max'] ){
						if($setting['lenerrormsg']){
							$errormsg = $setting['lenerrormsg'];
						}else{
							$errormsg = "%name%";
						}
						return $errormsg;
					}
				} else {
					if( $length > $len['max'] || $length < $len['min'] ){
						if( (isset($setting['lenerrormsg'])) && ($setting['lenerrormsg'])){
							$errormsg = $setting['lenerrormsg'];
						}else{
							$errormsg = "%name%";
						}
						return $errormsg;
					}
				}
			} else if( $len['min']>0 ){
				if($length < $len['min']){
					if(isset($setting['lenerrormsg']) && $setting['lenerrormsg']!=""){
						$errormsg = $setting['lenerrormsg'];
					}else{
						$errormsg = "%name%";
					}
					return $errormsg;
				}
			} else if( $len['max']>0 ){
				if($length > $len['max']){
					if(isset($setting['lenerrormsg']) && $setting['lenerrormsg']!=""){
						$errormsg = $setting['lenerrormsg'];
					}else{
						$errormsg = "%name%%lenmax%";
					}
					return $errormsg;
				}
			}
			return;
		}
	}
}
?>