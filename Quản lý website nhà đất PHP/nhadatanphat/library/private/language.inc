<?php
/**
 * @note:	file define array language
 * @author	ChienKV - khuongchien@gmail.com
 * @version 1.0
 */
if (!defined("ARRAY_LANG_INC") ) {
	Define("ARRAY_LANG_INC", 	"TRUE");
	/**
	 * @note:	array list languages
	 * @author	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	$languagelist 		= array(
								1=> array("lang_id" => 1, "lang_flag" => "UK.gif", "lang_name" => "English", "file_inform" => "lang_en"), 
								2=> array("lang_id" => 2, "lang_flag" => "VN.gif", "lang_name" => "VietNamese", "file_inform" => "lang_vn"),
								3=> array("lang_id" => 3, "lang_flag" => "JP.gif", "lang_name" => "Japane", "file_inform" => "lang_jp")
						   );
}						   
?>
