<?php
/**
 * @project_name: localproject 
 * @file_name	: fileUpload.inc
 * @descr:
 *  <form action="" method="post" enctype="multipart/form-data">
 *	<p>Pictures:
 *	<input type="file" name="pictures[]" />
 *	<input type="file" name="pictures[]" />
 *	<input type="file" name="pictures[]" />
 *	<input type="submit" value="Send" />
 *	</p>
 *	</form>
 * @author	ChienKV - khuongchien@gmail.com
 * @version 1.0
 **/
 
 if ( !defined("FILE_UPLOAD_INC") ) {
 	
 define("FILE_UPLOAD_INC",1);
 
 class FileUpload {
	
	private $_FILES;
	private $keys;
	private $count;
	private $keyName;
	
	public $str_key_name;
	
	// for php5
	public function __construct($FILES) {
		$this->FileUpload($FILES);
	}
	
	public function FileUpload($FILES) {
	
		$this->_FILES = $FILES;
		
		$this->count 		= 0;
		$this->keys			= array_keys($_FILES);
		$this->str_key_name = array_keys($_FILES);
		$this->keyName 		= "";
	
	}
		
	public function reset() {
		$_FILES 		= "";
		$keys 			= "";
		$str_key_name 	= "";
		$count 			= "";
		$keyName 		= "";
	}
		
	public function getKeys() {
		return $this->keys;	
	}
	
	public function getKeyName() {
		return $this->keys[$this->count];	
	}

	public function getName(){
		$key = $this->keys[$this->count];
		return $_FILES[$key]["name"];
	}
	
	public function getExtFile() {
		$name = strtolower($this->getName());
		return strrchr($name, ".");
	}
	
	public function getShortName() {
		$ext_len = strlen($this->getExtFile());
		$name = $this->getName();
		return substr($name, 0, -$ext_len);
	}
	
	public function getType() {
		$key = $this->keys[$this->count];
		return $_FILES[$key]["type"];
	}
	
	public function getError() {
		$key = $this->keys[$this->count];
		return $_FILES[$key]["error"];
	}
	
	// return int byte
	public function getSize() {
		$key = $this->keys[$this->count];
		return $_FILES[$key]["size"];
	}

	public function getTmpName() {
		$key = $this->keys[$this->count];
		return $_FILES[$key]["tmp_name"];
	}
	
	public function setKeyName($str) {
		for ($i = 0; $i < sizeof($this->keys); $i++){
			if($str == $this->keys[$i]){
				$this->count = $i;
				return ;
			}
		}
	}
	
	public function getErrorMessage() {
		$code = $this->getError();
		$str = "";
		switch($code) {
			case UPLOAD_ERR_OK:
				break;
			case UPLOAD_ERR_INI_SIZE:
				$str = " - Please check upload_file_size in php.ini file";
				break;
			case UPLOAD_ERR_FORM_SIZE:
				$str = " - Have a problem with upload form size";
				break;
			case UPLOAD_ERR_PARTIAL:
				$str = " - Error upload";
				break;
			case UPLOAD_ERR_NO_FILE:
				$str = " - File upload not found";
				break;
		}
		return $str;
	}
	
	public function hasItem() {
		if ($this->count < sizeof($this->_FILES)){
			return 1;
		}
		return 0;
	}
	
	public function next() {
		$this->count++;
	}
	
	/*
	 * Upload file
	 */
	public function uploaded_file($filePath,$filename = ""){
		if(!$filename){
			$filename = $this->getName();
		}
		$uploadfile = $filePath.$filename;
		return move_uploaded_file($this->getTmpName(), $uploadfile);
	}
	
	public function copyForce($filePath,$filename = ""){
		if($this->getError() == 0){
			$this->copy($filePath,$filename);
		}
	}
	
	public function fileValidate($setting=""){
		if(!isset($this->_FILES) || !is_array($this->_FILES) || sizeof($this->_FILES) <= 0 || ($this->getSize() <= 0)){
			if(isset($setting["nullok"]) && $setting["nullok"] == 0){
				if(isset($setting["nullerrormsg"])) return $setting["nullerrormsg"]."<br />\n";
				else return " - Please enter correct form <br />\n";
			}
		}
		return $this->getErrorMessage();
	}
  }
}