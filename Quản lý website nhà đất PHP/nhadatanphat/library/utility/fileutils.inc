<?php
/**
 * @note:
 * 
 * @author	ChienKV - khuongchien@gmail.com
 * @version 1.0
 */

class FileUtils {
	
	/**
	 * @note:	Create a folder
	 * 
	 * @param 	String:	folder
	 * @param 	Int	: 	chmod [Default: 0777]
	 * @return 	Boolean - true if create success or folder existing, false if create fail
	 * @author 	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 **/
	static function createFolder($folder, $chmod = 0777) {
		if (empty($folder)) return false;
		if(!is_dir($folder)) {
			if (!mkdir($folder, $chmod)) return false;
			else return true;
		} return true;
	}

	/**
	 * @note:	Rename a file in folder
	 * 
	 * @param	String:	folder
	 * @param	String:	oldname
	 * @param	String:	newName	[Default: null]
	 * @return	Boolean - TRUE if rename success else return FALSE
	 * @author 	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	static function renameFile($folder, $oldname, $newName = null){
		if(!is_dir($folder)) {
			if(!is_file($folder."/".$oldname)) {
				if (!is_null($newName)) return rename($folder."/".$oldname, $folder."/".$newName);
			}
		} else return false;
	}

	/**
	 * @note: 	get dir file
	 * 
	 * @param 	String file
	 * @return 	String
	 * @author 	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	static function getDirFile($file) {
		$file = pathinfo($file);
		if (isset($file["dirname"])) return strtolower($file["dirname"]);
		else return null;		
	}
	
	/**
	 * @note: 	get name file
	 * 
	 * @param 	String file
	 * @return 	String
	 * @author 	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	static function getFileName($file) {
		$file = pathinfo($file);
		if (isset($file["basename"])) return strtolower($file["basename"]);
		else return null;		
	}

	/**
	 * @note: 	get extension file
	 * 
	 * @param 	String file
	 * @return 	String
	 * @author 	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 */
	static function getFileExtension($file) {
		$file = pathinfo($file);
		if (isset($file["extension"])) return strtolower($file["extension"]);
		else return null;		
	}

	/**
	 * @note: 	check exist file
	 * 
	 * @param 	String:	folder
	 * @param 	String:	fileName
	 * @return 	Boolean
	 * @author 	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 **/
	static function checkFileExist($folder, $fileName) {
		if(!is_dir($folder)) return false; // if not exist source file
		$file = $folder ."/". $fileName;
		if (file_exists($file) && is_file($file)) return true;
		else return false;
	}
	
	/**
	 * @note: 	delete file
	 * 
	 * @param 	String:	folder
	 * @param 	String:	fileName
	 * @return 	Boolean
	 * @author 	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 **/
	static function deleteFile($folder, $fileName) {
		if(!is_dir($folder)) return false; // if not exist source file
		$file = $folder ."/". $fileName;
		if (file_exists($file) && is_file($file)) {
			return unlink ($file);
		} else return false;
	}
	
	/**
	 * @note: 	Check file type
	 * 
	 * @param 	String		:	filename
	 * @param 	Array/String:	extarray
	 * @return 	Boolean
	 * @author 	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 **/
	static function checkFileType($filename, $extarray = array()) {

		if (empty($extarray))
			return TRUE;
		$extarray = is_array($extarray) ? $extarray : explode(",",$extarray);
		$bCheck = FALSE;
		$fileType = pathinfo($filename);
		if ($fileType && isset($fileType['extension'])) {
			for( $i = 0; $i < count($extarray); $i++ ) {
				if (strtolower($fileType['extension']) == strtolower($extarray[$i]) ) {
					$bCheck = TRUE;
					break;
				}
			}
		}
		return $bCheck;
	}

	/**
	 * @note: 	Read file name in folder
	 * 
	 * @param 	String:	read_dir
	 * @param 	Array:	extarray
	 * @return 	Boolean
	 * @author 	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 **/
	static function readFileNameInFolder($read_dir, $extarray = array()) {
		if(is_dir($read_dir)){
			$arrayFile = array();
			@$dir = dir($read_dir);
			$i = 0;
			while($file=$dir->read()) {
				$fileType = pathinfo($read_dir."/".$file);
				if(file_exists($read_dir."/".$file) && FileUtils::checkFileType($file, $extarray)){
					$arrayFile[$i] = $file;
					$i++;
				}
			}
			for($i = 0; $i < sizeof($arrayFile)-1; $i++) {
				for ($j = $i+1 ; $j < sizeof($arrayFile); $j++) {
					if (date("Y-m-d-H-i-s", filemtime($read_dir."/".$arrayFile[$i])) < date("Y-m-d-H-i-s", filemtime($read_dir."/".$arrayFile[$j]))) {
						$tempFile 	   = $arrayFile[$i];
						$arrayFile[$i] = $arrayFile[$j];
						$arrayFile[$j] = $tempFile;
					}
				}
			}
			return $arrayFile;
		} else return array();
	}

	 /**
	 * @note:	function getImageRandomName
	 * @desc: 	Get file name with extention
	 * 
	 * @param 	String  folder
	 * @param 	String  fileName
	 * @return 	String - File name with radom extention
	 * @author	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 **/
	static function getImageRandomName($folder, $fileName, $extarray = array()) {
		$arrImgExtention= $extarray;
		$fileExtention 	= "";
		foreach($arrImgExtention as $imgExtention){
			$imgExtention = strtolower($imgExtention);
			$filePath = $folder . '/' . $fileName . "." . $imgExtention;
			if(file_exists($filePath)){
				$fileExtention = $imgExtention;
				break;
			}
		}
		if($fileExtention != "") return $fileName . "." . $fileExtention;
		else return null;
	}	

	/**
	 * @note:	get_filesize($dir, $file)
	 * 
	 * @param 	String - $dir, path of file
	 * @param 	String - $file
	 * @return 	int - size for file (bytes)
	 * @author	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	**/
	static function getFileSize($dir,$file) {
		$file = $dir.$file;
		return filesize($file);
	}
		
	/**
	 * @note:	int - get_upload_max_filesize_of_site($i_size_file)
	 * @desrc: 	get value upload_max_filesize of denifid of admin site	
	 * 
	 * @param 	int -$i_size_file // MB
	 * @return 	int
	 * @author	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 **/
	static function getUploadMaxFilesizeOfSite($i_size_file) {		
		$int_filesize = (FileUtils::getPostMaxSize() < FileUtils::getUploadMaxFileSize()) ? FileUtils::getPostMaxSize() : FileUtils::getUploadMaxFileSize();
		$int_filesize = ($int_filesize > $i_size_file) ? $i_size_file : $int_filesize;
		return $int_filesize;
	}
	
	/**
	 * @note:	int - function return_mb($val)
	 * 
	 * @param 	int  - $val, byte
	 * @return 	int - size of file = MB
	 * @author	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 **/
	static function returnMB($val) {
	    return $val/(1024*1024);
	}
	
	/**
	 * @note:	int - get_post_max_size()
	 * @desrc: 	get value post_max_size of PHP.INI	 
	 * @return 	int
	 * @author	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 **/
	static function getPostMaxSize() {
		$int_post_max_size = (int)ini_get("post_max_size");
		return $int_post_max_size;
	}

		
	/**
	 * @note:	int - get_upload_max_filesize()
	 * @desrc: 	get value upload_max_filesize of PHP.INI
	 * 
	 * @return 	int
	 * @author	ChienKV - khuongchien@gmail.com
	 * @version 1.0
	 **/
	static function getUploadMaxFileSize() {
		$int_max_filesize = (int)ini_get("upload_max_filesize");
		return $int_max_filesize;
	}
	
	/**
	 * is_download_file($dir, $file, $msg_erros)
	 * 
	 * @author ChienKV(ntsc279@hotmail.com)
	 * @param String - $dir, path of file
	 * @param String - $file, fiel name
	 * @param String - $msg_erros
	**/	

	function isDownloadFile($dir, $file, $msg_erros = "Download false", $rename = true) {
	    
	    if (is_file($dir.DS.$file)) {
	    	if(ereg("\.",$file)) {
			$str = str_replace(".","",strrchr($file, "."));
			}
			$content_type = Utils::getContentType($str);
		    if ((isset($file))&&(FileUtils::checkFileExist($dir, $file))) {
		       header("Content-type: application/force-download");
		       header('Content-Disposition: inline; filename="' . $dir.$file . '"');
		       header("Content-Transfer-Encoding: Binary");
		       header("Content-length: ".get_filesize($dir,$file));
		       header('Content-Type: '.$content_type);
		       $file_name_export = /*($rename) ? "document".date("Ymdhis").".$str" : */$file;
		       header('Content-Disposition: attachment; filename="'. $file_name_export . '"');
		       readfile($dir, $file);		       
		    } else {
		       echo $msg_erros;
		    } //end if	
	    } else echo $msg_erros;
	}
}
?>