<?php
/**
 * @project_name: localframe
 * @file_name: utils.inc
 * @descr:
 * 
 * @author 	Thunn - thunn84@gmail.com
 * @version 1.0
 **/ 
if (!defined("UTILS_INC") ) {
 	define("UTILS_INC",1);

	class Utils {
		public static function valid_ipv4($ip_addr) {
		    $num 	= "([0-9]|1?\d\d|2[0-4]\d|25[0-5])";
			$range 	= "([1-9]|1\d|2\d|3[0-2])";
			if(preg_match("/^$num\.$num\.$num\.$num(\/$range)?$/", $ip_addr)) {
				return 1;
		    }
			return 0;
		}

//		$str 	= "127.0.0.1 127.0.0.256 127.0.0.1/36 127.0.0.1/1";
//		$ip_array_temp = explode(" ", $str);
//		$arrIP 	= array();
//		foreach ($ip_array_temp as $ip_addr) {
//			if(valid_ipv4($ip_addr)) $arrIP[] = $ip_addr;
//		}
//		print_r($arrIP);

		public static function formatPrice($value = null) {
			$value 	= strval(trim($value));
			$i 		= 0;
			while (TRUE) {
				if (strlen($value)%3) {
					$value = "0".$value;
					$i++;
				} else break;
			}
			$value = substr(chunk_split($value, 3, "."), 0, -1);
			return substr($value, $i);
		}
		
		public static function getIPNumber($value = null, $mb_flag = false){
			$value 	= trim($value);
			$strip	= null;
			$IPArr	= array();
			if ($mb_flag) $length = mb_strlen($value, "SHIFT-JIS");
			else $length = strlen($value);
			for ( $i = 0; $i < $length; $i++ ) {
				$char = substr($value, $i, 1);
				if (ereg("^[0-9.]+$", $char)) $strip.= $char;
				if (!ereg("^[0-9.]+$", substr($value, $i+1, 1)) && strlen($strip)) {
					$arrtemp	= explode(".", $strip);
					if (count($arrtemp) == 4) {
						$check	= true;
						foreach ($arrtemp as $temp) if (strlen($temp) > 3) {
							$check	= false;
							break;
						}
						if ($check) $IPArr[]= $strip;
					}
					$strip	= null;
				}
			}
			return $IPArr;
		}
		
		public static function getNumberic($value = null, $mb_flag = false){
			$value 	= trim($value);
			$return = null;
			if ($mb_flag) $length = mb_strlen($value, "SHIFT-JIS");
			else $length = strlen($value);
			for ( $i = 0; $i < $length; $i++ ) {
				$char = substr($value, $i, 1);
				if (ereg("^[0-9]+$", $char)) $return.= $char;
			}
			return $return;
		}

		public static function getAnphabet($value = null){
			$value 	= trim($value);
			$return = null;
			$length = strlen($value);
			for ( $i = 0; $i < $length; $i++ ) {
				$char = substr($value, $i, 1);
				if (ereg("^[a-zA-Z]+$", $char)) $return.= $char;
			}
			return $return;
		}

		public static function cstr($text, $start=0, $limit=12) {
	        if (function_exists('mb_substr')) {
	            $more = (mb_strlen($text) > $limit) ? TRUE : FALSE;
	            $text = substr($text, 0, $limit);
	            return array($text, $more);
	        } elseif (function_exists('iconv_substr')) {
	            $more = (iconv_strlen($text) > $limit) ? TRUE : FALSE;
	            $text = iconv_substr($text, 0, $limit, 'UTF-8');
	            return array($text, $more);
	        } else {
	            preg_match_all("/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|\xe0[\xa0-\xbf][\x80-\xbf]|[\xe1-\xef][\x80-\xbf][\x80-\xbf]|\xf0[\x90-\xbf][\x80-\xbf][\x80-\xbf]|[\xf1-\xf7][\x80-\xbf][\x80-\xbf][\x80-\xbf]/", $text, $ar);   
	            if(func_num_args() >= 3) {   
	                if (count($ar[0])>$limit) {
	                    $more = TRUE;
	                    $text = join("",array_slice($ar[0],0,$limit))."..."; 
	                }
	                $more = TRUE;
	                $text = join("",array_slice($ar[0],0,$limit)); 
	            } else {
	                $more = FALSE;
	                $text =  join("",array_slice($ar[0],0)); 
	            }
	            return array($text, $more);
			} 
		}
	
		public static function cutTitle($text, $limit=255) {
			$val = Utils::cstr($text, 0, $limit);
			return $val[1] ? $val[0].".........." : $val[0];
		}
	
		public static function randomNumbers( $count = 9 ) {
			$start_numbers = 1; // [1]
			$end_numbers = 9; // [2]
			for ( $i = 1; $i < $count; $i++ ) {
				$start_numbers = $start_numbers.$start_numbers; // [3]
				$end_numbers = $end_numbers.$end_numbers; // [4]
			}
			mt_srand ((double) microtime() * $end_numbers);
			$random_numbers = mt_rand ( $start_numbers, $end_numbers );
	
			return $random_numbers;
		}
	 	
		public static function randomPassword($length, $allow = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") {
			$i 	 = 1;
			$ret = null;
			while ($i <= $length) {
				$max 	= strlen($allow)-1;
				$num 	= rand(0, $max);
				$temp 	= substr($allow, $num, 1);
				$ret 	= $ret . $temp;
				$i++;
			}
			return $ret;
		}

		/** 
		* ------------------------------------------------------------- 
		* @note:	getRandomForArray($arr_value, $number_records)
		* @descr: 	get number value random of array
		*  
		* @author	Thunn(thunn84@hotmail.com)
		* @param	array - $arr_value, array value
		* @param	int	  - $number_records, number records need
		* @return	array. 
		* ------------------------------------------------------------- 
		*/ 
		public static function getRandomForArray($arr_value, $number_index) {	
		    if (sizeof($arr_value) <= 0 )
		    	return null;
		    $result_temp = array();
		    $result = array(); 
		    for ($i = 0; $i < $number_index; $i++) {
				$result_temp = array_rand($arr_value);
				$result[$i] = $arr_value[$result_temp]; 
			}
		   	return $result; 
		}
		
		/***********************************************************************
		******** 
		    Function:		getContentType($ext ,$song=false) 
		    Description:    returns content type for the file type 
		    Arguments:      $ext as extension file string 
							$funcPath as name of function get path
		    Returns:        string 
		************************************************************************
		*******/ 
		public static function getContentType( $ext, $song=false ) {
			switch ( strtolower( $ext ) ) 
			{
				case "gif" :
					return "image/gif";
					break;
				case "png" :
					return "image/png";
					break;
				case "pnz" :
					return "image/png";
					break;
				case "jpg" :
					return "image/jpeg";
					break;
				case "jpz" :
					return "image/jpeg";
					break;
				case "jpeg" :
					return "image/jpeg";
					break;
				case "mld" :
					return "application/x-mld";
				case "mid":
					return "audio/mid";
					break;
				case "mmf" :
					return "application/x-smaf";
				case "pmd" :
					return "application/x-pmd";
				case "asf":
					return "video/x-ms-asf";
					break;
				case "flv":
					return "video/x-flv";
					break;
				case "amc":
					return "application/x-mpeg";
					break;
				case "3gp":
					
					if ($song == true) {
						return "audio/3gpp";
						break;
					}else {
						return "video/3gpp";
						break;
					}
				case "3g2":
					return "audio/3gpp2";
					break;
				case "mp4":
					return "video/mp4";
					break;
				case "dcf":
					return "";
					break;
				case "swf":
					return "application/x-shockwave-flash";
					break;
				case "dmt":
					return "application/octet-stream";
					break;
				default :
					return "application/octet-stream";
			}
		}
		
		/**
		 * show_template_mail($template_mail, $account_name, $new_pass)
		 * @descr: show temptale letter for member
		 * 
		 * @author	Thunn
		 * @param String - $template_mail
		 * @param array - $member_obj 
		 **/
		public static function showTemplateMail($template_mail, $member_obj) {
	    	global $smarty;
	    	$letter_footer = TEMPLATE_PATH."mail".DS."letter_footer".TPL_TYPE;
	 		$letter_header = TEMPLATE_PATH."mail".DS."letter_header".TPL_TYPE;
	 
	 		
	 		$smarty->assign("account_name", isset($member_obj['member_name']) ? $member_obj['member_name'] : '');
	 		$smarty->assign("account_name", isset($member_obj['account_name']) ? $member_obj['account_name'] : '');
	 		$smarty->assign("new_password", isset($member_obj['new_pass']) ? $member_obj['new_pass'] : '');
	 		
	 		$smarty->assign("recover_pass_obj", $member_obj);
	 		
	 		$smarty->assign("letter_footer", $letter_footer);
	 		$smarty->assign("letter_header", $letter_header);
	 		return $smarty->fetch($template_mail);
	    }
	    
	    /**
	     * @note:	function get random number
	     * @param	Int:	min
	     * @param	Int:	max
	     * @return 	String
	     * @author 	Thunn - thunn84@gmail.com
	     * @version 1.0
	     **/
	    public static function randNumber($min = null, $max = null) {
	        if (isset($min) && isset($max)) {
	            if ($min >= $max) {
	                return $min;
	            } else {
	                return mt_rand($min, $max);
	            }
	        } else {
	            return mt_rand();
	        }
	    }
	    
	    public static function createThumbnailsImage($fullPathToSource, $fullPathOutput, $thumbWidth, $ext){

		  	//$ext = strtolower(mime_content_type($fullPathToSource));
		  	$ext = strtolower($ext);
		  	if($ext == ".jpeg" || $ext == ".jpg"){//create image for JPG, JPEG
			  	try{
			  		// load image and get image size
			      	$img = imagecreatefromjpeg($fullPathToSource);
			      	$width = imagesx( $img );
			      	$height = imagesy( $img );
			      	/***********************************************************/
			      	$tmp_img = imagecreatetruecolor(150, 95);
      				imagecopyresampled($tmp_img, $this->img, 0, 0, 0, 0, 150, 95, $width, $height);
//      				$this->image = $new_image; 
					/***********************************************************/
			      	// calculate thumbnail size
//			      	$new_width = $thumbWidth;
//			      	$new_height = floor( $height * ( $thumbWidth / $width ) );
					// create a new tempopary image
//		      		$tmp_img = imagecreatetruecolor( $new_width, $new_height );
	
		      		// copy and resize old image into new image 
//		      		imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
		      		//save thumbnail into a file
					return imagejpeg($tmp_img);
			  	}catch(Exception $ex){
			  		//print_r($ex);
		  			return false;
			  	}
		  	}elseif($ext == ".gif"){
		  		try{
			  		// load image and get image size
			      	$img = imagecreatefromgif($fullPathToSource);
			      	$width = imagesx( $img );
			      	$height = imagesy( $img );
			
			      	// calculate thumbnail size
			      	$new_width = $thumbWidth;
			      	$new_height = floor( $height * ( $thumbWidth / $width ) );
			
					// create a new tempopary image
		      		$tmp_img = imagecreatetruecolor( $new_width, $new_height );
	
		      		// copy and resize old image into new image 
		      		imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
		      		
		      		/**************************************************************/
		      		
		      		
		      		
		      		/**************************************************************/
		
		      		//save thumbnail into a file
					return imagegif($tmp_img, $fullPathOutput);
		  		}catch(Exception $ex){
		  			//print_r($ex);
		  			return false;
		  			
		  		}
		  	}elseif($ext == ".png"){
			  	try{
			  		// load image and get image size
			      	$img = imagecreatefrompng($fullPathToSource);
			      	$width = imagesx( $img );
			      	$height = imagesy( $img );
			
			      	// calculate thumbnail size
			      	$new_width = $thumbWidth;
			      	$new_height = floor( $height * ( $thumbWidth / $width ) );
			
					// create a new tempopary image
		      		$tmp_img = imagecreatetruecolor( $new_width, $new_height );
	
		      		// copy and resize old image into new image 
		      		imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
		
		      		//save thumbnail into a file
					return imagepng($tmp_img, $fullPathOutput);
				}catch(Exception $ex){
		  			//print_r($ex);
		  			return false;
		  		}
		  	}else{//no extension match.
		  		return false;
		  	}
		}
		
		public static function vietnameseToASCII($inputStr){
			$returnVal = "";
			$inputStr = mb_strtolower($inputStr,"UTF-8");
			$patterns = array("/á/", "/à/", "/ả/", "/ã/", "/ạ/", "/ă/", "/ắ/", "/ằ/", "/ẳ/", "/ẵ/", "/ặ/", "/â/", "/ấ/", "/ầ/", "/ẩ/", "/ẫ/", "/ậ/"
					, "/đ/"
					, "/é/", "/è/", "/ẻ/", "/ẽ/", "/ẹ/", "/ê/", "/ế/", "/ề/", "/ể/", "/ễ/", "/ệ/"
					, "/í/","/ì/","/ỉ/","/ĩ/","/ị/"
					, "/ó/", "/ò/", "/ỏ/", "/õ/", "/ọ/", "/ô/", "/ố/", "/ồ/", "/ổ/", "/ỗ/", "/ộ/", "/ơ/", "/ớ/", "/ờ/", "/ở/", "/ỡ/", "/ợ/"
					, "/ú/","/ù/","/ủ/","/ũ/","/ụ/","/ư/","/ứ/","/ừ/","/ử/","/ữ/","/ự/"
					, "/ý/","/ỳ/","/ỷ/","/ỹ/","/ỵ/"
				);

			$replacements = array("a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a"
					, "d"
					, "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e"
					, "i", "i", "i", "i", "i"
					, "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o"
					, "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u"
					, "y", "y", "y", "y", "y"
				);

			$returnVal = preg_replace($patterns, $replacements, $inputStr);
			return $returnVal;
		}
	}
}// end defined
?>
