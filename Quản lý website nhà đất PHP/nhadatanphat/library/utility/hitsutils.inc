<?php
/**
 * @project_name: localframe
 * @file_name: hitsutils.inc
 * @descr:
 * 
 * @author 	ChienKV - khuongchien@gmail.com
 * @version 1.0
 **/ 
if (!defined("HITS_INC") ) {
 	define("HITS_INC",1);
 	
 	class Hits {
 		public static function getCountHits($dir = null, $filewrite = null) {
			if(!is_dir($dir)) $dir = ROOT_PATH;
			if (!is_file($filewrite)) $filewrite = "hitslog.log";
			$CountFile = $dir.DS.$filewrite;
			if (!file_exists($CountFile)) {
				$CF= fopen ($CountFile, "a");
				fwrite ($CF, 0);
				fclose ($CF);
			}
			$CF		= fopen ($CountFile, "r");
			$Hits 	= fread ($CF, filesize ($CountFile));
			fclose ($CF);
			$Hits++;
			$CF 	= fopen ($CountFile, "w");
			fwrite ($CF, $Hits);
			fclose ($CF);
			return $Hits;
 		}
 	}
}
?>
