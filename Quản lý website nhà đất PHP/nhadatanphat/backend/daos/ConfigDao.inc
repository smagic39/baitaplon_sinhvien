<?php 
/**
 * @project_name: ntmusic
 * @file_name: ConfigDao.inc
 * @descr:
 * 
 * @author thangnc
 * @version 1.0	 2008/04/07, thangnc, created
 **/
if (!defined("CONFIG_DAO_INC")) {
	define("CONFIG_DAO_INC", 1);

	class ConfigDao extends DbConnect {

		function __construct() {
			parent :: __construct();
		}

		public function getTable() {
			return "mst_configs";
		}

		/**
		* get_config_by_id($config_id)
		* 
		* @param int - $config_id
		* @return array
		**/
		public function get_config_by_id($config_id) {

			$strSQL = "SELECT * ";
			$strSQL .= "FROM " . $this->getTable() . " ";
			$strSQL .= "WHERE config_id = ?";

			$this->prepare($strSQL);
			$result = $this->execRun(array (
				$config_id
			));

			return isset ($result[0]) ? $result[0] : null;
		}

		/**
		 * get_categories_count($config_id='0')
		 * 
		 * @author Nguyen Chien Thang
		 * @param int - $config_id
		 * @return int
		 **/
		public function get_config_count($category_name = '') {

			$params = array ();

			$strSQL = "SELECT COUNT(*) AS cnt ";
			$strSQL .= "FROM " . $this->getTable() . " ";
			
			$this->prepare($strSQL);
			$result = $this->exec($params);

			return intval($result[0]['cnt']);
		}

		/**
		 * get_categories_list($config_id='0', $limit='', $page_id=1)
		 * 
		 * @author Nguyen Chien Thang
		 * @param int - $config_id
		 * @param int - $limit
		 * @param int - $page_id
		 * @return list
		 **/
		public function get_config_list($limit = '') {

			$params = array ();

			$strSQL = "SELECT * ";
			$strSQL .= "FROM " . $this->getTable() . " ";
			
			if (($limit > 0) ) {

				$strSQL .= "LIMIT ? ";
				array_push($params, (int)$limit);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);

			return $result;			
		}

		/**
		 * insert_data($categories_obj)  
		 * 
		 * @author Nguyen Chien Thang
		 * @param array - $categories_obj, array('key1' => 'value1', 'key2' => 'value2',...)
		 * @return void
		**/
		public function insert_config($categories_obj) {
			$this->insert_db($this->getTable(), $categories_obj);
		}

		/**
		 * update_categories($categories_obj, $config_id)  
		 * 
		 * @author Nguyen Chien Thang
		 * @param array - $categories_obj, array('key1' => 'value1', 'key2' => 'value2',...)
		 * @param int - $config_id
		 * @return void
		**/
		public function update_config($categories_obj, $config_id) {
			$this->update_db($this->getTable(), $categories_obj, "config_id = $config_id");
		}
		
		/**
		 * delete_categories($config_id)
		 * @desc: update infomation categories_active = LOGIC_DEL
		 **/
		public function delete_config($config_id) {
			$this->delete_db($this->getTable(), "config_id = ?", $config_id);
		}

		
	} // end class 	
}
?>