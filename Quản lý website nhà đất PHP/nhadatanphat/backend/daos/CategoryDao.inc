<?php
/**
 * @project_name: localframe
 * @file_name: CategoryDao.inc
 * @descr:
 * 
 * @author	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 **/ 
 if (!defined("CATEGORY_DAO_INC")) {

	define("CATEGORY_DAO_INC",1);
	
	class CategoryDao extends DbConnect /*DbMySQLConnect*/  {
		
		function __construct() {
			parent::__construct();
		}      
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " tbl_category ";
		}

		/**
		 * @note:	function get keys name
		 * 
		 * @version 1.0
		 */
		public function getKeys() {
			return " category_id ";
		}
		
		public function getAllCategories($categoryname = null, $status = null, $limit = null, $offset = null) {
			$params = array();
			$strSQL = " SELECT category.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS category ";
			$strSQL.= " WHERE LOWER(category.category_name) LIKE ? ";
			array_push($params, '%'.strtolower($categoryname).'%');
			if (strlen($status)) {
				$strSQL.= " AND category.status = ? ";
				array_push($params, $status);
			}
			$strSQL.= " ORDER BY category.category_sort ";
			if ($offset < 0) $offset = 0;
			if (($limit > 0) && ($offset >= 0)) {
				$strSQL.= " LIMIT ? OFFSET ?";
				array_push($params, $limit, $offset);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return is_array($result) ? $result : array();
		}

		public function getCountCategories($categoryname = null, $status = null) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS category ";
			$strSQL.= " WHERE LOWER(category.category_name) LIKE ? ";
			array_push($params, '%'.strtolower($categoryname).'%');
			if (strlen($status)) {
				$strSQL.= " AND category.status = ? ";
				array_push($params, $status);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return intval($result[0]["count"]);
		}

		public function getAllCategoryByKind($kind, $status = null) {
		 	$params = array();
		 	$strSQL = " SELECT category.* ";
			$strSQL.= " FROM ". $this->getTableName() ." category  ";
			$strSQL.= " WHERE category.category_kind = ? ";
			array_push($params, $kind);
			if (strlen($status)) {
				$strSQL.= " AND category.status = ? ";
				array_push($params, $status);
			}
			$strSQL.= " ORDER BY category.category_sort";

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return is_array($result) ? $result : array();
		}

		public function getAllCategoryByParentId($parentId) {
		 	$param  = array();
		 	$strSQL = " SELECT category.* ";
			$strSQL.= " FROM ". $this->getTableName() ." category  ";
			$strSQL.= " WHERE category.parent_id = ? ";
			$strSQL.= " ORDER BY category.category_id";
			array_push($param, $parentId);

			$this->prepare($strSQL);
			$result = $this->exec($param);

		 	return is_array($result) ? $result : array();
		}

		public function getCountCategoryByParentId($parentId) {
		 	$param  = array();
		 	$strSQL = " SELECT COUNT(category.category_id) AS count ";
			$strSQL.= " FROM ". $this->getTableName() ." category  ";
			$strSQL.= " WHERE category.parent_id = ? ";
			array_push($param, $parentId);

			$this->prepare($strSQL);
			$result = $this->exec($param);

		 	return intval($result[0]["count"]);
		}

		/**
		 * @note:	function get data of tabla tbl_category
		 * 
		 * @return	Array	: Data of table tbl_category
		 * @version 1.0
		 */
		public function getCategoryById($oid) {
			$params = array();
			$strSQL = " SELECT category.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS category ";
			$strSQL.= " WHERE ".$this->getKeys()." = ? ";
			array_push($params, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}

		public function checkExistedCategoriesName($categoryname, $oid) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS category ";
			$strSQL.= " WHERE category.category_name = ? AND ".$this->getKeys()." <> ? ";
			array_push($params, $categoryname, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return intval($result[0]["count"]);
		}
		
		/**
		 * @note:	function insert data for table tbl_category
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function insert_category($objects) {
			return $this->insert_db($this->getTableName(), $objects);
		}

		/**
		 * @note:	function update data for table tbl_category
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function update_category($objects, $oid) {
			return $this->update_db($this->getTableName(), $objects, $this->getKeys()." = ? ", array($oid));
		}

		/**
		 * @note:	function delete data for table tbl_category
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function delete_category($oid) {
			$this->delete_db(" tbl_inform ", $this->getKeys()." = ? ", array($oid));
			return $this->delete_db($this->getTableName(), $this->getKeys()." = ? ", array($oid));
		}
	}// end class		
 }
?>