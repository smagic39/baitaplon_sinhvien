<?php
/**
 * @project_name: localframe
 * @file_name: InformDao.inc
 * @descr:
 * 
 * @author	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 **/ 
 if (!defined("INFORM_DAO_INC")) {

	define("INFORM_DAO_INC",1);
	
	class InformDao extends DbConnect /*DbMySQLConnect*/  {
		
		function __construct() {
			parent::__construct();
		}      
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " tbl_inform ";
		}

		/**
		 * @note:	function get keys name
		 * 
		 * @version 1.0
		 */
		public function getKeys() {
			return " inform_id ";
		}
		
		public function getAllInforms($informname = null, $categoryid = null, $status = null, $limit = null, $offset = null) {
			$params = array();
			$strSQL = " SELECT inform.* ";/*, category.category_name */
			$strSQL.= " FROM ".$this->getTableName()." AS inform ";
			//$strSQL.= " INNER JOIN tbl_category AS category ON category.category_id = inform.category_id ";
			$strSQL.= " WHERE inform.category_id = 0 AND LOWER(inform.inform_name) LIKE ? ";
			array_push($params, '%'.strtolower($informname).'%');
			if (strlen($status)) {
				$strSQL.= " AND inform.status = ? ";
				array_push($params, $status);
			}
			$strSQL.= " ORDER BY inform.created_date DESC ";
			if ($offset < 0) $offset = 0;
			if (($limit > 0) && ($offset >= 0)) {
				$strSQL.= " LIMIT ? OFFSET ?";
				array_push($params, $limit, $offset);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return is_array($result) ? $result : array();
		}

		public function getCountInform($informname = null, $categoryid = null, $status = null) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS inform ";
			$strSQL.= " INNER JOIN tbl_category AS category ON category.category_id = inform.category_id ";
			$strSQL.= " WHERE category.category_kind = 0 AND LOWER(inform.inform_name) LIKE ? ";
			array_push($params, '%'.strtolower($informname).'%');
			if (strlen($categoryid)) {
				$strSQL.= " AND inform.category_id = ? ";
				array_push($params, $categoryid);
			}
			if (strlen($status)) {
				$strSQL.= " AND inform.status = ? ";
				array_push($params, $status);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return intval($result[0]["count"]);
		}

		/**
		 * @note:	function get data of tabla tbl_inform
		 * 
		 * @return	Array	: Data of table tbl_inform
		 * @version 1.0
		 */
		public function getInformById($oid) {
			$params = array();
			$strSQL = " SELECT inform.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS inform ";
			$strSQL.= " WHERE ".$this->getKeys()." = ? ";
			array_push($params, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}

		public function checkExistedInformName($informname, $oid) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS inform ";
			$strSQL.= " WHERE inform.inform_name = ? AND ".$this->getKeys()." <> ? ";
			array_push($params, $informname, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return intval($result[0]["count"]);
		}
		
		/**
		 * @note:	function insert data for table tbl_inform
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function insert_inform($objects) {
			return $this->insert_db($this->getTableName(), $objects);
		}

		/**
		 * @note:	function update data for table tbl_inform
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function update_inform($objects, $oid) {
			return $this->update_db($this->getTableName(), $objects, $this->getKeys()." = ? ", array($oid));
		}

		/**
		 * @note:	function delete data for table tbl_inform
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function delete_inform($oid) {
			return $this->delete_db($this->getTableName(), $this->getKeys()." = ? ", array($oid));
		}
	}// end class		
 }
?>