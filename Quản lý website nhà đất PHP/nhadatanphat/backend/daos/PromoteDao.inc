<?php
/**
 * @project_name: localframe
 * @file_name: PromoteDao.inc
 * @descr:
 * 
 * @author	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 **/ 
 if (!defined("PROMOTE_DAO_INC")) {

	define("PROMOTE_DAO_INC",1);
	
	class PromoteDao extends DbConnect /*DbMySQLConnect*/  {
		
		function __construct() {
			parent::__construct();
		}      
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " tbl_promote ";
		}

		/**
		 * @note:	function get keys name
		 * 
		 * @version 1.0
		 */
		public function getKeys() {
			return " promote_id ";
		}
		
		public function getAllPromotes($promotename = null, $status = null, $limit = null, $offset = null) {
			$params = array();
			$strSQL = " SELECT promote.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS promote ";
			$strSQL.= " LEFT JOIN tbl_province AS province ON province.province_id = promote.province_id ";
			$strSQL.= " WHERE LOWER(promote.promote_name) LIKE ? ";
			array_push($params, '%'.strtolower($promotename).'%');
			if (strlen($status)) {
				$strSQL.= " AND promote.status = ? ";
				array_push($params, $status);
			}
			$strSQL.= " ORDER BY promote.created_date DESC ";
			if ($offset < 0) $offset = 0;
			if (($limit > 0) && ($offset >= 0)) {
				$strSQL.= " LIMIT ? OFFSET ?";
				array_push($params, $limit, $offset);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return is_array($result) ? $result : array();
		}

		public function getCountPromotes($promotename = null, $status = null) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS promote ";
			$strSQL.= " LEFT JOIN tbl_province AS province ON province.province_id = promote.province_id ";
			$strSQL.= " WHERE LOWER(promote.promote_name) LIKE ? ";
			array_push($params, '%'.strtolower($promotename).'%');
			if (strlen($status)) {
				$strSQL.= " AND promote.status = ? ";
				array_push($params, $status);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	return intval($result[0]["count"]);
		}

		/**
		 * @note:	function get data of tabla tbl_promote
		 * 
		 * @return	Array	: Data of table tbl_promote
		 * @version 1.0
		 */
		public function getPromoteById($oid) {
			$params = array();
			$strSQL = " SELECT promote.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS promote ";
			$strSQL.= " LEFT JOIN tbl_province AS province ON province.province_id = promote.province_id ";
			$strSQL.= " WHERE ".$this->getKeys()." = ? ";
			array_push($params, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}

		public function checkExistedPromoteName($promotename, $oid) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS promote ";
			$strSQL.= " WHERE promote.promote_name = ? AND ".$this->getKeys()." <> ? ";
			array_push($params, $promotename, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return intval($result[0]["count"]);
		}

		/**
		 * @note:	function insert data for table tbl_promote
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function insert_promote($objects) {
			return $this->insert_db($this->getTableName(), $objects);
		}

		/**
		 * @note:	function update data for table tbl_promote
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function update_promote($objects, $oid) {
			return $this->update_db($this->getTableName(), $objects, $this->getKeys()." = ? ", array($oid));
		}

		/**
		 * @note:	function delete data for table tbl_promote
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function delete_promote($oidArr) {
			
			$params = array();
			$strSQL = " DELETE FROM " . $this->getTableName() . " WHERE " . $this->getKeys() . " IN(";
			$strIdHolder = "";
			foreach($oidArr as $key=>$val){
				$strIdHolder .= "?,";
				array_push($params, $val);
			}
			
			$strIdHolder .= "?";
			array_push($params, -1);
			
			$strSQL .= $strIdHolder . ")";
			
			$this->prepare($strSQL);
			$result = $this->exec($params);
			return $result;
		}
	}// end class		
 }
?>