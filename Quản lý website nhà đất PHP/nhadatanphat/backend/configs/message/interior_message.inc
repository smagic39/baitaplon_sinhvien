<?php
/*****************************************************************
 * @project_name: localframe
 * @package: package_name
 * @file_name: interior_message.inc
 * @descr:
 * 
 * @author 	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 *****************************************************************/
if (!Defined("INTERIOR_MESSAGE_INC") ) {
	Define("INTERIOR_MESSAGE_INC", 	TRUE);
	
	Define("INTERIOR_NAME_EXISTED", 	"Tên mẫu này đã tồn tại trong database.");
	Define("INTERIOR_IMAGE_ERROR", 		"File ảnh mẫu nội thất không đúng định dạng ảnh.");
	Define("INTERIOR_IMAGE_SIZE", 		"File ảnh mẫu nội thất phải nhỏ hơn ");
	Define("INTERIOR_NAME_NULL", 		"Tên mẫu nội thất bắt buộc nhập.");
	Define("INTERIOR_NAME_LENGTH", 		"Tên mẫu nội thất không nhập quá 255 ký tự.");
	Define("INTERIOR_IMAGE_NULL", 		"File ảnh mẫu nội thất là bắt buộc nhập.");
	Define("INTERIOR_IMAGE_LENGTH", 	"Tên file ảnh không nhập quá 255 ký tự.");
	Define("INTERIOR_DES_LENGTH", 		"Mô tả mẫu nội thất không quá 500 ký tự.");	
	define("LBL_ERRORS", "<b>Xuất hiện lỗi</b>:<br/>");
	define("MSG_ERR_DATA_NULL", "Dữ liệu này không tồn tại");
	define("MSG_ERR_CATNAME_EXITS_1", "Danh mục");
	define("MSG_ERR_CATNAME_EXITS_2", "đã tồn tại !");
}	
?>