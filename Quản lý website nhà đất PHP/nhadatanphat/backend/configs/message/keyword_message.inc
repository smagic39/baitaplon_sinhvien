<?php
/*****************************************************************
 * @project_name: localframe
 * @package: package_name
 * @file_name: support_message.inc
 * @descr:
 * 
 * @author 	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 *****************************************************************/
if (!Defined("KEYWORD_MESSAGE_INC") ) {
	Define("KEYWORD_MESSAGE_INC", 		TRUE);
	
	Define("KEYWORD_START_NULL",	"Khoảng giá bắt đầu là cần phải nhập.");
	Define("KEYWORD_START_ERRORS", 	"Khoảng giá bắt đầu là kiểu số không lớn hơn 20 ký tự (VD: 5.000.000).");
	Define("KEYWORD_END_ERRORS", 	"Khoảng giá kết thúc là kiểu số không lớn hơn 20 ký tự (VD: 5.000.000).");
}	
?>
