<?php
/*****************************************************************
 * @project_name: localframe
 * @package: package_name
 * @file_name: province_message.inc
 * @descr:
 * 
 * @author 	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 *****************************************************************/
if (!Defined("PROVINCE_MESSAGE_INC") ) {
	Define("PROVINCE_MESSAGE_INC", 	TRUE);
	
	Define("PROVINCE_NAME_EXISTED", 	"Tên tỉnh này đã tồn tại trong database.");
	Define("PROVINCE_NAME_NULL", 		"Tên tỉnh là bắt buộc nhập.");
	Define("PROVINCE_NAME_LENGTH", 		"Tên tỉnh nhập không quá 255 ký tự.");
}	
?>
