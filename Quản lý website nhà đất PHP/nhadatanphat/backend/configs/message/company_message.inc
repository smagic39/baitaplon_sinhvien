<?php
/*****************************************************************
 * @project_name: localframe
 * @package: package_name
 * @file_name: company_message.inc
 * @descr:
 * 
 * @author 	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 *****************************************************************/
if (!Defined("COMPANY_MESSAGE_INC") ) {
	Define("COMPANY_MESSAGE_INC", 	TRUE);
	
	Define("COMPANY_NAME_EXISTED", 	"Tên công ty này đã tồn tại trong database.");
	Define("COMPANY_LOGO_ERROR", 	"Logo công ty không đúng định dạng ảnh.");
	Define("COMPANY_LOGO_SIZE", 	"File ảnh logo phải nhỏ hơn ");
	Define("COMPANY_NAME_NULL", 	"Tên công ty là bắt buộc nhập");
	Define("COMPANY_NAME_LENGTH", 	"Tên công ty không lớn hơn 255 ký tự.");
	Define("COMPANY_LOGO_NULL", 	"File ảnh logo công ty là bắt buộc nhập.");
	Define("COMPANY_LOGO_LENGTH", 	"Tên file ảnh logo không lớn hơn 255 ký tự.");
	Define("COMPANY_WEBSITE_LENGTH","Tên Website công ty không lớn hơn 255 ký tự.");
	Define("COMPANY_CONTENT_LENGTH","Nội dung mô tả công ty không lớn hơn 500 ký tự.");
}	
?>
