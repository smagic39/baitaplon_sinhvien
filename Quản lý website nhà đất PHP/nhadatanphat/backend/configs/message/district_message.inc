<?php
/*****************************************************************
 * @project_name: localframe
 * @package: package_name
 * @file_name: comment_message.inc
 * @descr:
 * 
 * @author 	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 *****************************************************************/
if (!Defined("DISTRICT_MESSAGE_INC") ) {
	Define("DISTRICT_MESSAGE_INC", 		TRUE);
	
	Define("DISTRICT_NAME_EXISTED", 	"Tên huyện đã tồn tại trong database.");
	Define("DISTRICT_NAME_NULL", 		"Tên huyện là bắt buộc nhập.");
	Define("DISTRICT_NAME_LENGTH", 		"Tên huyện không lớn hơn 255 ký tự.");
}	
?>
