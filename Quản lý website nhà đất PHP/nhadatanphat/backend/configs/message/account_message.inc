<?php
/*****************************************************************
 * @project_name: localframe
 * @package: package_name
 * @file_name: account_message.inc
 * @descr:
 * 
 * @author 	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 *****************************************************************/
if (!Defined("ACCOUNT_MESSAGE_INC") ) {
	Define("ACCOUNT_MESSAGE_INC", 			TRUE);
	
	Define("ACCOUNT_NAME_EXISTED", 			"Tài khoản đăng nhập đã tồn tại.");
	Define("ACCOUNT_PASS_CONFIRM_ERROR", 	"Mật khẩu và mật khẩu confirm không giống nhau.");
	Define("ACCOUNT_NAME_NULL", 			"Tên tài khoản không được trống");
	Define("ACCOUNT_NAME_LENGTH", 			"Tên tài khoản phải nhỏ hơn 50 ký tự.");
	Define("ACCOUNT_NAME_ERROR", 			"Tên tài khoản không đúng định dạng.");
	Define("ACCOUNT_PASS_NULL", 			"Mật khẩu không được trống.");
	Define("ACCOUNT_PASS_LENGTH", 			"Mật khẩu phải lớn hơn 6 và nhỏ hơn 50 ký tự.");
	Define("ACCOUNT_FULLNAME_NULL", 		"Tên đầy đủ không được trống.");
	Define("ACCOUNT_FULLNAME_LENGTH", 		"Tên đầy đủ phải nhỏ hơn 255 ký tự.");
	Define("ACCOUNT_EMAIL_NULL", 			"Địa chỉ mail không được trống.");
	Define("ACCOUNT_EMAIL_LENGTH", 			"Địa chỉ mail phải nhỏ hơn 100 ký tự.");
	Define("ACCOUNT_EMAIL_ERROR", 			"Địa chỉ mail không đúng định dạng.");
	Define("ACCOUNT_ADDRESS_LENGTH", 		"Địa chỉ phải nhỏ hơn 255 ký tự.");
	Define("ACCOUNT_PHONE_LENGTH", 			"Phone phải nhỏ hơn 15 ký tự.");
	Define("ACCOUNT_PHONE_ERROR", 			"Phone is not format.");
	Define("ACCOUNT_MOBILE_LENGTH", 		"Mobile phải nhỏ hơn 15 ký tự.");
	Define("ACCOUNT_MOBILE_ERROR", 			"Mobile is not format.");
	Define("ACCOUNT_PASS_OLD_ERROR", 		"Mật khẩu cũ của bạn không đúng.");
}	
?>
