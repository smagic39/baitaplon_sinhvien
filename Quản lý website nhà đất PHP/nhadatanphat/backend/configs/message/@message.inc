<?php
/*****************************************************************
 * @project_name: localframe
 * @file_name: @message.php
 * @descr:
 * 
 * @author 	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 *****************************************************************/
if (!defined("MESSAGE_BACKEND_INC") ) {
	Define("MESSAGE_BACKEND_INC", 	TRUE);
	
	require_once "generals_message.inc";
	require_once "account_message.inc";
	require_once "news_message.inc";
	require_once "promote_message.inc";
	require_once "support_message.inc";
	require_once "category_message.inc";
	require_once "inform_message.inc";
	require_once "interior_message.inc";
	require_once "province_message.inc";
	require_once "keyword_message.inc";
	require_once "district_message.inc";
	require_once "company_message.inc";
	
	require_once "estate_message.inc";
	require_once "member_message.inc";
}
?>