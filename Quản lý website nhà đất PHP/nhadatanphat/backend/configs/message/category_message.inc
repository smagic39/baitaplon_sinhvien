<?php
/*****************************************************************
 * @project_name: localframe
 * @package: package_name
 * @file_name: category_message.inc
 * @descr:
 * 
 * @author 	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 *****************************************************************/
if (!Defined("CATEGORY_MESSAGE_INC") ) {
	Define("CATEGORY_MESSAGE_INC", 	TRUE);
	
	Define("CATEGORY_NAME_EXISTED", 	"Tên danh mục đã tồn tại trong database.");
	define("CATEGORY_PARENT_ERROR", 	"Loại này không được là cấp nhỏ hơn của chính nó.");
	Define("CATEGORY_ICON_ERROR", 		"Ảnh nhỏ không đúng định dạng.");
	Define("CATEGORY_ICON_SIZE", 		"File ảnh nhỏ phải nhỏ hơn ");
	Define("CATEGORY_NAME_NULL", 		"Tên danh mục không được trống");
	Define("CATEGORY_NAME_LENGTH", 		"Tên danh mục phải nhỏ hơn 100 ký tự.");
	Define("CATEGORY_ICON_LENGTH", 		"Tên file ảnh nhỏ phải nhỏ hơn 255 ký tự.");
	Define("CATEGORY_SORT_NULL", 		"Thứ tự hiển thị không được trống");
	Define("CATEGORY_SORT_NUMBER_ERROR","Thứ tự hiển thị là kiểu số nguyên lớn hơn 1 và nhỏ hơn 9999.");
}	
?>
