<?php 
/*****************************************************************
 * @project_name: localframe
 * @package: package_name
 * @file_name: contants.inc
 * @descr:
 * 
 * @author 	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 *****************************************************************/
if (!defined("CONTANTS_BACKEND_INC") ) {
	Define("CONTANTS_BACKEND_INC", 	TRUE);
	
	Define("LANGUAGES_PATH", 	BACKEND_CONF_PATH.DS."configsmarty".DS);
	
	Define("LIMIT_RECORD", 	"20"); 			/***********************Record view on page**********************/
	Define("PAGES_SIZES", 	"10"); 			/************************Page view on site***********************/
	define("PAGING_MODE", 	"Jumping"); 	/***********************Using paging of pear*********************/
}
?>
