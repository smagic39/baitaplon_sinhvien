{include file=$include_header}
{literal}
<script language="javascript" type="text/javascript">
	function submitform(url, value) {
		var theform = document.frmsupport;
		theform.action = url;
		theform.issubmit.value = value;
		theform.submit();
	}
	function hiddenform() {
		var theform = document.frmsupport;
		if (theform.editgeneral.checked) {
			theform.editgeneral.value = 1;
		} else {
			theform.editgeneral.value = 0;
		}
	}
</script>
{/literal}
{include file=$include_left}
		<td width="80%" align="left" valign="top">
		<form name="frmsupport" id="frmsupport" action="" method="post" enctype="multipart/form-data">
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			<!--header-->
			{if $message != ""}
			<tr valign="top">
				<td align="left" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;">{$message}</td>
					</tr>
					</table>
				</td>
			</tr>
			{/if}
			<tr class="stdHeader" valign="middle">
				<td style="height:20px;padding-left: 10px;">Update support</td>
			</tr>
			<tr valign="top">
				<td>
					<input type="hidden" name="supportid" id="supportid" value="{$supportid}" />
					<input type="hidden" name="page" id="page" value="{$page}" />
					<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
						{if $supportid != ''}
						<tr valign="top">
							<td width="30%" align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#FIELD_ID#}: </td>
							<td width="45%" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								{$supportid}
							</td>
						</tr>
						{/if}
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">{#SUPPORT_ACCOUNT#}: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type ="text" name="object[support_account]" id="object[support_account]" value="{$supportData.support_account}" class="input_text" />
							</td>
						</tr>
						<tr valign="top">
							<td valign="middle" class="tdregisttext">{#SUPPORT_TITLE#}: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type ="text" name="object[support_title]" id="object[support_title]" value="{$supportData.support_title}" class="input_text" />			
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#SUPPORT_GENRE#}: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<select name="object[support_genre]" id="object[support_genre]" class="dropdown">
									{foreach item=genres from=$genreList}
									<option value="{$genres.value}" {if $supportData.support_genre eq $genres.value && $supportData.support_genre ne ''}selected="selected"{/if}>{$genres.text}</option>
									{/foreach}
								</select>					
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#STATUS#}: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<select name="object[status]" id="object[status]" class="dropdown">
									{foreach item=status from=$statusList}
									<option value="{$status.value}" {if $supportData.status eq $status.value && $supportData.status ne ''}selected="selected"{/if}>{$status.text}</option>
									{/foreach}
								</select>					
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td style="height:15px;"></td>
			</tr>
			<tr valign="top">
				<td colspan="2" align="center" valign="middle" style="padding:10px;">
					<input type="button" name="update" id="update" value="{#UPDATE#}" class="button_new" onclick="javascript: submitform('?hdl=support/regist', 1)" />
					<input type="button" name="isback" id="isback" value="{#BACK#}" class="button_new" onclick="javascript: submitform('?hdl=support/list', 0)" />
					<input type="hidden" name="issubmit" id="issubmit" value="0" />
				</td>
			</tr>
			</table>
		</form>
		</td>
{include file=$include_footer}