{include file=$include_header}
{literal}
<script language="javascript" type="text/javascript">
	function submitform(url, value) {
		var theform = document.frmestate;
		theform.action = url;
		theform.issubmit.value = value;
		theform.submit();
	}
	function hiddenform() {
		var theform = document.frmestate;
		if (theform.editgeneral.checked) {
			theform.editgeneral.value = 1;
		} else {
			theform.editgeneral.value = 0;
		}
	}
</script>
{/literal}
{include file=$include_left}
		<td width="80%" align="left" valign="top">
		<form name="frmestate" id="frmestate" action="" method="post" enctype="multipart/form-data">
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			<!--header-->
			{if $message != ""}
			<tr valign="top">
				<td align="left" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;">{$message}</td>
					</tr>
					</table>
				</td>
			</tr>
			{/if}
			<tr class="stdHeader" valign="middle">
				<td style="height:20px;padding-left: 10px;">Update estate</td>
			</tr>
			<tr valign="top">
				<td>
					<input type="hidden" name="estateid" id="estateid" value="{$estateid}" />
					<input type="hidden" name="page" id="page" value="{$page}" />
					<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
						<tr valign="top">
							<td width="24%" align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#FIELD_ID#}: </td>
							<td width="75%" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								{$estateid}
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">{#ESTATE_TITLE#}: </td>
							<td align="left" valign="middle" class="tdregist">
								{$estateData.estate_title}
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">{#ESTATE_CATEGORY#}: </td>
							<td align="left" valign="middle" class="tdregist">
								{$estateData.category_name}{$categoryName}
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">{#ESTATE_ADDRESS#}: </td>
							<td align="left" valign="middle" class="tdregist">
								{$estateData.estate_address}-{$estateData.district_name}-{$estateData.province_name}
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">{#ESTATE_AREA#}: </td>
							<td align="left" valign="middle" class="tdregist">
								{$estateData.estate_area}
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">{#ESTATE_DERECTION#}: </td>
							<td align="left" valign="middle" class="tdregist">
								{$directionList[$estateData.estate_direction].text}
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">{#ESTATE_PRICE#}: </td>
							<td align="left" valign="middle" class="tdregist">
								{if $estateData.estate_price > 0}
                           			{$estateData.estate_price|money_format:"."}&nbsp;
                           			
                           			{if $estateData.estate_price_type == 0}
                           				VNĐ 
                           			{elseif $estateData.estate_price_type==1}
                           				SJC 
                           			{else}
                           				USD 
                           			{/if}
                           			{if $estateData.is_price_in_m2 eq "1"}
                           			/m2
                           			{/if}
                           			
                           			
                           			{if $estateData.is_price_negotiable eq '1'}
                           				(Có thể thỏa thuận)
                           			{/if}
                           		{else}
                           			(Thỏa thuận)
                           		{/if}
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">{#ESTATE_CREATE#}: </td>
							<td align="left" valign="middle" class="tdregist">
								{$estateData.created_date|date_format:"%d-%m-%Y"}
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">{#ESTATE_DURATION#}: </td>
							<td align="left" valign="middle" class="tdregist">
								{$estateData.estate_duration|date_format:"%d-%m-%Y"}
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">{#ESTATE_DELETE#}: </td>
							<td align="left" valign="middle" class="tdregist">
								{$estateData.deleted_date|date_format:"%d-%m-%Y"}
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">{#ESTATE_CONTENT#}: </td>
							<td align="left" valign="middle" class="tdregist">
								{$estateData.estate_note}
							</td>
						</tr>
						
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">
								File Video
							</td>
							<td align="left" valign="middle" class="tdregist">
								{if $estateData.estate_video neq ''}
									<object classid="CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95" 
											codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,5,715" 
											standby="Loading Microsoft Windows Media Player components..." type="application/x-oleobject"
											viewastext="" width="190" height="190"
										>
										<param name="FileName" value="{#AUD_ESTATE_DIR#}{$estateData.estate_video}">
										<param name="TransparentAtStart" value="true">
										<param name="AutoStart" value="true">
										<param name="AnimationatStart" value="false">
										<param name="ShowControls" value="true">
										<param name="ShowDisplay" value="false">
										<param name="playCount" value="999">
										<param name="displaySize" value="0">
										<param name="Volume" value="100">
										<embed 	type="application/x-mplayer2" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/" 
											src="{#AUD_ESTATE_DIR#}{$estateData.estate_video}" name="MediaPlayer" transparentatstart="0" 
											autostart="1" playcount="999" volume="100animationAtStart=0" displaysize="0" width="220" height="220">
									</object>
            					{/if}
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">Ảnh minh họa: </td>
							<td align="left" valign="middle" class="tdregist">
								{if $estateData.estate_image1 ne ''}
		                        <img src="{#IMG_ESTATE_DIR#}thumb_{$estateData.estate_image1}" width="150" class="border_img_white" />
		                        {/if}
		                        {if $estateData.estate_image2 ne ''}
		                        <img src="{#IMG_ESTATE_DIR#}thumb_{$estateData.estate_image2}" width="150" class="border_img_white" />
		                        {/if}
		                        {if $estateData.estate_image3 ne ''}
		                        <img src="{#IMG_ESTATE_DIR#}thumb_{$estateData.estate_image3}" width="150" class="border_img_white" />
		                        {/if}
		                        {if $estateData.estate_image4 ne ''}
		                        <img src="{#IMG_ESTATE_DIR#}thumb_{$estateData.estate_image4}" width="150" class="border_img_white" />
		                        {/if}
		                        {if $estateData.estate_image5 ne ''}
		                        <img src="{#IMG_ESTATE_DIR#}thumb_{$estateData.estate_image5}" width="150" class="border_img_white" />
		                        {/if}
							</td>
						</tr>
				<!--
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">Thông tin liên hệ: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="3">
               					<table width="98%" border="0" cellpadding="0" cellspacing="0">
               					<tr>
               						<td height="20" width="15%" align="left" valign="middle" style="font-weight: bold;">Họ và tên</td>
               						<td align="left">
               							:&nbsp;{$estateData.contact_person}
               						</td>
               						<td height="20" width="15%" align="left" valign="middle" style="font-weight: bold;">Địa chỉ email</td>
               						<td align="left">
               							:&nbsp;{$estateData.contact_mail}
               						</td>
               					</tr>
               					<tr>
               						<td height="20" width="15%" align="left" valign="middle" style="font-weight: bold;">Tên công ty</td>
               						<td align="left">
               							:&nbsp;{$estateData.contact_company}
               						</td>
               						<td height="20" width="15%" align="left" valign="middle" style="font-weight: bold;">Số điện thoại</td>
               						<td align="left">
               							:&nbsp;{$estateData.contact_phone}
               						</td>
               					</tr>
               					<tr>
               						<td height="20" width="15%" align="left" valign="middle" style="font-weight: bold;">Địa chỉ liên lạc</td>
               						<td align="left">
               							:&nbsp;{$estateData.contact_address}
               						</td>
               						<td height="20" width="15%" align="left" valign="middle" style="font-weight: bold;">Website</td>
               						<td align="left">
               							:&nbsp;{$estateData.contact_website}
               						</td>
               					</tr>
               					</table>
							</td>
						</tr>
					-->
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">Người đăng tin: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="3">
               					<table width="98%" border="0" cellpadding="0" cellspacing="0">
               					<tr>
               						<td style="padding-right: 5px;">
               							{if $logoImg neq ""}
											<img src="{#IMG_MEMBER_LOGO_DIR#}{$logoImg}" width="150" class="border_img_white" />
										{/if}
               						</td>
               						<td width="100%" align="left">
               							<table width="100%" border="0" cellpadding="0" cellspacing="0">
                 					<tr>
                 						<td height="19" width="30%" align="left" valign="middle" style="font-weight: bold;">Công ty / Cá nhân</td>
                 						<td align="left">
                 							:&nbsp;{$estateData.member_name}
                 						</td>
                 					</tr>
                 					<tr>
                 						<td height="19" width="15%" align="left" valign="middle" style="font-weight: bold;">Địa chỉ email</td>
                 						<td align="left">
                 							:&nbsp;<a href="mailto:{$estateData.member_mail}" style="color: #D70B2E;">{$estateData.member_mail}</a>
                 						</td>
                 					</tr>
                 					<tr>
                 						<td height="19" width="15%" align="left" valign="middle" style="font-weight: bold;">Số điện thoại</td>
                 						<td align="left">
                 							:&nbsp;{$estateData.member_phone}
                 						</td>
                 					</tr>
                 					<tr>
                 						<td height="19" width="15%" align="left" valign="middle" style="font-weight: bold;">Địa chỉ liên lạc</td>
                 						<td align="left">
                 							:&nbsp;{$estateData.member_address}
                 						</td>
                 					</tr>
                 					<tr>
                 						<td height="19" width="15%" align="left" valign="middle" style="font-weight: bold;">Website</td>
                 						<td align="left">
                 							:&nbsp;<a href="{$estateData.member_website}" target="_blank" style="color: #0000FF;">{$estateData.member_website}</a>
                 						</td>
                 					</tr>
               							</table>
               						</td>
               					</tr>
               					</table>
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#ESTATE_GENRE#}: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="3">
								{$genreList[$estateData.estate_genre]}
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#STATUS#}: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="3">
								{$statusList[$estateData.estate_status]}
							</td>
						</tr>
						
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">
								Loại tin:
							</td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="3">
								{if $estateData.is_fee_estate eq "1" }
									Tin có phí
								{else}
									Tin miễn phí
								{/if}
							</td>
						</tr>
						
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">
								Trạng thái tin của member
							</td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="3">
								{if $estateData.is_deleted_by_member eq "0" }
									Đang hoạt động
								{else}
									<font color='red'>
										Tin này đã bị xóa
									</font>
								{/if}
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">
								Số lần xem
							</td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="3">
								{$estateData.viewed}
							</td>
						</tr>
						
					</table>
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td style="height:15px;"></td>
			</tr>
			<tr valign="top">
				<td colspan="2" align="center" valign="middle" style="padding:10px;">
					<input type="button" name="update" id="update" value="{#EDIT#}" class="button_new" onclick="javascript: submitform('?hdl=estate/post', 0)" />
					<input type="button" name="isback" id="isback" value="{#BACK#}" class="button_new" onclick="javascript: submitform('?hdl=estate/list', 0)" />
					<input type="hidden" name="issubmit" id="issubmit" value="0" />
				</td>
			</tr>
			</table>
		</form>
		</td>
 {include file=$include_footer}