{include file=$include_header}
{literal}
<script language="javascript" type="text/javascript">
	function ConfirmDeleteRecord(url, districtid, page) {
		var theform = document.frmdistrict;
		if (confirm('Do you want to delete selected records?')) {
			theform.action = url;
			theform.districtid.value = districtid;
			theform.delete_.value = districtid;
			theform.page.value = page;
			theform.submit();
		} else return false;
	}
	function submitform(url, districtid, page, status) {
		var theform = document.frmdistrict;
		theform.action = url;
		theform.districtid.value = districtid;
		theform.page.value = page;
		theform.status.value = status;
		theform.submit();
	}
</script>
{/literal}
{include file=$include_left}
		<td width="80%" align="left" valign="top">
		<form name="frmdistrict" id="frmdistrict" action="?hdl=district/regist" method="post">
			<input type="hidden" name="districtid" id="districtid" value="" />
			<input type="hidden" name="page" id="page" value="{$page}" />
			<input type="hidden" name="status" id="status" value="0" />
			<input type="hidden" name="delete_" id="delete_" value="0" />
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="5"></td>
			</tr>
			{if $exefalse != ''}
			<tr>
				<td colspan="5" style="color: #FF0000;font-weight: bold;">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;">{$exefalse}</td>
					</tr>
					</table>
				</td>
			</tr>
			{/if}
			<tr>
				<td colspan="5" style="padding: 5px;padding-left: 0px;font-weight: bold;">
					<table cellpadding="0" cellspacing="0">
					<tr>
						<td align="left" valign="middle" style="padding-left: 5px;">
							{#DISTRICT_NAME#}&nbsp;:&nbsp;<input type ="text" name="districtname" id="districtname" value="{$searchData.districtname}" class="input_text" style="width: 200px;" />
						</td>
						<td align="left" valign="middle" style="padding-left: 5px;">
							{#PROVINCE_NAME#}&nbsp;:&nbsp;
							<select name="provinceid" id="provinceid" class="dropdown">
								<option value="">{#SELECTED#}</option>
								{foreach item=provinces from=$provinceList}
								<option value="{$provinces.province_id}" {if $searchData.provinceid eq $provinces.province_id}selected="selected"{/if}>{$provinces.province_name}</option>
								{/foreach}
							</select>					
						</td>
						<td align="left" valign="middle" style="padding-left: 5px;">
							{#STATUS#}&nbsp;:&nbsp;
							<select name="districtstatus" id="districtstatus" class="dropdown">
								<option value="">{#SELECTED#}</option>
								{foreach item=status from=$statusList}
								<option value="{$status.value}" {if $searchData.districtstatus eq $status.value && $searchData.districtstatus ne ''}selected="selected"{/if}>{$status.text}</option>
								{/foreach}
							</select>					
						</td>
						<td align="left" valign="middle" style="padding-left: 5px;">
							<input type="button" name="search" id="search" value="{#SEARCH#}" class="button_new" onClick="return submitform('?hdl=district/list', 0, '1', '1');"/>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<!--header-->
			<tr class="stdHeader" valign="top">
				<td width="4%" align="center" valign="middle" style="height:15px;">{#FIELD_ID#}</td>
				<td width="30%" align="center" valign="middle">{#PROVINCE_NAME#}</td>
				<td width="35%" align="center" valign="middle">{#DISTRICT_NAME#}</td>
				<td width="15%" align="center" valign="middle">{#STATUS#}</td>
				<td width="15%" align="center" valign="middle">{#FUNCTION#}</td>
			</tr>
			<!--If cls = 1 then strClass = "record" else strClass = "evenRecord" end if-->
			{if count($districtList) gt 0}
			{foreach key=key item=items from=$districtList}
			{assign var="i" value=$i+1}
			{if $i%2 eq 0}
				{assign var="class" value="record"}
			{else}
				{assign var="class" value="evenRecord"}
			{/if}
			<tr class="{$class}" valign="top">
				<td align="center" style="padding:5px;" valign="middle">{$items.district_id}</td>
				<td align="left" style="padding:5px;" valign="middle">
					{$items.province_name}
				</td>
				<td align="left" style="padding:5px;" valign="middle">
					{$items.district_name}
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					{$statusList[$items.status].text}
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					<table cellpadding="0" cellspacing="0" border="0px;">
					<tr>
					  	<td style="border: 0px;padding-right:5px;"><img src="backend/images/edit.gif" width="15" height="15" title="edit" onClick="return submitform('?hdl=district/regist',{$items.district_id},{$page},'2');" class="button" /></td>
						<td style="border: 0px;padding-left:5px;"><img src="backend/images/delete.jpg" width="15" height="15" title="delete" onClick="return ConfirmDeleteRecord('?hdl=district/list',{$items.district_id},{$page});" class="button" /></td>
					</tr>
					</table>
				</td>
			</tr>
			{/foreach}
			{else}
			<tr valign="top">
				<td colspan="5" align="center" style="padding:5px;" valign="middle">
				{$message}
				</td>
			</tr>
			{/if}
			<tr class="stdHeader" valign="top">
				<td align="center" colspan="5" valign="middle" style="height:25px;">
					{if $count gt 1}
					<div align="center" style="padding-right:5px;font-size:11px;"> <!-- Pager -->
						{$paging}
					</div>
					{/if}
				</td>
			</tr>
			<tr valign="top">
				<td colspan="5" align="center" style="padding:5px;" valign="middle">
					<input type="submit" name="addnew" id="addnew" value="{#REGIST1#}" class="button_new" />
				</td>
			</tr>
			</table>
		</form>
		</td>
{include file=$include_footer}