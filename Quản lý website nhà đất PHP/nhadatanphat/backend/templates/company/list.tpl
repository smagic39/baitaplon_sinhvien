{include file=$include_header}
{literal}
<script language="javascript" type="text/javascript">
	function ConfirmDeleteRecord(url, companyid, page) {
		var theform = document.frmcompany;
		if (confirm('Do you want to delete selected records?')) {
			theform.action = url;
			theform.companyid.value = companyid;
			theform.delete_.value = companyid;
			theform.page.value = page;
			theform.submit();
		} else return false;
	}
	function submitform(url, companyid, page, status) {
		var theform = document.frmcompany;
		theform.action = url;
		theform.companyid.value = companyid;
		theform.page.value = page;
		theform.status.value = status;
		theform.submit();
	}
</script>
{/literal}
{include file=$include_left}
		<td width="80%" align="left" valign="top">
		<form name="frmcompany" id="frmcompany" action="?hdl=company/regist" method="post">
			<input type="hidden" name="companyid" id="companyid" value="" />
			<input type="hidden" name="page" id="page" value="{$page}" />
			<input type="hidden" name="status" id="status" value="0" />
			<input type="hidden" name="delete_" id="delete_" value="0" />
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable"><!---->
				<tr>
					<td height="5px" colspan="7"></td>
				</tr>
				{if $exefalse != ''}
				<tr>
					<td colspan="7" style="color: #FF0000;font-weight: bold;">
						<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
							<td valign="middle" class="error" style="border-left: 0px;">{$exefalse}</td>
						</tr>
						</table>
					</td>
				</tr>
				{/if}
				<tr>
					<td colspan="7" style="padding: 5px;padding-left: 0px;font-weight: bold;">
						<table cellpadding="0" cellspacing="0" border='1'>
						<tr>
							<td align="left" valign="middle" style="padding-left: 5px;">
								{#COMPANY_NAME#}&nbsp;:&nbsp;<input type ="text" name="companyname" id="companyname" value="{$searchData.companyname}" class="input_text" />
							</td>
							<td align="left" valign="middle" style="padding-left: 5px;" colspan=2>
								{#STATUS#}&nbsp;:&nbsp;
								<select name="companystatus" id="companystatus" class="dropdown">
									<option value="">{#SELECTED#}</option>
									{foreach item=status from=$statusList}
									<option value="{$status.value}" {if $searchData.companystatus eq $status.value && $searchData.companystatus ne ''}selected="selected"{/if}>{$status.text}</option>
									{/foreach}
								</select>					
							</td>
						</tr>
						</table>
					</td>
				</tr>
				<tr valign="top">
					<td colspan="7" align="center" style="padding:5px;" valign="middle">
						<input type="button" name="search" id="search" value="{#SEARCH#}" class="button_new" onClick="return submitform('?hdl=company/list', 0, '1', '1');"/>
						<input type="submit" name="addnew" id="addnew" value="{#REGIST#}" class="button_new" />
					</td>
				</tr>
				<!--header-->
				<tr class="stdHeader" valign="top">
					<td width="4%" align="center" valign="middle" style="height:15px;">
						{#FIELD_ID#}
					</td>
					<td width="15%" align="center" valign="middle">{#COMPANY_NAME#}</td>
					<td width="10%" align="center" valign="middle">{#COMPANY_PROVINCE#}</td>
					<!--<td width="40%" align="center" valign="middle">{#COMPANY_CONTENT#}</td>-->
					<td width="15%" align="center" valign="middle">{#COMPANY_WEBSITE#}</td>
					<td width="8%" align="center" valign="middle">{#STATUS#}</td>
					<td width="8%" align="center" valign="middle">{#FUNCTION#}</td>
				</tr>
				<!--If cls = 1 then strClass = "record" else strClass = "evenRecord" end if-->
			{if count($companyList) gt 0}
				{foreach key=key item=items from=$companyList}
					{assign var="i" value=$i+1}
					{if $i%2 eq 0}
						{assign var="class" value="record"}
					{else}
						{assign var="class" value="evenRecord"}
					{/if}
				<tr class="{$class}" valign="top">
					<td align="center" style="padding:5px;" valign="middle">
						{$items.company_id}
					</td>
					
					<td align="left" style="padding:5px;" valign="middle">
						{if $items.is_show_in_homepage eq "1"}<B>{/if}
						{if $items.is_show_in_province eq "1"}<i>{/if}	
						{$items.company_name}
						{if $items.is_show_in_province eq "1"}</i>{/if}
						{if $items.is_show_in_homepage eq "1"}</B>{/if}
					</td>
					<td align="center" style="padding:5px;" valign="middle">
						{$items.province_name}
					</td>
					
					<td align="left" style="padding:5px;" valign="middle">
						{$items.company_website}
					</td>
					
					<td align="center" style="padding:5px;" valign="middle">
						{$statusList[$items.status].text}
					</td>
					<td align="center" style="padding:5px;" valign="middle">
					  	<img src="backend/images/edit.gif" width="15" height="15" title="edit" onClick="return submitform('?hdl=company/regist',{$items.company_id},{$page},'2');" class="button" />
						<img src="backend/images/delete.jpg" width="15" height="15" title="delete" onClick="return ConfirmDeleteRecord('?hdl=company/list',{$items.company_id},{$page});" class="button" />
					</td>
				</tr>
				{/foreach}
				{else}
				<tr valign="top">
					<td colspan="7" align="center" style="padding:5px;" valign="middle">
					{$message}
					</td>
				</tr>
				{/if}
				<tr class="stdHeader" valign="top">
					<td align="center" valign="middle" colspan="7" style="height:25px;">
						{if $count gt 1}
						<div align="center" style="padding-right:5px;font-size:11px;"> <!-- Pager -->
							{$paging}
						</div>
						{/if}
					</td>
				</tr>
			</table>
		</form>
		</td>
{include file=$include_footer}