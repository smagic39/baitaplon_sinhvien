{include file=$include_header} {include file=$include_left}
{literal}
<script language="javascript" type="text/javascript">
	function goto_pages(url_) {
		window.location = url_;
	}
</script>
{/literal}
<td width="80%" align="left" valign="top">
<table width="100%" class="adminform">
	<tr>
	  <td colspan="2" valign="top">
	  <fieldset class="adminform">
		<legend>Config site</legend>
		<form id="frm_edit" name="frm_edit" action="" method="post" enctype="multipart/form-data">
		<input type="hidden" name="config_obj[config_id]" id="config_obj[config_id]" value="{$config_obj.config_id}" />
		<table cellspacing="1" width="100%" class="admintable" bgcolor="#FFFFFF">
		  {if $msg_errors}
		  <tr>
			<td colspan="2" style="padding-left:5px; border-left: 0px;" class="error">{$msg_errors}</td>
		  </tr>
		  {/if}
		  <tr>
			<td class="key">Tiêu đề site</td>
			<td width="80%">
			<textarea name="config_obj[title_site]" id="config_obj[title_site]" rows="3" cols="50">{$config_obj.title_site}</textarea>
		  </tr>
		  <tr>
			<td class="key">Từ khóa tìm kiếm</td>
			<td width="80%">
			<textarea name="config_obj[keyword_site]" id="config_obj[keyword_site]" rows="3" cols="50">{$config_obj.keyword_site}</textarea>
		  </tr>		  
		  <tr>
			<td class="key">Từ khóa mô tả</td>
			<td width="80%">
			<textarea name="config_obj[description_site]" id="config_obj[description_site]" rows="3" cols="50">{$config_obj.description_site}</textarea>
		  </tr>		  
		  
		  <tr>
			<td class="key">Banner</td>
			<td width="80%">
				<input type="file" name="flash_banner" id="flash_banner" class="inputfield" />
				<input type="hidden" name="config_obj[flash_banner]" id="config_obj[flash_banner]" value="{$config_obj.flash_banner}" />
			</td>			
		  </tr>
		  
		  <tr>
			<td class="key">Tỷ giá USD-VNĐ</td>
			<td width="80%">
			<input type="text" name="config_obj[usd_vnd]" id="config_obj[usd_vnd]" size="15" value="{$config_obj.usd_vnd}" maxlength="50"/>
		  </tr>
		  
		  <tr>
			<td class="key">Tỷ giá SJC-VNĐ</td>
			<td width="80%">
			<input type="text" name="config_obj[sjc_vnd]" id="config_obj[sjc_vnd]" size="15" value="{$config_obj.sjc_vnd}" maxlength="50"/>
		  </tr>
		 		 
		 <tr valign="middle" height="35">
			<td align="right"><input type="submit" align="left" name="btn_save" id="btn_add" value="{#UPDATE#}" class="button_new"/></td>
			<td align="left"><input type="button" name="btn_add" id="btn_add" value="{#BACK#}" class="button_new" onclick="javascript:goto_pages('?hdl=config/config_edit');"/></td>
		  </tr>
		</table>			
		</form>
	  </fieldset>      </td>
  </tr>
</table>
</td>
{include file=$include_footer}