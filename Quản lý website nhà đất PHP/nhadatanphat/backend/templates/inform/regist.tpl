{include file=$include_header}
{literal}
<script language="javascript" type="text/javascript">
	function submitform(url, value) {
		var theform = document.frminform;
		theform.action = url;
		theform.issubmit.value = value;
		theform.submit();
	}
	function hiddenform() {
		var theform = document.frminform;
		if (theform.editgeneral.checked) {
			theform.editgeneral.value = 1;
		} else {
			theform.editgeneral.value = 0;
		}
	}
</script>
{/literal}
{include file=$include_left}
		<td width="80%" align="left" valign="top">
		<form name="frminform" id="frminform" action="" method="post" enctype="multipart/form-data">
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			<!--header-->
			{if $message != ""}
			<tr valign="top">
				<td align="left" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;">{$message}</td>
					</tr>
					</table>
				</td>
			</tr>
			{/if}
			<tr class="stdHeader" valign="middle">
				<td style="height:20px;padding-left: 10px;">Update inform</td>
			</tr>
			<tr valign="top">
				<td>
					<input type="hidden" name="informid" id="informid" value="{$informid}" />
					<input type="hidden" name="page" id="page" value="{$page}" />
					<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
						{if $informid != ''}
						<tr valign="top">
							<td width="20%" align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#FIELD_ID#}: </td>
							<td width="55%" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								{$informid}
							</td>
							<td rowspan="4" width="24%" align="center" valign="middle" class="tdregist">
								{if $informData.inform_image != ""}
								<img border="1" style="border-color:#CAD5DB;" width="120px" src="{#IMG_INFORM_DIR#}thumb_{$informData.inform_image}" />
								{/if}
							</td>
						</tr>
						{/if}

					<!--
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#INFORM_CATEGORY#}: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<select name="object[category_id]" id="object[category_id]" class="dropdown">
									{foreach item=categories from=$categoryList}
									<option value="{$categories.category_id}" {if $informData.category_id eq $categories.category_id && $informData.category_id ne ''}selected="selected"{/if}>{$categories.category_name}</option>
									{/foreach}
								</select>					
							</td>
						</tr>
					-->

						<tr valign="top">
							<td width="20%" valign="middle" class="tdregisttext">{#INFORM_NAME#}: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type ="text" name="object[inform_name]" id="object[inform_name]" value="{$informData.inform_name}" class="input_text" />
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">{#INFORM_TITLE#}: </td>
							<td align="left" valign="middle" class="tdregist">
								<textarea name="object[inform_title]" id="object[inform_title]" class="textarea">{$informData.inform_title}</textarea>
							</td>
						</tr>
					
						<tr valign="top">
							<td valign="middle" class="tdregisttext">{#INFORM_IMAGE#}: </td>
							<td colspan="3" align="left" valign="middle" class="tdregist">
								<input type="file" name="inform_image" id="inform_image" class="inputfield" />
								<input type="hidden" name="object[inform_image]" id="object[inform_image]" value="{$informData.inform_image}" />
							</td>
						</tr>
					
						<tr valign="top">
							<td valign="middle" class="tdregisttext">{#INFORM_CONTENT#}: </td>
							<td colspan="3" align="left" valign="middle" class="tdregist">
									{$inform_content}
							</td>
						</tr>
					<!--
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#INFORM_RIGHT#}: </td>
							<td colspan="3" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<select name="object[inform_index]" id="object[inform_index]" class="dropdown">
									{foreach item=shows from=$ShowRight}
									<option value="{$shows.value}" {if $informData.inform_index eq $shows.value && $informData.inform_index ne ''}selected="selected"{/if}>{$shows.text}</option>
									{/foreach}
								</select>
							</td>
						</tr>
					-->
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#STATUS#}: </td>
							<td colspan="3" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<select name="object[status]" id="object[status]" class="dropdown">
									{foreach item=status from=$statusList}
									<option value="{$status.value}" {if $informData.status eq $status.value && $informData.status ne ''}selected="selected"{/if}>{$status.text}</option>
									{/foreach}
								</select>					
							</td>
						</tr>

						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">
								Hiển thị trên trang chủ: 
							</td>
							<td colspan="3" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
									<input type="checkbox" name="object[isShowInHomePage]" 
										{if $informData.isShowInHomePage eq "on" or $informData.isShowInHomePage eq "1"} checked {/if}
									/>
							</td>
						</tr>

					</table>
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td style="height:15px;"></td>
			</tr>
			<tr valign="top">
				<td colspan="2" align="center" valign="middle" style="padding:10px;">
					<input type="button" name="update" id="update" value="{#UPDATE#}" class="button_new" onclick="javascript: submitform('?hdl=inform/regist', 1)" />
					<input type="button" name="isback" id="isback" value="{#BACK#}" class="button_new" onclick="javascript: submitform('?hdl=inform/list', 0)" />
					<input type="hidden" name="issubmit" id="issubmit" value="0" />
				</td>
			</tr>
			</table>
		</form>
		</td>
{include file=$include_footer}