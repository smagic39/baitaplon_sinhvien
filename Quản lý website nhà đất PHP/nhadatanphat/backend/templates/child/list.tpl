{include file=$include_header}
{literal}
<script language="javascript" type="text/javascript">
	function ConfirmDeleteRecord(url, categoryid, page) {
		var theform = document.frmcategory;
		if (confirm('Do you want to delete selected records?')) {
			theform.action = url;
			theform.categoryid.value = categoryid;
			theform.delete_.value = categoryid;
			theform.page.value = page;
			theform.submit();
		} else return false;
	}
	function submitform(url, categoryid, page, status) {
		var theform = document.frmcategory;
		theform.action = url;
		theform.categoryid.value = categoryid;
		theform.page.value = page;
		theform.status.value = status;
		theform.submit();
	}
</script>
{/literal}
{include file=$include_left}
		<td width="80%" align="left" valign="top">
		<form name="frmcategory" id="frmcategory" action="?hdl=child/regist" method="post">
			<input type="hidden" name="categoryid" id="categoryid" value="" />
			<input type="hidden" name="page" id="page" value="{$page}" />
			<input type="hidden" name="status" id="status" value="0" />
			<input type="hidden" name="delete_" id="delete_" value="0" />
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			{if $exefalse != ''}
			<tr>
				<td colspan="6" style="color: #FF0000;font-weight: bold;">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;">{$exefalse}</td>
					</tr>
					</table>
				</td>
			</tr>
			{/if}
			<!--header-->
			<tr valign="top">
				<td colspan="6" align="center" style="padding:5px;" valign="middle">
					<input type="submit" name="addnew" id="addnew" value="{#REGIST#}" class="button_new" />
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td width="4%" align="center" valign="middle" style="height:15px;">{#FIELD_ID#}</td>
				<td width="17%" align="center" valign="middle">Tên danh mục<!--{#CATEGORY_NAME#} & {#CHILD_NAME#}--></td>
				<td width="8%" align="center" valign="middle">{#CHILD_SORT#}</td>
				<td width="10%" align="center" valign="middle">{#STATUS#}</td>
				<td width="10%" align="center" valign="middle">{#FUNCTION#}</td>
			</tr>
			<!--If cls = 1 then strClass = "record" else strClass = "evenRecord" end if-->
			{if count($categoryList) gt 0}
			{foreach key=key item=items from=$categoryList}
			{assign var="i" value=$i+1}
			{if $i%2 eq 0}
				{assign var="class" value="record"}
			{else}
				{assign var="class" value="evenRecord"}
			{/if}
			<tr class="{$class}" valign="top">
				<td align="center" style="padding:5px;" valign="middle">{$items.child_id}</td>
				<td align="left" style="padding:5px;" valign="middle">
					{$items.category_name} {$items.child_name}
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					{$items.child_sort}
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					{$statusList[$items.status].text}
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					<table cellpadding="0" cellspacing="0" border="0px;">
					<tr>
					  	<td style="border: 0px;padding-right:5px;"><img src="backend/images/edit.gif" width="15" height="15" title="edit" onClick="return submitform('?hdl=child/regist',{$items.child_id},0,'2');" class="button" /></td>
						<td style="border: 0px;padding-left:5px;"><img src="backend/images/delete.jpg" width="15" height="15" title="delete" onClick="return ConfirmDeleteRecord('?hdl=child/list',{$items.child_id},0);" class="button" /></td>
					</tr>
					</table>
				</td>
			</tr>
			{/foreach}
			{else}
			<tr valign="top">
				<td colspan="6" align="center" style="padding:5px;" valign="middle">
				{$message}
				</td>
			</tr>
			{/if}
			<tr class="stdHeader" valign="top">
				<td align="center" colspan="6" style="height:25px;">
					{if $count gt 1}
					<div align="center" style="padding-right:5px;font-size:11px;"> <!-- Pager -->
						{$paging}
					</div>
					{/if}
				</td>
			</tr>
<!--
			<tr valign="top">
				<td colspan="6" align="center" style="padding:5px;" valign="middle">
					<input type="submit" name="addnew" id="addnew" value="{#REGIST#}" class="button_new" />
				</td>
			</tr>
-->
			</table>
		</form>
		</td>
{include file=$include_footer}