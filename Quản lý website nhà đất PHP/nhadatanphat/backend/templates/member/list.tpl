{include file=$include_header}
{literal}
<script language="javascript" type="text/javascript">
	function ConfirmDeleteRecord(url, memberid, page) {
		var theform = document.frmmember;
		if (confirm('Do you want to delete selected records?')) {
			theform.action = url;
			theform.memberid.value = memberid;
			theform.delete_.value = memberid;
			theform.page.value = page;
			theform.submit();
		} else return false;
	}
	function submitform(url, memberid, page, status) {
		var theform = document.frmmember;
		theform.action = url;
		theform.memberid.value = memberid;
		theform.page.value = page;
		theform.status.value = status;
		theform.submit();
	}
</script>
{/literal}
{include file=$include_left}
		<td width="80%" align="left" valign="top">
		<form name="frmmember" id="frmmember" action="?hdl=member/regist" method="post">
			<input type="hidden" name="memberid" id="memberid" value="" />
			<input type="hidden" name="page" id="page" value="{$page}" />
			<input type="hidden" name="status" id="status" value="0" />
			<input type="hidden" name="delete_" id="delete_" value="0" />
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="10"></td>
			</tr>
			{if $exefalse != ''}
			<tr>
				<td colspan="9" style="color: #FF0000;font-weight: bold;">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;">{$exefalse}</td>
					</tr>
					</table>
				</td>
			</tr>
			{/if}
			<tr>
				<td colspan="9" style="padding: 5px;padding-left: 0px;font-weight: bold;">
					<table cellpadding="0" cellspacing="0">
						<tr>
							<td align="left" valign="middle" style="padding-left: 5px;" colspan="3">
								{#REGIST_DATE#}&nbsp;:&nbsp;{$startdate}&nbsp;-&nbsp;{$enddate}
							</td>
						</tr>
						<tr>
							<td align="left" valign="middle" style="padding-left: 5px;" colspan="2">
								Tên thành viên:&nbsp;
								<input name="memberName" value="{$searchData.memberName}" />
							</td>
							<td align="left" valign="middle" style="padding-left: 5px;">
								{#STATUS#}&nbsp;:&nbsp;
								<select name="memberstatus" id="memberstatus" class="dropdown">
									<option value="">{#SELECTED#}</option>
									{foreach item=status from=$statusList}
									<option value="{$status.value}" {if $searchData.memberstatus eq $status.value &&  $searchData.memberstatus ne ''}selected="selected"{/if}>{$status.text}</option>
									{/foreach}
								</select>					
							</td>
						</tr>
						<tr>
							<td align="center" valign="middle" style="padding-left: 5px;" colspan="3">
								<input type="submit" name="search" id="search" value="{#SEARCH#}" class="button_new" onClick="return submitform('?hdl=member/list', 0, '1', '1');"/>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<!--header-->
			<tr class="stdHeader" valign="top">
				<td width="4%" align="center" valign="middle" style="height:15px;">{#FIELD_ID#}</td>
				<td width="10%" align="center" valign="middle">{#MEMBER_USER#}</td>
				<td width="15%" align="center" valign="middle">{#MEMBER_NAME#}</td>
				<td width="30%" align="center" valign="middle">{#MEMBER_ADDRESS#}</td>
				<td width="12%" align="center" valign="middle">{#MEMBER_MAIL#}</td>
				<td width="10%" align="center" valign="middle">{#MEMBER_PHONE#}</td>
				<td width="10%" align="center" valign="middle">{#STATUS#}</td>
				<td width="8%" align="center" valign="middle">{#FUNCTION#}</td>
			</tr>
			<!--If cls = 1 then strClass = "record" else strClass = "evenRecord" end if-->
			{if count($memberList) gt 0}
			{foreach key=key item=items from=$memberList}
			{assign var="i" value=$i+1}
			{if $i%2 eq 0}
				{assign var="class" value="record"}
			{else}
				{assign var="class" value="evenRecord"}
			{/if}
			<tr class="{$class}" valign="top">
				<td align="center" style="padding:5px;" valign="middle">{$items.member_id}</td>
				<td align="left" style="padding:5px;" valign="middle">
					{$items.member_user}
				</td>
				<td align="left" style="padding:5px;" valign="middle">
					{$items.member_name}
				</td>
				<td align="left" style="padding:5px;" valign="middle">
					{$items.member_address}
				</td>
				<td align="left" style="padding:5px;" valign="middle">
					<a href="mailto:{$items.member_mail}">{$items.member_mail}</a>
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					{$items.member_phone}
					<!--{$items.created_date|date_format:"%d-%m-%Y"}-->
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					{$statusList[$items.status].text}
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					<table cellpadding="0" cellspacing="0" border="0px;">
					<tr>
					  	<td style="border: 0px;padding-right:5px;"><img src="backend/images/edit.gif" width="15" height="15" title="edit" onClick="return submitform('?hdl=member/regist',{$items.member_id},{$page},'2');" class="button" /></td>
						<td style="border: 0px;padding-left:5px;"><img src="backend/images/delete.jpg" width="15" height="15" title="delete" onClick="return ConfirmDeleteRecord('?hdl=member/list',{$items.member_id},{$page});" class="button" /></td>
					</tr>
					</table>
				</td>
			</tr>
			{/foreach}
			{else}
			<tr valign="top">
				<td colspan="9" align="center" style="padding:5px;" valign="middle">
				{$message}
				</td>
			</tr>
			{/if}
			<tr class="stdHeader" valign="top">
				<td align="center" colspan="9" valign="middle" style="height:25px;">
					{if $count gt 1}
					<div align="center" style="padding-right:5px;font-size:11px;"> <!-- Pager -->
						{$paging}
					</div>
					{/if}
				</td>
			</tr>
			</table>
		</form>
		</td>
{include file=$include_footer}