{include file=$include_header} {include file=$include_left}
<td width="80%" align="left" valign="top">
	<table width="100%" border="1" class="adminform" cellpadding="3"
		cellspacing="3">
		<tr>
			<td colspan="2" height="35" class="textdetail"
				style="padding-left: 5px;padding-top: 15px; padding-bottom: 5px;">
				<a href="?hdl=interior/list"><b>{#INTERIOR_MANAGER#}</b>
				</a> |
				<a href="?hdl=interior/categories_list"><b>{#CAT_INTERIOR_MANAGER#}</b>
				</a>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center" valign="top">
				<fieldset>
					<legend>
						{#CAT_INTERIOR_LIST#}
					</legend>
					<form id="frm_categories" name="frm_categories" method="post"
						action="" enctype="application/x-www-form-urlencoded">
						<div>
							<input type="text" name="key_word" class="input_text" size="25"
								maxlength="40" value="{$key_word|escape}" />
							<input type="submit" name="btn_search" class="button_new"
								value="{#SEARCH#}" />
							<br /><br />							
						</div>
						<table width="100%" border="1" class="dataTable">
							<thead>
								<tr class="stdHeader">
									<td width="5%" align="center">
										Order									</td>
									<td>
										{#LBL_CAT_NAME#}									</td>
									<td width="20%" align="center">
										{#LBL_CAT_DESC#}									</td>
									<td width="10%" align="center">
										{#LBL_CAT_ORDER#}									</td>
									<td width="10%" align="center">
										{#STATUS#}									</td>
									<td width="15%" align="center">
										{#FUNCTION#}									</td>
								</tr>
							</thead>
							<tbody>
								{foreach from=$categories_list item=categories_obj key=order_no}
								<tr {if $categories_obj.parent_id>
									0} class="record" {else} class="evenRecord" {/if}>
									<td align="center">
										{$start+$order_no}									</td>
									<td valign="top">
										{$categories_obj.category_name|escape|indent:$categories_obj.level:"&nbsp;&nbsp;&nbsp;&nbsp;":"&#166;--&nbsp;"}									</td>
									<td>
										{$categories_obj.category_description|escape:"html"}									</td>
									<td align="center">
										{$categories_obj.category_order|escape:"html"}									</td>
									<td align="center">
										{$statusList[$categories_obj.category_active].text}									</td>
									<td align="center">
										<table cellpadding="0" cellspacing="0" border="0px;">
										<tr>
										  	<td style="border: 0px;padding-right:5px;">
										  		<a href={$links_params|cat:$categories_obj.category_id}>
													<img src="backend/images/edit.gif" width="15" height="15"
														class="button" title="Edit" /> </a>
										  	</td>
											<td style="border: 0px;padding-left:5px;">
												<a 	href={$links_params_del|cat:$categories_obj.category_id}>
													<img src="backend/images/delete.jpg" width="15" height="15"
														title="Delete" class="button" /> </a>
											</td>
										</tr>
										</table>
									</td>
							    </tr>
								{/foreach}
							</tbody>
							<tfoot>
							
							<tr class="stdHeader" valign="top" colspan="6">
							<td align="center" colspan="6" valign="middle" style="height:25px;">
								<div align="center" style="padding-right:5px;font-size:11px;"> <!-- Pager -->
									{$pages_list}
								</div>								
							</td>
						</tr>							
								<tr>
									<td colspan="6" align="right" valign="middle">
										<input type="button" name="btn_add" id="btn_add"
											value="{#REGIST1#}" class="button_new"
											onclick="window.location='?hdl=interior/categories_edit'" />									</td>
								</tr>								
							</tfoot>
					  </table>
					</form>
				</fieldset>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center" valign="top">&nbsp;
				
			</td>
		</tr>
	</table>
</td>
{include file=$include_footer}
