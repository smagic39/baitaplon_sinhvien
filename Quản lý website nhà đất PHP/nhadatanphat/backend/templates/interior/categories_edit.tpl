{include file=$include_header} {include file=$include_left}
{literal}
<script language="javascript" type="text/javascript">
	function goto_pages(url_) {
		window.location = url_;
	}
</script>
{/literal}
<td width="80%" align="left" valign="top">
<table width="100%" class="adminform">
	<tr>
	  <td  height="35" class="textdetail" style="padding-left: 5px;padding-top: 15px; padding-bottom: 5px;">
	  	<a href="?hdl=interior/list"><b>{#INTERIOR_MANAGER#}</b></a> | <a href="?hdl=interior/categories_list"><b>{#CAT_INTERIOR_MANAGER#}</b></a>	  
	  </td>
    </tr>
	<tr>
	  <td colspan="2" valign="top">
	  <fieldset class="adminform">
		<legend>{#LBL_CAT_ADD#}</legend>
		<form id="frm_edit" name="frm_edit" action="" method="post" enctype="multipart/form-data">
		<input type="hidden" name="category_id" id="category_id" value="{$categories_obj.category_id}" />
		{if $msg_system_errors == ''}
		<input type="hidden" name="categories_obj[category_id]" id="categories_obj[category_id]" value="{$categories_obj.category_id}" />
		<table cellspacing="1" width="100%" class="admintable" bgcolor="#FFFFFF">
		  {if $msg_errors}
		  <tr>
			<td colspan="2" style="padding-left:5px;" class="error">{$msg_errors}</td>
		  </tr>
		  {/if}
		  <tr>
			<td class="key">{#LBL_CAT_NAME#}</td>
			<td width="80%"><input type="text" name="categories_obj[category_name]" value="{$categories_obj.category_name}" size="25" maxlength="255" /></td>
		  </tr>          
		  <tr>
			<td class="key">{#LBL_CAT_SUB#}</td>
			<td>
			<select name="categories_obj[parent_id]">
            <option value="0">{#LBL_MAIN_MENU#}</option>
            {foreach from=$category_parent_id_list item="cat" }
			{if	$cat.id_path|strpos:"`$categories_obj.id_path`/" === false && $cat.category_id != $categories_obj.category_id || $categories_obj.category_id == ''}
			<option	value="{$cat.category_id}" {if $categories_obj.parent_id eq $cat.category_id}selected{/if}>{$cat.category_name|indent:$cat.level:"&#166;&nbsp;&nbsp;&nbsp;&nbsp;":"&#166;--&nbsp;&nbsp;"}</option>
			{/if}
			{/foreach}
            </select>            </td>
		  </tr>
                    
		  <tr>
			<td class="key">{#LBL_CAT_ORDER#}</td>
			<td><input type="text" name="categories_obj[category_order]" value="{$categories_obj.category_order}" size="5" maxlength="5" /></td>
		  </tr>
		  
		  <tr>
			<td class="key">{#STATUS#}</td>
			<td>
			<select name="categories_obj[category_active]" class="dropdown">
			{foreach item=status from=$statusList}
			<option value="{$status.value}" {if $categories_obj.category_active eq $status.value && $categories_obj.category_active ne ''}selected="selected"{/if}>{$status.text}</option>
			{/foreach}
			</select>
			
			</td>
		  </tr>
		  <tr valign="middle">
			<td align="right"><input type="submit" align="left" name="btn_save" id="btn_add" value="{#UPDATE#}" class="button_new"/></td>
			<td align="left"><input type="button" name="btn_add" id="btn_add" value="{#BACK#}" class="button_new" onclick="javascript:goto_pages('{$link_back}');"/></td>
		  </tr>
		</table>
		{else}
		<div align="center" class="nt_txt_validator">{$msg_system_errors}</div>
		{/if}	
		</form>
	  </fieldset>      </td>
  </tr>
</table>
</td>
{include file=$include_footer}