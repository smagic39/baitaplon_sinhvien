{include file=$include_header}
{literal}
<script language="javascript" type="text/javascript">
	function ConfirmDeleteRecord(url, accountid, page) {
		var theform = document.frmaccount;
		if (confirm('Do you want to delete selected records?')) {
			theform.action = url;
			theform.accountid.value = accountid;
			theform.page.value = page;
			theform.submit();
		} else return false;
	}
	function submitform(url, accountid, page) {
		var theform = document.frmaccount;
		theform.action = url;
		theform.accountid.value = accountid;
		theform.page.value = page;
		theform.submit();
	}
</script>
{/literal}
{include file=$include_left}
		<td width="80%" align="left" valign="top">
		<form name="frmaccount" id="frmaccount" action="?hdl=auth/regist" method="post">
			<input type="hidden" name="accountid" id="accountid" value="" />
			<input type="hidden" name="page" id="page" value="{$page}" />
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="7"></td>
			</tr>
			<!--header-->
			<tr class="stdHeader" valign="top">
				<td width="4%" align="center" valign="middle" style="height:15px;">{#ACCOUNT_ID#}</td>
				<td width="10%" align="center" valign="middle">{#ACCOUNT_NAME#}</td>
				<td width="15%" align="center" valign="middle">{#ACCOUNT_FULL_NAME#}</td>
				<td width="15%" align="center" valign="middle">{#ACCOUNT_EMAIL#}</td>
				<td width="30%" align="center" valign="middle">{#ACCOUNT_ADDRESS#}</td>
				<td width="10%" align="center" valign="middle">{#STATUS#}</td>
				<td width="10%" align="center" valign="middle">{#FUNCTION#}</td>
			</tr>
			<!--If cls = 1 then strClass = "record" else strClass = "evenRecord" end if-->
			{if count($accountList) gt 0}
			{foreach key=key item=items from=$accountList}
			{assign var="i" value=$i+1}
			{if $i%2 eq 0}
				{assign var="class" value="record"}
			{else}
				{assign var="class" value="evenRecord"}
			{/if}
			<tr class="{$class}" valign="top">
				<td align="center" style="padding:5px;" valign="middle">{$items.account_id}</td>
				<td align="left" style="padding:5px;" valign="middle">
					{$items.account_name}
				</td>
				<td align="left" style="padding:5px;" valign="middle">
					{$items.account_full_name}
				</td>
				<td align="left" style="padding:5px;" valign="middle">
					{$items.account_email}
				</td>
				<td align="left" style="padding:5px;" valign="middle">
					{$items.account_address}
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					<input type="checkbox" name="status[{$items.account_id}]" id="status" {if $items.status eq 1}checked="true"{/if} onClick="return submitform('?hdl=auth/list',{$items.account_id},{$page});"/>
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					<table cellpadding="0" cellspacing="0" border="0px;">
					<tr>
					  	<td style="border: 0px;padding-right:5px;"><img src="backend/images/edit.gif" width="15" height="15" title="edit" onClick="return submitform('?hdl=auth/regist',{$items.account_id},{$page});" class="button" /></td>
						<td style="border: 0px;padding-left:5px;"><img src="backend/images/delete.jpg" width="15" height="15" title="delete" onClick="return ConfirmDeleteRecord('?hdl=auth/list&delete=1',{$items.account_id},{$page});" class="button" /></td>
					</tr>
					</table>
				</td>
			</tr>
			{/foreach}
			{else}
			<tr valign="top">
				<td colspan="7" align="center" style="padding:5px;" valign="middle">
				{$message}
				</td>
			</tr>
			{/if}
			<tr class="stdHeader" valign="top">
				<td align="center" colspan="7" style="height:15px;">
					{if $count gt 1}
					<div align="center" style="padding-right:5px;font-size:11px;"> <!-- Pager -->
						{$paging}
					</div>
					{/if}
				</td>
			</tr>
			<tr valign="top">
				<td colspan="7" align="center" style="padding:5px;" valign="middle">
					<input type="submit" name="addnew" id="addnew" value="{#REGIST1#}" class="button_new" />
				</td>
			</tr>
			</table>
		</form>
		</td>
{include file=$include_footer}