<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: regist.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		require_once BACKEND_PAGE_PATH.DS."include".DS."categories.php";
		$smarty->config_load("estate.conf");
		
		$estateid  	= isset($_REQUEST["estateid"]) ? 	trim($_REQUEST["estateid"])	: null;
		$page	  	= isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 		: 1;
		$issubmit 	= isset($_POST["issubmit"]) ? 		trim($_POST["issubmit"]) 		: null;
		
		//
		
		$message  	= null;
		$estateDao 	= new EstateDao();
		$ChildDao	= new ChildDao();
		$categories	= new Categories();
		$provinceDao = new ProvinceDao();
		$districtDao = new DistrictDao();
		
		$province_list = $provinceDao->getAllProvinces();
		$smarty->assign("province_list", 	$province_list);
			
		$district_list = $districtDao->getAllDistricts();
		
		$estateData	= array();

		if (isset($_POST["object"])) {
			$estateData 	= $_POST["object"];
		} elseif ($estateid) {
			$estateData = $estateDao->getEstateById($estateid);
		}

		if ($issubmit) { // Is edit data
			//$estateData 	= isset($_POST["object"]) ? $_POST["object"]: null;

			if ($message == "") {
				$estateDao->beginTrans();
				try {
					$objects = $estateData;
					if (!is_null($estateid) && $estateid != "") $estateDao->update_estate($objects, $estateid);
					$estateDao->commitTrans();
					header("Location: ?hdl=estate/list&page=$page");
				} catch (Exception $ex) {
					$estateDao->rollbackTrans();
					if (!is_null($estateid) && $estateid != "") $message .= MESSAGE_UPDATE_FALSE.BR_TAG;
					else $message .= MESSAGE_INSERT_FALSE.BR_TAG;
				}
			}
		}
		$categoryNames = $categories->getCategoryName($ChildDao, $estateData["child_id"]);
		$categoryNames = array_values($categoryNames);
		for ($i = count($categoryNames); $i > 0 ; $i--)
			$categoryName .= " => ".$categoryNames[$i-1];

		$smarty->assign("estateData", 	$estateData);
		if(file_exists(LOGO_PATH.$estateData['member_id'].".jpg")){
			$smarty->assign("logoImg", $estateData['member_id'].".jpg");
		}
		
		$statusList = Status::getList($textlang);
		$statusArr = array(); 
		foreach($statusList as $row){
			$statusArr[$row['value']] = $row['text'];
		}
		
		$genreList = EstateGenre::getList($textlang);
		$genreArr = array(); 
		foreach($genreList as $row){
			$genreArr[$row['value']] = $row['text'];
		}

		$smarty->assign("statusList",  	$statusArr);
		$smarty->assign("genreList",  	$genreArr);
		$smarty->assign("directionList",Direction::getList($textlang));
		$smarty->assign("categoryName",	$categoryName);
		$smarty->assign("estateid",   	$estateid);
		$smarty->assign("page",   		$page);
		$smarty->assign("message", 		$message);
		$estateDao->doClose();
		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>