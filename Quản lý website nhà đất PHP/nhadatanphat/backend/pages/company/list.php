<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: list.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("company.conf");
		
		$page	 	= isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 		: null;
		$companyid	= isset($_REQUEST["companyid"]) ? 	trim($_REQUEST["companyid"])	: null;
		$status	 	= isset($_POST["status"]) ? 		trim($_POST["status"]) 			: null;
		$delete	 	= isset($_POST["delete_"]) ? 		trim($_POST["delete_"]) 		: null;

		if (is_null($status) && !strlen($page) && isset($_SESSION["SEARCH_COMPANY"])) $_SESSION["SEARCH_COMPANY"] = null;
		if (!empty($status)) {
			$_SESSION["SEARCH_COMPANY"]["companyname"]	= isset($_REQUEST["companyname"]) ? 	trim($_REQUEST["companyname"])		: null;
			$_SESSION["SEARCH_COMPANY"]["companystatus"]= isset($_REQUEST["companystatus"]) ? 	trim($_REQUEST["companystatus"])	: null;
		}
		if (!isset($_SESSION["SEARCH_COMPANY"])) $_SESSION["SEARCH_COMPANY"] = null;
		if (empty($page)) $page = 1;

		$companyDao = new companyDao();
		if (!empty($delete)) {
			$companyData 	= $companyDao->getCompanyById($companyid);
			$companyDao->beginTrans();
			try {
				$companyDao->delete_company($companyid);
				$companyDao->commitTrans();
				FileUtils::deleteFile(IMG_PATH.DS."company".DS, $companyData["company_logo"]);
			} catch (Exception $ex) {
				$companyDao->rollbackTrans();
				$smarty->assign("exefalse", MESSAGE_DELETE_FALSE);
			}
		}
		$offset 	   	= (int)($page-1)*LIMIT_RECORD;
		$count		   	= $companyDao->getCountCompany($_SESSION["SEARCH_COMPANY"]["companyname"], $_SESSION["SEARCH_COMPANY"]["companystatus"]);
		if ($offset >= $count) {
			$offset		= (int)$offset - (int)LIMIT_RECORD;
			$page		= $page - 1;
		}
		$companyList = $companyDao->getAllCompanies($_SESSION["SEARCH_COMPANY"]["companyname"], $_SESSION["SEARCH_COMPANY"]["companystatus"], (int)LIMIT_RECORD, $offset);
		foreach ($companyList as $key => $values) $companyList[$key]["company_content"] 	= Utils::cutTitle($values["company_content"]);

		//$smarty->assign("paging",   		PagerUtils::PagerSmarty($page, $count, "?hdl=company/list", (int)LIMIT_RECORD, (int)PAGES_SIZES));
		$pageAll = PagerUtils::PagerPear($count, $page, LIMIT_RECORD, array());
		$smarty->assign("paging",			$pageAll["all"]);
		$smarty->assign("companyList",   	$companyList);
		$smarty->assign("statusList",  		Status::getList($textlang));
		$smarty->assign("count", 			$count);
		$smarty->assign("page", 			$page);
		$smarty->assign("searchData", 		$_SESSION["SEARCH_COMPANY"]);
		$smarty->assign("message", 			MESSAGE_RESULT_NULL);

		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>