<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: left.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined('DS') or die("Errors System");
	$hdl = isset($_GET["hdl"]) ? trim($_GET["hdl"]): "index";
	list($mod) = split('[/]', $hdl);
	$smarty->assign("mod", 	$mod);
?>
