<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: list.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("province.conf");
		
		$page	 	= isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 		: null;
		$provinceid	= isset($_REQUEST["provinceid"]) ? 	trim($_REQUEST["provinceid"])	: null;
		$status	 	= isset($_POST["status"]) ? 		trim($_POST["status"]) 			: null;
		$delete	 	= isset($_POST["delete_"]) ? 		trim($_POST["delete_"]) 		: null;

		if (is_null($status) && !strlen($page) && isset($_SESSION["SEARCH_PROVINCE"])) $_SESSION["SEARCH_PROVINCE"] = null;
		if (!empty($status)) {
			$_SESSION["SEARCH_PROVINCE"]["provincename"]	= isset($_REQUEST["provincename"]) ? 	trim($_REQUEST["provincename"])		: null;
			$_SESSION["SEARCH_PROVINCE"]["provincestatus"]	= isset($_REQUEST["provincestatus"]) ? 	trim($_REQUEST["provincestatus"])	: null;
		}
		if (!isset($_SESSION["SEARCH_PROVINCE"])) $_SESSION["SEARCH_PROVINCE"] = null;
		if (empty($page)) $page = 1;

		$provinceDao = new ProvinceDao();
		if (!empty($delete)) {
			$provinceDao->beginTrans();
			try {
				$provinceDao->delete_province($provinceid);
				$provinceDao->commitTrans();
			} catch (Exception $ex) {
				$provinceDao->rollbackTrans();
				$smarty->assign("exefalse", MESSAGE_DELETE_FALSE);
			}
		}
		$offset 	   	= (int)($page-1)*LIMIT_RECORD;
		$count		   	= $provinceDao->getCountProvince($_SESSION["SEARCH_PROVINCE"]["provincename"], $_SESSION["SEARCH_PROVINCE"]["provincestatus"]);
		if ($offset >= $count) {
			$offset		= (int)$offset - (int)LIMIT_RECORD;
			$page		= $page - 1;
		}
		$provinceList = $provinceDao->getAllProvinces($_SESSION["SEARCH_PROVINCE"]["provincename"], $_SESSION["SEARCH_PROVINCE"]["provincestatus"], (int)LIMIT_RECORD, $offset);

		//$smarty->assign("paging",   		PagerUtils::PagerSmarty($page, $count, "?hdl=province/list", (int)LIMIT_RECORD, (int)PAGES_SIZES));
		$pageAll = PagerUtils::PagerPear($count, $page, LIMIT_RECORD, array());
		$smarty->assign("paging",			$pageAll["all"]);
		$smarty->assign("provinceList",   	$provinceList);
		$smarty->assign("statusList",  		Status::getList($textlang));
		$smarty->assign("count", 			$count);
		$smarty->assign("page", 			$page);
		$smarty->assign("searchData", 		$_SESSION["SEARCH_PROVINCE"]);
		$smarty->assign("message", 			MESSAGE_RESULT_NULL);

		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>