<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: regist.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("account.conf");
		
		$accountid   = isset($_REQUEST["accountid"]) ? 	trim($_REQUEST["accountid"]): null;
		$page	  	 = isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 	: 1;
		$issubmit 	 = isset($_POST["issubmit"]) ? 		trim($_POST["issubmit"]) 	: null;
		$message  	 = null;
		$accountData = array();
		$accountDao  = new AccountDao();

		$permissionList = UserRole::getRoleList();
		$permissionList = $permissionList[$_SESSION["_LOGIN_BACKEND_"]['role_id']];
		if(is_array($permissionList) && count($permissionList) > 0)
			$accountid = $_SESSION["_LOGIN_BACKEND_"]['account_id'];
		
		if (is_null($issubmit)) { // Is load default
			if ($accountid) $accountData = $accountDao->getAccountById((int)$accountid);
		} else if ($issubmit) { // Is edit data
			$accountid   			= isset($_POST["account_id"]) ? 			trim($_POST["account_id"]) 			: null;
			$account_name  			= isset($_POST["account_name"]) ? 			trim($_POST["account_name"]) 		: null;
			$account_pass  			= isset($_POST["account_pass"]) ? 			trim($_POST["account_pass"]) 		: null;
			$account_pass_confirm  	= isset($_POST["account_pass_confirm"]) ? 	trim($_POST["account_pass_confirm"]): null;
			$account_full_name		= isset($_POST["account_full_name"]) ? 		trim($_POST["account_full_name"]) 	: null;
			$account_sex  			= isset($_POST["account_sex"]) ? 			trim($_POST["account_sex"]) 		: null;
			$account_email  		= isset($_POST["account_email"]) ? 			trim($_POST["account_email"]) 		: null;
			$account_address  		= isset($_POST["account_address"]) ? 		trim($_POST["account_address"]) 	: null;
			$account_phone  		= isset($_POST["account_phone"]) ? 			trim($_POST["account_phone"]) 		: null;
			$account_mobile  		= isset($_POST["account_mobile"]) ? 		trim($_POST["account_mobile"]) 		: null;
			$account_status   		= isset($_POST["status"]) ? 				trim($_POST["status"]) 				: null;
			$role_id		   		= isset($_POST["role_id"]) ? 				trim($_POST["role_id"]) 				: null;

			$accountData["account_name"] 		= $account_name;
			$accountData["account_full_name"] 	= $account_full_name;
			$accountData["account_sex"] 		= (int)$account_sex;
			$accountData["account_email"] 		= $account_email;
			$accountData["account_address"] 	= $account_address;
			$accountData["account_phone"] 		= $account_phone;
			$accountData["account_mobile"] 		= $account_mobile;
			$accountData["status"] 				= $account_status;
			$accountData["role_id"] 			= $role_id;
			if (strlen($account_pass)) $accountData["account_pass"] = md5($account_pass);

			if ($account_pass == "" && $account_pass_confirm == "" && !empty($accountid)) {
				$account_pass 			= "123456";
				$account_pass_confirm 	= "123456";
			}
		 	$validate = new validate("account_validate", ACCOUNT_VALIDATION_FILE);	 	
			$message .= $validate->execute($account_name, $account_pass, $account_full_name, $account_email, $account_address, $account_phone, $account_mobile);

			if ($message == "") {
				$checkexisted = $accountDao->checkExistedAccountName($account_name, $account_id);
				if ($checkexisted) $message .= ACCOUNT_NAME_EXISTED;
				if ($account_pass != $account_pass_confirm) $message .= ACCOUNT_PASS_CONFIRM_ERROR;
			} 
			
			if ($message == "") {
				if($accountid == 1){
					$accountData['role_id'] = UserRole::admin;
					$accountData['status'] = Status::active;
				}
				
				$accountDao->beginTrans();
				try {
					if (!is_null($accountid) && $accountid != "") { // Is update data
						$accountDao->update_account($accountData, $accountid);
						$accountDao->commitTrans();
						if ($accountid == $account_info_["account_id"]) header("Location: ?hdl=login");
						else header("Location: ?hdl=auth/list&page=$page");
					} else { // Is regist date
						$accountDao->insert_account($accountData);
						$accountDao->commitTrans();
						header("Location: ?hdl=auth/list&page=$page");
					}
				} catch (Exception $ex) {
					$accountDao->rollbackTrans();
				}
			}
		} else { // Is back
			header("Location: ?hdl=auth/list&page=$page");
		}

		$smarty->assign("accountData",  $accountData);
		$smarty->assign("accountid",   	$accountid);
		$smarty->assign("sexList",  	SexFlag::getList($textlang));
		$smarty->assign("userRole",  	UserRole::getList($textlang));
		$smarty->assign("page",   		$page);
		$smarty->assign("message", 		$message);
		$accountDao->doClose();
		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>