<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: #core.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("account.conf");
		
		$accountid   = isset($_GET["accountid"]) ? 	trim($_GET["accountid"]) : null;
		$issubmit 	 = isset($_POST["issubmit"]) ? 	trim($_POST["issubmit"]) : null;
		$message  	 = null;
		$accountData = array();
		$accountDao  = new AccountDao();
		
		$permissionList = UserRole::getRoleList();
		$permissionList = $permissionList[$_SESSION["_LOGIN_BACKEND_"]['role_id']];
		if(is_array($permissionList) && count($permissionList) > 0)
			$accountid = $_SESSION["_LOGIN_BACKEND_"]['account_id'];
		
		if (is_null($issubmit)) { // Is load default
			if ($accountid) {
				$accountData = $accountDao->getAccountById((int)$accountid);
			}
		} else if ($issubmit) { // Is edit data
			$account_name  		= isset($_POST["account_name"]) ? 		trim($_POST["account_name"]) 		: null;
			$account_pass  		= isset($_POST["account_pass"]) ? 		trim($_POST["account_pass"]) 		: null;
			$account_full_name	= isset($_POST["account_full_name"]) ? 	trim($_POST["account_full_name"]) 	: null;
			$account_sex  		= isset($_POST["account_sex"]) ? 		trim($_POST["account_sex"]) 		: null;
			$account_email  	= isset($_POST["account_email"]) ? 		trim($_POST["account_email"]) 		: null;
			$account_address  	= isset($_POST["account_address"]) ? 	trim($_POST["account_address"]) 	: null;
			$account_phone  	= isset($_POST["account_phone"]) ? 		trim($_POST["account_phone"]) 		: null;
			$account_mobile  	= isset($_POST["account_mobile"]) ? 	trim($_POST["account_mobile"]) 		: null;
			$status   			= isset($_POST["statushidden"]) ? 		trim($_POST["statushidden"]) 		: null;
			
			$accountData["account_name"] 		= $account_name;
			$accountData["account_pass"] 		= $account_pass;
			$accountData["account_full_name"] 	= $account_full_name;
			$accountData["account_sex"] 		= (int)$account_sex;
			$accountData["account_email"] 		= $account_email;
			$accountData["account_address"] 	= $account_address;
			$accountData["account_phone"] 		= $account_phone;
			$accountData["account_mobile"] 		= $account_mobile;
			$accountData["status"] 				= ($status != "") ? 1 : 0;

			$accountid   			= isset($_POST["account_id"]) ? 			trim($_POST["account_id"]) 			: null;
			$account_pass_old 		= isset($_POST["account_pass_old"]) ? 		trim($_POST["account_pass_old"]) 	: null;
			$account_pass_new 		= isset($_POST["account_pass_new"]) ? 		trim($_POST["account_pass_new"]) 	: null;
			$account_pass_confirm  	= isset($_POST["account_pass_confirm"]) ? 	trim($_POST["account_pass_confirm"]): null;

		 	$validate = new validate("changepass_validate", ACCOUNT_VALIDATION_FILE);	 	
			$message .= $validate->execute($account_pass_new);

			if ($message == "") {
				if (md5($account_pass_old) != $account_pass) 	$message .= ACCOUNT_PASS_OLD_ERROR.BR_TAG;
				if ($account_pass_new != $account_pass_confirm)	$message .= ACCOUNT_PASS_CONFIRM_ERROR.BR_TAG;
			} 
			$accountObj["account_pass"] 		= md5($account_pass_new);
			
			if (!is_null($accountid) && $accountid != "") { // Is update data
				if ($message == "") {
					$accountDao->update_account($accountObj, $accountid);
					header("Location: ?hdl=login");
				}
			}
		} else { // Is back
			header("Location: ?hdl=index");
		}

		$smarty->assign("accountData",   	$accountData);
		$smarty->assign("accountid",   		$accountid);
		$smarty->assign("sexList",  		SexFlag::getList($textlang));
		$smarty->assign("message", 			$message);
		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>