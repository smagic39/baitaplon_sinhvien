<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: regist.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("promote.conf");
		
		$promoteid  = isset($_REQUEST["promoteid"]) ? 	trim($_REQUEST["promoteid"])	: null;
		$page	  	= isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 		: 1;
		$issubmit 	= isset($_POST["issubmit"]) ? 		trim($_POST["issubmit"]) 		: null;
		$message  	= null;
		$provinceDao= new ProvinceDao();
		$promoteDao = new PromoteDao();
		$promoteData= array();
		$promote_start	= new DateTimes("promote_start", 	START_DATE, END_DATE, DATE_PHP, FALSE, "dropdown");
		$promote_end	= new DateTimes("promote_end", 		START_DATE, END_DATE, DATE_PHP, FALSE, "dropdown");
		$promote_end->setEmpty(NULL, "");
		if (is_null($issubmit)) { // Is load default
			if ($promoteid) {
				$promoteData = $promoteDao->getPromoteById($promoteid);
				$promote_start->setSelected($promoteData["promote_start"]);
				$promote_end->setSelected($promoteData["promote_end"]);
			} else {
				$promote_start->setSelected(date(DATE_PHP));
				$promote_end->setSelected(date(DATE_PHP));
			}
		} else if ($issubmit) { // Is edit data
			$promoteData 	= isset($_POST["object"]) ? $_POST["object"]: null;
			$promoteData["promote_start"]	= $promote_start->getDateTime();
			$promoteData["promote_end"]		= $promote_end->getDateTime();

			$filesize 		= FileUtils::returnMB(MAX_UPLOAD_FILE_IMG*1024);
			$fileupload 	= new FileUpload($FILE);
		 	if ($fileupload->getName() == "") $promote_banner = $promoteData["promote_banner"];
			else $promote_banner	= "promote_".date("YmdHsi").$fileupload->getExtFile();

		 	$validate = new validate("promote_validate", PROMOTE_VALIDATION_FILE);	 	
			$message .= $validate->execute($promoteData["promote_name"], $promote_banner, $promoteData["promote_content"], $promoteData["promote_website"], $promoteData["promote_start"], $promoteData["promote_end"]);
			if (!strlen($promote_banner) && !strlen($promoteData["promote_content"])) $message .= PROMOTER_CONTENT_NULL.BR_TAG;

			if ($message == "" && $fileupload->getName() != "") {
				if (!FileUtils::checkFileType($fileupload->getName(), IMG_FORMAT)) $message .= PROMOTER_BANNER_ERROR.BR_TAG;
				else if (FileUtils::returnMB($fileupload->getSize())> FileUtils::getUploadMaxFilesizeOfSite($filesize)) $message .= PROMOTER_BANNER_SIZE.(FileUtils::getUploadMaxFilesizeOfSite($filesize)*1024)." KB.".BR_TAG;	
			}
			if ($message == "" && $promoteDao->checkExistedPromoteName($promoteData["promote_name"], $promoteid)) $message = PROMOTER_NAME_EXISTED;

			if ($message == "") {
				$promoteDao->beginTrans();
				try {
					$objects = $promoteData;
					$objects["promote_banner"] 	= $promote_banner;
					$objects["created_date"] 	= date(DATE_PHP);
					if (!is_null($promoteid) && $promoteid != "") { // Is update data
						if ($fileupload->getName() == "") $promoteDao->update_promote($objects, $promoteid);
						else if ($fileupload->uploaded_file(IMG_PATH.DS."promote".DS, $promote_banner)) {
							$promoteDao->update_promote($objects, $promoteid);
							FileUtils::deleteFile(IMG_PATH.DS."promote".DS, $promoteData["promote_banner"]);
						}
						/*for ($i=0; $i<5; $i++)
							$promoteDao->insert_promote($objects);
						*/
					} else { // Is regist date
						if($fileupload->getName() == "" || $fileupload->uploaded_file(IMG_PATH.DS."promote".DS, $promote_banner)) {
							$promoteDao->insert_promote($objects);
						}
					}
					$promoteDao->commitTrans();
					header("Location: ?hdl=promote/list&page=$page");
				} catch (Exception $ex) {
					$promoteDao->rollbackTrans();
					if (!is_null($promoteid) && $promoteid != "") $message .= MESSAGE_UPDATE_FALSE.BR_TAG;
					else $message .= MESSAGE_INSERT_FALSE.BR_TAG;
				}
			}
		}

		$smarty->assign("promoteData",  $promoteData);
		$smarty->assign("provinceList", $provinceDao->getAllProvinces(null, Status::active));
		$smarty->assign("positionList", Position::getList($textlang));
		$smarty->assign("statusList",  	Status::getList($textlang));
		$smarty->assign("promoteid",   	$promoteid);
		$smarty->assign("promote_start",$promote_start->render());
		$smarty->assign("promote_end", 	$promote_end->render());
		$smarty->assign("page",   		$page);
		$smarty->assign("message", 		$message);
		$promoteDao->doClose();
		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>