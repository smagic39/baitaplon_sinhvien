<?php
/**
 * @project_name: ntmusic
 * @file_name: categories_edit.php
 * @descr:
 * 
 * @author Thunn
 * @version 1.0	 2008/04/02, Thunn, created
 **/
  defined('DS') or die("Errors System");
 $smarty->config_load("interior.conf");
 $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : '';
 $key_word = isset($_REQUEST['key_word']) ? $_REQUEST['key_word'] : '';
 $category_id = isset($_REQUEST['category_id']) ? $_REQUEST['category_id'] : ''; 
   
 $categoriesDao = new CatInteriorDao();
 
 $category_parent_id_list = $categoriesDao->get_categories_tree();
 $smarty->assign("category_parent_id_list", $category_parent_id_list);
 
 $categories_obj = array();
 
 $lbl_mode_title = '';
 $msg_system_errors = '';
 
 if ($category_id > 0) {
 	$categories_obj = $categoriesDao->get_categories_by_category_id((int)$category_id);
 	if (sizeof($categories_obj) <= 0) {
 		$msg_system_errors = MSG_ERR_DATA_NULL;
 		$smarty->assign("msg_system_errors", $msg_system_errors);
 	}
 } else {
 	$categories_obj['category_order'] = $categoriesDao->getMaxId($categoriesDao->getTable(), 'category_id') + 1;
 }
 
 if ($msg_system_errors == '') {
 	
 	if (!empty($_POST['btn_save'])) {
 	
	 	$categories_obj = isset($_POST['categories_obj']) ? $_POST['categories_obj'] : null;
	 	$msg_errors = "";
	 	
	 	if (sizeof($categories_obj) > 0) {
	 		
            @$validate = new validate("CategoriesRegister", CAT_INTERIOR_VALIDATION_FILE);
            
	 		@$msg_errors.= $validate->execute(	$categories_obj['category_name'],
	 											$categories_obj['category_order']
	 											);
	 		if ($msg_errors == '') {
	 			
                $category_id_ = $categories_obj['category_id'];
                    
                    $parent_id_obj = $categoriesDao->get_categories_by_category_id((int)$categories_obj['parent_id']);
                    
                    $categories_obj['id_path'] =  isset($parent_id_obj['id_path']) ? $parent_id_obj['id_path'] : '';
                    
	 			    unset($categories_obj['category_id']);               
	 			    if ($category_id_ > 0) {// update
	 				    if ($categories_obj['id_path'] > 0)
	 				    	$categories_obj['id_path'].= "/".$category_id_;
	 				    else
	 				    	$categories_obj['id_path'] = $category_id_;
	 				    	
                        $categories_obj['updated_date'] = date("Y-m-d H:i:s");
                       
	 				    $categoriesDao->update_categories($categories_obj, (int)$category_id_);
 				    	unset($categories_obj);					    
	 			    } else {
	 				    $categoriesDao->insert_categories($categories_obj);	 				    
	 				    $id = $categoriesDao->getLastInsertId();                        
                        if ($categories_obj['id_path']!= '') {
                        	$categories_obj_['id_path'] =  $categories_obj['id_path']."/".$id;                        	
                        } else {
                        	$categories_obj_['id_path'] = $id;
                        }                        	
                        
                        $categoriesDao->update_categories($categories_obj_, (int)$id);                        
	 			    }
	 			    header("Location: ?hdl=interior/categories_list&page={$page}&key_word={$key_word}");               			
	 		} else {
	 			$msg_errors = LBL_ERRORS.$msg_errors;
	 		}
	 		 		
	 	} else {// end if is_null
	 		$msg_errors = MSG_ERR_SYSTEM;
	 	}
	 	
	 	$smarty->assign("msg_errors", $msg_errors); 	
	 }// end empty($_POST['btn_save'])
 }
 
 $smarty->assign("link_back", "?hdl=interior/categories_list&key_word={$key_word}&page={$page}");
 
 $smarty->assign("categories_obj", $categories_obj);
 unset($categories_obj);
  
 $smarty->assign("statusList", Status::getList($textlang));
 $template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;
if (file_exists($template)) {
	$smarty->display($template);
} else {
	$smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
}
?>