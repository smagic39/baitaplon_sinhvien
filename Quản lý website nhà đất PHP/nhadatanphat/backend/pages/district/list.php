<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: list.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("district.conf");
		
		$page	 	= isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 		: null;
		$districtid	= isset($_REQUEST["districtid"]) ? 	trim($_REQUEST["districtid"])	: null;
		$status	 	= isset($_POST["status"]) ? 		trim($_POST["status"]) 			: null;
		$delete	 	= isset($_POST["delete_"]) ? 		trim($_POST["delete_"]) 		: null;

		if (is_null($status) && !strlen($page) && isset($_SESSION["SEARCH_DISTRICT"])) $_SESSION["SEARCH_DISTRICT"] = null;
		if (!empty($status)) {
			$_SESSION["SEARCH_DISTRICT"]["districtname"]	= isset($_REQUEST["districtname"]) ? 	trim($_REQUEST["districtname"])		: null;
			$_SESSION["SEARCH_DISTRICT"]["provinceid"]		= isset($_REQUEST["provinceid"]) ? 		trim($_REQUEST["provinceid"])		: null;
			$_SESSION["SEARCH_DISTRICT"]["districtstatus"]	= isset($_REQUEST["districtstatus"]) ? 	trim($_REQUEST["districtstatus"])	: null;
		}
		if (!isset($_SESSION["SEARCH_DISTRICT"])) $_SESSION["SEARCH_DISTRICT"] = null;
		if (empty($page)) $page = 1;

		$districtDao = new DistrictDao();
		$provinceDao = new ProvinceDao();
		
		if (!empty($delete)) {
			$districtDao->beginTrans();
			try {
				$districtDao->delete_district($districtid);
				$districtDao->commitTrans();
			} catch (Exception $ex) {
				$districtDao->rollbackTrans();
				$smarty->assign("exefalse", MESSAGE_DELETE_FALSE);
			}
		}
		$offset 	   	= (int)($page-1)*LIMIT_RECORD;
		$count		   	= $districtDao->getCountDistrict($_SESSION["SEARCH_DISTRICT"]["districtname"], $_SESSION["SEARCH_DISTRICT"]["provinceid"], $_SESSION["SEARCH_DISTRICT"]["districtstatus"]);
		if ($offset >= $count) {
			$offset		= (int)$offset - (int)LIMIT_RECORD;
			$page		= $page - 1;
		}
		$districtList = $districtDao->getAllDistricts($_SESSION["SEARCH_DISTRICT"]["districtname"], $_SESSION["SEARCH_DISTRICT"]["provinceid"], $_SESSION["SEARCH_DISTRICT"]["districtstatus"], (int)LIMIT_RECORD, $offset);

		//$smarty->assign("paging",   		PagerUtils::PagerSmarty($page, $count, "?hdl=district/list", (int)LIMIT_RECORD, (int)PAGES_SIZES));
		$pageAll = PagerUtils::PagerPear($count, $page, LIMIT_RECORD, array());
		$smarty->assign("paging",			$pageAll["all"]);
		$smarty->assign("districtList",   	$districtList);
		$smarty->assign("provinceList",   	$provinceDao->getAllProvinces(null, Status::active));
		$smarty->assign("statusList",  		Status::getList($textlang));
		$smarty->assign("count", 			$count);
		$smarty->assign("page", 			$page);
		$smarty->assign("searchData", 		$_SESSION["SEARCH_DISTRICT"]);
		$smarty->assign("message", 			MESSAGE_RESULT_NULL);

		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>