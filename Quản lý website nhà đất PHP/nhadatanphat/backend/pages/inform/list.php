<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: list.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("inform.conf");

		$page		= isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 	: null;
		$informid	= isset($_REQUEST["informid"]) ? 	trim($_REQUEST["informid"])	: null;
		$status		= isset($_POST["status"]) ? 		trim($_POST["status"]) 		: null;
		$delete		= isset($_POST["delete_"]) ? 		trim($_POST["delete_"]) 	: null;

		if (is_null($status) && !strlen($page) && isset($_SESSION["SEARCH_INFORM"])) $_SESSION["SEARCH_INFORM"] = null;
		if (!empty($status)) {
			$_SESSION["SEARCH_INFORM"]["informname"]	= isset($_REQUEST["informname"]) ? 	trim($_REQUEST["informname"])	: null;
			$_SESSION["SEARCH_INFORM"]["informstatus"]	= isset($_REQUEST["informstatus"]) ?trim($_REQUEST["informstatus"])	: null;
			$_SESSION["SEARCH_INFORM"]["categoryid"]	= isset($_REQUEST["categoryid"]) ? 	trim($_REQUEST["categoryid"])	: null;
		}
		if (!isset($_SESSION["SEARCH_INFORM"]))
			$_SESSION["SEARCH_INFORM"] = null;
		if (empty($page))
			$page = 1;

		$informDao 		= new InformDao();
		$CategoryDao 	= new CategoryDao();

		if (!empty($delete)) {
			$informData = $informDao->getInformById($informid);
			$informDao->beginTrans();
			try {
				$informDao->delete_inform($informid);
				$informDao->commitTrans();
				FileUtils::deleteFile(IMG_PATH.DS."inform".DS, $informData["inform_image"]);
			} catch (Exception $ex) {
				$informDao->rollbackTrans();
				$smarty->assign("exefalse", MESSAGE_DELETE_FALSE);
			}
		}

		$offset 	   	= (int)($page-1)*LIMIT_RECORD;
		$count		   	= $informDao->getCountInform($_SESSION["SEARCH_INFORM"]["informname"], $_SESSION["SEARCH_INFORM"]["categoryid"], $_SESSION["SEARCH_INFORM"]["informstatus"]);
		if ($offset >= $count) {
			$offset		= (int)$offset - (int)LIMIT_RECORD;
			$page		= $page - 1;
		}
		$informList = $informDao->getAllInforms($_SESSION["SEARCH_INFORM"]["informname"], $_SESSION["SEARCH_INFORM"]["categoryid"], $_SESSION["SEARCH_INFORM"]["informstatus"], (int)LIMIT_RECORD, $offset);

		//$smarty->assign("paging",   	PagerUtils::PagerSmarty($page, $count, "?hdl=inform/list", (int)LIMIT_RECORD, (int)PAGES_SIZES));
		$pageAll = PagerUtils::PagerPear($count, $page, LIMIT_RECORD, array());
		$smarty->assign("paging",			$pageAll["all"]);
		$smarty->assign("informList",   $informList);
		$smarty->assign("categoryList", $CategoryDao->getAllCategoryByKind(CateKind::general, Status::active));
		$smarty->assign("count", 		$count);
		$smarty->assign("page", 		$page);
		$smarty->assign("searchData", 	$_SESSION["SEARCH_INFORM"]);
		$smarty->assign("message", 		MESSAGE_RESULT_NULL);

		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>