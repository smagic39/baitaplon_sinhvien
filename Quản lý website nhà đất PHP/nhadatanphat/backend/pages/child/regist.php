<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: regist.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		require_once BACKEND_PAGE_PATH.DS."include".DS."categories.php";
		$smarty->config_load("child.conf");
		
		$categoryid  = isset($_REQUEST["categoryid"]) ? trim($_REQUEST["categoryid"])	: null;
		$page	  	 = isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 		: 1;
		$issubmit 	 = isset($_POST["issubmit"]) ? 		trim($_POST["issubmit"]) 		: null;
		$message  	 = null;
		$childDao 	 = new ChildDao();
		$categoryDao = new CategoryDao();
		$categories  = new Categories();
		$categoryData= array();

		if (is_null($issubmit)) { // Is load default
			if ($categoryid) $categoryData = $childDao->getCategoryById($categoryid);
		} else if ($issubmit) { // Is edit data
			$categoryData 	= isset($_POST["object"]) ? $_POST["object"]: null;

		 	$validate = new validate("category_validate", CATEGORY_VALIDATION_FILE);	 	
			$message .= $validate->execute($categoryData["child_name"], "", $categoryData["child_sort"]);

			if (!empty($categoryid) && $categoryid == $categoryData["parent_id"]) $message .= CATEGORY_PARENT_ERROR.BR_TAG;

			if ($message == "") {
				$childDao->beginTrans();
				try {
					$objects = $categoryData;
					$objects["parent_id"] 		= (int)$categoryData["parent_id"];
					$objects["created_date"] 	= date(DATE_PHP);
					unset($objects["insert_all"]);
					if (!is_null($categoryid) && $categoryid != "") { // Is update data
						$childDao->update_category($objects, $categoryid);
					} else { // Is regist date
						$childDao->insert_category($objects);
					}
					$childDao->commitTrans();
					header("Location: ?hdl=child/list&page=$page");
				} catch (Exception $ex) {
					$childDao->rollbackTrans();
					if (!is_null($categoryid) && $categoryid != "") $message .= MESSAGE_UPDATE_FALSE.BR_TAG;
					else $message .= MESSAGE_INSERT_FALSE.BR_TAG;
				}
			}
		}
		$categoryList	= $categoryDao->getAllCategoryByKind(CateKind::estate, Status::active);
		$parentid		= empty($categoryData["category_id"]) ? $categoryList[0]["category_id"] : $categoryData["category_id"];

		$smarty->assign("parentList", 		$categories->ShowCategories($childDao, $parentid, 0, null, Status::active));
		$smarty->assign("categoryList", 	$categoryList);
		$smarty->assign("statusList",  		Status::getList($textlang));
		$smarty->assign("categoryData",   	$categoryData);
		$smarty->assign("categoryid",   	$categoryid);
		$smarty->assign("page",   			$page);
		$smarty->assign("message", 			$message);
		$childDao->doClose();
		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>