<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: list.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("support.conf");
		
		$page	 	= isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 		: null;
		$supportid	= isset($_REQUEST["supportid"]) ? 	trim($_REQUEST["supportid"])	: null;
		$status	 	= isset($_POST["status"]) ? 		trim($_POST["status"]) 			: null;
		$delete	 	= isset($_POST["delete_"]) ? 		trim($_POST["delete_"]) 		: null;

		if (is_null($status) && !strlen($page) && isset($_SESSION["SEARCH_SUPPORT"])) $_SESSION["SEARCH_SUPPORT"] = null;
		if (!empty($status)) {
			$_SESSION["SEARCH_SUPPORT"]["supportname"]	= isset($_REQUEST["supportname"]) ? 	trim($_REQUEST["supportname"])		: null;
			$_SESSION["SEARCH_SUPPORT"]["supportstatus"]	= isset($_REQUEST["supportstatus"]) ? 	trim($_REQUEST["supportstatus"])	: null;
		}
		if (!isset($_SESSION["SEARCH_SUPPORT"])) $_SESSION["SEARCH_SUPPORT"] = null;
		if (empty($page)) $page = 1;

		$supportDao = new SupportDao();
		if (!empty($delete)) {
			$supportDao->beginTrans();
			try {
				$supportDao->delete_support($supportid);
				$supportDao->commitTrans();
			} catch (Exception $ex) {
				$supportDao->rollbackTrans();
				$smarty->assign("exefalse", MESSAGE_DELETE_FALSE);
			}
		}
		$offset 	   	= (int)($page-1)*LIMIT_RECORD;
		$count		   	= $supportDao->getCountSupport($_SESSION["SEARCH_SUPPORT"]["supportname"], $_SESSION["SEARCH_SUPPORT"]["supportstatus"]);
		if ($offset >= $count) {
			$offset		= (int)$offset - (int)LIMIT_RECORD;
			$page		= $page - 1;
		}
		$supportList = $supportDao->getAllSupports($_SESSION["SEARCH_SUPPORT"]["supportname"], $_SESSION["SEARCH_SUPPORT"]["supportstatus"], (int)LIMIT_RECORD, $offset);

		//$smarty->assign("paging",   		PagerUtils::PagerSmarty($page, $count, "?hdl=support/list", (int)LIMIT_RECORD, (int)PAGES_SIZES));
		$pageAll = PagerUtils::PagerPear($count, $page, LIMIT_RECORD, array());
		$smarty->assign("paging",			$pageAll["all"]);
		$smarty->assign("supportList",   	$supportList);
		$smarty->assign("genreList",  		SupportGenre::getList($textlang));
		$smarty->assign("count", 			$count);
		$smarty->assign("page", 			$page);
		$smarty->assign("searchData", 		$_SESSION["SEARCH_SUPPORT"]);
		$smarty->assign("message", 			MESSAGE_RESULT_NULL);

		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>