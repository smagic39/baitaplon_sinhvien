<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: regist.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined("DS") or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("inform.conf");
		
		$informid  	= isset($_REQUEST["informid"]) ? 	trim($_REQUEST["informid"])	: null;
		$page	  	= isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"]) 	: 1;
		$issubmit 	= isset($_POST["issubmit"]) ? 		trim($_POST["issubmit"]) 	: null;
		$oFCKeditor = new FCKeditor("object[inform_content]") ;
		$oFCKeditor->Height	= "500";
		$oFCKeditor->Value 	= "" ;
		$message  	= null;
		$informDao 	= new InformDao();
		$CategoryDao= new CategoryDao();
		$informData	= array();
		
		if (is_null($issubmit)) { // Is load default
			if ($informid) $informData = $informDao->getInformById($informid);
		} else if ($issubmit) { // Is edit data
			$informData 	= isset($_POST["object"]) ? $_POST["object"]: null;

			$filesize 		= FileUtils::returnMB(MAX_UPLOAD_FILE_IMG*1024);
			$fileupload 	= new FileUpload($FILE);
		 	
		 	if($informid != ""){
			 	if ($fileupload->getName() == "") 
			 		$inform_image = $informData["inform_image"];
				else
					$inform_image	= "inform_".date("YmdHsi").$fileupload->getExtFile();
		 	}else{
		 		$inform_image = "inform_".date("YmdHsi").$fileupload->getExtFile();
		 	}
		 	
		 	$validate = new validate("inform_validate", INFORM_VALIDATION_FILE);	 	
			$message .= $validate->execute($informData["inform_name"], $informData["inform_title"], $inform_image, $informData["inform_content"]);

			if ($message == "" && $fileupload->getName() != "") {
				if (!FileUtils::checkFileType($fileupload->getName(), IMG_FORMAT))
					$message .= INFORM_IMAGE_ERROR.BR_TAG;
				else if (FileUtils::returnMB($fileupload->getSize())> FileUtils::getUploadMaxFilesizeOfSite($filesize)) 
					$message .= INFORM_IMAGE_SIZE.(FileUtils::getUploadMaxFilesizeOfSite($filesize)*1024)." KB.".BR_TAG;	
			}
			if ($message == "" && $informDao->checkExistedInformName($informData["inform_name"], $informid))
				$message = INFORM_NAME_EXISTED;

			if ($message == "") {
				//$informDao->beginTrans();
				try {
					$objects = $informData;
					$objects["inform_image"]= $inform_image;
					$objects["created_date"]= date("Y-m-d H:i");
					$objects["category_id"] = 0;
					
					if($objects["isShowInHomePage"] == "on"){
						$objects["isShowInHomePage"] = "1";
					}else{
						$objects["isShowInHomePage"] = "0";
					}
					
					if (!is_null($informid) && $informid != "") { // Is update data
						
						$objects["created_date"] = date("Y-m-d H:i");
						
						if ($fileupload->getName() == "") 
							$informDao->update_inform($objects, $informid);
						else{
							$fileupload->uploaded_file(IMG_PATH.DS."inform".DS, $inform_image);
							Utils::createThumbnailsImage(IMG_PATH.DS."inform".DS.$inform_image, IMG_PATH.DS."inform".DS."thumb_".$inform_image, THUMB_IMG_SIZE, $fileupload->getExtFile());

							$informDao->update_inform($objects, $informid);
							FileUtils::deleteFile(IMG_PATH.DS."inform".DS, $informData["inform_image"]);
							FileUtils::deleteFile(IMG_PATH.DS."inform".DS, "thumb_".$informData["inform_image"]);
						}
					}else{// Is regist date
						if($inform_image != ""){
							$fileupload->uploaded_file(IMG_PATH.DS."inform".DS, $inform_image);
							Utils::createThumbnailsImage(IMG_PATH.DS."inform".DS.$inform_image, IMG_PATH.DS."inform".DS."thumb_".$inform_image, THUMB_IMG_SIZE, $fileupload->getExtFile());
						}

						$informDao->insert_inform($objects);
					}
					
					//$informDao->commitTrans();
					header("Location: ?hdl=inform/list&page=$page");
				} catch (Exception $ex) {
					//$informDao->rollbackTrans();
					if (!is_null($informid) && $informid != "") $message .= MESSAGE_UPDATE_FALSE.BR_TAG;
					else $message .= MESSAGE_INSERT_FALSE.BR_TAG;
				}
			}
		}
		$oFCKeditor->Value	= isset($informData["inform_content"]) ? $informData["inform_content"] : null;
		$inform_content		= $oFCKeditor->CreateHtml() ;

		$smarty->assign("informData",   	$informData);
		$smarty->assign("inform_content", 	$inform_content);

		//$smarty->assign("categoryList", 	$CategoryDao->getAllCategoryByKind(CateKind::general, Status::active));
		//$smarty->assign("ShowRight",  		ShowRight::getList($textlang));

		$smarty->assign("informid",   		$informid);
		$smarty->assign("page",   			$page);
		$smarty->assign("message", 			$message);
		$informDao->doClose();
		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>