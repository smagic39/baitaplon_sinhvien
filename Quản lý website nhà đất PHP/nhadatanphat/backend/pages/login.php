<?php
	/*************************************************************
	 * @project_name: localframe
	 * @file_name: #core.php
	 * @descr:
	 * 
	 * @author 	Nguyen Ngoc - thunn84@gmail.com
	 * @version 1.0
	 **************************************************************/
	defined('DS') or die("Errors System");
	
	$template = BACKEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$smarty->config_load("account.conf");
		$islogin = isset($_POST["islogin"]) ? trim($_POST["islogin"]) 	: null;

		if (!empty($islogin)) { /*************Click logined*************/
			$account_name = isset($_POST["account_name"]) ? trim($_POST["account_name"]) 	: null;
			$account_pass = isset($_POST["account_pass"]) ? trim($_POST["account_pass"]) 	: null;

			$accountDao  = new AccountDao();
			$accountData = $accountDao->getAccountDataByAccountNameAndAccountPass($account_name, md5($account_pass));
	
			if (!empty($accountData)) {
				$_SESSION["_LOGIN_BACKEND_"] = $accountData;
				$smarty->assign("account_info",$accountData);
				header("Location: ?hdl=index");
			}
	
			$smarty->assign("account_name",   	$account_name);
			$smarty->assign("account_pass",   	$account_pass);
			$smarty->assign("message", 			MESSAGE_LOGIN);
		}
		
		return $smarty->display($template);
	} else {
		return $smarty->display(BACKEND_TEMPLATE_ERR);// Template not found
	}
?>
