<?php /* Smarty version 2.6.18, created on 2010-09-24 11:19:45
         compiled from /home/nhadatvanm/domains/nhadatvanminh.com.vn/public_html//backend/templates/promote/list.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', '/home/nhadatvanm/domains/nhadatvanminh.com.vn/public_html//backend/templates/promote/list.tpl', 144, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_header'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
<script language="javascript" type="text/javascript">

	function ConfirmDeleteRecord(url, promoteid, page) {
		var theform = document.frmpromote;
		if (confirm(\'Do you want to delete selected records?\')) {
			theform.action = url;
			theform.promoteid.value = promoteid;
			theform.delete_.value = promoteid;
			theform.page.value = page;
			theform.submit();
		} else return false;
	}
	function submitform(url, promoteid, page, status) {
		var theform = document.frmpromote;
		theform.action = url;
		theform.promoteid.value = promoteid;
		theform.page.value = page;
		theform.status.value = status;
		theform.submit();
	}
</script>
'; ?>


<script language="javascript">
	var page = "<?php echo $this->_tpl_vars['page']; ?>
";
	var status = "<?php echo $this->_tpl_vars['status']; ?>
";
	<?php echo '
	function deleteRecord(){
		var theform = document.frmpromote;
		theform.action = \'?hdl=promote/list\';
		theform.delete_.value = "1";
		theform.page.value = page;
		theform.submit();
	}
	
	function checkAll(fmobj) {
	  var status = fmobj.checkStatus.value;
	  var checkStat = (status == "1"?true:false);

	  for (var i=0;i<fmobj.elements.length;i++) {
	    var e = fmobj.elements[i];
	    if (e.type == \'checkbox\') {
	      e.checked = checkStat;
	    }
	  }
	  
	  fmobj.checkStatus.value = (fmobj.checkStatus.value=="1"?"0":"1");
	}
	'; ?>

</script>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_left'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<td width="80%" align="left" valign="top">
		<form name="frmpromote" id="frmpromote" action="?hdl=promote/regist" method="post">
			<input type="hidden" name="promoteid" id="promoteid" value="" />
			<input type="hidden" name="page" id="page" value="<?php echo $this->_tpl_vars['page']; ?>
" />
			<input type="hidden" name="status" id="status" value="0" />
			<input type="hidden" name="delete_" id="delete_" value="0" />
			<input type="hidden" name="checkStatus" value="1" />
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="10"></td>
			</tr>
			<?php if ($this->_tpl_vars['exefalse'] != ''): ?>
			<tr>
				<td colspan="10" style="color: #FF0000;font-weight: bold;">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;"><?php echo $this->_tpl_vars['exefalse']; ?>
</td>
					</tr>
					</table>
				</td>
			</tr>
			<?php endif; ?>
			<tr>
				<td colspan="10" style="padding: 5px;padding-left: 0px;font-weight: bold;">
					<table cellpadding="0" cellspacing="0">
					<tr>
						<td align="left" valign="middle" style="padding-left: 5px;">
							<?php echo $this->_config[0]['vars']['PROMOTE_NAME']; ?>
&nbsp;:&nbsp;<input type ="text" name="promotename" id="promotename" value="<?php echo $this->_tpl_vars['searchData']['promotename']; ?>
" class="input_text" />
						</td>
						<td align="left" valign="middle" style="padding-left: 5px;">
							<?php echo $this->_config[0]['vars']['STATUS']; ?>
&nbsp;:&nbsp;
							<select name="promotestatus" id="promotestatus" class="dropdown">
								<option value=""><?php echo $this->_config[0]['vars']['SELECTED']; ?>
</option>
								<?php $_from = $this->_tpl_vars['statusList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['status']):
?>
								<option value="<?php echo $this->_tpl_vars['status']['value']; ?>
" <?php if ($this->_tpl_vars['searchData']['promotestatus'] == $this->_tpl_vars['status']['value'] && $this->_tpl_vars['searchData']['promotestatus'] != ''): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['status']['text']; ?>
</option>
								<?php endforeach; endif; unset($_from); ?>
							</select>					
						</td>
						<td align="left" valign="middle" style="padding-left: 5px;">
							<input type="button" name="search" id="search" value="<?php echo $this->_config[0]['vars']['SEARCH']; ?>
" class="button_new" onClick="return submitform('?hdl=promote/list', 0, '1', '1');"/>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<!--header-->
			
			<tr valign="top">
				<td width="" align="right" valign="middle" style="height:15px;" colspan="8">
					<img src="backend/images/delete.jpg" width="15" height="15" 
						title="Xóa theo checkbox đã chọn" 
						onClick="if(confirm('Xóa các bản ghi đã chọn ở bảng?')) deleteRecord();"
						class="button" />
				</td>
			</tr>
			
			<tr class="stdHeader" valign="top">
				<td width="4%" align="center" valign="middle" style="height:15px;"><?php echo $this->_config[0]['vars']['FIELD_ID']; ?>
</td>
				<td width="15%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['PROMOTE_NAME']; ?>
</td>
				<td width="10%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['PROMOTE_START']; ?>
</td>
				<td width="10%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['PROMOTE_END']; ?>
</td>
			<!--
				<td width="8%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['PROMOTE_CLICKED']; ?>
</td>
			-->
				<td width="15%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['PROMOTE_WEBSITE']; ?>
</td>
				<td width="12%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['PROMOTE_POSITION']; ?>
</td>
				<td width="8%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['STATUS']; ?>
</td>
				<td width="8%" align="center" valign="middle">
					Sửa/<a onClick="checkAll(document.frmpromote);" style="cursor:pointer;">Chọn</a>
				</td>
			</tr>
			<!--If cls = 1 then strClass = "record" else strClass = "evenRecord" end if-->
			<?php if (count ( $this->_tpl_vars['promoteList'] ) > 0): ?>
			<?php $_from = $this->_tpl_vars['promoteList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['items']):
?>
			<?php $this->assign('i', $this->_tpl_vars['i']+1); ?>
			<?php if ($this->_tpl_vars['i']%2 == 0): ?>
				<?php $this->assign('class', 'record'); ?>
			<?php else: ?>
				<?php $this->assign('class', 'evenRecord'); ?>
			<?php endif; ?>
			<tr class="<?php echo $this->_tpl_vars['class']; ?>
" valign="top">
				<td align="center" style="padding:5px;" valign="middle"  width="20">
					<?php echo $this->_tpl_vars['items']['promote_id']; ?>

				</td>
				<td align="left" style="padding:5px;" valign="middle" width="">
					<?php echo $this->_tpl_vars['items']['promote_name']; ?>

				</td>
				<td align="center" style="padding:5px;" valign="middle" width="20">
					<?php echo ((is_array($_tmp=$this->_tpl_vars['items']['promote_start'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d-%m-%Y") : smarty_modifier_date_format($_tmp, "%d-%m-%Y")); ?>

				</td>
				<td align="center" style="padding:5px;" valign="middle" width="20">
					<?php echo ((is_array($_tmp=$this->_tpl_vars['items']['promote_end'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d-%m-%Y") : smarty_modifier_date_format($_tmp, "%d-%m-%Y")); ?>

				</td>
			<!--
				<td align="center" style="padding:5px;" valign="middle">
					<?php echo $this->_tpl_vars['items']['promote_click']; ?>

				</td>
			-->
				<td align="left" style="padding:5px;" valign="middle" width="70">
					<?php echo $this->_tpl_vars['items']['promote_website']; ?>

				</td>
				<td align="center" style="padding:5px;" valign="middle" width="40">
					<?php echo $this->_tpl_vars['positionList'][$this->_tpl_vars['items']['promote_position']]['text']; ?>

				</td>
				<td align="center" style="padding:5px;" valign="middle" width="30">
					<?php echo $this->_tpl_vars['statusList'][$this->_tpl_vars['items']['status']]['text']; ?>

				</td>
				<td align="center" style="padding:5px;" valign="middle" width="20">
					<table cellpadding="0" cellspacing="0" border="0px;">
					<tr>
					  	<td style="border: 0px;padding-right:5px;">
					  		<img src="backend/images/edit.gif" width="15" height="15" title="edit" onClick="return submitform('?hdl=promote/regist',<?php echo $this->_tpl_vars['items']['promote_id']; ?>
,<?php echo $this->_tpl_vars['page']; ?>
,'2');" class="button" />
					  	</td>
						<td style="border: 0px;padding-left:5px;">
							<!--
								<img src="backend/images/delete.jpg" width="15" height="15" title="delete" onClick="return ConfirmDeleteRecord('?hdl=promote/list',<?php echo $this->_tpl_vars['items']['promote_id']; ?>
,<?php echo $this->_tpl_vars['page']; ?>
);" class="button" />
							-->
							<input type="checkbox" name="deleteId[]" value="<?php echo $this->_tpl_vars['items']['promote_id']; ?>
" />
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<?php endforeach; endif; unset($_from); ?>
			<?php else: ?>
			<tr valign="top">
				<td colspan="10" align="center" style="padding:5px;" valign="middle">
				<?php echo $this->_tpl_vars['message']; ?>

				</td>
			</tr>
			<?php endif; ?>
			<tr class="stdHeader" valign="top">
				<td align="center" colspan="10" valign="middle" style="height:25px;">
					<?php if ($this->_tpl_vars['count'] > 1): ?>
					<div align="center" style="padding-right:5px;font-size:11px;"> <!-- Pager -->
						<?php echo $this->_tpl_vars['paging']; ?>

					</div>
					<?php endif; ?>
				</td>
			</tr>
			<tr valign="top">
				<td colspan="10" align="center" style="padding:5px;" valign="middle">
					<input type="submit" name="addnew" id="addnew" value="<?php echo $this->_config[0]['vars']['REGIST']; ?>
" class="button_new" />
				</td>
			</tr>
			</table>
		</form>
		</td>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_footer'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>