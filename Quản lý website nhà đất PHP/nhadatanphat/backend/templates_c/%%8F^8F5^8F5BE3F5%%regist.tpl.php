<?php /* Smarty version 2.6.18, created on 2011-04-05 20:09:02
         compiled from /home/nhadatan/public_html//backend/templates/inform/regist.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_header'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
<script language="javascript" type="text/javascript">
	function submitform(url, value) {
		var theform = document.frminform;
		theform.action = url;
		theform.issubmit.value = value;
		theform.submit();
	}
	function hiddenform() {
		var theform = document.frminform;
		if (theform.editgeneral.checked) {
			theform.editgeneral.value = 1;
		} else {
			theform.editgeneral.value = 0;
		}
	}
</script>
'; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_left'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<td width="80%" align="left" valign="top">
		<form name="frminform" id="frminform" action="" method="post" enctype="multipart/form-data">
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			<!--header-->
			<?php if ($this->_tpl_vars['message'] != ""): ?>
			<tr valign="top">
				<td align="left" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;"><?php echo $this->_tpl_vars['message']; ?>
</td>
					</tr>
					</table>
				</td>
			</tr>
			<?php endif; ?>
			<tr class="stdHeader" valign="middle">
				<td style="height:20px;padding-left: 10px;">Update inform</td>
			</tr>
			<tr valign="top">
				<td>
					<input type="hidden" name="informid" id="informid" value="<?php echo $this->_tpl_vars['informid']; ?>
" />
					<input type="hidden" name="page" id="page" value="<?php echo $this->_tpl_vars['page']; ?>
" />
					<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
						<?php if ($this->_tpl_vars['informid'] != ''): ?>
						<tr valign="top">
							<td width="20%" align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['FIELD_ID']; ?>
: </td>
							<td width="55%" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<?php echo $this->_tpl_vars['informid']; ?>

							</td>
							<td rowspan="4" width="24%" align="center" valign="middle" class="tdregist">
								<?php if ($this->_tpl_vars['informData']['inform_image'] != ""): ?>
								<img border="1" style="border-color:#CAD5DB;" width="120px" src="<?php echo $this->_config[0]['vars']['IMG_INFORM_DIR']; ?>
thumb_<?php echo $this->_tpl_vars['informData']['inform_image']; ?>
" />
								<?php endif; ?>
							</td>
						</tr>
						<?php endif; ?>

					<!--
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['INFORM_CATEGORY']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<select name="object[category_id]" id="object[category_id]" class="dropdown">
									<?php $_from = $this->_tpl_vars['categoryList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['categories']):
?>
									<option value="<?php echo $this->_tpl_vars['categories']['category_id']; ?>
" <?php if ($this->_tpl_vars['informData']['category_id'] == $this->_tpl_vars['categories']['category_id'] && $this->_tpl_vars['informData']['category_id'] != ''): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['categories']['category_name']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>					
							</td>
						</tr>
					-->

						<tr valign="top">
							<td width="20%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['INFORM_NAME']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type ="text" name="object[inform_name]" id="object[inform_name]" value="<?php echo $this->_tpl_vars['informData']['inform_name']; ?>
" class="input_text" />
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['INFORM_TITLE']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<textarea name="object[inform_title]" id="object[inform_title]" class="textarea"><?php echo $this->_tpl_vars['informData']['inform_title']; ?>
</textarea>
							</td>
						</tr>
					
						<tr valign="top">
							<td valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['INFORM_IMAGE']; ?>
: </td>
							<td colspan="3" align="left" valign="middle" class="tdregist">
								<input type="file" name="inform_image" id="inform_image" class="inputfield" />
								<input type="hidden" name="object[inform_image]" id="object[inform_image]" value="<?php echo $this->_tpl_vars['informData']['inform_image']; ?>
" />
							</td>
						</tr>
					
						<tr valign="top">
							<td valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['INFORM_CONTENT']; ?>
: </td>
							<td colspan="3" align="left" valign="middle" class="tdregist">
									<?php echo $this->_tpl_vars['inform_content']; ?>

							</td>
						</tr>
					<!--
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['INFORM_RIGHT']; ?>
: </td>
							<td colspan="3" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<select name="object[inform_index]" id="object[inform_index]" class="dropdown">
									<?php $_from = $this->_tpl_vars['ShowRight']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['shows']):
?>
									<option value="<?php echo $this->_tpl_vars['shows']['value']; ?>
" <?php if ($this->_tpl_vars['informData']['inform_index'] == $this->_tpl_vars['shows']['value'] && $this->_tpl_vars['informData']['inform_index'] != ''): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['shows']['text']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>
							</td>
						</tr>
					-->
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['STATUS']; ?>
: </td>
							<td colspan="3" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<select name="object[status]" id="object[status]" class="dropdown">
									<?php $_from = $this->_tpl_vars['statusList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['status']):
?>
									<option value="<?php echo $this->_tpl_vars['status']['value']; ?>
" <?php if ($this->_tpl_vars['informData']['status'] == $this->_tpl_vars['status']['value'] && $this->_tpl_vars['informData']['status'] != ''): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['status']['text']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>					
							</td>
						</tr>

						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;">
								Hiển thị trên trang chủ: 
							</td>
							<td colspan="3" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
									<input type="checkbox" name="object[isShowInHomePage]" 
										<?php if ($this->_tpl_vars['informData']['isShowInHomePage'] == 'on' || $this->_tpl_vars['informData']['isShowInHomePage'] == '1'): ?> checked <?php endif; ?>
									/>
							</td>
						</tr>

					</table>
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td style="height:15px;"></td>
			</tr>
			<tr valign="top">
				<td colspan="2" align="center" valign="middle" style="padding:10px;">
					<input type="button" name="update" id="update" value="<?php echo $this->_config[0]['vars']['UPDATE']; ?>
" class="button_new" onclick="javascript: submitform('?hdl=inform/regist', 1)" />
					<input type="button" name="isback" id="isback" value="<?php echo $this->_config[0]['vars']['BACK']; ?>
" class="button_new" onclick="javascript: submitform('?hdl=inform/list', 0)" />
					<input type="hidden" name="issubmit" id="issubmit" value="0" />
				</td>
			</tr>
			</table>
		</form>
		</td>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_footer'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>