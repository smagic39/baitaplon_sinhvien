<?php $_config_vars = array (
  'CATEGORY_NAME' => 'Tên danh mục menu',
  'CATEGORY_ICON' => 'Ảnh nhỏ',
  'CATEGORY_PARENT' => 'Cấp trên',
  'CATEGORY_HORIZONTAL' => 'Hiển thị menu trên',
  'CATEGORY_BOTTOM' => 'Hiển thị menu dưới',
  'CATEGORY_VERTICAL' => 'Hiển thị menu trái',
  'CATEGORY_KIND' => 'Nhóm danh mục',
  'CATEGORY_TITLES' => 'Cập nhật loại thông tin',
  'CATEGORY_SORT' => 'Sắp xếp',
); ?>