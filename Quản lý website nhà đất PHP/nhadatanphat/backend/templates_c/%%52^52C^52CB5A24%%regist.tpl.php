<?php /* Smarty version 2.6.18, created on 2011-03-25 15:29:29
         compiled from /home/nhadatan/public_html//backend/templates/child/regist.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_header'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
<script language="javascript" type="text/javascript">
	function submitform(url, value) {
		var theform = document.frmcategory;
		theform.action = url;
		theform.issubmit.value = value;
		theform.submit();
	}
	function hiddenform() {
		var theform = document.frmcategory;
		if (theform.editgeneral.checked) {
			theform.editgeneral.value = 1;
		} else {
			theform.editgeneral.value = 0;
		}
	}

	var ajaxObj = new XMLHTTP("services.php");

	function select_parent(val){
		ajaxObj.call(\'action=Category&act=SelectParent&category_id=\'+val, select_parent_resp);
	}
	function select_parent_resp(resp) {
		if(resp.result == \'1\') {
			var model = document.getElementById("object[parent_id]");
			ClearOption("object[parent_id]");
  			
			var modelOption1 = document.createElement(\'option\');
			modelOption1.text = "Lựa chọn";
  			modelOption1.value = \'\';
  			try {
    			model.add(modelOption1, null); // standards compliant; doesn\'t work in IE
  			}
  			catch(ex) {
    			model.add(modelOption1); // IE only
  			}
			if(resp.data){
			var id = resp.data.id;
			for(var i = 0; i < id.length; i++){
				var modelOption = document.createElement(\'option\');
  				modelOption.text = resp.data.title[i];
  				modelOption.value = id[i];

  				try {
    				model.add(modelOption, null); 
  				}
  				catch(ex) {
    				model.add(modelOption); 
  				}
			}
			}
		} else {
			alert(resp.result);
		}
	}
	function ClearOption(id){
		var list = document.getElementById(id);
		for(var i=list.length-1;i>=0;i--){
			list.remove(i);
		}
	}
</script>
'; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_left'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<td width="80%" align="left" valign="top">
		<form name="frmcategory" id="frmcategory" action="" method="post" enctype="multipart/form-data">
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			<!--header-->
			<?php if ($this->_tpl_vars['message'] != ""): ?>
			<tr valign="top">
				<td align="left" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;"><?php echo $this->_tpl_vars['message']; ?>
</td>
					</tr>
					</table>
				</td>
			</tr>
			<?php endif; ?>
			<tr class="stdHeader" valign="middle">
				<td style="height:20px;padding-left: 10px;">Update category</td>
			</tr>
			<tr valign="top">
				<td>
					<input type="hidden" name="categoryid" id="categoryid" value="<?php echo $this->_tpl_vars['categoryid']; ?>
" />
					<input type="hidden" name="page" id="page" value="<?php echo $this->_tpl_vars['page']; ?>
" />
					<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
						<?php if ($this->_tpl_vars['categoryid'] != ''): ?>
						<tr valign="top">
							<td width="30%" align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['FIELD_ID']; ?>
: </td>
							<td width="69%" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<?php echo $this->_tpl_vars['categoryid']; ?>

							</td>
						</tr>
						<?php endif; ?>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['CATEGORY_NAME']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<select name="object[category_id]" id="object[category_id]" class="dropdown" onChange="select_parent(this.options[selectedIndex].value)">
									<?php $_from = $this->_tpl_vars['categoryList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['categoies']):
?>
									<option value="<?php echo $this->_tpl_vars['categoies']['category_id']; ?>
" <?php if ($this->_tpl_vars['categoryData']['category_id'] == $this->_tpl_vars['categoies']['category_id']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['categoies']['category_name']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>					
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['CHILD_NAME']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<input type ="text" name="object[child_name]" id="object[child_name]" value="<?php echo $this->_tpl_vars['categoryData']['child_name']; ?>
" class="input_text" />
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['CHILD_PARENT']; ?>
: </td>
							<td align="left" valign="middle" class="tdregist">
								<select name="object[parent_id]" id="object[parent_id]" class="dropdown">
									<option value="0"><?php echo $this->_config[0]['vars']['SELECTED']; ?>
</option>
									<?php $_from = $this->_tpl_vars['parentList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['parent']):
?>
									<option value="<?php echo $this->_tpl_vars['parent']['child_id']; ?>
" <?php if ($this->_tpl_vars['categoryData']['parent_id'] == $this->_tpl_vars['parent']['child_id']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['parent']['child_name']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>					
							</td>
						</tr>
						<tr valign="top">
							<td valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['CHILD_SORT']; ?>
: </td>
							<td colspan="3" align="left" valign="middle" class="tdregist">
								<input type ="text" name="object[child_sort]" id="object[child_sort]" value="<?php echo $this->_tpl_vars['categoryData']['child_sort']; ?>
" class="input_text" />			
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['STATUS']; ?>
: </td>
							<td colspan="3" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<select name="object[status]" id="object[status]" class="dropdown">
									<?php $_from = $this->_tpl_vars['statusList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['status']):
?>
									<option value="<?php echo $this->_tpl_vars['status']['value']; ?>
" <?php if ($this->_tpl_vars['categoryData']['status'] == $this->_tpl_vars['status']['value'] && $this->_tpl_vars['categoryData']['status'] != ''): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['status']['text']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>					
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td style="height:15px;"></td>
			</tr>
			<tr valign="top">
				<td colspan="2" align="center" valign="middle" style="padding:10px;">
					<input type="button" name="update" id="update" value="<?php echo $this->_config[0]['vars']['UPDATE']; ?>
" class="button_new" onclick="javascript: submitform('?hdl=child/regist', 1)" />
					<input type="button" name="isback" id="isback" value="<?php echo $this->_config[0]['vars']['BACK']; ?>
" class="button_new" onclick="javascript: submitform('?hdl=child/list', 0)" />
					<input type="hidden" name="issubmit" id="issubmit" value="0" />
				</td>
			</tr>
			</table>
		</form>
		</td>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_footer'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>