<?php /* Smarty version 2.6.18, created on 2011-03-24 14:34:40
         compiled from /home/nhadatan/public_html//backend/templates/member/regist.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', '/home/nhadatan/public_html//backend/templates/member/regist.tpl', 102, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_header'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
<script language="javascript" type="text/javascript">
	function submitform(url, value) {
		var theform = document.frmmember;
		theform.action = url;
		theform.issubmit.value = value;
		theform.submit();
	}

	function hiddenform() {
		var theform = document.frmmember;
		if (theform.editgeneral.checked) {
			theform.editgeneral.value = 1;
		} else {
			theform.editgeneral.value = 0;
		}
	}

  	function showUploadOption(boolVal){
  		if(boolVal == true){
			document.getElementById("memberLogo").style.display="";
  		}else{
  			document.getElementById("memberLogo").style.display="none";
  		}
	}
</script>
'; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_left'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<td width="80%" align="left" valign="top">
		<form name="frmmember" id="frmmember" action="" method="post" enctype="multipart/form-data">
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			<!--header-->
			<?php if ($this->_tpl_vars['message'] != ""): ?>
			<tr valign="top">
				<td align="left" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;"><?php echo $this->_tpl_vars['message']; ?>
</td>
					</tr>
					</table>
				</td>
			</tr>
			<?php endif; ?>
			<tr class="stdHeader" valign="middle">
				<td style="height:20px;padding-left: 10px;">Update member</td>
			</tr>
			<tr valign="top">
				<td>
					<input type="hidden" name="memberid" id="memberid" value="<?php echo $this->_tpl_vars['memberid']; ?>
" />
					<input type="hidden" name="page" id="page" value="<?php echo $this->_tpl_vars['page']; ?>
" />
					<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
						<tr valign="top">
							<td width="30%" align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['FIELD_ID']; ?>
: </td>
							<td colspan=2 width="65%" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="2">
								<?php echo $this->_tpl_vars['memberid']; ?>

							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['MEMBER_USER']; ?>
: </td>
							<td  colspan=2 align="left" valign="middle" class="tdregist">
								<?php echo $this->_tpl_vars['memberData']['member_user']; ?>

							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['MEMBER_NAME']; ?>
: </td>
							<td colspan=2  align="left" valign="middle" class="tdregist">
								<?php echo $this->_tpl_vars['memberData']['member_name']; ?>

							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['MEMBER_ADDRESS']; ?>
: </td>
							<td  colspan=2 align="left" valign="middle" class="tdregist">
								<?php echo $this->_tpl_vars['memberData']['member_address']; ?>

							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['MEMBER_MAIL']; ?>
: </td>
							<td colspan=2  align="left" valign="middle" class="tdregist">
								<a href="mailto:<?php echo $this->_tpl_vars['memberData']['member_mail']; ?>
"><?php echo $this->_tpl_vars['memberData']['member_mail']; ?>
</a>
							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['MEMBER_PHONE']; ?>
: </td>
							<td  colspan=2 align="left" valign="middle" class="tdregist">
								<?php echo $this->_tpl_vars['memberData']['member_phone']; ?>

							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['MEMBER_WEBSITE']; ?>
: </td>
							<td  colspan=2 align="left" valign="middle" class="tdregist">
								<?php echo $this->_tpl_vars['memberData']['member_website']; ?>

							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['REGIST_DATE']; ?>
: </td>
							<td  colspan=2 align="left" valign="middle" class="tdregist">
								<?php echo ((is_array($_tmp=$this->_tpl_vars['memberData']['created_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d-%m-%Y") : smarty_modifier_date_format($_tmp, "%d-%m-%Y")); ?>

							</td>
						</tr>
						<tr valign="top">
							<td width="30%" valign="middle" class="tdregisttext">TK AnPhat Land</td>
							<td colspan=2  align="left" valign="middle" class="tdregist" style="padding-left: 0px;">
								&nbsp;&nbsp;
								<input type="checkbox" name="object[member_genre]" id="objectmember_genre" <?php if ($this->_tpl_vars['memberData']['member_genre'] == 1): ?>checked="true"<?php endif; ?>  value="1"
									onClick="showUploadOption(this.checked);"
								/>
							</td>
						</tr>
						<tr valign="top" id="memberLogo">
							<td width="30%" valign="middle" class="tdregisttext">Upload Logo: </td>
							<td align="left" valign="bottom" class="tdregist">
								&nbsp;&nbsp;<input type="file" name="memberLogo" />
							</td>
							<td  style="padding-left: 0px;" class="tdregist">
								<?php if ($this->_tpl_vars['logoImg'] != ""): ?>
									<img src="<?php echo $this->_config[0]['vars']['IMG_MEMBER_LOGO_DIR']; ?>
<?php echo $this->_tpl_vars['logoImg']; ?>
" width="150" class="border_img_white" />
								<?php endif; ?>
							</td>
						</tr>
						<tr valign="top">
							<td align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['STATUS']; ?>
: </td>
							<td  colspan=2 align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;" colspan="3">
								<select name="object[status]" id="object[status]" class="dropdown">
									<?php $_from = $this->_tpl_vars['statusList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['status']):
?>
									<option value="<?php echo $this->_tpl_vars['status']['value']; ?>
" <?php if ($this->_tpl_vars['memberData']['status'] == $this->_tpl_vars['status']['value'] && $this->_tpl_vars['memberData']['status'] != ''): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['status']['text']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>					
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td style="height:15px;"></td>
			</tr>
			<tr valign="top">
				<td colspan="2" align="center" valign="middle" style="padding:10px;">
					<input type="button" name="update" id="update" value="<?php echo $this->_config[0]['vars']['UPDATE']; ?>
" class="button_new" onclick="javascript: submitform('?hdl=member/regist', 1)" />
					<input type="button" name="isback" id="isback" value="<?php echo $this->_config[0]['vars']['BACK']; ?>
" class="button_new" onclick="javascript: submitform('?hdl=member/list', 0)" />
					<input type="hidden" name="issubmit" id="issubmit" value="0" />
				</td>
			</tr>
			</table>
		</form>
		</td>
	<script language="javascript">
		<?php echo '
			showUploadOption(document.getElementById("objectmember_genre").checked);
		'; ?>

	</script>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_footer'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>