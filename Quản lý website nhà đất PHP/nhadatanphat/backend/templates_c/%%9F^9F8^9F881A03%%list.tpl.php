<?php /* Smarty version 2.6.18, created on 2011-03-19 17:59:23
         compiled from /home/nhadatan/public_html//backend/templates/company/list.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_header'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
<script language="javascript" type="text/javascript">
	function ConfirmDeleteRecord(url, companyid, page) {
		var theform = document.frmcompany;
		if (confirm(\'Do you want to delete selected records?\')) {
			theform.action = url;
			theform.companyid.value = companyid;
			theform.delete_.value = companyid;
			theform.page.value = page;
			theform.submit();
		} else return false;
	}
	function submitform(url, companyid, page, status) {
		var theform = document.frmcompany;
		theform.action = url;
		theform.companyid.value = companyid;
		theform.page.value = page;
		theform.status.value = status;
		theform.submit();
	}
</script>
'; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_left'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<td width="80%" align="left" valign="top">
		<form name="frmcompany" id="frmcompany" action="?hdl=company/regist" method="post">
			<input type="hidden" name="companyid" id="companyid" value="" />
			<input type="hidden" name="page" id="page" value="<?php echo $this->_tpl_vars['page']; ?>
" />
			<input type="hidden" name="status" id="status" value="0" />
			<input type="hidden" name="delete_" id="delete_" value="0" />
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable"><!---->
				<tr>
					<td height="5px" colspan="7"></td>
				</tr>
				<?php if ($this->_tpl_vars['exefalse'] != ''): ?>
				<tr>
					<td colspan="7" style="color: #FF0000;font-weight: bold;">
						<table width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
							<td valign="middle" class="error" style="border-left: 0px;"><?php echo $this->_tpl_vars['exefalse']; ?>
</td>
						</tr>
						</table>
					</td>
				</tr>
				<?php endif; ?>
				<tr>
					<td colspan="7" style="padding: 5px;padding-left: 0px;font-weight: bold;">
						<table cellpadding="0" cellspacing="0" border='1'>
						<tr>
							<td align="left" valign="middle" style="padding-left: 5px;">
								<?php echo $this->_config[0]['vars']['COMPANY_NAME']; ?>
&nbsp;:&nbsp;<input type ="text" name="companyname" id="companyname" value="<?php echo $this->_tpl_vars['searchData']['companyname']; ?>
" class="input_text" />
							</td>
							<td align="left" valign="middle" style="padding-left: 5px;" colspan=2>
								<?php echo $this->_config[0]['vars']['STATUS']; ?>
&nbsp;:&nbsp;
								<select name="companystatus" id="companystatus" class="dropdown">
									<option value=""><?php echo $this->_config[0]['vars']['SELECTED']; ?>
</option>
									<?php $_from = $this->_tpl_vars['statusList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['status']):
?>
									<option value="<?php echo $this->_tpl_vars['status']['value']; ?>
" <?php if ($this->_tpl_vars['searchData']['companystatus'] == $this->_tpl_vars['status']['value'] && $this->_tpl_vars['searchData']['companystatus'] != ''): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['status']['text']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>					
							</td>
						</tr>
						</table>
					</td>
				</tr>
				<tr valign="top">
					<td colspan="7" align="center" style="padding:5px;" valign="middle">
						<input type="button" name="search" id="search" value="<?php echo $this->_config[0]['vars']['SEARCH']; ?>
" class="button_new" onClick="return submitform('?hdl=company/list', 0, '1', '1');"/>
						<input type="submit" name="addnew" id="addnew" value="<?php echo $this->_config[0]['vars']['REGIST']; ?>
" class="button_new" />
					</td>
				</tr>
				<!--header-->
				<tr class="stdHeader" valign="top">
					<td width="4%" align="center" valign="middle" style="height:15px;">
						<?php echo $this->_config[0]['vars']['FIELD_ID']; ?>

					</td>
					<td width="15%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['COMPANY_NAME']; ?>
</td>
					<td width="10%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['COMPANY_PROVINCE']; ?>
</td>
					<!--<td width="40%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['COMPANY_CONTENT']; ?>
</td>-->
					<td width="15%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['COMPANY_WEBSITE']; ?>
</td>
					<td width="8%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['STATUS']; ?>
</td>
					<td width="8%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['FUNCTION']; ?>
</td>
				</tr>
				<!--If cls = 1 then strClass = "record" else strClass = "evenRecord" end if-->
			<?php if (count ( $this->_tpl_vars['companyList'] ) > 0): ?>
				<?php $_from = $this->_tpl_vars['companyList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['items']):
?>
					<?php $this->assign('i', $this->_tpl_vars['i']+1); ?>
					<?php if ($this->_tpl_vars['i']%2 == 0): ?>
						<?php $this->assign('class', 'record'); ?>
					<?php else: ?>
						<?php $this->assign('class', 'evenRecord'); ?>
					<?php endif; ?>
				<tr class="<?php echo $this->_tpl_vars['class']; ?>
" valign="top">
					<td align="center" style="padding:5px;" valign="middle">
						<?php echo $this->_tpl_vars['items']['company_id']; ?>

					</td>
					
					<td align="left" style="padding:5px;" valign="middle">
						<?php if ($this->_tpl_vars['items']['is_show_in_homepage'] == '1'): ?><B><?php endif; ?>
						<?php if ($this->_tpl_vars['items']['is_show_in_province'] == '1'): ?><i><?php endif; ?>	
						<?php echo $this->_tpl_vars['items']['company_name']; ?>

						<?php if ($this->_tpl_vars['items']['is_show_in_province'] == '1'): ?></i><?php endif; ?>
						<?php if ($this->_tpl_vars['items']['is_show_in_homepage'] == '1'): ?></B><?php endif; ?>
					</td>
					<td align="center" style="padding:5px;" valign="middle">
						<?php echo $this->_tpl_vars['items']['province_name']; ?>

					</td>
					
					<td align="left" style="padding:5px;" valign="middle">
						<?php echo $this->_tpl_vars['items']['company_website']; ?>

					</td>
					
					<td align="center" style="padding:5px;" valign="middle">
						<?php echo $this->_tpl_vars['statusList'][$this->_tpl_vars['items']['status']]['text']; ?>

					</td>
					<td align="center" style="padding:5px;" valign="middle">
					  	<img src="backend/images/edit.gif" width="15" height="15" title="edit" onClick="return submitform('?hdl=company/regist',<?php echo $this->_tpl_vars['items']['company_id']; ?>
,<?php echo $this->_tpl_vars['page']; ?>
,'2');" class="button" />
						<img src="backend/images/delete.jpg" width="15" height="15" title="delete" onClick="return ConfirmDeleteRecord('?hdl=company/list',<?php echo $this->_tpl_vars['items']['company_id']; ?>
,<?php echo $this->_tpl_vars['page']; ?>
);" class="button" />
					</td>
				</tr>
				<?php endforeach; endif; unset($_from); ?>
				<?php else: ?>
				<tr valign="top">
					<td colspan="7" align="center" style="padding:5px;" valign="middle">
					<?php echo $this->_tpl_vars['message']; ?>

					</td>
				</tr>
				<?php endif; ?>
				<tr class="stdHeader" valign="top">
					<td align="center" valign="middle" colspan="7" style="height:25px;">
						<?php if ($this->_tpl_vars['count'] > 1): ?>
						<div align="center" style="padding-right:5px;font-size:11px;"> <!-- Pager -->
							<?php echo $this->_tpl_vars['paging']; ?>

						</div>
						<?php endif; ?>
					</td>
				</tr>
			</table>
		</form>
		</td>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_footer'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>