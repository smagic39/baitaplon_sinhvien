<?php /* Smarty version 2.6.18, created on 2011-03-19 20:08:09
         compiled from /home/nhadatan/public_html//backend/templates/member/list.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', '/home/nhadatan/public_html//backend/templates/member/list.tpl', 114, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_header'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
<script language="javascript" type="text/javascript">
	function ConfirmDeleteRecord(url, memberid, page) {
		var theform = document.frmmember;
		if (confirm(\'Do you want to delete selected records?\')) {
			theform.action = url;
			theform.memberid.value = memberid;
			theform.delete_.value = memberid;
			theform.page.value = page;
			theform.submit();
		} else return false;
	}
	function submitform(url, memberid, page, status) {
		var theform = document.frmmember;
		theform.action = url;
		theform.memberid.value = memberid;
		theform.page.value = page;
		theform.status.value = status;
		theform.submit();
	}
</script>
'; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_left'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<td width="80%" align="left" valign="top">
		<form name="frmmember" id="frmmember" action="?hdl=member/regist" method="post">
			<input type="hidden" name="memberid" id="memberid" value="" />
			<input type="hidden" name="page" id="page" value="<?php echo $this->_tpl_vars['page']; ?>
" />
			<input type="hidden" name="status" id="status" value="0" />
			<input type="hidden" name="delete_" id="delete_" value="0" />
			<table width="100%" cellpadding="0" cellspacing="0" class="dataTable">
			<tr>
				<td height="5px" colspan="10"></td>
			</tr>
			<?php if ($this->_tpl_vars['exefalse'] != ''): ?>
			<tr>
				<td colspan="9" style="color: #FF0000;font-weight: bold;">
					<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" width="5%" class="error"><img src="backend/images/error.jpg" width="40" height="40" title="Errors !" /></td>
						<td valign="middle" class="error" style="border-left: 0px;"><?php echo $this->_tpl_vars['exefalse']; ?>
</td>
					</tr>
					</table>
				</td>
			</tr>
			<?php endif; ?>
			<tr>
				<td colspan="9" style="padding: 5px;padding-left: 0px;font-weight: bold;">
					<table cellpadding="0" cellspacing="0">
						<tr>
							<td align="left" valign="middle" style="padding-left: 5px;" colspan="3">
								<?php echo $this->_config[0]['vars']['REGIST_DATE']; ?>
&nbsp;:&nbsp;<?php echo $this->_tpl_vars['startdate']; ?>
&nbsp;-&nbsp;<?php echo $this->_tpl_vars['enddate']; ?>

							</td>
						</tr>
						<tr>
							<td align="left" valign="middle" style="padding-left: 5px;" colspan="2">
								Tên thành viên:&nbsp;
								<input name="memberName" value="<?php echo $this->_tpl_vars['searchData']['memberName']; ?>
" />
							</td>
							<td align="left" valign="middle" style="padding-left: 5px;">
								<?php echo $this->_config[0]['vars']['STATUS']; ?>
&nbsp;:&nbsp;
								<select name="memberstatus" id="memberstatus" class="dropdown">
									<option value=""><?php echo $this->_config[0]['vars']['SELECTED']; ?>
</option>
									<?php $_from = $this->_tpl_vars['statusList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['status']):
?>
									<option value="<?php echo $this->_tpl_vars['status']['value']; ?>
" <?php if ($this->_tpl_vars['searchData']['memberstatus'] == $this->_tpl_vars['status']['value'] && $this->_tpl_vars['searchData']['memberstatus'] != ''): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['status']['text']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>					
							</td>
						</tr>
						<tr>
							<td align="center" valign="middle" style="padding-left: 5px;" colspan="3">
								<input type="submit" name="search" id="search" value="<?php echo $this->_config[0]['vars']['SEARCH']; ?>
" class="button_new" onClick="return submitform('?hdl=member/list', 0, '1', '1');"/>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<!--header-->
			<tr class="stdHeader" valign="top">
				<td width="4%" align="center" valign="middle" style="height:15px;"><?php echo $this->_config[0]['vars']['FIELD_ID']; ?>
</td>
				<td width="10%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['MEMBER_USER']; ?>
</td>
				<td width="15%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['MEMBER_NAME']; ?>
</td>
				<td width="30%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['MEMBER_ADDRESS']; ?>
</td>
				<td width="12%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['MEMBER_MAIL']; ?>
</td>
				<td width="10%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['MEMBER_PHONE']; ?>
</td>
				<td width="10%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['STATUS']; ?>
</td>
				<td width="8%" align="center" valign="middle"><?php echo $this->_config[0]['vars']['FUNCTION']; ?>
</td>
			</tr>
			<!--If cls = 1 then strClass = "record" else strClass = "evenRecord" end if-->
			<?php if (count ( $this->_tpl_vars['memberList'] ) > 0): ?>
			<?php $_from = $this->_tpl_vars['memberList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['items']):
?>
			<?php $this->assign('i', $this->_tpl_vars['i']+1); ?>
			<?php if ($this->_tpl_vars['i']%2 == 0): ?>
				<?php $this->assign('class', 'record'); ?>
			<?php else: ?>
				<?php $this->assign('class', 'evenRecord'); ?>
			<?php endif; ?>
			<tr class="<?php echo $this->_tpl_vars['class']; ?>
" valign="top">
				<td align="center" style="padding:5px;" valign="middle"><?php echo $this->_tpl_vars['items']['member_id']; ?>
</td>
				<td align="left" style="padding:5px;" valign="middle">
					<?php echo $this->_tpl_vars['items']['member_user']; ?>

				</td>
				<td align="left" style="padding:5px;" valign="middle">
					<?php echo $this->_tpl_vars['items']['member_name']; ?>

				</td>
				<td align="left" style="padding:5px;" valign="middle">
					<?php echo $this->_tpl_vars['items']['member_address']; ?>

				</td>
				<td align="left" style="padding:5px;" valign="middle">
					<a href="mailto:<?php echo $this->_tpl_vars['items']['member_mail']; ?>
"><?php echo $this->_tpl_vars['items']['member_mail']; ?>
</a>
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					<?php echo $this->_tpl_vars['items']['member_phone']; ?>

					<!--<?php echo ((is_array($_tmp=$this->_tpl_vars['items']['created_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d-%m-%Y") : smarty_modifier_date_format($_tmp, "%d-%m-%Y")); ?>
-->
				</td>
				<td align="center" style="padding:5px;" valign="middle">
					<?php echo $this->_tpl_vars['statusList'][$this->_tpl_vars['items']['status']]['text']; ?>

				</td>
				<td align="center" style="padding:5px;" valign="middle">
					<table cellpadding="0" cellspacing="0" border="0px;">
					<tr>
					  	<td style="border: 0px;padding-right:5px;"><img src="backend/images/edit.gif" width="15" height="15" title="edit" onClick="return submitform('?hdl=member/regist',<?php echo $this->_tpl_vars['items']['member_id']; ?>
,<?php echo $this->_tpl_vars['page']; ?>
,'2');" class="button" /></td>
						<td style="border: 0px;padding-left:5px;"><img src="backend/images/delete.jpg" width="15" height="15" title="delete" onClick="return ConfirmDeleteRecord('?hdl=member/list',<?php echo $this->_tpl_vars['items']['member_id']; ?>
,<?php echo $this->_tpl_vars['page']; ?>
);" class="button" /></td>
					</tr>
					</table>
				</td>
			</tr>
			<?php endforeach; endif; unset($_from); ?>
			<?php else: ?>
			<tr valign="top">
				<td colspan="9" align="center" style="padding:5px;" valign="middle">
				<?php echo $this->_tpl_vars['message']; ?>

				</td>
			</tr>
			<?php endif; ?>
			<tr class="stdHeader" valign="top">
				<td align="center" colspan="9" valign="middle" style="height:25px;">
					<?php if ($this->_tpl_vars['count'] > 1): ?>
					<div align="center" style="padding-right:5px;font-size:11px;"> <!-- Pager -->
						<?php echo $this->_tpl_vars['paging']; ?>

					</div>
					<?php endif; ?>
				</td>
			</tr>
			</table>
		</form>
		</td>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['include_footer'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>