<?php
	/**
	 * @note:	file forgot.php
	 * 
	 * @author 	Thunn - thunn84@gmail.com
	 * @version 1.0
	 */
	/*******************************Check login member***********************************/
	defined('DS') or die("Errors System");
	$template = FRONTEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$forgot	= isset($_POST["forgot"]) ? "TRUE" : null;

		if (!empty($forgot)) { // Click forgot
			$object		= isset($_POST["object"]) ?	$_POST["object"]: NULL;
			$message	= null;
			
			if (!strlen($object["member_user"]) || !strlen($object["member_user"])) $message = "Bạn cần phải nhập đầy đủ thông tin.";
			else {
				/*******************************************************************/
				$memberDao  = new MemberDao();
				$checked 	= $memberDao->getMemberByUserAndMail($object["member_user"], $object["member_mail"]);
				if (!empty($checked)) { // Send mail to member
					try {
						$password 	= Utils::randomPassword(6);
						/*******************************Area send mail*****************************/
			 			$sendmail	= new MailSend();
						$subject 	= "Nhà đất Văn Minh xin chào bạn !.";
						$headers 	= "From: ". CONFIG_MAIL .""."\r\n";
						$headers   .= "Reply-To : ". CONFIG_MAIL." <". CONFIG_MAIL .">"."\r\n";
						$body 		= $checked["member_user"]. ", inform login [ Username : ".$object["member_user"].", Password : ".$password.".
									  Hãy click <a href=\"www.nhadatvanminh.com\">www.nhadatvanminh.com</a> và đăng nhập vào hệ thống.
									  nhadatvanminh.com.";
						$param 		= "";
						@$sendmail->send($object["member_mail"], $subject, $body, $headers, $param, 0);
						/**************************************************************************/
						$memberDao->update_member(array("member_pass"=>md5($password)), $checked["member_id"]);
						header("Location: index.php");
					} catch (Exception $ex) {
						$memberDao->rollbackTrans();
					}
				} else $message = "Tên đăng nhập hoặc mật khẩu của bạn không đúng.";
				/*******************************************************************/
			}
			$smarty->assign("message", 	$message);
			$smarty->assign("object", 	$object);
		}
		
		return $smarty->display($template);
	}
?>