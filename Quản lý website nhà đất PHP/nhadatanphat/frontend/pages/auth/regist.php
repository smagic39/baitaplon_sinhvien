<?php
	/**
	 * @note:	file default.php
	 * 
	 * @author 	Thunn - thunn84@gmail.com
	 * @version 1.0
	 */
	/*******************************Check login member***********************************/
	defined('DS') or die("Errors System");
	$template = FRONTEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$object		= isset($_POST["object"]) ?		$_POST["object"]	 		: NULL;
		
		if ($object["regist"]) {
			$MemberDao= new MemberDao();
			$message  = null;
			$validate = new validate("member_register", MEMBER_VALIDATION_FILE);
			$message .= $validate->execute(	$object["member_user"], $object["member_pass"], $object["member_name"], $object["member_mail"],
											$object["member_address"], $object["member_phone"], $object["member_website"]);
			if ($message == "" && trim($object["member_pass"]) != trim($object["pass_confirm"])) $message.= PASS_CONFIRM_ERRORS;
			if ($message == "") {
				$existed = $MemberDao->checkExistedAccount(trim($object["member_user"]));
				if ($existed) $message.= USERNAME_EXISTED;
			}
			if ($message == "") {
				$MemberDao->beginTrans();
				try {
					foreach ($object as $key=>$value) $member[$key] = trim($value);
					$member["created_date"] = date(DATE_PHP);
					$member["member_pass"] 	= md5($member["member_pass"]);
					$member["status"] 		= 1;
					unset($member["pass_confirm"]);
					unset($member["regist"]);

					$MemberDao->insert_member($member);
					$MemberDao->commitTrans();
					$smarty->assign("result", 	MESSAGE_REGIST_SUCCESS);
				} catch (Exception $ex) {
					$MemberDao->rollbackTrans();
					if (!is_null($estateid) && $estateid != "") $message .= MESSAGE_UPDATE_FALSE.BR_TAG;
					else $message .= MESSAGE_INSERT_FALSE.BR_TAG;
				}
			}
			$smarty->assign("message", 	$message);
		}
		$smarty->assign("object", 	$object);
		
		return $smarty->display($template);
	}
?>