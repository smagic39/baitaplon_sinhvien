<?php
	/**
	 * @note:	file default.php
	 * 
	 * @author 	Thunn - thunn84@gmail.com
	 * @version 1.0
	 */
	/*******************************Check login member***********************************/
	defined('DS') or die("Errors System");
	$template = FRONTEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$estateid  	= isset($_REQUEST["estateid"]) ?	$_REQUEST["estateid"]	: null;
		$restore	= isset($_REQUEST["restore"]) ?		$_REQUEST["restore"]	: FALSE;
		$EstateDao 	= new EstateDao();
		$ChildDao	= new ChildDao();
		$categories = new Categories();
		
		if (!empty($restore)) 	$EstateDao->restore_estate($estateid);
		$estate		= $EstateDao->getEstateById($estateid);

		$categoryNames = $categories->getCategoryName($ChildDao, $estate["child_id"]);
		$categoryNames = array_values($categoryNames);
		for ($i = count($categoryNames); $i > 0 ; $i--) $categoryName .= " => ".$categoryNames[$i-1];

		$smarty->assign("Direction",	Direction::getList($textlang));
		$smarty->assign("categoryName",	$categoryName);
		$smarty->assign("estate",		$estate);
		
		if(file_exists(LOGO_PATH.$estate['member_id'].".jpg")){
			$memberLogo = $estate['member_id'].".jpg";
			$smarty->assign("memberLogo",		$memberLogo);
		}
		
		return $smarty->display($template);
	}
?>