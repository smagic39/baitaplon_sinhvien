<?php
	/**
	 * @note:	file default.php
	 * 
	 * @author 	Thunn - thunn84@gmail.com
	 * @version 1.0
	 */
	/*******************************Check login member***********************************/
	defined('DS') or die("Errors System");
	$template = FRONTEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$object		= isset($_POST["object"]) ?		$_POST["object"]	 		: NULL;
		$MemberDao	= new MemberDao();

		if ($object["regist"]) {
			$message  = null;
			$validate = new validate("member_register", MEMBER_VALIDATION_FILE);
			$message .= $validate->execute(	$object["member_user"], "123456", $object["member_name"], $object["member_mail"],
											$object["member_address"], $object["member_phone"], $object["member_website"]);

			if ($message == "") {
				//$MemberDao->beginTrans();//This transaction contains error, so I have to remove it.
				try {
					foreach ($object as $key=>$value) $member[$key] = trim($value);
					$memberid	= $_SESSION["_LOGIN_ESTATE_"]["member_id"];
					$member["created_date"] = date(DATE_PHP);
					unset($member["regist"]);

					$MemberDao->update_member($member, $memberid);
					//$MemberDao->commitTrans();
					$_SESSION["_LOGIN_ESTATE_"]	= $member;
					$_SESSION["_LOGIN_ESTATE_"]["member_id"]= $memberid;
					header("Location: index.php?hdl=member/index");
					exit();
				} catch (Exception $ex) {
					//$MemberDao->rollbackTrans();
					if (!is_null($estateid) && $estateid != "") $message .= MESSAGE_UPDATE_FALSE.BR_TAG;
					else $message .= MESSAGE_INSERT_FALSE.BR_TAG;
				}
			}
			$smarty->assign("message", 	$message);
		} else {
			$object = $_SESSION["_LOGIN_ESTATE_"];
			$object["pass_confirm"] = null;
			$object["member_pass"]	= null;
		}
		$smarty->assign("object", 	$object);
		
		return $smarty->display($template);
	}
?>