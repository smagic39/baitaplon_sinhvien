<?php
	/**
	 * @note:	file default.php
	 * 
	 * @author 	Thunn - thunn84@gmail.com
	 * @version 1.0
	 */
	/*******************************Check login member***********************************/
	defined('DS') or die("Errors System");
	$template = FRONTEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$object		= isset($_POST["object"]) ?		$_POST["object"]	 		: NULL;
		$MemberDao	= new MemberDao();
		
		if ($object["regist"]) {
			$message  = null;
			$validate = new validate("change_password", MEMBER_VALIDATION_FILE);
			$message .= $validate->execute(	$object["member_pass"]);
			if ($message == "" && md5(trim($object["pass_old"])) != trim($_SESSION["_LOGIN_ESTATE_"]["member_pass"])) $message.= PASS_OLD_ERRORS;
			if ($message == "" && trim($object["member_pass"]) != trim($object["pass_confirm"])) $message.= PASS_CONFIRM_ERRORS;

			if ($message == "") {
				$MemberDao->beginTrans();
				try {
					foreach ($object as $key=>$value) $member[$key] = trim($value);
					$member["created_date"] = date(DATE_PHP);
					$member["member_pass"] 	= md5($member["member_pass"]);
					unset($member["pass_confirm"]);
					unset($member["pass_old"]);
					unset($member["regist"]);

					$MemberDao->update_member($member, $_SESSION["_LOGIN_ESTATE_"]["member_id"]);
					$MemberDao->commitTrans();
					$_SESSION["_LOGIN_ESTATE_"]["member_pass"] = $member["member_pass"];
					header("Location: index.php?hdl=member/index");exit();
				} catch (Exception $ex) {
					$MemberDao->rollbackTrans();
					if (!is_null($estateid) && $estateid != "") $message .= MESSAGE_UPDATE_FALSE.BR_TAG;
					else $message .= MESSAGE_INSERT_FALSE.BR_TAG;
				}
			}
			$smarty->assign("message", 	$message);
		} else {
			$object = $MemberDao->getMemberById($_SESSION["_LOGIN_ESTATE_"]["member_id"]);
			$object["pass_confirm"] = null;
			$object["member_pass"]	= null;
		}
		$smarty->assign("object", 	$object);
		
		return $smarty->display($template);
	}
?>