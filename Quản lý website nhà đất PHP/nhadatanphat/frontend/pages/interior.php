<?php
	/**
	 * @note:	file default.php
	 * 
	 * @author 	Thunn - thunn84@gmail.com
	 * @version 1.0
	 */
	/*******************************Check login member***********************************/
	defined('DS') or die("Errors System");
	$template = FRONTEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$page	 		= isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"])	: 1;
		
		$cat_interior_id = isset($_REQUEST["cat_interior_id"]) ? trim($_REQUEST["cat_interior_id"])	: '';
		
		$InteriorDao	= new InteriorDao();

		$offset 	= (int)($page-1)*LIMIT_INTERIOR;
		$cat_interior_id = (int)$cat_interior_id;
		$count		= $InteriorDao->getCountInterior(Status::active, $cat_interior_id);
		if ($offset >= $count) {
			$offset	= (int)$offset - (int)LIMIT_INTERIOR;
			$page	= $page - 1;
		}
		$InteriorList = $InteriorDao->getAllInteriors(Status::active, $cat_interior_id, (int)LIMIT_INTERIOR, $offset, 0);
		
		$lable = "CÁC MẪU NỘI THẤT";
		
		$categoriesDao = new CatInteriorDao();
		$categories_list = $categoriesDao->get_categories_list();
		$smarty->assign("cat_list", $categories_list);
		
		if ($cat_interior_id > 0) :
			$catInteriorDao = new CatInteriorDao();
			$catInteriorObj = $catInteriorDao->get_categories_by_id($cat_interior_id);
			if (!empty($catInteriorObj['category_name']))
				$lable.= " -> ".$catInteriorObj['category_name'];
		endif;
				
		if (strlen($kind)) 
			$params = "&interiorid=$interiorid&cat_id={$cat_interior_id}";
		else
			$params = null;

		//$smarty->assign("paging",   	PagerUtils::PagerSmarty($page, $count, "?hdl=interior", (int)LIMIT_INTERIOR, (int)PAGES_SIZES));
		$pageAll = PagerUtils::PagerPear($count, $page, LIMIT_INTERIOR, array());
		$smarty->assign("paging",		$pageAll["all"]);
		$smarty->assign("InteriorList", $InteriorList);
		$smarty->assign("page",$page);
		$smarty->assign("lable",$lable);

		$smarty->assign("cat_interior_id", $cat_interior_id);
		unset($InteriorDao);

		return $smarty->display($template);
	}
?>