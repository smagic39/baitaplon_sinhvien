<?php
	/**
	 * @note:	file default.php
	 * 
	 * @author 	Thunn - thunn84@gmail.com
	 * @version 1.0
	 */
	/*******************************Check login member***********************************/
	defined('DS') or die("Errors System");
	$template = FRONTEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$cateid   		= isset($_REQUEST["cateid"]) ?		$_REQUEST["cateid"] 	: null;
		$childid   		= isset($_REQUEST["childid"]) ?		$_REQUEST["childid"] 	: null;
		$province  		= isset($_REQUEST["province"]) ?	$_REQUEST["province"] 	: null;
		$district  		= isset($_REQUEST["district"]) ?	$_REQUEST["district"] 	: null;
		$direction 		= isset($_REQUEST["direction"]) ?	$_REQUEST["direction"] 	: null;
		$price  		= isset($_REQUEST["price"]) ?		$_REQUEST["price"] 		: null;
		$page	 		= isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"])	: 1;

		$EstateDao		= new EstateDao();
		$ProvinceDao	= new ProvinceDao();

		$prices	 	= split('[-]', $price);
		$pricestart	= $prices[0];
		$priceend	= isset($prices[1]) ? $prices[1] : null;
		$sqlparam	= array("categoryid"=>$cateid, "childid"=>$childid, "pricestart"=>$pricestart, "priceend"=>$priceend, "provinceid"=>$province, "direction"=>$direction, "districtid"=>$district, "genre"=>1);
		
		$offset 	= (int)($page-1)*LIST_RECORD;
		$count		= $EstateDao->getCountEstateByParams($sqlparam, Status::active);
		if ($offset >= $count) {
			$offset	= (int)$offset - (int)LIST_VIP;
			$page	= $page - 1;
		}

		$EstateGenreList= $EstateDao->getAllEstateByParams($sqlparam, Status::active, null, $offset);
		//$EstateGenreList= $EstateDao->getAllEstateByParams($sqlparam, Status::active, (int)LIST_VIP, $offset);

		$params = "";
		if (strlen($cateid)) 	$params .= "&cateid=$cateid";
		if (strlen($province)) 	$params .= "&province=$province";
		if (strlen($district)) 	$params .= "&district=$district";
		if (strlen($direction)) $params .= "&direction=$direction";
		if (strlen($price)) 	$params .= "&price=$price";
		
		$PositionFiv = $PromoteDao->getAllPromotes($province, Status::active, Position::positionfive, 2, 0);

		$smarty->assign("paging",   		PagerUtils::PagerSmarty($page, $count, "?hdl=vip$params", (int)LIST_VIP, (int)PAGES_SIZES));
		$smarty->assign("EstateGenreList", 	$EstateGenreList);
		$smarty->assign("Direction",		Direction::getList($textlang));
		$smarty->assign("PositionFiv",		$PositionFiv);
		$smarty->assign("ProvinceDetail", 	$ProvinceDao->getProvinceById($province));

		$smarty->assign("page",				$page);
		$smarty->assign("cateid",			$cateid);
		$smarty->assign("province",			$province);
		$smarty->assign("district",			$district);
		$smarty->assign("direction",		$direction);
		$smarty->assign("price",			$price);
		$smarty->assign("params",			$params);

		return $smarty->display($template);
	}
?>