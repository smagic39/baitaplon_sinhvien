<?php
	/**
	 * @note:	file default.php
	 * 
	 * @author 	Thunn - thunn84@gmail.com
	 * @version 1.0
	 */
	/*******************************Check login member***********************************/
	defined('DS') or die("Errors System");
	$template = FRONTEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$page	 	= isset($_REQUEST["page"]) ?		trim($_REQUEST["page"])	: 1;
		//$cateid 	= isset($_REQUEST["cateid"]) ? 		$_REQUEST["cateid"] 	: null;
		$informid 	= isset($_REQUEST["informid"]) ? 	$_REQUEST["informid"] 	: null;
		$InformDao 	= new InformDao();
		
		$InformDetail	= $InformDao->getInformById($informid);
		
		if (!empty($newsid)) $limit = LIMIT_INFORM;
		else $limit = INFORM_RECORD;
		$offset 	= (int)($page-1)*$limit;
		$count		= $InformDao->getCountInform($cateid, Status::active, $informid);

		if ($offset >= $count) {
			$offset	= (int)$offset - (int)$limit;
			$page	= $page - 1;
		}

		$InformList	= $InformDao->getAllInforms($cateid, Status::active, $informid, (int)$limit, (int)$offset);
		unset($InformDao);
		
		if (empty($InformDetail) && $count == 1) {
			$InformDetail 	= $InformList[0];
			$informid		= $InformList[0]["inform_id"];
			$InformList		= null;
		}
		
		$params = null;
		if (!empty($cateid)) $params 	.= "&cateid=$cateid";
		if (!empty($informid)) $params 	.= "&informid=$informid";

		//$smarty->assign("paging",   	PagerUtils::PagerSmarty($page, $count, "?hdl=inform$params", (int)$limit, (int)PAGES_SIZES));
		$pageAll = PagerUtils::PagerPear($count, $page, $limit, array());
		$smarty->assign("paging",		$pageAll["all"]);
		$smarty->assign("InformList", 	$InformList);
		$smarty->assign("InformDetail", $InformDetail);
		$smarty->assign("cateid", 		$cateid);
		$smarty->assign("informid", 	$informid);
		$smarty->assign("page",			$page);
		
		return $smarty->display($template);
	}
?>