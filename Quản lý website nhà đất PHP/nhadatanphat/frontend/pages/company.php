<?php
	/**
	 * @note:	file default.php
	 * 
	 * @author 	Thunn - thunn84@gmail.com
	 * @version 1.0
	 */
	/*******************************Check login member***********************************/
	defined('DS') or die("Errors System");
	$template = FRONTEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$companyid	= isset($_REQUEST["companyid"]) ?	$_REQUEST["companyid"] 	: null;
		$province  	= isset($_REQUEST["province"]) ?	$_REQUEST["province"] 	: null;
		$page	 	= isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"])	: 1;
		$CompanyDao	= new CompanyDao();
		
		
		$CompanyDetail = $CompanyDao->getCompanyById($companyid);
		
		if(isset($CompanyDetail['province_id'])){
			$province = $CompanyDetail['province_id'];
		}
		
		if (!empty($companyid)) $limit = LIMIT_COMPANY;
		else $limit = COMPANY_RECORD;
		$offset 	= (int)($page-1)*$limit;
		$count		= $CompanyDao->getCountCompany($companyid, $province, Status::active);
		if ($offset >= $count) {
			$offset	= (int)$offset - (int)$limit;
			$page	= $page - 1;
		}

		$CompanyList = $CompanyDao->getAllCompanies($companyid, $province, Status::active, (int)$limit, $offset, array());
		
		if (!empty($companyid))
			$params = "&companyid=$companyid";
		else
			$params = null;

		if(isset($province)){
			$provinceDao	= new ProvinceDao();
			$provinceInfo 	= $provinceDao->getProvinceById($province);
			$smarty->assign("provinceInfo",	$provinceInfo);
		}

		//$smarty->assign("paging",   	PagerUtils::PagerSmarty($page, $count, "?hdl=company$params", (int)$limit, (int)PAGES_SIZES));
		$pageAll = PagerUtils::PagerPear($count, $page, $limit, array());
		$smarty->assign("paging",		$pageAll["all"]);
		$smarty->assign("CompanyList", 	$CompanyList);
		$smarty->assign("CompanyDetail",$CompanyDetail);
		$smarty->assign("companyid",	$companyid);
		$smarty->assign("page",			$page);

		return $smarty->display($template);
	}
?>