<?php
	/**
	 * @note:	file default.php
	 * 
	 * @author 	Thunn - thunn84@gmail.com
	 * @version 1.0
	 */
	/*******************************Check login member***********************************/
	defined('DS') or die("Errors System");
	$template = FRONTEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$newsid		= isset($_REQUEST["newsid"]) ?	$_REQUEST["newsid"] 	: "";
		$genre	 	= isset($_REQUEST["genre"]) ?	trim($_REQUEST["genre"]): "";
		$page	 	= isset($_REQUEST["page"]) ?	trim($_REQUEST["page"])	: 1;
		$NewsDao	= new NewsDao();

		$NewsDetail = $NewsDao->getNewsById($newsid);
		
		if (!empty($newsid))
			$limit = LIMIT_NEW;
		else 
			$limit = NEW_RECORD;
		$offset 	= (int)($page-1)*$limit;
		$count		= $NewsDao->getCountNews($genre, Status::active, $newsid);
		if ($offset >= $count) {
			$offset	= (int)$offset - (int)$limit;
			$page	= $page - 1;
		}
		$NewsList= $NewsDao->getAllNews($genre, Status::active, $newsid, (int)$limit, $offset);
		unset($NewsDao);

		$params = null;
		if ($genre != "")
			$params .= "&genre=$genre";

		if($newsid != "")
			$params .= "&newsid=$newsid";

		//$smarty->assign("paging",   	PagerUtils::PagerSmarty($page, $count, "?hdl=news$params", (int)$limit, (int)PAGES_SIZES));
		$pageAll = PagerUtils::PagerPear($count, $page, $limit, array());
		$smarty->assign("paging",			$pageAll["all"]);
		$smarty->assign("NewsList", 	$NewsList);
		$smarty->assign("NewsDetail",	$NewsDetail);
		$smarty->assign("newsid",		$newsid);
		$smarty->assign("genre",		$genre);
		$smarty->assign("page",			$page);

		return $smarty->display($template);
	}
?>