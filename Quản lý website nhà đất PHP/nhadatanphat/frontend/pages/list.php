<?php
	/**
	 * @note:	file default.php
	 * 
	 * @author 	Thunn - thunn84@gmail.com
	 * @version 1.0
	 */
	/*******************************Check login member***********************************/
	defined('DS') or die("Errors System");
	$template = FRONTEND_TEMPLATE_PATH.$hdl.TPL_TYPE;
$cleanUnicodeStr = trim(preg_replace('#[^\p{L}\p{N}]+#u', ' ', $unicodeStr));
echo $cleanUnicodeStr;

	if (file_exists($template)) {
		$keyword		= isset($_REQUEST["keyword"]) ?		isset($_POST["keyword"]) ? $_POST["keyword"] : urldecode($_GET["keyword"]) 	: null;
		
		$cateid  		= isset($_REQUEST["cateid"]) ?		$_REQUEST["cateid"] 	: null;
		$child  		= isset($_REQUEST["child"]) ?		$_REQUEST["child"] 	: null;
		$province  		= isset($_REQUEST["province"]) ?	$_REQUEST["province"] 	: null;
		$district  		= isset($_REQUEST["district"]) ?	$_REQUEST["district"] 	: null;
		$direction 		= isset($_REQUEST["direction"]) ?	$_REQUEST["direction"] 	: null;
		$price  		= isset($_REQUEST["price"]) ?		$_REQUEST["price"] 		: null;
		$page	 		= isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"])	: 1;

		$EstateDao		= new EstateDao();
		$CompanyDao	  	= new CompanyDao();
		$ProvinceDao	= new ProvinceDao();
		
		/*******************************Get keyword by id********************************/
		$KeywordDao		= new KeywordDao();
		$prices	= $KeywordDao->getKeywordById($price);
		if($price == -1){
			$pricestart	= -1;
			$priceend   = -1;
		}else{
			$pricestart	= $prices['keyword_start'];
			$priceend   = $prices['keyword_end'];
		}
		
		//$pricestart	= $prices[0];
		//$priceend	= isset($prices[1]) ? Utils::getNumberic($prices[1]) : null;
		//$sqlparam	= array("state"=>1, "keyword"=>$keyword, "categoryid"=>$cateid, "childid"=>$child, "pricestart"=>$pricestart, "priceend"=>$priceend, "provinceid"=>$province, "districtid"=>$district, "direction"=>$direction);
		$keyword = Utils::vietnameseToASCII($keyword);

		$sqlparam	= array("isVipGroup"=>1, "keyword"=>$keyword, "categoryid"=>$cateid, "childid"=>$child, "pricestart"=>$pricestart, "priceend"=>$priceend, "provinceid"=>$province, "districtid"=>$district, "direction"=>$direction);
		if($cateid == "-1"){
			//cateId=-1=>rao vat o list van lay danh sach VIP, Noi bat, co Phi.
			unset($sqlparam['childid']);
			unset($sqlparam['categoryid']);
			$sqlparam['genreSearch']=1;
		}else{
			$sqlparam['genreSearch']=1;
		}
		if ($province)
			$pageSize = 70;//for province.
		else 
			$pageSize = 45;//for province.
		
		//$paramvip	= array("state"=>1, "keyword"=>$keyword, "categoryid"=>$cateid, "childid"=>$child, "pricestart"=>$pricestart, "priceend"=>$priceend, "provinceid"=>$province, "districtid"=>$district, "direction"=>$direction, "genre"=>EstateGenre::estatevip);
		//$paramvip	= array("feeEstateOnly"=>1, "keyword"=>$keyword, "categoryid"=>$cateid, "childid"=>$child, "pricestart"=>$pricestart, "priceend"=>$priceend, "provinceid"=>$province, "districtid"=>$district, "direction"=>$direction, "genre"=>1);//Vip: genre=1
		$EstateVIPList	= $EstateDao->getAllEstateByParams($sqlparam, null, /*(int)VIP_ESTATE*/$pageSize, 0, $configObj['usd_vnd'], $configObj['sjc_vnd']);

		$sqlparamNewNews = array();
		if($cateid == "-1"){
			$sqlparamNewNews["isFreeNewsOnly"] = 1;
		}
		$sqlparamNewNews["isNewNews"] = 1;
		$sqlparamNewNews["keyword"] = $keyword;
		$sqlparamNewNews["categoryid"] = $cateid;
		$sqlparamNewNews["childid"] = $child;
		$sqlparamNewNews["pricestart"] = $pricestart;
		$sqlparamNewNews["priceend"] = $priceend;
		$sqlparamNewNews["provinceid"] = $province;
		$sqlparamNewNews["districtid"] = $district;
		$sqlparamNewNews["direction"] = $direction;
		$genre 			= isset($_GET["genre"]) ?	$_GET["genre"] 	: "2";
		$news 			= isset($_GET["news"]) ?	$_GET["news"] 	: null;
		$type 			= isset($_GET["type"]) ? 	$_GET["type"] 	: null;
		$smarty->assign("genre",			$genre);
		$smarty->assign("news",				$news);
		$smarty->assign("type",				$type);
		if (is_null($news) && is_null($type)) {
			$EstateGenreList1 = $EstateDao->getAllEstateByParams(array("genre"=>$genre, "state"=>1, "feeEstateOnly"=>1), Status::active, 4, 0);
			$smarty->assign("EstateGenreList1", 	$EstateGenreList1);
		}
		if (!is_null($news)) {
			$NewsDao		= new NewsDao();
			$NewsList		= array();
			$NewsList		= $NewsDao->getAllNews($news, Status::active, null, 4, 0);
			$smarty->assign("NewsList", 	$NewsList);
			unset($NewsDao);
		}
		$InteriorDao  	= new InteriorDao();
		if ($type == "interior") {
			$InteriorTops 	= $InteriorDao->getAllInteriors(Status::active, '',10, 0, 1);
			$smarty->assign("InteriorTops",	$InteriorTops);
		}
		$InteriorList 	= $InteriorDao->getAllInteriors(Status::active, '',5,0, 1);
		$smarty->assign("InteriorList",		$InteriorList);
		unset($InteriorDao);
		
		$page = (int)$page;
		if ($province)
			$limit_estate = 52;
		else
			$limit_estate = 33;
		$offset 	= ($page-1)*$limit_estate;
		
		$EstateGenreList = $EstateDao->getAllEstateByParams($sqlparamNewNews, Status::active, $limit_estate, $offset, $configObj['usd_vnd'], $configObj['sjc_vnd']);
		$count		= sizeof($EstateDao->getAllEstateByParams($sqlparamNewNews, Status::active, 0, 0, 0, 0));

		if($offset >= $count){
			$offset	= (int)$offset - $limit_estate;
			$page	= $page - 1;
		}
		
		$params = null;
		if (strlen($keyword)) 	$params .= "&keyword=".urlencode($keyword);
		if (strlen($cateid)) 	$params .= "&cateid=$cateid";
		if (strlen($child)) 	$params .= "&childid=$child";
		if (strlen($province)) 	$params .= "&province=$province";
		if (strlen($district)) 	$params .= "&district=$district";
		if (strlen($direction)) $params .= "&direction=$direction";
		if (strlen($price)) 	$params .= "&price=$price";
		
		$provinceTmp = -1;//toan quoc.
		$companyParams = array();
		if(isset($province) && strlen($province) > 0){
			$provinceTmp = $province;
			$companyParams = array("isShowInProvince"=>1, "isRandom"=>1);
		}else{
			$companyParams = array("isShowInHomePage"=>1, "isRandom"=>1);
		}
		
		//$PositionFiv = $PromoteDao->getAllPromotes($province, Status::active, Position::positionfive, 2, 0);
		$PositionFiv = $PromoteDao->getAllPromotes($provinceTmp, Status::active, Position::positionfive, 1, 0);
		
		$CompanyList = $CompanyDao->getAllCompanies(null, /*$provinceTmp*/$province, Status::active, 10,0, $companyParams);

		$PositionSix 	= $PromoteDao->getAllPromotes($provinceTmp, Status::active, Position::positionsix, 1, 0);
		$PositionFou 	= $PromoteDao->getAllPromotes($provinceTmp, Status::active, Position::positionfour, 0, 0);
		$PositionThr 	= $PromoteDao->getAllPromotes($provinceTmp, Status::active, Position::positionthree, 0, 0);
		$PositionTC 	= $PromoteDao->getAllPromotes($provinceTmp, Status::active, Position::positionone, 1, 0);
		$PositionTR 	= $PromoteDao->getAllPromotes($provinceTmp, Status::active, Position::positiontwo, 1, 0);
		
		//$smarty->assign("paging",   		PagerUtils::PagerSmarty($page, $count, "?hdl=list$params", (int)LIST_ESTATE, (int)PAGES_SIZES));
		$pageAll = PagerUtils::PagerPear($count, $page, LIST_ESTATE, array());
		$smarty->assign("paging",			$pageAll["all"]); 
		$smarty->assign("EstateVIPList", 	$EstateVIPList);
		$smarty->assign("EstateGenreList", 	$EstateGenreList);
		$smarty->assign("CompanyList", 		$CompanyList);
		$smarty->assign("Direction",		Direction::getList($textlang));
		$smarty->assign("PositionFiv",		$PositionFiv);
		$smarty->assign("ProvinceDetail", 	$ProvinceDao->getProvinceById($province));
		$smarty->assign("page",				$page);
		$smarty->assign("cateid",			$cateid);
		$smarty->assign("childid",			$child);
		$smarty->assign("province",			$province);
		$smarty->assign("district",			$district);
		$smarty->assign("direction",		$direction);
		$smarty->assign("price",			$price);
		$smarty->assign("params",			$params);
		
		$smarty->assign("Position5", 	$PositionFive);
		$smarty->assign("PositionSix",	$PositionSix);
		$smarty->assign("Position4", 	$PositionFou);
		$smarty->assign("Position3", 	$PositionThr);
		$smarty->assign("PositionTopCenter", $PositionTC[0]);
		$smarty->assign("PositionTopRight", $PositionTR[0]);
		
		unset($EstateDao);

		return $smarty->display($template);
	}
?>