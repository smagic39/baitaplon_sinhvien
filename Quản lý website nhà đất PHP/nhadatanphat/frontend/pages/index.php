<?php
	/**
	 * @note:	file default.php
	 * 
	 * @author 	Thunn - thunn84@gmail.com
	 * @version 1.0
	 */
	/*******************************Check login member***********************************/
	defined('DS') or die("Errors System");
	$template = FRONTEND_TEMPLATE_PATH.$hdl.TPL_TYPE;

	if (file_exists($template)) {
		$genre 			= isset($_GET["genre"]) ?	$_GET["genre"] 	: "2";
		$news 			= isset($_GET["news"]) ?	$_GET["news"] 	: null;
		$type 			= isset($_GET["type"]) ? 	$_GET["type"] 	: null;
		$page	 		= isset($_REQUEST["page"]) ? 		trim($_REQUEST["page"])	: 1;
		$EstateDao		= new EstateDao();
		$sqlparam	= array("isVipGroup"=>1);
		$sqlparam['genreSearch']=1;
		$pageSize =70;
		
		//$paramvip	= array("state"=>1, "keyword"=>$keyword, "categoryid"=>$cateid, "childid"=>$child, "pricestart"=>$pricestart, "priceend"=>$priceend, "provinceid"=>$province, "districtid"=>$district, "direction"=>$direction, "genre"=>EstateGenre::estatevip);
		$EstateVIPList	= $EstateDao->getAllEstateByParams($sqlparam, null, /*(int)VIP_ESTATE*/$pageSize, 0, $configObj['usd_vnd'], $configObj['sjc_vnd']);
		$smarty->assign("EstateVIPList", 	$EstateVIPList);
//		print_r($EstateVIPList);die('fuckj');
		
		if (is_null($news) && is_null($type)) {
			$EstateGenreList1 = $EstateDao->getAllEstateByParams(array("genre"=>$genre, "state"=>1, "feeEstateOnly"=>1), Status::active, 4, 0);
			$smarty->assign("EstateGenreList", 	$EstateGenreList1);
//			print_r($EstateGenreList1);die('fuckj');
		}
		
		for ($i=0; $i<(3-(count($GroupEstate)%3)); $i++) 
			$another[$i] = "";
		$page = (int)$page;
		$limit_estate = 47;
		$offset 	= ($page-1)*$limit_estate;
		
		$EstateNewList	= $EstateDao->getAllEstateByParams(array("isNewNews"=>1, "state"=>1), Status::active, $limit_estate, $offset);
//		print_r($EstateNewList); die('xx');
		$count		= $EstateDao->getCounOfEstateByParams(array("isNewNews"=>1, "state"=>1), Status::active);
		if($offset >= $count){
			$offset	= (int)$offset - $limit_estate;
			$page	= $page - 1;
		}
		$pageAll = PagerUtils::PagerPear($count, $page, $limit_estate, array());
		$smarty->assign("paging",			$pageAll["all"]); 
		
		$smarty->assign("another",			$another);
		$smarty->assign("EstateNewList", 	$EstateNewList);
		unset($EstateDao);
		
		if (!is_null($news)) {
			$NewsDao		= new NewsDao();
			$NewsList		= array();
			$NewsList		= $NewsDao->getAllNews($news, Status::active, null, 4, 0);
//			print($NewsList);die();
			$smarty->assign("NewsList", 	$NewsList);
			unset($NewsDao);
		}
		
		$InteriorDao  	= new InteriorDao();
		if ($type == "interior") {
			$InteriorTops 	= $InteriorDao->getAllInteriors(Status::active, '',10, 0, 1);
			$smarty->assign("InteriorTops",	$InteriorTops);
		}
		$InteriorList 	= $InteriorDao->getAllInteriors(Status::active, '',5,0, 1);
		$smarty->assign("InteriorList",		$InteriorList);
		unset($InteriorDao);
		
		
		
		
		
		$CompanyDao	  	= new CompanyDao();
		$additionalParams = array("isShowInHomePage"=>1, "isRandom"=>1);
		$CompanyList	= $CompanyDao->getAllCompanies(null, null, Status::active, 10,0, $additionalParams);
		$smarty->assign("CompanyList", 		$CompanyList);
		unset($CompanyDao);
		
		$smarty->assign("Position5", 	$PositionFive);
		$smarty->assign("genre",			$genre);
		$smarty->assign("news",				$news);
		$smarty->assign("type",				$type);
		$smarty->assign("Direction",		Direction::getList($textlang));
		return $smarty->display($template);
	}
?>