<?php 
	$config_frontend_dir = dirname(__FILE__);
	
	require_once $config_frontend_dir . DS."constants.inc";
	require_once $config_frontend_dir . DS."init_smarty_frontend.inc";
	require_once $config_frontend_dir . DS."validate".DS."@validate.inc";
	require_once $config_frontend_dir . DS."message".DS."message.inc";
	unset($config_frontend_dir);
?>