<?php
/*****************************************************************
 * @project_name: localframe
 * @package: package_name
 * @file_name: message.inc
 * @descr:
 * 
 * @author 	Khuong Van Chien - khuongchien@gmail.com
 * @version 1.0
 *****************************************************************/
if (!defined("MESSAGE_BACKEND_INC") ) {
	Define("MESSAGE_RESULT_NULL", 		"Không có dữ liệu hiển thị ở đây.");
	
	Define("ORDER_NAME_NULL", 			"Sản phẩm bạn mua không được trống");
	Define("ORDER_AMOUNT_NULL", 		"Số lượng hàng mua không được trống");
	Define("ORDER_AMOUNT_LENGTH", 		"Số lượng hàn mua không lớn hơn 4 ký tự.");
	Define("ORDER_AMOUNT_ERROR", 		"Số lượng hàng là kiểu nguyên lớn hơn 1 va nhỏ hơn 4 ký tự.");
	Define("ORDER_PRICE_NULL", 			"Giá tiền không được trống");
	Define("ORDER_PRICE_LENGTH", 		"Giá tiền không lớn hơn 10 ký tự.");
	Define("ORDER_PRICE_ERROR", 		"Giá tiền là không nhỏ hơn 1 và lớn hơn 10 ký tự.");
	Define("ORDER_PERSON_NULL", 		"Tên người mua hàng không được để trống");
	Define("ORDER_PERSON_LENGTH", 		"Tên người mua hàng không lớn hơn 255 ký tự.");
	Define("ORDER_EMAIL_NULL", 			"Mail của người mua hàng không được để trống.");
	Define("ORDER_EMAIL_LENGTH", 		"Mail người mua hàng không lớn hơn 100 ký tự.");
	Define("ORDER_EMAIL_ERROR", 		"Mail người mua hàng không đúng định dạng. VD: chienkv@hotmail.com");
	Define("ORDER_PHONE_NULL", 			"SĐT người mua hàng không được để trống.");
	Define("ORDER_PHONE_LENGTH", 		"Phone của bạn không lớn hơn 15 ký tự.");
	Define("ORDER_PHONE_ERROR", 		"Phone của bạn không đúng định dạng. VD: 0321.936.368");
	Define("ORDER_MOBILE_LENGTH", 		"Mobile của bạn không lớn hơn 15 ký tự.");
	Define("ORDER_MOBILE_ERROR", 		"Mobile của bạn không đúng định dạng. VD: 0973.586.186");
	Define("ORDER_ADDRESS_NULL", 		"Địa chỉ giao hàng của bạn không được trống.");
	Define("ORDER_ADDRESS_LENGTH", 		"Địa chỉ giao hàng của bạn không được lớn hơn 255 ký tự.");
	Define("ORDER_DATE_NULL", 			"Ngày giao hàng không được trống.");
	Define("ORDER_DATE_ERROR", 			"Ngày giao hàng không đúng định dạng.");
	Define("ORDER_NOTE_NULL", 			"Ghi chú không được để trống.");
	Define("ORDER_NOTE_LENGTH", 		"Ghi chú không được lớn hơn 500 ký tự.");
	
}
?>
