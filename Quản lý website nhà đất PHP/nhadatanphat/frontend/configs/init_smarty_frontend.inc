<?php
	$smarty = new Smarty();
	$smarty->template_dir 	= FRONTEND_TEMPLATE_PATH;
	$smarty->compile_dir 	= FRONTEND_TEMPLATE_C_PATH;
	$smarty->cache_dir 		= CACHE_PATH;
	$smarty->config_dir 	= LANGUAGES_PATH;
	$smarty->plugins_dir 	= array(SMARTY_DIR . "plugins", SMARTY_DIR."plugins".DS."plugins_ajax");
?>