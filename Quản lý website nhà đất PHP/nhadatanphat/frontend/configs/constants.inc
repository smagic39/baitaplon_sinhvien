<?php 
/*****************************************************************
 * @project_name: localframe
 * @package: package_name
 * @file_name: contants.inc
 * @descr:
 * 
 * @author 	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 *****************************************************************/
if (!defined("CONTANTS_FRONTEND_INC") ) {
	define("LANGUAGES_PATH", 	FRONTEND_CONF_PATH.DS."languages".DS);

	define("LIMIT_RECORD", 	"40"); 		/***********************Record view on page**********************/
	define("PAGES_SIZES", 	"10"); 		/************************Page view on site***********************/
	define("PAGING_MODE", 	"Jumping"); /***********************Using paging of pear*********************/
	define("DETAIL_RECORD", "20");
	define("LIST_RECORD", 	"20");
	
	define("LIST_ESTATE", 	"20");
	define("VIP_ESTATE", 	"10");
	
	define("LIMIT_INTERIOR","21");
	define("LIMIT_NEW",		"20");
	define("NEW_RECORD",	"30");
	define("LIMIT_COMPANY",	"20");
	define("COMPANY_RECORD","30");
	define("LIMIT_INFORM",	"20");
	define("INFORM_RECORD",	"30");
	define("LIMIT_ESTATE", 	"40");
	define("LIST_VIP", 		"26");
	define("ESTATE_RECORD", "30");
	
	
	define("NO_FRIEND", 	0);
	define("IS_FRIEND", 	1);
	define("CANCEL_FRINED", 2);
	
	define("COMMENT_TO_MEMBER",	0);
	define("COMMENT_TO_CLUB", 	1);
	define("MSG_CONFIRM_DELETE", "Are you source delete?");
	
	define("NO_ACTIVE",	0);
	define("ACTIVE", 1);
	define("CREDITS", 500);
	
	define("LINK_ACTIVE", "This link is hidden from user.");
}
?>
