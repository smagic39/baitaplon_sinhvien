<?php
/**
 * @project_name: localframe
 * @file_name: DistrictDao.inc
 * @descr:
 * 
 * @author	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 **/ 
 if (!defined("DISTRICT_DAO_INC")) {

	define("DISTRICT_DAO_INC",1);
	
	class DistrictDao extends DbConnect /*DbMySQLConnect*/  {
		
		function __construct() {
			parent::__construct();
		}      
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " tbl_district ";
		}

		/**
		 * @note:	function get keys name
		 * 
		 * @version 1.0
		 */
		public function getKeys() {
			return " district_id ";
		}
		
		public function getAllDistricts($provinceid = null, $status = null, $limit = null, $offset = null) {
			$params = array();
			$strSQL = " SELECT district.*, province.province_name ";
			$strSQL.= " FROM ".$this->getTableName()." AS district ";
			$strSQL.= " INNER JOIN tbl_province AS province ON province.province_id = district.province_id ";
			$strSQL.= " WHERE 1 = 1 ";
			if (strlen($provinceid)) {
				$strSQL.= " AND district.province_id = ? ";
				array_push($params, $provinceid);
			}
			if (strlen($status)) {
				$strSQL.= " AND district.status = ? AND province.status = ? ";
				array_push($params, $status, $status);
			}
			$strSQL.= " ORDER BY district.province_id ";
			if ($offset < 0) $offset = 0;
			if (($limit > 0) && ($offset >= 0)) {
				$strSQL.= " LIMIT ? OFFSET ?";
				array_push($params, $limit, $offset);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return is_array($result) ? $result : array();
		}

		public function getCountDistrict($provinceid = null, $status = null) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS district ";
			$strSQL.= " INNER JOIN tbl_province AS province ON province.province_id = district.province_id ";
			$strSQL.= " WHERE 1 = 1 ";
			if (strlen($provinceid)) {
				$strSQL.= " AND district.province_id = ? ";
				array_push($params, $provinceid);
			}
			if (strlen($status)) {
				$strSQL.= " AND district.status = ? AND province.status = ? ";
				array_push($params, $status, $status);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return intval($result[0]["count"]);
		}

		/**
		 * @note:	function get data of tabla tbl_District
		 * 
		 * @return	Array	: Data of table tbl_District
		 * @version 1.0
		 */
		public function getDistrictById($oid) {
			$params = array();
			$strSQL = " SELECT district.*, province.province_name ";
			$strSQL.= " FROM ".$this->getTableName()." AS district ";
			$strSQL.= " INNER JOIN tbl_province AS province ON province.province_id = district.province_id ";
			$strSQL.= " WHERE ".$this->getKeys()." = ? ";
			array_push($params, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}

		public function checkExistedDistrictName($districtname, $oid) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS district ";
			$strSQL.= " WHERE district.district_name = ? AND ".$this->getKeys()." <> ? ";
			array_push($params, $districtname, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return intval($result[0]["count"]);
		}
		
		/**
		 * @note:	function insert data for table tbl_district
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function insert_district($objects) {
			return $this->insert_db($this->getTableName(), $objects);
		}

		/**
		 * @note:	function update data for table tbl_district
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function update_district($objects, $oid) {
			return $this->update_db($this->getTableName(), $objects, $this->getKeys()." = ? ", array($oid));
		}

		/**
		 * @note:	function delete data for table tbl_district
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function delete_district($oid) {
			return $this->delete_db($this->getTableName(), $this->getKeys()." = ? ", array($oid));
		}
	}// end class		
 }
?>