<?php
/**
 * @project_name: localframe
 * @file_name: ContactDao.inc
 * @descr:
 * 
 * @author	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 **/ 
 if (!defined("CONTACT_DAO_INC")) {

	define("CONTACT_DAO_INC",1);
	
	class ContactDao extends DbConnect /*DbMySQLConnect*/  {
		
		function __construct() {
			parent::__construct();
		}      
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " tbl_contact ";
		}

		/**
		 * @note:	function get keys name
		 * 
		 * @version 1.0
		 */
		public function getKeys() {
			return " contact_id ";
		}
		
		/**
		 * @note:	function insert data for table tbl_contact
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function insert_contact($objects) {
			return $this->insert_db($this->getTableName(), $objects);
		}

		/**
		 * @note:	function update data for table tbl_contact
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function update_contact($objects, $oid) {
			return $this->update_db($this->getTableName(), $objects, $this->getKeys()." = ? ", array($oid));
		}

		/**
		 * @note:	function delete data for table tbl_contact
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function delete_contact($oid) {
			return $this->delete_db($this->getTableName(), $this->getKeys()." = ? ", array($oid));
		}
	}// end class		
 }
?>