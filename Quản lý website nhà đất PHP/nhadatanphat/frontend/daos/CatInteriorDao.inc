<?php
/**
 * @project_name: ntmusic
 * @file_name: CatInteriorDao.inc
 * @descr:
 * 
 * @author thangnc
 * @version 1.0	 2008/04/07, thangnc, created
 **/
function fn_cat_sort($a, $b) {
		    if (empty($a["category_order"]) && empty($b['category_order'])) {
			    return strnatcmp($a["category_name"], $b["category_name"]);
		    } else {
			    return strnatcmp($a["category_order"], $b["category_order"]);
		    }
	}

if (!defined("CATEGORIES_DAO_INC")) {
	define("CATEGORIES_DAO_INC", 1);

	class CatInteriorDao extends DbConnect {

		function __construct() {
			parent :: __construct();
		}

		public function getTable() {
			return "tbl_cat_interior";
		}

		/**
		* get_categories_by_category_id($category_id)
		* 
		* @param int - $category_id
		* @return array
		**/
		public function get_categories_by_id($category_id, $active = 1) {

			$strSQL = "SELECT * ";
			$strSQL .= "FROM " . $this->getTable() . " ";
			$strSQL .= "WHERE category_id = ?";
			
			$params = array ($category_id);
			if ($active != '') {
				$strSQL .= " AND category_active = ? ";
				array_push($params, $active);
			}
			
			$this->prepare($strSQL);
			$result = $this->execRun($params);

			return isset ($result[0]) ? $result[0] : null;
		}

		/**
		 * check_categories_exits($category_name, $category_id = '')
		 * 
		 * @author Nguyen Chien Thang
		 * @param String - $category_name
		 * @param int - $category_id = ''
		 * @return Booean - True if exits
		 * 					False if not exits
		 **/
		public function check_categories_exits($category_name, $category_id = '') {
			$strSQL = "SELECT category_id ";
			$strSQL .= "FROM " . $this->getTable() . " ";
			$strSQL .= "WHERE LOWER(category_name) = ? ";

			$params = array (strtolower($category_name));
			if ($category_id > 0) {
				$strSQL .= "AND category_id <> ?";
				array_push($params, $category_id);
			}
			$result = $this->exec($params);
			return (isset ($result[0])) ? true : false;
		}

		/**
		 * get_categories_count($category_id='0')
		 * 
		 * @author Nguyen Chien Thang
		 * @param int - $category_id
		 * @return int
		 **/
		public function get_categories_count($category_name = '') {

			$params = array ();

			$strSQL = "SELECT COUNT(*) AS cnt ";
			$strSQL .= "FROM " . $this->getTable() . " ";
			$strSQL .= "WHERE 1 = 1 ";

			if ($category_name != '') {

				$strSQL .= "AND LOWER(category_name) LIKE ? ";
				$category_name = '%' . strtolower($category_name) . '%';
				array_push($params, $category_name);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);

			return intval($result[0]['cnt']);
		}

		/**
		 * get_categories_list($category_id='0', $limit='', $page_id=1)
		 * 
		 * @author Nguyen Chien Thang
		 * @param int - $category_id
		 * @param int - $limit
		 * @param int - $page_id
		 * @return list
		 **/
		public function get_categories_list() {

			$params = array ();

			$strSQL = "SELECT * ";
			$strSQL .= "FROM " . $this->getTable() . " ";
			$strSQL .= "WHERE category_active = 1 ";
			$strSQL .= "ORDER BY category_order DESC ";

			$this->prepare($strSQL);
			$result = $this->exec($params);

			if (sizeof($result) <= 0)
				return null;

			$tmp = array ();
			foreach ($result as $k => $v) {
				$v['level'] = substr_count($v['id_path'], "/");
				$tmp[$v['level']][$v['category_id']] = $v;
			}

			ksort($tmp, SORT_NUMERIC);

			$tmp = array_reverse($tmp);

			foreach ($tmp as $level => $v) {
				foreach ($v as $k => $data) {
					if (isset ($tmp[$level +1][$data['parent_id']])) {
						$tmp[$level +1][$data['parent_id']]['subcategories'][] = $tmp[$level][$k];
						usort($tmp[$level +1][$data['parent_id']]['subcategories'], "fn_cat_sort");
						unset ($tmp[$level][$k]);
					}
				}
			}

			$tmp = array_pop($tmp);
			usort($tmp, "fn_cat_sort");
			/*
			$get_tree = true;            
			if ($get_tree == true) {
			    return $this->fn_plain_categories($tmp);
			} else {
			    return $tmp;
			} */
			return $this->fn_plain_categories($tmp);
			//return $result;			
		}

		/**
		 * insert_data($categories_obj)  
		 * 
		 * @author Nguyen Chien Thang
		 * @param array - $categories_obj, array('key1' => 'value1', 'key2' => 'value2',...)
		 * @return void
		**/
		public function insert_categories($categories_obj) {
			$this->insert_db($this->getTable(), $categories_obj);
		}

		/**
		 * update_categories($categories_obj, $category_id)  
		 * 
		 * @author Nguyen Chien Thang
		 * @param array - $categories_obj, array('key1' => 'value1', 'key2' => 'value2',...)
		 * @param int - $category_id
		 * @return void
		**/
		public function update_categories($categories_obj, $category_id) {
			$this->update_db($this->getTable(), $categories_obj, "category_id = $category_id");
		}

		/**
		 * delete_categories($category_id)
		 * @desc: update infomation categories_active = LOGIC_DEL
		 **/
		public function delete_categories($category_id) {
			$this->delete_db($this->getTable(), "category_id = ?", $category_id);
		}

		public function fn_plain_categories($categories, $result = array ()) {
			foreach ($categories as $k => $v) {
				if (!empty ($v['subcategories'])) {
					unset ($v['subcategories']);
					array_push($result, $v);
					$result = $this->fn_plain_categories($categories[$k]['subcategories'], $result);
				} else {
					array_push($result, $v);
				}
			}
			return $result;
		}
		
		public function get_categories_tree() {
		 	
		 	$strSQL = "SELECT * ";
			$strSQL.= "FROM ".$this->getTable()." ";
			$strSQL.= "WHERE category_active = ? ";
			$strSQL.= "ORDER BY category_order";
			$this->prepare($strSQL);						
			$result = $this->exec(array(1));
			
			if (empty($result)) {
				return array();
			}
			
			$tmp = array();
			foreach ($result as $k => $v) {
				$v['level'] = substr_count($v['id_path'], "/");
				$tmp[$v['level']][$v['category_id']] = $v;			
			}			
			
			ksort($tmp, SORT_NUMERIC);
			
			$tmp = array_reverse($tmp);
			
			foreach ($tmp as $level => $v) {
				foreach ($v as $k => $data) {
					if (isset($tmp[$level+1][$data['parent_id']])) {
						$tmp[$level+1][$data['parent_id']]['subcategories'][] = $tmp[$level][$k];
						usort($tmp[$level+1][$data['parent_id']]['subcategories'], "fn_cat_sort");
						unset($tmp[$level][$k]);
					}
				}
			}
			
			$tmp = array_pop($tmp);			
			usort($tmp, "fn_cat_sort");
            /*
            $get_tree = true;            
			if ($get_tree == true) {
		        return $this->fn_plain_categories($tmp);
	        } else {
		        return $tmp;
	        } */
            return $this->fn_plain_categories($tmp);
		 }
	} // end class 	
}
?>