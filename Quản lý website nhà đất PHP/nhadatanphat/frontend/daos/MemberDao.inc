<?php
/**
 * @project_name: localframe
 * @file_name: MemberDao.inc
 * @descr:
 * 
 * @author	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 **/ 
 if (!defined("MEMBER_DAO_INC")) {

	define("MEMBER_DAO_INC",1);
	
	class MemberDao extends DbConnect /*DbMySQLConnect*/  {
		
		function __construct() {
			parent::__construct();
		}      
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " tbl_member ";
		}

		/**
		 * @note:	function get keys name
		 * 
		 * @version 1.0
		 */
		public function getKeys() {
			return " member_id ";
		}

		/**
		 * @note:	function get data of tabla tbl_member
		 * 
		 * @return	Array	: Data of table tbl_member
		 * @version 1.0
		 */
		public function getMemberByUserAndPass($username, $passname) {
			$params = array();
			$strSQL = " SELECT member.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS member ";
			$strSQL.= " WHERE member.member_user = ? AND member.member_pass = ? AND status = 1 LIMIT 1 ";
			array_push($params, $username, $passname);

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}

		public function getMemberByUserAndMail($username, $mailname) {
			$params = array();
			$strSQL = " SELECT member.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS member ";
			$strSQL.= " WHERE member.member_user = ? AND member.member_mail = ? AND status = 1 LIMIT 1 ";
			array_push($params, $username, $mailname);

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}

		public function getMemberById($oid) {
			$params = array();
			$strSQL = " SELECT member.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS member ";
			$strSQL.= " WHERE ".$this->getKeys()." = ? ";
			array_push($params, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}

		public function checkExistedAccount($username) {
			$params = array();
			$strSQL = " SELECT member.* ";
			$strSQL.= " FROM ".$this->getTableName()." AS member ";
			$strSQL.= " WHERE member.member_user = ? AND status = 1 LIMIT 1 ";
			array_push($params, $username);

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}
		
		/**
		 * @note:	function insert data for table tbl_member
		 * @param	Object: objects
		 * @version 1.0
		 */
		public function insert_member($objects) {
			return $this->insert_db($this->getTableName(), $objects);
		}

		public function update_member($objects, $memberid) {
			return $this->update_db($this->getTableName(), $objects, $this->getKeys()." = ? ", array($memberid));
		}
		
	}// end class		
 }
?>