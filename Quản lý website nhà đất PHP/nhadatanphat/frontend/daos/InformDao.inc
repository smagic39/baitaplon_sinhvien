<?php
/**
 * @project_name: localframe
 * @file_name: InformDao.inc
 * @descr:
 * 
 * @author	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 **/ 
 if (!defined("INFORM_DAO_INC")) {

	define("INFORM_DAO_INC",1);
	
	class InformDao extends DbConnect /*DbMySQLConnect*/  {
		
		function __construct() {
			parent::__construct();
		}      
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " tbl_inform ";
		}

		/**
		 * @note:	function get keys name
		 * 
		 * @version 1.0
		 */
		public function getKeys() {
			return " inform_id ";
		}
		
		public function getAllInforms ($categoryid = null, $status = null, $informid = null, $limit = null, $offset = null){
			
			$params = array();
			$strSQL = " SELECT inform.* "; /*, category.category_name ";*/
			$strSQL.= " FROM ".$this->getTableName()." AS inform ";
			//$strSQL.= " INNER JOIN tbl_category AS category ON category.category_id = inform.category_id ";
			$strSQL.= " WHERE inform.category_id = 0 ";
		/*
			if (strlen($categoryid)) {
				$strSQL.= " AND inform.category_id = ? ";
				array_push($params, $categoryid);
			}
		*/
			if (strlen($status)) {
				$strSQL.= " AND inform.status = ? ";
				array_push($params, $status);
			}
			if (strlen($informid)) {
				$strSQL.= " AND inform.inform_id <> ? ";
				array_push($params, $informid);
			}
			$strSQL.= " ORDER BY isShowInHomePage DESC, created_date DESC ";

			if ($offset < 0)
				$offset = 0;

			if (($limit > 0) && ($offset >= 0)) {
				$strSQL.= " LIMIT ? OFFSET ?";
				array_push($params, $limit, $offset);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);

		 	return is_array($result) ? $result : array();
		}

		public function getCountInform($categoryid = null, $status = null, $informid = null) {
			$params = array();
			$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
			$strSQL.= " FROM ".$this->getTableName()." AS inform ";
			//$strSQL.= " INNER JOIN tbl_category AS category ON category.category_id = inform.category_id ";
			$strSQL.= " WHERE inform.category_id = 0 ";
		/*
			if (strlen($categoryid)) {
				$strSQL.= " AND inform.category_id = ? ";
				array_push($params, $categoryid);
			}
		*/
			if (strlen($status)) {
				$strSQL.= " AND inform.status = ? ";
				array_push($params, $status);
			}
			if (strlen($informid)) {
				$strSQL.= " AND inform.inform_id <> ? ";
				array_push($params, $informid);
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return intval($result[0]["count"]);
		}

		/**
		 * @note:	function get data of tabla tbl_inform
		 * 
		 * @return	Array	: Data of table tbl_inform
		 * @version 1.0
		 */
		public function getInformById($oid) {
			$params = array();
			$strSQL = " SELECT inform.*";//", category.category_name ";
			$strSQL.= " FROM ".$this->getTableName()." AS inform ";
			//$strSQL.= " INNER JOIN tbl_category AS category ON category.category_id = inform.category_id ";
			$strSQL.= " WHERE ".$this->getKeys()." = ? ";
			array_push($params, $oid);

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	
		 	return isset($result[0]) ? $result[0] : array();
		}
	}// end class		
 }
?>