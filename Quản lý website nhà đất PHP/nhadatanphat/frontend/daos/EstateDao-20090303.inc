<?php
/**
 * @project_name: localframe
 * @file_name: EstateDao.inc
 * @descr:
 * 
 * @author	Nguyen Ngoc - thunn84@gmail.com
 * @version 1.0
 **/ 
 if (!defined("ESTATE_DAO_INC")) {

	define("ESTATE_DAO_INC",1);

	class EstateDao extends DbConnect /*DbMySQLConnect*/  {

		function __construct() {
			parent::__construct();
		}
			
		/**
		 * @note:	function get table name
		 * 
		 * @version 1.0
		 */
		public function getTableName() {
			return " tbl_estate ";
		}

		/**
		 * @note:	function get keys name
		 * 
		 * @version 1.0
		 */
		public function getKeys() {
			return " estate_id ";
		}
		
		public function getEstateTitle(){
			$strSQL = " SELECT estate_title, estate_id FROM tbl_estate";
			$this->prepare($strSQL);
			$result = $this->exec(array());
		 	return is_array($result) ? $result : array();
		}
		
		public function getEstateGroupProvince($proviceId="", $limit=0){
			$params	= array();
			$strSQL = " SELECT DISTINCT province_name, estate.province_id, COUNT(estate_id) AS count  ";
			$strSQL.= " FROM ".$this->getTableName()." AS estate ";
			$strSQL.= " INNER JOIN tbl_member AS member ON member.member_id = estate.member_id ";
			$strSQL.= " LEFT JOIN tbl_category AS category ON category.category_id = estate.category_id ";
			$strSQL.= " LEFT JOIN tbl_child AS child ON child.child_id = estate.child_id ";
			$strSQL.= " LEFT JOIN tbl_province AS province ON province.province_id = estate.province_id ";
			$strSQL.= " LEFT JOIN tbl_district AS district ON district.district_id = estate.district_id ";
			//$strSQL.= " AND district.status = 1 ";
			$strSQL.= " WHERE estate.estate_duration > now() ";
			$strSQL.= "	AND estate.status = 1 AND province.status = 1 ";
			if($proviceId != ""){
				$strSQL .= " AND estate.province_id <> ? ";
				$params[] = $proviceId;
			}
	
			//$strSQL.= " AND estate.estate_genre = 1 ";
			$strSQL.= " AND (estate.deleted_date > now() OR estate.deleted_date IS NULL)";
			$strSQL.= " AND estate.is_deleted_by_member = 0";
			
			$strSQL.= " GROUP BY province_name, estate.province_id ";
			$strSQL.= " ORDER BY count DESC ";
			
			if($limit > 0){
				$strSQL.= " LIMIT ? OFFSET 0";
				$params[] = $limit;
			}

			$this->prepare($strSQL);
			$result = $this->exec($params);
		 	return is_array($result) ? $result : array();
		}

	public function getEstateGroupCategory($provinceid = null) {
		$params	= array();
		$strSQL = " SELECT DISTINCT category_name, estate.category_id, COUNT(estate_id) AS count ";
		$strSQL.= " FROM ".$this->getTableName()." AS estate ";
		$strSQL.= " INNER JOIN tbl_member AS member ON member.member_id = estate.member_id ";
		$strSQL.= " LEFT JOIN tbl_category AS category ON category.category_id = estate.category_id ";
		$strSQL.= " LEFT JOIN tbl_child AS child ON child.child_id = estate.child_id ";
		$strSQL.= " LEFT JOIN tbl_province AS province ON province.province_id = estate.province_id ";
		$strSQL.= " LEFT JOIN tbl_district AS district ON district.district_id = estate.district_id AND district.status = 1 ";
		$strSQL.= " WHERE estate.estate_duration > now() ";
		$strSQL.= "	AND estate.status = 1 AND province.status = 1 ";
		if (strlen($provinceid)){
			$strSQL.= " AND estate.province_id = ? ";
			array_push($params, $provinceid);
		}

		//$strSQL.= " AND estate.estate_genre = 1 ";
		$strSQL.= " AND (estate.deleted_date > now() OR estate.deleted_date IS NULL)";
		$strSQL.= " AND estate.is_deleted_by_member = 0";

		$strSQL.= " GROUP BY category_name, estate.category_id ";
		$strSQL.= " ORDER BY category_name ASC ";
		$this->prepare($strSQL);

		$result = $this->exec($params);
	 	return is_array($result) ? $result : array();
	}
	
	public function getAllEstateGroupCategory() {
		$params	= array();
		$strSQL = " SELECT category_id, category_name ";
		$strSQL.= " FROM tbl_category ";
		$strSQL.= " WHERE status=1 ";
		$strSQL.= " ORDER BY category_name ASC ";
		$this->prepare($strSQL);

		$result = $this->exec($params);
	 	return is_array($result) ? $result : array();
	}
	
	public function getAllEstateByParams($paraminput, $status = null, $limit = null, $offset = null, $usd_vnd=null, $sjc_vnd=null) {
		$params = array();
		if ($usd_vnd==null)
			$usd_vnd = 16000;
		if ($sjc_vnd==null)
			$sjc_vnd = 1700000;

		$strSQL = " SELECT * FROM (SELECT estate.*,estate.created_date as estate_created_date,  member.member_user,member.member_name,member.member_mail,
						member.member_address,member.member_phone,member.member_website,
						 category_name, child_name, province_name, district_name, 
						CASE WHEN estate.estate_price_type >0 THEN 
						 CASE WHEN estate.estate_price_type =1
							  THEN estate.estate_price * $usd_vnd
							  ELSE estate.estate_price * $sjc_vnd
					     END
                     ELSE estate.estate_price
                     END AS e_price, ";
		$strSQL.= "	CASE WHEN estate.estate_genre=1 THEN
						 '1'
					WHEN estate.estate_genre=2 THEN '1'
					WHEN estate.is_fee_estate=1 THEN '1'
					ELSE '0'
				    END AS new_news_order, ";
		$strSQL.= "	CASE WHEN estate.estate_genre=1 	THEN '1'
						 WHEN estate.estate_genre=2		THEN '1'
					ELSE '0'
				    END AS vip_group_order ";
		$strSQL.= " FROM ".$this->getTableName()." AS estate ";
		$strSQL.= " LEFT JOIN tbl_member AS member ON member.member_id = estate.member_id ";
		$strSQL.= " LEFT JOIN tbl_category AS category ON category.category_id = estate.category_id ";
		$strSQL.= " LEFT JOIN tbl_child AS child ON child.child_id = estate.child_id ";
		$strSQL.= " LEFT JOIN tbl_province AS province ON province.province_id = estate.province_id ";
		$strSQL.= " LEFT JOIN tbl_district AS district ON district.district_id = estate.district_id ";

		if ( strtolower(substr($paraminput['keyword'], 0, 3)) == "vm-"){
			$paraminput['keyword'] =  substr($paraminput['keyword'], 3, strlen($paraminput['keyword']) - 3);
			$strSQL.= " WHERE estate.estate_id = ? ";
			array_push($params, $paraminput['keyword']);
		} else {
			$strSQL.= " WHERE (estate.estate_title_ascii LIKE ? OR estate.estate_id LIKE ?) ";
			array_push($params, '%' . $paraminput["keyword"] . '%', '%'. $paraminput["keyword"] .'%');
		}

		$strSQL.= " AND estate.status = 1";
		
		if(isset($paraminput["feeEstateOnly"]) && strlen($paraminput["feeEstateOnly"])) {
			$strSQL.= " AND estate.is_fee_estate = 1 ";
		}
		
		if (isset($paraminput["categoryid"]) && strlen($paraminput["categoryid"])){
			if($paraminput["categoryid"] == "-1"){
				$strSQL.= " AND estate.category_id = -1 ";
			}else{
				$strSQL.= " AND category.category_id = ? ";
				array_push($params, $paraminput["categoryid"]);
			}
		}else{
			$strSQL.= " AND estate.category_id <> -1 ";
		}

		if (isset($paraminput["childid"]) && strlen($paraminput["childid"])) {
			$strSQL.= " AND estate.child_id = ? ";
			array_push($params, $paraminput["childid"]);
		}

		if (isset($paraminput["provinceid"]) && strlen($paraminput["provinceid"])) {
			$strSQL.= " AND estate.province_id = ? ";
			array_push($params, $paraminput["provinceid"]);
		}
		
		if (isset($paraminput["districtid"]) && strlen($paraminput["districtid"])) {
			$strSQL.= " AND estate.district_id = ? ";
			array_push($params, $paraminput["districtid"]);
		}
		if (isset($paraminput["direction"]) && strlen($paraminput["direction"])) {
			$strSQL.= " AND estate.estate_direction = ? ";
			array_push($params, $paraminput["direction"]);
		}
		
		if (isset($paraminput["genre"]) && strlen($paraminput["genre"])) {
			$strSQL.= " AND estate.estate_genre = ? ";
			array_push($params, $paraminput["genre"]);
		}

		if (isset($paraminput["genreSearch"]) && strlen($paraminput["genreSearch"])) {
			$strSQL.= " AND (estate.estate_genre=1 OR estate.estate_genre=2)";
		}

		if (isset($paraminput["isFreeNewsOnly"]) && strlen($paraminput["isFreeNewsOnly"])) {
			$strSQL.= " AND estate.estate_genre=0 AND estate.is_fee_estate=0 ";
		}

		/*if (isset($paraminput["state"]) && (trim($paraminput["state"]) !=0)) {
			$strSQL.= " AND estate.estate_state = ? ";
			array_push($params, $paraminput["state"]);
		}*/

		if (isset($paraminput["state"]) && (trim($paraminput["state"])==0)) {
			$strSQL.= " AND estate.estate_id in (select estate_id FROM tbl_members_news WHERE member_id=?) ";
			array_push($params, $_SESSION["_LOGIN_ESTATE_"]["member_id"]);
		}

		//if (!isset($paraminput["delete"])) 
		//	$strSQL.= " AND estate.deleted_date IS NULL ";
		//else 
		//	$strSQL.= " AND estate.deleted_date > now() ";
		
		if (isset($paraminput["duration"]))
			$strSQL.= " AND estate.estate_duration < now() ";
		else 
			$strSQL.= " AND estate.estate_duration >= now() ";
		
		if (isset($paraminput["member"]) && strlen($paraminput["member"])) {
			$strSQL.= " AND estate.member_id = ? ";
			array_push($params, $paraminput["member"]);
		}
		if(isset($paraminput["oid"]) && strlen($paraminput["oid"])){
			$strSQL.= " AND estate.estate_id <> ? ";
			array_push($params, $paraminput["oid"]);
		}
		
		$strSQL.= " AND estate.is_deleted_by_member = 0 ";
		
		$strSQL.= " AND (estate.deleted_date > now() OR estate.deleted_date IS NULL)";
		$strSQL.= ") AS rst";
		$strSQL.= " WHERE 1=1 ";
		
		if(isset($paraminput["currentNewsDate"])){
			$strSQL .= " AND rst.estate_created_date <= ? ";
			array_push($params, $paraminput["currentNewsDate"]);
		}
		
		if(($paraminput["pricestart"] == -1) && ($paraminput["priceend"] == -1)){
			$strSQL.= " AND rst.is_price_negotiable=1 ";
		}else{
			if (isset($paraminput["pricestart"]) && strlen($paraminput["pricestart"])) {
				$strSQL.= " AND rst.e_price >= ? ";
				array_push($params, $paraminput["pricestart"]);
			}
			if (isset($paraminput["priceend"]) && strlen($paraminput["priceend"])) {
				$strSQL.= " AND rst.e_price <= ? ";
				array_push($params, $paraminput["priceend"]);
			}
		}
		if(!isset($paraminput["currentNewsDate"])){
			if($paraminput["isNewNews"]){
				$strSQL.= " ORDER BY  rst.new_news_order DESC, rst.estate_created_date DESC ";
			}elseif($paraminput["isVipGroup"]){
				$strSQL.= " ORDER BY  rst.vip_group_order DESC, rst.estate_created_date DESC ";
			}else{
				$strSQL.= " ORDER BY rst.estate_genre DESC, rst.is_fee_estate DESC, rst.estate_created_date DESC ";
			}
		}else{
			$strSQL.= " ORDER BY rst.estate_created_date DESC ";
		}

		if ($offset < 0)
			$offset = 0;

		if (($limit > 0) && ($offset >= 0)){
			$strSQL.= " limit ? offset ? ";
			array_push($params, $limit);
			array_push($params, (int)$offset);
		}
		
		$this->prepare($strSQL);
		$result = $this->exec($params);

	 	return is_array($result) ? $result : array();
	}
	
	public function getAllEstateMemberAreaByParams($paraminput, $status = null, $limit = null, $offset = null, $usd_vnd=null, $sjc_vnd=null) {
		$params = array();
		if ($usd_vnd==null)
			$usd_vnd = 16000;
		if ($sjc_vnd==null)
			$sjc_vnd = 1700000;

		$strSQL = " SELECT * FROM (SELECT estate.*, member.member_user,member.member_name,member.member_mail,
						member.member_address,member.member_phone,member.member_website,
						 category_name, child_name, province_name, district_name, 
						CASE WHEN estate.estate_price_type >0 THEN 
						 CASE WHEN estate.estate_price_type =1
							  THEN estate.estate_price * $usd_vnd
							  ELSE estate.estate_price * $sjc_vnd
					     END
                     ELSE estate.estate_price
                     END AS e_price" ;
		$strSQL.= " FROM ".$this->getTableName()." AS estate ";
		$strSQL.= " LEFT JOIN tbl_member AS member ON member.member_id = estate.member_id ";
		$strSQL.= " LEFT JOIN tbl_category AS category ON category.category_id = estate.category_id ";
		$strSQL.= " LEFT JOIN tbl_child AS child ON child.child_id = estate.child_id ";
		$strSQL.= " LEFT JOIN tbl_province AS province ON province.province_id = estate.province_id ";
		$strSQL.= " LEFT JOIN tbl_district AS district ON district.district_id = estate.district_id ";

		if ( strtolower(substr($paraminput['keyword'], 0, 3)) == "vm-"){
			$paraminput['keyword'] =  substr($paraminput['keyword'], 3, strlen($paraminput['keyword']) - 3);
			$strSQL.= " WHERE estate.estate_id = ? ";
			array_push($params, $paraminput['keyword']);
		} else {
			$strSQL.= " WHERE (estate.estate_title_ascii LIKE ? OR estate.estate_id LIKE ?) ";
			array_push($params, '%'. $paraminput["keyword"].'%', '%'. $paraminput["keyword"] . '%');
		}
		
		if(!isset($paraminput["discardStatus"])){
			if($status !== null){
				$strSQL.= " AND estate.status = ?";
				array_push($params, $status);
			}else{
				$strSQL.= " AND estate.status = 1";
			}
		}
		
		if(isset($paraminput["feeEstateOnly"]) && strlen($paraminput["feeEstateOnly"])) {
			$strSQL.= " AND estate.is_fee_estate = 1 ";
		}
		
		if (isset($paraminput["categoryid"]) && strlen($paraminput["categoryid"]) > 0){
			if($paraminput["categoryid"] == "-1"){
				$strSQL .= " AND estate.category_id = -1 ";
			}else{
				$strSQL .= " AND category.category_id = ? ";
				array_push($params, $paraminput["categoryid"]);
			}
		}elseif($paraminput["exceptCategoryId"] == ""){
			$strSQL.= " AND estate.category_id <> -1 ";
		}

		if (isset($paraminput["childid"]) && strlen($paraminput["childid"])) {
			$strSQL.= " AND estate.child_id = ? ";
			array_push($params, $paraminput["childid"]);
		}

		if (isset($paraminput["provinceid"]) && strlen($paraminput["provinceid"])) {
			$strSQL.= " AND estate.province_id = ? ";
			array_push($params, $paraminput["provinceid"]);
		}
		
		if (isset($paraminput["districtid"]) && strlen($paraminput["districtid"])) {
			$strSQL.= " AND estate.district_id = ? ";
			array_push($params, $paraminput["districtid"]);
		}
		if (isset($paraminput["direction"]) && strlen($paraminput["direction"])) {
			$strSQL.= " AND estate.estate_direction = ? ";
			array_push($params, $paraminput["direction"]);
		}
		
		if (isset($paraminput["genre"]) && strlen($paraminput["genre"])) {
			$strSQL.= " AND estate.estate_genre = ? ";
			array_push($params, $paraminput["genre"]);
		}

		if (isset($paraminput["state"]) && strlen(trim($paraminput["state"]))) {
			$strSQL.= " AND estate.estate_state = ? ";
			array_push($params, $paraminput["state"]);
		}

		if (isset($paraminput["state"]) && (trim($paraminput["state"])==0)) {
			$strSQL.= " AND estate.estate_id in (select estate_id FROM tbl_members_news WHERE member_id=?) ";
			array_push($params, $_SESSION["_LOGIN_ESTATE_"]["member_id"]);
		}

		if(!isset($paraminput["discardDeleteDate"])){
			if (!isset($paraminput["delete"])){ 
				$strSQL.= " AND estate.deleted_date IS NULL ";
			}else{
				$strSQL.= " AND estate.deleted_date > now() ";
			}
		}
		
		if(isset($paraminput["isDeletedByMember"])){
			$strSQL.= " AND estate.is_deleted_by_member = ? ";
			array_push($params, $paraminput["isDeletedByMember"]);
		}
		
		if(!isset($paraminput["isDiscardDuration"])){
			if (isset($paraminput["duration"]))
				$strSQL .= " AND estate.estate_duration < now() ";
			else 
				$strSQL.= " AND estate.estate_duration >= now() ";
		}

		if (isset($paraminput["member"]) && strlen($paraminput["member"])) {
			$strSQL.= " AND estate.member_id = ? ";
			array_push($params, $paraminput["member"]);
		}
		if(isset($paraminput["oid"]) && strlen($paraminput["oid"])){
			$strSQL.= " AND estate.estate_id <> ? ";
			array_push($params, $paraminput["oid"]);
		}

		$strSQL.= " AND (estate.deleted_date > now() OR estate.deleted_date IS NULL)";
		$strSQL.= ") AS rst";
		$strSQL.= " WHERE 1=1 ";
		
		if(($paraminput["pricestart"] == -1) && ($paraminput["priceend"] == -1)){
			$strSQL.= " AND rst.is_price_negotiable=1 ";
		}else{
			if (isset($paraminput["pricestart"]) && strlen($paraminput["pricestart"])) {
				$strSQL.= " AND rst.e_price >= ? ";
				array_push($params, $paraminput["pricestart"]);
			}
			if (isset($paraminput["priceend"]) && strlen($paraminput["priceend"])) {
				$strSQL.= " AND rst.e_price <= ? ";
				array_push($params, $paraminput["priceend"]);
			}
		}

		$strSQL.= " ORDER BY rst.created_date DESC, rst.estate_genre DESC, rst.is_fee_estate DESC ";

		if ($offset < 0) $offset = 0;
		if (($limit > 0) && ($offset >= 0)) {
			$strSQL.= " LIMIT ? OFFSET ?";
			array_push($params, $limit, $offset);
		}

		$this->prepare($strSQL);
		$result = $this->exec($params);

	 	return is_array($result) ? $result : array();
	}
	
	public function getCountEstateByParams($paraminput, $status = null) {
		$params = array();
		$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
		$strSQL .= " FROM ".$this->getTableName()." AS estate ";
		$strSQL .= " LEFT JOIN tbl_member AS member ON member.member_id = estate.member_id ";
		$strSQL .= " LEFT JOIN tbl_category AS category ON category.category_id = estate.category_id ";
		$strSQL .= " LEFT JOIN tbl_child AS child ON child.child_id = estate.child_id ";
		$strSQL .= " LEFT JOIN tbl_province AS province ON province.province_id = estate.province_id ";
		$strSQL .= " LEFT JOIN tbl_district AS district ON district.district_id = estate.district_id ";

		if (strlen($status))
			$strSQL .= " AND district.status = 1 ";

		if ( strtolower(substr($paraminput['keyword'], 0, 3)) == "vm-"){
			$paraminput['keyword'] =  substr($paraminput['keyword'], 3, strlen($paraminput['keyword']) - 3);
			$strSQL .= " WHERE estate.estate_id = ? ";
			array_push($params, $paraminput['keyword']);
		} else {
			$strSQL .= " WHERE (estate.estate_title_ascii LIKE ? OR estate.estate_id LIKE ?) ";
//			$strSQL.= " WHERE (CONVERT(LOWER(CONVERT(estate.estate_title USING ucs2)) USING utf8) LIKE ? OR LOWER(estate.estate_id) LIKE ?) ";
//			array_push($params, '%'.strtolower($paraminput["keyword"]).'%', '%'.strtolower($paraminput["keyword"]).'%');
			array_push($params, '%'. $paraminput["keyword"] .'%', '%' . $paraminput["keyword"] .'%');
		}
		
		if(isset($paraminput["currentNewsDate"])){
			$strSQL .= " AND estate.created_date <= ? ";
			array_push($params, $paraminput["currentNewsDate"]);
		}
		
		if (strlen($status)) {
			$strSQL .= " AND estate.status = ?";
			array_push($params, $status);
		}
		
		if(isset($paraminput["feeEstateOnly"]) && strlen($paraminput["feeEstateOnly"])) {
			$strSQL .= " AND estate.is_fee_estate = 1 ";
		}
		
		if (isset($paraminput["isFreeNewsOnly"]) && strlen($paraminput["isFreeNewsOnly"])) {
			$strSQL.= " AND estate.estate_genre=0 AND estate.is_fee_estate=0 ";
		}
		
		if (isset($paraminput["categoryid"]) && strlen($paraminput["categoryid"])){
			if($paraminput["categoryid"] == "-1"){
				$strSQL .= " AND estate.category_id = -1 ";
			}else{
				$strSQL .= " AND category.category_id = ? ";
				array_push($params, $paraminput["categoryid"]);
			}
		}else{
			$strSQL.= " AND estate.category_id <> -1 ";
		}

		if (isset($paraminput["childid"]) && strlen($paraminput["childid"])) {
			$strSQL .= " AND estate.child_id = ? ";
			array_push($params, $paraminput["childid"]);
		}

		if (isset($paraminput["provinceid"]) && strlen($paraminput["provinceid"])) {
			$strSQL .= " AND estate.province_id = ? ";
			array_push($params, $paraminput["provinceid"]);
		}
		
		if (isset($paraminput["districtid"]) && strlen($paraminput["districtid"])) {
			$strSQL .= " AND estate.district_id = ? ";
			array_push($params, $paraminput["districtid"]);
		}
		if (isset($paraminput["direction"]) && strlen($paraminput["direction"])) {
			$strSQL .= " AND estate.estate_direction = ? ";
			array_push($params, $paraminput["direction"]);
		}
		
		if (isset($paraminput["genre"]) && strlen($paraminput["genre"])) {
			$strSQL .= " AND estate.estate_genre = ? ";
			array_push($params, $paraminput["genre"]);
		}

		/*if (isset($paraminput["state"]) && (trim($paraminput["state"]) !=0)) {
			$strSQL.= " AND estate.estate_state = ? ";
			array_push($params, $paraminput["state"]);
		}*/

		if (isset($paraminput["state"]) && (trim($paraminput["state"])==0)) {
			$strSQL .= " AND estate.estate_id in (select estate_id FROM tbl_members_news WHERE member_id=?) ";
			array_push($params, $_SESSION["_LOGIN_ESTATE_"]["member_id"]);
		}

		//if (!isset($paraminput["delete"])) 
		//	$strSQL.= " AND estate.deleted_date IS NULL ";
		//else 
		//	$strSQL.= " AND estate.deleted_date > now() ";
		
		if (isset($paraminput["duration"]))
			$strSQL.= " AND estate.estate_duration < now() ";
		else 
			$strSQL.= " AND estate.estate_duration >= now() ";

		if (isset($paraminput["member"]) && strlen($paraminput["member"])) {
			$strSQL.= " AND estate.member_id = ? ";
			array_push($params, $paraminput["member"]);
		}
		if(isset($paraminput["oid"]) && strlen($paraminput["oid"])){
			$strSQL.= " AND estate.estate_id <> ? ";
			array_push($params, $paraminput["oid"]);
		}
		
		$strSQL.= " AND estate.is_deleted_by_member = 0 ";

		$this->prepare($strSQL);
		$result = $this->exec($params);

	 	return intval($result[0]["count"]);
	}
	
	public function getCountEstateMemberAreaByParams($paraminput, $status = null) {
		$params = array();
		$strSQL = " SELECT COUNT(".$this->getKeys().") AS count ";
		$strSQL .= " FROM ".$this->getTableName()." AS estate ";
		$strSQL .= " LEFT JOIN tbl_member AS member ON member.member_id = estate.member_id ";
		$strSQL .= " LEFT JOIN tbl_category AS category ON category.category_id = estate.category_id ";
		$strSQL .= " LEFT JOIN tbl_child AS child ON child.child_id = estate.child_id ";
		$strSQL .= " LEFT JOIN tbl_province AS province ON province.province_id = estate.province_id ";
		$strSQL .= " LEFT JOIN tbl_district AS district ON district.district_id = estate.district_id ";

		if (strlen($status))
			$strSQL .= " AND district.status = 1 ";

		if ( strtolower(substr($paraminput['keyword'], 0, 3)) == "vm-"){
			$paraminput['keyword'] =  substr($paraminput['keyword'], 3, strlen($paraminput['keyword']) - 3);
			$strSQL .= " WHERE estate.estate_id = ? ";
			array_push($params, $paraminput['keyword']);
		} else {
			$strSQL .= " WHERE (estate.estate_title_ascii LIKE ? OR estate.estate_id LIKE ?) ";
			array_push($params, '%'.$paraminput["keyword"] .'%', '%'.$paraminput["keyword"].'%');
		}

	/*
		if (strlen($status)){
			$strSQL .= " AND estate.status = ?";
			array_push($params, $status);
		}
	*/
		if(!isset($paraminput["discardStatus"])){
			if($status !== null){
				$strSQL.= " AND estate.status = ?";
				array_push($params, $status);
			}else{
				$strSQL.= " AND estate.status = 1";
			}
		}
		
		if(isset($paraminput["feeEstateOnly"]) && strlen($paraminput["feeEstateOnly"])) {
			$strSQL .= " AND estate.is_fee_estate = 1 ";
		}

		if (isset($paraminput["categoryid"])){
			if($paraminput["categoryid"] == "-1"){
				$strSQL .= " AND estate.category_id = -1 ";
			}else{
				$strSQL .= " AND category.category_id = ? ";
				array_push($params, $paraminput["categoryid"]);
			}
		}elseif($paraminput["exceptCategoryId"] == ""){
			$strSQL.= " AND estate.category_id <> -1 ";
		}

		if (isset($paraminput["childid"]) && strlen($paraminput["childid"])) {
			$strSQL .= " AND estate.child_id = ? ";
			array_push($params, $paraminput["childid"]);
		}

		if (isset($paraminput["provinceid"]) && strlen($paraminput["provinceid"])) {
			$strSQL .= " AND estate.province_id = ? ";
			array_push($params, $paraminput["provinceid"]);
		}
		
		if (isset($paraminput["districtid"]) && strlen($paraminput["districtid"])) {
			$strSQL .= " AND estate.district_id = ? ";
			array_push($params, $paraminput["districtid"]);
		}
		if (isset($paraminput["direction"]) && strlen($paraminput["direction"])) {
			$strSQL .= " AND estate.estate_direction = ? ";
			array_push($params, $paraminput["direction"]);
		}
		
		if (isset($paraminput["genre"]) && strlen($paraminput["genre"])) {
			$strSQL .= " AND estate.estate_genre = ? ";
			array_push($params, $paraminput["genre"]);
		}

		if (isset($paraminput["state"]) && strlen($paraminput["state"]) > 0){
			$strSQL.= " AND estate.estate_state=? ";
			array_push($params, $paraminput["state"]);
		}

		if (isset($paraminput["state"]) && (trim($paraminput["state"])==0)) {
			$strSQL .= " AND estate.estate_id in (select estate_id FROM tbl_members_news WHERE member_id=?) ";
			array_push($params, $_SESSION["_LOGIN_ESTATE_"]["member_id"]);
		}

		if(!isset($paraminput["discardDeleteDate"])){
			if (!isset($paraminput["delete"])){ 
				$strSQL.= " AND estate.deleted_date IS NULL ";
			}else{
				$strSQL.= " AND estate.deleted_date > now() ";
			}
		}
		
		if(isset($paraminput["isDeletedByMember"])){
			$strSQL .= " AND estate.is_deleted_by_member = ? ";
			array_push($params, $paraminput["isDeletedByMember"]);
		}

		if(!isset($paraminput["isDiscardDuration"])){
			if (isset($paraminput["duration"]))
				$strSQL .= " AND estate.estate_duration < now() ";
			else 
				$strSQL.= " AND estate.estate_duration >= now() ";
		}

		if (isset($paraminput["member"]) && strlen($paraminput["member"])) {
			$strSQL.= " AND estate.member_id = ? ";
			array_push($params, $paraminput["member"]);
		}
		if(isset($paraminput["oid"]) && strlen($paraminput["oid"])){
			$strSQL.= " AND estate.estate_id <> ? ";
			array_push($params, $paraminput["oid"]);
		}

		$this->prepare($strSQL);
		$result = $this->exec($params);
	 	return intval($result[0]["count"]);
	}

	/**
	 * @note:	function get data of tabla tbl_estate
	 * 
	 * @return	Array	: Data of table tbl_estate
	 * @version 1.0
	 */
	public function getEstateById($oid) {
		$params = array();
		$strSQL = " SELECT estate.*, DATE_FORMAT(estate_duration, '%d-%m-%Y') as duration, estate.created_date as estate_last_updated, member.*, category_name, child_name, province_name, district_name ";
		$strSQL.= " FROM ".$this->getTableName()." AS estate ";
		$strSQL.= " INNER JOIN tbl_member AS member ON member.member_id = estate.member_id ";
		$strSQL.= " INNER JOIN tbl_category AS category ON category.category_id = estate.category_id ";
		$strSQL.= " INNER JOIN tbl_child AS child ON child.child_id = estate.child_id ";
		$strSQL.= " INNER JOIN tbl_province AS province ON province.province_id = estate.province_id ";
		$strSQL.= " LEFT JOIN tbl_district AS district ON district.district_id = estate.district_id ";
		$strSQL.= " WHERE ".$this->getKeys()." = ? ";
		array_push($params, $oid);

		$this->prepare($strSQL);
		$result = $this->exec($params);
	 	
	 	return isset($result[0]) ? $result[0] : array();
	}

	/**
	 * @note:	function insert data for table tbl_estate
	 * @param	Object: objects
	 * @version 1.0
	 */
	public function insert_estate($objects) {
		return $this->insert_db($this->getTableName(), $objects);
	}

	/**
	 * @note:	function update data for table tbl_estate
	 * @param	Object: objects
	 * @version 1.0
	 */
	public function update_estate($objects, $oid) {
		return $this->update_db($this->getTableName(), $objects, $this->getKeys()." = ? ", array($oid));
	}

	/**
	 * @note:	function restore data for table tbl_estate
	 * @param	Object: objects
	 * @version 1.0
	 */
	public function restore_estate($oid, $memberId) {
		return $this->update_db($this->getTableName(), array("is_deleted_by_member" =>0), $this->getKeys()." = ? AND member_id =?", array($oid, $memberId));
	}

	/**
	 * @note:	function delete data for table tbl_estate
	 * @param	Object: objects
	 * @version 1.0
	 */
	public function delete_estate($oid, $memberId) {
		return $this->update_db($this->getTableName(), array("is_deleted_by_member"=>1), $this->getKeys()." = ? AND member_id =?", array($oid, $memberId));
	}
	/**
	 * @note:	function insert data for table tbl_member
	 * @param	Object: objects
	 * @version 1.0
	 */
	public function save_news($objects) {
		if (!$this->check_history($objects['estate_id']))
			return $this->insert_db("tbl_members_news", $objects);
	}
	public function check_history($oid) {
		if (!isset($_SESSION["_LOGIN_ESTATE_"]["member_id"]))
			return 0;
		$params = array();
		$strSQL = " SELECT count(id) as cnt";
		$strSQL.= " FROM tbl_members_news ";
		$strSQL.= " WHERE member_id=? AND estate_id=?";
		array_push($params, $_SESSION["_LOGIN_ESTATE_"]["member_id"], $oid);

		$this->prepare($strSQL);
		$result = $this->exec($params);
	 	return $result[0]['cnt'];
	}
	
	public function updateViewCounter($newCounter, $oid){
		return $this->update_db($this->getTableName(), array("viewed" =>$newCounter), $this->getKeys()." = ? ", array($oid));
	}
	
	public function delete_physical($oid, $memberId) {
		return $this->delete_db($this->getTableName(), $this->getKeys()." = ? AND member_id=? ", array($oid, $memberId));
	}
	
	/**
	 * @note:	function delete data for table tbl_estate
	 * @param	Object: objects
	 * @version 1.0
	 */
	public function delete_save_news($oid) {
		return $this->delete_db("tbl_members_news", "id = ? ", array($oid));
	}			
	}// end class		
 }
?>