
/**
 * ジャンル選択フォーム値読込み
 */
var genre_options = new Array();
function ReadGenreOption (cf) {
	var c_num = document.forms[1].elements[cf].options.length;
	for (i = 1; i < c_num; i++) {
		var c_cd  = document.forms[1].elements[cf].options[i].value;
		if (document.forms[1].elements['category_'+c_cd]) {
			var g_val = document.forms[1].elements['category_'+c_cd].value;

			genre_options[c_cd] = new Array();
			genre_options[c_cd] = g_val.split("<=>");
		}
	}
}
/**
 * スタイル選択フォーム値読込み
 */
var spcate_options = new Array();
function ReadSPcategoryOption (gv) {
	if (document.forms[1].elements['spcategory_'+gv]) {
		var s_val = document.forms[1].elements['spcategory_'+gv].value;
		spcate_options[gv] = new Array();
		spcate_options[gv] = s_val.split("<=>");
		return true;
	}
	return false;
}

/**
 * ジャンル選択フォーム値設定
 */
function SetStatusOption (cf,gf,ca) {
	var len1 = document.forms[1].elements[gf].options.length;
	for (i = 1; i < len1; i++) {
		document.forms[1].elements[gf].options[1] = null;
	}
	var len2 = document.forms[1].elements[ca].options.length;
	for (i = 1; i < len2; i++) {
		document.forms[1].elements[ca].options[1] = null;
	}
	var ci = document.forms[1].elements[cf].options.selectedIndex;
	if (ci == 0) {
		return;
	}
	if (genre_options.length == 0) {
		ReadGenreOption(cf);
	}
	var cv = document.forms[1].elements[cf].options[ci].value;
	if (! genre_options[cv] || genre_options.length == 0) {
		return;
	}
	for (i = 1; i <= genre_options[cv].length; i++) {
		var data = genre_options[cv][i-1].split('=');
		document.forms[1].elements[gf].options[i] = new Option(data[1],data[0]);
	}
    
    SetComboSelected(gf);
    
	return;
}
/**
 * 
 */
function SetSPCategoryOption (cf,spf) {
	var len = document.forms[1].elements[spf].options.length;
	for (i = 1; i < len; i++) {
		document.forms[1].elements[spf].options[1] = null;
	}
	var gi = document.forms[1].elements[cf].options.selectedIndex;
    //alert(gi);
	if (gi == 0) {
		return;
	}
	var gv = document.forms[1].elements[cf].options[gi].value;
	if (! spcate_options[gv]) {
		var ret = ReadSPcategoryOption(gv);
		if (ret == false) {
			return;
		}
	}
	for (i = 1; i <= spcate_options[gv].length; i++) {
		var data = spcate_options[gv][i-1].split('=');
		document.forms[1].elements[spf].options[i] = new Option(data[1],data[0]);
	}
    
    SetComboSelected(spf);
    
	return;
}

/**
 * Reset combo box value
*/
function SetComboSelected(cf) {
    //alert(cf);
	var gv = document.forms[1].elements['hdn_'+cf].value;
	if (gv == '') {
		return;
	}
	var len = document.forms[1].elements[cf].options.length;
	
	for (i = 1; i < len; i++) {
		if (document.forms[1].elements[cf].options[i].value == gv) {
			document.forms[1].elements[cf].options[i].selected = true;
			break;
		}
	}
	return;
}


/**
 * ジャンル選択フォーム初期値セット
*/
function SetGenreSelected(cf,gf) {
    //alert('4');
	var ci = document.forms[1].elements[cf].options.selectedIndex;
	var gv = document.forms[1].elements['hdn_'+gf].value;
	if (ci == 0 || gv == '') {
		return;
	}
	var len = document.forms[1].elements[gf].options.length;
	if (len == 1) {
		SetStatusOption(cf,gf);
		len = document.forms[1].elements[gf].options.length;
	}
	for (i = 1; i < len; i++) {
		if (document.forms[1].elements[gf].options[i].value == gv) {
			document.forms[1].elements[gf].options[i].selected = true;
			break;
		}
	}
	return;
}
/**
 * スタイル選択フォーム初期値セット
*/
function SetStyleSelected (gf,sf) {
    //alert('5');
	var gi  = document.forms[1].elements[gf].options.selectedIndex;
	var sv  = document.forms[1].elements['hdn_'+sf].value;
	if (gi == 0 || sv == '') {
		return;
	}
	var len = document.forms[1].elements[sf].options.length;
	if (len == 1) {
		SetStyleOption(gf,sf);
		len = document.forms[1].elements[sf].options.length;
	}
	for (i = 1; i < len; i++) {
		if (document.forms[1].elements[sf].options[i].value == sv) {
			document.forms[1].elements[sf].options[i].selected = true;
			break;
		}
	}
	return;
}
/**
 *
 */
 function FontTag1 (f) {
    var c = document.forms[1].elements[f+'_color'].value;
	var str = "<span style=\"color:" + c + ";";

	pos = document.selection.createRange().duplicate();
	txt = (pos.text != null) ? pos.text : '';

    if (document.forms[1].elements[f+'_size'].options[2].selected == true) {
		str += "font-size:x-small;";
	}
	if(txt == '') {
		document.forms[1].elements[f].value += str + "\">" + txt + "</span>";
	} else {
		pos.text = str + "\">" + txt + "</span>";
	}
	pos=null;
}
/**
 * Link tag
 */
function LinkTag1 (f) {
	
	pos = document.selection.createRange().duplicate();
	txt = (pos.text != null) ? pos.text : '';
	var l = document.forms[1].elements[f+'_url'].value;
    if (l != '') {
		if (document.forms[1].elements[f+'_link_type'][0].checked == true) {
			if (txt==''){
				document.forms[1].elements[f].value += "<a href=\"#?sku=" + l + "\"></a>";
			} else {
				pos.text = "<a href=\"#?sku=" + l + "\">" + txt + "</a>";
			}
			
		} else if (document.forms[1].elements[f+'_link_type'][1].checked == true) {
			if (txt==''){
				document.forms[1].elements[f].value += "<a href=\"#?a=" + l + "\"></a>";
			} else {
				pos.text = "<a href=\"#?a=" + l + "\">" + txt + "</a>";
			}
		}
	}
	if (document.forms[1].elements[f+'_link_type'][2]) {
		if (document.forms[1].elements[f+'_link_type'][2].checked == true) {
			if (txt==''){ 
				document.forms[1].elements[f].value += "<a href=\"\"></a>";
			} else {
				pos.text = "<a href=\"\">" + txt + "</a>";
			}
		}
	}
	pos=null;
}
/**
 * Set current date
 */
function SetCurrentDateOption (f,n) {
	if (f != 'searchdir') {
		SetHiddenValSelected(f+'_hh');
		SetHiddenValSelected(f+'_mm');
	}
	// Nﾝ?
	if (n == '') return;
	var y_offset = parseInt(n);
	if (y_offset > 2006) {
		y_offset = 2006;
	}
	var dt = new Date();
	var y_limit = dt.getYear();
	/*
    if (f == 'searchdir') {
		y_limit += 1;
	} else {
		y_limit += 20;
	}
    */
	var val = 0;
	if (document.forms[1].elements['hdn_'+f+'_y']) {
		val = document.forms[1].elements['hdn_'+f+'_y'].value;
	}

	for (i = 0; i <= (y_limit - y_offset); i++) {
		var y = y_limit - i;
		document.forms[1].elements[f+'_y'].options[i+1] = new Option(y,y);
		if (y == val) {
			document.forms[1].elements[f+'_y'].options[i+1].selected = true;
		}
	}
	SetHiddenValSelected(f+'_m');
	SetDayOption(f);
	SetHiddenValSelected(f+'_d');
	return;
}

/**
 * Set future date
 */
function SetFutureDateOption (f,n) {
	if (f != 'searchdir') {
		SetHiddenValSelected(f+'_hh');
		SetHiddenValSelected(f+'_mm');
	}
	// Nﾝ?
	if (n == '') return;
	var y_offset = parseInt(n);
	if (y_offset > 2006) {
		y_offset = 2006;
	}
	var dt = new Date();
	var y_limit = dt.getYear();
    y_limit = y_limit + 5;
	/*
    if (f == 'searchdir') {
		y_limit += 1;
	} else {
		y_limit += 20;
	}
    */
	var val = 0;
	if (document.forms[1].elements['hdn_'+f+'_y']) {
		val = document.forms[1].elements['hdn_'+f+'_y'].value;
	}

	for (i = 0; i <= (y_limit - y_offset); i++) {
		var y = y_limit - i;
		document.forms[1].elements[f+'_y'].options[i+1] = new Option(y,y);
		if (y == val) {
			document.forms[1].elements[f+'_y'].options[i+1].selected = true;
		}
	}
	SetHiddenValSelected(f+'_m');
	SetDayOption(f);
	SetHiddenValSelected(f+'_d');
	return;
}


/**
 * Set today date selected
 */
function SetTodaySelected() { 
	sd = new Date();
	ed = new Date();
	if (i == 1) {
		ed.setTime(ed.getTime() + (24 * 3600000));
	} else if (i == 2) {
		sd.setTime(sd.getTime() - (24 * 3600000));
	} else if (i == 3) {
		sd.setTime(sd.getTime() - (24 * 3600000 * 7));
		ed.setTime(ed.getTime() + (24 * 3600000));
	}
	y = sd.getYear();
	if (y < 2000) y += 1900; 
    if( document.forms[1].elements['hdn_s_open_y'].value == '' )
	    SetSelected('s_open_y',   y);
    if( document.forms[1].elements['hdn_s_open_m'].value == '' ) 
	    SetSelected('s_open_m',   sd.getMonth() + 1); 
	SetDayOption('s_open');
    if( document.forms[1].elements['hdn_s_open_d'].value == '' ) 
	    SetSelected('s_open_d',   sd.getDate());
	//SetSelected('s_open_hh',  0);
	//SetSelected('s_open_mm',  0);
	//y = ed.getYear();
	//if (y < 2000) y += 1900;
	//SetSelected('s_close_y',  y);
	//SetSelected('s_close_m',  ed.getMonth() + 1);
	//SetDayOption('s_close');
	//SetSelected('s_close_d',  ed.getDate());
	//SetSelected('s_close_hh', 0);
	//SetSelected('s_close_mm', 0);
}
        
/**
 * 各種選択オプション初期選択値設定
 */
function SetSelected (f,v) {
	var len = document.forms[1].elements[f].options.length;
	for (i = 1; i < len; i++) {
		if (document.forms[1].elements[f].options[i].value == v) {
			document.forms[1].elements[f].options[i].selected = true;
			break;
		}
	}
	return;
}        
/**
 newOpenWindow
*/
function newOpenWindow(url, w, h) {
        var options = "width=" + w + ",height=" + h + ",";
        options += "resizable=yes,scrollbars=yes,status=yes,";
        options += "menubar=no,toolbar=no,location=no,directories=no";
        var newWin = window.open(url, 'newWin', options);
        newWin.focus();
}

/**
 * HIDDEN値を選択オプション初期選択値に設定
 */
function SetHiddenValSelected (f) {
	if (document.forms[1].elements['hdn_'+f]) {
		var v = document.forms[1].elements['hdn_'+f].value;
		if (v == '') {
			return;
		}
		SetSelected(f,v);
	}
	return;
}

/**
 * 日付選択オプション値生成
 */
function SetDayOption (f) {
	var yi = document.forms[1].elements[f+'_y'].options.selectedIndex;
	var yv = parseInt(document.forms[1].elements[f+'_y'].options[yi].value);
	var mi = document.forms[1].elements[f+'_m'].options.selectedIndex;
	var mv = parseInt(document.forms[1].elements[f+'_m'].options[mi].value);
	if (yi == 0 || mi == 0) {
		return;
	}
	var dv = 0;
	if (mv == 2) {
		if((yv % 4 == 0) && (yv % 100 != 0 || yv % 400 == 0)) {
			dv = 29; 
		} else {
			dv = 28;
		}
	} else if ((mv < 8 && (mv % 2) == 1) || (mv >= 8 && (mv % 2) == 0)) {
		dv = 31; 
	} else {
		dv = 30;
	}
	if ((dv + 1) == document.forms[1].elements[f+'_d'].options.length) {
		return;
	}
	for (i = 1; i <= dv; i++) {
		document.forms[1].elements[f+'_d'].options[i] = new Option(i,i);
	}
	var len = document.forms[1].elements[f+'_d'].options.length;
	dv++;
	if (len > dv) {
		for (i = dv; i < len; i++) {
			document.forms[1].elements[f+'_d'].options[dv] = null;
		}
	}
	return;
}

/**
 * イメージ検索画面オープン
 */
var img_list_win;
function SearchImageISC(f) {
	document.forms[1].img_list_form.value = f;
	if (!img_list_win || img_list_win.closed) {
		img_list_win = window.open("../../image.php?action_image_list=1", "img_list_win", "width=800, height=800, scrollbars=yes, resizable=yes");
	}
	img_list_win.window.focus();
}
/**
 * イメージアップロード画面オープン
 */
var img_up_win;
function ImageUploaderISC (f) {
	document.forms[1].img_up_form.value = f;
	if (!img_up_win || img_up_win.closed) {
		img_up_win = window.open("../../image.php?action_image_add=1", "img_up_win", "width=650, height=450, scrollbars=no, resizable=yes");
	}
	img_up_win.window.focus();
}
/**
 * イメージプレビュー
 */
function PreviewImageISC (v) {
	var path = document.forms[1].path.value;
	if (path == '' || v == '') {
		return;
	}
	document.images['preview'].src = path + v;
	document.forms[1].url.value = path + v;
	document.getElementById('image_preview').style.display = 'inline';
	return;
}
/**
 * イメージフォーム名設定
 */
function UploadImageISC () {
	document.forms[1].form_name.value = opener.document.forms[1].img_up_form.value;
	document.forms[1].submit();
}
/**
 * イメージURL挿入
 */
function SelectImageISC (f) {
	var url = document.forms[1].url.value;
	with (opener) {
		window.focus();
		var val = document.forms[1].elements[f].value;
		document.forms[1].elements[val].value = url;
	}
	return true;
}
/**
 * イメージタグ挿入
 */
function InputImgTagISC(f) {
	var img = document.forms[1].elements[f+'_img'].value;
	if (img == '') {
		return;
	}

	var str = '';
	var img = document.forms[1].elements[f+'_img'].value;
	if (img == '') {
		return;
	}
	str = "<img src='" + img + "' />";
	document.forms[1].elements[f].value += str;
	return true;
}

/**
*
**/
function aleftConfirm(strMsg){
	if((strMsg == null) || (strMsg == ''))
		strMsg = '更新します。よろしいでしょうか？';
	if(confirm(strMsg))
		return true;
	return false;
}
