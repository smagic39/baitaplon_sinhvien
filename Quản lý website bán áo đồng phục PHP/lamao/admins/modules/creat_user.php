<?php if (substr_count($_SERVER['PHP_SELF'],'/creat_user.php')>0) die ("You can't access this file directly..."); ?>
<?php
$allow_edit=false;
if ($_SESSION['usergroup']<2)
  	{
//----------------------------------------- Actions -------------------------------------------
	if (isset($_POST['confirmvalue']) && $_POST['confirmvalue']==1)
		{
		$msg='';	
		$error=0;
		$var_array=array('username','password','groupof','realname','birthday','genre',
				'address','phone','email','job','jobadd','ymid','website','description',
				'avatar','sign','regtime','active');
		$i=0;
		while (isset($var_array[$i]))
			{
			if (isset($_POST[$var_array[$i]]))
				$$var_array[$i]=addslashes(strip_tags(chop($_POST[$var_array[$i]])));
			else
				$$var_array[$i]='';
			$i++;
			}
		$username=strtolower($username);
		$password=encrypt(strtolower($password));
		$active='y';
		if (isset($_POST['day'],$_POST['month'],$_POST['year']))
			$birthday=$_POST['year'].'-'.$_POST['month'].'-'.$_POST['day'];
		switch ($avatar)
			{
			case 'select':
			$avatar=$_POST['selectimage'];
			break;
			
			case 'url':
			if (validateimage('url',$_POST['urlimage'],$_POST['urlimage']))
				$avatar=$_POST['urlimage'];
			else
				{
				$msg.='Hình không tồn tại trên web hoặc bạn nhập không đúng !<br>';
				$error+=1;
				}
			break;
			
			case 'upload':
			$avatar=$_FILES['uploadimage']['name'];
			$checkimage=validateimage('upload','uploadimage','../avatar');
			if ($checkimage=='ok')
				{
				$uploadimage='../avatar/'.$avatar;
				$itemp=$_FILES['uploadimage']['tmp_name'];
				if (move_uploaded_file($itemp,$uploadimage))
					{
					$msg.='Image uploaded successfully !<br>';
					}
				else
					{
					$error+=1;
					$msg.='Can not upload Image !<br>';
					}
				}
			else
				{
				$error+=1;
				$msg.=$error_code[$checkimage];
				}
			break;
			}
		$regtime=date('Y-m-d H:m:i');

		if ($error==0)
			{
			$query='insert into users';
			$query.='(';
			$i=0;
			while (isset($var_array[$i]))
				{
				if ($i>0)
					$query.=',';
				$query.=$var_array[$i];
				$i++;
				}
			$query.=') values(';
			$i=0;
			while (isset($var_array[$i]))
				{
				if ($i>0)
					$query.=',';
				$query.='"'.$$var_array[$i].'"';
				$i++;
				}
			$query.=')';
			//echo $query;
			if (checkdata('users','username',$username)>0)
				{
				set_error($strErr['106']);
				}
			else
				{
				if ($doquery=mysql_query($query,$link))
					{
					set_notice($strNotice['101']);
					}
				else
					{
					set_error($strErr['101']);
					}
				}
			}
		}
//------------------------------------- End Actions ----------------------------------------

//----------------------------------------- Form -------------------------------------------
?>
<form name="creat_user" method="post" enctype="multipart/form-data" action="" onsubmit="return validateForm(f['',bConfirm['',bDisable['',bDisableR['',groupError['',errorMode]]]]])">
<input type="hidden" name="submit" value="1">
<?php $ftitle=array(); ?>
<table width="80%" border="0" cellspacing="0" cellpadding="4">
	<tr onMouseOver="change(this,'#E9E9E9');" onMouseOut="undo(this);">
	<td width="25%" valign="top"><p class="formtitle">
	<?php echo $strUserName; ?><font color="#FF3300">*</font></p></td>
	<td width="5%">&nbsp;</td>
	<td width="70%">
	<input name="username" type="text" value="<?php if (isset($username,$error) && $error==1) echo $username; ?>" class="mediuminput" id="username" size="16" maxlength="16">
	<p class="formindex"><?php echo $strUserNameRule; ?></p>
	</td>
	</tr>
		
	<tr onMouseOver="change(this,'#E9E9E9');" onMouseOut="undo(this);">
	<td width="25%" valign="top"><p class="formtitle">
	<?php echo $strPassWord; ?><font color="#FF3300">*</font></p></td>
	<td width="5%">&nbsp;</td>
	<td width="70%">
	<input name="password" type="password" class="mediuminput" id="password" size="16" maxlength="16">
	<p class="formindex"><?php echo $strPassWordRule; ?></p>
	</td>
	</tr>
	
	<tr onMouseOver="change(this,'#E9E9E9');" onMouseOut="undo(this);">
	<td width="25%" valign="top"><p class="formtitle">
	<?php echo $strConfirm.' '.$strPassWord; ?><font color="#FF3300">*</font></p></td>
	<td width="5%">&nbsp;</td>
	<td width="70%">
	<script>
	function comparepass()
		{
		if (document.creat_user.confirmpass.value!=document.creat_user.password.value && document.creat_user.confirmpass.value!='')
			{
			alert("<?php echo $strConfirm.' '.$strPassWord.' '.$strNotLike.' '.$strPassWord; ?>")
			document.creat_user.confirmpass.focus()
			}
		}
	</script>
	<input name="confirmpass" type="password" class="mediuminput" id="confirm" size="16" maxlength="16" onblur="comparepass()">
	<p class="formindex"></p></td>
	</tr>
		
	<tr onMouseOver="change(this,'#E9E9E9');" onMouseOut="undo(this);">
	<td width="25%" valign="top"><p class="formtitle">
	<?php echo $strGroup.' '.$strUser; ?><font color="#FF3300">*</font></p></td>
	<td width="5%">&nbsp;</td>
	<td width="70%">
	<select name="groupof">
	<option value="">??</option>
	<?php
	$groups ='select * from user_groups';
	$groups.=' where id>='.$_SESSION['usergroup'];
	$groups.=' order by id ASC';
	$dogroup=mysql_query($groups,$link);
	if ($dogroup)
		{
		while ($result=mysql_fetch_array($dogroup))
			{
			echo '<option value="'.$result['id'].'">';
			echo $result['description'];
			echo '</option>';
			}
		}
	?>
	</select>
	<p class="formindex"></p>
	</td>
	</tr>
	
	<tr onMouseOver="change(this,'#E9E9E9');" onMouseOut="undo(this);">
	<td width="25%" valign="top"><p class="formtitle">
	<?php echo $strRealName; ?><font color="#FF3300">*</font></p></td>
	<td width="5%">&nbsp;</td>
	<td width="70%">
	<input class="mediuminput" type="text" name="realname" value="<?php if (isset($realname,$error) && $error==1) echo $realname; ?>" maxlength="40" check="blank">
	<p class="formindex"><?php echo $strRealNameRule; ?></p></td>
	</tr>

	<tr onMouseOver="change(this,'#E9E9E9');" onMouseOut="undo(this);">
	<td width="25%" valign="top"><p class="formtitle">
	<?php echo $strBirthDay; ?><font color="#FF3300">*</font></p></td>
	<td width="5%">&nbsp;</td>
	<td width="70%">
	<p class="formindex">
	<?php echo $strDay; ?>
	<select name="day">
	<option value=''>??</option>
	<?php
	for ($i=1;$i<=31;$i++)
		{
		if ($i<10) $i='0'.$i;
		echo '<option value="'.$i.'"';
		if (isset($day,$error) && $i==$day && $error==1)
			echo ' selected';
		echo '>'.$i.'</option>';
		}
	?>
	</select>
	<?php echo $strMonth; ?> 
	<select name="month">
	<option value=''>??</option>
	<?php
	for ($i=1;$i<=12;$i++)
		{
		if ($i<10)	$i='0'.$i;
		echo '<option value="'.$i.'"';
		if (isset($month,$error) && $i==$month && $error==1)
			echo ' selected';
		echo '>'.$i.'</option>';
		}
	?>
	</select>
	<?php echo $strYear; ?> 
	<select name="year">
	<option value=''>????</option>
	<?php
	$cur_year=date('Y'); $start_year=$cur_year-80;
	for ($i=$start_year;$i<=$cur_year;$i++)
		{
		echo '<option value="'.$i.'"';
		if (isset($year,$error) && $i==$year && $error==1)
			echo ' selected';
		echo '>'.$i.'</option>';
		}
	?>
	</select>
	</p>
	</td>
	</tr>

	<tr onMouseOver="change(this,'#E9E9E9');" onMouseOut="undo(this);">
	<td width="25%" valign="top"><p class="formtitle">
	<?php echo $strGender; ?><font color="#FF3300">*</font></p></td>
	<td width="5%">&nbsp;</td>
	<td width="70%">
	<select name="genre">
	<option value=''>??</option>
	<option value='m'><?php echo $strMale; ?></option>
	<option value='f'><?php echo $strFemale; ?></option>
	</select>
	</td>
	</tr>

	<tr onMouseOver="change(this,'#E9E9E9');" onMouseOut="undo(this);">
	<td width="25%" valign="top"><p class="formtitle">
	<?php echo $strAddress; ?><font color="#FF3300">*</font></p></td>
	<td width="5%">&nbsp;</td>
	<td width="70%">
	<input class="mediuminput" type="text" name="address" value="<?php if (isset($address,$error) && $error==1) echo $address; ?>" maxlength="120">
	<p class="formindex"><?php echo $strAddressRule; ?></p>
	</td>
	</tr>
	   
	<tr onMouseOver="change(this,'#E9E9E9');" onMouseOut="undo(this);">
	<td width="25%" valign="top"><p class="formtitle">
	<?php echo $strJob; ?><font color="#FF3300">*</font></p></td>
	<td width="5%">&nbsp;</td>
	<td width="70%">
	<input class="mediuminput" type="text" name="job" value="<?php if (isset($job,$error) && $error==1) echo $job; ?>" maxlength="40">
	<p class="formindex"><?php echo $strJobRule; ?></p>
	</td>
	</tr>

	<tr onMouseOver="change(this,'#E9E9E9');" onMouseOut="undo(this);">
	<td width="25%" valign="top"><p class="formtitle">
	<?php echo $strJobAddress; ?><font color="#FF3300">*</font></p></td>
	<td width="5%">&nbsp;</td>
	<td width="70%">
	<input class="mediuminput" type="text" name="jobadd" value="<?php if (isset($jobadd,$error) && $error==1) echo $jobadd; ?>" maxlength="120">
	<p class="formindex"><?php echo $strJobAddressRule; ?></p>
	</td>
	 </tr>
	 <tr onMouseOver="change(this,'#E9E9E9');" onMouseOut="undo(this);">
	<td width="25%" valign="top"><p class="formtitle">
	<?php echo $strPhone; ?><font color="#FF3300">*</font></p></td>
	<td width="5%">&nbsp;</td>
	<td width="70%">
	<input name="phone" type="text" value="<?php if (isset($phone,$error) && $error==1) echo $phone; ?>" class="mediuminput" maxlength="20">
	<p class="formindex"><?php echo $strPhoneRule; ?></p>
	</td>
	 </tr>
	 <tr onMouseOver="change(this,'#E9E9E9');" onMouseOut="undo(this);">
	<td width="25%" valign="top"><p class="formtitle">
	<?php echo $strEmail; ?><font color="#FF3300">*</font></p></td>
	<td width="5%">&nbsp;</td>
	<td width="70%">
	<input name="email" type="text" value="<?php if (isset($email,$error) && $error==1) echo $email; ?>" class="mediuminput" maxlength="40">
	<p class="formindex"><?php echo $strEmailRule; ?></p>
	</div>
	</td>
	 </tr>
	 <tr onMouseOver="change(this,'#E9E9E9');" onMouseOut="undo(this);">
	<td width="25%" valign="top"><p class="formtitle">
	<?php echo $strYahooId; ?></p></td>
	<td width="5%">&nbsp;</td>
	<td width="70%">
	<input name="ymid" type="text" value="<?php if (isset($ymid,$error) && $error==1) echo $ymid; ?>" class="mediuminput" maxlength="40">
	<p class="formindex"></p>
	</td>
	 </tr>
	 <tr onMouseOver="change(this,'#E9E9E9');" onMouseOut="undo(this);">
	<td width="25%" valign="top"><p class="formtitle">
	<?php echo $strWebsite; ?></p></td>
	<td width="5%">&nbsp;</td>
	<td width="70%">
	<input name="website" type="text" value="<?php if (isset($website,$error) && $error==1) echo $website; ?>" class="mediuminput" maxlength="60">
	<p class="formindex"><?php echo $strWebsiteRule; ?></p>
	</td>
	 </tr>
	 <tr onMouseOver="change(this,'#E9E9E9');" onMouseOut="undo(this);">
	<td width="25%" valign="top"><p class="formtitle">
	<?php echo $strDescription; ?></p></td>
	<td width="5%">&nbsp;</td>
	<td width="70%">
	<textarea name="description" onkeyup="TrackCount(this,'descount',255)" onkeypress="LimitText(this,255)"><?php if (isset($description,$error) && $error==1) echo $description; ?></textarea>
	<p class="formindex"><?php echo $strDescriptionRule; ?>
	<input type="text" name="descount" size="3" value="255">
	</p>
	</td>
	 </tr>
	 <tr onMouseOver="change(this,'#E9E9E9');" onMouseOut="undo(this);">
	<td width="25%" valign="top"><p class="formtitle">
	<?php echo $strSign; ?></p></td>
	<td width="5%">&nbsp;</td>
	<td width="70%">
	<textarea name="sign" onkeyup="TrackCount(this,'sigcount',255)" onkeypress="LimitText(this,255)"><?php if (isset($sign,$error) && $error==1) echo $sign; ?></textarea>
	<p class="formindex">
	<?php echo $strSignRule; ?>
	<input type="text" name="sigcount" size="3" value="255">
	<br>
	<input type="checkbox" name="checkbox" value="checkbox">
	<?php echo $strShowSign; ?></p>
	</td>
	</tr>
		  
	<tr onMouseOver="change(this,'#E9E9E9');" onMouseOut="undo(this);">
	<td width="25%" valign="top"><p class="formtitle">
	<?php echo $strAvatar; ?></p></td>
	<td width="5%">&nbsp;</td>
	<td width="70%">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td colspan="4" align="center" valign="top">
		<p class="formindex"><strong>
		<input type="radio" id="option1" name="avatar" value="select" onClick="javascript: Toggle('1');">
		<?php echo $strExistImage; ?></strong></p></td>
		</tr>
	 
	  <tr id="1" style="display: none">
	  <td>
	  <select name="selectimage" size="5" onChange="preview()">
		<?php
		//Doc thu muc chua anh dai dien
		  $setdir=$avatar_dir;
		  $dir=opendir($setdir);
		  if ($dir)
			{
			$counter=0;
			while ($cur_img=readdir($dir))
				{
				if ($cur_img!='.' && $cur_img!='..' && $cur_img!='Thumbs.db')
					{
					$counter+=1;
					echo '<option value="'.$cur_img.'"';
					//Luu lai anh dau tien
					if ($counter==1)
						{
						$first_img=$cur_img;
						echo ' selected';
						}
					echo '> '.$strImage.' '.$counter.' </option>';
					}
				}
			}
			if ($dir)
				closedir($dir);
			?>
		</select>
	  <p class="formindex"><?php echo $strViewAll; ?></p>
	  </td>
	  <td width="5%">&nbsp;</td>
	  <td>
	  <script language="JavaScript">
	  function preview()
		  {
		  var image='<?php echo $avatar_dir; ?>/' + document.creat_user.selectimage.value;
		  document.images.viewavatar.src=image;
		  }
	  </script>
	  <?php //echo $first_img; ?>
		<p class="formindex"><strong>
		<?php echo $strPreview; ?></strong></p>
		<img class="imgframe" name="viewavatar" width="96" height="96" src="<?php echo $setdir.$first_img; ?>">  
	  </td>
	  <td width="5%">&nbsp;</td>
	  </tr>
	  
	  <tr>
		<td colspan="4" align="center" valign="top">
		<p class="formindex"><strong>
		<input type="radio" id="option2" name="avatar" value="url" onClick="Toggle('2');">
		<?php echo $strUrlImage; ?></strong><br><?php echo $strUrlImageRule; ?></p></td>
	  </tr>
	  <tr id="2" style="display: none">
	  <td colspan="4">
	  <p class="formindex">
	  <input type="text" name="urlimage" class="longinput"></p>
	  </td>
	  </tr>
	  
	  <tr>
		<td colspan="4" align="center" valign="top">
		<p class="formindex"><strong>
		<input type="radio" id="option3" name="avatar" value="upload" onClick="Toggle('3');">
		<?php echo $strUploadImage; ?></strong><br></p></td>
	  </tr>
	  <tr id="3" style="display: none">
	  <td colspan="4">
	  <p class="formindex">
	  <input type="file" name="uploadimage" class="longinput">
	  </p>
	  </td>
	  </tr>
	  
	</table>
			</td>
		  </tr>
		  
		  <tr>
			<td colspan="3" align="center" valign="top">
			<p class="formindex">
			<input type="checkbox" name="confirmbut" onClick="Toggle('confirmbox');" value="">
			<strong><?php echo $strClickView; ?> !
			<div id="confirmbox" style="display: none" class="formindex" align="center">
			<input type="checkbox" name="confirmvalue" value="1">
			<?php echo $strConfirm; ?>
			</div>
			</strong>
			</p>
			</td>
		  </tr>
		  
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  
		  <tr align="center" valign="middle">
			<td colspan="3">
			<table width="80%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td align="right"><input type="submit" name="Submit" value="<?php echo $strCreatNew.' '.$strUser; ?>"></td>
				<td width="10%">&nbsp;</td>
				<td align="left"><input type="reset" name="Restore" onClick="" value="<?php echo $strReset; ?>"></td>
				</tr>
			  </table>
			</td>
			</tr>
		  
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
	</table>

</form>

		<SCRIPT language="JavaScript">
		///*
		var frmvalidator  = new Validator("creat_user");
		frmvalidator.addValidation("username","req","<?php echo $strNoUserName; ?>");
		frmvalidator.addValidation("username","alnumhyphen","<?php echo $strUserNameRule; ?>");
		frmvalidator.addValidation("username","minlen=4","<?php echo $strUserNameRule; ?>");
		frmvalidator.addValidation("username","maxlen=21","<?php echo $strUserNameRule; ?>");
		
		frmvalidator.addValidation("password","req","<?php echo $strNoPassWord; ?>");
		frmvalidator.addValidation("password","alphanumeric","<?php echo $strPassWordRule; ?>");
		frmvalidator.addValidation("password","minlenght=4","<?php echo $strPassWordRule; ?>");
		frmvalidator.addValidation("password","maxlenght=21","<?php echo $strPassWordRule; ?>");
		frmvalidator.addValidation("confirmpass","req","<?php echo $strConfirm.' '.$strPassWord; ?>");
		frmvalidator.addValidation("confirmpass","confirmpass=password","<?php echo $strConfirm.' '.$strPassWord; ?>");
		
		frmvalidator.addValidation("realname","req","<?php echo $strInput.' '.$strRealName; ?>");
		//frmvalidator.addValidation("realname","alpha");
		frmvalidator.addValidation("realname","maxlen=40","<?php echo $strRealNameRule; ?>"); 
		
		frmvalidator.addValidation("groupof","dontselect=0","<?php echo $strSelect.' '.$strGroup; ?>");
		frmvalidator.addValidation("day","dontselect=0","<?php echo $strSelect.' '.$strDay; ?>");
		frmvalidator.addValidation("month","dontselect=0","<?php echo $strSelect.' '.$strMonth; ?>");
		frmvalidator.addValidation("year","dontselect=0","<?php echo $strSelect.' '.$strYear; ?>");
		
		frmvalidator.addValidation("genre","dontselect=0","<?php echo $strSelect.' '.$strGender; ?>");
		
		frmvalidator.addValidation("address","req","<?php echo $strInput.' '.$strAddress; ?>");
		frmvalidator.addValidation("address","maxlen=120","<?php echo $strAddressRule; ?>"); 
		frmvalidator.addValidation("address","minlen=5","<?php echo $strAddressRule; ?>"); 
		//frmvalidator.addValidation("address","albet"); 
		
		frmvalidator.addValidation("job","req","<?php echo $strInput.' '.$strJob; ?>");
		frmvalidator.addValidation("job","maxlen=120","<?php echo $strJobRule; ?>"); 
		frmvalidator.addValidation("job","minlen=5","<?php echo $strJobRule; ?>"); 
		
		frmvalidator.addValidation("jobadd","req","<?php echo $strInput.' '.$strJobAddress; ?>");
		frmvalidator.addValidation("jobadd","maxlen=120","<?php echo $strJobAddressRule; ?>"); 
		frmvalidator.addValidation("jobadd","minlen=5","<?php echo $strJobAddressRule; ?>"); 
		
		frmvalidator.addValidation("phone","num","<?php echo $strPhoneRule; ?>");
		//frmvalidator.addValidation("phone","minlen=6");
		frmvalidator.addValidation("email","email","<?php echo $strEmailRule; ?>");
		frmvalidator.addValidation("email","req","<?php echo $strEmailRule; ?>");
		frmvalidator.addValidation("ymid","alnumhyphen");
		//frmvalidator.addValidation("description","req","Ban nen co doi chut gioi thieu ve ban than");
		//frmvalidator.addValidation("sign","req","Ban can co chu ky de hien thi cuoi moi bai viet");
		//frmvalidator.addValidation("avatar","req","Ban can co Hinh dai dien ");
		
		frmvalidator.addValidation("confirmbut","dontcheck","<?php echo $strConfirm; ?>");
		frmvalidator.addValidation("confirmvalue","dontcheck","<?php echo $strConfirm.' '.$strAgain; ?>");
		//*/
		</script>

<?php
	}
else
	{
	?>
	<p class="bigtitle"><?php echo $strErr['201']; ?></p>
	<?php
	}
?>