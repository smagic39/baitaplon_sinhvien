<?php

function subscriber_exists($email)
{
  if(!$email)
    return false;

  if(!db_connect())
    return false;

  $query = "select count(*) from subscribers where email = '$email'";

  $result = mysql_query($query);
        if(!$result)
    return false;

  return (mysql_result($result, 0, 0)>0);
}

// kiem tra dia chi email da dang ky thu tin trong CSDL
function subscribed($email, $listid)
{
  if(!$email||!$listid)
    return false;

  if(!db_connect())
    return false;

  $query = "select count(*) from sub_lists where email = '$email'
            and listid = $listid";

  $result = mysql_query($query);
        if(!$result)
    return false;

  return (mysql_result($result, 0, 0)>0);
}

// kiem tra su ton tai cua danh sach?
function list_exists($listid)
{
  if(!$listid)
    return false;

  if(!db_connect())
    return false;

  $query = "select count(*) from lists where listid = '$listid'";

  $result = mysql_query($query);
        if(!$result)
    return false;

  return (mysql_result($result, 0, 0)>0);
}

// ham nhan ten user cua dia chi email
function get_real_name($email)
{
  if(!$email)
    return false;

  if(!db_connect())
    return false;

  $query = "select realname from subscribers where email = '$email'";

  $result = mysql_query($query);
        if(!$result)
    return false;

  return trim(mysql_result($result, 0, 0));
}

// nhan kieu thu tin (HTML hoac Text) ma user da dang ky
function get_mimetype($email)
{
  if(!$email)
    return false;

  if(!db_connect())
    return false;

  $query = "select mimetype from subscribers where email = '$email'";

  $result = mysql_query($query);
        if(!$result)
    return false;

  return trim(mysql_result($result, 0, 0));
}

// dang ky dia chi email den danh sach trong CSDL
function subscribe($email, $listid)
{
  if(!$email||!$listid||!list_exists($listid)||!subscriber_exists($email))
    return false;

  //neu mail dang ky da ton tai tren danh sach
  if(subscribed($email, $listid))
    return false;

  if(!db_connect())
    return false;

  $query = "insert into sub_lists values ('$email', $listid)";

  $result = mysql_query($query);
        return $result;
}

// huy bo dang ky doi voi dia chi email tren mot danh sach
function unsubscribe($email, $listid)
{
  if(!$email||!$listid)
    return false;

  if(!db_connect())
    return false;

  $query = "delete from sub_lists where email = '$email' and listid = $listid";

  $result = mysql_query($query);
  return $result;
}

// nap thong tin cua thu tin tu CSDL
function load_mail_info($mailid)
{
  if(!$mailid)
    return false;

  if(!db_connect())
    return false;

  $query = "select subject, listid, status, sent from mail
            where mailid = $mailid";

  $result = mysql_query($query);

  if(!$result)
  {
    echo "Khong the tim lai mail nay $query";
    return false;
  }
  return mysql_fetch_array($result);

}

// nap thong tin cua danh sach tu CSDL
function load_list_info($listid)
{
  if(!$listid)
    return false;

  if(!db_connect())
    return false;

  $query = "select listname, blurb from lists where listid = $listid";
  $result = mysql_query($query);
        if(!$result)
  {
    echo 'Khong the lay lai danh sach nay';
    return false;
  }
  $info =  mysql_fetch_array($result);

  $query = "select count(*) from sub_lists where listid = $listid";
  $result = mysql_query($query);
        if($result)
  {
    $info['subscribers'] = mysql_result($result, 0, 0);
  }
  $query = "select count(*) from mail where listid = $listid
            and status = 'SENT'";
  $result = mysql_query($query);
        if($result)
  {
    $info['archive'] = mysql_result($result, 0, 0);
  }
  return $info;
}


// ham nhan tin cua danh sach tro den listid
function get_list_name($listid)
{
  if(!$listid)
    return false;

  if(!db_connect())
    return false;

  $query = "select listname from lists where listid = $listid";
  $result = mysql_query($query);
        if(!$result)
  {
    return false;
  }
  return mysql_result($result, 0);
}
// them mot danh sach moi vao CSDL
function store_list($admin_user, $details)
{
  if(!filled_out($details))
  {
    echo 'Tat ca cac truong phai duoc dien day du. Thu lai lan nua.<br /><br />';
    return false;
  }
  else
  {
    if(!check_admin_user($admin_user))
      return false;  

    if(!db_connect())
    { 
      return false;
    }
    
    $query = "select count(*) from lists where listname = '".$details['name']."'";
    $result = mysql_query($query);
    if(mysql_result($result, 0, 0) > 0)
    {
      echo 'Loi, ten danh sach nay da ton tai.';
      return false;
    }
    
    $query = "insert into lists values (NULL, 
                                       '".$details['name']."',
                                       '".$details['blurb']."')";

    $result = mysql_query($query);
    return $result; 
  }
}

// ham nhan danh sach ma user dang ky
function get_subscribed_lists($email)
{
  $list = array();  

  $query = "select lists.listid, listname from sub_lists, lists 
            where email='$email' and lists.listid = sub_lists.listid 
            order by listname";

  if(db_connect())
  {
    $result = mysql_query($query);
    if(!$result)
      echo '<p>Khong the lay danh sach tu CSDL.';
    $num = mysql_numrows($result);
    for($i = 0; $i<$num; $i++)
    {
      array_push($list, array(mysql_result($result, $i, 0),
                              mysql_result($result, $i, 1)));
    }
  }
  return $list;
}

//nhan danh sach ma user huy bo
function get_unsubscribed_lists($email)
{
  $list = array();  

  $query = "select lists.listid, listname, email from lists left join sub_lists 
                   on lists.listid = sub_lists.listid 
                   and email='$email' where email is NULL order by listname";
  if(db_connect())
  {
    $result = mysql_query($query);
    if(!$result)
      echo '<p>Khong the lay danh sach tu CSDL.';
    $num = mysql_numrows($result);
    for($i = 0; $i<$num; $i++)
    {
      array_push($list, array(mysql_result($result, $i, 0), 
                 mysql_result($result, $i, 1)));
    }
  }
  return $list;
}

// nhan tat ca danh sach
function get_all_lists()
{
  $list = array();  

  $query = 'select listid, listname from lists order by listname';

  if(db_connect())
  {
    $result = mysql_query($query);
    if(!$result)
      echo "<p>Khong the lay danh sach tu CSDL - $query.";
    $num = mysql_numrows($result);
    for($i = 0; $i<$num; $i++)
    {
      array_push($list, array(mysql_result($result, $i, 0), 
                              mysql_result($result, $i, 1)));
    }
  }
  return $list;
} 

function get_archive($listid)
{
  //tra ve mot day cac tin luu tru cho danh sach chon

  $list = array();  
  $listname = get_list_name($listid);
  
  $query = "select mailid, subject, listid from mail 
            where listid = $listid and status = 'SENT' order by sent"; 

  if(db_connect())
  {
    $result = mysql_query($query);
    if(!$result)
    {
      echo "<p>Khong the lay danh sach tu CSDL - $query.";
      return false;
    }
    $num = mysql_numrows($result);
    for($i = 0; $i<$num; $i++)
    {
      $row = array(mysql_result($result, $i, 0), 
                   mysql_result($result, $i, 1), $listname, $listid); 
      array_push($list, $row);
    }
  }
  return $list;
} 

// nhan danh sach cua mail duoc tao nhung chua gui
function get_unsent_mail($email)
{
  if(!check_admin_user($email))
  {
    return false;
  }
  
  $list = array();  

  $query = "select mailid, subject, listid from mail 
            where status = 'STORED' or status = 'TESTED' order by modified"; 

  if(db_connect())
  {
    $result = mysql_query($query);
    if(!$result)
    {
      echo '<p>Khong the lay danh sach tu CSDL.';
      return false;
    }
    $num = mysql_numrows($result);
    for($i = 0; $i<$num; $i++)
    {
      array_push($list, array(mysql_result($result, $i, 0), 
                              mysql_result($result, $i, 1), 
                              get_list_name(mysql_result($result, $i, 2)),
                              mysql_result($result, $i, 2)
                              )
                 );
    }
  }
  return $list;
} 

// them mot user dang ky moi vao CSDL hoac user thay doi thong tin tai khoan
function store_account($normal_user, $admin_user, $details)
{
  if(!filled_out($details))
  {
    echo 'Tat ca cac truong phai duoc dien day du. Thu lai lan nua.<br /><br />';
    return false;
  }
  else
  {
    if(subscriber_exists($details['email']))
    {
      if(get_email()==$details['email'])
      {
        $query = "update subscribers set realname = '$details[realname]',
                                         mimetype = '$details[mimetype]'
                  where email = '" . $details[email] . "'";
        if(db_connect() && mysql_query($query))
        {
          return true;
        }
        else
        {
          echo 'Khong the luu cac thay doi.<br /><br /><br /><br /><br /><br />';
          return false;
        }
      }
      else
      {
        echo '<p>Loi, dia chi email nay da duoc dang ky.';
        echo '<p>Ban can dang nhap voi dia chi email nay de thay doi cac thong tin.';
        return false;                 
      }      
    }
    else // tai khoan moi
    {
      $query = "insert into subscribers 
                        values ('$details[email]',
                        '$details[realname]',
                        '$details[mimetype]',
                         password('$details[new_password]'),
                                                0)";          
      if(db_connect() && mysql_query($query))
      {
        return true;
      }
      else
      {
        echo 'Khong the luu lai tai khoan moi.<br /><br /><br /><br /><br /><br />';
        return false;
      }
    }
  }
}
//gui thu tin
function send($mailid, $admin_user)
{
  if(!check_admin_user($admin_user))
    return false;
  
  if(!($info = load_mail_info($mailid)))
  { 
    echo "Khong the lay thong tin cho message $mailid";
    return false;
  }
  $subject = $info[0];
  $listid = $info[1];
  $status = $info[2];
  $sent = $info[3];
    
  $from_name = 'Newsletter';
      
  $from_address = 'return@address';
    
  $query = "select email from sub_lists where listid = $listid";
  
  $result = mysql_query($query);
  if (!$result)
  {
    echo $query;
    return false;  
  }
  else if (mysql_num_rows($result)==0)
  {
    echo "Hien tai chua co dang ky nao cho danh sach nay $listid";
    return false; 
  }
  else
  {
    // trieu goi lop mail trong thu vien PEAR
    include('Mail.php');
    include('Mail/mime.php');

    $message = new Mail_mime("\r\n");

    $textfilename = "archive/$listid/$mailid/text.txt";
    $tfp = fopen($textfilename, "r");
    $text = fread($tfp, filesize($textfilename));
    fclose($tfp);

    $htmlfilename = "archive/$listid/$mailid/index.html";
    $hfp = fopen($htmlfilename, "r");
    $html = fread($hfp, filesize($htmlfilename));
    fclose($hfp);

    $message->setTXTBody($text);
    $message->setHTMLBody($html);

    // nhan cac hinh anh lien quan cua message
    $query = "select path, mimetype from images where mailid = $mailid";
    if(db_connect())
    {
      $result = mysql_query($query);
      if(!$result)
      {
        echo '<p>Khong the lay hinh tu CSDL.';
        return false;
      }
      $num = mysql_numrows($result);
      for($i = 0; $i<$num; $i++)
      {  
        //nap tung hinh mot tu dia
        $imgfilename = "archive/$listid/$mailid/".mysql_result($result, $i, 0);
        $imgtype = mysql_result($result, $i, 1);

        // them tung hinh mot vao trong doi tuong
        $message->addHTMLImage($imgfilename, $imgtype, $imgfilename, true);
      }
    }  
    
    // tao body cua message
    $body = $message->get();  

    // tao header cua message
    $from = '"'.get_real_name($admin_user).'" <'.$admin_user.'>';
    $hdrarray = array(              
                 'From'    => $from,
                 'Subject' => $subject);

    $hdrs = $message->headers($hdrarray);

    $sender =& Mail::factory('mail');
 
    if($status == 'STORED')
    {
      // gui message HTML den admin
      $sender->send($admin_user, $hdrs, $body);
      
      mail($admin_user, $subject, $text, 'From: "'.get_real_name($admin_user).'" <'.$admin_user.">");

      echo "Tin duoc gui den $admin_user";

      $query = "update mail set status = 'TESTED' where mailid = $mailid";
      if(db_connect())
      {
        $result = mysql_query($query);
      }    
    
      echo '<p>Nhan send lan nua de gui tin den cac user da dang ky.<center>';
      display_button('send', "&id=$mailid");
      echo '</center>';
    }    
    else if($status == 'TESTED')
    {
      //gui message den danh sach dang ky
    
      $query = "select subscribers.realname, sub_lists.email, 
                       subscribers.mimetype  
                from sub_lists, subscribers 
                where listid = $listid and 
                      sub_lists.email = subscribers.email";
                      
      if(!db_connect())
        return false;
    
      $result = mysql_query($query);
      if(!$result)
        echo '<p>Loi nhan danh sach user dang ky';
      
      $count = 0;      
      // cho moi user dang ky
      while( $subscriber = mysql_fetch_row($result) )
      {
        if($subscriber[2]=='H')
        {
          //gui message HTML den user dang ky HTML
          $sender->send($subscriber[1], $hdrs, $body);
        }
        else
        {
          //gui message text den user khong dang ky HTML
          mail($subscriber[1], $subject, $text, 
                         'From: "'.get_real_name($admin_user).'" <'.$admin_user.">");
        }
        $count++; 
      }
        
      $query = "update mail set status = 'SENT', sent = now() 
                where mailid = $mailid";
      if(db_connect())
      {
        $result = mysql_query($query);
      }
      echo "<p>Tong cong $count tin da duoc gui.";
    }
    else if($status == 'SENT')
    {
      echo '<p>Tin nay da duoc gui.';
    }
  }
}
