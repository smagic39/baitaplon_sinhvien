<?php

// Tien xu ly
  include ('include_fns.php');
  session_start();

  $action = $HTTP_GET_VARS['action'];
  $buttons = array();

  $status = '';
   
  if($HTTP_POST_VARS['email']&&$HTTP_POST_VARS['password'])
  {
    $login = login($HTTP_POST_VARS['email'], $HTTP_POST_VARS['password']);
    
    if($login == 'admin')
    {
      $status .= "<p><b>".get_real_name($HTTP_POST_VARS['email']).
                 "</b> dang nhap"." thanh cong </p>
                  <br /><br /><br /><br /><br />";
      $HTTP_SESSION_VARS['admin_user'] = $HTTP_POST_VARS['email'];
    }
    else if($login == 'normal')
    {
      $status .= "<p><b>".get_real_name($HTTP_POST_VARS['email'])."</b> dang nhap"
                 ." thanh cong.</p><br /><br />";
      $HTTP_SESSION_VARS['normal_user'] = $HTTP_POST_VARS['email'];
    }
    else
    {
      $status .= "<p>Loi dang nhp, sai dia chi email hoac mat khau.</p><br />";
    }
  }

  if($action == 'log-out')
  {
    unset($action);
    unset($HTTP_SESSION_VARS);
    session_destroy();
  }

// Thiet lap header

  // thiet lap cac nut chuc nang sau khi dang nhap
  if(check_normal_user())
  {
    // doi voi user
    $buttons[0] = 'change-password';
    $buttons[1] = 'account-settings';
    $buttons[2] = 'show-my-lists';
    $buttons[3] = 'show-other-lists';
    $buttons[4] = 'log-out'; 
  }
  else if(check_admin_user())
  {
    // doi voi admin
    $buttons[0] = 'change-password';
    $buttons[1] = 'create-list';
    $buttons[2] = 'create-mail';
    $buttons[3] = 'view-mail';
    $buttons[4] = 'log-out';
    $buttons[5] = 'show-all-lists';
    $buttons[6] = 'show-my-lists';
    $buttons[7] = 'show-other-lists';
  }
  else
  {
    // chua dang nhap
    $buttons[0] = 'new-account'; 
    $buttons[1] = 'show-all-lists';
    $buttons[4] = 'log-in';
  }
  
  if($action)
  {
    // hien thi header va mo ta trang ung dung
    do_html_header('Newsletter '.format_action($action));
  }
  else
  {
    do_html_header('Newsletter ');
  }
  
  display_toolbar($buttons);
  
  echo $status;

// Thuc hien cong viec sau khi dang nhap thanh cong


  switch ( $action )
  {
    case 'new-account' :
    {
      session_destroy();
      display_account_form();   
      break;
    }
    case 'store-account' :
    {
      if (store_account($HTTP_SESSION_VARS['normal_user'], 
               $HTTP_SESSION_VARS['admin_user'], $HTTP_POST_VARS))
        $action = '';
      if(!check_logged_in())
        display_login_form($action);
      break;
    }
    case 'log-in' :
    case '':
    {
      if(!check_logged_in())
        display_login_form($action);
      break;
    }
    case 'show-all-lists' :
    {
      display_items('Tat ca danh sach', get_all_lists(), 'information',
                    'show-archive','');
      break;
    }
    case 'show-archive' :
    {
      display_items('Luu tru cho '.get_list_name($HTTP_GET_VARS['id']),
                     get_archive($HTTP_GET_VARS['id']), 'view-html', 
                    'view-text', '');
      break;
    }
    case 'information' :
    {
      display_information($HTTP_GET_VARS['id']);
      break;
    }
    
  }
  
  //user thuc hien cong viec sau khi dang nhap
  if(check_logged_in())
  {
    switch ( $action )
    {
      case 'account-settings' :
      {
        display_account_form(get_email(), 
              get_real_name(get_email()), get_mimetype(get_email()));   
        break;
      }
      case 'show-other-lists' :
      {
        display_items('Danh sach chua dang ky',
                      get_unsubscribed_lists(get_email()), 'information', 
                      'show-archive', 'subscribe');
        break;
      }
      case 'subscribe' :
      {
        subscribe(get_email(), $HTTP_GET_VARS['id']);
        display_items('Danh sach dang ky', get_subscribed_lists(get_email()),
                        'information', 'show-archive', 'unsubscribe');
        break;
      }
      case 'unsubscribe' :
      {
        unsubscribe(get_email(), $HTTP_GET_VARS['id']);
        display_items('Danh sach dang ky', get_subscribed_lists(get_email()),
                      'information', 'show-archive', 'unsubscribe');
        break;
      }
      case '':
      case 'show-my-lists' :
      {               
        display_items('Danh sach dang ky', get_subscribed_lists(get_email()),
                     'information', 'show-archive', 'unsubscribe');
        break;
      }
      case 'change-password' :
      {               
        display_password_form();
        break;
      }
      case 'store-change-password' :
      {               
        if(change_password(get_email(), $HTTP_POST_VARS['old_passwd'], 
           $HTTP_POST_VARS['new_passwd'], $HTTP_POST_VARS['new_passwd2']))
        {
          echo '<p>Mat khau da duoc thay doi.</p>
                <br /><br /><br /><br /><br /><br />';
        }
        else
        {
          echo '<p>Loi, mat khau khong the thay doi.</p>';
          display_password_form();
        } 
        break;
      }
    }
  }
  // admin thuc hien cong viec
  if(check_admin_user())
  {
    switch ( $action )
    {
      case 'create-mail' :
      {
        display_mail_form(get_email());   
        break;
      }
      case 'create-list' :
      {
        display_list_form(get_email());   
        break;
      }
      case 'store-list' :
      {
        if(store_list($HTTP_SESSION_VARS['admin_user'], $HTTP_POST_VARS))
        {
          echo '<p>Them danh sach moi</p><br />';
          display_items('Tat ca danh sach', get_all_lists(), 'information',
                        'show-archive','');
        }
        else     
          echo '<p>Danh sach khong the luu, thu lai lan nua '
               .'</p><br /><br /><br /><br /><br />';
                
        break;
      }
      case 'send' :
      {
        send($HTTP_GET_VARS['id'], $HTTP_SESSION_VARS['admin_user']);   
        break;
      }
      case 'view-mail' :
      {
        display_items('Tin chua gui', get_unsent_mail(get_email()),
                      'preview-html', 'preview-text', 'send');
        break;
      }
    }
  } 

// hien thi footer
  do_html_footer();
?> 
