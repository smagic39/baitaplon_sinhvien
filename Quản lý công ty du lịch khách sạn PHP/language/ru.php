<?php
$lang=array();

$lang['DATE_FORMAT'] = 'n-j-Y';
$lang['language']='Язык';

//header
$lang['home']='Главная';
$lang['introduction']='Введение';
$lang['gallery']='Фото галерея';
$lang['news']='Новости';
$lang['event']='Акции';
$lang['contact']='Контакт';

$lang['search']='Tìm kiếm';
$lang['google_search']='Tìm kiếm trên Google';
$lang['online_support']='Поддержка';//Онлайн поддержка
$lang['advertisement']='Quảng cáo';

$lang['visitors']='Lượt truy cập';
$lang['visitorsonline']='Online';
$lang['weblink']='Liên kết website';
$lang['booking_online']='Бронирование онлайн';

//booking
$lang['roomtypes']='НОМЕРЫ';
$lang['roomrates']='ЦЕНЫ НА НОМЕРА';
$lang['booking']='БРОНЬ';
$lang['personal_info']='Ваша информация';
$lang['roomtype']='Категории номеров';
$lang['select']='Выберите';
$lang['book_info']='Информация о бронировании';
$lang['arrival_date']='День прибытия';
$lang['departure_date']='День убытия';
$lang['rooms']='Количество номеров';
$lang['adults']='Количество взрослых';
$lang['child']='Количество детей';
$lang['price_info']='Оценить Диапазон';
$lang['from_rate']='Из Оценить';
$lang['to_rate']='Для оценки';
$lang['other_info']='Другие просит';
$lang['booksuc']='Спасибо за ваш заказ.<br>Наш персонал свяжется с Вами как можно скорее.';
$lang['error_roomtype']='Пожалуйста, выберите категории номеров.';
$lang['error_arrival_date']='Пожалуйста, введите день прибытия.';
$lang['error_rooms']='Пожалуйста, введите Количество номеров.';

//more
$lang['send']='Отправить';
$lang['save']='Lưu';
$lang['reset']='Сброс';
$lang['submit']='Chấp nhận';
$lang['cancel']="Hủy bỏ";
$lang['continue']="Tiếp tục";
$lang['finish']="Hoàn tất";
$lang['top']='Начало страницы';
$lang['back']='Về trang trước';
$lang['field']='необходимую информацию';
$lang['error_field']='Vui lòng nhập đầy đủ các thông tin bắt buộc.';
$lang['validfield']='Vui lòng không nhập < và >.';
$lang['sendsuc']='<b>Chân thành cảm ơn.</b><br>Thông tin yêu cầu của bạn đã được gửi đến chúng tôi.<br>Chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất.';

$lang['othernews']='Другие Новости';
$lang['otherarticles']='Các bài viết khác';

$lang['search_results']='Kết quả tìm kiếm';
$lang['updating']='Đang cập nhật';
$lang['detail']='подробно';
$lang['viewmore']='Xem tiếp';
$lang['viewall']='Xem tất cả';

//contact
$lang['send_email']='Отправить письмо';
$lang['name']="Полное имя";
$lang['firstname']='Первый Название  ';
$lang['middlename']='Ближний Название';
$lang['lastname']='Фамилия';
$lang['company']='Компания';
$lang['address']="Адрес";
$lang['city']="Город";
$lang['country']="Страна";
$lang['tel']='Телефон';
$lang['email']='Электронная почта';
$lang['subject']='Тема';
$lang['content']='Содержание';
$lang['error_name']='Пожалуйста, введите полное имя.';
$lang['error_firstname']='Пожалуйста, введите первый название.';
$lang['error_lastname']='Пожалуйста, введите фамилия название.';
$lang['error_email']='Пожалуйста, введите адрес электронной почты.';
$lang['invalidemail']='К сожалению, недействительными по электронной почте.';
$lang['error_company']='Пожалуйста, введите компании.';
$lang['error_address']='Пожалуйста, введите адрес.';
$lang['error_city']='Пожалуйста, введите город.';
$lang['error_country']='Пожалуйста, выберите страна.';
$lang['error_tel']='Пожалуйста, введите телефон';
$lang['error_subject']='Пожалуйста, введите тему.';
$lang['error_content']='Пожалуйста, введите содержание.';
$lang['sended_email']='Ваш электронный адрес был отправлен к нам.';

?>