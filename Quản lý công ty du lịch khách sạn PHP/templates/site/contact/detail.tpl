			<div id="contentleft">
      	<? include("./templates/site/top_main.tpl"); ?>
        <div id="contentleft_cell">
        	<div class="title1"><?=$lang['contact']?></div>
          <div class="space_content"><?=stripslashes($intro['content'])?></div>
          
          <div id="line2"><img src="images/spacer.gif" /></div>
          
          <div class="title1"><?=$lang['send_email']?></div>
          <div class="space_content">
            <? if ($_GET['send']){?>
            <div align="center" class="red12"><?=$lang['sended_email']?></div>
            <div id="line4"><img src="images/spacer.gif" /></div>	
            <? } ?>					
            <div class="black11"><span class="red11">(*)</span> <?=$lang['field']?></div>
            <div id="line4"><img src="images/spacer.gif" /></div>		
            <div>
              <form name="frmcontact" action="?do=contact&act=contactsm" method="post" onSubmit="return nospace(name, '<?=$lang['error_name']?>') && nospace(email, '<?=$lang['error_email']?>') && goodEMail(email, '<?=$lang['invalidemail']?>') && nospace(message, '<?=$lang['error_content']?>');" style="margin:0px;">
              <table border="0" cellspacing="0" cellpadding="3">
                <tr>
                  <td width="150" align="right"><span class="red11">(*)</span> <?=$lang['name']?> :</td>
                  <td><input type="text" name="name" class="box"></td>
                </tr>
                <tr>
                  <td align="right"><?=$lang['company']?> :</td>
                  <td><input type="text" name="company" class="box"></td>
                </tr>
                <tr>
                  <td align="right"><?=$lang['address']?> :</td>
                  <td><input type="text" name="address" class="box"></td>
                </tr>
                <tr>
                  <td align="right"><?=$lang['tel']?> :</td>
                  <td><input type="text" name="tel" class="box"></td>
                </tr>
                <tr>
                  <td align="right"><span class="red11">(*)</span> <?=$lang['email']?> :</td>
                  <td><input type="text" name="email" class="box"></td>
                </tr>
                <tr>
                  <td align="right" valign="top"><span class="red11">(*)</span> <?=$lang['content']?> :</td>
                  <td><textarea name="message" rows="8" class="box"></textarea></td>
                </tr>
                <tr><td height="5"></td></tr>
                <tr>
                  <td></td>
                  <td>
                  <input type="hidden" name="toemail" value="<?=stripslashes($intro['email'])?>">
                  <input type="submit" value=" <?=$lang['send']?> " class="button">
                  <input type="reset" value=" <?=$lang['reset']?> " class="button">
                  </td>
                </tr>
              </table>
              </form>
            </div>
          </div>
          
          <div id="line1"><img src="images/spacer.gif" /></div>
            
          <div align="right"><img src="images/top.gif" align="absmiddle"> <a href="#top" class="green11"><?=$lang['top']?></a></div>	 
        </div>
			</div>