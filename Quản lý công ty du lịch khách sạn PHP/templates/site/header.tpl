<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<TITLE>SaiGon NinhChu Hotel</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="SaiGon NinhChu Hotel - Khách sạn Sài Gòn Ninh Chữ" />
<meta name="keywords" content="du lich, khach san, phan rang, ninh thuan, sai gon, ninh chu, hotel, tour" />
<meta name="Author" content="Viet Arrow software company" />
<meta name="robots" content="index,follow" />
<!--[if lt IE 7.]><script defer type="text/javascript" src="css/pngfix.js"></script><![endif]-->
<!--[if lt IE 6.]><script defer type="text/javascript" src="css/pngfix.js"></script><![endif]-->
<!--[if lt IE 5.]><script defer type="text/javascript" src="css/pngfix.js"></script><![endif]-->
<link href="css/main_styles.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="includes/check_form.js"></script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>

<? if ($do=='main') { ?>
<!--slide img-->
<link type="text/css" href="slide/styles.css" rel="stylesheet" />
<script type="text/javascript" src="slide/jquery.js"></script>
<script type="text/javascript" src="slide/jquery.pikachoose.js"></script>
<script type="text/javascript" src="slide/jquery.jcarousel.min.js"></script>
<script language="javascript">
<!--
$(document).ready(
	function (){
		$("#pikame").PikaChoose();
		$("#pikame").jcarousel({scroll:4,					
			initCallback: function(carousel) {
				$(carousel.list).find('img').click(function() {
					//console.log($(this).parents('.jcarousel-item').attr('jcarouselindex'));
					carousel.scroll(parseInt($(this).parents('.jcarousel-item').attr('jcarouselindex')));
				});
			}
		});
	});
-->
</script>
<!--end slide img-->
<? } ?>

<? if ($do=='gallery') { ?>
<!--box img-->
<script type="text/javascript" src="slidebox/highslide.js"></script>
<link rel="stylesheet" type="text/css" href="slide/highslide.css" />
<script type="text/javascript">
	hs.graphicsDir = 'slidebox/graphics/';
	hs.wrapperClassName = 'wide-border';
</script>
<!--end box img-->
<? } ?>

<? if ($do=='roomtypes') { ?>
<!--light box img-->
<script type="text/javascript" src="lightbox/mootools-yui-compressed.js"></script>
<script type="text/javascript" src="lightbox/sexylightbox.v2.3.mootools.min.js"></script>
<link rel="stylesheet" type="text/css" href="lightbox/sexylightbox.css" media="all" />
<script type="text/javascript">
	window.addEvent('domready', function(){
		SexyLightbox = new SexyLightBox({color:'white', dir: 'lightbox/sexyimages'});
	});
</script>
<style>
  .alertbox {
    background  : url(lightbox/images/dialog-help.png) no-repeat scroll left top;
    margin      : 0 10px;
    padding     : 0 0 5px 40px;
    font-family : Verdana;
    font-size   : 12px;
    text-align  : left;
  }
  .alertbox .buttons {
    text-align  : right;
  }
</style>
<!--end light box img-->
<? } ?>

<? if ($do=='booking') { ?>
<!--calendar-->
<script type="text/javascript" src="selectdate/calendar_us.js"></script>
<link rel="stylesheet" href="selectdate/calendar.css">
<!--end calendar-->
<? } ?>
</head>
<body>
<script type="text/javascript" src="js/tooltip.js"></script>
<div id="body_wrapper">
<div id="wrapper">
<a name="top"></a>
	<div><script type="text/javascript">AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0','width','968','height','255','src','flash/banner','quality','high','wmode','transparent','pluginspage','http://www.macromedia.com/go/getflashplayer','movie','flash/banner' ); //end AC code
</script><noscript><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="968" height="255">
    <param name="movie" value="flash/banner.swf" />
    <param name="quality" value="high" />
    <param name="wmode" value="transparent" />
    <embed src="flash/banner.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="968" height="255" wmode="transparent"></embed></object></noscript></div>  
		
	<div id="Nav">
		<div id="Nav_cell"><a href="index.php" class="menu<? if ($do=='main') echo '_o';?>"><?=$lang['home']?></a></div>
		<div id="Nav_line"><img src="images/spacer.gif" /></div>
		<div id="Nav_cell"><a href="?do=intro&id=5" class="menu<? if ($do=='intro' && $intro['id']==5) echo '_o';?>"><?=$lang['introduction']?></a></div>
		<div id="Nav_line"><img src="images/spacer.gif" /></div>
		<div id="Nav_cell"><a href="?do=gallery" class="menu<? if ($do=='gallery') echo '_o';?>"><?=$lang['gallery']?></a></div>
		<div id="Nav_line"><img src="images/spacer.gif" /></div>
		<div id="Nav_cell"><a href="?do=news" class="menu<? if ($do=='news') echo '_o';?>"><?=$lang['news']?></a></div>
		<div id="Nav_line"><img src="images/spacer.gif" /></div>
		<div id="Nav_cell"><a href="?do=intro&id=7" class="menu<? if ($do=='intro' && $intro['id']==7) echo '_o';?>"><?=$lang['event']?></a></div>
		<div id="Nav_line"><img src="images/spacer.gif" /></div>
		<div id="Nav_cell"><a href="?do=contact" class="menu<? if ($do=='contact') echo '_o';?>"><?=$lang['contact']?></a></div>
    <div id="Nav_line"><img src="images/spacer.gif" /></div>
		<div id="right" style="padding:10px 7px 0px 0px;"><a href="?<?=$link?>&lang=vn"><img src="images/vn.gif" border="0"></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="?<?=$link?>&lang=en"><img src="images/en.gif" border="0"></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="?<?=$link?>&lang=ru"><img src="images/ru.gif" border="0"></a></div>
	</div>
	
	<div id="clear"></div>	
	
	<div id="content">
		<div id="contentcell">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
      <td valign="top">      