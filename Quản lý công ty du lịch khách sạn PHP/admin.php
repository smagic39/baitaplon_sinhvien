<?php
session_start();
include("./includes/config.php");
include("./includes/functions.php");
include("./includes/va_db.php");

$do   = isset($_GET["do"])  ? $_GET["do"] :'main';
$act   = isset($_GET["act"])  ? $_GET["act"]  : "";
$page   = isset($_GET["page"])  ? $_GET["page"]  : '1';//for paging

if (!isset($_SESSION["admin_login"])) $do="login";
if (file_exists("./sources/admin/".$do.".php"))
	require("./sources/admin/".$do.".php");
else {
	$msg="Không có chức năng này.";
	$p='./admin.php';
	page_transfer($msg,$p);
}

$DATE_FORMAT = 'd-m-Y';

//display html
include("./templates/admin/header.tpl");
if ($do != "login") {
	require_once("./sources/admin/menu.php");
	include("./templates/admin/".$do."/menu.tpl");
}
include("./templates/admin/".$tpl.".tpl");
include("./templates/admin/footer.tpl");

?>