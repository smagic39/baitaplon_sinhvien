<?php
/**
* YOOgallery Core
*
* @author    yootheme.com
* @copyright Copyright (C) 2008 YOOtheme. All rights reserved.
* @license	 GNU/GPL
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

class YOOThumbnail {

	var $img_file;
	var $img_format;
	var $img_source;
	var $img_width;
	var $img_height;
	var $thumb_width;
	var $thumb_height;
	var $thumb_resize;
	var $thumb_quality;

    function YOOThumbnail($file) {
        $this->img_file      = $file;
        $this->thumb_resize  = true;
        $this->thumb_quality = 90;

		// get image info
		list($width, $height, $type, $attr) = @getimagesize($this->img_file, $info);

		// set image dimensions and type
		if (is_array($info)) {

	        $this->img_width    = $width;
	        $this->img_height   = $height;
	        $this->thumb_width  = $width;
	        $this->thumb_height = $height;
	
			switch ($type) {
	            case 1:
	                $this->img_format = 'gif';
	            	$this->img_source = imagecreatefromgif($this->img_file);
	                break;
	            case 2:
                	$this->img_format = 'jpeg';
	            	$this->img_source = imagecreatefromjpeg($this->img_file);
	                break;
	            case 3:
            		$this->img_format = 'png';
	            	$this->img_source = imagecreatefrompng($this->img_file);
	                break;
	            default:
	                $this->img_format = null;
	            	$this->img_source = null;
	                break;
	        } 
		}		
    } 

    function setResize($resize) {
		$this->thumb_resize = $resize;
    } 

    function setSize($width, $height) {
		$this->thumb_width  = $width;
		$this->thumb_height = $height;
    } 

    function sizeWidth($width) { 
		$this->thumb_width  = $width;
		$this->thumb_height = @($width / $this->img_width) * $this->img_height;
    } 

    function sizeHeight($height) {
		$this->thumb_width  = @($height / $this->img_height) * $this->img_width;
		$this->thumb_height = $height;
    } 

    function save($file) { 
		$return = false;
		
		if ($this->img_format) {
	        // create thumbnail image
			$thumbnail = imagecreatetruecolor($this->thumb_width, $this->thumb_height);

	        // copy source image to thumbnail image
			if ($this->thumb_resize) {
				if (function_exists('imagecopyresampled')) {
					@imagecopyresampled($thumbnail, $this->img_source, 0, 0, 0, 0, $this->thumb_width, $this->thumb_height, $this->img_width, $this->img_height);
				} else {
					@imagecopyresized($thumbnail, $this->img_source, 0, 0, 0, 0, $this->thumb_width, $this->thumb_height, $this->img_width, $this->img_height);
				}
			} else {
				@imagecopy($thumbnail, $this->img_source, 0, 0, 0, 0, $this->thumb_width, $this->thumb_height);
			}

			// save thumbnail to file
			switch ($this->img_format) {
	            case 'gif':
	            	$return = imagegif($thumbnail, $file);
	                break;
	            case 'jpeg':
	       			$return = imagejpeg($thumbnail, $file, $this->thumb_quality);
	                break;
	            case 'png':
	    			$return = imagepng($thumbnail, $file);
					break;
	        } 
		}

		return $return;
    } 

    function check() {
		$gd_functions = array(
			'getimagesize',
			'imagecreatefromgif',
			'imagecreatefromjpeg',
			'imagecreatefrompng',
			'imagecreatetruecolor',
			'imagecopyresized',
			'imagecopy',
			'imagegif',
			'imagejpeg',
			'imagepng'
			);
		
		foreach ($gd_functions as $name) {
			if (!function_exists($name)) return false;
		}
		
		return true;
    } 

} 

?>