<?php
/**
 * @version $Id: extranews.php  2008-05-05 18:42 
 * @ Viet Nguyen Hoang - viet4777 - www.xahoihoctap.net & www.luyenkim.net $
 * @package     Joomla
 * @subpackage Content
 * @copyright  Copyright (C) 2005 - 2007 Open Source Matters. All rights reserved.
 * @license    GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 * 
 * Thanks Peter vd Hulst (petervanderhulst@hotmail.com) for correct DHTML display  
 */
// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

$enabled = JPluginHelper :: isEnabled  ('content','extranews');   
/**
 * Content Plugin
 *
 * @package    Joomla
 * @subpackage Content
 * @since      1.5
 */

class plgContentExtranews extends JPlugin
{
	var $type, $term, $IsIphone = false, $extranewsOn, $catid, $sectionid;
	var $doc;
   /**
    * Constructor
    *
    * For php4 compatability we must not use the __constructor as a constructor for plugins
    * because func_get_args ( void ) returns a copy of all passed arguments NOT references.
    * This causes problems with cross-referencing necessary for the observer design pattern.
    *
    * @param object $subject The object to observe
    * @param object $params  The object that holds the plugin parameters
    * @since 1.5
    */
   function plgContentExtranews( &$subject, $params )
   {  
      //$this->_plugin =& JPluginHelper::getPlugin('content','extranews');
      //$this->params =& new JParameter( $this->_plugin->params );
        // Joomla's plugin-installer does not handle separate language files for
        // backend and frontend if installing a plugin (kind of silly, as it works
        // with components). Instead, it installs the language files always into the
        // JPATH_ADMINISTRATOR/languages. Therefore we have to specify this path
        // explicitely.
        //JPlugin::loadLanguage('plg_content_extranews', JPATH_ADMINISTRATOR);   //Call language file 
        if (JPlugin::loadLanguage('plg_content_extranews')) ;  //Call language file
        else JPlugin::loadLanguage('plg_content_extranews', JPATH_ADMINISTRATOR);
		parent::__construct($subject, $params);
		$this->doc = & JFactory::getDocument();
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strpos($_SERVER['HTTP_USER_AGENT'], 'iPod')) $this->IsIphone = true;
		$this->extranewsOn = true;
		$this->type = 'n';
		$this->catid = -1;
		$this->term = null;		
        //JPlugin::loadLanguage('plg_content_extranews',JPATH_ROOT . DS . 'plugins'. DS .'content'. DS .'extranews'. DS .'i18n'. DS ); //Call language file        
		//JURI::base() va JURI::base(true),
		//JURI::base() -> http://xahoihoctap.net/webs/joomla15/
		//JURI::base(true) -> /webs/joomla15/
   }

   /**
    * prepare content method
    *
    * Method is called by the view
    *
    * @param   object      The article object.  Note $article->text is also available
    * @param   object      The article params
    * @param   int         The 'page' number
    */
	function onPrepareContent( &$article, &$params, $limitstart )
	{
		global $mainframe;

	}	
function onAfterDisplayContent ( &$article, &$params, $limitstart ) //onAfterDisplayContent onPrepareContent
   {
		$db      =& JFactory::getDBO(); //Ket noi CSDL
		$user    =& JFactory::getUser(); //Thong tin user
		$option  = JRequest::getCmd('option'); //JRequest::getCmd
		$view    = JRequest::getCmd('view'); //JRequest::getCmd

		$config =& JFactory::getConfig();
		$offset = $config->getValue('config.offset');
		$aid     = $user->get('aid', 0);
		//$datetool = & new JDate();//strtotime("now"),
		//$datetool->setoffset($offset);
		//$content = ''.$datetool->toFormat()." ".$offset."<br/>".strtotime("now")."<br/>";
		$plugin_enabled = $this->params->get('enabled','1');
	if($option=="com_content" && $view=="article"){
         
/******************Plugin Parameters*******************************/   
		$regex = "#{extranews\s*(.*?)}(.*?){/extranews}#s";
		if($this->doc->gettype()=='pdf' || !$plugin_enabled){
			$article->text = preg_replace($regex, '', $article->text);
			return;
		}
/*Check allowing plugin redering*/
	$catid_list= $this->params->get( 'catid_list','-');
	$sectionid_list= $this->params->get( 'sectionid_list','-');
	$id_list= $this->params->get( 'id_list','-');

	$itemid = @JSite::getMenu()->getActive()->id;
	if(!is_array($catid_list)) $catid_list = array($catid_list);
		if(!is_array($sectionid_list)) $sectionid_list = array($sectionid_list);
			if(!is_array($id_list)) $id_list = array($id_list);
			
	$rendering = in_array('-',$catid_list, true) || 
		in_array($article->catid,$catid_list, true) ||
			in_array('-',$sectionid_list, true) || 
			in_array($article->sectionid,$sectionid_list, true) ||
				in_array('-',$id_list, true) || 
				in_array($itemid,$id_list, true);
			
	if($this->params->get( 'chooseoutput') == 0) $rendering = !$rendering;
	if(!$rendering) return;
		$option     = JRequest::getCmd('option'); //JRequest::getCmd
		$view    = JRequest::getCmd('view'); //JRequest::getCmd
		$plugin_enabled = $this->params->get('enabled','1');
		
/*Check allowing plugin redering*/
	
		$this->extranewsOn = true;
			//Category
			$render_method = $this->params->get("render_method", 0);
			$render_cat = $this->params->get("render_cat", null);		
			if($render_method && $render_cat) {
				if(!is_array($render_cat)) $render_cat = array($render_cat);
				if($render_method == 1) $this->extranewsOn = in_array($article->catid, $render_cat);
				else $this->extranewsOn = !in_array($article->catid, $render_cat);
			}
		
			if (preg_match_all($regex, $article->text, $matches, PREG_SET_ORDER) > 0) {
				foreach ($matches as $match) {
					$match[2] = trim(strip_tags($match[2]));
					$theparams = & JUtility::parseAttributes(htmlspecialchars_decode(trim(strip_tags($match[1])))); //parameters
					$nitems = isset($theparams["items"])?$theparams["items"]:1;
					$title = isset($theparams["title"])?$theparams["title"]:'';
					$this->catid = isset($theparams["catid"])?$theparams["catid"]:-1;
					$this->term = isset($theparams["term"])?$theparams["term"]:null;
					
					$replace = '';
					switch($match[2]) {
						case 'off':
							$this->extranewsOn = false;
							$article->text = preg_replace($regex, '', $article->text);			
						break;
						case 'related':
						case 'newer':
						case 'older':
						case 'latest': 
							$this->type = $match[2][0]; 
							$article->text = preg_replace($regex, '<!--noparsing-->'.$this->built_news_list($article,$nitems,$title).'<!--/noparsing-->', $article->text, 1);
							JHTML::_('behavior.tooltip');
						break;
					}					
				}
			}
		
			if($this->extranewsOn){							
				$document = & JFactory::getDocument();
				$document->addStyleSheet(JURI::base() . 'plugins/content/extranews/css/extranews.css');
				JHTML::_('behavior.tooltip');			  
				$content = '';
				$showextranews = $this->params->get("showextranews", 'r,n,o');
				if(strpos($showextranews,',')>0) $showextranews = explode(',',$showextranews); else $showextranews = array($showextranews);
				
				foreach($showextranews as $thenews) switch($thenews){
					case 'r':
					case 'n':
					case 'l':
					case 'o':
						$this->type = $thenews;
						$content .= $this->built_news_list($article);
					break;
					default: $content .= '';			
				}

				//assigned content
				if($content) //str_replace(array('{','}'),array('<','>'),$this->params->get( 'textbefore', ''))
					$article->text .= '<div class="extranews_separator"></div>' . $content;
			}
		}
	}     

	/******************************************************************************************************************************/

	function ExtranewsItems(&$article, $article_items = null) 
	{
		//$type
		//o: older news
		//n: newer news
		//r: related news
		//l: latest news -> newer
		$type = $this->type;
		if($type == 'l') $type= 'n';
		$nitems 		= $this->params->get ($type.'_items',7);
		
		if($article_items) $nitems = (int) $article_items;
		
		
		$ordering 		= $this->params->get ($type.'_ordering',5);// 1 = ordering | 2 = popular | 3 = random 
		$chars 			= $this->params->get ($type.'_chars',150);
		$allow_tags		= $this->params->get ($type.'_allow_tags');
		$allow_tags = str_replace(" />", ">", $allow_tags);		
		
		$date_format = $this->params->get ($type.'_dformat',JText::_('DATE_FORMAT_LC'));
		if($date_format == '0') $date_format	= $this->params->get ($type.'_dateformat','%d-%m-%Y %I:%M');
		
		$time_zone	= $this->params->get ('timezone',1); //0: Global 1: User timezone
		
		$imgW 		= $this->params->get ($type.'_imgw',90);
		$imgH 		= $this->params->get ($type.'_imgh',68);
		
		/* prepare database */
		$db			=& JFactory::getDBO();
		//$user		=& JFactory::getUser();
		//$userId		= (int) $user->get('id');
		
		//$aid		= JFactory::getUser()->get('id'); //$authorised = JAccess::getAuthorisedViewLevels($userId);
		//$authorised = JAccess::getAuthorisedViewLevels((int) $user->get('id'));
		$contentConfig = & JComponentHelper::getParams( 'com_content' );
		$access		= ! JComponentHelper::getParams('com_content')->get('show_noauth');
		$nullDate	= $db->getNullDate();
		$date =       & JFactory::getDate();
		$now =         $date->toMySQL(); //date('Y-m-d H:i:s');
		//JError::raiseWarning(500, $db->Quote($now));




		global $mainframe;

		$db			=& JFactory::getDBO();
		$user		=& JFactory::getUser();
		$userId		= (int) $user->get('id');
		$catid		= $article->catid;
		$secid		= $article->sectionid;
		$aid		= $user->get('aid', 0);

		$contentConfig = &JComponentHelper::getParams( 'com_content' );
		$access		= !$contentConfig->get('show_noauth');

		$nullDate	= $db->getNullDate();

		$date =& JFactory::getDate();
		$now = $date->toMySQL();
		//$app =& JFactory::getApplication();
		$config =& JFactory::getConfig();
		if($time_zone) $offset = $user->getParam('timezone');
		else $offset = $config->getValue('config.offset');
		
		$where		= 'a.state = 1'
			. ' AND ( a.publish_up = '.$db->Quote($nullDate).' OR a.publish_up <= '.$db->Quote($now).' )'
			. ' AND ( a.publish_down = '.$db->Quote($nullDate).' OR a.publish_down >= '.$db->Quote($now).' )'
			;
		
		/* set items order */
		$ordering = 5;
		$ord = array(
			1=>'a.ordering',
			2=>'a.hits',
			3=>'RAND()',
			4=>'a.created ASC',
			5=>'a.created DESC'
		);
			$order = $ord[$ordering];
		/*
		// User Filter
		switch ($params->get( 'user_id' ))
		{
			case 'by_me':
				$where .= ' AND (created_by = ' . (int) $userId . ' OR modified_by = ' . (int) $userId . ')';
				break;
			case 'not_me':
				$where .= ' AND (created_by <> ' . (int) $userId . ' AND modified_by <> ' . (int) $userId . ')';
				break;
		}
		*/
		if ($catid)
		{
			$ids = explode( ',', $catid );
			JArrayHelper::toInteger( $ids );
			$catCondition = ' AND (cc.id=' . implode( ' OR cc.id=', $ids ) . ')';
		}
		if ($secid)
		{
			$ids = explode( ',', $secid );
			JArrayHelper::toInteger( $ids );
			$secCondition = ' AND (s.id=' . implode( ' OR s.id=', $ids ) . ')';
		}		 
		switch ($this->type){
			case 'l':
				//$where .= ' AND a.created >= '.$db->Quote($article->created);
				$ordering = 5;
				$order = $ord[$ordering];			
			break;
			case 'n':
				$where .= ' AND a.created >= '.$db->Quote($article->created);
				$ordering = 4;
				$order = $ord[$ordering];
				$where .= ' AND (a.id <>'.$article->id.')';
			break;
			case 'o':
				$where .= ' AND a.created <= '.$db->Quote($article->created);
				$where .= ' AND (a.id <>'.$article->id.')';
			break;		
			case 'r':
				$likes = array();
				//JError::raisewarning('500',  'Related: '. $this->term .'  '.$article->metakey);
				if(is_null($this->term) && empty($article->metakey)) return null;
				//if(is_null($this->term) && empty($article->metakey)) JError::raisewarning('500', 'jgjkg');
				if($this->term) $keys = explode( ',', $this->term );
				else $keys = explode( ',', $article->metakey );
				// assemble any non-blank word(s)
				foreach ($keys as $key) {
					$key = trim( $key );
					if ($key) {
						$likes[] = $db->getEscaped( $key );
					}
				}
				$catCondition = $secCondition = '';
				//JError::raiseWarning(500, print_r($keys,true));
				$where .= " AND ( a.metakey LIKE '%" . implode( "%' OR a.metakey LIKE '%", $likes ) ."%' )";

			default:
		}
		
		$show_front = 1;
		// Content Items only
		$query = 'SELECT a.*, ' .
			' CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug,'.
			' CASE WHEN CHAR_LENGTH(cc.alias) THEN CONCAT_WS(":", cc.id, cc.alias) ELSE cc.id END as catslug'.
			' FROM #__content AS a' .
			' INNER JOIN #__categories AS cc ON cc.id = a.catid' .
			' INNER JOIN #__sections AS s ON s.id = a.sectionid' .
			' WHERE '. $where .' AND s.id > 0' .
			($access ? ' AND a.access <= ' .(int) $aid. ' AND cc.access <= ' .(int) $aid. ' AND s.access <= ' .(int) $aid : '').
			($catid ? $catCondition : '').
			($secid ? $secCondition : '').
			' AND s.published = 1' .
			' AND cc.published = 1' .
			' ORDER BY '. $order;
		if($this->type =='n') $query = "SELECT * FROM ({$query}) as b ORDER by b.created DESC";
		$db->setQuery($query, 0, $nitems);
		$rows = $db->loadObjectList();
		//JError::raisewarning('500',  $nitems.':'.count($rows ).' :'.$this->type.': '. $query);
		$extranews_items	= array();
		if($rows) foreach ( $rows as $row )
		{
			if($row->access <= $aid)
			{
				$row->link = JRoute::_(ContentHelperRoute::getArticleRoute($row->slug, $row->catslug, $row->sectionid));
			} else {
				$row->link = JRoute::_('index.php?option=com_user&view=login');
			}

			$intro = trim($this->removetag(strip_tags($row->introtext,$allow_tags)));
			if(Jstring::strlen($intro)>$chars) $intro = JString::substr($intro,0,$chars) . ' &hellip;';
			$extranews_item = array( //Offset TIME
					'date' 	=> JHTML::_('date', $row->created, JText::_($date_format),$offset),
					'intro' 	=> $intro,
					'link' 	=> htmlspecialchars($row->link, ENT_QUOTES, 'UTF-8'),
					'img' 		=>$this->sized_image($row, $imgW,$imgH),
					'title' 	=> htmlspecialchars($row->title, ENT_QUOTES, 'UTF-8'),
					'id'	 	=> $row->id
					//'ctitle' 	=> htmlspecialchars($row->cattitle, ENT_QUOTES, 'UTF-8')
				);
				$extranews_items[] = $extranews_item;				
			}			
		return $extranews_items;			
	} 

/******************************************************************************************************************************/
	function built_news_list(& $article, $numitems = false, $overidetitle = false) {
		$listtitle = array('n'=>JText::_('LBL_NEWERNAME'), 'o'=>JText::_('LBL_OLDERNAME'),'r'=>JText::_('LBL_RELATEDNAME'),'l'=>JText::_('LBL_LATESTNAME'));
		$listclass = array('n'=>'newer', 'o'=>'older','r'=>'related','l'=>'newer');
		$type = $type2 = $this->type;
		$content = '';
		if ($type == 'l') { $heading = $this->params->get("n_heading",'h2'); $type2 = 'n';}
		else $heading = $this->params->get("{$type}_heading",'h2'); 
		
		if(!$overidetitle) $overidetitle = $listtitle[$type];

		
		if($ItemsList = $this->ExtranewsItems($article,$numitems)){	
			$content .= '<div class="extranews_box"><'.$heading.'><strong>'.JText::_($overidetitle).'</strong></'.$heading.'><div class="extranews_'.$listclass[$type].'"><ul class="'.$listclass[$type].'">';

			$tooltip = $this->params->get("{$type2}_enable_tooltip",true);
			foreach($ItemsList as $item){
						$content .= '<li>'.$this->built_tooltip($item, $tooltip).'</li>';
			}
			$content .= '</ul></div></div>';
		}
		return $content;
		
	}
	function built_tooltip(& $item, $tooltip = true)
	{
		if($this->type == 'l') $type = 'n'; else $type = $this->type;
		
		$title = JText::sprintf($this->params->get($type.'_linkedtitleformat','%1$s - %2$s'), $item['title'], '<span class="extranews_date">'.$item['date'].'</span>');
		$t_content = '<p class="'.$this->params->get($type.'_textalign','extranews_justify').'">'.$item['intro'].'</p>'; 
		$img_align = $this->params->get($type.'_imgalign','icenter');
		$img = ''; 
		if($item['img']) $img = '<img src="'.$item['img'].'" alt="" class="iextranews '.$img_align.'" />';

		if($img_align == 'icenter2') $t_content = $t_content.'<pan class="extranews_clear"></span>'.$img;
		else $t_content = $img.$t_content;
		if($tooltip) return JHTML::tooltip($t_content, $item['title'], '', $title, $item['link']);
		else return '<a href="'.$item['link'].'" title="'.$item['title'].'">'.$title.'</a>';
	} 
	
	function sized_image($article, $w=90, $h=68)
	{
		$w = (int) $w;
		$h = (int) $h;
		$rootpath = JPATH_SITE.DS.'images'.DS;
		$imagefolder = 'plg_imagesized';
		
		if(!file_exists($rootpath.$imagefolder)) $this->make_folder($rootpath,$imagefolder);
		
		$urlbaseimage = JURI::base(true).'/images/' . $imagefolder . '/';
		$articleImag = $this->search_extranews_image( $article->introtext );
		if( $articleImag == '' ) $articleImag= $this->search_extranews_image( $article->fulltext );
		
		if($articleImag){			
			$filepart = explode('/', $articleImag);
			$filename  = array_pop($filepart);
			$thumb = "thumb_{$w}_{$h}_".$filename;
			$thenewimg = $rootpath.$imagefolder.DS.$thumb;
			$imgx = $this->extranews_image_fixlink($articleImag);
			if(!file_exists($thenewimg) && $this->isimage($imgx) ){
				/*******Load class SimpleImage****************************************************/
				   if (!class_exists('SimpleImage'))
				   ////plg_imagesized
					  include(JPATH_SITE.DS.'plugins'.DS.'content'.DS.'extranews'.DS.'lib'.DS.'simpleimage.php');
				/*********************************************************************************/  			
				$image = new SimpleImage();								 
				$image->load($imgx);
				$image->resize($w,$h);
				$returnvalue = $image->save($thenewimg);
				if(!$returnvalue)return $articleImag;
			}
			if(file_exists($thenewimg)) $articleImag= $urlbaseimage.$thumb;
		}
				
		return $articleImag;		
	}

	/**
	 * Searches for all images inside a text and returns the first one found
	 *
	 * @param string $content
	 * @return string
	 */
	function search_extranews_image( $content )
	{		
		preg_match_all("#\<img(.*)src\=\"(.*)\"#Ui", $content, $mathes);		
		return isset($mathes[2][0]) ? $mathes[2][0] : '';			
	}	

	function extranews_image_fixlink($articleImag){
		if(strpos($articleImag, 'http://') === 0) return $articleImag; 
		else return (JPATH_SITE.DS.str_replace('/',DS,$articleImag));
	}
	function isimage($imageurl)  //Write to a log file for checking what has been done
	{
		return in_array(strtolower(substr(strrchr($imageurl, "."), 1)), array('gif','jpg','png','jpeg'));
	} 

	function strip_newline($text){
	//strip \r\n
	$order = array("\r\n","\n","\r");
	//$replace = '<br />';
	$replace = ' ';
	$text=str_replace($order,$replace,$text);
	return $text;
	}

		  
	function removetag($string){
		  $pattern = "|{[^}]+}(.*){/[^}]+}|U";
		  $replacement = '';
		  return preg_replace($pattern, $replacement, $string);
	}
private function make_folder($rootpath, $imagefolder, $chmod = 0755){
	$folders = explode(DS,$imagefolder);         
	$tmppath = $rootpath;
	for($i=0;$i <= count($folders)-1; $i++){
        if(!file_exists($tmppath.$folders[$i])) {
		if(!mkdir($tmppath.$folders[$i],$chmod)) return 0; //can not create folder
	} //Folder exist
        $tmppath = $tmppath.$folders[$i].DS;
        //make a blank content
        $ffilename = $tmppath . 'index.html';
	        if(!file_exists($ffilename)){
	                $filecontent = '<html><body bgcolor="#FFFFFF"></body></html>';
	                $handle = fopen($ffilename, 'x+');
	                fwrite($handle, $filecontent);
	                fclose($handle);  
	        }       
        }
	return 1;
}		
} // End Class


?>