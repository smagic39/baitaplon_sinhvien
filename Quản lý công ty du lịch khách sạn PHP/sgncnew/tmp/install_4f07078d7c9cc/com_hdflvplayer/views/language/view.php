<?php
/**
 * @version		$Id: view.php 1.4 2010-11-30 $
 * @package		Joomla
 * @subpackage	hdflvplayer
 * @copyright Copyright (C) 2010-2011 Contus Support
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */
// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');



class hdflvplayerViewlanguage extends JView
{

	function language()
	{
        $model =& $this->getModel();
		$detail = $model->getlanguage();
        $this->assignRef('detail', $detail);

		parent::display();
	}

}
?>   
