<?php
/**
 * @version		$Id: addview.php 1.4 2010-11-30 $
 * @package		Joomla
 * @subpackage	hdflvplayer
 * @copyright Copyright (C) 2010-2011 Contus Support
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

jimport( 'joomla.application.component.model' );

class hdflvplayerModeladdview extends JModel
{
    /**
     * Gets the greeting
     *
     * @return string The greeting to be displayed to the user
     */
    function getviewcount()
    {
    $thumbid1="";
    $thumbid1=JRequest::getvar('thumbid','','get','int');
    $db =& JFactory::getDBO();
    $query="update #__hdflvplayerupload SET times_viewed=1+times_viewed where id=$thumbid1";
    $db->setQuery($query );
    $db->query();
    
    }
    
}