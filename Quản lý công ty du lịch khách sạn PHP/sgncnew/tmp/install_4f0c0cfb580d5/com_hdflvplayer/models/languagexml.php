<?php
/**
 * @version		$Id: languagexml.php 1.4 2010-11-30 $
 * @package		Joomla
 * @subpackage	hdflvplayer
* @copyright Copyright (C) 2010-2011 Contus Support
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

jimport( 'joomla.application.component.model' );


class hdflvplayerModellanguagexml extends JModel
{
	/**
	 * Gets the greeting
	 * 
	 * @return string The greeting to be displayed to the user
	 */
        function getlanguage()
        {

          $db =& JFactory::getDBO();
          $query = "select * from #__hdflvplayerlanguage where published='1' and home=1";//and id=2";
            $db->setQuery( $query );
            $rows = $db->loadObjectList();
            return $rows;
        }
}