<?php
/**
 * @version		$Id: view.php 1.4 2010-11-30 $
 * @package		Joomla
 * @subpackage	hdflvplayer
 * @copyright Copyright (C) 2010-2011 Contus Support
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */
// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');


class hdflvplayerViewimpressionclicks extends JView
{

    function impressionclicks()
    {
        $model = $this->getModel();
        $impressionclicks = $model->impressionclicks();
        //parent::display();
    }

}
?>   
