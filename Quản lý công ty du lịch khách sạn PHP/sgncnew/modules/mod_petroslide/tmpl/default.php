<?php
/**
* @package Autor
* @author Duong Khoan Giam
* @website http://www.tccomputervn.com
* @email duongkhoangiam@gmail.com
* @copyright Jimmy
* @license For Jimmy 0978904060
**/

// no direct access
defined('_JEXEC') or die('Restricted access');



?>
<div id="blinds">
<?php foreach($images as $image){
echo JHTML::_('image', 'images/slide/'.$image->name, $image->name, array('width' => $image->width, 'height' => $image->height));
} ?>
</div>
<script type="text/javascript">
      jQuery(document).ready(function() {
        $('#blinds').nivoSlider({
			controlNav:false
		});
    });
    </script>