<?php
switch ($act){	
	case "booksm":
		booksm();
		break;
	default:
		$tpl="booking/detail";
}

function booksm() {
	$roomtype=$_POST["roomtype"];	
	$firstname=$_POST["firstname"];
	$middlename=$_POST["middlename"];
	$lastname=$_POST["lastname"];	
	$address=$_POST["address"];
	$city=$_POST["city"];	
	$country=$_POST["country"];
	$tel=$_POST["tel"];	
	$email=$_POST["email"];
	$arrival_date=$_POST["arrival_date"];
	$departure_date=$_POST["departure_date"];	
	$rooms=$_POST["rooms"];
	$adults=$_POST["adults"];	
	$child=$_POST["child"];
	$from_rate=$_POST["from_rate"];
	$to_rate=$_POST["to_rate"];
	$other_info=$_POST["other_info"];
	
	$toemail="sales@saigonninhchuhotel.com.vn, reservation@saigonninhchuhotel.com.vn, ".$email;
	$title='Khach hang dat phong tu website SaiGon NinhChu Hotel';		
	$content='<http><body>	
	<table width="100%" border="0" cellspacing="0" cellpadding="3">
		<tr><td colspan="2"><b>Thông tin khách hàng</b></td></tr>
		<tr><td height="5"></td></tr>
		<tr>
			<td width="150" align="right">Loại phòng :</td>
			<td>'.$roomtype.'</td>
		</tr>
		<tr>
			<td align="right">Họ :</td>
			<td>'.$firstname.'</td>
		</tr>
		<tr>
			<td align="right">Tên đệm :</td>
			<td>'.$middlename.'</td>
		</tr>
		<tr>
			<td align="right">Tên :</td>
			<td>'.$lastname.'</td>
		</tr>
		<tr>
			<td align="right">Địa chỉ :</td>
			<td>'.$address.'</td>
		</tr>
		<tr>
			<td align="right">Tỉnh/Thành phố :</td>
			<td>'.$city.'</td>
		</tr>
		<tr>
			<td align="right">Quốc gia :</td>
			<td>'.$country.'</td>
		</tr>
		<tr>
			<td align="right">Điện thoại :</td>
			<td>'.$tel.'</td>
		</tr>
		<tr>
			<td align="right">Email :</td>
			<td>'.$email.'</td>
		</tr>
		<tr><td height="10"></td></tr>
		<tr><td colspan="2"><b>Thông tin đặt phòng</b></td></tr>
		<tr><td height="5"></td></tr>
		<tr>
			<td align="right">Ngày đến :</td>
			<td>'.$arrival_date.'</td>
		</tr>
		<tr>
			<td align="right">Ngày đi :</td>
			<td>'.$departure_date.'</td>
		</tr>
		<tr>
			<td align="right">Số lượng phòng :</td>
			<td>'.$rooms.'</td>
		</tr> 
		<tr>
			<td align="right">Số lượng người lớn :</td>
			<td>'.$adults.'</td>
		</tr>
		<tr>
			<td align="right">Số lượng trẻ em :</td>
			<td>'.$child.'</td>
		</tr>
		<tr><td height="10"></td></tr>
		<tr><td colspan="2"><b>Khung giá</b></td></tr>
		<tr><td height="5"></td></tr>    
		<tr>
			<td align="right">Từ giá :</td>
			<td>'.$from_rate.' USD</td>
		</tr>
		<tr>
			<td align="right">Đến giá :</td>
			<td>'.$to_rate.' USD</td>
		</tr>
		<tr><td height="10"></td></tr>
		<tr><td colspan="2"><b>Yêu cầu khác</b></td></tr>
		<tr><td height="5"></td></tr>
		<tr>
			<td align="right"></td>
			<td>'.str_replace("\n","\n<br>",$other_info).'</td>
		</tr>   
	</table>
	</body></html>';
	//echo $content;
	$headers ="MIME-Version: 1.0\r\n";
	$headers.="Content-type: text/html; charset=utf-8\r\n";
	$headers.="From: ".$email."\r\n";	
	$headers.="Reply-To: ".$email."\r\n";
	$headers.="X-Mailer: PHP/".phpversion();
	
	mail($toemail,$title,$content,$headers);
	page_transfer2("?do=booking&send=1");
}

?>