<?php
switch($act){
	case("add"):
	case("edit"):
		if($act=='edit') show_edit();		
		$tpl ="roomtypes/edit";			
		break;
	case("addsm"):
	case("editsm"):
		add_edit();
		break;
	case "move":
		move();
		break;
	case "del":
		del();
		break;
	case "delselect":
		delselect();
		break;
	default:
		showroomtypes();			
		$tpl ="roomtypes/list";
		break;
}

function showroomtypes() {
	global $db,$roomtypes,$page,$plpage,$set_per_page;
	$set_per_page=10;
	$key=trim($_GET['key']);
	$sqlstmt="select id, title_vn, img, special, new, `order`, date from `roomtypes` where 1=1";
	if ($key) $sqlstmt.=" and (title_vn like '%".$key."%' or title_en like '%".$key."%' or title_ru like '%".$key."%')";
	$sqlstmt.=" order by `order`, id";	
	$plpage = plpage($sqlstmt,$page,$set_per_page);
	$sqlstmt = sqlmod($sqlstmt,$page,$set_per_page);
	$roomtypes = $db->getAll($sqlstmt);
}

function show_edit(){
	global $db,$roomtypes;
	$id=$_GET['id'];
	$sqlstmt="select * from `roomtypes` where id=$id";
	$roomtypes = $db->getRow($sqlstmt);
	if (!$roomtypes) page_transfer2("?do=roomtypes");			
}

function add_edit(){
	global $db,$act;
	$arr['title_vn']=htmlspecialchars($_POST['title_vn'], ENT_QUOTES);
	$arr['title_en']=htmlspecialchars($_POST['title_en'], ENT_QUOTES);
	$arr['title_ru']=htmlspecialchars($_POST['title_ru'], ENT_QUOTES);
	$arr['desc_vn']=addslashes($_POST['desc_vn']);
	$arr['desc_en']=addslashes($_POST['desc_en']);
	$arr['desc_ru']=addslashes($_POST['desc_ru']);
	$arr['content_vn']=addslashes($_POST['content_vn']);
	$arr['content_en']=addslashes($_POST['content_en']);
	$arr['content_ru']=addslashes($_POST['content_ru']);
	$arr['new']=$_POST['new']?$_POST['new']:0;
	$arr['special']=$_POST['special']?$_POST['special']:0;
	
	$img = isset($_FILES['img']['name']) ? $_FILES['img']['name'] : '';
	$sizeimg = $_FILES['img']['size'];
	$filename='';
	//--------------------------------------------------
	if(!empty($img)&& $sizeimg>0){
		$start = strpos($img,".");
		$type = substr($img,$start,strlen($img));
		$filename = "img_".time().$type;
		$filename = strtolower($filename);
		copy($_FILES['img']['tmp_name'], "./upload/roomtypes/" . $filename) ;
		$arr['img']=$filename;
  }
	
	if($act=='addsm'){
		$arr['date']=date("Y-m-d H:i:s");
		$sqlmaxvt="select max(`order`) as maxorder from roomtypes";		
		$remaxvt=$db->getRow($sqlmaxvt);
		$arr['order']=$remaxvt['maxorder']+1;	
		vaInsert('roomtypes',$arr);		
		$msg="Thêm mới thành công";	
	}	elseif($act=='editsm'){
		$id=$_POST['id'];
		if ($_POST['noimage'] || $sizeimg>0){		
			$sqlstmt="select img from `roomtypes` where id=$id";
			$r = $db->getRow($sqlstmt);
			$img_old=$r['img'];	
			if($_POST['noimage']) $arr['img']='';
			if($sizeimg>0) $arr['img']=$filename;
			$delfile = "./upload/roomtypes/".$img_old;
			if(file_exists($delfile) &&$img_old!="") unlink($delfile);
		}
		vaUpdate('roomtypes',$arr,'id='.$id);
		$msg="Cập nhật thành công";
	}		
	$page="?do=roomtypes&page=".$_POST['page'];
	page_transfer($msg,$page);
}

function move(){
	global $db;
	$id1=$_GET['id1'];
	$id2=$_GET['id2'];
	$pos1=$_GET['pos1'];
	$pos2=$_GET['pos2'];
	$update1="UPDATE roomtypes SET `order` = ".$pos1." WHERE id=".$id1;
	$db->query($update1);
	$update2="UPDATE roomtypes SET `order` = ".$pos2." WHERE id=".$id2;
	$db->query($update2);	
	$page="?do=roomtypes&page=".$_GET['page'];
	page_transfer2($page);		
}

function del(){
	global $db;
	$id=$_GET["id"];
	
	$sqlstmt="select img from `roomtypes` where id=$id";
	$r = $db->getRow($sqlstmt);
	$img_old=$r['img'];
	$delfile = "./upload/roomtypes/".$img_old;
	if(file_exists($delfile) && $img_old!="") unlink($delfile);
			
	$sqldel="delete from roomtypes where id=$id";
	$db->query($sqldel);
	
	$page="?do=roomtypes&page=".$_GET['page'];
	$msg="Xóa thành công";
	page_transfer($msg,$page);
}

function delselect(){
	global $db;
	$arrid=$_POST['checkid'];
	
	for ($i=0;$i<count($arrid);$i++){
		$id=$arrid[$i];
			
		$sqlstmt="select img from `roomtypes` where id=$id";
		$r = $db->getRow($sqlstmt);
		$img_old=$r['img'];
		$delfile = "./upload/roomtypes/".$img_old;
		if(file_exists($delfile) && $img_old!="") unlink($delfile);
		
		$sqldel="delete from roomtypes where id=$id";
		$db->query($sqldel);
	}
	
	$page="?do=roomtypes&page=".$_GET['page'];
	$msg="Xóa thành công";
	page_transfer($msg,$page);
}

?>