<?php
switch($act){
	case("add"):
	case("edit"):
		if($act=='edit') show_edit();
		$tpl ="nickchat/edit";			
		break;
	case("addsm"):
	case("editsm"):
		add_edit();
		break;
	case "move":
		move();
		break;
	case "del":
		del();
		break;
	case "delselect":
		delselect();
		break;
	default:
		shownickchat();	
		$tpl ="nickchat/list";
		break;
}

function shownickchat() {
	global $db,$nickchat;
	$sqlstmt="select * from `nickchat` order by `order`, id";
	$nickchat = $db->getAll($sqlstmt);
}

function show_edit(){
	global $db,$nickchat;
	$id=$_GET['id'];
	$sqlstmt="select * from `nickchat` where id=$id";
	$nickchat = $db->getRow($sqlstmt);
	if (!$nickchat) page_transfer2("?do=nickchat");		
}

function add_edit(){
	global $db,$act;
	$arr['title_vn']=htmlspecialchars($_POST['title_vn'], ENT_QUOTES);
	$arr['title_en']=htmlspecialchars($_POST['title_en'], ENT_QUOTES);
	$arr['title_ru']=htmlspecialchars($_POST['title_ru'], ENT_QUOTES);
	$arr['nick']=htmlspecialchars($_POST['nick'], ENT_QUOTES);
	$arr['type']=$_POST['type'];
	
	if($act=='addsm'){	
		$sqlmaxvt="select max(`order`) as maxorder from nickchat";		
		$remaxvt=$db->getRow($sqlmaxvt);
		$arr['order']=$remaxvt['maxorder']+1;	
		vaInsert('nickchat',$arr);		
		$msg="Thêm mới thành công";		
	}	elseif($act=='editsm'){
		$id=$_POST['id'];		
		vaUpdate('nickchat',$arr,'id='.$id);
		$msg="Cập nhật thành công";
	}		
	$page="?do=nickchat";
	page_transfer($msg,$page);
}

function move(){
	global $db;
	$id1=$_GET['id1'];
	$id2=$_GET['id2'];
	$pos1=$_GET['pos1'];
	$pos2=$_GET['pos2'];
	$update1="UPDATE nickchat SET `order` = ".$pos1." WHERE id=".$id1;
	$db->query($update1);
	$update2="UPDATE nickchat SET `order` = ".$pos2." WHERE id=".$id2;
	$db->query($update2);	
	$page="?do=nickchat";
	page_transfer2($page);		
}

function del(){
	global $db;
	$id=$_GET["id"];
	
	$sqldel="delete from nickchat where id=$id";
	$db->query($sqldel);
		
	$page="?do=nickchat";
	$msg="Xóa thành công";
	page_transfer($msg,$page);
}

function delselect(){
	global $db;	
	$arrid=$_POST['checkid'];
	
	for ($i=0;$i<count($arrid);$i++){
		$id=$arrid[$i];
		
		$sqldel="delete from nickchat where id=$id";
		$db->query($sqldel);
	}
	
	$msg="Xóa thành công";	
	$page="?do=nickchat";
	page_transfer($msg,$page);
}

?>