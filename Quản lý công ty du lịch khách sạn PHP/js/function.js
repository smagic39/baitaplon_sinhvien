// open_window
function open_window(url,name,width,height) {
	var left=(screen.width - width)/2;
	var top=(screen.height - height)/2;
	window.open(url, name, 'scrollbars=yes,resizeable=no,locationbar=no,width='+width+',height='+height+',left='+left+',top='+top);
}

//web_links
function funcweblink() {
	var number = document.frmlink.weblink.selectedIndex;
	var strweblink = document.frmlink.weblink.options[number].value;
	if (strweblink!='') {
		document.frmlink.action=strweblink;
		document.frmlink.submit();
	}
}	

// set opacity
function changeOpac(opacity, id) {	
	var object = document.getElementById(id).style;  
	object.opacity = (opacity / 100);  
	object.MozOpacity = (opacity / 100);  
	object.KhtmlOpacity = (opacity / 100);  
	object.filter = "alpha(opacity=" + opacity + ")";  
}

//load data
function loaddata(url,id,loader,eval_str){
	if(document.getElementById){var x=(window.ActiveXObject)?new ActiveXObject("Microsoft.XMLHTTP"):new XMLHttpRequest();}
	if(x){x.onreadystatechange=function() {
			el=document.getElementById(id);
			if(loader) el.innerHTML=loader;
			if(x.readyState==4&&x.status==200){
					el.innerHTML='';
					el=document.getElementById(id);
					el.innerHTML=x.responseText;
					eval(eval_str);
					}
			}
	x.open("GET",url,true);x.send(null);
	}
}