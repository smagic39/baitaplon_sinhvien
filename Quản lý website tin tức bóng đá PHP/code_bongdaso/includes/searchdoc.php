<?php
$template -> set_filenames(array('searchdoc'=> $dir_template . 'searchdoc.tpl'));
	$cmd = "";
	if(isset($HTTP_POST_VARS['cmd']))
		$cmd = $HTTP_POST_VARS['cmd'];
	
	$condition = '';
	if($cmd == 'Tìm kiếm')
	{
		unset($_SESSION['sr_name']);
		unset($_SESSION['sr_order']);
							
		if(isset($_POST['txtName']) && $_POST['txtName'] != '')
		{
			 $_SESSION['sr_name']   = addslashes($_POST['txtName']);
		}
		if(isset($_POST['cbOrder']) && $_POST['cbOrder'] != '')
		{
			 $_SESSION['sr_order']   = addslashes($_POST['cbOrder']);
		}
		
	}
	
	$template -> assign_vars(array(
		'name' => (isset($_SESSION['sr_name']))?$_SESSION['sr_name']:'',
		'searchinfo' => (isset($_SESSION['sr_order']) && $_SESSION['sr_order'] == 0)?'selected':'',
		'searchdoc' => (isset($_SESSION['sr_order']) && $_SESSION['sr_order'] == 1)?'selected':'',
	));
	if(isset($_SESSION['sr_order']))
	{
		if($_SESSION['sr_order'] == 0)
		{
			// Tong so ban ghi 
			$sql_law_total = "SELECT COUNT(*) FROM tblinfo WHERE 1 AND title_vn like '%".$_SESSION['sr_name']."%'";
			$sql_law_total = $db->sql_query($sql_law_total) or die(mysql_error());
			$nRows = $db->sql_fetchfield(0);
			if ($nRows == 0 )
			{
				$template -> assign_block_vars('ALERT', array(
						'txt_alert'	=> 'Không tìm thấy thông tin nào hợp lệ',
					));
			}
			else
			{
				$p = !empty($HTTP_GET_VARS['p']) ? (int)$HTTP_GET_VARS['p'] : 0;
				$p = ( ($p >= $nRows) || !is_numeric($p) || ($p < 0)) ? 0 : $p;
				// So trang dang hien thi
				$link_current_page = ($p > 0) ? '&p=' . $p : '';
				$nNewsSize = 10;
				// So trang dang hien thi
				$sql_law = "SELECT * FROM tblinfo WHERE  1 AND title_vn like '%".$_SESSION['sr_name']."%'
									ORDER BY create_time
									LIMIT " . $p . ", " . $nNewsSize . "";
				$sql_law = $db->sql_query($sql_law) or die(mysql_error());
				$nCounter = 0;
				while ( $law_rows = $db->sql_fetchrow($sql_law))
				{
					$nCounter++;
					$template -> assign_block_vars('DOC1', array(
						'order'				=> $p+$nCounter,
						'link'				=> './?act=info&id=',
						'id'				=> $law_rows['id'],
						'docname'			=> displayData_DB($law_rows['title_vn']),
					));
				}
			}
		}
		else
		{
			// Tong so ban ghi 
			$sql_law_total = "SELECT COUNT(*) FROM tbl_law WHERE 1 AND name_vn like '%".$_SESSION['sr_name']."%'";
			$sql_law_total = $db->sql_query($sql_law_total) or die(mysql_error());
			$nRows = $db->sql_fetchfield(0);
			if ($nRows == 0 )
			{
				$template -> assign_block_vars('ALERT',array(
						'txt_alert'	=> 'Không tìm thấy thông tin nào',
				));
			}
			else
			{
				$p = !empty($HTTP_GET_VARS['p']) ? (int)$HTTP_GET_VARS['p'] : 0;
				$p = ( ($p >= $nRows) || !is_numeric($p) || ($p < 0)) ? 0 : $p;
				// So trang dang hien thi
				$link_current_page = ($p > 0) ? '&p=' . $p : '';
				$nNewsSize = 10;
				// So trang dang hien thi
				$sql_law = "SELECT * FROM tbl_law WHERE  1 AND name_vn like '%".$_SESSION['sr_name']."%'
									ORDER BY create_time
									LIMIT " . $p . ", " . $nNewsSize . "";
				$sql_law = $db->sql_query($sql_law) or die(mysql_error());
				$nCounter = 0;
				while ( $law_rows = $db->sql_fetchrow($sql_law))
				{
					$nCounter++;
					$template -> assign_block_vars('DOC1', array(
						'order'				=> $p+$nCounter,
						'id'				=> (file_exists($data_upload.$law_rows['doc_vn']))?$law_rows['doc_vn']:'#',
						'link'				=> "./download.php?name=",
						'docname'			=> displayData_DB($law_rows['name_vn']),
					));
				}
			}
		}
	}

	// Hien thi phan trang
	$url = './?act=searchdoc';
	$page = generate_pagination($url, $nRows, $nNewsSize, $p);
	$template -> assign_vars(array(
		'txt_page'	=> $page
	));

$template -> pparse('searchdoc');
?>