<?php
/******************************************************
Company: ECOPRO Co., Ltd
Project: SO XAY DUNG DIEN BIEN
Author: Vu Thanh Toan
Email: vttoan@gmail.com
Phone: 84913319908
Commen Day: 12/8/2006 9:38 AM
*******************************************************/

function generate_pagination_java($base_url, $num_items, $per_page, $start_item, $add_prevnext_text = TRUE)
{
	if(isset($_SESSION['lang']))
	{
		if($_SESSION['lang'] == 'vn')
		{
			$pre = 'Trước';
			$nex = 'Sau';
		}
		else
		{
			$pre = 'Prev';
			$nex = 'Next';
		}
			
	}
	
	$total_pages = ceil($num_items/$per_page);

	if ( $total_pages == 1 )
	{
		return '';
	}

	$on_page = floor($start_item / $per_page) + 1;

	$page_string = '';
	if ( $total_pages > 10 )
	{
		$init_page_max = ( $total_pages > 3 ) ? 3 : $total_pages;
		
		for($i = 1; $i < $init_page_max + 1; $i++)
		{
			$page_string .= ( $i == $on_page ) ? '<b class="txtsearch">' . $i . '</b>' : '<a href="#" onclick="request(\'' . $base_url . '&p=' . ( ( $i - 1 ) * $per_page ) . '\')" class="txtsearch">' . $i . '</a>';
			if ( $i <  $init_page_max )
			{
				$page_string .= ", ";
			}
		}

		if ( $total_pages > 3 )
		{
			if ( $on_page > 1  && $on_page < $total_pages )
			{
				$page_string .= ( $on_page > 5 ) ? ' ... ' : ', ';

				$init_page_min = ( $on_page > 4 ) ? $on_page : 5;
				$init_page_max = ( $on_page < $total_pages - 4 ) ? $on_page : $total_pages - 4;

				for($i = $init_page_min - 1; $i < $init_page_max + 2; $i++)
				{
					$page_string .= ($i == $on_page) ? '<b class="txtsearch">' . $i . '</b>' : '<a  href="#" onclick="request(\'' . $base_url . '&p=' . ( ( $i - 1 ) * $per_page )  . '\')" class="txtsearch">' . $i . '</a>';
					if ( $i <  $init_page_max + 1 )
					{
						$page_string .= ', ';
					}
				}

				$page_string .= ( $on_page < $total_pages - 4 ) ? ' ... ' : ', ';
			}
			else
			{
				$page_string .= ' ... ';
			}

			for($i = $total_pages - 2; $i < $total_pages + 1; $i++)
			{
				$page_string .= ( $i == $on_page ) ? '<b class="txtsearch">' . $i . '</b>'  : '<a  href="#" onclick="request(\'' . $base_url . '&p=' . ( ( $i - 1 ) * $per_page ) . '\')" class="txtsearch"><span style="text-decoration: none">' . $i . '</span></a>';
				if( $i <  $total_pages )
				{
					$page_string .= ", ";
				}
			}
		}
	}
	else
	{
		for($i = 1; $i < $total_pages + 1; $i++)
		{
			$page_string .= ( $i == $on_page ) ? '<b class="txtsearch">[' . $i . ']</b>' : '<a  href="#" onclick="request(\'' . $base_url . '&p=' . ( ( $i - 1 ) * $per_page ) . '\')" class="txtsearch">' . $i . '</a>';
			if ( $i <  $total_pages )
			{
				$page_string .= ' ';
			}
		}
	}

	if ( $add_prevnext_text )
	{
		if ( $on_page > 1 )
		{
			$page_string = ' <a  href="#" onclick="request(\'' . $base_url . '&p=' . ( ( $on_page - 2 ) * $per_page ) . '\')" class="txtsearch"><span style="text-decoration: none">'.$pre.'</span></a>&nbsp;&nbsp;' . $page_string;
		}
		
		if ( $on_page < $total_pages )
		{
			$page_string .= '&nbsp;&nbsp;<a  href="#" onclick="request(\'' . $base_url . '&p=' . ( $on_page * $per_page ) . '\')" class="txtsearch"><span style="text-decoration: none">'.$nex.'</span</a>';
		}

	}
	//$page_string = 'Trang: ' . $page_string;
	$page_string = $page_string;
	if($page_string != '' && $_SESSION['lang'] == 'en') $page_string = '<span class="txtsearch">Page:</span> '.$page_string;
	if($page_string != '' && $_SESSION['lang'] == 'vn') $page_string = '<span class="txtsearch">Trang:</span> '.$page_string;
	return $page_string;
}

////////////
function generate_pagination($base_url, $num_items, $per_page, $start_item, $add_prevnext_text = TRUE)
{
	
	
	if(isset($_SESSION['lang']))
	{
		
		if($_SESSION['lang'] == 'en')
		{
			$pre = 'Prev';
			$nex = 'Next';
		}
		else
		{
			$pre = 'Trước';
			$nex = 'Sau';
		}
			
	}
	
	$total_pages = ceil($num_items/$per_page);

	if ( $total_pages == 1 )
	{
		return '';
	}

	$on_page = floor($start_item / $per_page) + 1;

	$page_string = '';
	if ( $total_pages > 10 )
	{
		$init_page_max = ( $total_pages > 3 ) ? 3 : $total_pages;
		
		for($i = 1; $i < $init_page_max + 1; $i++)
		{
			$page_string .= ( $i == $on_page ) ? '<b class="txtsearch">' . $i . '</b>' : '<a href="' . $base_url . "&amp;p=" . ( ( $i - 1 ) * $per_page ) . '" class="txtsearch">' . $i . '</a>';
			if ( $i <  $init_page_max )
			{
				$page_string .= ", ";
			}
		}

		if ( $total_pages > 3 )
		{
			if ( $on_page > 1  && $on_page < $total_pages )
			{
				$page_string .= ( $on_page > 5 ) ? ' ... ' : ', ';

				$init_page_min = ( $on_page > 4 ) ? $on_page : 5;
				$init_page_max = ( $on_page < $total_pages - 4 ) ? $on_page : $total_pages - 4;

				for($i = $init_page_min - 1; $i < $init_page_max + 2; $i++)
				{
					$page_string .= ($i == $on_page) ? '<b class="txtsearch">' . $i . '</b>' : '<a href="' . $base_url . "&amp;p=" . ( ( $i - 1 ) * $per_page )  . '" class="txtsearch">' . $i . '</a>';
					if ( $i <  $init_page_max + 1 )
					{
						$page_string .= ', ';
					}
				}

				$page_string .= ( $on_page < $total_pages - 4 ) ? ' ... ' : ', ';
			}
			else
			{
				$page_string .= ' ... ';
			}

			for($i = $total_pages - 2; $i < $total_pages + 1; $i++)
			{
				$page_string .= ( $i == $on_page ) ? '<b class="txtsearch">' . $i . '</b>'  : '<a href="' . $base_url . "&amp;p=" . ( ( $i - 1 ) * $per_page ) . '" class="txtsearch"><span style="text-decoration: none">' . $i . '</span></a>';
				if( $i <  $total_pages )
				{
					$page_string .= ", ";
				}
			}
		}
	}
	else
	{
		for($i = 1; $i < $total_pages + 1; $i++)
		{
			$page_string .= ( $i == $on_page ) ? '<b class="txtsearch">[' . $i . ']</b>' : '<a href="' . $base_url . "&amp;p=" . ( ( $i - 1 ) * $per_page ) . '" class="txtsearch">' . $i . '</a>';
			if ( $i <  $total_pages )
			{
				$page_string .= ' ';
			}
		}
	}

	if ( $add_prevnext_text )
	{
		if ( $on_page > 1 )
		{
			$page_string = ' <a href="' . $base_url . "&amp;p=" . ( ( $on_page - 2 ) * $per_page ) . '" class="txtsearch"><span style="text-decoration: none">'.$pre.'</span></a>&nbsp;&nbsp;' . $page_string;
		}

		if ( $on_page < $total_pages )
		{
			$page_string .= '&nbsp;&nbsp;<a href="' . $base_url . "&amp;p=" . ( $on_page * $per_page ) . '" class="txtsearch"><span style="text-decoration: none">'.$nex.'</span</a>';
		}

	}

	//$page_string = 'Trang: ' . $page_string;
	$page_string = $page_string;
	if($page_string != '' && $_SESSION['lang'] == 'en') $page_string = '<span class="txtsearch">Page:</span> '.$page_string;
	if($page_string != '' && $_SESSION['lang'] == 'vn') $page_string = '<span class="txtsearch">Trang:</span> '.$page_string;
	return $page_string;
}

///////////
?>