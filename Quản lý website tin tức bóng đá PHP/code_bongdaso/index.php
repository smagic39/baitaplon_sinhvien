<?php
include('./includes/config.inc.php');
include($dir_inc.'template.php');
include($dir_inc.'function.php');
include($dir_inc.'functions.php');

/******Xu li ngon ngu********/
if(!isset($_SESSION['lang'])) $_SESSION['lang'] = 'vn';
$lang_ = isset($_GET['lang'])?$_GET['lang']:'';
$_SESSION['lang'] = $lang_!=''?$lang_:$_SESSION['lang'];
unset($_GET['lang']);

if ($_SESSION['lang'] =='en')
	include('lang/lang_english.php');
else include('lang/lang_vietnamese.php');

$template = new Template();


$act = '';
if(isset($HTTP_GET_VARS['act']))
	$act = $HTTP_GET_VARS['act'];

if ( $act == 'logout')
{
	session_destroy();
	$ret=setcookie("UserId", 0, time()+3600);
	header('location: ./');
	exit();
}
//Goi file xu ly dang nhap

if(!isset($_SESSION['login']))
{
	//Dem so luot truy cap
	if(file_exists("counter.dat"))
	{
	    $fh1 = fopen("counter.dat","r");
	    $line = fgets($fh1);
		fclose($fh1);
		$counter = (int)$line;
		if($counter < 31000) $counter = 31000; 
		$new_counter = $counter + 1;
		//echo $new_counter;
		$fh1 = fopen("counter.dat","w");
		 fputs($fh1,(string)$new_counter);
		 fclose($fh1);
	}
	else
	{
		$fh1 = fopen("counter.dat","w");
		fputs($fh1,'31000');
		fclose($fh1);
	}
	$_SESSION['login'] = 'login';
}

include($dir_inc.'header.php');

$permission = 0;
if(isset($_SESSION['member_type']))
	$permission = $_SESSION['member_type'];
///////////////////////////////////////////////////
include($dir_inc.'left.php');
$right = true;
switch ($act)
{		
	case 'info':
		private_unset();
		if(isset($_GET['id']))
		{
			include ($dir_inc.'news_detail.php');
		}
		elseif(isset($_GET['code']) || isset($_GET['cat_id']))
		{
			include ($dir_inc.'news.php');
		}
		else
		{
			include ($dir_inc.'home.php');	
		}
		break;
	case 'FullTime':
		private_unset();
		include($dir_inc.'FullTime.php');		
		break;
	//login
	case 'login':
		private_unset();
		include($dir_inc.'processLogin.php');
		echo "<script type='text/javascript'>location.href='?act=member'</script>";
		break;
	//Part of register
	case 'forgot':
		private_unset();
		include($dir_inc.'forgot.php');		
		break;
	//Part of register
	case 'candidate':
		if(isset($_SESSION['member_login']))
			include ($dir_inc.'update.php');	
		break;
	case 'member':
		private_unset();
		if(isset($_SESSION['member_login']))
		include($dir_inc.'member.php');	
		else
			include ($dir_inc.'home.php');	
		break;	
		break;
	case 'reg':
		private_unset();
		if(!isset($_SESSION['member_login']))
			include ($dir_inc.'reg.php');	
		else
			{
			echo "<script>alert(\"Bạn phải đăng xuất ra mới có thể đăng ký được! thanks!\")</script>";
			include ($dir_inc.'member.php');	
			}
		break;
	case 'candidate':
		if(isset($_SESSION['member_login']))
			include ($dir_inc.'update.php');	
		break;
	case 'search':
		private_unset();
			include ($dir_inc.'search.php');				
		break;
	default:
		include ($dir_inc.'home.php');
	default:
		include ($dir_inc.'home.php');
}
if($right)
	include($dir_inc.'right.php');
	//else 
	//include($dir_inc.'right2.php');
include($dir_inc.'footer.php');

?>