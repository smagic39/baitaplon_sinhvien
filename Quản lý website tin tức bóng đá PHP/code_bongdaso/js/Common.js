﻿// Check for Browser & Platform for PC & IE specific bits
// More details from: http://www.mozilla.org/docs/web-developer/sniffer/browser_type.html
var clientPC = navigator.userAgent.toLowerCase(); // Get client info
var clientVer = parseInt(navigator.appVersion); // Get browser version
var is_ie = ((clientPC.indexOf("msie") != -1) && (clientPC.indexOf("opera") == -1));
var is_ie5 = (navigator.appVersion.indexOf("MSIE 5.5")!=-1) ? 1 : 0; 
var is_nav = ((clientPC.indexOf('mozilla')!=-1) && (clientPC.indexOf('spoofer')==-1)
                && (clientPC.indexOf('compatible') == -1) && (clientPC.indexOf('opera')==-1)
                && (clientPC.indexOf('webtv')==-1) && (clientPC.indexOf('hotjava')==-1));
var is_moz = 0;
var is_win = ((clientPC.indexOf("win")!=-1) || (clientPC.indexOf("16bit") != -1));
var is_mac = (clientPC.indexOf("mac")!=-1);
var is_opera = ((navigator.userAgent.indexOf("Opera6")!=-1)||(navigator.userAgent.indexOf("Opera/6")!=-1)) ? 1 : 0; 
var is_netscape = (navigator.userAgent.indexOf('Netscape') >= 0) ? 1 : 0; 

var FirstCallCounter = 0;
var Counter = 0;
var Interval = 1000;
var ShowStatus = false;	
	
window.setTimeout("FirstRefresh()", 1000);
window.setTimeout("showTime()", 100);
window.setTimeout("startBlink()", 200);
if(typeof startup=='function')
	window.setTimeout("startup()", 10000);
	
function FirstRefresh()
{ 
	if (__AJAXObjectList == undefined)
	{
		if (FirstCallCounter>=5)
			return;
		FirstCallCounter++;
		window.setTimeout("FirstRefresh()", 1000);			
	}
	for (var i=0;i<__AJAXObjectList.length;i++)
	{
		if (__AJAXObjectList[i].NoOfPeriods == 0)
		{
			__AJAXObjectList[i].xmlHttpObj = GetXmlHttpObject(eval('CallBackHandler' + i));             
			//Send the xmlHttp get to the specified url 
			xmlHttp_Get(__AJAXObjectList[i].xmlHttpObj, __AJAXObjectList[i].URL); 
		}
		else
			ShowStatus = true;			
	}
	window.setTimeout("RefreshMe()", 1000);
}

function RefreshMe()
{ 
	for (var i=0;i<__AJAXObjectList.length;i++)
	{
		if ((__AJAXObjectList[i].NoOfPeriods>0) && (Counter%__AJAXObjectList[i].NoOfPeriods == 0))
		{
			__AJAXObjectList[i].xmlHttpObj = GetXmlHttpObject(eval('CallBackHandler' + i));             
			//Send the xmlHttp get to the specified url 
			xmlHttp_Get(__AJAXObjectList[i].xmlHttpObj, __AJAXObjectList[i].URL); 
		}
    }
	Counter = (Counter+1)%Periods;    
	if (ShowStatus)
	{
		var Time = Periods - Counter;
		if (Time<60)
			window.status="Tự động cập nhật sau "+(Periods - Counter)+" giây";
		else
		{
			var Minute = (Time - (Time%60))/60;
			var Status = "Tự động cập nhật sau "+Minute+" phút";
			if ((Time%60)>0)
				Status += " " + (Time%60) + " giây";
			window.status = Status;
		}
	}
	window.setTimeout("RefreshMe()", Interval);    
}

// XMLHttp send GET request 
function xmlHttp_Get(xmlhttp, url) 
{ 
    xmlhttp.open('GET', url, true); 
    xmlhttp.send(null); 
} 

function GetXmlHttpObject(handler) 
{ 
    var objXmlHttp = null;    //Holds the local xmlHTTP object instance 
    //Depending on the browser, try to create the xmlHttp object 
    if (is_ie)
    { 
        //The object to create depends on version of IE 
        //If it isn't ie5, then default to the Msxml2.XMLHTTP object 
        var strObjName = (is_ie5) ? 'Microsoft.XMLHTTP' : 'Msxml2.XMLHTTP'; 
            
        //Attempt to create the object 
        try
        { 
            objXmlHttp = new ActiveXObject(strObjName); 
            objXmlHttp.onreadystatechange = handler; 
        } 
        catch(e)
        { 
        //Object creation errored 
            alert('IE detected, but object could not be created. Verify that active scripting and activeX controls are enabled'); 
            return; 
        } 
    } 
    else if (is_opera)
    { 
        //Opera has some issues with xmlHttp object functionality 
        alert('Opera detected. The page may not behave as expected.'); 
        return; 
    } 
    else
    { 
        // Mozilla | Netscape | Safari 
        objXmlHttp = new XMLHttpRequest(); 
        objXmlHttp.onload = handler; 
        objXmlHttp.onerror = handler; 
    } 
        
    //Return the instantiated object 
    return objXmlHttp; 
} 

function ShowHide(id) 
{
	var itm = null;
	if (document.getElementById) 
	{
		itm = document.getElementById(id);
	}
	else if (document.all)
	{
		itm = document.all[id];
	} 
	else if (document.layers)
	{
		itm = document.layers[id];
	}
	if (!itm) 
	{
		// do nothing
	}
	else if (itm.style) 
	{
		if (itm.style.display == "none")
		{ 
			itm.style.display = ""; 
		}
		else
		{
			itm.style.display = "none"; 
		}
	}
}

function Show(id) 
{
	var itm = null;
	if (document.getElementById) 
	{
		itm = document.getElementById(id);
	}
	else if (document.all)
	{
		itm = document.all[id];
	} 
	else if (document.layers)
	{
		itm = document.layers[id];
	}
	if (!itm) 
	{
		// do nothing
	}
	else if (itm.style) 
	{
		//alert('show:'+itm.style.display);
		if (itm.style.display == "none")
		{ 			
			itm.style.display = "inline"; 
		}
	}
}

function Hide(id) 
{
	var itm = null;
	if (document.getElementById) 
	{
		itm = document.getElementById(id);
	}
	else if (document.all)
	{
		itm = document.all[id];
	} 
	else if (document.layers)
	{
		itm = document.layers[id];
	}
	if (!itm) 
	{
		// do nothing
	}
	else if (itm.style) 
	{	
		//alert('hide:'+itm.style.display);	
		if (itm.style.display != "none")
		{ 
			itm.style.display = "none"; 
		}
	}
}

function Trim(iStr)
{
	while (iStr.charCodeAt(0) <= 32)
	{
		iStr=iStr.substr(1);
	}

	while (iStr.charCodeAt(iStr.length - 1) <= 32)
	{
		iStr=iStr.substr(0, iStr.length - 1);
	}

	return iStr;
}

function CheckEmailAddress(Email)
{
	Email = Trim(Email);

	while (Email != '')
	{
		c = Email.charAt(0);	
		if (c==' ' || c=='<' || c==39 || c==':' || c=='.')
		{
			Email = Email.substr(1);
		}
		else
		{
			break;
		}
	}

	i = Email.indexOf('>');
	if (i==-1)
	{
		while (Email != '')
		{
			c = Email.charAt(Email.length - 1);
			if (c==' ' || c==39 || c=='.')
			{
				Email = Email.substr(0, Email.length - 1);
			}
			else
			{
				break;
			}
		}
	}
	else
	{
		Email = Email.substr(0, i);
	}

	if (Email.length > 96)
		return '';

	i = Email.lastIndexOf('@');
	j = Email.lastIndexOf('.');
	if (i < j)
		i = j;

	switch (Email.length - i - 1)
	{
	case 2:
		break;
	case 3:
		switch (Email.substr(i))
		{
		case '.com':
		case '.net':
		case '.org':
		case '.edu':
		case '.mil':
		case '.gov':
		case '.biz':
		case '.pro':
		case '.int':
			break;
		default:
			return '';
		}
		break;
	default:
		switch (Email.substr(i))
		{
		case '.name':
		case '.info':
			break;
		default:
			return '';
		}
		break;
	}

	Email = Email.toLowerCase();

	if (Email == '')
		return '';

	if (Email.indexOf(' ') != -1)
		return '';

	if (Email.indexOf('..') != -1)
		return '';

	if (Email.indexOf('.@') != -1)
		return '';

	if (Email.indexOf('@.') != -1)
		return '';

	if (Email.indexOf(':') != -1)
		return '';

	for (i=0; i < Email.length; i++)
	{
		c = Email.charAt(i);

		if (c >= '0' && c <= '9')
			continue;
		
		if (c >= 'a' && c <= 'z')
			continue;
		
		if ('`~!#$%^&*-_+=?/\\|@.'.indexOf(c) != -1)
			continue;

		return '';
	}

	if ((i=Email.indexOf('@'))==-1)
		return '';

	if (Email.substr(i + 1).indexOf('@')!=-1)
		return '';

	if (Email.charAt(0)=='.' || Email.charAt(Email.length - 1)=='.')
		return '';

	return Email;
}

function findPosX(obj) {
	var curleft = 0;
	if (obj.offsetParent) {
		curleft = obj.offsetLeft;
		while (obj = obj.offsetParent) {
			curleft += obj.offsetLeft;
		}
	}
	return curleft;
}

function findPosY(obj) {
	var curtop = 0;
	if (obj.offsetParent) {
		curtop = obj.offsetTop;
		while (obj = obj.offsetParent) {
			curtop += obj.offsetTop;
		}
	}
	return curtop;
}

function SubmitForm(button,submitID) {
  if (submitID == 1)
  {	
	//search submit
	var obj = document.getElementById("SearchParam");
    var search = obj.value;
	if ( search == '')
	{
		if (document.myForm.SearchBy[0].checked)
			alert('Hãy điền tên đội bóng bạn cần tìm!');
		else
			alert('Hãy điền tên cầu thủ bạn cần tìm!');
		obj.focus();
		return 0;
	}
	else if (!isProper(search))
	{
		alert('Không sử dụng các ký tự không cho phép khi tìm kiếm!');
		obj.focus();
		return 0;
	}
  }
  else if (submitID == 2)
  {
	//vote submit
	var vote = '';
	for(var i = 0; i<20; i++) 
	{
		if (document.myForm.VoteParam[i] == undefined)
		{
			break;
		}
		if (document.myForm.VoteParam[i].checked)
		{
			vote = document.myForm.VoteParam[i].value;
			break;
		}
	}
	if (vote == '')
	{
		alert('Hãy chọn option!');
		return 0;
	}
  }  
  else if (submitID == 3)
  {
	//login submit
	var obj = document.getElementById("Alias");
    var alias = obj.value;
	if ( alias == '')
	{
		alert('Hãy điền tên đăng nhập của bạn!');
		obj.focus();
		return 0;
	}
	else if (!isProper(alias))
	{
		alert('Không sử dụng các ký tự không cho phép khi đăng nhập!');
		obj.focus();
		return 0;
	}
	obj = document.getElementById("Passwd");
    var pass = obj.value;
	if ( pass == '')
	{
		alert('Hãy điền mật khẩu đăng nhập của bạn!');
		obj.focus();
		return 0;
	}
	else if (!isProper(pass))
	{
		alert('Không sử dụng các ký tự không cho phép khi đăng nhập!');
		obj.focus();
		return 0;
	}	
  }    

  var obj = document.getElementById("SubmitID");
  obj.value = submitID; 
  button.style.visibility = "hidden";
  document.myForm.target = "_self";
  return 1;
}

function ShowPollResult(RegularVoteID)
{
	vWW = 560;
	vWH = 250;

	vWN = 'ShowPollResult';
	winDef = 'status=no,resizable=no,scrollbars=no,toolbar=no,location=no,fullscreen=no,titlebar=yes,height='.concat(vWH).concat(',').concat('width=').concat(vWW).concat(',');
	winDef = winDef.concat('top=').concat((screen.height - vWH)/2).concat(',');
	winDef = winDef.concat('left=').concat((screen.width - vWW)/2);
	newwin = open('', vWN, winDef);

	var action = RegularVoteID + "";
	action = "http://www.bongdaso.com/Chart/ShowPollResult.aspx?ID=" + action;
	document.myForm.action = action;
	document.myForm.target = vWN;
	window.setTimeout("ClearShowPollResult()", 300);
	return;
}
       
function ClearShowPollResult()
{
	document.myForm.action = '';
	document.myForm.target = '';
}         

function OnKeyPress(tb)
{
	// If it is the Enter key -> simulating Click on the default button
	if (event.keyCode == 13)
	{
		// cancel the default submit
		event.returnValue=false;
		event.cancel = true;
		// submit the form by programmatically clicking the specified button
		var obj = document.getElementById("SearchSubmit"); 
		if (obj != null) 
			obj.click();
	}
}

// Function to disallow certain characters in search string
function isProper(string) {
  if (!string) return false;
  var iChars = "*|,\":<>[]{}`;@#%^+?\\";
  for (var i = 0; i < string.length; i++) {
   if (iChars.indexOf(string.charAt(i)) != -1)
     return false;
  }
  return true;
}

function getTime(diff)
{
	var time = new Date();
	//Adjust summer-winter time
	var DLSTime = new Date();
	var gmtMS = time.getTime() + ((time.getTimezoneOffset()+diff*60) * 60000);	//winter time
	//var gmtMS = time.getTime() + ((time.getTimezoneOffset()+(diff+1)*60) * 60000);	//summer time
	var gmtTime =  new Date(gmtMS);
	var hr = gmtTime.getHours();
	var min = gmtTime.getMinutes();
	var sec = gmtTime.getSeconds();

	hr = ((hr < 10) ? " " : "") + hr;
	min = ((min < 10) ? "0" : "") + min;
	sec = ((sec < 10) ? "0" : "") + sec;
	return hr + ":" + min;
}

function showTime()
{
	for (var tz = 0; tz<4 ; tz++)
	{
		for (var i = 1; i < 100; i++)
		{
			var clock_name = "time" + tz + "_" + i;
			var obj = document.getElementById(clock_name);
			if (obj == null)
				break;
			obj.innerHTML = getTime(tz);
		}
	}
	setTimeout("showTime()", 1000);
}

function doBlink() 
{
  var blink = document.all.tags("BLINK")
  for (var i=0; i < blink.length; i++)
    blink[i].style.visibility = blink[i].style.visibility == "" ? "hidden" : "";
}

function startBlink() 
{
  // Make sure it is IE4
  if (document.all)
    setInterval("doBlink()",1000);
}

function ShowAnnouncement()
{
	window.open('Announcement.htm','annoucement','width=550,height=480,menubar=no,status=yes,location=no,toolbar=no,scrollbars=no,resizable=yes');
}
//cookie manipulation functions

function getCookie( name ) 
{	
	var start = document.cookie.indexOf( name + "=" );
	var len = start + name.length + 1;
	if ( ( !start ) &&	( name != document.cookie.substring( 0, name.length ) ) )
	{
		return null;
	}
	if ( start == -1 ) 
		return null;
	var end = document.cookie.indexOf( ";", len );
	if ( end == -1 ) 
		end = document.cookie.length;
	return unescape( document.cookie.substring( len, end ) );
}

function checkVoting(voting_cookie_name)
{
	var obj = document.getElementById('VoteSubmit');
	if (obj != null)
	{
		var cname = getCookie(voting_cookie_name);
		if (cname != null)
		{
			obj.disabled = true;
		}
	}
}


// Preload
var image_more = new Image(); 
image_more.src = "images/art_more.gif"; 
var image_less = new Image(); 
image_less.src = "images/art_less.gif"; 

// image swap
function swapImages(img) 
{ 
    if (img.src.indexOf(image_more.src)>=0) 
    {
		img.src = image_less.src; 
		img.alt = 'Thu hẹp';
		img.title = 'Thu hẹp';
	}
    else 
    {
		img.src = image_more.src; 
		img.alt = 'Mở rộng';
		img.title = 'Mở rộng';
	}
}

function textOnOff() 
{
	for (var i=0; i < arguments.length; i++) 
	{
		var item = document.getElementById(arguments[i]);
		if (item.className=="textOn") 
		{
			item.className= "textOff";
		} else 
		{ 
			item.className= "textOn";
		}
	}	
}

function changeImage(ImageID, ImageURL)
{
    var img = document.createElement('img');
    img.onload = function (evt) {
        document.getElementById(ImageID).src=this.src;
        document.getElementById(ImageID).width=this.width;
        document.getElementById(ImageID).height=this.height;
    }
    img.src = ImageURL;
    return false;
}
