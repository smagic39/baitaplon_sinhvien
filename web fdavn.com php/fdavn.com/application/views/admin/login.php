<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="Gia công mỹ phẩm trọn gói" />
<title>Gia công mỹ phẩm trọn gói - Đăng nhập</title>
<link rel="shortcut icon" href="<?php echo base_url('public/css_front/images/favicon.ico'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css_admin/login.css'); ?>" media="screen" />

</head>
<body>
<div class="wrap">
	<div id="content">
		<div id="main">
        	
           
			<div class="full_w">
            	<div class="forget_box">
            		<div class="forget_h2"><h2>Đăng nhập quản trị</h2></div>
                </div>
				<?php echo form_open('cm-admin/kiem-tra-dang-nhap'); ?>
                	<?php
						echo $this->session->flashdata('mss');
					?>
					<label class="user-icon" for="login">Tên đăng nhập:</label>
					<input id="login" name="tendn" class="text" required="required" />
					<label class="pass-icon" for="pass">Mật khẩu:</label>
					<input id="pass" name="matkhau" type="password" class="text" required="required" />
					<button type="submit" class="ok">Đăng nhập</button>
                    <a class="button forget" href="<?php echo base_url('cm-admin/forget-password'); ?>">Quên mật khẩu?</a>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>

</body>
</html>
