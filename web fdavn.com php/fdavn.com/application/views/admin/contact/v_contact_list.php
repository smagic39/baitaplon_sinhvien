<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css_admin/jquery.dataTables.css'); ?>"/>
<script type="text/javascript" src="<?php echo base_url('public/js_admin/jquery.dataTables.js'); ?>"></script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#table').dataTable({"sPaginationType": "full_numbers"});
	} );
</script>
<div class="full_w">
        <div class="h_title h_contact">Danh sách tin nhắn liên hệ</div>
        <?php if(count($contact)>0 && $contact!= null){ ?>
        <table id="table">
            <thead>
                <tr>
                    <th scope="col">STT</th>
                    <th scope="col">Họ tên</th>
                    <th scope="col">Email</th>
                    <th scope="col">Nội dung</th>
                    <th scope="col" style="width: 70px;"></th>
                </tr>
            </thead>
            <tbody>
            	<?php $count = 0; ?>
            	<?php foreach($contact as $ct){ ?> 
                <?php $count += 1; $this->load->helper('array'); ?>          
                <tr>
                    <td class="align-center"><?php echo $count; ?></td>
                    <td style="padding-left:10px;"><?php echo $ct->fullname; ?></td>
                    <td style="padding-left:10px;"><?php echo $ct->email; ?></td>
                    <td style="padding-left:10px;"><?php echo character_limiter($ct->content, 75); ?></td>
                    <td style="text-align:center">
                        <a href="<?php echo base_url('cm-admin/edit-contact/'.$ct->id); ?>" class="table-icon edit" title="Chỉnh sửa" style="margin-right:5px"></a>
                        <a href="<?php echo base_url('cm-admin/delete-contact/'.$ct->id); ?>" class="table-icon delete" title="Xóa"></a>
                    </td>
                </tr>
                <?php }	?>
            </tbody>
        </table>
        <div style="clear:both"></div>
        <?php }else{
				echo '<div class="no-entry">Chưa có tin nhắn liên hệ nào</div>';
			} ?>
    </div>