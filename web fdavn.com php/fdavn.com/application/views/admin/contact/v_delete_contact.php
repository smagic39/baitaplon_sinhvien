<div class="full_w">
    <div class="h_title h_contact">Xóa tin nhắn liên hệ của: <?php echo $contact->fullname; ?></div>
    <?php echo form_open(''); ?>
        <div class="element">
            <label for="delete">Bạn thật sự muốn xóa tin nhắn này?</label>
            <?php echo form_radio('delete', 'no', TRUE); ?> Không
            <?php echo form_radio('delete', 'yes', FALSE); ?> Có 
        </div>
        <div class="entry" style="margin-top:10px">
        	<input type="hidden" name="action" value="Delete contact"/>
            <button type="submit" class="btnUpdate">Xóa</button> 
            <button type="button" class="cancel" onClick="window.history.back();">Hủy bỏ</button>
        </div>
    <?php echo form_close(); ?>
    </form>
</div>