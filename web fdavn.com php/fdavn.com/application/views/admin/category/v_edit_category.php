<script type="text/javascript" src="<?php echo base_url('ckeditor/ckeditor.js'); ?>" language="javascript"></script>
<script src="<?php echo base_url('public/js_admin/jquery.validate.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function () {
	   $("#insertform").validate({
		   rules: {
			   cat_name:{
				   required:true,
				   rangelength:[1,150]
			   },
			   cat_brief:"required",
			   cat_content:"required",
			   title_page:{rangelength:[1,100]},
			   description_page:{rangelength:[1,200]},
			   keyword_page:{rangelength:[1,200]}
			},
		   messages:{
			   cat_name:{rangelength:"Vui lòng nhập tối đa 150 ký tự"},
			   title_page:{rangelength:"Vui lòng nhập tối đa 100 ký tự"},
			   description_page:{rangelength:"Vui lòng nhập tối đa 200 ký tự"},
			   keyword_page:{rangelength:"Vui lòng nhập tối đa 200 ký tự"}
		   }
	   });
	});
</script>
<div class="full_w">
    <div class="h_title h_service">Cập nhật dịch vụ: <?php echo $edit_category->cat_name; ?></div>
    <?php 
		if(isset($cat_name_error)){echo $cat_name_error;}
		echo $this->session->flashdata('mss');
		$attributes = array('id' => 'insertform');
		echo form_open_multipart('',$attributes);
	?>
    <script type="text/javascript">
		$(document).ready(function(){
			$('.tabs_content:not(:first)').hide();
			$('.tabs li a').click(function(){
				$('.tabs li a').removeClass('active');
				$(this).addClass('active');
				$('.tabs_content').hide();
				var activeTab = $(this).attr('href');
				$(activeTab).fadeIn();
				return false;
			});
		});
	</script>
    	<div class="sub_tab">
        	<ul class="tabs">
                <li><a href="#chung" class="active">Chung</a></li>
                <li><a href="#seo">SEO</a></li>
            </ul>
        </div>
    
    	<div class="tab_container">
            <div id="chung" class="tabs_content">
                <input type="hidden" name="hidden_hinh" value="<?php echo $edit_category->cat_pic; ?>"/>
                <div class="element">
                    <label for="cat_name">Tên dịch vụ <span class="red">(*)</span></label>
                    <?php
                        $data=array('name'=>'cat_name','id'=>'cat_name','class'=>'text','value'=>set_value('cat_name',$edit_category->cat_name));
                        echo form_input($data);
                    ?>
                </div>
                <div class="element">
                    <label for="cat_brief">Nội dung tóm tắt <span class="red">(*)</span></label>
                    <?php
                        $data=array('name'=>'cat_brief','id'=>'cat_brief','value'=>set_value('cat_brief',$edit_category->cat_brief));
                        echo form_textarea($data);
                    ?>  
                    <script type="text/javascript">CKEDITOR.replace('cat_brief'); </script>  
                </div>
                <div class="element">
                    <label for="cat_content">Nội dung chi tiết <span class="red">(*)</span></label>
                    <?php
                        $data=array('name'=>'cat_content','id'=>'cat_content','value'=>set_value('cat_content',$edit_category->cat_content));
                        echo form_textarea($data);
                    ?>  
                    <script type="text/javascript">CKEDITOR.replace('cat_content'); </script>  
                </div>
                <div class="element">
                    <label for="cat_pic">Hình ảnh đại diện <span class="red">(*)</span></label>
                    <?php
                        $manghinh = explode('|',$edit_category->cat_pic);
                        for($i=0;$i<count($manghinh);$i++){
                            echo '<img src="'.base_url('public/category_images/'.$manghinh[$i]).'" width="150px" height="70px" style="margin-right:5px; margin-bottom:5px"/>';	
                        }
                    ?>
                    <script language="javascript" type="text/javascript">
                        function tao_files_upload()
                        {
                            so_hinh=document.getElementById("so_hinh").value;
                            if(so_hinh>0)
                            {
                                chuoi="";
                                for(i=0;i<so_hinh;i++)
                                {
                                    chuoi+= "<p><input type='file' name='file" + i + "' size='41' /></p>";
                                }
                                document.getElementById("tao_file").innerHTML=chuoi;
                            }
                            else
                                document.getElementById("tao_file").innerHTML="<p><input type='file' name='file0' size='41' /></p>";
                        }
                    </script>
                    <div style="border:1px solid #ccc; padding: 0px 10px 20px 60px">
                        <p style="line-height:1.6; color:#FF5252">
                            -- Những hình ảnh này sẽ được sử dụng để hiển thị ngoài Trang chủ,<br/>
                            vì thế bạn nên sử dụng những hình ảnh có kích thước nhỏ để tăng tốc độ tải website.<br/>
                            -- Kích thước ảnh tốt nhất là <strong>200px - 200px, ảnh nền trong suốt</strong>, tham khảo http://demo1.homevn.vn/khangmedia/public/category_images/1386389063cung-cap-my-pham.png
                        </p>
                        <p style="margin-top:20px">Số hình cần upload:
                        <input type="text" name="so_hinh" id="so_hinh" value="1" style="width: 130px; height:22px; margin-right:5px;">
                        <input type="button" onClick="tao_files_upload()" value="Tạo"></p>
                        <div id="tao_file" name="tao_file">
                            <input type="file" name="file0" size="41" />
                        </div>
                    </div>
                </div>
                 <div class="element">
                    <label for="status">Trạng thái</label>
                    <select name="status">
                        <option value="1" <?php if($edit_category->status == 1) echo 'selected="selected"'; ?> >Hiện</option>
                        <option value="0" <?php if($edit_category->status == 0) echo 'selected="selected"'; ?>>Ẩn</option>
                    </select>
                </div>
                <div class="element">
                    <label for="position">Thứ tự <span class="red">(bắt buộc)</span></label>
                    <select name="position">
                        <option value="0">Chọn thứ tự</option>
                        <?php
                            for($i=1;$i<=$thutu;$i++){
                                echo '<option value="'.$i.'"';
                                    if($edit_category->position == $i) echo 'selected="selected"';
                                echo '>'.$i.'</option>';
                            }
                        ?>
                    </select>
                </div>
        	</div><!-- end #chung -->
            <div id="seo" class="tabs_content">
        		<div class="element">
                    <label for="title_page">Tiêu đề trang <span class="red">(Chiều dài tốt nhất từ 10-70 ký tự)</span></label>
                    <?php
                        $data=array('name'=>'title_page','id'=>'title_page','class'=>'text','value'=>set_value('title_page',$edit_category->title_page));
                        echo form_input($data);
                    ?>
                </div>
                <div class="element">
                    <label for="description_page">Mô tả trang <span class="red">(Chiều dài tốt nhất từ 70-160 ký tự)</span></label>
                    <?php
                        $data=array('name'=>'description_page','id'=>'description_page','rows'=>'3','value'=>set_value('description_page',$edit_category->description_page));
                        echo form_textarea($data);
                    ?>
                </div>
                <div class="element">
                    <label for="keyword_page">Từ khoá trang </label>
                    <?php
                        $data=array('name'=>'keyword_page','id'=>'keyword_page','rows'=>'3','value'=>set_value('keyword_page',$edit_category->keyword_page));
                        echo form_textarea($data);
                    ?> 
                </div>
            </div><!-- end #seo -->
        </div>
        
        
        <div class="entry" style="margin-top:10px">
        	<input type="hidden" name="action" value="Edit category"/>
            <button type="submit" class="update">Cập nhật</button> 
            <button type="button" class="cancel" onClick="window.history.back();">Hủy bỏ</button>
        </div>
    <?php echo form_close(); ?>
    </form>
</div>