<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">

<head>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta name="author" content="Gia công mỹ phẩm trọn gói" />

<title>Quản trị Website</title>

<link rel="shortcut icon" href="<?php echo base_url('public/css_front/images/favicon.ico'); ?>">

<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css_admin/style.css'); ?>" media="screen" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css_admin/navi.css'); ?>" media="screen" />

<script type="text/javascript" src="<?php echo base_url('public/js_admin/jquery-1.7.2.min.js'); ?>"></script>

<script type="text/javascript">

$(function(){

	$(".box .h_title").not(this).next("ul").hide("normal");

	$(".box .h_title").not(this).next("#home").show("normal");

	$(".box").children(".h_title").click( function() { $(this).next("ul").slideToggle(); });

});

</script>

<?php date_default_timezone_set('Asia/Ho_Chi_Minh'); ?>

</head>

<body>

<div class="wrap">

	<div id="header">

		<div id="top">

			<div class="left">

				<p class="sitename"><a href="<?php echo base_url(); ?>">Admin Manager</a></p>

			</div>

			<div class="right">

				<div class="align-right">

                	<p class="logout-top"><a href="<?php echo base_url("cm-admin/thoat"); ?>">Thoát</a></p>

                    <p class="user-top"><a href="<?php echo base_url("cm-admin/chi-tiet-tai-khoan/".$this->session->userdata('ma_nguoi_dung')); ?>">Thông tin tài khoản</a></p>

				</div>

			</div>

		</div>

		

	</div>

	

	<div id="content">

    	<div id="sidebar">

			<ul class="navi">

            	<li <?php if($this->uri->segment(2) == 'he-thong-quan-tri' || $this->uri->segment(2) == '') echo "class='active'"; ?>><a class="bangdieukhien sidebarhover" href="<?php echo base_url('cm-admin'); ?>">Bảng điều khiển</a></li>

				<li class="lihover <?php if(isset($tk_active)) echo "active"; ?>"><a class="taikhoan sidebarhover">Tài khoản</a>

                	<ul>

                    	<li><a href="<?php echo base_url('cm-admin/danh-sach-tai-khoan'); ?>">Xem tài khoản</a></li>

                        <li><a href="<?php echo base_url('cm-admin/them-tai-khoan'); ?>">Thêm tài khoản</a></li>

                    </ul>

                </li>

                <li <?php if(isset($menu_active)) echo "class='active'"; ?>><a href="<?php echo base_url('cm-admin/danh-sach-menu'); ?>" class="menu sidebarhover">Menu</a></li>

                <li <?php if(isset($cat_active)) echo "class='active'"; ?>><a class="dichvu sidebarhover" href="<?php echo base_url('cm-admin/category-list'); ?>">Dịch vụ</a></li>

                <li class="lihover <?php if(isset($pro_active)) echo "active"; ?>"><a class="sanpham sidebarhover">Sản phẩm</a>

                	<ul>

						<li><a href="<?php echo base_url('cm-admin/product-list'); ?>">Danh sách sản phẩm</a></li>

                        <li><a href="<?php echo base_url('cm-admin/add-product'); ?>">Thêm sản phẩm</a></li>

					</ul>

                </li>

                <li class="lihover <?php if(isset($sl_active)) echo "active"; ?>"><a class="slider sidebarhover">Slider</a>

                	<ul>

						<li><a href="<?php echo base_url('cm-admin/slider-list'); ?>">Danh sách slider</a></li>

						<li><a href="<?php echo base_url('cm-admin/add-slider'); ?>">Thêm slider</a></li>

                    </ul>

                </li>

                <li <?php if($this->uri->segment(2) == 'background') echo "class='active'"; ?>><a class="mausac sidebarhover" href="<?php echo base_url('cm-admin/background/1'); ?>">Màu sắc</a></li>

                <li <?php if(isset($ct_active)) echo "class='active'"; ?>><a class="lienhe sidebarhover" href="<?php echo base_url('cm-admin/contact-list'); ?>">Liên hệ</a></li>

                <li <?php if($this->uri->segment(2) == 'setting') echo "class='active'"; ?>><a class="cauhinh sidebarhover" href="<?php echo base_url('cm-admin/setting/1'); ?>">Cấu hình</a></li>

        	</ul>

            

            <script type="text/javascript">

				$(".lihover").click(function(e){

					$(this).find("ul").slideToggle(350);

				  });

			</script>

		</div><!-- end #sidebar -->

        

        

        

		<div id="main">

			<?php $this->load->view($path); ?>

		</div>

		<div class="clear"></div>

	</div>



	<div id="footer">

		<div class="left">

			<p>Thiết kế bởi : <a href="http://www.homevn.net/">HomeVN</a></p>

		</div>

		<div class="right">

			<p></p>

		</div>

	</div>

</div>



</body>

</html>

