<?php
	function dmy_to_ymd($ngay) //chuyển ngày/tháng/năm thành năm-tháng-ngày
	{
		$kq = '';
		$mang = explode('/', $ngay);
		$kq = "{$mang[2]}-{$mang[1]}-{$mang[0]}";		
		return $kq;
	}
	function mdy_to_ymd($ngay) //chuyển tháng/ngày/năm thành năm-tháng-ngày
	{
		$kq = '';
		$mang = explode('/', $ngay);
		$kq = "{$mang[2]}-{$mang[0]}-{$mang[1]}";
		return $kq;
	}
	function mdy_to_dmy($ngay) //chuyển tháng/ngày/năm thành ngày/tháng/năm
	{
		$kq = '';
		$mang = explode('/', $ngay);
		$kq = "{$mang[1]}/{$mang[0]}/{$mang[2]}";
		return $kq;
	}
	function ymd_to_dmy($ngay) //chuyển năm-tháng-ngày thành ngày/tháng/năm
	{
		$kq = '';
		if(strlen($ngay)>10)
		{
			$mang = explode(' ', $ngay);
			$ngay = $mang[0];
		}
		$mang = explode('-', $ngay);
		$kq = "{$mang[2]}/{$mang[1]}/{$mang[0]}";
		
		return $kq;
	}
	function ymd_to_mdy($ngay) //chuyển năm-tháng-ngày thành tháng/ngày/năm
	{
		$kq = '';
		if(strlen($ngay)>10)
		{
			$mang = explode(' ', $ngay);
			$ngay = $mang[0];
		}
		$mang = explode('-', $ngay);
		$kq = "{$mang[1]}/{$mang[2]}/{$mang[0]}";
		return $kq;
	}

?>
