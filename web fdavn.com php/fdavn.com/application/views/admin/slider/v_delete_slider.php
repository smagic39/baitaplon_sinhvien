<div class="full_w">
    <div class="h_title h_slider">Xóa slider: <?php echo $slider->sl_title; ?></div>
    <?php echo form_open(''); ?>
        <div class="element">
            <input type="hidden" name="hidden_hinh" value="<?php echo $slider->sl_image; ?>" />
            <label for="delete">Bạn thật sự muốn xóa slider này?</label>
            <?php echo form_radio('delete', 'no', TRUE); ?> Không
            <?php echo form_radio('delete', 'yes', FALSE); ?> Có 
        </div>
        <div class="entry" style="margin-top:10px">
        	<input type="hidden" name="action" value="Delete slider"/>
            <button type="submit" class="btnUpdate">Xóa</button> 
            <button type="button" class="cancel" onClick="window.history.back();">Hủy bỏ</button>
        </div>
    <?php echo form_close(); ?>
    </form>
</div>