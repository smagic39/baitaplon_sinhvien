<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Gia công mỹ phẩm trọn gói - Quên mật khẩu</title>
<link rel="shortcut icon" href="<?php echo base_url('public/css_front/images/favicon.ico'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css_admin/login.css'); ?>" media="screen" />

</head>
<body>
<div class="wrap">
	<div id="content">
		<div id="main">
			<div class="full_w">
            	<div class="forget_box" style="background:#b91c47">
            		<div class="forget_h2"><h2>Quên mật khẩu</h2></div>
                    <div class="sep"></div>
                    <p style="color:#f2f2f2">Bạn quên mật khẩu đăng nhập? Xin hãy nhập địa chỉ Email đăng ký thành viên ở đây. Chúng tôi sẽ gửi mật khẩu mới cho bạn qua Email.            
                    </p>
                </div>
				<?php echo form_open(''); ?>
                	<?php
						echo $this->session->flashdata('mss');
					?>
					<label class="email-icon" for="email">Địa chỉ Email:</label>
					<input id="email" name="email" class="text" required="required" />
					<div class="sep"></div>
					<button type="submit" class="gyc">Gửi yêu cầu</button>
                    <a class="button forget" href="<?php echo base_url('cm-admin'); ?>">Đăng nhập</a>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>

</body>
</html>
