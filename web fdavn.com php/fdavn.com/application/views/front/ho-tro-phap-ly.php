<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<?php echo base_url(); ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo @$title_page; ?></title>
<link rel="shortcut icon" href="<?php echo base_url('public/css_front/images/favicon.ico'); ?>">
<meta content="<?php echo @$keyword_page; ?>" name="keywords" />
<meta content="<?php echo @$description_page; ?>" name="description" />
<link rel="stylesheet" href="public/css_front/cung-cap-my-pham.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="public/css_front/style.css" media="all" />
<script src="public/js_front/javascript_757c080409.js" type="text/javascript"></script>
<link rel='stylesheet' type='text/css' href='public/css_front/menu.css' />
<!-- Skitter Styles -->
<link href="public/css_front/skitter.styles.css" type="text/css" media="all" rel="stylesheet" />
<!-- Skitter JS -->

<script src="public/js_front/jquery-1.6.2.min.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="public/js_front/jquery.easing.1.3.js"></script>
<script type="text/javascript" language="javascript" src="public/js_front/jquery.animate-colors-min.js"></script>
<script type="text/javascript" language="javascript" src="public/js_front/jquery.skitter.min.js"></script>
<script> (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); ga('create', '<?php if($chi_tiet_cau_hinh->analytics != '') echo $chi_tiet_cau_hinh->analytics; ?>', 'giacongmyphamtrongoi.com'); ga('send', 'pageview');</script>
<!-- Init Skitter -->
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $('.box_skitter_large').skitter({
            theme: 'clean',
            numbers_align: 'center',
            dots: true, 
            preview: true,
			interval: 7000
        });
    });
</script>
<script type="text/javascript">
$(function(){
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) $('#goTop').fadeIn();
		else $('#goTop').fadeOut();
	});
	$('#goTop').click(function () {
		$('body,html').animate({scrollTop: 0}, 'slow');
	});
});
</script>
</head>

<body>
	
    <div id="header">
        <div id="logo">
            <h1><a href="">Gia công mỹ phẩm trọn gói</a></h1>
        </div><!-- end #logo -->
        <div id="nav">
			<?php if(isset($menu) && $menu != null){ ?>
            <ul id="main-nav">
                <?php foreach($menu as $row){ ?>
                    <li><a <?php if($row->link != '') echo 'href="'.base_url($row->link).'"'; ?>><?php echo $row->tenmenu; ?></a>
                        <?php if(isset($category) && $category != null && $row->idmenu == 5){ ?>
                            <ul>
                                <?php
                                    $this->load->view('front/include/Functions');
                                    foreach($category as $row){
                                        $khongdau = khongdau($row->cat_name);
                                ?>
                                    <li><a href="<?php echo 'dich-vu/'.$khongdau.'-'.$row->cat_id.'.html'; ?>"><?php echo $row->cat_name; ?></a></li>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                    </li>
                <?php } ?>
            </ul>
            <?php } ?>
            
        </div><!-- end #nav -->
    </div><!-- end #header -->
    <div class="clear"></div>
    
    <div id="slideshow-chailo">
    	<div class="box_skitter box_skitter_large box_skitter-02">
            <?php if(isset($slider) && $slider != null){ ?>
            <ul>
            	<?php foreach($slider as $row)
				{
                	echo '<li><a href="'.$row->sl_link.'"><img src="public/slide_images/'.$row->sl_image.'" class="random"/></a></li>';
                }
                ?>
				
            </ul>
            <?php } ?>
        </div>
    </div><!-- end #slideshow -->
    
    
    <div id="container" style="background: <?php if($background->phap_ly != '') echo '#'.$background->phap_ly; ?>">
        <div class="service-box">
            <?php echo $chitietloai->cat_content; ?>
        </div>
    </div><!-- end #container -->
        
    <div class="clear"></div>
    <div id="footer" style="background: <?php if($background->footer != '') echo '#'.$background->footer; ?>">
    	<div id="main-foot">
            <div id="logo-foot">
                <img src="public/css_front/images/logo-foot-white.png" />
            </div><!-- end #logo-foot -->
            <div class="footer-content">
            	<?php echo $chi_tiet_cau_hinh->footer; ?>
            </div><!-- end #footer-content -->
            <div class="social">
            	<a href="<?php echo $chi_tiet_cau_hinh->facebook_acc; ?>"><img src="public/css_front/images/facebook.png" /></a>
                <a href="<?php echo $chi_tiet_cau_hinh->google_acc; ?>"><img src="public/css_front/images/google.png" /></a>
                <a href="<?php echo $chi_tiet_cau_hinh->twitter_acc; ?>"><img src="public/css_front/images/twitter.png" /></a>
               </div><!-- end #social -->

     <div class="author">Designed by FDA Corp</div>
       
            <div class="clear"></div>
        </div>
    </div><!-- end #footer -->
    <script type="text/javascript">
$(function(){
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) $('#goTop').fadeIn();
		else $('#goTop').fadeOut();
	});
	$('#goTop').click(function () {
		$('body,html').animate({scrollTop: 0}, 'slow');
	});
});
</script>
   <div id="goTop"><img src="public/css_front/images/gotop.png" alt="về đầu trang" title='về đầu trang' /></div>
</body>
</html>
