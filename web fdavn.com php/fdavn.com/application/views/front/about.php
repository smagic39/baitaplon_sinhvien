<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<?php echo base_url(); ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="<?php echo base_url('public/css_front/images/favicon.ico'); ?>">
<title><?php echo @$title_page; ?></title>
<meta content="<?php echo @$keyword_page; ?>" name="keywords" />
<meta content="<?php echo @$description_page; ?>" name="description" />
<link rel="stylesheet" href="public/css_front/about.css" type="text/css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) $('#goTop').fadeIn();
		else $('#goTop').fadeOut();
	});
	$('#goTop').click(function () {
		$('body,html').animate({scrollTop: 0}, 'slow');
	});
});
</script>
<link rel='stylesheet' type='text/css' href='public/css_front/menu.css' /><script> (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); ga('create', '<?php if($chi_tiet_cau_hinh->analytics != '') echo $chi_tiet_cau_hinh->analytics; ?>', 'giacongmyphamtrongoi.com'); ga('send', 'pageview');</script>
<link rel='stylesheet' type='text/css' href='public/css_front/style.css' />
</head>

<body class="about-bd">
<div id="wrapper">	
    <div id="header">
        <div id="logo">
            <h1><a href="">Gia công mỹ phẩm trọn gói</a></h1>
        </div><!-- end #logo -->
        <div id="nav">
			<?php if(isset($menu) && $menu != null){ ?>
            <ul id="main-nav">
                <?php foreach($menu as $row){ ?>
                    <li><a <?php if($row->link != '') echo 'href="'.base_url($row->link).'"'; ?>><?php echo $row->tenmenu; ?></a>
                        <?php if(isset($category) && $category != null && $row->idmenu == 5){ ?>
                            <ul>
                                <?php
                                    $this->load->view('front/include/Functions');
                                    foreach($category as $row){
                                        $khongdau = khongdau($row->cat_name);
                                ?>
                                    <li><a href="<?php echo 'dich-vu/'.$khongdau.'-'.$row->cat_id; ?>"><?php echo $row->cat_name; ?></a></li>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                    </li>
                <?php } ?>
            </ul>
            <?php } ?>
            
        </div><!-- end #nav -->
    </div><!-- end #header -->
    <div class="clear"></div>    
    
    <div id="about">
        <div class="about-intro">
        	<?php echo $chi_tiet_cau_hinh->about; ?>
        </div><!-- end #about-intro -->
        <div class="about-contact">
        	<?php
				if($background->about_img != ''){
			?>
           <img src="public/css_front/images/<?php echo $background->about_img; ?>" /><br />
           <?php
				}
			?>
           <div class="ab-contact">Liên hệ</div>
           <div style="float:left">
               <div class="ab-phone ab-info">
               		<?php
						$phone_arr = explode('#',$chi_tiet_cau_hinh->phone);
						$phone='';
						for($i=0;$i<count($phone_arr);$i++){
							$phone = $phone.$phone_arr[$i].' - ';
						}
						$phone = substr($phone, 0, strlen($phone)-3);
						echo $phone;
					?>
                </div>
                <div class="ab-email ab-info">
                    <?php echo $chi_tiet_cau_hinh->receive_email; ?>
                </div>
                <div class="ab-website ab-info">
                    <?php echo base_url(); ?>
                </div>
             </div>
        </div><!-- end #contact-direct -->
    </div><!-- end #about -->
  </div>      
    <div class="clear"></div>
    
    <?php
		if(isset($chi_tiet_cau_hinh->about1) && $chi_tiet_cau_hinh->about1 != ''){
        	echo '<div class="about-box1" style="background: #'.$background->about1.'"><div style="width:1026px; margin:0 auto">'.$chi_tiet_cau_hinh->about1.'</div></div>';
		}
	?>
    <?php
		if(isset($chi_tiet_cau_hinh->about2) && $chi_tiet_cau_hinh->about2 != ''){
        	echo '<div class="about-box2" style="background: #'.$background->about2.'"><div style="width:1026px; margin:0 auto">'.$chi_tiet_cau_hinh->about2.'</div></div>';
		}
	?>
       
    <div id="footer" style="background: <?php if($background->footer != '') echo '#'.$background->footer; ?>">
    	<div id="main-foot">
            <div id="logo-foot">
                <img src="public/css_front/images/logo-foot-white.png" />
            </div><!-- end #logo-foot -->
            <div class="footer-content">
            	<?php echo $chi_tiet_cau_hinh->footer; ?>
            </div><!-- end #footer-content -->
            <div class="social">
            	<a href="<?php echo $chi_tiet_cau_hinh->facebook_acc; ?>"><img src="public/css_front/images/facebook.png" /></a>
                <a href="<?php echo $chi_tiet_cau_hinh->google_acc; ?>"><img src="public/css_front/images/google.png" /></a>
                <a href="<?php echo $chi_tiet_cau_hinh->twitter_acc; ?>"><img src="public/css_front/images/twitter.png" /></a>
               </div><!-- end #social -->

     <div class="author">Designed by FDA Corp</div>
           
            <div class="clear"></div>
        </div>
    </div><!-- end #footer -->
 <div id="goTop"><img src="public/css_front/images/gotop.png" alt="về đầu trang" title='về đầu trang' /></div>
</body>
</html>
