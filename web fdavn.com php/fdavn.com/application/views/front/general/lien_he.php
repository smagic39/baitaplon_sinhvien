<script src="<?php echo base_url('public/js_admin/jquery.validate.js'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function () {
	   $("#formContact").validate({
		   rules: {
			   tendaydu:"required",
			   email:{
				   required:true,
				   email:true
			   },
			   dienthoai:{
				   required:true,
				   number:true,
				   rangelength:[8,12]
			   },
			   diachi:"required",
			   noidung:{
				   required:true,
				   rangelength:[1,360]
			   },
			   ma_xt:"required",
		   },
		   messages:{
			  tendaydu:{required:"Phải nhập tên đầy đủ"},
			  email:{
				  required:"Phải nhập email",
				  email:"Phải nhập email hợp lệ"
			  },
			  dienthoai:{
				  required:"Phải nhập điện thoại",
				  number:"Điện thoại phải là số",
				  rangelength:"Điện thoại phải từ 8-12 số"
			  },
			  diachi:{required:"Phải nhập địa chỉ"},
			  noidung:{
				  required:"Phải nhập nội dung",
				  rangelength:"Vui lòng nhập 1-360 ký tự"
			  },
			  ma_xt:{required:"Phải nhập mã xác nhận"}
		   }
	   });
	});
</script>
<div id="vnt-container">
  <div class="content-bg">&nbsp;</div>
  <div class="clear"></div>
  <div id="vnt-content">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tbody>
        <tr>
          <td id="vnt-sidebar"><div class="box_datetime">Thứ tư, 06/11/2013 - <span id="ext_time">14:16</span> GMT+7</div>
            <script language="javascript">
				var mydate=new Date()             
				var hour = mydate.getHours()
				if (hour < 10)
				hour = "0" + hour
				var minute=mydate.getMinutes()
				if (minute < 10)
				minute = "0" + minute 
				var text_time = hour+":"+minute ;
				$("#ext_time").html(text_time);	
			</script>
            <?php
				if(isset($diachivanphong) && $diachivanphong != null){
					echo $diachivanphong;
				}
			?>
          </td>
          <td id="vnt-main"><div class="box_mid">
              <div class="boxM-tl fixPNG">
                <div class="boxM-tr fixPNG">
                  <div class="boxM-t"></div>
                </div>
              </div>
              <div class="boxM">
                <div class="mid-title">
                  <div class="titleL">
                    <h1>CÔNG TY CỔ PHẦN CÁP NHỰA VĨNH KHÁNH</h1>
                  </div>
                  <div class="titleR">
                    <div class="more"></div>
                  </div>
                  <div class="clear"></div>
                </div>
                <div class="mid-content">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                      <tr>
                        <td valign="top" style="padding-right:20px;">
                        	<!-- Nội dung liên hệ -->
                        	<?php
								if(isset($lienhe) && $lienhe != null){
									echo $lienhe;
								}
							?>
                        </td>
                        <td width="260" valign="top"><div class="formContact_t">
                            <div class="formContact_b">
                              <div class="formContact">
                              	<?php 
									echo $this->session->flashdata('mss');
									$attributes = array('id' => 'formContact', 'class' => 'form', 'name' => 'formContact');
									echo form_open('',$attributes);
								?>
                                <?php if(isset($success)){ ?>
                                    <div style="border:1px solid #ccc; margin-bottom:10px; padding:6px; color:#039"><span>Nội dung liên hệ của bạn đã được gửi đi. Chúng tôi sẽ liên hệ sớm với bạn.<br><br> Về <a href="<?php echo base_url(); ?>">trang chủ</a></span></div>
                                <?php }else if(isset($warning)){ ?>
                                    <div style="border:1px solid #ccc; margin-bottom:10px; padding:6px; color:#f00"><span>Có lỗi xảy ra, vui lòng thực hiện lại!<br><br> Về <a href="<?php echo base_url(); ?>">trang chủ</a></span></div>
                                <?php } ?>   
                                  <table width="100%" border="0" cellspacing="2" cellpadding="2" align="center">
                                    <tbody>
                                      <tr>
                                        <td><label>Họ tên : <span class="font_err">(*)</span>
                                            <?php
												$data=array(
														'name'=>'tendaydu',
														'id'=>'tendaydu',
														'class'=>'textfiled',
														'style'=>'width:100%',
														'value'=>set_value('tendaydu','')
												);
												echo form_input($data);
											?>
                                          </label></td>
                                      </tr>
                                      <tr>
                                        <td><label>Email : <span class="font_err">(*)</span></label>
                                          <?php
												$data=array(
														'name'=>'email',
														'id'=>'email',
														'class'=>'textfiled',
														'style'=>'width:100%',
														'value'=>set_value('email','')
												);
												echo form_input($data);
											?>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><label>Địa chỉ : <span class="font_err">(*)</span></label>
                                          <?php
											  $data=array(
													  'name'=>'diachi',
													  'id'=>'diachi',
													  'class'=>'textfiled',
													  'style'=>'width:100%',
													  'value'=>set_value('diachi','')
											  );
											  echo form_input($data);
										  ?>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><label>Điện thoại : <span class="font_err">(*)</span></label>
                                          <?php
												$data=array(
														'name'=>'dienthoai',
														'id'=>'dienthoai',
														'class'=>'textfiled',
														'style'=>'width:100%',
														'value'=>set_value('dienthoai','')
												);
												echo form_input($data);
											?>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><label>Liên hệ tới phòng : <span class="font_err">(*)</span></label>
                                          <select name="staff" id="staff" class="select" style="width:100%">
                                            <option value="luu.onlyyou@gmail.com">Sales Admin</option>
                                            <option value="lien.vcom@gmail.com">Bộ Phận Kinh Doanh Cáp Mạng</option>
                                            <option value="ailien@vcom.com.vn">Phòng Mua Vật Tư</option>
                                            <option value="minhtuan@vcom.com.vn">Quan Hệ Cổ Đông</option>
                                            <option value="pctinh@vcom.com.vn">Bộ Phận -IT</option>
                                          </select></td>
                                      </tr>
                                      <tr>
                                        <td><label>Nội dung liên lạc : <span class="font_err">(*)</span></label>
                                          <?php
												$data=array(
													'name'=>'noidung',
													'id'=>'noidung',
													'rows'=>5,
													'class'=>'textarea',
													'style'=>'width:100%',
													'value'=>set_value('noidung','')
												);
												echo form_textarea($data);
											?>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><label>Mã bảo vệ : <span class="font_err">(*)</span></label>
                                          <?php
												$data=array(
													'name'=>'ma_xt',
													'id'=>'ma_xt',
													'class'=>'textfiled',
													'size'=>'15'
												);
												echo form_input($data);
												echo '<div style="width:120px; float:right; margin-left:5px">'.$image.'</div>';
											?>
                                            <?php
												if(isset($xt_error)){
													echo "<label class='error' style='line-height:25px'>Mã số bảo vệ không chính xác!</label>";
												}
											?>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><br>
                                          <button id="do_submit" name="action" type="submit" class="btn" value="Gửi"><span>Gửi</span></button>
                                          &nbsp;
                                          <button id="btnReset" name="btnReset" type="reset" class="btn" value="Làm lại"><span>Làm lại</span></button></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                <?php echo form_close(); ?>
                              </div>
                            </div>
                          </div></td>
                      </tr>
                    </tbody>
                  </table>
                  <div class="clear"></div>
                </div>
              </div>
              <div class="boxM-bl fixPNG">
                <div class="boxM-br fixPNG">
                  <div class="boxM-b"></div>
                </div>
              </div>
            </div></td>
        </tr>
      </tbody>
    </table>
    <div class="clear"></div>
  </div>
</div>

