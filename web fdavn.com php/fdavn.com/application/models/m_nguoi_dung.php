<?php class M_nguoi_dung extends CI_Model{
	function lay_nguoi_dung($tendn,$matkhau){
		$this->db->select('ma_nguoi_dung, ten_nguoi_dung, quyen');	
		$this->db->where('tendn',$tendn);
		$this->db->where('mat_khau',$matkhau);
		$nguoi_dung = $this->db->get('nguoi_dung');
		if($nguoi_dung->num_rows()>0)
			return $nguoi_dung->row_array();
		else
			return false;
	}
	function ds_nguoi_dung(){
		$nguoi_dung = $this->db->get('nguoi_dung');
		if($nguoi_dung->num_rows()>0){
			$arr=array();
			foreach ($nguoi_dung->result() as $row){
				$arr[]=$row;
			}
			return $arr;
		}else{
			return false;	
		}
	}
	function ds_nguoi_dung_phan_trang($limit,$start){
		$this->db->limit($limit,$start);
		$query = $this->db->get("nguoi_dung");
		if($query->num_rows()>0){
			$arr=array();
			foreach ($query->result() as $row){
				$arr[]=$row;
			}
			return $arr;
		}else{
			return false;	
		}
	}
	function tong_so_mau_tin(){
		return $this->db->count_all('nguoi_dung');
	}
	function them_nguoi_dung($tendangnhap,$matkhau,$tennguoidung,$ngaysinh,$gioitinh,$diachi,$email,$dienthoai,$quyen){
		$arr = array(
			'tendn' => $tendangnhap,
			'mat_khau' => $matkhau,
			'ten_nguoi_dung' => $tennguoidung,
			'ngay_sinh' => $ngaysinh,
			'gioi_tinh' => $gioitinh,
			'dia_chi' => $diachi,
			'email' => $email,
			'dien_thoai' => $dienthoai,
			'quyen' => $quyen
		);
		return $this->db->insert('nguoi_dung',$arr);	
	}
	function chi_tiet_nguoi_dung($mand){
		$this->db->where('ma_nguoi_dung',$mand);
		$query = $this->db->get('nguoi_dung');
		if($query->num_rows>0){
			return $query->row();
		}else{
			return false;
		}
	}
	function cap_nhat_nguoi_dung($mand,$tendangnhap,$matkhau,$tennguoidung,$ngaysinh,$gioitinh,$diachi,$email,$dienthoai,$quyen){
		$arr = array(
			'tendn' => $tendangnhap,
			'mat_khau' => $matkhau,
			'ten_nguoi_dung' => $tennguoidung,
			'ngay_sinh' => $ngaysinh,
			'gioi_tinh' => $gioitinh,
			'dia_chi' => $diachi,
			'email' => $email,
			'dien_thoai' => $dienthoai,
			'quyen' => $quyen
		);
		$this->db->where('ma_nguoi_dung',$mand);
		return $this->db->update('nguoi_dung',$arr);	
	}
	function xoa_nguoi_dung($mand){
		return $this->db->delete('nguoi_dung', array('ma_nguoi_dung' => $mand)); 
	}
	function check_email($email){
		$this->db->where('email', $email);
		return $this->db->get('nguoi_dung');
	}
	function reset_password($ma_nguoi_dung,$mat_khau)
	{
		$this->db->where('ma_nguoi_dung', $ma_nguoi_dung);
		$this->db->update('nguoi_dung',array('mat_khau'=>MD5($mat_khau)));
	}
}?>