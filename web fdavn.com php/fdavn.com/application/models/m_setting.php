<?php class M_setting extends CI_Model{
	function edit_setting($id){
		$this->db->where('id',$id);
		$query = $this->db->get('setting');
		if($query->num_rows>0){
			return $query->row();
		}else{
			return false;
		}
	}
	function change_setting($id,$title_page,$description_page,$keyword_page,$analytics,$facebook_acc,$twitter_acc,$google_acc,$phone,$smtpuser,$smtppass,$receive_email,$about,$about1,$about2,$footer){
		$arr = array(
			'title_page' => $title_page,
			'description_page' => $description_page,
			'keyword_page' => $keyword_page,
			'analytics' => $analytics,
			'facebook_acc' => $facebook_acc,
			'twitter_acc' => $twitter_acc,
			'google_acc' => $google_acc,
			'phone' => $phone,
			'smtp_user' => $smtpuser,
			'smtp_pass' => $smtppass,
			'receive_email' => $receive_email,
			'about' => $about,
			'about1' => $about1,
			'about2' => $about2,
			'footer' => $footer
		);
		$this->db->where('id',$id);
		return $this->db->update('setting',$arr);	
	}
}?>