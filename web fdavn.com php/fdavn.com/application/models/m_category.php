<?php class M_category extends CI_Model{

	function category_list(){

		$this->db->order_by("position", "asc");

		$query = $this->db->get('category');

		if($query->num_rows()>0){

			$arr=array();

			foreach ($query->result() as $row){

				$arr[]=$row;

			}

			return $arr;

		}else{

			return false;	

		}

	}

	function count_all(){

		return $this->db->count_all('category');

	}

	function check_cat_name($cat_name){

		$this->db->select('cat_name');

		$this->db->where('cat_name', $cat_name);

		$query = $this->db->get('category');

		if($query->num_rows()>0){

			return $query->num_rows();

		}else{

			return false;

		}

	}

	function check_cat_name_upt($cat_id,$cat_name){

		$this->db->select('cat_name');

		$this->db->where('cat_name', $cat_name);

		$this->db->where('cat_id !=', $cat_id);

		$query = $this->db->get('category');

		if($query->num_rows()>0){

			return $query->num_rows();

		}else{

			return false;

		}

	}

	function add_category($cat_name,$cat_brief,$cat_content,$status,$position,$hinh_luu,$title_page,$description_page,$keyword_page){

		$arr = array(

			'cat_name' => $cat_name,

			'cat_brief' => $cat_brief,

			'cat_content' => $cat_content,

			'status' => $status,

			'position' => $position,

			'cat_pic' => $hinh_luu,

			'title_page' => $title_page,

			'description_page' => $description_page,

			'keyword_page' => $keyword_page

		);

		return $this->db->insert('category',$arr);	

	}

	function edit_category($cat_id){

		$this->db->where('cat_id',$cat_id);

		$query = $this->db->get('category');

		if($query->num_rows>0){

			return $query->row();

		}else{

			return false;

		}

	}

	function change_category($cat_id,$cat_name,$cat_brief,$cat_content,$status,$position,$title_page,$description_page,$keyword_page){

		$arr = array(

			'cat_name' => $cat_name,

			'cat_brief' => $cat_brief,

			'cat_content' => $cat_content,

			'status' => $status,

			'position' => $position,

			'title_page' => $title_page,

			'description_page' => $description_page,

			'keyword_page' => $keyword_page

		);

		$this->db->where('cat_id',$cat_id);

		return $this->db->update('category',$arr);	

	}

	function change_category_with_image($cat_id,$cat_name,$cat_brief,$cat_content,$status,$position,$hinh_luu,$title_page,$description_page,$keyword_page){

		$arr = array(

			'cat_name' => $cat_name,

			'cat_brief' => $cat_brief,

			'cat_content' => $cat_content,

			'status' => $status,

			'position' => $position,

			'cat_pic' => $hinh_luu,

			'title_page' => $title_page,

			'description_page' => $description_page,

			'keyword_page' => $keyword_page

		);

		$this->db->where('cat_id',$cat_id);

		return $this->db->update('category',$arr);	

	}

	function delete_category($cat_id){

		return $this->db->delete('category', array('cat_id' => $cat_id)); 

	}

	/*function lay_ten_category($cat_id){

		$this->db->select('tenloai');

		$this->db->where('cat_id',$cat_id);

		$doixe = $this->db->get('category');

		if($doixe->num_rows()>0){

			return $doixe->row_array();

		}else{

			return false;}			

	}*/

}?>