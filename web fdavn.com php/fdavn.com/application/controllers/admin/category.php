<?php class Category extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_category');
	}
	function category_list(){
		if($this->session->userdata('ten_nguoi_dung') != NULL){
			$data['cat_active'] = '';
			$data['category'] = $this->m_category->category_list();
			$data['path'] = 'admin/category/v_category_list';
			$this->load->view('admin/layout',$data);
		}else{
			redirect('cm-admin');
		}
	}
	function add_category(){
		if($this->session->userdata('ten_nguoi_dung') != NULL){
			if(isset($_POST['action']) && $_POST['action'] == 'Add category'){
				$cat_name = trim($this->input->post('cat_name'));
				$cat_brief = trim($this->input->post('cat_brief'));
				$cat_content = trim($this->input->post('cat_content'));
				$status = $this->input->post('status');
				$position = $this->input->post('position');
				$title_page = trim($this->input->post('title_page'));
				$description_page = trim($this->input->post('description_page'));
				$keyword_page = trim($this->input->post('keyword_page'));
				$kt = $this->m_category->check_cat_name($cat_name);
				if($kt == 1){
					$data['cat_name_error'] = '<div class="n_warning"><p>Tên dịch vụ đã có. Vui lòng nhập tên khác!</p></div>';
				}else{
					//upload category images
					$this->load->library('upload');
					$config['upload_path'] = './public/category_images/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size']	= '2000';
					$config['max_width']  = '800';
					$config['max_height']  = '600';
					$hinh_luu = '';
					$so_hinh = $this->input->post('so_hinh');
					for($i = 0; $i < $so_hinh; $i++){
						$ten = 'file'.$i;
						$hinh = time() . str_replace(' ','',$_FILES[$ten]['name']);
						$config['file_name'] = $hinh;
						$hinh_luu .= $hinh . '|';
						$this->upload->initialize($config);
						if(!$this->upload->do_upload($ten)){
							$this->session->set_flashdata('mss', '<div class="n_warning"><p>Hình không hợp lệ. Vui lòng chọn hình khác!</p></div>');
							redirect('cm-admin/add-category');
						}
						$hinh = "";
					}
					$hinh_luu = substr($hinh_luu,0,strlen($hinh_luu)-1);
					//end upload category images
					$kq = $this->m_category->add_category($cat_name,$cat_brief,$cat_content,$status,$position,$hinh_luu,$title_page,$description_page,$keyword_page);
					if($kq){
						$this->session->set_flashdata('mss', '<div class="n_ok"><p>Thêm thành công</p></div>');
					}else{
						$this->session->set_flashdata('mss', '<div class="n_error"><p>Thêm không thành công</p></div>');
					}
					redirect('cm-admin/add-category');
				}
			}
			$data['cat_active'] = '';
			$data['thutu'] = $this->m_category->count_all()+1;
			$data['path'] = 'admin/category/v_add_category';
			$this->load->view('admin/layout',$data);
		}else{
			redirect('cm-admin');
		}
	}
	function edit_category($cat_id){
		if($this->session->userdata('ten_nguoi_dung') != NULL){
			if(isset($_POST['action']) && $_POST['action'] == 'Edit category'){
				$cat_name = trim($this->input->post('cat_name'));
				$cat_brief = trim($this->input->post('cat_brief'));
				$cat_content = trim($this->input->post('cat_content'));
				$status = $this->input->post('status');
				$position = $this->input->post('position');
				$title_page = trim($this->input->post('title_page'));
				$description_page = trim($this->input->post('description_page'));
				$keyword_page = trim($this->input->post('keyword_page'));
				$kt = $this->m_category->check_cat_name_upt($cat_id,$cat_name);
				if($kt == 1){
					$data['cat_name_error'] = '<div class="n_warning"><p>Tên dịch vụ đã có. Vui lòng nhập tên khác!</p></div>';
				}else{
					if($_FILES['file0']['name'] != NULL){
						$this->load->library('upload');
						$config['upload_path'] = './public/category_images/';
						$config['allowed_types'] = 'gif|jpg|png|jpeg';
						$config['max_size']	= '2000';
						$config['max_width']  = '800';
						$config['max_height']  = '600';
						$hinh_luu = '';
						$so_hinh = $this->input->post('so_hinh');
						for($i=0;$i<$so_hinh;$i++){
							$ten = 'file'.$i;
							$hinh = time() . str_replace(' ','',$_FILES[$ten]['name']);
							$config['file_name'] = $hinh;
							$hinh_luu .= $hinh . '|';
							$this->upload->initialize($config);
							if(!$this->upload->do_upload($ten)){
								$this->session->set_flashdata('mss', '<div class="n_warning"><p>Hình không hợp lệ. Vui lòng chọn hình khác!</p></div>');
								redirect('cm-admin/edit-category/'.$cat_id);
							}
							$hinh = "";
						}
						$hinh_luu = substr($hinh_luu,0,strlen($hinh_luu)-1);
						$kq = $this->m_category->change_category_with_image($cat_id,$cat_name,$cat_brief,$cat_content,$status,$position,$hinh_luu,$title_page,$description_page,$keyword_page);
						$chuoi_hinh_cu = $this->input->post('hidden_hinh');
						$mang_hinh_cu = explode('|',$chuoi_hinh_cu);
						for($j=0;$j<count($mang_hinh_cu);$j++){
							$path='./public/category_images/'.$mang_hinh_cu[$j];
							if(file_exists($path)){
								unlink($path);
							}	
						}
						if($kq){
							$this->session->set_flashdata('mss', '<div class="n_ok"><p>Cập nhật thành công</p></div>');
						}else{
							$this->session->set_flashdata('mss', '<div class="n_error"><p>Cập nhật không thành công</p></div>');
						}
						redirect('cm-admin/edit-category/'.$cat_id);
					}else{
						$kq = $this->m_category->change_category($cat_id,$cat_name,$cat_brief,$cat_content,$status,$position,$title_page,$description_page,$keyword_page);
						if($kq){
							$this->session->set_flashdata('mss', '<div class="n_ok"><p>Cập nhật thành công</p></div>');
						}else{
							$this->session->set_flashdata('mss', '<div class="n_error"><p>Cập nhật không thành công</p></div>');
						}
						redirect('cm-admin/edit-category/'.$cat_id);
					}
				}//end else $kt
			}
			$data['cat_active'] = '';
			$data['thutu'] = $this->m_category->count_all();
			$data['edit_category']=$this->m_category->edit_category($cat_id);			
			$data['path'] = 'admin/category/v_edit_category';
			$this->load->view('admin/layout',$data);
		}else{
			redirect('cm-admin');
		}
	}
	function delete_category($cat_id){
		if($this->session->userdata('ten_nguoi_dung') != NULL){
			$data['category'] = $this->m_category->edit_category($cat_id);
			if(isset($_POST['action']) && $_POST['action'] == 'Delete category'){
				$xoa = $this->input->post('delete');
				if($xoa == 'yes'){
					$this->m_category->delete_category($cat_id);
					$chuoi_hinh_cu = $this->input->post('hidden_hinh');
					$mang_hinh_cu = explode('|',$chuoi_hinh_cu);
					for($j=0;$j<count($mang_hinh_cu);$j++){
						$path='./public/category_images/'.$mang_hinh_cu[$j];
						if(file_exists($path)){
							unlink($path);
						}	
					}
				}
				redirect('cm-admin/category-list');
			}
			$data['cat_active'] = '';
			$data['path'] = 'admin/category/v_delete_category';
			$this->load->view('admin/layout',$data);
		}else{
			redirect('cm-admin');
		}
	}
}?>
