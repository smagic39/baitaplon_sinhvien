<?php class Slider extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_slider');
	}
	function slider_list(){
		if($this->session->userdata('ten_nguoi_dung') != NULL){	
			$data['sl_active'] = '';
			$data['slider'] = $this->m_slider->slider_list($catp_id=0);
			$data['path'] = 'admin/slider/v_slider_list';
			$this->load->view('admin/layout',$data);
		}else{
			redirect('cm-admin');
		}
	}
	function add_slider(){
		if($this->session->userdata('ten_nguoi_dung') != NULL){
			if(isset($_POST['action']) && $_POST['action'] == 'Add slider'){
				$config['upload_path'] = './public/slide_images/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size']	= '20000';
				$config['max_width']  = '1920';
				$config['max_height']  = '1600';
				$hinh = time() . str_replace(' ','',$_FILES['hinh']['name']);
				$config['file_name'] = $hinh;
				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload('hinh')){
					$data['hinh_error'] = '<div class="n_warning"><p>Hình không hợp lệ. Vui lòng chọn hình khác!</p></div>';
				}else{
					$config1['image_library'] = 'gd2';
					$config1['source_image'] = './public/slide_images/' . $hinh;
					$this->load->library('image_lib', $config1);
					$this->image_lib->clear();
					$sl_title = trim($this->input->post('sl_title'));
					$sl_link = trim($this->input->post('sl_link'));
					$catp_id = $this->input->post('catp_id');
					$kq = $this->m_slider->add_slider($sl_title,$hinh,$sl_link,$catp_id);
					if($kq){
						$this->session->set_flashdata('mss', '<div class="n_ok"><p>Thêm slider thành công</p></div>');
					}else{
						$this->session->set_flashdata('mss', '<div class="n_error"><p>Thêm slider không thành công</p></div>');
					}
					redirect('cm-admin/add-slider');
				}
			}
			$data['sl_active'] = '';
			$this->load->model('m_category');
			$data['category'] = $this->m_category->category_list();
			$data['path'] = 'admin/slider/v_add_slider';
			$this->load->view('admin/layout',$data);
		}else{
			redirect('cm-admin');
		}
	}
	function edit_slider($sl_id){
		if($this->session->userdata('ten_nguoi_dung') != NULL){
			if(isset($_POST['action']) && $_POST['action'] == 'Edit slider'){
				$sl_title = trim($this->input->post('sl_title'));
				$sl_link = trim($this->input->post('sl_link'));
				$catp_id = $this->input->post('catp_id');
				if($_FILES['hinh']['name'] != NULL)	{
					$config['upload_path'] = './public/slide_images/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size']	= '20000';
					$config['max_width']  = '1920';
					$config['max_height']  = '1600';
					$hinh = time() . str_replace(' ','',$_FILES['hinh']['name']);
					$config['file_name'] = $hinh;
					$this->load->library('upload', $config);
					if ( ! $this->upload->do_upload('hinh')){
						$data['hinh_error'] = '<div class="n_warning"><p>Hình không hợp lệ. Vui lòng chọn hình khác!</p></div>';
					}else{
						$config1['image_library'] = 'gd2';
						$config1['source_image'] = './public/slide_images/' . $hinh;
						$this->load->library('image_lib', $config1);
						$this->image_lib->clear();
						$path_max='./public/slide_images/'.$this->input->post('hidden_hinh');
						if(file_exists($path_max)){
							unlink($path_max);
						}						
						$kq = $this->m_slider->change_slider_with_img($sl_id,$sl_title,$hinh,$sl_link,$catp_id);
						if($kq){
							$this->session->set_flashdata('mss', '<div class="n_ok"><p>Cập nhật slider thành công</p></div>');
						}else{
							$this->session->set_flashdata('mss', '<div class="n_error"><p>Cập nhật slider không thành công</p></div>');
						}
						redirect('cm-admin/edit-slider/'.$sl_id);
					}	
				}else{
					$kq = $this->m_slider->change_slider($sl_id,$sl_title,$sl_link,$catp_id);
					if($kq){
						$this->session->set_flashdata('mss', '<div class="n_ok"><p>Cập nhật slider thành công</p></div>');
					}else{
						$this->session->set_flashdata('mss', '<div class="n_error"><p>Cập nhật slider không thành công</p></div>');
					}
					redirect('cm-admin/edit-slider/'.$sl_id);
				}
			}
			$data['sl_active'] = '';
			$this->load->model('m_category');
			$data['category'] = $this->m_category->category_list();
			$data['chi_tiet_slider']=$this->m_slider->edit_slider($sl_id);
			$data['path'] = 'admin/slider/v_edit_slider';
			$this->load->view('admin/layout',$data);
		}else{
			redirect('cm-admin');
		}
	}
	function delete_slider($sl_id){
		if($this->session->userdata('ten_nguoi_dung') != NULL){
			$data['slider'] = $this->m_slider->edit_slider($sl_id);
			if(isset($_POST['action']) && $_POST['action'] == 'Delete slider'){
				$xoa = $this->input->post('delete');
				if($xoa == 'yes'){
					$this->m_slider->delete_slider($sl_id);
					$path_max='./public/slide_images/'.$this->input->post('hidden_hinh');
					if(file_exists($path_max)){
						unlink($path_max);
					}
					redirect('cm-admin/slider-list');
				}else{
					redirect('cm-admin/slider-list');
				}
			}
			$data['sl_active'] = '';
			$data['path'] = 'admin/slider/v_delete_slider';
			$this->load->view('admin/layout',$data);
		}else{
			redirect('cm-admin');
		}
	}
}?>
