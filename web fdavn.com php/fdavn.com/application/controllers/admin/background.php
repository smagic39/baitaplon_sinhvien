<?php class Background extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_background');
	}
	function index($id=1){
		if($this->session->userdata('ten_nguoi_dung') != NULL){
			if(isset($_POST['action']) && $_POST['action'] == 'Thiết lập'){
				$home = trim($this->input->post('home'));
				$about1 = trim($this->input->post('about1'));
				$about2 = trim($this->input->post('about2'));
				$contact = trim($this->input->post('contact'));
				$my_pham = trim($this->input->post('my_pham'));
				$chai_lo = trim($this->input->post('chai_lo'));
				$thiet_ke = trim($this->input->post('thiet_ke'));
				$phap_ly = trim($this->input->post('phap_ly'));
				$footer1 = trim($this->input->post('footer1'));				
				$home_img = $this->input->post('hidden_hinh_home');
				$this->load->library('upload');
				if($_FILES['home_img']['name'] != NULL)	{
					$config['upload_path'] = './public/css_front/images/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$home_img = time() . str_replace(' ','',$_FILES['home_img']['name']);
					$config['file_name'] = $home_img;
					$this->upload->initialize($config);
					if ( ! $this->upload->do_upload('home_img')){
						$data['hinh_error'] = '<div class="n_warning"><p>Hình không hợp lệ. Vui lòng chọn hình khác!</p></div>';
					}else{
						$path_max_home ='./public/css_front/images/'.$this->input->post('hidden_hinh_home');
						if(file_exists($path_max_home)){
							unlink($path_max_home);
						}
					}
				}
				$about_img = $this->input->post('hidden_hinh_about');
				if($_FILES['about_img']['name'] != NULL)	{
					$config_ab['upload_path'] = './public/css_front/images/';
					$config_ab['allowed_types'] = 'gif|jpg|png|jpeg';
					$about_img = time() . str_replace(' ','',$_FILES['about_img']['name']);
					$config_ab['file_name'] = $about_img;
					$this->upload->initialize($config_ab);
					if ( ! $this->upload->do_upload('about_img')){
						$data['hinh_error'] = '<div class="n_warning"><p>Hình không hợp lệ. Vui lòng chọn hình khác!</p></div>';
					}else{
						$path_max_about = './public/css_front/images/'.$this->input->post('hidden_hinh_about');
						if(file_exists($path_max_about)){
							unlink($path_max_about);
						}
					}
				}
				$contact_img = $this->input->post('hidden_hinh_contact');
				if($_FILES['contact_img']['name'] != NULL)	{
					$config_ct['upload_path'] = './public/css_front/images/';
					$config_ct['allowed_types'] = 'gif|jpg|png|jpeg';
					$contact_img = time() . str_replace(' ','',$_FILES['contact_img']['name']);
					$config_ct['file_name'] = $contact_img;
					$this->upload->initialize($config_ct);
					if ( ! $this->upload->do_upload('contact_img')){
						$data['hinh_error'] = '<div class="n_warning"><p>Hình không hợp lệ. Vui lòng chọn hình khác!</p></div>';
					}else{
						$path_max_contact = './public/css_front/images/'.$this->input->post('hidden_hinh_contact');
						if(file_exists($path_max_contact)){
							unlink($path_max_contact);
						}
					}
				}
				$kq = $this->m_background->change_background_with_img($id,$home,$home_img,$about_img,$about1,$about2,$contact,$contact_img,$my_pham,$chai_lo,$thiet_ke,$phap_ly,$footer1);
				if($kq){
					$this->session->set_flashdata('mss', '<div class="n_ok"><p>Cập nhật thành công</p></div>');
				}else{
					$this->session->set_flashdata('mss', '<div class="n_error"><p>Cập nhật không thành công</p></div>');
				}							
				redirect('cm-admin/background/'.$id);
				
				
			}
			$data['edit_background'] = $this->m_background->edit_background($id);
			$data['path'] = 'admin/background/v_background';
			$this->load->view('admin/layout',$data);
		}else{
			redirect('cm-admin');
		}
	}
	function delete_bg_img($img,$col)
	{
		//echo $img.'-'.$col;
		$path_max = './public/css_front/images/'.$img;
		if(file_exists($path_max)){
			unlink($path_max);
		}
		$this->m_background->delete_home_img($col);
	}
}?>
