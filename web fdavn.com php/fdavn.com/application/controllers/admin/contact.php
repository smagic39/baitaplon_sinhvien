<?php class Contact extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_contact');
	}
	function contact_list(){
		if($this->session->userdata('ten_nguoi_dung') != NULL){
			$this->load->helper('text');
			$data['ct_active'] = '';
			$data['contact'] = $this->m_contact->contact_list();
			$data['path'] = 'admin/contact/v_contact_list';
			$this->load->view('admin/layout',$data);
		}else{
			redirect('cm-admin');
		}
	}
	function edit_contact($id){
		if($this->session->userdata('ten_nguoi_dung') != NULL){
			$data['ct_active'] = '';
			$data['edit_contact']=$this->m_contact->edit_contact($id);			
			$data['path'] = 'admin/contact/v_edit_contact';
			$this->load->view('admin/layout',$data);
		}else{
			redirect('cm-admin');
		}
	}
	function delete_contact($id){
		if($this->session->userdata('ten_nguoi_dung') != NULL){
			$data['contact'] = $this->m_contact->edit_contact($id);
			if(isset($_POST['action']) && $_POST['action'] == 'Delete contact'){
				$xoa = $this->input->post('delete');
				if($xoa == 'yes'){
					$this->m_contact->delete_contact($id);
				}
				redirect('cm-admin/contact-list');
			}
			$data['ct_active'] = '';
			$data['path'] = 'admin/contact/v_delete_contact';
			$this->load->view('admin/layout',$data);
		}else{
			redirect('cm-admin');
		}
	}
}?>
