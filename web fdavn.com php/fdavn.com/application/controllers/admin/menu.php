<?php class Menu extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_menu');
	}
	function ds_menu(){
		if($this->session->userdata('ten_nguoi_dung') != NULL){
			$data['menu_active'] = '';
			$data['menu'] = $this->m_menu->ds_menu();
			$data['path'] = 'admin/includes/menu/v_ds_menu';
			$this->load->view('admin/layout',$data);
		}else{
			redirect('cm-admin');
		}
	}
	function chi_tiet_menu($idmenu){
		if($this->session->userdata('ten_nguoi_dung') != NULL){
			$data['menu_active'] = '';
			$data['thutu'] = $this->m_menu->tong_so_mau_tin();
			$data['menu']=$this->m_menu->chi_tiet_menu($idmenu);
			if(isset($_POST['action']) && $_POST['action'] == 'Cập nhật menu'){
				$tenmenu = $this->input->post('tenmenu');
				$link = $this->input->post('link');
				$trangthai = $this->input->post('trangthai');
				$thutu = $this->input->post('thutu');				
				$kq = $this->m_menu->cap_nhat_menu($idmenu,$tenmenu,$link,$trangthai,$thutu);
				if($kq){
					$this->session->set_flashdata('mss', '<div class="n_ok"><p>Cập nhật thành công</p></div>');
				}else{
					$this->session->set_flashdata('mss', '<div class="n_error"><p>Cập nhật không thành công</p></div>');
				}
				redirect('cm-admin/chi-tiet-menu/'.$idmenu);			
			}				
			$data['path'] = 'admin/includes/menu/v_chi_tiet_menu';
			$this->load->view('admin/layout',$data);
		}else{
			redirect('cm-admin');
		}
	}
}?>
