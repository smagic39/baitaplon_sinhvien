<?php class Tai_khoan extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_tai_khoan');
	}
	function ds_tai_khoan(){
		if($this->session->userdata('ten_nguoi_dung') != NULL){
			$data['tk_active'] = '';
			$data['tai_khoan'] = $this->m_tai_khoan->ds_tai_khoan();
			$data['path'] = 'admin/includes/tai_khoan/v_ds_tai_khoan';
			$this->load->view('admin/layout',$data);
		}else{
			redirect('cm-admin');
		}
	}
	function them_tai_khoan(){
		if($this->session->userdata('ten_nguoi_dung') != NULL){
			if(isset($_POST['action']) && $_POST['action'] == 'Thêm tài khoản'){
				$errors = array();
				if(preg_match('/^[A-z][\w\.]{4,20}$/', trim($_POST['tendangnhap']))){
					$tendangnhap = trim($_POST['tendangnhap']);
					$kt = $this->m_tai_khoan->kiem_tra_ten_dang_nhap($tendangnhap);
					if($kt == 1){
						$errors[] = '<div class="n_warning"><p>Tên đăng nhập đã có, vui lòng chọn tên đăng nhập khác!</p></div>';
					}
				}else{
					$errors[] = '<div class="n_warning"><p>Tên đăng nhập ko hợp lệ(A-Z,a-z,0-9,-,_,không dấu cách)</p></div>';
				}
				if(preg_match('/^[\w\'.-@#]{6,20}$/', trim($_POST['matkhau']))){
					$matkhau = md5(trim($_POST['matkhau']));
				}else{
					$errors[] = '<div class="n_warning"><p>Mật khẩu ko hợp lệ(A-Z,a-z,0-9,-,_,không dấu cách)</p></div>';
				}
				$email = trim($_POST['email']);
				$ktemail = $this->m_tai_khoan->kiem_tra_email($email);
				if($ktemail == 1){
					$errors[] = '<div class="n_warning"><p>Email đã có, vui lòng chọn email khác!</p></div>';
				}
				if($errors != null){
					$data['errors'] = $errors;
				}else{
					$hoten = trim($_POST['hoten']);
					$dienthoai = trim($_POST['dienthoai']);
					$diachi = trim($_POST['diachi']);
					$this->load->view('admin/includes/xl_ngay');					
					$ngaysinh = trim($_POST['ngaysinh']);
					$ngaysinh = dmy_to_ymd($ngaysinh);
					$cmnd = trim($_POST['cmnd']);
					$quyen = 2;
					$kq = $this->m_tai_khoan->them_tai_khoan($tendangnhap,$matkhau,$hoten,$email,$dienthoai,$diachi,$cmnd,$quyen,$ngaysinh);
					if($kq){
						$this->session->set_flashdata('mss', '<div class="n_ok"><p>Thêm thành công</p></div>');
					}else{
						$this->session->set_flashdata('mss', '<div class="n_error"><p>Thêm không thành công</p></div>');
					}
					redirect('cm-admin/them-tai-khoan');
				}							
			}
			$data['tk_active'] = '';
			$data['path'] = 'admin/includes/tai_khoan/v_them_tai_khoan';
			$this->load->view('admin/layout',$data);
		}else{
			redirect('cm-admin');
		}
	}
	function chi_tiet_tai_khoan($manguoidung){
		if($this->session->userdata('ten_nguoi_dung') != NULL){
			if(filter_var($manguoidung, FILTER_VALIDATE_INT, array('min_range' => 1))){
				if($manguoidung == 1){
					redirect('cm-admin/danh-sach-tai-khoan');
				}else{
					$data['chi_tiet_tai_khoan']=$this->m_tai_khoan->chi_tiet_tai_khoan($manguoidung);
					if(isset($_POST['action']) && $_POST['action'] == 'Cập nhật tài khoản'){
						$errors = array();
						if(preg_match('/^[A-z][\w\.]{4,20}$/', trim($_POST['tendangnhap']))){
							$tendangnhap = trim($_POST['tendangnhap']);
							$kt = $this->m_tai_khoan->kiem_tra_ten_dang_nhap_cap_nhat($manguoidung,$tendangnhap);
							if($kt == 1){
								$errors[]='<div class="n_warning"><p>Tên đăng nhập đã có, vui lòng chọn tên đăng nhập khác!</p></div>';
							}
						}else{
							$errors[]='<div class="n_warning"><p>Tên đăng nhập ko hợp lệ(A-Z,a-z,0-9,-,_,không dấu cách)</p></div>';
						}
						$email = trim($_POST['email']);
						$ktemail = $this->m_tai_khoan->kiem_tra_email_cap_nhat($manguoidung,$email);
						if($ktemail == 1){
							$errors[] = '<div class="n_warning"><p>Email đã có, vui lòng chọn email khác!</p></div>';
						}
						if($_POST['matkhau'] != ''){
							if(preg_match('/^[\w\'.-@#]{6,20}$/', trim($_POST['matkhau']))){
								$matkhau = md5(trim($_POST['matkhau']));
							}else{
								$errors[]='<div class="n_warning"><p>Mật khẩu ko hợp lệ(A-Z,a-z,0-9,-,_,không dấu cách)</p></div>';
							}
						}
						if($errors != null){
							$data['errors'] = $errors;
						}else{
							$hoten = trim($_POST['hoten']);
							$dienthoai = trim($_POST['dienthoai']);
							$diachi = trim($_POST['diachi']);
							$this->load->view('admin/includes/xl_ngay');					
							$ngaysinh = trim($_POST['ngaysinh']);
							$ngaysinh = dmy_to_ymd($ngaysinh);
							$cmnd = trim($_POST['cmnd']);
							$quyen = 2;
							$kq = $this->m_tai_khoan->cap_nhat_tai_khoan($manguoidung,$tendangnhap,$email,$hoten,$dienthoai,$diachi,$ngaysinh,$cmnd,$quyen);
							if($_POST['matkhau'] != ''){
								$kq = $this->m_tai_khoan->cap_nhat_tai_khoan_mk($manguoidung,$tendangnhap,$matkhau,$email,$hoten,$dienthoai,$diachi,$ngaysinh,$cmnd,$quyen);
							}
							if($kq){
								$this->session->set_flashdata('mss', '<div class="n_ok"><p>Cập nhật thành công</p></div>');
							}else{
								$this->session->set_flashdata('mss', '<div class="n_error"><p>Cập nhật không thành công</p></div>');
							}
							redirect('cm-admin/chi-tiet-tai-khoan/'.$manguoidung);
						}
					}
				}
				$data['tk_active'] = '';
				$data['path'] = 'admin/includes/tai_khoan/v_chi_tiet_tai_khoan';
				$this->load->view('admin/layout',$data);
			}else {
				redirect('cm-admin/danh-sach-tai-khoan');
			}
		}else{
			redirect('cm-admin');
		}
	}
	function xoa_tai_khoan($manguoidung){
		if($this->session->userdata('ten_nguoi_dung') != NULL){
			$data['tk_active'] = '';
			if(filter_var($manguoidung, FILTER_VALIDATE_INT, array('min_range' => 1))){
				if($manguoidung == 1 || $manguoidung == $this->session->userdata('ma_nguoi_dung')){
					redirect('cm-admin/danh-sach-tai-khoan');
				}else{
					$data['tai_khoan'] = $this->m_tai_khoan->chi_tiet_tai_khoan($manguoidung);
					if(isset($_POST['action']) && $_POST['action'] == 'Xóa tài khoản'){
						$xoa = $this->input->post('delete');
						if($xoa == 'yes'){				
							$this->m_tai_khoan->xoa_tai_khoan($manguoidung);
							redirect('cm-admin/danh-sach-tai-khoan');
						}else{
							redirect('cm-admin/danh-sach-tai-khoan');
						}
					}
					$data['path'] = 'admin/includes/tai_khoan/v_xoa_tai_khoan';
					$this->load->view('admin/layout',$data);
				}
			}else{
				redirect('cm-admin/danh-sach-tai-khoan');
			}
		}else{
			redirect('cm-admin');
		}
	}
}?>
