<?php	

	class Product extends CI_Controller

	{

		function __construct()

		{

			parent::__construct();

			$this->load->model('m_product');

			$this->load->model('m_category');

		}

		function product_list_cat()

		{			

			//lấy mã loại sản phẩm

			$cat = $this->uri->segment(2);

			$cat = str_replace(".html","",$cat);

			$mang_cat = explode("-", $cat);

			$i = count($mang_cat)-1;

			$maloai =  $mang_cat[$i];

			//end lấy mã loại sản phẩm

			

			$chitietloai = $this->m_category->edit_category($maloai); //lấy chi tiết của dịch vụ

			$data['chitietloai'] = $chitietloai;

			$data['description_page'] = $chitietloai->description_page;

			$data['keyword_page'] = $chitietloai->keyword_page;

			$data['title_page'] = $chitietloai->title_page;

			$data['product_cat'] = $this->m_product->list_product_for_parent($maloai);

			

			//lấy màu nền

			$this->load->model('m_background');

			$data['background'] = $this->m_background->edit_background(1);

			//lấy slider

			$this->load->model('m_slider');

			$data['slider'] = $this->m_slider->slider_list($maloai);

			//lấy cấu hình

			$this->load->model('m_setting');

			$data['chi_tiet_cau_hinh'] = $this->m_setting->edit_setting(1);

			//lấy menu

			$this->load->model('m_menu');

			$data['menu'] = $this->m_menu->ds_menu();

			//lấy danh sách dịch vụ

			$data['category'] = $this->m_category->category_list();

			//lấy sản phẩm theo loại dịch vụ

			$data['product'] = $this->m_product->list_product_for_parent($maloai);

			

			if($maloai == 2)

				$this->load->view('front/gia-cong-my-pham',$data);

			if($maloai == 3)

				$this->load->view('front/cung-cap-chai-lo',$data);

			if($maloai == 4)

				$this->load->view('front/xay-dung-thuong-hieu',$data);

			if($maloai == 5)

				$this->load->view('front/ho-tro-phap-ly',$data);

		}

		function product_details($pro_id){

			$product_details = $this->m_product->edit_product($pro_id);

			$data['product_details'] = $product_details;

			$catp_id = $product_details->catp_id;

			

			//lấy 1 sản phẩm trước nó

			$older = $this->m_product->older_products($pro_id,$catp_id);

			if(count($older)>0){

				foreach($older as $row){

					$old_id = $row->alas;

				}

				$data['product_details_older'] = $old_id;

			}



			//lấy 1 sản phẩm sau nó.

			$newer = $this->m_product->newer_products($pro_id,$catp_id);

			if(count($newer)>0){

				foreach($newer as $row){

					$new_id = $row->alas;

				}

				$data['product_details_newer'] = $new_id;

			}

						 

			

			$this->load->view('front/product/product_details',$data);

		}

	}

?>