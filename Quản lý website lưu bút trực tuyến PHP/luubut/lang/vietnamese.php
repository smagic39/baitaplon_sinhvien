<?php
# General

$LANG["metatag"]    = '<META http-equiv=Content-Type content="text/html; charset=UTF-8">';

$LANG["BookMess1"]  = "Mã HTML Đang Tắt";
$LANG["BookMess2"]  = "Mã HTML Đang Mở";
$LANG["BookMess3"]  = "Viết Lưu Bút";
$LANG["BookMess4"]  = "Xem Lưu Bút";
$LANG["BookMess5"]  = "Quản Lý Lưu Bút";
$LANG["BookMess6"]  = "Cám Ơn Bạn Đã Ủng Hộ WebSite , Chúc Bạn Có Những Giây Phút Vui Vẻ !";
$LANG["BookMess7"]  = "Nội Dung Lưu Bút Và Các cảm Nhận Dành Cho Lưu Bút Này";
$LANG["BookMess8"]  = "Quay Lại Để Nhập Lại Nội Dung Lưu Bút";
$LANG["BookMess9"]  = "Lưu Lại";
$LANG["BookMess10"] = "<b>Cám Ơn Bạn Đã Viết Lưu Bút Cho Chúng Tôi, Lưu Bút Của Bạn Đã Được Lưu Hoàn Tất.</b><br><br>Bạn sẽ Được Xem Lưu Bút Bạn Đã Viết Sau 2 Giây";

$LANG["EmailAdminSubject"] = "wWw.TânPhú.Net || Lưu bút đã được lưu";
$LANG["EmailGuestSubject"] = "Cám Ơn bạn Đã Viết Lưu Bút Tại WebSite wWw.TânPhú.Net , Chúc Bạn Vui Vẻ";

$LANG["AltIP"]    = "IP Của Máy Truy Cập Vào WebSite";
$LANG["AltICQ"]   = "Gửi Tin Nhắn ICQ";
$LANG["AltEmail"] = "Gửi E-mail";
$LANG["AltUrl"]   = "Xem Trang Web Của Người Này";
$LANG["AltAim"]   = "Tin Nhắn gấp";
$LANG["AltCom"]   = "Viết cảm nhận của bạn sau khi đọc lưu bút này";

# Form
$LANG["FormMess1"]   = "Bạn Hãy Điền Đày Đủ Các Thông Tin Vào Ô Bên Dưới Rồi Nhấn Nút Đồng Ý Để Lưu Hoặc Nút Xem Thử Để Xem Lại Trước Khi Lưu. Các Ô Có Dấu * Là Bắt Buộc Nhập. Chúc Bạn Vui Vẻ.";
$LANG["FormMess2"]   = "Biểu Tượng Mặt Cười Đang Mở";
$LANG["FormMess3"]   = "Mã AG Đang Mở";
$LANG["FormMess4"]   = "Xem hướng dẫn & Ghi chú";
$LANG["FormMess5"]   = "Ở dưới là nội dung lưu bút và các thông tin bạn đã nhập. Lưu bút của bạn sẽ hiển thị giống y như vậy.<br><b>Lưu ý:</b> Các từ có nội dung tục sẽ bị thay thế bằng từ khác hoặc kí tự đặc biệt.";
$LANG["FormMess6"]   = "Mã AG Đang Tắt";
$LANG["FormMess7"]   = "Biểu Tượng Mặt Cười Đang Tắt";
$LANG["FormName"]    = "Tên Người Viết Lưu Bút";
$LANG["FormEmail"]   = "E-mail";
$LANG["FormLoc"]     = "Đến Từ ";
$LANG["FormPic"]     = "Ảnh Của Bạn";
$LANG["FormUrl"]     = "Trang Web Của Bạn : ";
$LANG["FormGender"]  = "Giới Tính Của Bạn Là ";
$LANG["FormMale"]    = "Con Trai";
$LANG["FormFemale"]  = "Con Gái";
$LANG["FormMessage"] = "Nội Dung Lưu Bút";
$LANG["FormSelect"]  = "Xem Lưu Bút";
$LANG["FormUser"]    = "Tên Đăng Nhập";
$LANG["FormPass"]    = "Mật Khẩu";
$LANG["FormReset"]   = "Nhập Lại";
$LANG["FormSubmit"]  = "Đồng Ý";
$LANG["FormPreview"] = "Xem Thử";
$LANG["FormBack"]    = "Quay Trở Lại";
$LANG["FormEnter"]   = "Khu Vực Quản Lý Lưu Bút Của Administration, Để có Thể Quản Lý Bạn Phải Đăng Nhập Với Tên Và Mật Khẩu Của Admin Cung Cấp!";
$LANG["FormButton"]  = "Tiếp Tục";
$LANG["FormPriv"]    = "Tin Nhắn Riêng";

# Navigation Bar
$LANG["NavTotal"]   = "Tổng Cộng Lưu Bút Đã Được Lưu: ";
$LANG["NavRecords"] = "Số Lưu Bút Hiển Thị Trong 1 Trang: ";
$LANG["NavPrev"]    = "Trang Trước";
$LANG["NavNext"]    = "Trang sau";

# Post Errors
$LANG["ErrorPost1"]  = "Bạn chưa điền tên.";
$LANG["ErrorPost2"]  = "Bạn quên nhập thông điệp , vui lòng điền vào rồi bấm đồng ý.";
$LANG["ErrorPost3"]  = "Thông điệp của bạn quá ngắn hoặc quá dài, vui lòng điền lại rồi bấm đồng ý.";
$LANG["ErrorPost4"]  = "1 trong số các ô bạn nhập vào ko hợp lệ.";
$LANG["ErrorPost5"]  = "Lưu bút-Lỗi";
$LANG["ErrorPost6"]  = "File ảnh này quá lớn";
$LANG["ErrorPost7"]  = "Định dạng ảnh sai";
$LANG["ErrorPost8"]  = "Xin lỗi, website đã kích hoạt chế đô phòng chống spam. Vui lòng đợi chút và thử lại!";
$LANG["ErrorPost9"]  = "IP của bạn đã bị khoá!";
$LANG["ErrorPost10"] = "Nội dung lưu bút của bạn chứa vài từ có lỗi, bạn hãy bấm vào <br><b>Quay Lại Để Nhập Lại Nội Dung Lưu Bút</b> để nhập lại nồi dung.<br>Sau khi nhập xong thì bạn bấm <b>Đồng Ý</b> để gửi.";
$LANG["ErrorPost11"] = "Bạn quên chưa điền vào ô cảm nhận, vui lòng điền lại rồi bấm Đồng Ý.";

$LANG["PassMess1"] = "Vui Lòng Điền Tên Và Mật Khẩu :";
$LANG["PassMess2"] = "Tên Người Dùng Sai.";
$LANG["PassMess3"] = "Sai Mật Khẩu Rồi.";

# Days
$weekday[0] = "Chủ Nhật";
$weekday[1] = "Thứ 2";
$weekday[2] = "Thứ 3";
$weekday[3] = "Thứ 4";
$weekday[4] = "Thứ 5";
$weekday[5] = "Thứ 6";
$weekday[6] = "Thứ 7";

# Months
$months[0]  = "Tháng 1, Năm ";
$months[1]  = "Tháng 2, Năm ";
$months[2]  = "Tháng 3, Năm ";
$months[3]  = "Tháng 4, Năm ";
$months[4]  = "Tháng 5, Năm ";
$months[5]  = "Tháng 6, Năm ";
$months[6]  = "Tháng 7, Năm ";
$months[7]  = "Tháng 8, Năm ";
$months[8]  = "Tháng 9, Năm ";
$months[9]  = "Tháng 10, Năm ";
$months[10] = "Tháng 11, Năm ";
$months[11] = "Tháng 12, Năm ";

?>