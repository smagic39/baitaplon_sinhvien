<?php
/**
 * @version 1.5.x
 * @package JoomVision Project
 * @email webmaster@joomvision.com
 * @copyright (C) 2008 http://www.JoomVision.com. All rights reserved.
 */
defined('_JEXEC') or die('Restricted access');// no direct access
require_once (dirname(__FILE__).DS.'helper.php');
$moduleId = $module->id;
$layoutStyle = $params->get('layout_style');
$slideDelay = trim( $params->get('timming'));
switch ($layoutStyle) {
	case "jv_slide1":
		$list_slidecontent = modJVHeadlineCommonHelper::getSlideContent($params);		
		$height = $params->get('jv_news_height');
		$showthumb = trim( $params->get('showthumb') ); //Show Image Thumbnail
		$thumbsize = trim( $params->get('news_thumbsize') ); //Thumbnail Size
		$timming = trim( $params->get('timming') );  //Time to rollover content
		$autorun = trim( $params->get('news_autorun') ); //Autorun
		if($autorun == 1) $autorun = 'true'; 
		else $autorun = 'false';
		require(JModuleHelper::getLayoutPath('mod_jv_headline'));
		break;
	case "jv_slide2":
		$moduleHeight = $params->get('jv2_height');
		$autoRun = $params->get('jv2_autorun');
		$slides = modJVHeadlineCommonHelper::getSlideContent($params);
		require(JModuleHelper::getLayoutPath('mod_jv_headline','smooth'));
		break;
	case "jv_slide3":
		$moduleHeight = $params->get('jv_lago_height');
		$css_width = 'width:'.$params->get('jv_lago_main_width').'px';
		$verticalStyle = $params->get('lago_animation');
		$css_slidebar_width = 'width:'.$params->get('jv_lago_slidebar_width').'px';
		$css_height = 'height:'.$moduleHeight.'px';
		$css_item_heigth ='height:'.$params->get('lago_hitem_sliderbar').'px';
		$divHeadLine = "jv_vheadline".$moduleId;
		$slideOuter = "#slide_outer".$moduleId." div.slide";
		$slideBarItem ='#jv_pagislide'.$moduleId." div.slideitem";
		$slide_width = $params->get('jv_lago_main_width');
		$image_width = $params->get('lago_thumb_width');
		$image_height = $params->get('lago_thumb_height');
		$showButNext = $params->get('lago_show_next');
		$slides = modJVHeadlineCommonHelper::getSlideContent($params);
		if(count($slides)){
			require(JModuleHelper::getLayoutPath('mod_jv_headline','vertical_effect'));
		}
		break;
	case "jv_slide4":
		$moduleWidth = ($params->get("jv_sello2_width"));
		$moduleHeight = $params->get('jv_sello2_height');		
		$showReadmore = $params->get('sello2_readmore',1);
		$showButNext = $params->get('sello2_show_next');
		$number_items_per_line = ($params->get("sello2_no_items_per_line", "2"));
		$list = modJVHeadlineCommonHelper::getSlideContent($params);
		if(count($list)) require(JModuleHelper::getLayoutPath('mod_jv_headline','horizontal'));
		break;
	case "jv_slide5":
		$height = $params->get('jv_maju_height');
		$isReadMore = $params->get('maju_readmore');
		$moduleWidth = $params->get('jv_maju_width');
		$moduleHeight = $params->get('jv_maju_height');
		$imgWidth = $params->get('maju_thumb_width');
		$imgHeight = $params->get('maju_thumb_height');		
		$showButNext = $params->get('maju_show_next');
		$list = modJVHeadlineCommonHelper::getSlideContent($params);
		if(count($list)) require(JModuleHelper::getLayoutPath('mod_jv_headline','slideshow5'));
		break;
	case "jv_slide6":
		$moduleWidth = $params->get('jv_sello1_width');		
		$imgSlideWidth = $params->get('sello1_imgslide_width');
		$imgSlideHeight = $params->get('sello1_imgslide_height');
		$showButNext = $params->get('sello1_show_next');
		$list = modJVHeadlineCommonHelper::getSlideContent($params);
		$isReadMore = $params->get('sello1_readmore');
		require(JModuleHelper::getLayoutPath('mod_jv_headline','slideshow6'));
		break;
	case "jv_slide7":
		$mainModWidth = $params->get('jv7_main_width');
		$mainModHeight = $params->get('jv7_height');
		$isReadMore = $params->get('jv7_readmore');
		$showButNext = $params->get('jv7_show_next');
		$list = modJVSlide7::getSlideContent($params);
		if(count($list)) require(JModuleHelper::getLayoutPath('mod_jv_headline','slideshow7'));
		break;
	case "jv_slide8":
		$isReadMore = $params->get('pedon_readmore');
		$moduleWidth = $params->get('jv_pedon_width');
		$moduleHeight = $params->get('jv_pedon_height');
		$imgWidth = $params->get('pedon_thumb_width');
		$imgHeight = $params->get('pedon_thumb_height');		
		$showButNext = $params->get('pedon_show_next');
		$list = modJVHeadlineCommonHelper::getSlideContent($params);
		if(count($list)) require(JModuleHelper::getLayoutPath('mod_jv_headline','slideshow8'));
		break;	
}
?>