<?php
/**
* Package PPL Shopping
* @Copyright Phan Phuoc Long
*/
/** ensure this file is being included by a parent file */
defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );
 $Itemid =  "&Itemid=".trim( $params->get( 'menu_id' )); 

?>
<div id=cart style="text-align: center;">
<?php
    $ok=0;
    $link= JRoute::_( 'index.php?option=com_pplshop&task=viewcart'. $Itemid ); 
    if(isset($_SESSION['pplCart']) && count($_SESSION['pplCart'])>0)
    {
        foreach($_SESSION['pplCart'] as $k=>$v)
        {
            if(isset($k))
            {
            $ok=2;
            }
        }
    }

    if ($ok != 2)
     {
        echo '<p>'.JText::_('NO PRODUCT').'</p>';
        echo '<div style="padding: 5px"><center><a href="'.$link.'"><span class="button">'.JText::_('VIEW CART').'</span></a></center></div>';
    } else {
        $items = $_SESSION['pplCart'];
        echo '<p>'.JText::_('HAVE').' <b>'.count($items).'</b> '.JText::_('PRODUCTS').'</p>';
        echo '<div style="padding: 5px"><center><a href="'.$link.'"><span class="button" >'.JText::_('VIEW CART').'</span></a></center></div>';
    }
?>
</div>