<?php
/*======================================================================*\
|| #################################################################### ||
|| # Youjoomla LLC - YJ- Licence Number 1777BP315
|| # Licensed to - Chien Du
|| # ---------------------------------------------------------------- # ||
|| # Copyright (C) 2006-2009 Youjoomla LLC. All Rights Reserved.        ||
|| # This file may not be redistributed in whole or significant part. # ||
|| # ---------------- THIS IS NOT FREE SOFTWARE ---------------- #      ||
|| # http://www.youjoomla.com | http://www.youjoomla.com/license.html # ||
|| #################################################################### ||
\*======================================================================*/
	/**
	 * AJAX form validation
	 */
	error_reporting(E_ALL);
	
	// Set flag that this is a parent file
	define( '_JEXEC', 1 );
	define('JPATH_BASE', str_replace(array('modules\mod_yj_booking','modules/mod_yj_booking'),'',dirname(__FILE__)) );	
	define( 'DS', DIRECTORY_SEPARATOR );
	require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
	require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );
	$mainframe =& JFactory::getApplication('site');
	$mainframe->initialise();
	$session =& JFactory::getSession();
	
	$lang = & JFactory :: getLanguage();
	$lang->load('mod_yj_booking', JPATH_BASE);
	jimport( 'joomla.methods' );
	
	$captcha_value = $session->get('YJ_ver_c');
	$neccesary_fields = array(
		JText::_('FIRST NAME')		=>'first_name',
		JText::_('LAST NAME')			=>'surname'		
	);	
	
	if( isset($_POST['Subject']) )
	{
		/**
		 * check if any fields required are empty
		 */
		$fields = array();
		$error = array();
		
		$fields['phone'] = 0;
		if( !($_POST['phone']) ){
			$error[] = JText::_('Nhap noi dung (Enter content)');	
			$fields['phone'] = 1;
		}
		$fields['captcha_ver'] = 0;
		if( $captcha_value != $_POST['captcha_ver'] ){
			$error[] = JText::_('CAPTCHA VERIFY');
			$fields['captcha_ver'] = 1;
		}		
		
		if( count($error)>0 ){
			require('libs/JSON.class.php');
			$response = array('err_fields'=>$fields, 'error'=>$error[0]);
			$json = new JSON($response);
			echo $json->result;
			exit();
		}
		/**
		 * get params
		 */
		$db	=& JFactory::getDBO();
		$sql = "SELECT params FROM #__modules WHERE module = 'mod_yj_booking'";
		$db->setQuery( $sql );
		$p = $db->loadObjectList();		
		require_once(JPATH_BASE.DS.'libraries'.DS.'joomla'.DS.'html'.DS.'parameter.php');		
		$params = new JParameter($p[0]->params, JPATH_BASE.DS.'modules'.DS.'mod_yj_booking'.DS.'mod_yj_booking.xml');
		
		/**
		 * get link for offer
		 */
		$sql =  'SELECT a.*, ' .
				' CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug,'. 
				' CASE WHEN CHAR_LENGTH(cc.alias) THEN CONCAT_WS(":", cc.id, cc.alias) ELSE cc.id END as catslug,'.
				'cc.title as cattitle,'.
				's.title as sectitle'.
				' FROM #__content AS a' .
				' INNER JOIN #__categories AS cc ON cc.id = a.catid' .
				' INNER JOIN #__sections AS s ON s.id = a.sectionid' .
				' WHERE a.title LIKE \''.addslashes(strip_tags($_POST['destination_pack'])).'\'';
		$db->setQuery( $sql );
		$offer = $db->loadObjectList();	
		
		require_once(JPATH_BASE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php');
		$base = str_replace('modules/mod_yj_booking/','',JURI::base());
		$doc_url = $base.ContentHelperRoute::getArticleRoute($offer[0]->slug, $offer[0]->catslug, $offer[0]->sectionid);
			
		
		require_once('libs/PHPMailer.class.php');
		$mail = new PHPMailer();
		$mail->From = $_POST['email'];
		$mail->FromName = $_POST['first_name'].' '.$_POST['surname'];
		$mail->Subject = $params->get('email_subject', 'New Booking Information');
		//$mail->ReplyTo = $_POST['email'];
		
		/**
		 * continue with mail
		 */
		$mail->AddAddress($params->get('your_email'));
		
		/**
		 * e-mail body
		 */
		$body = array();
		$body[] = '<table width="500" style="font-family:verdana; font-size:12px;">';
		$body[] = '<tr><td>'.JText::_('Nội dung góp ý: ').' </td>'.$_POST['phone'].'</td></tr>';
		$body[] = '</table>';		
		
		$mail->IsHTML(true);
		$mail->Body = implode("\n",$body);
		require('libs/JSON.class.php');
		if( $mail->Send() ){
			$response = array('success'=> $params->get('sentmsg1').' <a href="'.$doc_url.'" title="'.$_POST['destination_pack'].'">'.$_POST['destination_pack'].'</a> '.$params->get('sentmsg2'));
			$json = new JSON($response);
			echo $json->result;
         
			exit();
		}		
		else{
			$response = array('error'=>JText::_('NOT SENT'));
			$json = new JSON($response);
			echo $json->result;
			exit();
		}	
		
	}
	
?>