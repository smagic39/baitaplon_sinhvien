<?php
/**
* @version		$Id: helper.php 10857 2008-08-30 06:41:16Z willebil $
* @package		dmt
* @copyright	Phan Phuoc Long.
* @license		DanangServices
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

require_once (JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php');

class modShoppingHelper
{
	 function getData($catid, $type, $count) {
        $db =& JFactory::getDBO();
        $strwhere = ($catid == '') ? " WHERE 1" : " WHERE a.category_id in (".$catid.")";
        if($type==0)
            $query = "SELECT * FROM #__pplshop_product as a ".$strwhere." AND a.published = 1 ORDER BY a.created DESC limit ".$count;
        else if ($type==1)
            $query = "SELECT a.* FROM #__pplshop_product as a, #__pplshop_order_details as b ".$strwhere." AND a.id=b.prod_id group by b.prod_id AND a.published = 1 ORDER BY sum(qty) DESC limit ".$count; 
        else if ($type==2)
            $query = "SELECT * FROM #__pplshop_product as a ".$strwhere." AND a.published = 1 ORDER BY hits DESC limit ".$count; 
        $db->setQuery($query);  
        $listChild = $db->loadObjectList();
        return $listChild;     
    } 
}
 