<?php
require_once (JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php');
class modJVNewsHelper{
	var $params = array();
	function __construct($params){
		$this->params = $params;
		$this->createdDirThumb();
	}
	//Function get singleton pattern
	function &getInstance($params){
		static $instance = null;
		if( !$instance ){
			$instance = new modJVNewsHelper($params);
		}
		return $instance;
	}
	function getCategories(){
		$categories = ( array )$this->params->get('categories',array());
		if(count($categories)){
			$strCatId = implode(',',$categories);
			$categoriesCondi = " AND c.id IN ($strCatId)";
		}
		$db	=& JFactory::getDBO();
		$order = (string)$this->params->get('cat_ordering',1);
		switch($order){		
			case "1":
				$orderBy = " ORDER BY c.title ASC";
				break;
			case "2":
				$orderBy = " ORDER BY c.title DESC";
				break;
			case "3":
				$orderBy = " ORDER BY c.ordering";
				break;				
		}
		$sql ="SELECT id,title,section
					 FROM #__categories AS c 
					 WHERE c.published = 1 ".(count($categories) ? $categoriesCondi:'').$orderBy;		
		$db->setQuery($sql);
		$results = $db->loadObjectList();
		$rows = array();
		if(count($results)){
			$i=0;
			foreach($results as $item){
				$rows[$i]->id = $item->id;
				$rows[$i]->title = $item->title;
				$rows[$i]->link = JRoute::_(ContentHelperRoute::getCategoryRoute($item->id,$item->section));
				$i++;
			}
		}
		return $rows;
	}
	function getItemsByCatId($catId){
		global $mainframe;
		$db         =& JFactory::getDBO();
		$user       =& JFactory::getUser();
		$userId     = (int) $user->get('id');
		$count = (int)$this->params->get('no_intro_items') + (int)$this->params->get('no_link_items');
		$intro_lenght = intval($this->params->get( 'intro_length', 200) );
		$aid        = $user->get('aid', 0);
		$imgWidth = $this->params->get('image_width');
		$imgHeight = $this->params->get	('image_height');
		$contentConfig = &JComponentHelper::getParams( 'com_content' );
		$access     = !$contentConfig->get('shownoauth');
		$nullDate   = $db->getNullDate();
		$date =& JFactory::getDate();
		$now = $date->toMySQL();
		$where      = 'a.state = 1'
		. ' AND ( a.publish_up = '.$db->Quote($nullDate).' OR a.publish_up <= '.$db->Quote($now).' )'
		. ' AND ( a.publish_down = '.$db->Quote($nullDate).' OR a.publish_down >= '.$db->Quote($now).' )'
		;
		// Ordering
		$ordering       = 'a.created DESC';
		// Content Items only
		$query = 'SELECT a.*,a.id as key1, cc.id as key2, cc.title as cat_title, ' .
            ' CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug,'.
            ' CASE WHEN CHAR_LENGTH(cc.alias) THEN CONCAT_WS(":", cc.id, cc.alias) ELSE cc.id END as catslug'.
            ' FROM #__content AS a' .             
            ' INNER JOIN #__categories AS cc ON cc.id = a.catid' .
            ' INNER JOIN #__sections AS s ON s.id = a.sectionid' .
            ' WHERE '. $where .' AND s.id > 0' .
		($access ? ' AND a.access <= ' .(int) $aid. ' AND cc.access <= ' .(int) $aid. ' AND s.access <= ' .(int) $aid : '').
            ' AND s.published = 1 AND cc.id='.$catId.
            ' AND cc.published = 1' .
            ' ORDER BY '. $ordering;		
		$db->setQuery($query, 0, $count);
		$rows = $db->loadObjectList();
		$i      = 0;
		$lists  = array();
		foreach ( $rows as $row ){
			//$imageurl = modJVHeadLineHelper::checkImage($row->introtext);
			$imageurl = $this->checkImage($row->introtext);
			$folderImg = DS.$row->id;
			$this->createdDirThumb('com_content',$folderImg);
			$lists[$i]->title = $row->title;
			$lists[$i]->alias = $row->alias;
			$lists[$i]->link = JRoute::_(ContentHelperRoute::getArticleRoute($row->slug, $row->catslug, $row->sectionid));
			$lists[$i]->introtext = $this->introContent($row->introtext, $intro_lenght);
			$lists[$i]->created = $row->created;
			$lists[$i]->thumb ='';
			if($this->FileExists($imageurl)) {
				$lists[$i]->thumb = $this->getThumb($row->introtext,$imgWidth,$imgHeight,false,$row->id);
				$images_size = $this->getImageSizes($lists[$i]->thumb);
				if($images_size[0] != $imgWidth || $images_size[1] != $imgHeight) {
					@unlink($lists[$i]->thumb);
					$lists[$i]->thumb = $this->getThumb($row->introtext,$imgWidth,$imgHeight,false,$row->id);
				}			
			} 
			$i++;
		}
		return $lists;
	}
	
	/*
	 * Function get intro content
	 * @Created by joomvision
	 */
	function introContent( $text, $length=200 ) {
		$text = preg_replace( "'<script[^>]*>.*?</script>'si", "", $text );
		$text = preg_replace( '/{.+?}/', '', $text);
		$text = strip_tags(preg_replace( "'<(br[^/>]*?/|hr[^/>]*?/|/(div|h[1-6]|li|p|td))>'si", ' ', $text ));
		if (strlen($text) > $length) {
			$text = substr($text, 0, strpos($text, ' ', $length)) . "..." ;
		}
		return $text;
	}
	//End
	function createdDirThumb($comp='com_content',$folderImage=''){
		$thumbImgParentFolder = JPATH_BASE.DS.'images'.DS.'stories'.DS.'thumbs'.DS.$comp.$folderImage;
		if(!JFolder::exists($thumbImgParentFolder)){
			JFolder::create($thumbImgParentFolder);
		}
	}
	function getThumb($text, $tWidth,$tHeight, $reflections=false,$id=0){
		preg_match("/\<img.+?src=\"(.+?)\".+?\/>/", $text, $matches);
		$paths = array();
		$showbug = true;
		if (isset($matches[1])) {
			$image_path = $matches[1];
			//joomla 1.5 only
			$full_url = JURI::base();
			//remove any protocol/site info from the image path
			$parsed_url = parse_url($full_url);
			$paths[] = $full_url;
			if (isset($parsed_url['path']) && $parsed_url['path'] != "/") $paths[] = $parsed_url['path'];
			foreach ($paths as $path) {
				if (strpos($image_path,$path) !== false) {
					$image_path = substr($image_path,strpos($image_path, $path)+strlen($path));
				}
			}
			// remove any / that begins the path
			if (substr($image_path, 0 , 1) == '/') $image_path = substr($image_path, 1);
			//if after removing the uri, still has protocol then the image
			//is remote and we don't support thumbs for external images
			if (strpos($image_path,'http://') !== false ||
			strpos($image_path,'https://') !== false) {
				return false;
			}
			// create a thumb filename
			$file_div = strrpos($image_path,'.');
			$thumb_ext = substr($image_path, $file_div);
			$thumb_prev = substr($image_path, 0, $file_div);
			$thumb_path = '';
			$thumb_path = 'images/stories/thumbs/com_content/'.$id.'/jvnews_'.$tWidth.'x'.$tHeight.$thumb_ext;
			// check to see if this file exists, if so we don't need to create it
			if ($thumb_path !='' && function_exists("gd_info") && !file_exists($thumb_path)) {
				// file doens't exist, so create it and save it
				include_once('thumbnail.inc.php');
				$thumb = new JVThumbnail($image_path);
				if ($thumb->error) {
					if ($showbug)   echo "JV Image ERROR: " . $thumb->errmsg . ": " . $image_path;
					return false;
				}
				//$thumb->resize($size);
				$thumb->resize_image($tWidth,$tHeight);
				if ($reflections) {
					$thumb->createReflection(30,30,60,false);
				}
				if (!is_writable(dirname($thumb_path))) {
					$thumb->destruct();
					return false;
				}
				$thumb->save($thumb_path);
				$thumb->destruct();
			}
			return ($thumb_path);
		} else {
			return false;
		}
	}

	/*
	 * Function check existion of image in content
	 * @Created by joomvision
	 */
	function checkImage($file) {
		preg_match("/\<img.+?src=\"(.+?)\".+?\/>/", $file, $matches);
		if(count($matches)){
			return $matches[1];
		} else {return '';}
	}
	//End function
	function FileExists($file) {
		if(file_exists($file))
		return true;
		else
		return false;
	}
	function getImageSizes($file) {
		return getimagesize($file);
	}
}
?>