<?php
/**
 * @version		$Id: modules.php 10381 2008-06-01 03:35:53Z pasamio $
 * @package		Joomla
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

function modChrome_jvrounded($module, &$params, &$attribs)
{ 
?>
		<div class="module<?php echo $params->get('moduleclass_sfx'); ?>">
			<div class="jvmod-c">
				<?php if ($module->showtitle != 0) : ?>
					<h3><span><?php echo $module->title; ?></span></h3>
				<?php endif; ?>
				<div class="jvmod-cc">		
					<?php echo $module->content; ?>
				</div>	
			</div>
		</div>
	<?php
}
function modChrome_jvrounded2($module, &$params, &$attribs)
{ 
?>
		<div class="module<?php echo $params->get('moduleclass_sfx'); ?>" style="position: relative;">
			<div class="jvmod-t">
				<?php if ($module->showtitle != 0) : ?>
					<h3><span><?php echo $module->title; ?></span></h3>
				<?php endif; ?>
			</div>	
			<div class="jvmod-c">
				<div class="jvmod-cc">		
					<?php echo $module->content; ?>
				</div>	
			</div>
			<div class="jvmod-b"></div>
		</div>
	<?php
}
?>