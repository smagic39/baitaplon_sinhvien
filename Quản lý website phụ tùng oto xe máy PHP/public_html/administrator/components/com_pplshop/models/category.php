<?php
 
// No direct access
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport( 'joomla.application.component.model' );
 

class pplshopModelCategory extends JModel
{

    var $_db = null;
    function __construct() {
        $this->_db = JFactory::getDBO();
        parent::__construct();
    }
    
    function getListCategory() {
        $query = "SELECT * FROM #__pplshop_category";
        $this->_db->setQuery($query);
        $listCategory = $this->_db->loadObjectList();
        return $listCategory;
    }
    
    function getCategoryInfo() {
        $array = JRequest::getVar('cid',  0, '', 'array');
        $catid = $array[0];
        $row = JTable::getInstance('Category', 'Table');
        $row->load($catid);
        return $row;        
    }
    function getListParent() {
        $query = "SELECT * FROM #__pplshop_category WHERE parentid = '0' ORDER BY ordering ASC";
        $this->_db->setQuery($query);
        $listParent = $this->_db->loadObjectList();
        return $listParent; 
    }
    
    function getChildCategory($parentid) {
        $query = "SELECT * FROM #__pplshop_category WHERE parentid = '$parentid' ORDER BY ordering ASC";
        $this->_db->setQuery($query);
        $listChild = $this->_db->loadObjectList();
        return $listChild;
    }
    
    function saveCat(){
        $data = JRequest::get('POST');
        $row = JTable::getInstance('Category', 'Table');
        $row->bind($data);
        if(!$row->store()) {
            echo "<script> alert('".addslashes($this->_db->stderr())."');
                 window.history.go(-1); </script>\n"; 
                return false;
        } else {
            return true;
        }
    }
    
    function delCat(){
        $array = JRequest::getVar('cid',  0, '', 'array');
        $catid = $array[0];
       // die( $catid);
        if(!$this->checkHaveChild($catid)){
            if(!$this->checkHaveItems($catid)){
                $row = JTable::getInstance('Category', 'Table');
                if (!$row->delete( $catid )) {
                    $this->setError( $row->getErrorMsg() );
                    return false;
                } else {
                    return true;
                }
            } else {return false;}
        } else { return false;}
        
    }
    
    function checkHaveChild($catid){
        $query = "SELECT count(*) FROM #__pplshop_category WHERE parentid = '$catid'";
        $this->_db->setQuery($query);
        $haveChild = $this->_db->loadResult();
         if($haveChild > 0) {
            return true;
        }else {
            return false;
        }
  
    }
    
    function checkHaveItems($catid) {
        $query = "SELECT count(*) FROM #__pplshop_product WHERE category_id = '$catid'";
        $this->_db->setQuery($query);
        $haveChild = $this->_db->loadResult();
        if($haveChild > 0) {
            return true;
        } else return false;
    }
}
