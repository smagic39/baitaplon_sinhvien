<?php

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * Hello Table class
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class TableProduct extends JTable
{

    var $id = null;
    var $product_name = null;
    var $manufacturer = null;
    var $category_id= null;
    var $price= null;
    var $priceb = null;
    var $hoted = null;  
    var $image1= null;  
    var $image2= null;
    var $width = null;
    var $height = null;
    var $diameter = null; 
    var $material = null;
    var $capacity = null;
    var $warranty = null;
    var $description= null;
    var $published = null;
    var $created = null;
    var $hits = null;   

    function TableProduct(& $db) {
        parent::__construct('#__pplshop_product', 'id', $db);
    }
}