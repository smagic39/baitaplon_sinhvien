<?php

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * Hello Table class
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class TableCategory extends JTable
{
    /**
     * Primary Key
     *
     * @var int
     */
    var $id = null;

    /**
     * @var string
     */
    var $category_name = null;
    var $parentid = null;
    var $ordering = null; 

    function TableCategory(& $db) {
        parent::__construct('#__pplshop_category', 'id', $db);
    }
}