<?php

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * Hello Table class
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class TableOrderdetail extends JTable
{

    var $id = null;
    var $order_id = null;
    var $prod_id = null;
    var $qty= null;
    var $unitprice= null;
    var $total = null;  
    var $shorttext= null;  
   

    function TableProduct(& $db) {
        parent::__construct('#__pplshop_order_details', 'id', $db);
    }
}