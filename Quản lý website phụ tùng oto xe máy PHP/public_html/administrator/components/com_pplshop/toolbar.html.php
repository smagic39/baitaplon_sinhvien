<?
class TOOLBAR_pplshop {
    function _editCat() {
        JToolBarHelper::title(  JText::_( 'Chỉnh sửa Danh mục' ), 'generic.png' );  
        JToolBarHelper::save('saveCat');
        JToolBarHelper::cancel('cancelCat');
    }
    function _addCat() {
        JToolBarHelper::title(  JText::_( 'Thêm mới Danh mục' ), 'generic.png' );  
        JToolBarHelper::save('saveCat');
        JToolBarHelper::cancel('cancelCat');
    }
    function _listCat() {
        JToolBarHelper::title(   JText::_( 'Quản lý Danh mục' ), 'generic.png' );
        JToolBarHelper::deleteList('Bạn có chắc chắn xóa Danh mục này không?','delCat');
        JToolBarHelper::editListX('editCat');
        JToolBarHelper::addNewX('addCat');
    }
     function _viewOrder() {
        JToolBarHelper::title(   JText::_( 'Quản lý Đơn đặt hàng' ), 'generic.png' );
        JToolBarHelper::divider();
        JToolBarHelper::deleteList('Bạn có chắc chắn xóa đơn đặt hàng này không?','delOrder');

    }
    function _DEFAULT() {
        JToolBarHelper::title(   JText::_( 'Quản lý sản phẩm' ), 'generic.png' );
        JToolBarHelper::divider();
        JToolBarHelper::publishList();
        JToolBarHelper::unpublishList();
        JToolBarHelper::deleteList('Bạn có chắc chắn xóa sản phẩm này không?','delProduct');  
        JToolBarHelper::editListX('editProduct');
        JToolBarHelper::addNewX('addProduct');
    }
    
    function _addProduct() {
        JToolBarHelper::title(  JText::_( 'Thêm mới sản phẩm' ), 'generic.png' );  
        JToolBarHelper::save('saveProduct');
        JToolBarHelper::cancel();
    }
     function _orderDetail() {
        JToolBarHelper::title(  JText::_( 'Xem thông tin Đơn đặt hàng' ), 'generic.png' );  
        JToolBarHelper::save('saveOrder');
        JToolBarHelper::cancel('cancelOrder');

    }
    function _editProduct() {
        JToolBarHelper::title(  JText::_( 'Thay đổi thông tin sản phẩm' ), 'generic.png' );  
        JToolBarHelper::save('saveProduct');
        JToolBarHelper::cancel();
    }
}
?> 