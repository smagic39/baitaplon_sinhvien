<?php
/**
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://docs.joomla.org/Developing_a_Model-View-Controller_Component_-_Part_1
 * @license    GNU/GPL
*/
 
// no direct access
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport( 'joomla.application.component.view');
 
/**
 * HTML View class for the HelloWorld Component
 *
 * @package    HelloWorld
 */
 
class pplshopViewProduct extends JView
{
    function display($tpl = null)
    {
        $model = $this->getModel();
        $layout = $this->getLayout();  
        if($layout == 'default') {
            $items['product'] = $model->getListProduct();
            $items['listParent'] = $model->getListParent();
        } else if($layout == 'editProduct') {
            $items['listParent'] = $model->getListParent();
            $items['product'] = $model->getProductInfo();
        }
        $this->assignRef( 'items', $items );
        parent::display($tpl);
    }
    
    
}
