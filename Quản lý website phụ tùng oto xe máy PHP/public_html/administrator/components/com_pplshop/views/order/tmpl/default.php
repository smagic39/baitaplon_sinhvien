<?php
    defined('_JEXEC') or die('Restricted access');
    $order = $this->items['Order'];
    $listProduct = $order['listOrder'];
    $nav = $order['Nav'];
    $category_id = $product['category_id'];
    $listParent = $this->items['listParent']; 
    $model = $this->getModel();
    
    
    
?>

<form action="index.php" method="post" name="adminForm">
<div id="editcell">

    <table class="adminlist">
    <thead>
        <tr>
            <th width="5">
                <?php echo JText::_( 'ID' ); ?>
            </th>
            <th width="20">
                <input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $listProduct ); ?>);" />
            </th>            
            <th>
                <?php echo JText::_( 'Người đặt hàng' ); ?>
            </th>
            
            <th>
                <?php echo JText::_( 'Email' ); ?>
            </th>
             <th>
                <?php echo JText::_( 'Điện thoại' ); ?>
            </th>
            <th>
                <?php echo JText::_( 'Tổng giá trị' ); ?>
            </th>
            <th>
                <?php echo JText::_( 'Ngày đặt hàng' ); ?>
            </th>

           
        </tr>
    </thead>
    <?php
    jimport('joomla.filter.output');
    $k = 0;
    for ($i=0, $n=count( $listProduct ); $i < $n; $i++)    {
        $row = &$listProduct[$i];
        $checked     = JHTML::_('grid.id',   $i, $row->id );

        $link         = JRoute::_( 'index.php?option=com_pplshop&task=orderDetail&cid[]='. $row->id );
        
        ?>
        <tr class="<?php echo "row$k"; ?>">
            <td>
                <?php echo $row->id; ?>
            </td>
            <td>
                <?php echo $checked; ?>
            </td>
            <td>
                <a href="<?php echo $link; ?>"><?php echo $row->name; ?></a>
            </td>
             <td>
                <a href="<?php echo $link; ?>"><?php echo $row->email; ?></a>
            </td>
             <td>
                <a href="<?php echo $link; ?>"><?php echo $row->phone; ?></a>
            </td>
            
            <td>
                <?php echo number_format($row->total, 0, '', '.'); ?>
            </td>
             <td align="center">
                <?php echo JHTML::date($row->order_time, '%d.%m.%Y %H') ?></td>
            </td>
            
            
        </tr>
        <?php
        $k = 1 - $k;
    } if($nav->getPagesLinks() != null) {
    ?>
        <tr class="row1"> 
             <td colspan="8 ">
                <?=$nav->getListFooter(); ?>
              </td> 
        </tr>
       <?php } ?> 
    </table>
</div>

<input type="hidden" name="option" value="com_pplshop" />
<input type="hidden" name="task" value="viewOrder" />
<input type="hidden" name="boxchecked" value="0" />
</form>
