<?php
    defined('_JEXEC') or die('Restricted access');
    $order = $this->items['order'];
    $listProduct = $this->items['order_detail'];
    $menthod = array(
                    1 =>'Chuyển tiền qua bưu điện hoặc dịch vụ. ',
                    2 =>'Chuyển tiền vào tài khoản ngân hàng. ',
                    3 =>'Thanh toán khi nhận hàng. ',
                    )

?>

<form action="index.php" method="post" name="adminForm" id="adminForm" >
<div class="col100">
<table width="100%">
<tr><td><b><?php echo JText::_('Mã số đơn hàng').": ".$order->id;?></b></td><td>Ngày đặt hàng: <?php echo JHTML::date($order->order_time,'%d.%m.%Y');?></td></tr>
<tr><td>
    <div id="pplshop_com">THÔNG TIN NGƯỜI ĐẶT HÀNG</div>
        <table width="100%" cellpadding="2" cellspacing="5" style="border: 1px solid green">
        <tr><td width="150px">Họ và tên </td><td><?php echo $order->name;?></td></tr>     
        <tr><td width="150px">Email </td><td><?php echo $order->email;?></td></tr>
        <tr><td width="150px">Điện thoại </td><td><?php echo $order->phone;?></td></tr>
        <tr><td width="150px">Tỉnh / Thành phố</td><td> <?php echo $order->city;?></td></tr>
        <tr><td width="150px">Địa chỉ </td><td><?php echo $order->address;?></td></tr>     
        </table>                    
</td><td>
    <div id="pplshop_com">THÔNG TIN NGƯỜI NHẬN HÀNG</div>
        <table width="100%" cellpadding="2" cellspacing="5" style="border: 1px solid green">
        <tr><td width="150px">Họ và tên </td><td><?php echo $order->name_rev;?></td></tr>     
        <tr><td width="150px">Email </td><td><?php echo $order->email_rev;?></td></tr>
        <tr><td width="150px">Điện thoại </td><td><?php echo $order->phone_rev;?></td></tr>
        <tr><td width="150px">Tỉnh / Thành phố</td><td> <?php echo $order->city_rev;?></td></tr>
        <tr><td width="150px">Địa chỉ </td><td><?php echo $order->address_rev;?></td></tr>     
        </table>                    
</td></tr>
<tr><td colspan="2">
    <div id="pplshop_com">CHI TIẾT ĐƠN ĐẶT HÀNG</div>
      
         <table id="admin" width="100%" cellpadding="2" cellspacing="1" style="border: 1px dashed rgb(172, 198, 141);">
                   <th width="10px" >ID</th><th width="250px">Tên sản phẩm</th><th width="100px">Đơn giá</th><th width="70px">Số lượng</th><th width="100px">Tổng giá</th><th width="12px"></th>
                    <?php
                       $tonggia =0;
                        for ($i=0, $n=count( $listProduct ); $i < $n; $i++){
                          
                          $rs1 =& $listProduct[$i]; 
                          $tonggia += ($rs1->priceb*$rs1->qty);
                          echo "<tr height='22px'><td align='center' style=\"border-bottom:1px dashed rgb(172, 198, 141);\">".$rs1->id."</td><td style=\"border-bottom:1px dashed rgb(172, 198, 141);\">".$rs1->product_name."</td><td align='right' style=\"border-bottom:1px dashed rgb(172, 198, 141);\">".number_format($rs1->priceb,0)."<br><span style='font-size: 9px'>".$rs1->price_detail."</span></td><td align='center' style=\"border-bottom:1px dashed rgb(172, 198, 141);\">".$rs1->qty."</td><td align='right' style=\"border-bottom:1px dashed rgb(172, 198, 141);\">".number_format($rs1->priceb*$rs1->qty, 0)."</td><td align='center' style=\"border-bottom:1px dashed rgb(172, 198, 141);\">vnd</td></tr>";
                        }                    
                    ?>
                    <tr height='25px'><td colspan="4" align="right"><b>Tổng giá trị của đơn đặt hàng:</b></td><td align='right'><b><?php echo number_format($tonggia,0)?></b></td><td align="center">VND</td></tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
         
        </table>

</td></tr>
<tr><td>Phương thức thanh toán: </td><td><?php echo $menthod[$order->payment_menthod];?>
</td></tr>
<tr><td colspan="2">Tình trạng đơn hàng: 
<select name="status">
    <option value="0"> Đơn hàng mới </option>
    <option value="1" selected="selected"> Đã xem - Đang xử lý </option>
    <option value="2"> Đã hoàn thành </option>
</select>
</td></tr>
<tr><td colspan="2">Ghi chú: </td></tr>
<tr><td colspan="2">
 <?php
                $editor =& JFactory::getEditor();
                echo $editor->display('notes', $order->notes, '100%', '200', '20', '15', false);
        ?>

</td></tr>
</table>    
</div>
<div class="clr"></div>
<input type="hidden" name="option" value="com_pplshop" />
<input type="hidden" name="id" value="<?php echo $order->id; ?>" />
<input type="hidden" name="task" value="saveOrder" />
<input type="hidden" name="controller" value="" />
</form>

