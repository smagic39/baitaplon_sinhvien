<?
defined('_JEXEC')or die('Restrict Access');
switch($task){
    case 'addCat':
        TOOLBAR_pplshop::_addCat();
        break;
    case 'editCat':
		TOOLBAR_pplshop::_editCat();
		break;
    case 'listCat':
        TOOLBAR_pplshop::_listCat();
        break;
    case 'addProduct':
        TOOLBAR_pplshop::_addProduct();
        break;
    case 'editProduct':
        TOOLBAR_pplshop::_editProduct();
        break;
    case 'viewOrder':
        TOOLBAR_pplshop::_viewOrder();
        break;
    case 'orderDetail':
        TOOLBAR_pplshop::_orderDetail();
        break;     
	default:
		TOOLBAR_pplshop::_DEFAULT();
		break;
}
?>