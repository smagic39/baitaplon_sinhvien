 <link rel="stylesheet"  href="components/com_pplshop/views/product/tmpl/style/pplshop.css" type="text/css"    />
<?php 
    defined('_JEXEC') or die('Restricted access');
   $listProduct = $this->items['listProduct'];
   $Nav = $this->items['Nav']; 
       
?>
    <script>
    function dispInfo(i)
    {
        if (i==1)
         document.getElementById('infoRev').style.display = 'block';
        else if (i==0)
         document.getElementById('infoRev').style.display = 'none';
    }
    function checkInfo()
    {
        frmcheck = document.order;
        if (frmcheck.name.value == '')
         {
             alert('Nhập thông tin Tên người đặt hàng');
             frmcheck.name.focus();
             
         }
        else if (frmcheck.email.value == '')
         {
             alert('Nhập Email người đặt hàng');
             frmcheck.email.focus();
             
         }
        else if (frmcheck.phone.value == '')
         {
             alert('Nhập Số điện thoại người đặt hàng');
             frmcheck.phone.focus();
             
         }
        else if (frmcheck.address.value == '')
         {
             alert('Nhập thông tin Địa chỉ người đặt hàng');
             frmcheck.address.focus();
             
         }
        else if (document.getElementById('infoRev').style.display == 'block')
         {
             if (frmcheck.name_rev.value == '')
             {
                 alert('Nhập thông tin Họ tên người nhận hàng');
                 frmcheck.name_rev.focus();
                 
             }
            else if (frmcheck.phone_rev.value == '')
             {
                 alert('Nhập Số điện thoại người nhận hàng');
                 frmcheck.phone_rev.focus();
                 
             }
            else if (frmcheck.address_rev.value == '')
             {
                 alert('Nhập thông tin Địa chỉ người nhận hàng');
                 frmcheck.address_rev.focus();
                 
             }
            else frmcheck.submit(); 
         }                             
        else frmcheck.submit();
    }
    </script>
    <div class="content_pplshop">                
        <div class="breakline">
            <div style="padding: 4px;">
            <?php
            if(isset($_SESSION['pplCart']) && count($_SESSION['pplCart'])>0){  
                ?>
                    <form method="post" name="order" onsubmit="return checkInfo();">
                     <div id="frmContact">
                      <div id="pplshop_com">THÔNG TIN NGƯỜI ĐẶT HÀNG</div>
                        <table width="100%" cellpadding="2" cellspacing="5" style="border: 1px solid green">
                        <tr><td width="150px">Họ và tên (*)</td><td><input type="text" name="name" size="40" /></td></tr>     
                        <tr><td width="150px">Email (*)</td><td><input type="text" name="email" size="40" /></td></tr>
                        <tr><td width="150px">Điện thoại (*)</td><td><input type="text" name="phone" size="40" /></td></tr>
                        <tr><td width="150px">Tỉnh / Thành phố</td><td>
                            <select name='city' style="width:200px">
                                <option value="Đà Nẵng">Đà Nẵng</option>
                                <option value="Quảng Nam">Quảng Nam</option>
                            </select>
                        </td></tr>
                        <tr><td width="150px">Địa chỉ (*)</td><td><textarea name="address" cols="30"></textarea></td></tr>     
                        </table>
                          
                     </div>
                      <div id="frmContact">
                      <div id="pplshop_com"><span style="padding: 0 80px 0 0">THÔNG TIN NGƯỜI NHẬN HÀNG</span><span style="padding: 0 10px 0 0"><input type="radio" name="nhanhang" checked="checked" value="1" onclick="dispInfo(0);">Là người đặt hàng</span><span><input type="radio" name="nhanhang" onclick="dispInfo(1);" value="0">Khác</span></div>
                        <div id='infoRev' style="display: none">
                        <table width="100%" cellpadding="2" cellspacing="5" style="border: 1px solid green">
                        <tr><td width="150px">Họ và tên (*)</td><td><input type="text" name="name_rev" size="40" /></td></tr>     
                         <tr><td width="150px">Email </td><td><input type="text" name="email_rev" size="40" /></td></tr>
                        <tr><td width="150px">Điện thoại (*)</td><td><input type="text" name="phone_rev" size="40" /></td></tr>
                        <tr><td width="150px">Tỉnh / Thành phố</td><td>
                            <select name='city_rev' style="width:200px">
                                <option value="Đà Nẵng">Đà Nẵng</option>
                                 <option value="Quảng Nam">Quảng Nam</option>
                            </select>
                        </td></tr>            
                        <tr><td width="150px">Địa chỉ (*)</td><td><textarea name="address_rev" cols="30"></textarea></td></tr>     
                        </table>
                        </div>  
                     </div>
                     <div id="frmPayment">
                       <div id="pplshop_com">PHƯƠNG THỨC THANH TOÁN</div>
                        <table width="100%" cellpadding="2" cellspacing="5" style="border: 1px solid green">
                        <tr><td width="10px"><input type="radio" name="menthod" value="1"></td><td>Ghi nợ <br>(Đối với khách hàng truyền thống của <b> Áo Trắng </b>)</td></tr>     
                        <tr><td width="10px"><input type="radio" name="menthod" checked="checked" value="2"></td><td>Chuyển tiền vào tài khoản ngân hàng <br>(Mr Nguyễn Chức - STK: 4206205000283 - Ngân hàng NN&PTNT Quế Sơn - Quảng Nam; Tài khoản thẻ ATM Agribank: 4206215001073)<br> (Chuyển tiền trước nhận hàng sau)</td></tr>
                        <tr><td width="10px"><input type="radio" name="menthod" value="3"></td><td>Thanh toán khi nhận hàng <br> (Thanh toán tiền mặt cho người giao hàng)</td></tr>
                        
                        </table>
                     </div>
                     <div id="frmVanchuyen">
                     </div>
                       <div>Những phần đánh dấu (*) là thông tin bắt buộc nhập.</div> 
                       <div style="text-align:right; padding:10px 5px 0"><a href="javascript:history.go(-1);"><span id="pplshop_btn_order"> Xem lại đơn hàng</span></a><a href="javascript:checkInfo();"><span id="pplshop_btn_order">Hoàn tất đơn hàng</span></a></div> 
                       <input type="hidden" name="option" value="com_pplshop" />
                       <input type="hidden" name="task" value="vieworder" />
                       <input type="hidden" name="Itemid" value="<?=JRequest::getInt('Itemid')?>" />
                    </form>
                    
             <?php
            }else echo "Bạn chưa chọn sản phẩm để đặt hàng.";                                                                                                                                                                             
             ?>
             </div>
    </div>        
    </div>

