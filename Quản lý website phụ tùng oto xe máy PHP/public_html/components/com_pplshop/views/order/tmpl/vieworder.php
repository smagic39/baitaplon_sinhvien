<?php 
defined('_JEXEC') or die('Restricted access');

    $listProduct =$this->items['orderDetail'];
    $contactInfo =$this->items['contactInfo'];
    $contactRevInfo =$this->items['contactRevInfo']; 
    $menthod =$this->items['menthod']; 
    $menthod_payment = array(
                    1 =>'Ghi nợ. ',
                    2 =>'Chuyển tiền vào tài khoản ngân hàng. ',
                    3 =>'Thanh toán khi nhận hàng. ',
                    )
?>
<link rel="stylesheet"  href="components/com_pplshop/views/product/tmpl/style/pplshop.css" type="text/css"    />     
<div>
 <table width="100%" cellpadding="0" cellspacing="3" border="0">
 <tr>
 <td  valign="top">
     <div id="pplshop_com">THÔNG TIN NGƯỜI ĐẶT HÀNG</div>
        <table width="100%" cellpadding="2" cellspacing="5" style="border: 1px solid green">
        <tr><td width="130px">Họ và tên </td><td><?php echo $contactInfo['name'];?></td></tr>     
        <tr><td width="130px">Email </td><td><?php echo $contactInfo['email'];?></td></tr>
        <tr><td width="130px">Điện thoại </td><td><?php echo $contactInfo['phone'];?></td></tr>
        <tr><td width="130px">Tỉnh / Thành phố</td><td> <?php echo $contactInfo['city'];?></td></tr>
        <tr><td width="130px">Địa chỉ </td><td><?php echo $contactInfo['address'];?></td></tr>     
        </table>
        <input type="hidden" name="name" value="<?php echo $contactInfo['name'];?>" />                       
       </div>
 </td> </tr><tr>
 <td valign="top">
    <div id="pplshop_com">THÔNG TIN NGƯỜI NHẬN HÀNG</div>
        <table width="100%" cellpadding="2" cellspacing="5" style="border: 1px solid green">
        <tr><td width="130px">Họ và tên </td><td><?php echo $contactRevInfo['name'];?></td></tr>  
        <tr><td width="130px">Email </td><td><?php echo $contactRevInfo['email'];?></td></tr>    
        <tr><td width="130px">Điện thoại </td><td><?php echo $contactRevInfo['phone'];?></td></tr>
        <tr><td width="130px">Tỉnh / Thành phố</td><td><?php echo $contactRevInfo['city'];?></td></tr>            
        <tr><td width="130px">Địa chỉ </td><td><?php echo $contactRevInfo['address'];?></td></tr>     
        </table>
      </div> 
 </td></tr>
 <tr><td> 
    <div id="pplshop_com">CHI TIẾT ĐƠN ĐẶT HÀNG</div>
      
         <table id="admin" width="100%" cellpadding="2" cellspacing="1" style="border: 1px dashed rgb(172, 198, 141);">
                   <th width="10px" >ID</th><th width="250px"><?php echo JText::_('PRODUCT NAME');?></th><th width="100px"><?php echo JText::_('UNIT PRICE');?></th><th width="70px"><?php echo JText::_('QUANTITY');?></th><th width="100px"><?php echo JText::_('SUB TOTAL');?></th><th width="12px"></th>
                    <?php
                       $tonggia =0;
                        for ($i=0, $n=count( $listProduct ); $i < $n; $i++){
                          
                          $rs1 =& $listProduct[$i];
                          $priceb =  ($rs1->priceb != 0) ? $rs1->priceb : $rs1->price;
                          $link1         = JRoute::_( 'index.php?option=com_pplshop&task=removefromcart&prod_id='. $rs1->id );  
                          $link2         = JRoute::_( 'index.php?option=com_pplshop&task=checkout');  
                          $tonggia += ($priceb*$_SESSION['pplCart'][$rs1->id]);
                          echo "<tr height='22px'><td align='center' style=\"border-bottom:1px dashed rgb(172, 198, 141);\">".$rs1->id."</td><td style=\"border-bottom:1px dashed rgb(172, 198, 141);\">".$rs1->product_name."</td><td align='right' style=\"border-bottom:1px dashed rgb(172, 198, 141);\">".number_format($priceb,0)."</td><td align='center' style=\"border-bottom:1px dashed rgb(172, 198, 141);\">".$_SESSION['pplCart'][$rs1->id]."</td><td align='right' style=\"border-bottom:1px dashed rgb(172, 198, 141);\">".number_format($priceb*$_SESSION['pplCart'][$rs1->id], 0)."</td><td align='center' style=\"border-bottom:1px dashed rgb(172, 198, 141);\"></td></tr>";
                        }                    
                    ?>
                    <tr height='25px'><td colspan="4" align="right"><b><?php echo JText::_('TOTAL');?>:</b></td><td align='right'><b><?php echo number_format($tonggia,0)?></b></td><td align="center"><?php echo JText::_('USD');?></td></tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
         
        </table>
      </div> </td></tr>
 <tr><td>
    <div id="pplshop_com">PHƯƠNG THỨC THANH TOÁN</div>
        <table width="100%" cellpadding="2" cellspacing="5" style="border: 1px solid green"> 
        <tr><td>
            <?php echo $menthod_payment[$menthod];?>
        </td></tr>
      </table> 
 </td></tr>
 </table>
<div style="text-align:right; padding:10px"><a href="index.php"><span id="pplshop_btn_order"> Đặt đơn hàng khác</span></a><a href="#"><span id="pplshop_btn_order">In đơn hàng</span></a></div> 

</div>
<?php unset($_SESSION['pplCart']);?>