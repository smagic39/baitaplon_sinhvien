<?php
/**
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://docs.joomla.org/Developing_a_Model-View-Controller_Component_-_Part_1
 * @license    GNU/GPL
*/
 
// no direct access
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport( 'joomla.application.component.view');
 
/**
 * HTML View class for the HelloWorld Component
 *
 * @package    HelloWorld
 */
 
class pplshopViewOrder extends JView
{
     
    function display($tpl = null)
    {
        $model = $this->getModel();
        $layout = $this->getLayout();
       
        //$layout     = JRequest::getCmd('layout');
        //$task        = JRequest::getCmd('task');

       
            
               if ($layout == 'vieworder') {
                  $items =$model->getOrder();
               } else if  ( $layout == 'viewcart') {
                $items =$model->getCart(); 
               } else if  ( $layout == 'checkout') {
                $items =$model->getCheckout(); 
               }       
                    
        
        
    
        $this->assignRef( 'items', $items );
        parent::display($tpl);
        
    }
   
}