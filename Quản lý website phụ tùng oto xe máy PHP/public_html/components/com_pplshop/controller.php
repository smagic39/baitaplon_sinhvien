<?php

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');

/**
 * Hello World Component Controller
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class pplshopController extends JController
{
      
 
   function display()
    {
        JRequest::setVar('view','category');
        JRequest::setVar('layout','index');
        parent::display();        

    }
    
  
     function viewDetails()
    {
        JRequest::setVar('view', 'product');
        JRequest::setVar('layout','viewDetails');
        parent::display();
    }
    function product(){
        JRequest::setVar('view', 'product');
        JRequest::setVar('layout','viewProduct');
        parent::display();
    }
     function producten(){
        JRequest::setVar('view', 'product');
        JRequest::setVar('layout','viewproducten');
        parent::display();
    }
    function viewcart(){
        JRequest::setVar('view', 'order');
        JRequest::setVar('layout','viewcart');
        parent::display();
    }
    function vieworder(){
        JRequest::setVar('view', 'order');
        JRequest::setVar('layout','vieworder');
        parent::display();
    }
    function checkout(){
        JRequest::setVar('view', 'order');
        JRequest::setVar('layout','checkout');
        parent::display();
    }
    function addtocart()
    {
        $db=& JFactory::getDBO(); 
        $productid = JRequest::getInt('prod_id');
        $queryup = "UPDATE #__pplshop_product SET hits = hits + 1 WHERE id = $productid";
        $db->setQuery($queryup);
        $db->query();
        
        if(!is_array($_SESSION['pplCart']))
        {
               session_start();
               $_SESSION['pplCart'] = '';
        }

                //session_register("pplCart");
                $prod_id =  JRequest::getInt('prod_id');
               // die($prod_id) ;
                if (isset($_SESSION['pplCart']))
                  $qty =  $_SESSION['pplCart'][$prod_id] + 1;
                else $qty = 1;
               $_SESSION['pplCart'][$prod_id] = $qty;
               
         $link =  JRoute::_('index.php?option=com_pplshop&task=viewcart&Itemid='.JRequest::getInt('Itemid'));  
         $this->setRedirect($link);
    }
    function removefromcart()
    {
           $prod_id =  JRequest::getInt('prod_id');
            if($prod_id == 0)
            {
                unset($_SESSION['pplCart']);
            }
            else
            {
                unset($_SESSION['pplCart'][$prod_id]);
            }
         $link =  JRoute::_('index.php?option=com_pplshop&task=viewcart&Itemid='.JRequest::getInt('Itemid'));   
         $this->setRedirect($link);
    }
    function updatecart()
    {
          $prod_id =  JRequest::getInt('prod_id');
          foreach($_SESSION['pplCart'] as $key=>$value)
          {
              $tmp = 'txt'.$key;
              $qtytmp = JRequest::getVar($tmp);
              if (($qtytmp ==0) && is_numeric($qtytmp))
                   unset($_SESSION['pplCart'][$key]);
              elseif (($qtytmp > 0) && is_numeric($qtytmp))
                   $_SESSION['pplCart'][$key] = $qtytmp;
          }
            
         $link =  JRoute::_('index.php?option=com_pplshop&task=viewcart&Itemid='.JRequest::getInt('Itemid'));  
         $this->setRedirect($link);
    }

}

?>
