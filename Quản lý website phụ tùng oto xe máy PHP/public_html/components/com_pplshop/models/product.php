<?php
 
// No direct access
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport( 'joomla.application.component.model' );
require_once( JPATH_COMPONENT_ADMINISTRATOR.DS.'helpers'.DS.'images.php' ); 
 
/**
 * Hello Model
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class pplshopModelProduct extends JModel
{
    /**
     * Gets the greeting
     * @return string The greeting to be displayed to the user
     */
    function getListProduct()
    {
        jimport('joomla.html.pagination'); 
        $category_id = JRequest::getVar('catid');
        $listcat = $this->getAllCatId($category_id);
        $keyword = JRequest::getVar('keyword');
        $giatu = (JRequest::getVar('giatu')>1) ? " AND priceb > '".JRequest::getVar('giatu')."'" : "";
        $giaden = (JRequest::getVar('giaden')>1) ? "AND priceb < '".JRequest::getVar('giaden')."'" : "";
        $strkey = ($keyword) ? " AND product_name like '%$keyword%' " : ""; 
        $db =& JFactory::getDBO();
        $limit = 12; 
        $limitstart = JRequest::getVar('limitstart', 0, '', 'int');
        
        $query = "SELECT * FROM #__pplshop_product WHERE published ='1' $strkey $giatu $giaden";
        ($category_id) ? $query .= " AND category_id IN $listcat " : "";
        $query .=" order by created DESC";                                   

        $db->setQuery( $query );
        $listProduct = $db->loadObjectList();
        $total = count($listProduct);

        $items['Nav'] = new JPagination($total, $limitstart, $limit);
        $items['listProduct'] = $this->_getList($query, $limitstart, $limit);
        $query3 ="SELECT * FROM #__pplshop_category WHERE id = '".$category_id."'";
        $db->setQuery( $query3 );
        $items['cat'] = $db->loadObject();
        return $items;
    }
    function getListProductEn()
    {
        jimport('joomla.html.pagination'); 
        $category_id = JRequest::getVar('catid');
        $listcat = $this->getAllCatId($category_id);
        $keyword = JRequest::getVar('keyword');
        $giatu = (JRequest::getVar('giatu')>1) ? " AND ((a.value > '".JRequest::getVar('giatu')."') AND (a.reference_field='priceb')) " : "";
        $giaden = (JRequest::getVar('giaden')>1) ? "AND ((a.value < '".JRequest::getVar('giaden')."') AND (a.reference_field='priceb')) " : "";
        $strkey = ($keyword) ? " AND ( (a.value like '%$keyword%' ) AND (a.reference_field = 'product_name') )" : ""; 
        $db =& JFactory::getDBO();
        $limit = 12; 
        $limitstart = JRequest::getVar('limitstart', 0, '', 'int');
        
        $querysrch = "SELECT a.reference_id FROM #__jf_content as a WHERE a.reference_table='pplshop_product' AND a.published ='1' $strkey $giatu $giaden";
        
        $query = "SELECT * FROM #__pplshop_product WHERE published ='1'";
        ($category_id) ? $query .= " AND category_id IN $listcat " : "";
        $query .= " AND id IN (".$querysrch." )";
        $query .=" order by created DESC";                                   

        $db->setQuery( $query );
        $listProduct = $db->loadObjectList();
        $total = count($listProduct);

        $items['Nav'] = new JPagination($total, $limitstart, $limit);
        $items['listProduct'] = $this->_getList($query, $limitstart, $limit);
        $query3 ="SELECT * FROM #__pplshop_category WHERE id = '".$category_id."'";
        $db->setQuery( $query3 );
        $items['cat'] = $db->loadObject();
        return $items;
    }
    function getAllCatId($catid) {
        $query = "SELECT id FROM #__pplshop_category WHERE parentid = '$catid' ORDER BY ordering";
        $this->_db->setQuery($query);
        $listId = $this->_db->loadResultArray();
        $list_temp = implode(',', $listId);
        if(count($listId) > 0) {
            $list = '('.$catid.','.$list_temp.')';
        } else if(count($listId == 0)) {
            $list = '('.$catid.')';
        }
        return $list; 
    }
    
   function getProduct()
    {
       $db=& JFactory::getDBO();
       $array = JRequest::getVar('cid',  0, '', 'array');
       $productid = $array[0];
       $document = &JFactory::getDocument();
        $queryup = "UPDATE #__pplshop_product SET hits = hits + 1 WHERE id = '$productid'";
        $db->setQuery($queryup);
        $db->query();
        $query ="SELECT * FROM #__pplshop_product WHERE published = '1' AND id = '".$productid."'";
        $db->setQuery( $query );
        $Product['pr'] = $db->loadObject();
        $query2 ="SELECT * FROM #__pplshop_product WHERE published = '1' AND id != '".$productid."' AND category_id = '".$Product['pr']->category_id."' ORDER BY created DESC limit 9";
        $db->setQuery( $query2 );
        $Product['other'] = $db->loadObjectList();
        $query3 ="SELECT * FROM #__pplshop_category WHERE id = '".$Product['pr']->category_id."'";
        $db->setQuery( $query3 );
        $Product['catname'] = $db->loadObject();
        $document->setTitle($Product['pr']->product_name);
        return $Product;
    } 
    
      
    
	function getData()
	{
		// Load the Category data
		if ($this->_loadData())
		{
			// Initialize some variables
			$user	=& JFactory::getUser();

			// raise errors
		}

		return $this->_data;
	}
    
    
    
}
