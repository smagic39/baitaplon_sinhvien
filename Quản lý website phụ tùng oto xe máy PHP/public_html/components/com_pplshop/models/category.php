<?php
  defined('_JEXEC') or die();
  jimport('joomla.application.component.model');
  class pplshopModelCategory extends JModel{
       function getAllCat(){
           $sql ="Select * from #__pplshop_category where parentid = 0 ORDER BY ordering";
           $this->_db->setQuery($sql);
           return $this->_db->LoadObjectList();
       }
     function getAllCatId($catid) {
        $db = JFactory::getDBO();
        $query = "SELECT id FROM #__pplshop_category WHERE parentid = '$catid' ORDER BY ordering";
        $db->setQuery($query);
        $listId = $db->loadResultArray();
        $list_temp = implode(',', $listId);
        if(count($listId) > 0) {
            $list = '('.$catid.','.$list_temp.')';
        } else if(count($listId == 0)) {
            $list = '('.$catid.')';
        }
        return $list; 
    }
    
    function getItems($catid) {
        $db = JFactory::getDBO();
        $list = $this->getAllCatId($catid);
        $query = "SELECT * FROM #__pplshop_product as a WHERE a.category_id IN $list AND a.published = '1' order by a.created DESC limit 6";
        $db->setQuery($query);
        $Items = $db->LoadObjectList();
        return $Items;
    }      
  }
?>
