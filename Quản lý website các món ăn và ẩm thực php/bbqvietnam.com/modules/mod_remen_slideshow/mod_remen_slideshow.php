<?php
/**
 * Slideshow Module Entry Point
 * 
 * @package    Joomla 1.5.0
 * @subpackage Modules
 * @license        GNU/GPL, see LICENSE.php
 * mod_tagcloud is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

//no direct access
defined('_JEXEC') or die('Direct Access to this location is not allowed.');
 
// include the helper file
require_once(dirname(__FILE__).DS.'helper.php');

$params->set('intro_only', 1);
$params->set('hide_author', 1);
$params->set('hide_createdate', 0);
$params->set('hide_modifydate', 1);

// Disable edit ability icon
$access = new stdClass();
$access->canEdit	= 0;
$access->canEditOwn = 0;
$access->canPublish = 0;

//params
$fx = $params->get( 'fx', 'fade' );
$timeout = $params->get( 'timeout' );
$slideSpeed = $params->get( 'slideSpeed' );
$tabSpeed = $params->get( 'tabSpeed' );

$number_items = $params->get( 'number_items' );


//append javascript AND css files files
$document =& JFactory::getDocument();	
$document->addCustomTag( '<script type="text/javascript">jQuery.noConflict();</script>' );
$document->addscript(JURI::root().'/modules/mod_remen_slideshow/js/jquery-1.2.3.pack.js');
$document->addscript(JURI::root().'/modules/mod_remen_slideshow/js/jcarousellite.js');
$document->addStyleSheet(JURI::root().'/modules/mod_remen_slideshow/css/style.css');

// get the items to display from the helper
$slideShow = ModRemenSlieshow::getSlideshow($params, $access);
if ($number_items==0)  $number_items = count($slideShow);
// include the template for display
require(JModuleHelper::getLayoutPath('mod_remen_slideshow'));

?>