<?php
/*------------------------------------------------------------------------
# JA Rutile for Joomla 1.5 - Version 1.0 - Licence Owner JA122250
# ------------------------------------------------------------------------
# Copyright (C) 2004-2008 J.O.O.M Solutions Co., Ltd. All Rights Reserved.
# @license - Copyrighted Commercial Software
# Author: J.O.O.M Solutions Co., Ltd
# Websites:  http://www.joomlart.com -  http://www.joomlancers.com
# This file may not be redistributed in whole or significant part.
-------------------------------------------------------------------------*/
		//get Itemid of category
		if ($catorsec) {
			$catlink   = JRoute::_(ContentHelperRoute::getCategoryRoute($rows[0]->catslug, $rows[0]->sectionid));
		}else{
			$catlink   = JRoute::_(ContentHelperRoute::getSectionRoute($rows[0]->sectionid));
		}

		$cattitle = ($catorsec) ? $rows[0]->cattitle:$rows[0]->sectitle;
		$catdesc = ($catorsec) ? $rows[0]->catdesc:$rows[0]->secdesc;
		
		$cls_sufix = trim($params->get('blog_theme',''));
		
		if($cls_sufix) $cls_sufix = '-'.$cls_sufix;
?>
<script type="text/javascript" language="javascript">
jQuery(document).ready(function() {
	jQuery(".carousel .jCarouselLite").jCarouselLite({
	    btnNext: ".carousel .next",
	    btnPrev: ".carousel .prev",
	    auto: 5000,
	    speed: 1000
	});
});
</script>
	<div id="jCarouselLiteWrap">
<div class="carousel clearfix">
    <div class="next"></div>

    <div class="jCarouselLite">
        <ul>

<?php
		$i = 0;
		while($i < $introitems && $i<count($rows)) {
			$row = $rows[$i];
			$link   = JRoute::_(ContentHelperRoute::getArticleRoute($row->slug, $row->catslug, $row->sectionid));
			$image = modJANewsHelper::replaceImage ($row, $img_align, $autoresize, $maxchars, $showimage, $img_w, $img_h, $hiddenClasses);
			//Show the latest news
			$link1= $link.'&Itemid=76';
?>
	
            	<li><div class="slide-prj-image">
                	<?php if ($showimage) : ?>
				<?php echo $image; ?>
				<?php endif; ?>
               <div class="clr"></div>
<a href="<?php echo $link1;?>" title="<?php echo strip_tags($row->title);?>"><?php echo $row->title;?></a>
                </div></li>




<?php
			$i++;
		}
		
?>
        </ul>
    </div>
    <div class="prev"></div>
    <div class="clear"></div>   
</div>
</div>
