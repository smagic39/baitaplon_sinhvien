<!-- BEGIN: main -->
<script type="text/javascript">
$(document).ready(function() {
	$(".username").focus(function() {
		$(".user-icon").css("left","-48px");
	});
	$(".username").blur(function() {
		$(".user-icon").css("left","0px");
	});
	
	$(".password").focus(function() {
		$(".pass-icon").css("left","-48px");
	});
	$(".password").blur(function() {
		$(".pass-icon").css("left","0px");
	});
});
</script>
<ul id="login-reg">
	<li class="reg">
	<a href="{USER_REGISTER}">{LANG.register}</a>
	</li>
	<li class="login">
	<a href="#" id="login"> {LANG.loginsubmit}</a>
	</li>
</ul>

<div id="popup">
<div class="user-icon"></div>
<div class="pass-icon"></div>
<form class="login-form" action="{USER_LOGIN}" method="post">
<span class="button b-close">X</span>
    <div class="header">
		<h1 class="cf">Đăng Nhập</h1>
		<span>Bạn Điền Đầy Đủ Thông Tin Dưới Đây.</span>
    </div>

    <div class="content">
		<input name="nv_login" type="text" class="input username" maxlength="{NICK_MAXLENGTH}" value="{LANG.username}" onfocus="this.value=''" />
		<input name="nv_password" type="password" class="input password" maxlength="{PASS_MAXLENGTH}" value="{LANG.password}" onfocus="this.value=''" />
    </div>

    <div class="footer">
		<input name="nv_redirect" value="{REDIRECT}" type="hidden" />
		<input type="submit" class="button" value="{LANG.loginsubmit}" />

		<a href="{USER_REGISTER}" class="register"> Đăng Ký </a>
    </div>


</form>
</div>
<!-- END: main -->

<!-- BEGIN: signed -->
<span style="font-size: 18px;"> {LANG.wellcome}: {USER.full_name}</span>&nbsp;&nbsp;&nbsp;&nbsp;
<a title="{LANG.edituser}" href="{CHANGE_INFO}">{LANG.edituser}</a>&nbsp;&nbsp;&nbsp;&nbsp;
<a title="{LANG.changpass}" href="{CHANGE_PASS}">{LANG.changpass}</a>
<!-- BEGIN: admin -->&nbsp;&nbsp;&nbsp;&nbsp;<a title="{LANG.logout}" href="{LOGOUT_ADMIN}">{LANG.logout}</a><!-- END: admin -->
    {in_group}
<!-- END: signed -->