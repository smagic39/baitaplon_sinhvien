<!-- BEGIN: main -->
	<link rel="stylesheet" type="text/css"	href="{NV_BASE_SITEURL}themes/{BLOCK_THEME}/css/navigation.css" />
        <script src="{NV_BASE_SITEURL}themes/{BLOCK_THEME}/js/ci.js" type="text/javascript"></script>
		<script>
            $(function(){
                $('#navigation').circleMenu(
				{
					direction:'full',
					step_in: 200,
					step_out: 200,
				});                                              
            });
        </script>
	<script type="text/javascript">
	$(document).ready(function(){
			var moving = false, offset = { x: 0, y: 0 };
			$(document).mouseup(function(e) {
				moving = false;
			});
			$("#navigation").mousedown(function(e) {
				e.preventDefault();
				moving = true;
				offset.y = (e.pageY - $("#navigation")[0].offsetTop);
				offset.x = (e.pageX - $("#navigation")[0].offsetLeft);
			});
			$(document).mousemove(function(e) {
				if(moving) {
					var o = {
						x: (e.pageX - offset.x) + 'px',
						y: (e.pageY - offset.y) + 'px'
						}
					$("#navigation")[0].style.left = o.x;
					$("#navigation")[0].style.top = o.y;
				}
			});
	});
</script>
	<ul class="navigation" id="navigation">
       <li class="home"><a title="{LANG.Home}" ></a></li>
       <!-- BEGIN: top_menu -->
		<li class="{TOP_MENU.class}">
		<a href="{TOP_MENU.link}" alt="{TOP_MENU.title}" title="{TOP_MENU.title}"></a>
		</li>
		<!-- END: top_menu -->
    </ul>
<!-- END: main -->
