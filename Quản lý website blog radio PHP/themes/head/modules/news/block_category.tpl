<!-- BEGIN: main -->
<script type="text/javascript">
$(document).ready(function() {
	$('.list-category a').animate({ paddingLeft:"0px"});
	$('.list-category a').hover(function() {
		$(this).stop().animate({ paddingLeft:"15px"});
	}, function() {
		$(this).stop().animate({ paddingLeft:"0px"});
	});
});
</script>
<ul class="list-category cf">
{HTML_CONTENT}
</ul>
<!-- END: main -->