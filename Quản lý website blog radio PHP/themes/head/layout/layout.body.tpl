<!-- BEGIN: main -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	{THEME_PAGE_TITLE}
    {THEME_META_TAGS}
<link rel="Stylesheet" href="{NV_BASE_SITEURL}themes/{TEMPLATE}/css/style.css" type="text/css" />
		{THEME_CSS}
        {THEME_SITE_RSS}
        {THEME_SITE_JS}
</head>
<body>
{MODULE_CONTENT}
</body>
</html>
<!-- END: main -->