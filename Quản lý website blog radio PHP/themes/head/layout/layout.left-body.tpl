<!-- BEGIN: main -->
{FILE "header.tpl"}	
<div id="wrapper">
	<!-- khoi ben trai -->
	<div id="left">
		<div class="top-left">
		[SLIDER]
		</div>
		<div id="sidebar">
			<a href="{THEME_SITE_HREF}" title="{THEME_LOGO_TITLE}">
			<img alt="{THEME_LOGO_TITLE}"  src="{NV_BASE_SITEURL}themes/{TEMPLATE}/images/like.png" width="200" height="110" />
			</a>
		<div class="left">
		[LEFT]
		</div>
		</div>
		<div id="fix-left">
			<div id="fix-height">
			[FIX-LEFT]
			</div>
		</div>
		<script type="text/javascript" charset="utf-8">
			// make social widgets fixed after scrolling to 50px beneath browser top
			jQuery(function () {

				var msie6 = jQuery.browser == 'msie' && jQuery.browser.version < 7;				
				if (!msie6) {
					var top = jQuery('#fix-left').offset().top;
					var height = jQuery('#sidebar').height();
					jQuery(window).scroll(function (event) {
						//var y = jQuery(this).scrollTop() - 400;
						var y = jQuery(this).scrollTop();
						if (y >= top) {
							jQuery('#fix-left').addClass('fix-left').slideDown('5000').find('#fix-height').css({
								'max-height':height,
								'overflow':'hidden'
							});
						} else {
							jQuery('#fix-left').hide();
						}
					});
				}
			});
        </script>  
	</div>	
	<!-- het khoi ben trai -->	
<div id="project">
	<div id="prohead"></div>
	<div id="probody">
	{MODULE_CONTENT}
	<div class="clear"> </div>
	</div>	
	<div id="profoot"></div>
</div>
</div>
{FILE "footer.tpl"}
<!-- END: main -->