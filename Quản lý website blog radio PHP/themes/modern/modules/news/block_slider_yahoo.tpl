<!-- BEGIN: main -->
<div id="lofslidecontent45" class="lof-slidecontent">
	<div class="preload"><div></div></div>
	  <div class="lof-main-outer">
		<ul class="lof-main-wapper">
			<!-- BEGIN: content -->
			<li>
				<div class="lof-main-item">
					<img src="{main.homeimgfile}" alt="{main.title}" title="{main.title}" height="300" width="900" />           
						 <div class="lof-main-item-desc">
							<h3><a target="_parent" title="{main.title}" href="{main.link}">{main.title}</a></h3>
							<p>{main.hometext}</p>
						</div>			
				</div> 
			</li> 
			<!-- END: content -->
		</ul>  	
	</div>
	<div class="lof-navigator-outer">
		<ul class="lof-navigator">
		<!-- BEGIN: nav -->
			<li>
				<div>
					<img src="{main.imgsource}" alt="" />
					<h3 style="font-weight: normal;">{main.title}</h3>
				</div>    
			</li>
		<!-- END: nav -->
		</ul>
	</div>
</div> 
<!-- END: main -->