<?
ob_start();
session_start();
header("Pragma: no-cache");
header("Expires: 0");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
define("edocom","edocom",true);
if(!isset($_SESSION['idadminpc'])) {
	 header('location:login.php');
}
include "../define_data.php";
include "../config.php";
include "sql.php";
include("../fckeditor/fckeditor.php") ;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 4.01 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>HE THONG QUAN TRI NOI DUNG WEBSITE</title>
		<link href="style.css"  rel="stylesheet" type="text/css">
		<script src="../style/functions.js" type="text/javascript"></script>
		<script src="../style/ajax.js" type="text/javascript"></script>
		<script src="../style/gop.js" type="text/javascript"></script>
		<script src="../style/js_admin.js" type="text/javascript"></script>
		<SCRIPT language=javascript src="functions.v2.js"></SCRIPT>
	</head>
	<body>

		<DIV id=docLoading  style="width:197px; height:39px; background-color:#FF9900; position: absolute;left:809px; top:6px; padding:5px; font-family:Tahoma; font-size:12px;">
			<IMG src="loading.gif" border=0 align="left" style="margin-right:10px" />
			<STRONG><FONT color="#fff">Đang tải dữ liệu....<BR></FONT><FONT color=#0000ff>Xin vui lòng chờ.</FONT></STRONG>
		</DIV>

		<div id="header-box">
			<div id="logoText" align="center"><a href="index.php">GFX.VN<br>Graphic Resources</a></div>
			<div class="clear"></div>	
			<div id="module-menu" class="autohide">
				<ul id="menu">
					<li class="node"><a class="top" style="padding-right:15px;" href="index.php?menu=config&site=config">Cấu hình hệ thống</a></li>
					<li class="node"><a class="top" style="padding-right:15px;" href="index.php?menu=MenuProduct&amp;site=MenuProduct">Quản lý danh mục </a></li>
					<li class="node"><a class="top" style="padding-right:15px;" href="index.php?menu=result&amp;site=result">Quản lý kết quả </a></li>
					<li class="node"><a class="top" style="padding-right:15px;" href="index.php?menu=dreams&amp;site=dreams">Quản lý sổ mơ </a></li>
					<li class="node"><a class="top" style="padding-right:15px;" href="index.php?menu=members&amp;site=members">Quản lý thành viên </a></li>
					<li class="node"><a class="top" style="padding-right:15px;" href="index.php?menu=guide&amp;site=guide">Hướng dẫn chơi game</a></li>
					<li class="node"><a class="top" style="padding-right:15px;" href="index.php?menu=sms&amp;site=sms">Dịch vụ SMS</a></li>
					<li class="node"><a class="top" style="padding-right:15px;" href="index.php?menu=xeng&amp;site=xeng">Nạp xeng </a></li>
					<li class="node"><a class="top" style="padding-right:15px;" href="index.php?menu=manager_adverting&site=manager_adverting">Quản lý quảng cáo</a></li>
					<li class="node"><a class="top" style="padding-right:15px;" href="index.php?menu=change_pass&site=change_pass">Đổi mật khẩu</a></li>
					<li class="node"><a class="top" style="padding-right:15px;" href="index.php?menu=ManageMode&site=ManageMode" class="style4">Phân quyền hệ thống</a></li>
					<li class="node"><a class="top" style="padding-right:15px;" href="index.php?menu=ManageCategory&site=ManageCategory" class="style4">Q.lý nhóm danh mục</a></li>
					<li class="node"><a class="top" style="padding-right:15px;" href="logout.php">Thoát đăng nhập</a></li>
				</ul>	
			</div>
			<div class="clr"></div>
		</div>
		<div id="content-wrap">
			<div id="border-top" >	
				Thành viên đăng nhập: <?=$_SESSION['fullname']?> (<? echo $_SESSION['mod']=1?'Quản trị hệ thống':'Người dùng'?>) - IP: <?=$_SERVER['REMOTE_ADDR']?><div class="clear"></div>
			</div>
			<div class="clear"></div>
			
			<div id="content-box">
				<div id="toolbar-box">
					<div class="m">
						<h3><?
							if($_GET['site'] != '')	 {
							if(stristr($_SESSION['nhomdanhmuc'],$_GET['site'])=='') {
								 header('location:index.php');
								}
							}
							  include "MenuCms.php";
							?>	
						</h3>
						<div class="clear"></div>
						<div class="toolbar" id="toolbar"></div>
					</div>
				</div>
				<div class="clear"></div>
				<div id="element-box">
					<div class="m">
						<?
						if(file_exists("./".$_GET['site'].".php"))	  {
						   include "./".$_GET['site'].".php";
						}
						?>
					</div>
					<div id="common_echo"></div>
				</div>
				<div class="clear"></div>
				<p class="copyright"> Template designed by HKDA Corporation<br></p>
			</div>
		</div>
		<SCRIPT language=javascript>
		<!--
		 setInterval("doInterval()", 7000);
		 show_hide_layer('docLoading', 0);
		 show_hide_layer('mainPage', 1);
		 clearInterval(intChk);
		//--> 
		</SCRIPT>
	</body>
</html>



