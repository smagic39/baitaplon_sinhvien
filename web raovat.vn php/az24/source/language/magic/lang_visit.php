<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
*		Vietnamese Translation by DCV Team - Discuz.vn 2/9/2012
 *      $Id: lang_visit.php 27449 2012-02-01 05:32:35Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

$lang = array
(
	'visit_name' => 'Thẻ ghé thăm',
	'visit_desc' => 'Truy cập ngẫu nhiên vào Blog/Chào hỏi/Trêu chọc',
	'visit_num' => 'Lượt ghé thăm',
	'visit_info' => 'Truy cập ngẫu nhiên (lên đến {num} lần)',
);

?>