<?php
function countryCityFromIP($ipAddr)
{
   //function to find country and city name from IP address
   //Developed by Roshan Bhattarai 
   //Visit http://roshanbh.com.np for this script and more.
  
  //verify the IP address for the  
  ip2long($ipAddr)== -1 || ip2long($ipAddr) === false ? trigger_error("Invalid IP", E_USER_ERROR) : "";
  // This notice MUST stay intact for legal use
  $ipDetail=array(); //initialize a blank array
  //get the XML result from hostip.info
  $xml = file_get_contents("http://api.hostip.info/?ip=".$ipAddr);
  /*echo $xml;
  echo "<pre>";print_r($xml);exit;*/
  //get the city name inside the node <gml:name> and </gml:name>
  preg_match("@<Hostip>(\s)*<gml:name>(.*?)</gml:name>@si",$xml,$match);
  //assing the city name to the array
  $ipDetail['city']=$match[2]; 
  //get the country name inside the node <countryName> and </countryName>
  preg_match("@<countryName>(.*?)</countryName>@si",$xml,$matches);
  //assign the country name to the $ipDetail array 
  $ipDetail['country']=$matches[1];
  //get the country name inside the node <countryName> and </countryName>
  preg_match("@<countryAbbrev>(.*?)</countryAbbrev>@si",$xml,$cc_match);
  $ipDetail['country_code']=$cc_match[1]; //assing the country code to array
  //return the array containing city, country and country code
  return $ipDetail;
} 
//search address ip 
function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}
$ip_add = '218.102.220.12';// getRealIpAddr();
echo $ip_add.'--'.$_SERVER['REMOTE_ADDR'];
$IPDetail=countryCityFromIP($ip_add); 
echo $IPDetail['country']; //country of that IP address 
echo $IPDetail['city']; //outputs the IP detail of the city
?>
