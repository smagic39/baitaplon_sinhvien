function delete_record(display_page, field_name, record_id)
{
	if(confirm("Sure to delete?"))
	{
		document.forms[0].action=display_page + "?" + field_name + "=" + record_id;
		document.forms[0].method="post";
		document.forms[0].submit();
		return;
	}
	else
	{
		return;
	}
}

function trim(str)
{
	var k=0;
	var i=0;
	var j=0;
	k=str.length;

 	while (str.charAt(j)==" ")
	{
   		j++;
	} 
	i=k;
 	while (str.charAt(i-1)==" ")
	{
   		i=i-1;
	}
   if (j!=k)
   		trimstr=str.substring(j,i);
   else
    	trimstr="";
   return trimstr;
}

function isDate(date, month, year, datefrm, monthfrm, yearfrm, errMsg)
{
	date=date+"";
	month=month+"";
	year=year+"";
	arrMonth=new Array();
	arrDay=new Array(31,28,31,30,31,30,31,31,30,31,30,31);

	if(isNaN(date) || date < 1)
	{
		alert(errMsg);
		datefrm.focus();
		return false;
	}  
	else if(isNaN(month) || month < 1 || month > 12) 
	{
		alert(errMsg)
		monthfrm.focus()
		return false
	}    
	else if (isNaN(year) || year==-1) 
	{
		alert(errMsg);
		yearfrm.focus();
		return false;
	}	
  	if ((year % 4) != 0 )
	{
		if (date > 28 && month == 2)
		{
			alert(errMsg);
			datefrm.focus();
			return false;
		}
	} 
	if ((year % 4) == 0 )
	{
		if ((year % 100) == 0 && (year % 400) != 0)
		{
			if(date > 28 && month == 2)
			{
				alert(errMsg);
            datefrm.focus();             
				return false;
			}
		}
		else if(date > 29 && month == 2)
		{
			alert(errMsg);
         datefrm.focus();
			return false;
		}
	}	
	if(arrDay[month - 1] < date)
	{
	alert(errMsg);
	datefrm.focus();
	return false;
	}
	return true;
}

function dateDiff(date, month, year, datefrm, monthfrm, yearfrm, date1, month1, year1, dateto, monthto, yearto, errMsg)
{
	if (parseInt(year1)<parseInt(year))
	{
		alert(errMsg);
		yearto.focus();
		return false;
	}
	else if((parseInt(month1)<parseInt(month)) && (parseInt(year)==parseInt(year1)))
	{
		alert(errMsg);
		monthto.focus();
		return false;
	}
	else if((parseInt(date1)<=parseInt(date)) && (parseInt(month1)==parseInt(month)) && (parseInt(year1)==parseInt(year)))
	{
		alert(errMsg);
		dateto.focus();
		return false;
	}
	return true;
}

function ifPickSelected(pickField, errMsg)
{
	if (pickField.options[pickField.selectedIndex].value == "-1")
	{
		alert(errMsg);
		pickField.focus();
		return false;
	}
	return true;
}

function isNumeric(textField, errMsg)
{
	if ((trim(textField.value).length==0) || isNaN(textField.value))
	{
		alert(errMsg);
		textField.focus();
		return false;
	}
	return true;
}

function isBlank(textField, errMsg)
{
	if ((trim(textField.value).length==0))
	{
		alert(errMsg);
		textField.focus();
		return false;
	}
	return true;
}

function isEmail(textField, errMsg)
{
	var str = trim(textField.value)
	var filter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if (filter.test(str))
	{
		return true;
	}
	else
	{
		alert(errMsg);
		textField.focus();
		return false;
	}
	return true;
}

function isLength(textField, errMsg)
{
	str=trim(textField.value)
	if (str.length>=5)
	{
		return true;
	}
	else
	{
		alert(errMsg);
		textField.focus();
		return false;
	}
	return true;
}

function isLengthCode(textField, errMsg)
{
	str=trim(textField.value)
	//Check if the length of the code is 42;true-->proceed
	if (str.length == 42)
	{
		return true;
	}
	else
	{
		alert(errMsg);
		textField.focus();
		return false;
	}
	return true;
}

function isAlpha(textField, errMsg)
{
	var i;
	var sText=textField.value;
	for(i=0;i<sText.length;i++)
		
	if("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ' -.".indexOf(sText.charAt(i))==-1)
	{
		alert(errMsg);
		textField.focus();
		return false;
	}
	return true;
}

function isAlphaNumeric(textField, errMsg)
{
	var i;
	sText=textField.value;
	for(i=0;i<sText.length;i++)
	
	if("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".indexOf(sText.charAt(i))==-1)
	{
		alert(errMsg);
		textField.focus();
		return false;
	}
	return true;
}

function isAlphaNumericUnder(textField, errMsg)
{
	var i;
	sText=textField.value;
	for(i=0;i<sText.length;i++)
	
	if("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_".indexOf(sText.charAt(i))==-1)
	{
		alert(errMsg);
		textField.focus();
		return false;
	}
	return true;
}

function isValidPhone(textField, errMsg)
{
	strValue = textField.value;
	var objRegExp  = /^\([1-9]\d{2}\)\d{3}\-\d{4}$/;

	if (objRegExp.test(strValue) == false)
	{
		alert(errMsg+"\nThe number should be in '(999)999-9999' format.");
		textField.focus();
		return false;
	}
	return true;
}

function isValidPostalCode(textField, errMsg)
{
	strValue = textField.value;
	var objRegExp  = /^\D{1}\d{1}\D{1}\d{1}\D{1}\d{1}$/;

	if (objRegExp.test(strValue) == false)
	{
		alert(errMsg);
		textField.focus();
		return false;
	}
	return true;
}

function openWin(url,winName,features)
{
	var win1 = window.open(url,winName,features);
	if ( in1 != null)
	{	  
		win1.focus();
	}
}

function formReset()
{
	document.form1.reset();
}

function isNegativeValue(textField, errMsg)
{
	if (parseInt(textField.value) < 0)
	{
		alert(errMsg);
		textField.focus();
		return false;
	}
	return true;
}

function isHexaDecimal(textField, errMsg)
{
	var i;
	sText=trim(textField.value);
	
	for(i=0;i<sText.length;i++)
		if("1234567890abcdef".indexOf(sText.charAt(i))==-1)
		{
			alert(errMsg);
			textField.focus();
			return false;
		}
	return true;
}

function isLengthNotes(textField, errMsg)
{
	str=trim(textField.value)
	if (str.length>50)
	{
		alert(errMsg);
		textField.focus();
		return false;
	}
	return true;
}