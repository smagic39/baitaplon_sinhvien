<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
class TableProfiles extends JTable
{
	/*var $id = null;
  var $mid = null;
  var $salutation = null;
  var $name = null;
  var $alias = null;
  var $pass = null;
  var $c_name = null;//5
  var $company = null;
  var $country = null;
  var $con_name = null;
  var $address1 = null;
  var $c_summary = null;
  var $email = null;
  var $phone = null;
  var $fax = null;
  var $mobile = null;
  var $web = null;//15
  var $image = null;
  var $published = null;
  var $credit = null;
  var $type_credit = null;
  var $desc_credit = null;//20
  var $startdate = null;
  var $enddate = null;
  var $show_name 	= null;
  var $show_phone 	= null;
  var $show_fax 	= null;//25
  var $show_email   = null;
  var $show_addr 	= null;
  var $ordering 	= null;*/
/*  var $checked_out = null;
  var $checked_out_time = null; */
	
	/*id 	salutation 	name 	pass 	c_name 	company 	country 	con_name 	address1 	c_summary 	email 	phone 	fax 	
	mobile 	web 	image 	published 	credit 	type_credit 	
	desc_credit 	startdate 	enddate 	show_name 	show_phone 	show_fax 	show_email 	show_addr 	checked_out 	checked_out_time */
  
  var $id = null;
  var $mid = null;
  var $salutation = null;
  var $name = null;
  var $alias = null;
  // var $pass = null;
   var $c_name = null;
  var $company = null;
   var $cyid = null;
  var $con_name = null;
  var $address1 = null;
  var $c_summary = null;
  var $properties = null;
  //var $email = null;
  var $phone = null;
  var $fax = null;
  var $mobile = null;
  var $web = null;
  var $credit = null;
  var $type_credit = null;
  var $desc_credit = null;//20
  var $startdate = null;
  var $enddate = null;
  var $show_name 	= null;
  var $show_phone 	= null;
  var $show_fax 	= null;//25
  var $show_email   = null;
  var $show_addr 	= null;
  var $show_packet = null;
  var $end_datebuy = null;
  var $image = null;
  var $logo_image = null;
  var $logo_image_large = null;
  var $published = null;
  var $ordering = null;
  var $checked_out = null;
  var $checked_out_time = null; 
  
   function __construct(&$db)
  {
    parent::__construct( '#__properties_profiles', 'id', $db );
  }
  
	function check()
	{
		// check for http on webpage		
		if(empty($this->alias)) {
			$this->alias = $this->name;
		}
		$this->alias = JFilterOutput::stringURLSafe($this->alias);
		if(trim(str_replace('-','',$this->alias)) == '') {
			$datenow =& JFactory::getDate();
			$this->alias = $datenow->toFormat("%Y-%m-%d-%H-%M-%S");
		}
		return true;
	}
}
?>