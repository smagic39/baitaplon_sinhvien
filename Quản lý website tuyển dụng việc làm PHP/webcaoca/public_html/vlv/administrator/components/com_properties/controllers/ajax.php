<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.controller');

class PropertiesControllerAjax extends JController
{ 
    function display()
    {
        parent::display();
    }
	function days_between($start, $end) { 
	    $diffInSeconds = abs($end - $start); 
	    $diffInDays = ceil($diffInSeconds / 86400);//86400 =  (24*60*60)
	    return $diffInDays; 
	}
	function checkdate(){
		global $mainframe;
		$db 	= JFactory::getDBO();
		$qry 	= "select id,name,jp,p1,p2,p3,startdate,enddate,date1,date2,date3 from #__properties_products where published = 1 order by id desc";
		$db->setQuery($qry);
		$res 	= $db->loadObjectList();
		
		$check_date = date('Y-m-d');//echo $check_date; 
		$check_date = explode('-',$check_date); 
		$check_date = mktime(0, 0, 0, $check_date[1], $check_date[2], $check_date[0]);
		
		foreach ($res as $re) {
			//mktime(hour,minute,second,month,day,year,is_dst)
			$start = $re->startdate;
			$start = explode('-',$start); 
			$start = mktime(0, 0, 0, $start[1], $start[2], $start[0]); 
			//$check_date = $this->days_between($start,$check_date);
			
			$end = $re->enddate;
			$end = explode('-',$end); 
			$end = mktime(0, 0, 0, $end[1], $end[2], $end[0]);
			//$pub_date = $this->days_between($end,$check_date);
			if ($check_date - $end >=0){
				$pub 	= "update #__properties_products set published=0 where id = ".$re->id;
				$db->setQuery($pub);
				$db->query();
			}else{
				$date1 = $re->date1;
				$date1 = explode('-',$date1); 
				$date1 = mktime(0, 0, 0, $date1[1], $date1[2], $date1[0]); 
				if ($check_date - $date1 >= 0){
					$qry1 	= "update #__properties_products set p1=0 where id = ".$re->id;
					$db->setQuery($qry1);
					$db->query();
				}
				$date2 = $re->date2;
				$date2 = explode('-',$date2); 
				$date2 = mktime(0, 0, 0, $date2[1], $date2[2], $date2[0]); 
				if ($check_date - $date2 >= 0){
					$qry2 	= "update #__properties_products set p2=0 where id = ".$re->id;
					$db->setQuery($qry2);
					$db->query();
				}
				$date3 = $re->date3;
				$date3 = explode('-',$date3); 
				$date3 = mktime(0, 0, 0, $date3[1], $date3[2], $date3[0]); 
				if ($check_date - $date3 >= 0){
					$qry3 	= "update #__properties_products set p3=0 where id = ".$re->id;
					$db->setQuery($qry3);
					$db->query();
				}
			}
		}
		echo JText::_('Success !');
		//$mainframe->redirect('index.php?option=com_properties&view=products');exit;
	}
    //reset goi cv
    function Resets() {
    	$db 		= JFactory::getDBO();
		$mid		= JRequest::getVar('mid');
		//update trong bang #__properties_profiles (1:kich hoat--2:huy--3:trang thai cho duyet)
		$qry 		= "update #__properties_profiles set idgoi_cv='',goicv ='',goi_cv='',xem_cv = '',show_packet ='' where mid = $mid";
		$db->setQuery($qry);
		$db->query();
		//update trong bang #__properties_buypacket
		$qry_buy	= "update #__properties_buypacket set checkbuy ='2' where agent_id  = $mid";
		$db->setQuery($qry_buy);
		$db->query();
		$html = '';
		$html .= '
			<tbody>
			<tr>
				<td class="key">
					<label for="name">
						Buy Packet:
					</label>
				</td>
				<td>
				<input name="end_datebuy" id="end_datebuy" value="2010-11-25" type="text"><img class="calendar" src="/website/td-1711/templates/system/images/calendar.png" alt="calendar" id="end_datebuy_img">					</td>
			</tr>
			<tr>
				<td class="key">
					<label for="name">
						Show Packet:
					</label>
				</td>
				<td>
					<img alt="Đang chờ xét duyệt" src="images/publish_x.png" border="0">
					<input value="0" id="show_packet_n" name="show_packet" checked="checked" type="radio">
					<label for="show_packet_n">Không</label>
					<input value="1" id="show_packet_y" name="show_packet" type="radio">
					<label for="show_packet_y">Yes</label>
					<span id="resets_cv" style="float: right; border: 1px solid rgb(204, 204, 204); margin: 3px; padding: 2px; color: rgb(220, 89, 89); cursor: pointer;">'.JText::_('Success !').'</span>
				</td>
			</tr>
			</tbody>
		';
		echo $html;
    }
//ket qua search-left
function resultsearch(){
	$db = JFactory::getDBO();
	$cs_search		= JRequest::getVar('cs_search');
	$cid_search		= JRequest::getVar('cid_search');
	$type_search	= JRequest::getVar('type_search');
	$eid			= JRequest::getVar('eid');
	$pid			= JRequest::getVar('pid');
	$work_experience	= JRequest::getVar('work_experience');
	$price_currency		= JRequest::getVar('price_currency');
	//echo $cs_search.'-'.$cid_search.'-'.$type_search.'-'.$eid.'-'.$pid.'-'.$work_experience.'-'.$price_currency;
	$where = array();
	$cs_search		= JRequest::getVar('cs_search', 0, '', 'int');
	if(isset($cs_search)) {
		if ($cs_search)
			$where[] 	=  	'p.sid='.$cs_search;
	}
	$cid_search		= JRequest::getVar('cid_search', 0, '', 'int');
	if(isset($cid_search)) {
		if ($cid_search)
			$where[] 	=	'p.cid='.$cid_search;
	}
	$type_search	= JRequest::getVar('type_search', 0, '', 'int');
	if(isset($type_search)) {
		if($type_search)
			$where[] 	= 'p.type='.$type_search;
	}
	$eid			= JRequest::getVar('eid', 0, '', 'int');
	if(isset($eid)) {
			if ($eid)
				$where[] 	= 'p.eid='.$eid;
		}
		$pid			= JRequest::getVar('pid', 0, '', 'int');
	if(isset($pid)) {
			if ($pid)
				$where[] 	= 'p.pid='.$pid;
		}
		$price_currency		= JRequest::getVar('price_currency', 0, '', 'int');
	if(isset($price_currency)) {
		switch ($price_currency){
			case 0:
				//$where[] 	= '';
			break;
			case 1://thoa thuan
				$where[] 	= 'p.i_curren= 0';
			break;
			case 2://1 triệu - 3 triệu
				$where[] 	= 'p.i_curren >= 1';
				$where[] 	= 'p.i_curren < 3';
			break;
			case 3://3 triệu - 5 triệu
				$where[] 	= 'p.i_curren >= 3';
				$where[] 	= 'p.i_curren < 5';
			break;
			case 4://5 triệu - 10 triệu
				$where[] 	= 'p.i_curren >= 5';
				$where[] 	= 'p.i_curren < 10';
			break;
			case 5://trên 10 triệu
				$where[] 	= 'p.i_curren >= 10';
			break;
		}
	}
		$work_experience	= JRequest::getVar('work_experience', 0, '', 'int');
	if(isset($work_experience)) {
		switch ($price_currency){
			case 0:
				//$where[] 	= '';
			break;
			case 1://Dưới 1 năm
				$where[] 	= 'p.jobyear < 1';
			break;
			case 2://1 năm
				$where[] 	= 'p.jobyear >= 1';
				$where[] 	= 'p.jobyear < 2';
			break;
			case 3://2 năm
				$where[] 	= 'p.jobyear >= 2';
				$where[] 	= 'p.jobyear < 3';
			break;
			case 4://3 năm
				$where[] 	= 'p.jobyear >= 3';
				$where[] 	= 'p.jobyear < 4';
			break;
			case 5://4 năm
				$where[] 	= 'p.jobyear >= 4';
				$where[] 	= 'p.jobyear < 5';
			break;
			case 6://5 năm
				$where[] 	= 'p.jobyear >= 5';
			break;
		}
	}
	$where 			= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
	//$qry = "SELECT COUNT(*) FROM `#__timesheet_userlevel` $where";
	$qry = "SELECT count(*)
			FROM #__properties_products as p
			$where ";		
	$db->setQuery($qry);
	$total = $db->loadResult();
	echo $total;
}
function ChangePacket(){
	//echo "thanhtd"; list_type
	$db 		=& JFactory::getDBO();
	$cont = JRequest::getVar('cont');
	$pack_id 	= JRequest::getVar('pack_id');
	$at 		= JRequest::getVar('at');
	/*echo '<script type="text/javascript">
		var val = document.getElementById(\'hid_jp\');
		alert("val.value");
		val.value = "";
	</script>';*/
	//echo $pack_id.'-';
	$query 		= 	"SELECT * from #__properties_packet where published = 1 and id = ".$pack_id;
	$db->setQuery( $query );				
	$resPack = $db->loadObject();
	$list_types = explode(',',$resPack->list_type);
	foreach ($list_types as $list_type) {
		if ($list_type !='')
			$arr .= $list_type.',';
	}
	$arr 	= substr($arr,0,-1);
	//echo $arr;
	$qry = "SELECT * from #__properties_typepacket where published = 1 and id IN ($arr)"; 
	$db->setQuery( $qry );				
	$res = $db->loadObjectList();
	//echo "<pre>";print_r($res);exit($qry);
	$html = '';
	$html .= '<div id="j_pack">';
	$i = 0;
	
	foreach ($res as $re) {
		if ($cont == $pack_id)
			$cheked = (in_array($re->id,explode(',',$at)))?'checked="checked"':'';
		$html .= $re->name.'<input '.$cheked.' class="j_p" type="checkbox" id="'.$re->id.'" name="jp'.$i.'" value="'.$re->id.'" onclick="check_jp(this.id)" >';
		$i++;
	}
	$html .= '</div>';
	echo $html;
}

function ChangeState() {
	global $mainframe;
	$datos = null;
	$db 	=& JFactory::getDBO();
	$Country_id = JRequest::getVar('Country_id');
	
	$query = 	"SELECT * from #__properties_state where published = 1 and parent = ".$Country_id;
	$db->setQuery( $query );				
	$provincias = $db->loadObjectList();
	$nP = count($provincias);
	$mitems[0]->id=0;
	$mitems[0]->name='State';
			foreach ( $provincias as $item ) {
				$mitems[] = $item;
			}
	$javascript = 'onChange="ChangeLocality(this.value)"';
	$Comboprovincias        = JHTML::_('select.genericlist',   $mitems, 'sid', 'class="inputbox" size="1" style="font-size: 10px; width: 108px;"'.$javascript,'id', 'name',  0); 
	echo $Comboprovincias;

}


function ChangeLocality() {
	global $mainframe;
	$datos = null;
	$db 	=& JFactory::getDBO();
	$State_id = JRequest::getVar('State_id');
	$query = 	"SELECT * from #__properties_locality where published = 1 and parent = ".$State_id;
	$db->setQuery( $query );				
	$ciudades = $db->loadObjectList();
	$nP = count($ciudades);
	$mitems[0]->id=0;
	$mitems[0]->name='Locality';
			foreach ( $ciudades as $item ) {
				$mitems[] = $item;
			}
	$javascript = '';
	$Combociudades        = JHTML::_('select.genericlist',   $mitems, 'lid', 'class="inputbox" size="1" style="font-size: 10px; width: 108px;"'.$javascript,'id', 'name',  0); 
	echo $Combociudades;

}

function ChangeAgent()
{	
	global $mainframe;
	$datos = null;
	$db 	=& JFactory::getDBO();
	$agent_id = JRequest::getVar('agent_id');
	
		$query = 	"SELECT * from #__properties_profiles WHERE mid = ".$agent_id;
	$db->setQuery( $query );			
	$agent = $db->loadObjectList();
	
	
	echo '<input class="text_area" type="text" name="agent" id="agent" size="20" maxlength="255" value="'.$agent[0]->name.'" /><br>';
	echo 'Id: '.$agent[0]->id.' UsrId: '.$agent[0]->mid;
//}

	
	}
	
	
	
function ChangeType() {

	global $mainframe;
	$datos = null;
	$db 	=& JFactory::getDBO();
	$Category_id = JRequest::getVar('Category_id');
	//$search = JRequest::getVar('search');
	$query = 	"SELECT * from #__properties_type where published = 1 and parent = ".$Category_id." OR parent = 0";
	$db->setQuery( $query );				
	$types = $db->loadObjectList();
	$nP = count($types);
	$mitems[0]->id=0;
	$mitems[0]->name='Type';
		foreach ( $types as $item ) {
			$mitems[] = $item;
		}
	//$javascript = 'onChange="ChangeTypeSearch(this.value)"';//edit thanhtd
	$Combotypes        = JHTML::_('select.genericlist',   $mitems, 'type', 'class="inputbox" size="1" style="font-size: 10px; width: 108px;"'.$javascript,'id', 'name',  0); 
	echo $Combotypes;

}
//edit search
function ChangeType_Search() {

	global $mainframe;
	$datos = null;
	$db 	=& JFactory::getDBO();
	$Category_id = JRequest::getVar('Category_id');
	//$search = JRequest::getVar('search');
	$query = 	"SELECT * from #__properties_type where published = 1 and parent = ".$Category_id." OR parent = 0";
	$db->setQuery( $query );				
	$types = $db->loadObjectList();
	$nP = count($types);
	$mitems[0]->id=0;
	$mitems[0]->name='Chọn kiểu ngành nghề';
		foreach ( $types as $item ) {
			$mitems[] = $item;
		}
	$javascript = 'onChange="Categories_Search(this.value)"';//edit thanhtd
	$Combotypes        = JHTML::_('select.genericlist',   $mitems, 'type_search', 'class="inputbox" size="1" style="font-size: 10px;"'.$javascript,'id', 'name',  0); 
	echo $Combotypes;

}

function ChangeShowRatesList() {

	global $mainframe;
	$ratelistid=JRequest::getVar('ratelistid');
	echo '<div id="progressS"></div>';
	echo ''.$ratelistid.'<br>';
	jimport('joomla.filesystem.file');
	$rates_file = JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_properties'.DS. 'rates_list' . DS . $ratelistid ;
	if(JFile::exists($rates_file)){



	$gestor = fopen($rates_file, "r");
	if($gestor)
		{
		$x=0;
		echo '<table>';
	while (!feof($gestor)) {
	$bufer = fgets($gestor, 4096);	
	$result=explode(';',$bufer);
	$from=$result[1];
	$to=$result[2];	
	$from_spanish=explode('/',$from);
	$to_spanish=explode('/',$to);	
	
	//echo '<b>'.$from_spanish[2].'-'.$from_spanish[1].'-'.$from_spanish[0].'</b><br>';
	//echo '<b>'.$to_spanish[2].'-'.$to_spanish[1].'-'.$to_spanish[0].'</b><br>';
	
	$saverate['title']=$result[0];
	$saverate['validfrom']=$from_spanish[2].'-'.$from_spanish[1].'-'.$from_spanish[0];
	$saverate['validto']=$to_spanish[2].'-'.$to_spanish[1].'-'.$to_spanish[0];
	$saverate['rateperweek']=$result[3];
	$saverate['weekonly']=1;
	$saverate['published']=1;
	$saverate['ordering']=$x;
	$x++;
	
	echo '<tr>';
	echo '<td style="border: 1px solid #CCCCCC;">'.$saverate['title'].'</td>    <td style="border: 1px solid #CCCCCC;">'.$from.'</td>    <td style="border: 1px solid #CCCCCC;">'.$to.'</td>    <td style="border: 1px solid #CCCCCC;">'.$saverate['rateperweek'].'</td>';
	echo '</tr>';
	
	}	
	echo '</table>';
	fclose ($gestor);	
		}
	
	
}else{
echo 'file not exists.';
}	
	
	
	
}


}
?>