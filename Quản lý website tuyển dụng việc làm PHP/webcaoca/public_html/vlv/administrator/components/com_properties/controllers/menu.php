<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

class PropertiesControllerMenu extends PropertiesController
{	 
	function __construct()
	{
		parent::__construct();		
		$this->registerTask( 'add'  , 	'edit' );
		$this->registerTask('save2new',		'save');
		$this->registerTask( 'apply',	'save' );
		$this->registerTask( 'unpublish', 	'publish');			

$this->cid 		= JRequest::getVar( 'cid', array(0), '', 'array' );
JArrayHelper::toInteger($this->cid, array(0));		
	
	}	

	


	function save()
	{
	global $mainframe, $option;	
	$model = $this->getModel('menu');
	$Categories=$this->getCategories();	

	$component = JComponentHelper::getComponent( $option );
	$compid=$component->id;
//echo $compid;
	$db 	=& JFactory::getDBO();

	$q="SELECT id FROM 	#__menu_types WHERE menutype = 'propertieshidden'";
	$db->setQuery( $q );
	$is_menu_types = $db->loadResult();
	if ($is_menu_types)
		{
		//$msg="menu type propertieshidden exists!" ;
		$this->Menu_Types_Id=$is_menu_types;
		}else{		
		$q1="INSERT IGNORE INTO #__menu_types VALUES ('', 'propertieshidden', 'propertieshidden', 'propertieshidden');";
		$db->setQuery( $q1 );
		if (!$db->query())
		{
			JError::raiseError(500, $db->getErrorMsg() );
		}
	}				
		

$q="SELECT id FROM 	#__menu WHERE menutype = 'propertieshidden' AND componentid = ".$compid;
$db->setQuery( $q );
$id_menu_types = $db->loadResultArray();
//echo $q;


JArrayHelper::toInteger($id_menu_types, array(0));
            $where = ' WHERE (id = ' . implode( ' OR id = ', $id_menu_types ) . ') AND home = 0';
           /* $query = 'UPDATE #__menu' .
                    ' SET published = -2 '.
                    $where;*/
					
			$query = 'DELETE FROM #__menu'.$where;					
					
            $db->setQuery( $query );
            if (!$db->query()) {
                $this->setError( $db->getErrorMsg() );
               // echo 'error';
            }
	
$menu['menutype']='propertieshidden';			
$menu['name']='Categories and Types';
$menu['link']='index.php?option=com_properties&view=properties';	
$menu['type']='component';
$menu['published']='1';
$menu['parent']=0;
$menu['componentid']=$compid;
$menu['ordering']=0;
$menu['home']='0';	
	
	$model->store($menu);
$LastModif = $model->getLastModif();	
$Categories_and_Types_MenuId=$LastModif;
	//echo '<br>LastModif:'.$LastModif;
	
$ord=0;
$TopParent=$LastModif;
foreach($Categories as $cat)
{
$ord++;

if($cat->parent=='0')
	{
	$parent=$TopParent;
	$parent_next_menu=NULL;
	}else{	
	
	if($parent_next_menu)
		{
		$parent =$parent_next_menu;
		}else{
		$parent = $model->getLastModif();
		$parent_next_menu=$model->getLastModif();
		}
	}

$menu['id']='';
$menu['menutype']='propertieshidden';			
$menu['name']=$cat->name;
$menu['link']='index.php?option=com_properties&view=properties&cid='.$cat->id;	
$menu['type']='component';
$menu['published']='1';
$menu['parent']=$parent;
$menu['componentid']=$compid;
$menu['ordering']=$ord;
$menu['home']='0';
$menu['access']=$cat->access;	
				
		/*		
		if ($model->store($menu)) {				
			$cat->MenuId=$model->getLastModif();
			$this->CategoryTypeMenu($cat,$compid);							
		} else {
			$msg = JText::_( 'Error Saving Greeting' );
			$msg .=  'err'.$this->Err;
		}	
		*/
}


$this->CountryMenu($compid);

$this->ProductsMenu($compid);

$link = 'index.php?option='.$option.'&view=menu';
		$this->setRedirect($link, $msg);
	}



	function CategoryTypeMenu($cat,$compid)
	{
	
global $mainframe,$option;
$model = $this->getModel('menu');	
$db =& JFactory::getDBO();

$query = ' SELECT * from #__properties_type WHERE parent = '.$cat->id.'';

$db->setQuery( $query );
		$mitems = $db->loadObjectList();		
		
		$ord++;
		foreach($mitems as $v)
			{
			$menu['id']='';
$menu['menutype']='propertieshidden';			
$menu['name']=$v->name;
$menu['link']='index.php?option=com_properties&view=properties&cid='.$v->parent.'&tid='.$v->id;	
$menu['type']='component';
$menu['published']='1';
$menu['parent']=$cat->MenuId;
$menu['componentid']=$compid;
$menu['ordering']=$ord;
$menu['home']='0';
$menu['access']=$cat->access;	

//echo '<pre>';print_r($menu);echo '</pre>';

if ($model->store($menu)) {	$this->reseult[]=$menu; }
			
			}
			
	return $model->getLastModif();
}











	function ProductsMenu($compid)
	{				
global $mainframe,$option;
$model = $this->getModel('menu');	
$db =& JFactory::getDBO();

$query = ' SELECT p.name as name_property ,c.name as name_category,t.name as name_type,cy.name as name_country,s.name as name_state,l.name as name_locality,p.id as pid, c.id as cid,t.id as tid,cy.id as cyid,s.id as sid,l.id as lid, '
		. ' CASE WHEN CHAR_LENGTH(p.alias) THEN CONCAT_WS(":", p.id, p.alias) ELSE p.id END as Pslug,'
		. ' CASE WHEN CHAR_LENGTH(c.alias) THEN CONCAT_WS(":", c.id, c.alias) ELSE c.id END as Cslug,'
		. ' CASE WHEN CHAR_LENGTH(cy.alias) THEN CONCAT_WS(":", cy.id, cy.alias) ELSE cy.id END as CYslug,'
		. ' CASE WHEN CHAR_LENGTH(s.alias) THEN CONCAT_WS(":", s.id, s.alias) ELSE s.id END as Sslug,'		
		. ' CASE WHEN CHAR_LENGTH(l.alias) THEN CONCAT_WS(":", l.id, l.alias) ELSE l.id END as Lslug, '	
		. ' CASE WHEN CHAR_LENGTH(t.alias) THEN CONCAT_WS(":", t.id, t.alias) ELSE t.id END as Tslug '			
				. ' FROM #__properties_products AS p '				
				. ' LEFT JOIN #__properties_country AS cy ON cy.id = p.cyid '				
				. ' LEFT JOIN #__properties_state AS s ON s.id = p.sid '
				. ' LEFT JOIN #__properties_locality AS l ON l.id = p.lid '					
				. ' LEFT JOIN #__properties_category AS c ON c.id = p.cid '
				. ' LEFT JOIN #__properties_type AS t ON t.id = p.type '
				. ' WHERE p.published = 1 '
				. ' ORDER BY p.ordering';				

		$db->setQuery( $query );
		$mitems = $db->loadObjectList();
		
		$ord=0;
		
foreach ( $mitems as $v )
			{
			
			
		
$LocalityMenuId=$this->getMenuId($v,'locality');

if(!$CategoryMenuId=$this->getMenuId($v,'category'))
	{
	$CategoryMenuId=$this->CategoryMenu($v->cid,$compid,$LocalityMenuId);	
	}


if(!$TypeMenuId=$this->getMenuId($v,'type'))
{
$TypeMenuId=$this->TypeMenu($v->cid,$v->lid,$compid,$CategoryMenuId);
}




echo $LocalityMenuId;
echo '<br>'.$CategoryMenuId;
echo '<br>'.$TypeMenuId;
//require('s');


			$ord++;
			$menu['id']='';
$menu['menutype']='propertieshidden';			
$menu['name']=$v->name_property;
$Plink='index.php?option=com_properties&view=properties&task=showproperty';
$Plink.='&cyid='.$v->cyid.'';
$Plink.='&sid='.$v->sid.'';
$Plink.='&lid='.$v->lid.'';
$Plink.='&cid='.$v->cid.'';
$Plink.='&tid='.$v->tid.'';
$Plink.='&id='.$v->id.'';

$menu['link']=$Plink;	
$menu['type']='component';
$menu['published']='1';
$menu['parent']=$TypeMenuId;
$menu['componentid']=$compid;
$menu['ordering']=$ord;
$menu['home']='0';
$menu['access']=$cat->access;	


if ($model->store($menu)) {	$this->reseult[]=$menu; }

			}	
	
//require('a');	
	}



	function getMenuId($v,$ItemFind)
	{
	echo '<pre>';print_r($v);echo '<pre>';	
	
	
	$db 	=& JFactory::getDBO();			


	
	$match = null;
		
		if($ItemFind=='locality')
			{
			$q="SELECT id FROM 	#__menu WHERE menutype = 'propertieshidden'".
			" AND link = 'index.php?option=com_properties&view=properties&cyid=".$v->cyid."&sid=".$v->sid."&lid=".$v->lid."'";
			}
		
		if($ItemFind=='category')
			{
			$q="SELECT id FROM 	#__menu WHERE menutype = 'propertieshidden'".
			" AND link = 'index.php?option=com_properties&view=properties&cid=".$v->cid."'";
			}
		
		if($ItemFind=='type')
			{
			$q="SELECT id FROM 	#__menu WHERE menutype = 'propertieshidden'".
			" AND link = 'index.php?option=com_properties&view=properties&tid=".$v->tid."'";
			}
				
$db->setQuery( $q );
$id_menu = $db->loadResult();			
	
//echo '<b>'.$id_menu.'</b>';
	
return $id_menu;
	
		}

function getCategories()
		{
		
		global $mainframe,$option;
		$filter_category		= $mainframe->getUserStateFromRequest( "$option.filter_category",		'filter_category',		'',		'int' );
		$db =& JFactory::getDBO();		
		
		if (!$row->parent) {
			$row->parent = 0;
		}
		$query = 'SELECT * ' .
				' FROM #__properties_category ' .				
				' WHERE published != -2' .				
				' ORDER BY parent, ordering';
		$db->setQuery( $query );
		$mitems = $db->loadObjectList();
		
		$children = array();

		if ( $mitems )
		{			
			foreach ( $mitems as $v )
			{
				$pt 	= $v->parent;
				$list 	= @$children[$pt] ? $children[$pt] : array();
				array_push( $list, $v );
				$children[$pt] = $list;
			}
		}
		$list = JHTML::_('menu.treerecurse', 0, '', array(), $children, 9999, 0, 0 );
		

		return $list;
		
		}
		
		
		
		
		
		function RegionsMenu()
	{
	global $mainframe, $option;	
	$model = $this->getModel('menu');
	
	$component = JComponentHelper::getComponent( $option );
	$compid=$component->id;
	
	
	
	
	
	
	
	
	$db 	=& JFactory::getDBO();

	$q="SELECT id FROM 	#__menu_types WHERE menutype = 'propertieshidden'";
	$db->setQuery( $q );
	$is_menu_types = $db->loadResult();
	
	if ($is_menu_types)
		{
		//$msg="menu type propertieshidden exists!" ;
		$this->Menu_Types_Id=$is_menu_types;
		}else{		
		$q1="INSERT IGNORE INTO #__menu_types VALUES ('', 'propertieshidden', 'propertieshidden', 'propertieshidden');";
		$db->setQuery( $q1 );
		if (!$db->query())
		{
			JError::raiseError(500, $db->getErrorMsg() );
		}
	}				
		

$q="SELECT id FROM 	#__menu WHERE menutype = 'propertieshidden' AND componentid = ".$compid;
$db->setQuery( $q );
$id_menu_types = $db->loadResultArray();



JArrayHelper::toInteger($id_menu_types, array(0));
            $where = ' WHERE (id = ' . implode( ' OR id = ', $id_menu_types ) . ') AND home = 0';
           /* $query = 'UPDATE #__menu' .
                    ' SET published = -2 '.
                    $where;*/
					
			$query = 'DELETE FROM #__menu'.$where;					
					
            $db->setQuery( $query );
            if (!$db->query()) {
                $this->setError( $db->getErrorMsg() );
               // echo 'error';
            }
			
			
			
$menu['menutype']='propertieshidden';			
$menu['name']='Categories and Types';
$menu['link']='index.php?option=com_properties&view=properties';	
$menu['type']='component';
$menu['published']='1';
$menu['parent']=0;
$menu['componentid']=$compid;
$menu['ordering']=0;
$menu['home']='0';	


$model->store($menu);
$LastModif = $model->getLastModif();	
$Categories_and_Types_MenuId=$LastModif;

$this->CountryMenu($compid);

}	
		
		
		
		
		
		
		
		
		
		
		
	function CountryMenu($compid)
	{
		
global $mainframe,$option;
$model = $this->getModel('menu');	
$db =& JFactory::getDBO();

$query = ' SELECT * from #__properties_country ORDER BY ordering';

$db->setQuery( $query );
		$mitems = $db->loadObjectList();
		
$menu['menutype']='propertieshidden';			
$menu['name']='Countries States and Localities';
$menu['link']='index.php?option=com_properties&view=properties';	
$menu['type']='component';
$menu['published']='1';
$menu['parent']=0;
$menu['componentid']=$compid;
$menu['ordering']=0;
$menu['home']='0';		
	
	$model->store($menu);
$PropertiesMenu = $model->getLastModif();
		
$ord=0;		
foreach ( $mitems as $v )
	{
			$ord++;
			$menu['id']='';
$menu['menutype']='propertieshidden';			
$menu['name']=$v->name;
$menu['link']='index.php?option=com_properties&view=properties&cyid='.$v->id;	
$menu['type']='component';
$menu['published']='1';
$menu['parent']=$PropertiesMenu;
$menu['componentid']=$compid;
$menu['ordering']=$ord;
$menu['home']='0';
$menu['access']=$cat->access;	

		if ($model->store($menu)) 
			{	
				$ParentState = $model->getLastModif();
				$this->StatesMenu($v->id,$ParentState,$compid);	 
			}
	}
}


	function StatesMenu($CountryId,$ParentState,$compid)
	{
		
global $mainframe,$option;
$model = $this->getModel('menu');	
$db =& JFactory::getDBO();

$query = ' SELECT * from #__properties_state WHERE parent = '.$CountryId.' ORDER BY ordering';

$db->setQuery( $query );
		$mitems = $db->loadObjectList();
		
$ord=0;		
foreach ( $mitems as $v )
	{
			$ord++;
			$menu['id']='';
$menu['menutype']='propertieshidden';			
$menu['name']=$v->name;
$menu['link']='index.php?option=com_properties&view=properties&cyid='.$CountryId.'&sid='.$v->id;	
$menu['type']='component';
$menu['published']='1';
$menu['parent']=$ParentState;
$menu['componentid']=$compid;
$menu['ordering']=$ord;
$menu['home']='0';
$menu['access']=$cat->access;	

		if ($model->store($menu)) 
			{	
				$ParentLocality = $model->getLastModif();
				$this->LocalityMenu($CountryId,$v->id,$ParentLocality,$compid);	 
			}
	}
}


	function LocalityMenu($CountryId,$StateId,$ParentLocality,$compid)
	{
		
global $mainframe,$option;
$model = $this->getModel('menu');	
$db =& JFactory::getDBO();

$query = ' SELECT * from #__properties_locality WHERE parent = '.$StateId.' ORDER BY ordering';

$db->setQuery( $query );
		$mitems = $db->loadObjectList();
		
$ord=0;		
foreach ( $mitems as $v )
	{
			$ord++;
			$menu['id']='';
$menu['menutype']='propertieshidden';			
$menu['name']=$v->name;
$menu['link']='index.php?option=com_properties&view=properties&cyid='.$CountryId.'&sid='.$StateId.'&lid='.$v->id;	
$menu['type']='component';
$menu['published']='1';
$menu['parent']=$ParentLocality;
$menu['componentid']=$compid;
$menu['ordering']=$ord;
$menu['home']='0';
$menu['access']=$cat->access;	

		if ($model->store($menu)) 
			{	
				$ParentLocality = $model->getLastModif();
				//$this->StatesMenu($ParentState,$compid);	 
			}
	}
}









	function CategoryMenu($cid,$compid,$LocalityMenuId)
	{		
	
global $mainframe,$option;
$model = $this->getModel('menu');	


		
		
$db =& JFactory::getDBO();

$query = ' SELECT * from #__properties_category WHERE id = '.$cid.'';
echo $query;
$db->setQuery( $query );
		$mitems = $db->loadObject();
		

			$menu['id']='';
$menu['menutype']='propertieshidden';			
$menu['name']=$mitems->name;
$menu['link']='index.php?option=com_properties&view=properties&cid='.$mitems->id.'';	
$menu['type']='component';
$menu['published']='1';
$menu['parent']=$LocalityMenuId;
$menu['componentid']=$compid;
$menu['ordering']=$ord;
$menu['home']='0';
$menu['access']=$cat->access;	

//echo '<pre>';print_r($menu);echo '</pre>';

if ($model->store($menu)) {	$this->reseult[]=$menu; }
return $model->getLastModif();
}




	function TypeMenu($cid,$tid,$compid,$CategoryMenuId)
	{
	
global $mainframe,$option;
$model = $this->getModel('menu');	
$db =& JFactory::getDBO();

$query = ' SELECT * from #__properties_type WHERE id = '.$tid.'';
echo $query;
$db->setQuery( $query );
		$mitems = $db->loadObject();		
		
		
			$menu['id']='';
$menu['menutype']='propertieshidden';			
$menu['name']=$mitems->name;
$menu['link']='index.php?option=com_properties&view=properties&cid='.$mitems->parent.'&tid='.$mitems->id;	
$menu['type']='component';
$menu['published']='1';
$menu['parent']=$CategoryMenuId;
$menu['componentid']=$compid;
$menu['ordering']=$ord;
$menu['home']='0';
$menu['access']=$cat->access;	

//echo '<pre>';print_r($menu);echo '</pre>';

if ($model->store($menu)) {	$this->reseult[]=$menu; }

	return $model->getLastModif();
}

		
		
		
		
		
		
		
		
		
		
		
		
		

	
	
	
	
	
}