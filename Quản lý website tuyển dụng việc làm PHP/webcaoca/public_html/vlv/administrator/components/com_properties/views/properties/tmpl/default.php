﻿<?php
/**
* @copyright	Copyright(C) 2008-2010 Fabio Esteban Uzeltinger
* @license 		GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @email		admin@com-property.com
**/
// Disallow direct access to this file
defined('_JEXEC') or die('Restricted access');
$Version='3.1.0518';
?>

<?php
require_once( JPATH_COMPONENT.DS.'helpers'.DS.'menu_left.php' );
?>
<table width="100%">
	<tr>
		<td align="left" width="200" valign="top">
<?php echo MenuLeft::ShowMenuLeft();?>

		</td>
        <td align="left" valign="top">                
        
<table width="100%" border="0">
	<tr>
		<td width="70%" valign="top">
			<div id="cpanel">
            	<?php echo $this->addIcon('configuration.png','configuration', JText::_('Configuration'));?>
				<?php echo $this->addIcon('categories.png','categories', JText::_('Categories'));?>
				<?php echo $this->addIcon('types.png','types', JText::_('Types'));?>
				<?php echo $this->addIcon('properties.png','products', JText::_('Products'));?>
				<?php echo $this->addIcon('countries.png','countries', JText::_('Countries'));?>
				<?php echo $this->addIcon('states.png','states', JText::_('States'));?>
				<?php echo $this->addIcon('profiles.png','profiles', JText::_('Agents Profiles'));?>
                <?php echo $this->addIcon('users.png','profiles', JText::_('Users Data'));?>
			</div>
		</td>
		<td width="30%" valign="top">
			<?php
				echo $this->pane->startPane( 'stat-pane' );
				echo $this->pane->startPanel( JText::_('Properties') , 'welcome' );
			?>
			<table class="adminlist">
				<tr>
					<td>
						<div style="font-weight:700;">
							<?php echo JText::_('Version').' : '.$Version;?> 
						</div>
						<p>							
					<a href="http://www.com-property.com" target="_blank">http://www.com-property.com</a>							
						</p>
						<p>
					<a href="http://www.com-property.com" target="_blank">http://www.com-property.com/</a>
						</p>
					</td>
				</tr>
			</table>
			<?php
				echo $this->pane->endPanel();
				echo $this->pane->startPanel( JText::_('Products') , 'community' );
			?>
				<table class="adminlist">
					<tr>
						<td>
							<?php echo JText::_( 'Products' ).': '; ?>
						</td>
						<td align="center">
							<strong><?php echo $this->products; ?></strong>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo JText::_( 'Categories' ).': '; ?>
						</td>
						<td align="center">
							<strong><?php echo $this->categories; ?></strong>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo JText::_( 'Types' ).': '; ?>
						</td>
						<td align="center">
							<strong><?php echo $this->types; ?></strong>
						</td>
					</tr>
				</table>

			<?php
				echo $this->pane->endPanel();
				echo $this->pane->endPane();
			?>
		</td>
	</tr>
</table>

		</td>
	</tr>
</table>


<form action="index.php" method="post" name="adminForm">


		
		<input type="hidden" name="option" value="com_properties" />
		<input type="hidden" name="task" value="" />
        
		
		
</form>