<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view' );

class PropertiesViewProperty extends JView
{
	function display($tpl = null)
	{
	global $mainframe;
	$mainframe->addCustomHeadTag('<link rel="stylesheet" href="components/com_properties/imagenes/properties.css" type="text/css" />');



$TableName = JRequest::getCmd('table');
$datos		=& $this->get('Data');
$Catdatos	=& $this->get('Categorias');

$isNew		= ($datos->id < 1);
			
		$text = $isNew ? JText::_( 'New' ) : JText::_( 'Edit' );
		JToolBarHelper::title(   JText::_( $TableName ).': <small><small>[ ' . $text.' ]</small></small>' );
	JToolBarHelper::apply();
		JToolBarHelper::save();
		if ($isNew)  {
			JToolBarHelper::cancel();
		} else {
			// for existing items the button is renamed `close`
			JToolBarHelper::cancel( 'cancel', 'Close' );
		}

if($TableName=='category'){
$tpl=$TableName;
}
if($TableName=='type'){
$tpl=$TableName;
}
if($TableName=='products'){
$tpl=$TableName;
$user =& JFactory::getUser();
$this->assignRef('user',$user);	
}
if($TableName=='profiles'){
$tpl=$TableName;
$user =& JFactory::getUser();
$this->assignRef('user',$user);	
}
		$this->assignRef('datos',		$datos);	
		$this->assignRef('Catdatos',		$Catdatos);	

		parent::display($tpl);
	}
}