<?php defined('_JEXEC') or die('Restricted access'); 
$option = JRequest::getCmd('option');
JHTML::_('behavior.tooltip');
jimport('joomla.html.pane');
JHTML::_('behavior.formvalidation');
require_once( JPATH_COMPONENT.DS.'helpers'.DS.'calendar.php' );
$tab=JRequest::getVar('tab');
$pane =& JPane::getInstance('tabs', array('startOffset'=>$tab)); 

$component = JComponentHelper::getComponent( 'com_properties' );
$params = new JParameter( $component->params );
$UseCountry=$params->get('UseCountry');
$UseCountryDefault=$params->get('UseCountryDefault');
$UseState=$params->get('UseState');
$UseStateDefault=$params->get('UseStateDefault');
$UseLocality=$params->get('UseLocality');
$UseLocalityDefault=$params->get('UseLocalityDefault');

//echo "<pre>";print_r($this->datos);exit;
if($this->datos){
	//echo 'datos';
}else{
	//echo 'NO datos';
	$this->datos->cyid=$UseCountryDefault;
	$this->datos->sid=$UseStateDefault;
	$this->datos->eid=$UseLocalityDefault;
}
//print_r($this->user);
?>
<script type="text/javascript">
/*function CountJP(adf) {
	var num = $('.j_p').length();
	alert(num);
}*/
</script>
<script language="javascript" type="text/javascript">

function submitbutton(pressbutton) {
	var form = document.adminForm;
	if (pressbutton == 'cancel') {
		submitform( pressbutton );
		return;
	}
	else {
		submitform( pressbutton );
	}
}

</script>
 
<script language="javascript" type="text/javascript">
function submitdate(date,task) {
	var url = 'index.php?option=com_properties&controller=availables&task='+task+'&id=<?PHP echo $this->datos->id;?>&date='+date;
	window.location.href=url;
}
</script>
<script language="javascript" type="text/javascript">
function submitFromTo(task) {
	var from = document.getElementById('from').value;
	var to = document.getElementById('to').value;	
	var url = 'index.php?option=com_properties&controller=availables&task='+task+'&id=<?PHP echo $this->datos->id;?>&from='+from+'&to='+to;
	window.location.href=url;	
}
</script>
<script type="text/javascript">
	Window.onDomReady(function(){
		document.formvalidator.setHandler('passverify', function (value) { return ($('password').value == value); }	);
	});
</script>

<script type="text/javascript">
function ChangeType(a){
	var progressT = $('progressT');
	new Ajax("<?php echo JURI::base();?>index.php?option=com_properties&controller=ajax&format=raw&task=ChangeType",
	{
		method: 'get',
		onRequest: function(){progressT.setStyle('visibility', 'visible');},
		onComplete: function(){progressT.setStyle('visibility', 'hidden');},
		update: $('AjaxType'), 
		data: 'Category_id='+a
	}).request();
}
function ChangePacket(a){
	var at="<?php echo $this->datos->jp?>";
	var ktr = "<?php echo $this->datos->jpacket?>";
	var val = document.getElementById('hid_jp');
	val.value = "";
	if(a == ktr)
		val.value =at;
	
	var progressJP = $('progressJP');
	new Ajax("<?php echo JURI::base();?>index.php?option=com_properties&controller=ajax&format=raw&task=ChangePacket",
	{
		method: 'get',
		onRequest: function(){progressJP.setStyle('visibility', 'visible');},
		onComplete: function(){progressJP.setStyle('visibility', 'hidden');},
		update: $('AjaxPack'), 
		data: 'pack_id='+a+'&at='+at+'&cont='+ktr
	}).request();
}
function ChangeState(a){
	
	var progressS = $('progressS');
	new Ajax("<?php echo JURI::base();?>index.php?option=com_properties&controller=ajax&format=raw&task=ChangeState",
	{
		method: 'get',
		onRequest: function(){progressS.setStyle('visibility', 'visible');},
		onComplete: function(){progressS.setStyle('visibility', 'hidden');},
		update: $('AjaxState'), 
		data: 'Country_id='+a
	}).request();
}	
				
function ChangeLocality(a){
	var progressL = $('progressL');
	new Ajax("<?php echo JURI::base();?>index.php?option=com_properties&controller=ajax&format=raw&task=ChangeLocality",
	{
		method: 'get',
		onRequest: function(){progressL.setStyle('visibility', 'visible');},
		onComplete: function(){progressL.setStyle('visibility', 'hidden');},
		update: $('AjaxLocality'), 
		data: 'State_id='+a
	}).request();
}
</script>

<script type="text/javascript">
function ShowListRates(a){
	var progressSLR = $('progressSLR');
	new Ajax("<?php echo JURI::base();?>index.php?option=com_properties&controller=rates&format=raw&task=ShowListRates",
	{
		method: 'get',
		onRequest: function(){progressSLR.setStyle('visibility', 'visible');},
		onComplete: function(){progressSLR.setStyle('visibility', 'hidden');},
		update: $('div_addrates'), 
		data: '&productid='+a
	}).request();
}	
</script>


<script type="text/javascript">

function publishAjax(a,b){
var progressPA = $('progressPA');
new Ajax("<?php echo JURI::base();?>index.php?option=com_properties&controller=rates&format=raw&task=publishAjax",
{
	method: 'get',
	onRequest: function(){progressPA.setStyle('visibility', 'visible');},
	onComplete: function(){progressPA.setStyle('visibility', 'hidden');},
	update: $('publishAjax'+a),
	data: '&productid='+a+'&change='+b
	}).request();
}

function deleteAjax(a){
	var progressDA = $('progressDA');
	new Ajax("<?php echo JURI::base();?>index.php?option=com_properties&controller=rates&format=raw&task=removeAjax",
	{method: 'get',
	onRequest: function(){progressDA.setStyle('visibility', 'visible');},
	onComplete: function(){progressDA.setStyle('visibility', 'hidden');},
	update: $('div_addrates'),
	data: '&rateid='+a}).request();
}

function ShowAddRate(a,b){
	var progressSAR = $('progressSAR');
	new Ajax("<?php echo JURI::base();?>index.php?option=com_properties&controller=rates&format=raw&task=ShowAddRate",
	{method: 'get',
	onRequest: function(){progressSAR.setStyle('visibility', 'visible');},
	onComplete: function(){progressSAR.setStyle('visibility', 'hidden');},
	update: $('div_addrates'), 
	data: '&product_id='+a+'&rate_id='+b}).request();
}	


function AddRate(){
	var progressAR = $('progressAR');
	new Ajax("<?php echo JURI::base();?>index.php?option=com_properties&controller=rates&format=raw&task=AddRate",
	{method: 'post',
	onRequest: function(){progressAR.setStyle('visibility', 'visible');},
	onComplete: function(){progressAR.setStyle('visibility', 'hidden');},
	update: $('div_addrates'),
	data: $('adminFormAddRate')}).request();
}

function jSelectArticle(id, title, object) {
	document.getElementById(object + '_id').value = id;
	document.getElementById(object + '_name').value = title;
	
	document.getElementById('parent').value = id;
	
	document.getElementById('sbox-window').close();
}
function jSelectCoord(lat,lng) {
	document.getElementById('lat').value = lat;
	document.getElementById('lng').value = lng;			
	document.getElementById('sbox-window').close();
}		
</script>
<script type="text/javascript">
function check_jp(a){
	//alert(a);
	var chk = $(a);
	var val = document.getElementById('hid_jp');
	
	if(chk.checked==true ){
		var val_add = val.value+a+','; 
		val.value = val_add;
	}
	else if(chk.checked==false){
		var val_rem = val.value;
		val.value = val_rem.replace(a+',','');
	}
}
window.addEvent('domready', function() {
    if($('sid').value)
    	document.getElementById('addcity').style.display='block';
});
</script>
<div id="progressSLR"></div>
<div id="progressSAR"></div>
<div id="progressAR"></div>
<link rel="stylesheet" type="text/css" href="<?php echo JURI::root()?>components/com_properties/assets/css/style.css"></link>
<form action="index.php" method="post" name="adminForm" id="adminForm" class="form-validate" enctype="multipart/form-data" >
<?php 
//echo "<pre>";print_r($this->datos->agent_id);exit;
	/*if($this->datos->agent_id){
		$agent_id = $this->datos->agent_id;
	}else{
		$agent_id =$this->user->id;
	}*/?>
<input type="hidden" value="<?php if ($this->datos->sid) echo $this->datos->sid?>" id="sid" name="sid" >	
<input type="hidden" id="agent_id_employ" name="agent_id_employ" value="<?php echo $this->datos->agent_id; ?>" />

<?php if($this->datos->agent){$agent_name = $this->datos->agent;}else{$agent_name =$this->user->username;}?>
<input type="hidden" id="agent" name="agent" value="<?php echo $this->user->username; ?>" />
<?php 
if($this->datos->listdate){$listdate = $this->datos->listdate;}else{$listdate =date("Y-m-d");}
$refresh_time =date("Y-m-d H:i:s");
//$listdate = $this->datos->listdate ? date("Y-m-d") : $this->datos->listdate;
?>
<input type="hidden" id="refresh_time" name="refresh_time" value="<?php echo $refresh_time; ?>" />
<input type="hidden" id="listdate" name="listdate" value="<?php echo $listdate; ?>" />


<div class="col width-100" style="width:100%;">
<?php
echo $pane->startPane( 'pane' );
echo $pane->startPanel( 'Details', 'panel1' );
?>
	
	<fieldset class="adminform">    
	
	<legend><?php echo JText::_( 'Details' ); ?></legend>
	<div class="col" style="width: 40%;border: 1px solid #ccc;" >
			<table class="admintable" border="0">
                <tr>
					<td class="key"><label for="name"><?php echo JText::_( 'Title' ); ?>:</label></td>
					<td>
                        <input class="text_area" type="text" name="name" id="name" size="60" maxlength="250" value="<?php echo $this->datos->name; ?>" />
                        <!-- <textarea class="inputbox required" name="name" id="name"  cols="34" rows="3"><?php //echo $this->datos->name; ?></textarea> -->
					</td>
                    
			   </tr>
                  
                  			<tr>
			<td align="right" class="key">
				<label for="name">
							<?php echo JText::_( 'Alias' ); ?>:
						</label>
			</td>
			<td>
				<input class="text_area" type="text" name="alias" id="alias" size="60" maxlength="250" value="<?php echo $this->datos->alias;?>" />
			</td>
           
		</tr> 
                <tr style="display:none;">        
<td align="right" class="key">
		<label for="name"><?php echo JText::_( 'Category' ); ?>:</label>			</td>
<td>
<?php echo CatTreeHelper::ParentCategoryType( $this->datos,'category','products' ); ?></td>
		
                </tr> 
                
                
                
                <tr>
					<td class="key"><label for="name"><?php echo JText::_( 'Type' ); ?>:</label></td>
					<td width="260" >
                     <?php //echo SelectHelper::SelectType( $this->datos,'type',$this->datos->type); ?>	
                     
 					<div id="AjaxType" style="float:left">
                    
                    <?php 
		  $row->id=0;
		  $row->parent 	= $this->datos->cid;
		  $row->type 	= $this->datos->type;
		  echo SelectHelper::SelectTypeEmploy( $row,'type','form_products' ); 
		  ?>              
              
              		</div>
              		<div id="progressT"></div>        
                     </td>
				    
				</tr>
			<tr>
					<td class="key"><label for="user_id"><?php echo JText::_( 'Country' ); ?>:</label></td>
					<td>
                    <?php echo SelectHelper::SelectAjaxPaises( $this->datos,'country',$this->datos->cyid); ?>              
                      </td>   
				    <td rowspan="3"><div id="progressccc"></div>
                    </td>
  			</tr>
  			<tr>                
		      	<td class="key"><label for="catid"><?php echo JText::_( 'State' ); ?>:</label></td>
				<td>
					<div id="AjaxState-test" style="float:left">
			          <?php 
						$row->id=0;
						$row->cyid = $this->datos->cyid;
						$row->sid = $this->datos->sid;
						$arr_tinhthanh = explode(',',$this->datos->sid);
						$lists = SelectHelper::SelectAjaxStates( $row,'states',$this->datos->sid );
						?>
						<!-- select 3 tinh thanh -->
						<div style="height: 115px;" class="scrollBox  nobold select span-7">
						  	<ul>
						<?php 
					    foreach ($lists as $list) {
					  	?>
	                        <li>
								<input <?php if (in_array($list->id,$arr_tinhthanh)) echo 'checked="checked"' ?> class="sel_checkbox" type="checkbox" onclick="addTinhThanh('<?php echo $list->id?>','<?php echo $list->name?>','tinhthanh_<?php echo $list->id?>')" id="tinhthanh_<?php echo $list->id?>" value="<?php echo $list->id?>" tabindex="14" name="tinhthanh[]">
								<label id="textcity_<?php echo $list->id?>" class="pointer"><?php echo $list->name?></label>
							</li>
						<?php }?>
						
							</ul>
						</div>
						<div style="left: 428px;display: none;margin-top:-50px;" class="floatBox span-5 absolute" id="addcity">
							<span class="floatBoxArrow">&nbsp;</span>
							<ul class="relative" id="tinhthanhAdd" style="padding-left:5px;">
								<?php 
								foreach ($lists as $list) {
									if (in_array($list->id,$arr_tinhthanh)){
								?>
									<li id="tinhthanh_<?php echo $list->id;?>" class="clear"><span class="span-4plus"><?php echo $list->name;?></span>
										<span class="absolute light">[<a href="javascript:void(0)" onclick="removeTinhThanh('<?php echo $list->id;?>','tinhthanh_<?php echo $list->id;?>');">x</a>]</span>
									</li>
								<?php }
									}?>
							</ul>
						</div>
						<!-- end select -->
		          </div>
		          <!--<div id="AjaxState" style="float:left">
		          
		          <?php 
				 /* $row->id=0;
				  $row->cyid = $this->datos->cyid;
				  $row->sid = $this->datos->sid;
				  echo SelectHelper::SelectAjaxStates( $row,'states',$this->datos->sid );*/ 
				  ?>
		          
		              </div>
		              --><div id="progressS"></div>
		              </td>

				</tr>
                <tr>
					<td class="key"><label for="user_id"><?php echo JText::_( 'Published' ); ?>:</label></td>
					<td >
<?php $chequeado0 = $this->datos->published ? JText::_( '' ) : JText::_( 'checked="checked"' );?>
<?php $chequeado1 = $this->datos->published ? JText::_( 'checked="checked"' ) : JText::_( '' );?>
                    <input name="published" id="published0" value="0" <?php echo $chequeado0;?> type="radio">
	<label for="published0"><?php echo JText::_( 'No' ); ?></label>
	<input name="published" id="published1" value="1" <?php echo $chequeado1;?> type="radio">
	<label for="published1"><?php echo JText::_( 'Yes' ); ?></label>					</td>
				    <td >&nbsp;</td>
                </tr>
				<tr>
                    <td class="key">
                    <label>
						<?php echo JText::_( 'Upload CV' ); ?>:
					</label></td>
                    <td colspan="2">
                    <input class="input_box" id="panoramic" name="panoramic" size="43" type="file" />                    
                    <?php echo JHTML::_('tooltip', JText::_( 'TOOLTIP_CHANGE_PANORAMIC')); ?></td>
                </tr>
                <tr>
					<td class="key"><label for="image"><?php echo JText::_( 'Panoramic' ); ?>:</label></td>
					<td >
                   <?php 
				 
					$img_path = $mainframe->getSiteURL() .'images/properties/panoramics/'.$this->datos->panoramic;?>
					<span class="editlinktip hasTip" title="<?php echo $this->datos->image1;?>::
					<img border=&quot;1&quot; src=&quot;<?php echo $img_path; ?>&quot; name=&quot;imagelib&quot; alt=&quot;<?php echo JText::_( 'No preview available'.$img_path ); ?>&quot; width=&quot;206&quot; height=&quot;100&quot; />">
					<a class="modal" rel="{handler: 'iframe', size: {x: 640, y: 480}}" href="<?php echo $img_path; ?>"><?php echo $this->datos->panoramic; ?></a></span>					</td>
				    <td >&nbsp;</td>
                  </tr>  
                   <tr>
						<td  class="key"><label for="name"><?php echo JText::_( 'Agent' ); ?>:</label></td>
						<td>
			           <?php echo $this->datos->agent_id;?> : <?php echo SelectHelper::SelectAgent($this->datos->agent_id); ?>                       
	                    </td>
					    <td>&nbsp;</td>
					</tr>  
		        	<tr>
							<td class="key">
								<label for="name">
									<?php echo JText::_( 'Name' ); ?>:						</label>					</td>
							<td >
		                    
												</td>
						    <td >&nbsp;</td>
		        	</tr> 
				</table>
			
	</div>
  	<div class="col" style="width: 45%;border: 1px solid #ccc;margin-left: 10px;">
  		<table class="admintable" border="0">
                <tr>
					<td class="key"><label for="name"><?php echo JText::_( 'Work experience' ); ?>:</label></td>
					<td>
                        <input class="text_area" type="text" name="jobyear" id="jobyear" size="10" maxlength="250" value="<?php echo $this->datos->jobyear;?>" />
                        
                    </td>
			   </tr>
			   <tr>
					<td class="key"><label for="name"><?php echo JText::_( 'Degree Level' ); ?>:</label></td>
					<td>
                        <?php echo CatTreeHelper::getEducation( $this->datos->eid,'education'); ?></td>
                    </td>
			   </tr>
			   <tr>
					<td class="key"><label for="name"><?php echo JText::_( 'Time job work' ); ?>:</label></td>
					<td>
						<?php echo CatTreeHelper::getJobTime( $this->datos->jid,'jobtime'); ?></td>
                        
                    </td>
			   </tr>
			   <tr>
					<td class="key"><label for="name"><?php echo JText::_( 'Minimum Salary' ); ?>:</label></td>
					<td>
						<input class="text_area" type="text" name="m_salary" id="m_salary" size="10" maxlength="255" value="<?php echo $this->datos->m_salary; ?>" />
						<?php echo CatTreeHelper::getSalary( $this->datos->salary,'salary'); ?>
		                      
		                  </td>
			   </tr>
				<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'In Currency' ); ?>:
						</label>
					</td>
					<td >
						<input class="text_area" type="text" name="i_curren" id="i_curren" size="10" maxlength="255" value="<?php echo $this->datos->i_curren; ?>" />
					</td>
		             <td width="56">
		            </td>
				</tr>
			    <tr>
					<td class="key"><label for="name"><?php echo JText::_( 'Time effect' ); ?>:</label></td>
					<td>
					<?php //echo JText::_('From') ?>
					<?php 
						echo JHTML::calendar($this->datos->startdate,'startdate','startdate','%Y-%m-%d');
					?>
                    <?php echo JText::_('=>') ?>
					<?php 
						echo JHTML::calendar($this->datos->enddate,'enddate','enddate','%Y-%m-%d');
					?>   
					<?php  //echo Calendar::ShowCalendar( $this->datos,'category','products' ); ?>   
                    </td>
			   </tr>
			   
		</table> 
  	</div>	
  	<br />
  	<div class="col" style="width: 45%;border: 1px solid #ccc;margin-left: 10px;margin-top: 10px;">
  		<table class="admintable" border="0">
            <tr>
				<td class="key"></td>
				<td>
					<label class="key" for="name"><?php echo JText::_( 'Packet Job' ); ?>:</label>
              		<?php //echo CatTreeHelper::getJobPacket( $this->datos->jpacket,'jpacket'); ?>
             	</td>
             	
	   		</tr>
	   		<tr>
		   		<td></td>
		   		<td>
		   				<?php
							//$arrpacks = array(); 
							//$listpaks = $this->datos->jp;//echo $listpaks; 
							//$listpak = explode(',',substr($listpaks,0,-1));//echo "<pre>";print_r($listpak);exit;
							//$arrpacks = implode(',',$listpak);print_r($arrpacks); check_jp(this.value); if (in_array('1',$listpak)) echo 'checked="checked"';
							?>
						<p><input type="checkbox" id="1" name="p1" value="1" <?php if ($this->datos->p1 == 1) echo 'checked="checked"';?> />&nbsp; <?php echo JText::_('Top đầu')?> <?php echo JHTML::_('tooltip', JText::_( 'TOOLTIP_CHANGE_TOP_DAU')); ?>
							<?php 
								echo JHTML::calendar($this->datos->date1,'date1','date1','%Y-%m-%d');
							?>
						</p>
						<p><input type="checkbox" id="2" name="p2" value="2" <?php if ($this->datos->p2 == 2) echo 'checked="checked"';?> />&nbsp; <?php echo JText::_('Bôi đậm đỏ')?> <?php echo JHTML::_('tooltip', JText::_( 'TOOLTIP_CHANGE_BOI_DAM_DO')); ?>
							<?php 
								echo JHTML::calendar($this->datos->date2,'date2','date2','%Y-%m-%d');
							?>
						</p>
						<p><input type="checkbox" id="3" name="p3" value="3" <?php if ($this->datos->p3 == 3) echo 'checked="checked"';?> />&nbsp; <?php echo JText::_('Việc làm tốt nhất')?> <?php echo JHTML::_('tooltip', JText::_( 'TOOLTIP_CHANGE_VIEC_LAM_TOT_NHAT')); ?>
							<?php 
								echo JHTML::calendar($this->datos->date3,'date3','date3','%Y-%m-%d');
							?>
						</p>
	              		<?php //echo CatTreeHelper::getJobPacket( $row_product->jpacket,'jpacket'); ?>
	             	<div id="AjaxPack" style="float:left">
	             		<?php //onclick="check_jp(this.id)" echo CatTreeHelper::getCheckJob( $this->datos->jpacket,$this->datos->jp,'jpacket'); ?>
	      		 	</div>
	      		 	<div id="progressJP"></div>
	      		 	<input type="hidden" name="jp" value="<?php echo $this->datos->jp;?>" id="hid_jp" />
		   		</td>
	   		</tr>
	   		
		</table> 
  	</div>	
  </fieldset>  
 
<?php 
echo $pane->endPanel();
echo $pane->startPanel( 'Description', 'panel3' );
?>
<fieldset>  
<legend><?php echo JText::_('Description')?></legend>
<table class="admintable" border="0">
<tr>
	<td class="key"><label for="name"><?php echo JText::_( 'Kinh nghiệm/Kỹ năng chi tiết' ); ?>:</label></td>
	<td>
	<?php
		$editor = &JFactory::getEditor();		
		echo $editor->display('description', $this->datos->description, '600', '300', '60', '40');
	?>
	</td>
</tr>
<tr>
	<td class="key"><label for="name"><?php echo JText::_( 'Mô tả chi tiết công việ' ); ?>:</label></td>
	<td>
	<?php
		$editor = &JFactory::getEditor();		
		echo $editor->display('text', $this->datos->text, '600', '300', '60', '40');
	?>
	</td>
</tr>
<tr>
	<td class="key"><label for="name"><?php echo JText::_( 'Quyền lợi được hưởng' ); ?>:</label></td>
	<td>
	<?php
		$editor = &JFactory::getEditor();		
		echo $editor->display('quyenloi', $this->datos->quyenloi, '600', '300', '60', '40');
	?>
	</td>
</tr>

</table>
</fieldset>  
<?php 
echo $pane->endPanel();
?>

<div class="clr"></div>
<input type="hidden" name="option" value="<?php echo $option; ?>" />
<input type="hidden" name="table" value="<?php echo $TableName; ?>" />
<input type="hidden" name="id" value="<?php echo $this->datos->id; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="controller" value="products" />

<?php echo JHTML::_( 'form.token' ); ?>
</div>
</form>

<?php

echo $pane->endPane();
//require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_properties'.DS.'check_job.php'); 
?>