<?php 
/**
* @copyright	Copyright(C) 2008-2010 Fabio Esteban Uzeltinger
* @license 		GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @email		admin@com-property.com
**/
defined('_JEXEC') or die('Restricted access'); 
$option = JRequest::getCmd('option');
JHTML::_('behavior.tooltip');
JHTML::_('behavior.tooltip');
jimport('joomla.html.pane');
$pane =& JPane::getInstance('tabs', array('startOffset'=>0)); 
?>

<?php
require_once( JPATH_COMPONENT.DS.'helpers'.DS.'menu_left.php' );
?>
<script type="text/javascript">
function ChangeType(a){
	var progressT = $('progressT');
	new Ajax("<?php echo JURI::base();?>index.php?option=com_properties&controller=ajax&format=raw&task=ChangeType",
	{
		method: 'get',
		onRequest: function(){progressT.setStyle('visibility', 'visible');},
		onComplete: function(){progressT.setStyle('visibility', 'hidden');},
		update: $('AjaxType'), 
		data: 'Category_id='+a
	}).request();
}

</script>


<table width="100%">
	<tr>
		<td align="left" width="200" valign="top">
<?php echo MenuLeft::ShowMenuLeft();?>
		</td>
        <td align="left" valign="top">        
<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
<div class="col100">

		<table class="admintable">
<tr><td>
	<fieldset class="adminform">
		<legend><?php echo JText::_( 'Information Seeker' ); ?></legend>
<table>   
         <tr>
			<td  class="key"><label for="name"><?php echo JText::_( 'Client' ); ?>:</label></td>
			<td>
                  <div style="float:left;">
					 <?php echo SelectHelper::SelectCliente( $this->seeker,'users',$this->seeker->cid); ?>
                      </div>                         
				<div style="float:left; margin-left:10px; " id="AjaxCliente">                   
                  </div><div id="progressR"></div>  
            </td>
		</tr> 
		<tr>
			<td  class="key"><label for="name"><?php echo JText::_( 'Name' ); ?>:</label></td>
			<td>
				<select size="1" class="inputbox" id="salsek" name="salsek">
					<option value="Mr." <?php if ($this->seeker->salsek == 'Mr.')echo 'selected="selected"';?>>Mr.</option>
					<option value="Mrs." <?php if ($this->seeker->salsek == 'Mrs.')echo 'selected="selected"';?>>Mrs.</option>
				</select>
				<input class="text_area" type="text" name="name" id="name" size="20" maxlength="255" value="<?php echo $this->seeker->name; ?>" />
			</td>
		</tr> 
	      <tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Birth day' ); ?>:
						</label>
					</td>
					<td>
						<?php 
							echo JHTML::calendar($this->seeker->birth_day,'birth_day','birth_day');
						?>
	                </td>
				</tr>
	       	<tr>
	       		<td class="key"><?php echo JText::_( 'Gender' ); ?></td>
	       		<td>
	       			<select class="field_list gender" id="gender" name="gender">
					  	<option value="0" >....</option>
					  	<option value="1" <?php if ($this->seeker->gender == 1)echo 'selected="selected"'?>>Nam</option>
						<option value="2" <?php if ($this->seeker->gender == 2)echo 'selected="selected"'?>>Nữ</option>
					</select>
	       		</td>
	       	</tr>
	       	<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Country' ); ?>:
						</label>
					</td>
					<td >
						<?php echo SelectHelper::SelectAjaxPaises( $this->seeker,'country',$this->seeker->country); ?>
					</td>
				</tr>
	       	<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Location' ); ?>:
						</label>
					</td>
					<td >
						<input class="text_area" type="text" name="location" id="location" size="20" maxlength="255" value="<?php echo $this->seeker->location; ?>" />
					</td>
				</tr>    
                <tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Address' ); ?>:
						</label>
					</td>
					<td >
						<input class="text_area" type="text" name="address" id="address" size="20" maxlength="255" value="<?php echo $this->seeker->address; ?>" />
					</td>
				</tr>
	       		
	       	<tr>
				<td class="key">
					<label for="name">
						<?php echo JText::_( 'How You Know' ); ?>:
					</label>
				</td>
				<td >
					<?php echo CatTreeHelper::getYouKnow( $this->seeker->youknow,'youknow'); ?>
				</td>
			</tr>
	       	
   </table></fieldset>
   <fieldset class="adminform">
		<legend><?php echo JText::_( 'Experience / Education' ); ?></legend>
<table>              
        <tr>
			<td class="key">
				<label for="name">
					<?php echo JText::_( 'Current Position' ); ?>:
				</label>
			</td>
			
			<td >
				<input class="text_area" type="text" name="c_post" id="c_post" size="20" maxlength="255" value="<?php echo $this->seeker->c_post; ?>" />
			</td>
		</tr>
		<!-- bỏ -->                
        <!--<tr>
			<td class="key"><label for="name"><?php //echo JText::_( 'Bằng cấp' ); ?>:</label></td>
			<td>
                 <?php //echo CatTreeHelper::getEducation( $this->seeker->eid,'education'); ?>
             </td>
	   </tr> 
		--></table>
	</fieldset>
	
	
  </td>
  <td>
  	<fieldset class="adminform">
		<legend><?php echo JText::_( 'Desired Employment' ); ?></legend>
	<table>    
		<tr>        
			<td align="right" class="key">
					<label for="name"><?php echo JText::_( 'Category' ); ?>:</label>			</td>
			<td>
			<?php echo CatTreeHelper::ParentCategoryType( $this->seeker,'category','products' ); ?></td>
		
        </tr>
		<tr>
			<td class="key"><label for="name"><?php echo JText::_( 'Type' ); ?>:</label></td>
			<td width="260" >
             <?php //echo SelectHelper::SelectType( $this->datos,'type',$this->datos->type); ?>	
                 
			<div id="AjaxType" style="float:left">
                <?php 
				  $row->id=0;
				  $row->parent = $this->seeker->cid;
				  $row->type = $this->seeker->type;
				  echo SelectHelper::SelectType( $row,'type','form_products' ); 
				?>              
   
   		</div>
        </td>
				    
		</tr>
		
	   <tr>
			<td class="key"><label for="name"><?php echo JText::_( 'Time job work' ); ?>:</label></td>
			<td>
				<?php echo CatTreeHelper::getJobTime( $this->seeker->jid,'jobtime'); ?></td>
                  </td>
	   </tr>
	   <!--<tr>
			<td class="key"><label for="name"><?php //echo JText::_( 'Minimum Salary' ); ?>:</label></td>
			<td>
				<input class="text_area" type="text" name="m_salary" id="m_salary" size="20" maxlength="255" value="<?php echo $this->seeker->m_salary; ?>" />
				<?php //echo CatTreeHelper::getSalary( $this->seeker->salary,'salary'); ?></td>
                      
                  </td>
	   </tr>
		<tr>
			<td class="key">
				<label for="name">
					<?php //echo JText::_( 'In Currency' ); ?>:
				</label>
			</td>
			<td >
				<input class="text_area" type="text" name="i_curren" id="i_curren" size="20" maxlength="255" value="<?php echo $this->seeker->i_curren; ?>" />
			</td>
             <td width="56">
            </td>
		</tr>
		--><tr>
			<td class="key">
				<label for="name">
					<?php echo JText::_( 'Phone' ); ?>:
				</label>
			</td>
			<td >
				<input class="text_area" type="text" name="phone" id="phone" size="20" maxlength="255" value="<?php echo $this->seeker->phone; ?>" />
			</td>
		</tr>
     	<tr>    
		<td class="key">
			<label for="name">
				<?php echo JText::_( 'Published' ); ?>:
			</label>
		</td>
                 <td>
<?php $chequeado0 = $this->seeker->published ? JText::_( '' ) : JText::_( 'checked="checked"' );?>
<?php $chequeado1 = $this->seeker->published ? JText::_( 'checked="checked"' ) : JText::_( '' );?>
                    <input name="published" id="published0" value="0" <?php echo $chequeado0;?> type="radio">
	<label for="published0"><?php echo JText::_( 'No' ); ?></label>
	<input name="published" id="published1" value="1" <?php echo $chequeado1;?> type="radio">
	<label for="published1"><?php echo JText::_( 'Yes' ); ?></label>  
					</td>
				</tr>       
           		<tr>
					<td class="key">
						<label for="name">
							<?php echo JText::_( 'Ordering' ); ?>:
						</label>
					</td>
					<td >
						<input class="text_area" type="text" name="ordering" id="ordering" size="20" maxlength="255" value="<?php echo $this->seeker->ordering; ?>" />
					</td>
				</tr> 
		
		<tr>
          <?php
             $seeker_path = $mainframe->getSiteURL().'images/properties/profiles/';
             ?>
             <td class="key"><label>
	<?php echo JText::_( 'Image' ); ?>:
</label></td>
             <td>                    
             <img src="<?php echo $seeker_path.$this->seeker->image; ?>" /><br />
          </tr>  
          <tr>
              <td class="key"><label>
				<?php echo JText::_( 'Change Image' ); ?>:
				</label>
                       <br />  Max. 140x200
              </td>
              <td>
              <input class="input_box" id="image" name="image" type="file" />
              </td>              
          </tr>	         
	</table>
	</fieldset>
  </td>
				</tr>   
	</table>       
       
        
</div>

<div class="clr"></div>
<?php //$TableName = 'seeker';?>
<input type="hidden" name="option" value="<?php echo $option; ?>" />
<input type="hidden" name="table" value="<?php echo $TableName; ?>" />
<input type="hidden" name="id" value="<?php echo $this->seeker->id; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="controller" value="seeker" />
</form>
	</td>
		</tr>
			</table> 