<?php

// No Restricted access

defined( '_JEXEC' ) or die( 'Restricted access' );



$bar =& JToolBar::getInstance();

JToolBarHelper::title(JText::_('Edit').' '.$this->row->name , 'generic');

JToolBarHelper::save('save');

//JToolBarHelper::apply('apply');

JToolBarHelper::cancel('cancel');

JRequest::setVar('hidemainmenu', 1);

$editor =& JFactory::getEditor();

// clean item data

jimport('joomla.filter.output');

JFilterOutput::objectHTMLSafe($row, ENT_QUOTES, 'content');

$document =& JFactory::getDocument();

JHTML::_('behavior.combobox');
jimport('joomla.html.pane');
$pane =& JPane::getInstance('sliders');

JHTML::_('behavior.tooltip');
//onSubmit="outputSelected(adminForm.types)"
?>
<script type="text/javascript">
	function show_img(){
		tourimg = document.getElementById('tourimg');
		img_original = document.getElementById('img_original');								
		tourimg.src = '../'+img_original.value;
	}
</script>
<form action="index.php" method="post" name="adminForm">

	<div class="col width-70">

		<fieldset class="adminform">

			<legend><?php echo JText::_('Details'); ?></legend>
			
		<table class="admintable" cellspacing="1">

			<tr>

				<td valign="top" class="key">

					<?php echo JText::_('Name Company'); ?>:

				</td>

				<td>

					<input type="text" name="title" id="title" value="<?php echo $this->row->title; ?>" class="require inputbox" size="140">

				</td>

			</tr>

			<tr>

				<td valign="top" class="key">

					<?php echo JText::_('Alias'); ?>:

				</td>

				<td>

					<input type="text" name="alias" id="alias" value="<?php echo $this->row->alias; ?>" class="require inputbox" size="140">

				</td>

			</tr>
			
			<tr>
				<td valign="top" class="key">
					<?php echo JText::_('Phone'); ?>:
				</td>
				<td>
					<input type="text" name="phone" id="phone" value="<?php echo $this->row->phone; ?>" class="require inputbox" size="140">
				</td>
			</tr>
			
			<tr>
				<td valign="top" class="key">
					<?php echo JText::_('Email'); ?>:
				</td>
				<td>
					<input type="text" name="email" id="email" value="<?php echo $this->row->email; ?>" class="require inputbox" size="140">
				</td>
			</tr>
			<tr>
				<td valign="top" class="key">
					<?php echo JText::_('Website'); ?>:
				</td>
				<td>
					<input type="text" name="website" id="website" value="http://<?php echo $this->row->website; ?>" class="require inputbox" size="140">
				</td>
			</tr>
			
			<tr>

				<td valign="top" class="key">

					<?php echo JText::_('Published'); ?>:

				</td>

				<td>

					<?php echo $this->lists['published']; ?>

				</td>

			</tr>	

			<tr>

				<td valign="top" class="key">

					<?php echo JText::_('Description'); ?>:

				</td>

				<td>

					<?php echo $editor->display( 'description',  $this->row->description, '600', '150', '60', '10', array('pagebreak', 'readmore') ) ; ?>


				</td>

			</tr>

			</table>

		</fieldset>
	</div>

	<div class="col width-30">

		<fieldset class="adminform">

			<legend><?php echo JText::_('Images'); ?></legend>

			<table class="admintable" cellspacing="1">

				<tr>

					<td valign="top" class="key">Image Preview</td>

					<td>
							
						<img alt="View image" width="200" height="150" id="tourimg" src="<?php echo JURI::root();?><?php echo $this->row->img_original; ?>" >
						
						<p><?php echo JText::_('View Image'); ?></p>

					</td>

				</tr>

				<tr>

					<td valign="top" class="key">

						<?php echo JText::_('Images'); ?>:

					</td>

					<td>

						<script type="text/javascript">
							
							function outputSelected(seflist) {
								var cat = seflist.catid;
								var catidslist = document.getElementById('catidslist');
								var catid = document.getElementById('catid');
								var list = '';
								for (var i=0;i<cat.options.length; i++) {
									if(i!=0) { spe = ','; } else { spe = ''; }
									if (cat.options[i].selected) {
										list += spe + cat.options[i].value;
									}
								}
								catidslist.value = list;
								catid.value = list;

							function outputSelected(selRef) {

								var typeslist = document.getElementById('typeslist');

								var types = document.getElementById('types');

								var lists = '';

								for (var i=0;i<selRef.options.length; i++) {

									if(i!=0) { spe = ','; } else { spe = ''; }

									if (selRef.options[i].selected) {

										lists+= spe + selRef.options[i].value;

									}

								}

								typeslist.value = lists;

								types.value = lists;

							 }

						</script>						

						<input id="img_original" name="img_original" value="<?php echo $this->row->img_original; ?>" size="50" type="text">

						<p>

						<div class="button2-left"><div class="image"><a class="modal-button" title="Get Image" href="index.php?option=com_media&amp;view=images&amp;tmpl=component&amp;e_name=image&amp;el=img_original" rel="{handler: 'iframe', size: {x: 700, y: 500}}">Get Image</a></div></div>

						<div class="button2-left"><div class="image"><a title="View Image" href="javascript:void(0);" onClick="show_img();">View Image</a></div></div>						

						</p>						

					</td>

				</tr>

			</table>

		</fieldset>

		<fieldset class="adminform">

			<legend><?php echo JText::_('PROPERTIES'); ?></legend>

			<table class="admintable" cellspacing="1">

				<tr>

					<td valign="top" class="key">

						<?php echo JText::_('Categories'); ?>:

					</td>
					<td>
						<input type="hidden" name="catidslist" id="catidslist" value="" >
						
						<?php echo $this->lists['catid']; ?>					
					</td>

				</tr>


				<tr>
					<td valign="top" class="key">
						<?php echo JText::_('Started date'); ?>:
					</td>

					<td>
						<?php echo JHTML::calendar($this->row->startdate,'startdate','startdate','%Y-%m-%d'); ?>					
					</td>
				</tr>				

			</table>

		</fieldset>

		<fieldset class="adminform">

			<legend><?php echo JText::_('Metadata Information'); ?></legend>	
				<table class="admintable" cellspacing="1">

					<tr>
						<td valign="top" class="key">
							<?php echo JText::_('Description'); ?>:
						</td>

						<td>
							<textarea name="metadesc" cols="26" rows="5" class="text_area" id="metadesc"><?php echo $this->row->metadesc; ?></textarea>
						</td>
					</tr>

					<tr>

						<td valign="top" class="key">
							<?php echo JText::_('Keywords'); ?>:
						</td>

						<td>
							<textarea name="metakey" cols="26" rows="5" class="text_area" id="metakey"><?php echo $this->row->metakey; ?></textarea>
						</td>
					</tr>
				</table>
				<?php
			?>

		</fieldset>

	</div>


	<input type="hidden" name="option" value="com_product" />

	<input type="hidden" name="controller" value="product" />

	<input type="hidden" name="task" value="edit" />

	<input type="hidden" name="id" value="<?php echo $this->row->id; ?>" />

</form>