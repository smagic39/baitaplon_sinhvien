<?php

// no direct access

defined('_JEXEC') or die('Restricted access');
// Import Joomla! libraries

jimport( 'joomla.application.component.view');

class ProductViewProduct extends JView {

    function display($tpl = null) {

        JToolBarHelper::title(JText::_('Default Title'), 'module');
		global $mainframe, $option;
		
		$app = JFactory::getApplication('administrator');
		
		$document =& JFactory::getDocument();

		$filter_state		= $app->getUserStateFromRequest($option.'filter_state',		'filter_state',		'',		'filter_state');

		$filter_order		= $app->getUserStateFromRequest($option.'filter_order',		'filter_order',		'name');

		$search				= $app->getUserStateFromRequest($option.'search','search','','string');

		$search				= JString::strtolower($search);

		// Get data from the model

		$items		= & $this->get('Data');
		//echo "<pre>";print_r($items);exit;
		$total		= & $this->get('Total');

		$pagination = & $this->get('Pagination');

		// Build list of categories
		$javascript 	= 'onchange="document.adminForm.submit();"';

		// State filter
		$lists['state']	= JHTML::_('grid.state', $filter_state);
		
		// Table ordering
		$lists['order_Dir'] = $filter_order_Dir;

		$lists['order'] 	= $filter_order;

		// Search filter
		$lists['search']= $search;
		$this->assignRef('lists',		$lists);

		$this->assignRef('items',		$items);

		$this->assignRef('pagination',	$pagination);

		parent::display($tpl);
    }
}

?>