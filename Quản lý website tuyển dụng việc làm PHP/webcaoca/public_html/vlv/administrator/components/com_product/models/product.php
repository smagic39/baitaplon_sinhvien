<?php

// no direct access

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class ProductModelProduct extends JModel {

    var $_data 			= null;

	var $_total 		= null;

	var $_pagination 	= null;

	var $_xml			= null;

	var $_categories	= null;

	var $_types	= null;
	
	function __construct()	{

		parent::__construct();
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$state = $app->getUserStateFromRequest($this->context.'.filter.state', 'filter_state', '', 'string');
		$this->setState('filter.state', $state);

	}

	function publish($id) {

		$product =& JTable::getInstance('product', 'Table');
		$product->load($id);
		if ($product->published == 0) {
			$product->published = 1;
		} else {
			$product->published = 0;
		}
		$product->store();

	}
	
	function state($id){
		$product =& JTable::getInstance('product','Table');
		$product->load($id);
		if($product->state==0){
			$product->state=1;
		}else{
			$product->state=0;
			
		}
		$product->store();
	}
	// Delete product

	function delete($id) {
		
		$product =& JTable::getInstance('product', 'Table');

		if (!$product->load($id)) {

			return JError::raiseWarning(500, $product->getError());
		}
		return $product->delete($id);
	}
	// Save changes
	function resizeImage(&$img, $width, $height) {
			if (! $img)
				return '';
			if (! function_exists ( 'imagejpeg' ))
				return $img;
			$img = str_replace ( JURI::root (), '', $img );
			$img = rawurldecode ( $img );
			$imagesurl = (file_exists ( JPATH_SITE . '/' . $img )) ? JURI::root () . ProductModelProduct::jaResize ( $img, $width, $height ) : '';
			return $imagesurl;
		}
		
		function jaResize($image, $max_width, $max_height) {
			$path = JPATH_SITE;
			$sizeThumb = getimagesize ( JPATH_SITE . '/' . $image );
			$width = $sizeThumb [0];
			$height = $sizeThumb [1];
			if (! $max_width && ! $max_height) {
				$max_width = $width;
				$max_height = $height;
			} else {
				if (! $max_width)
					$max_width = 1000;
				if (! $max_height)
					$max_height = 1000;
			}
			$x_ratio = $max_width / $width;
			$y_ratio = $max_height / $height;
			if (($width <= $max_width) && ($height <= $max_height)) {
				$tn_width = $width;
				$tn_height = $height;
			} else if (($x_ratio * $height) < $max_height) {
				$tn_height = ceil ( $x_ratio * $height );
				$tn_width = $max_width;
			} else {
				$tn_width = ceil ( $y_ratio * $width );
				$tn_height = $max_height;
			}
			// read image
			$ext = strtolower ( substr ( strrchr ( $image, '.' ), 1 ) ); // get the file extension
			$rzname = strtolower ( substr ( $image, 0, strpos ( $image, '.' ) ) ) . "_{$tn_width}_{$tn_height}.{$ext}"; // get the file extension
			$resized = $path . '/images/resized/' . $rzname;
			if (file_exists ( $resized )) {
				$smallImg = getimagesize ( $resized );
				if (($smallImg [0] <= $tn_width && $smallImg [1] == $tn_height) || ($smallImg [1] <= $tn_height && $smallImg [0] == $tn_width)) {
					return "images/resized/" . $rzname;
				}
			}
			if (! file_exists ( $path . '/images/resized/' ) && ! mkdir ( $path . '/images/resized/', 0755 ))
				return '';
			$folders = explode ( '/', $image );
			$tmppath = $path . '/images/resized/';
			for($i = 0; $i < count ( $folders ) - 1; $i ++) {
				if (! file_exists ( $tmppath . $folders [$i] ) && ! mkdir ( $tmppath . $folders [$i], 0755 ))
					return '';
				$tmppath = $tmppath . $folders [$i] . '/';
			}
			switch ($ext) {
				case 'jpg' : // jpg
					$src = imagecreatefromjpeg ( JPATH_SITE . '/' . $image );
					break;
				case 'png' : // png
					$src = imagecreatefrompng ( JPATH_SITE . '/' . $image );
					break;
				case 'gif' : // gif
					$src = imagecreatefromgif ( JPATH_SITE . '/' . $image );
					break;
				default :
			}
			$dst = imagecreatetruecolor ( $tn_width, $tn_height );
			//imageantialias ($dst, true);
			if (function_exists ( 'imageantialias' ))
				imageantialias ( $dst, true );
			imagecopyresampled ( $dst, $src, 0, 0, 0, 0, $tn_width, $tn_height, $width, $height );
			imagejpeg ( $dst, $resized, 90 ); // write the thumbnail to cache as well...
			return "images/resized/" . $rzname;
		}
	function save($id) {
		$product =& JTable::getInstance('product', 'Table');

		$post = JRequest::get('post');

		// fix up special html fields

		$post['description'] = JRequest::getVar( 'description', '', 'post', 'string', JREQUEST_ALLOWRAW );
		$post['content'] = JRequest::getVar( 'content', '', 'post', 'string', JREQUEST_ALLOWRAW );
		$catid = JRequest::getVar('catid');		
		//echo('<pre>');print_r($catid);exit("  asdg");
		$post['categories'] = implode(',',$catid); 
		$get_img = JRequest::getVar("img_original");
		
		$path_img = $this->resizeImage($get_img,160,90);
		$post['img_thump'] = $path_img;
		//exit($path_img);
		//echo('<pre>');print_r($post);exit ('  asdgsdag');
		
		if (!$product->load($id) && JRequest::getVar('task')=='edit' ) {
			return JError::raiseWarning(500, $product->getError());

		}
		if (!$product->bind($post)) {
			return JError::raiseWarning(500, $product->getError());
		}
		
		return $product->store();
	}
	// Apply changes

	function apply($id) {		

		$product =& JTable::getInstance('product', 'Table');

		$post = JRequest::get('post');

		// fix up special html fields

		$post['description'] = JRequest::getVar( 'description', '', 'post', 'string', JREQUEST_ALLOWRAW );

		$post['content'] = JRequest::getVar( 'content', '', 'post', 'string', JREQUEST_ALLOWRAW );
		$catid = JRequest::getVar('categories');
		//echo "<pre>";print_r($catid);exit(count($catid));
		$post['categories'] = implode(',',$catid); 
		if (!$product->load($id)) {
			return JError::raiseWarning(500, $product->getError());
		}

		if (!$product->bind($post)) {

			return JError::raiseWarning(500, $product->getError());
		}

		return $product->store();

	}

	// Get edit Categories

	function &getEditcategories() {

		if (empty($this->_categories)){
			
			$query = 'SELECT id,parent,name FROM #__cate WHERE published = 1';
			
			$this->_categories = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
			
		} 
		return $this->_categories;

	}

	// Edit extension

	function &getEditdata() {

		$cid = JRequest::getVar('cid', array(0), 'method', 'array');
		
		//exit("alo" . $cid[0]);
		
		$this->_id = $cid[0];
		$product =& JTable::getInstance('product', 'Table');
		$product->load($this->_id);
		$product->params =  $this->getParams($product->params, $product->extension);
		//echo('<pre>');print_r($product);exit('asdf');
		return ($product);
	}
	
	function &getParams($params, $extension_name)	{

		$params	= new JParameter($params);
		if ($xml =& $this->_getXML($extension_name)) {

			if ($ps = & $xml->document->params) {

				foreach ($ps as $p)	{

					$params->setXML($p);

				}

			}

		}

		return $params;

	}
	// Get XML

	function &_getXML($extension_name) {

		if (!$this->_xml) {

			$xmlfile = JPATH_SITE.DS."administrator".DS."components".DS."com_product".DS."extensions".DS.$extension_name.'.xml';
			if (file_exists($xmlfile)) {

				$xml =& JFactory::getXMLParser('Simple');

				if ($xml->loadFile($xmlfile)) {

					$this->_xml = &$xml;

				}

			}

		}

		return $this->_xml;

	}

	

	// Get data about extensions

	function &getData() {

		if (empty($this->_data)){

			$query = $this->_buildViewQuery();
			//echo $query;
			$this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
			$tmp = $this->_data;
//			echo $query;echo "<pre>";print_r($tmp);exit("123");
			
		}

		return $this->_data;

	}

	

	// Get total extensions

	function &getTotal() {

		if (empty($this->_total)){

			$query = $this->_buildViewQuery();

			$this->_total = $this->_getListCount($query);

		}

		return $this->_total;

	}

	

	// Get pagination

	function &getPagination(){

		if (empty($this->_pagination)) {

			jimport('joomla.html.pagination');

			$this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));

		}

		return $this->_pagination;

	}		

	

	// Finally build query

	function _buildViewQuery() {

		$where = $this->_buildViewWhere();

		$query = 'SELECT t.*, c.name catname FROM #__product AS t LEFT JOIN #__cate AS c ON t.categories = c.id '.$where.' ORDER BY t.ordering';

		return $query;

	}

	

	// Filters function

	function _buildViewWhere() {

		global $mainframe, $option;

//		$filter_state		= $mainframe->getUserStateFromRequest($option.'filter_state',		'filter_state',		'',				'word');
//
//		$search				= $mainframe->getUserStateFromRequest($option.'search',				'search',			'',				'string');
//
//		$search				= JString::strtolower($search);

		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		//$search = $app->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$search				= $app->getUserStateFromRequest($option.'search',				'search',			'',				'string');
		
		$this->setState('filter.search', $search);

		$state = $app->getUserStateFromRequest($this->context.'.filter.state', 'filter_state', '', 'string');
		$this->setState('filter.state', $state);

		$where = array();

		if ($search) {
			$where[] = '(LOWER(t.title) LIKE '.$this->_db->Quote('%'.$search.'%').")";
		}

		if ($filter_state) {

			if ($filter_state == 'P') {

				$where[] = 't.published = 1';

			} elseif ($filter_state == 'U') {

				$where[] = 't.published = 0';

			}

		}

		$where = (count($where) ? ' WHERE '. implode(' AND ', $where) : '');
		//echo $where;
		return $where;

	}

}

?>