<?php 
defined('_JEXEC') or die('Truy cap khong hop le');

//require a basic controller
require_once JPATH_COMPONENT.DS.'controller.php';

//require a controller if required
if($controller = JRequest::getVar('controller')) {
	$path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';
	if(file_exists($path)) {
	    require_once($path);
	} else {
	    $controller = '';
	}
}
$classname = 'ProductController'.$controller;
// Initialize the controller
$controller = new $classname( );
// Perform the Request task
$controller->execute( JRequest::getCmd('task'));
$controller->redirect();

?>