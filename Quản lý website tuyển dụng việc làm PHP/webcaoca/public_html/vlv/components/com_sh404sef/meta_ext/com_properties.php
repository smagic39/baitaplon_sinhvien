<?php
defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );

global $Itemid;

global $shMosConfig_locale, $sh_LANG, $mainframe;

$sefConfig = & shRouter::shGetConfig();

$database =& JFactory::getDBO();
$type = JREQUEST::getVar('type', null);
$view = JREQUEST::getVar('view', null);
$catid = JREQUEST::getVar('catid', null);
$id = JREQUEST::getVar('id', null);
$limit = JREQUEST::getVar('limit', null);
$limitstart = JREQUEST::getVar('limitstart', null);
$layout = JREQUEST::getVar('layout', null);
$showall = JREQUEST::getVar('showall', null);
$format = JREQUEST::getVar('format', null);
$print = JREQUEST::getVar('print', null);
$tmpl = JREQUEST::getVar('tmpl', null);
$lang = JREQUEST::getVar('lang', null);
$mid = JREQUEST::getVar('mid', null);
$task = JREQUEST::getVar('task', null);
$idgoi_cv  = JREQUEST::getVar('idgoi_cv', null);
$id_seecker = JREQUEST::getVar('id_seecker', null);
$id_cv = JREQUEST::getVar('id_cv', null);
$click = JREQUEST::getVar('click', null);
$cid = JREQUEST::getVar('cid', null);
$cvs_id_product = JREQUEST::getVar('cvs_id_product', null);
$cvs_id_employ = JREQUEST::getVar('cvs_id_employ', null);
		    	
$shLangName = empty($lang) ? $shMosConfig_locale : shGetNameFromIsoCode( $lang);
$shLangIso = isset($lang) ? $lang : shGetIsoCodeFromName( $shMosConfig_locale);
$shLangIso = shLoadPluginLanguage( 'com_content', $shLangIso, 'COM_SH404SEF_CREATE_NEW');
//-------------------------------------------------------------

global 	$shCustomTitleTag, $shCustomDescriptionTag, $shCustomKeywordsTag,
$shCustomLangTag, $shCustomRobotsTag;

// add no follow to print pages
$shCustomRobotsTag = ($tmpl == 'component' && !empty( $print) ) ? 'noindex, nofollow' : $shCustomRobotsTag;

// calculate page title
$title= array();
switch ($view) {
	//sef search all work 
    case 'all' :
    	$title[] = 'Danh sách việc làm';
    	$shCustomTitleTag = JString::ltrim(implode( ' | ', $title), '/ | ');
    break;
    //sef post cv 
    case 'postcv' :
    	if ($task == 'list'){
    		$title[] = 'Danh sách CV ứng viên';
    	} else if($task == 'edit'){
    		$title[] = 'Sửa CV ứng viên';
    		if (!empty($mid) && !empty($id)){
    			$query  = "SELECT vitri, id, mid FROM #__properties_seekekjop" ;
			    $query .= "\n WHERE id=".$database->Quote($id);
			    $query .= "\n AND mid=".$database->Quote($mid);
			    $database->setQuery($query);
			    if (shTranslateUrl($option, $shLangName))
			    	$result = $database->loadObject();
			    else
			    	$result = $database->loadObject( false);
			    if (!empty($result)){
			    	$title[] = $result->id;
			    	$title[] = $result->vitri;
			    }else{
			    	$title[] = $id;
			    }
    		}
    		//&id=23&mid=84&task=
    	}else{
    		$title[] = 'Đăng hồ sơ';
    	}
    	$shCustomTitleTag = JString::ltrim(implode( ' | ', $title), '/ | ');
    break;
	case 'employpost' :
		if ($task == 'cv_seecker_apply'){
			if (!empty($cvs_id_product) && !empty($cvs_id_employ)) {
		    	$title[] = 'Xem ứng viên apply';
		    }
		}
		if ($task == 'cv_seecker_save') {
    		$query  = "SELECT id,name_packet,type_packet FROM #__properties_buypacket" ;
		    $query .= "\n WHERE id=".$database->Quote($idgoi_cv);
		    $database->setQuery( $query );
		    if (shTranslateUrl($option, $shLangName))
		    	$result = $database->loadObject();
		    else
		    	$result = $database->loadObject( false);
		    if (!empty($result)){
		    	$title[] = $result->type_packet.'-'.$result->name_packet;
		    }
		    $title[] = $idgoi_cv;
		    $title[] = 'Danh sách hồ sơ ứng viên đã chọn';
    	}
		if ($task == 'cv_seecker') {
    		$title[] = 'Xem hồ sơ ứng viên';
    	}
		if ($task == 'show_seecker') {
    		$title[] = 'Xem chi tiết hồ sơ ứng viên';
    		$title[] = $id_cv;
    		if (!empty($click))
    			$title[] = $click;
    		else
    			$title[] = '0';
    	}
		if ($task == 'jobapply') {
    		$title[] = 'Xem hồ sơ đã chọn';
    	}
		if ($task == 'buy_packetedit') {
    		$title[] = 'Sửa gói hồ sơ';
    	}
		if ($task == 'buy_packet') {
    		$title[] = 'Mua gói hồ sơ';
    	}
		if ($task == 'list_packet') {
    		$title[] = 'Danh sách gói hồ sơ';
    	}
    	if ($task == 'add') {
    		$title[] = 'Tạo mới việc làm';
    	}
		if ($task == 'edit') {
		    $query  = "SELECT name, id, type FROM #__properties_products" ;
		    $query .= "\n WHERE id=".$database->Quote($cid[0]);
		    $database->setQuery( $query );
		    if (shTranslateUrl($option, $shLangName))
		    	$result = $database->loadObject();
		    else
		    	$result = $database->loadObject( false);
	
		    if (!empty($result)){
		    	$title[] = $result->name;
		    	$title[] = $result->id;
		    	$query_cat  = "SELECT name, id FROM #__properties_type" ;
			    $query_cat .= "\n WHERE id=".$result->type;
			    $database->setQuery( $query_cat );
			    
			    if (shTranslateUrl($option, $shLangName))
			    	$result_cat = $database->loadObject();
			    else 
			    	$result_cat = $database->loadObject( false);
		    	
			    if (!empty($result_cat)){
			    	$title[] = $result_cat->name;
			    }else{
			    	$title[] = $result->type;
		    	}
		    }
		    else
		    	$title[] = $cid[0];
		    $title[] = 'Sửa việc làm';
	    }
	    $title[] = 'Nhà tuyển dụng';
	    $shCustomTitleTag = JString::ltrim(implode( ' | ', $title), '/ | ');
    break;
	//sef nha tuyen dung 
    case 'search_ntd' :
	    $title[] = 'Search-nha-tuyen-dung';
	    $shCustomTitleTag = JString::ltrim(implode( ' | ', $title), '/ | ');
    break;
    case 'employ' :
	    if ($task == 'add') {
	    	$title[] = 'Đăng ký nhà tuyển dụng';
	    }
		if ($task == 'edit') {
	    	$title[] = 'Sửa thông tin nhà tuyển dụng';
	    }
	    $shCustomTitleTag = JString::ltrim(implode( ' | ', $title), '/ | ');
    break;
	//sef thanh vien
    case 'seecker' :
	    if ($task == 'add') {
	    	$title[] = 'Đăng ký thành viên';
	    }
	    else if ($task=='edit' && !empty($mid)){
	    	$title[] = 'Sửa thông tin thành viên';
	    }
	    else if (!empty($mid)) {
     		$title[] = 'ứng viên';
	    }else{
	    	$title[] = 'CV ứng viên';
	    }
	    $shCustomTitleTag = JString::ltrim(implode( ' | ', $title), '/ | ');
    break;
    case 'search' :
	    $title[] = 'Search-viec-lam';
    break;
	case 'detail' :
	    if (!empty($type)) {
		      $query  = "SELECT name, id FROM #__properties_type" ;
		      $query .= "\n WHERE id=".$database->Quote($type);
		      $database->setQuery( $query );
		      if (shTranslateUrl($option, $shLangName))
		      		$result = $database->loadObject();
		      else 
		      		$result = $database->loadObject( false);
		      if (!empty($result))  {
		        	$title[] = $result->name;
		      }	else {
		        	$title[] = $detail;
		      }
	    }
	    $shCustomTitleTag = JString::ltrim(implode( ' | ', $title), '/ | ');   
    break;
	case 'chitiet' :
		if (!empty($id)) {
	        $q = 'SELECT id, name,type FROM #__properties_products WHERE id='.$database->Quote($id);
	        $database->setQuery($q);
	        if (shTranslateUrl($option, $shLangName)) // V 1.2.4.m
	        	$shTitle = $database->loadObject( );
	        else 
	        	$shTitle = $database->loadObject( false);
	        if ($shTitle) {
	        	$title[] = $shTitle->name;
	        	$query_cat  = "SELECT name, id FROM #__properties_type WHERE id=".$database->Quote($shTitle->type);
	        	$database->setQuery( $query_cat );
	        	
	        	if (shTranslateUrl($option, $shLangName)) // V 1.2.4.m
		        	$shTitle_cat = $database->loadObject( );
		        else 
		        	$shTitle_cat = $database->loadObject( false);
		        if ($shTitle_cat) {	
		        	$title[] = $shTitle_cat->name;
		        }else{
		        	$title[] = $shTitle->type;
		        }
	        }else{
	        	$title[] = $id;
	        }
      	}
		$shCustomTitleTag = JString::ltrim(implode( ' | ', $title), '/ | ');
	break;
	default:
    break;
}

?>
