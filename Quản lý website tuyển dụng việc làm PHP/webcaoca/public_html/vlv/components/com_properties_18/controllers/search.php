<?php
// ensure a valid entry point
defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.controller');
class JLORDCOREControllerSearch extends JController
{
	function __construct()
	{
		parent::__construct();
	}
	function display()
	{
		global $mainframe;
		$cid 	= JRequest::getVar('cid', array(0), 'post', 'array');
		$user 	= & JFactory::getUser(); 
		JArrayHelper::toInteger($cid, array(0));
		$mName 		= 'search';	
		$vName 		= 'search';	
		$vLayout 	= 'default';
		$option 	= JRequest::getVar('option');	
		$task = JRequest::getVar('task');
		switch ($task){
			default:
				$vName 		= 'search';	
				$vLayout 	= 'default';							
		}
		
		$document 	= JFactory::getDocument();
		$vType 		= $document->getType();		
		$view = $this->getView($vName, $vType);
		if ($model 	= &$this->getModel($mName)) {
			$view->setModel($model, true);
		}
		$view->setLayout($vLayout);		
		$view->display();		
	}
} // end class
?>