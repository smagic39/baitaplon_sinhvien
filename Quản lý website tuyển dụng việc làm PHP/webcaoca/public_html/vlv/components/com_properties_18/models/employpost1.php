<?php
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();
jimport('joomla.application.component.model');
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');
jimport('joomla.installer.helper');
jimport( 'joomla.application.component.view');
JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_properties'.DS.'tables'); 
//exit(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_properties'.DS.'tables');
class JlordCoreModelEmploypost extends JModel
{
	//var $_db = NULL;administrator/components/com_properties/tables
	var $_userid = NULL;
	var $_data = NULL;
	var $TableName = null;
	function getLastModif()
	{
	$TableName 	= 'profiles';
		 $query = ' SELECT id FROM #__properties_'.$TableName.' ORDER BY id desc LIMIT 1';
	 $this->_db->setQuery( $query );	
			$this->_data = $this->_db->loadResult();
	
	//print_r($this->_data);
	 return $this->_data;
	}
 	function store($data)
	{
	$row =& $this->getTable('profiles');
	/*if ($row)
		exit('1');
	else 
		exit('0');*/
		//echo "<pre>";print_r($row);exit;
		if (!$row->bind($data)) {
			$this->setError($this->_db->getErrorMsg());			
			return false;
		}		
		if (!$row->check()) {
			$this->setError($this->_db->getErrorMsg());			
			return false;
		}
		
		/*if (!$row->store()) {
			$this->setError( $this->_db->getErrorMsg() );
			return false;
		}*/
		//exit('456');
		if($TableName=='profiles'){
	//	$this->Notification();	
		}
		//exit('3');
		return true;
	}
	function getShowData(){
		global $mainframe, $option;
		$user = JFactory::getUser();
		//echo "<pre>";print_r($user);
		$agent_id = $user->get('id');
		$db 					= JFactory::getDBO();
		$option					= JRequest::getVar('option');
		
		$limit					= $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart				= $mainframe->getUserStateFromRequest( $option.'limitstart', 'limitstart', 0, 'int');		
		
		$filter_state			= $mainframe->getUserStateFromRequest( $option.'filter_state_usergr','filter_state','','word');
		$filter_order_post	= $mainframe->getUserStateFromRequest( $option.'filter_order_post','filter_order','p.id','cmd');
		$filter_order_Dir_post= $mainframe->getUserStateFromRequest( $option.'filter_order_Dir_post','filter_order_Dir','desc','word');
		
		$search 				= $mainframe->getUserStateFromRequest( $option.'search_post','search_post','','string');
		$search					= JString::strtolower( $search );

		$where = array();
		if ($agent_id > 1)
			$where[] 	= 'p.agent_id='.$agent_id;
		//where publish
		if ( $filter_state )
		{
			if ( $filter_state == 'P' )
			{
				$where[] = ' p.published = 1';
			}
			else if ($filter_state == 'U' )
			{
				$where[] = ' p.published = 0';
			}
		}
		if ($search)
		{
			$where[] 	= ' p.name LIKE'.$db->Quote( '%'.$db->getEscaped( $search, true ).'%', false );
		}
		$where 			= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		
		$orderby 		= ' ORDER BY '. $filter_order_post .' '. $filter_order_Dir_post;
		
		//$qry = "SELECT COUNT(*) FROM `#__timesheet_userlevel` $where";
		$qry = "SELECT count(*)
				FROM #__properties_products as p
				$where ";		
		$db->setQuery($qry);
		$total = $db->loadResult();
		jimport('joomla.html.pagination');
		$pageNav 		= new JPagination( $total, $limitstart, $limit );
				
		$qry = "SELECT p.*,ps.name as ps_name,pp.company as pp_company
				FROM #__properties_products as p
				INNER JOIN #__properties_state as ps ON p.sid = ps.id
				INNER JOIN #__properties_profiles as pp ON p.agent_id = pp.mid
				$where  $orderby ";
				//echo $qry;
		$db->setQuery($qry,$pageNav->limitstart, $pageNav->limit);		
		$rows = $db->loadObjectList();
		//echo $qry;echo "<pre>";print_r($rows);exit;
		if ($db->getErrorNum()){
			echo $db->stderr();
			return false;
		}		
		
		//table ordering
		$lists['order_Dir_post']	= $filter_order_Dir_post;
		$lists['order_post']		= $filter_order_post;
		$lists['state_usergr']	= JHTML::_('grid.state',  $filter_state);
		$lists['search_post'] 	= $search;
		
		$res 			= new stdClass();
		$res->lists		= $lists;
		$res->rows 		= $rows;
		$res->pageNav 	= $pageNav;
		return $res;		
	}
	function getEditData(){
		$db = & JFactory::getDBO();
		$task = JRequest::getVar('task');
		$user = JFactory::getUser();
		
		if($task == 'edit'){
			$cid 		= JRequest::getVar('cid', array(0), '', 'array');
			$id 		= $cid[0];
			$qry	= "	SELECT  *
				FROM #__properties_products  
				WHERE id=".$id;
			$db->setQuery($qry);
			$row 		= $db->loadObject();
			
			$postcatid_selected = $row->cid;
			$posttype_selected = $row->type;
			$postlocality_selected = $row->lid;
			$poststate_selected = $row->sid;
			$postcountry_selected = $row->cyid;
		} else {
			$postcatid_selected = '0';
			$posttype_selected = '0';
			$postlocality_selected = '0';
			$poststate_selected = '0';
			$postcountry_selected = '0';
		}
		$script = 'site';
		//get categories
		/*$post_catid = LibFunction::getCategories($postcatid_selected);	
		//get type
		$post_type = LibFunction::getType($script,$posttype_selected);
		//get categories
		$post_locality = LibFunction::getLocality($script,$postlocality_selected);	
		//get type
		$post_state = LibFunction::getState($script,$poststate_selected);
		//get categories
		$post_country = LibFunction::getCountry($script,$postcountry_selected);	*/
			
		$res				= new stdClass();
		$res->post_catid	= $post_catid;
		$res->post_type		= $post_type;
		$res->post_locality	= $post_locality;
		$res->post_state	= $post_state;
		$res->post_country	= $post_country;
		$res->row 	= $row;
		//$res->lists 		= $lists;
		return $res;
	}
}
?>