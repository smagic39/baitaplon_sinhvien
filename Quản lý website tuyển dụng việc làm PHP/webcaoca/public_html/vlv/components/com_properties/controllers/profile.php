<?php

// no direct access

defined('_JEXEC') or die('Restricted Access');

// Import JController

jimport('joomla.application.component.controller');

// Control Panel Controller Class

class JLORDCOREControllerProfile extends JController {			

	function __construct() 	{

		parent::__construct();

		$this->registerTask('unpublish', 'publish');
		$this->registerTask('nostate', 'state');	
		$this->registerTask('add', 'edit');
	}

	// View product

	function view() {

		JRequest::setVar('model', 'profile');

		JRequest::setVar('view', 'profile');

        JRequest::setVar('layout' , 'default');
        parent::display();
	}

	// Edit product

	function edit() {

		$model =& $this->getModel('profile');

		$view  = $this->getView('profile','edit');

		$view->setModel($model, true);

		$view->edit('edit');

	}

	// Delete product
	

	function delete() {

		$cid = JRequest::getVar('cid', array(0), 'method', 'array');

		$model = $this->getModel('profile');

		foreach ($cid as $id) {

			$model->delete($id);

		}
		// Return to extensions page
		$this->setRedirect('index.php?option=com_properties&view=profile', JTEXT::_('Deleted'));

	}
	// (un)Publish the product

	function publish() {
		$model = $this->getModel('profile');
		$cid = JRequest::getVar ('cid', array(0), 'method', 'array');
		//exit($cid[0]);
		foreach ($cid as $id) {

			$model->publish($id);

		}

		// Return to product page

		$this->setRedirect('index.php?option=com_properties&view=profile');

	}
	
	function state() {
		$model = $this->getModel('profile');
		$cid = JRequest::getVar ('cid', array(0), 'method', 'array');
		//exit($cid[0]);
		foreach ($cid as $id) {

			$model->state($id);

		}

		// Return to product page

		$this->setRedirect('index.php?option=com_properties&view=profile');

	}

	// Save changes

	function save() {

		$id = JRequest::getVar('id', NULL, 'method', 'int');		
		$model = $this->getModel('profile');
		if (!$model->save($id)) {
			//exit('sadfasd');
			return JError::raiseWarning(500, $url_record->getError());

		}

		// Return to extensions page

		$this->setRedirect('index.php?option=com_properties&view=profile', JTEXT::_('Saved'));

	}

	// Apply changes

	function apply() {

		$id = JRequest::getVar('id', 0, 'method', 'int');

		$model = $this->getModel('localweb');

		if (!$model->apply($id)) {

			return JError::raiseWarning(500, $url_record->getError());

		}

		// Return to extensions page

		$this->setRedirect('index.php?option=com_properties&controller=profile&task=edit&cid[]='.$id, JTEXT::_('Applied'));

	}

	// Cancel saving changes

	function cancel() {

		$this->setRedirect('index.php?option=com_properties&view=profile');

	}

}

?>