<?php
// no direct access
defined('_JEXEC') or die('Restricted access');

// Require the base controller
require_once JPATH_COMPONENT.DS.'controller.php';

// Get controller
if($controller = JRequest::getVar('controller')) {
	$path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';
	if(file_exists($path)) {
	    require_once($path);
	} else {
	    $controller = '';
	}
}
$classname = 'ProductController'.$controller;
$controller = new $classname();

$controller->execute( JRequest::getVar('task') );

// Redirect if set by the controller
$controller->redirect();
?>