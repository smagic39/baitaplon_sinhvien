<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php if(JPluginHelper::isEnabled('authentication', 'openid')) :
		$lang = &JFactory::getLanguage();
		$lang->load( 'plg_authentication_openid', JPATH_ADMINISTRATOR );
		$langScript = 	'var JLanguage = {};'.
						' JLanguage.WHAT_IS_OPENID = \''.JText::_( 'WHAT_IS_OPENID' ).'\';'.
						' JLanguage.LOGIN_WITH_OPENID = \''.JText::_( 'LOGIN_WITH_OPENID' ).'\';'.
						' JLanguage.NORMAL_LOGIN = \''.JText::_( 'NORMAL_LOGIN' ).'\';'.
						' var comlogin = 1;';
		$document = &JFactory::getDocument();
		$document->addScriptDeclaration( $langScript );
		JHTML::_('script', 'openid.js');
endif; ?>
<?php //echo"<pre>";print_r($this);exit;?>
<div class="box-center">
	<div class="css_dangnhap">
	<div class="info_dangnhap">
						<div class="box_tin_dn">
						<p> 
						Đăng ký dễ dàng chỉ với 3 bước <a href="<?php echo JRoute::_( 'index.php?option=com_user&view=register' ); ?>" title=" Đăng ký"><?php echo JText::_('Đăng Ký Ngay!')?></a></p>
						<p> 
								<strong>Sau khi tạo hồ sơ trên website, bạn có thể: </strong></p>
						<ul>
							<li><strong>Lựa chọn bảo mật</strong> - nhà tuyển dụng không thể xem thông tin hồ sơ của bạn </li>
							<li><strong>Trạng thái hồ sơ  </strong>- Kiểm tra trạng thái kích hoạt hồ sơ </li>
							<li><strong>Hỗ trợ bạn tìm việc</strong> - Thường xuyên gửi qua email của bạn những việc làm mới nhất </li>
							<li><strong>Ứng tuyển </strong> - Kiểm tra trạng thái hiển thị hồ sơ của bạn </li>
							<li><strong>Lưu thông tin tuyển dụng </strong> - Xem lại vào lần sau và ứng tuyển. </li>
						</ul>
						</div>
						
						</div>
						
<form action="<?php echo JRoute::_( 'index.php', true, $this->params->get('usesecure')); ?>" method="post" name="com-login" id="com-form-login" class="form_dangnhap">
<table>
	<tbody>
	<tr>
		<td class="title_dn"><?php echo JText::_('Username') ?></td>
		<td class="title_dn2"><input name="username" id="username" type="text" class="inputbox" alt="username" size="18" /></td>
	</tr>
	<tr>
		<td class="title_dn"><?php echo JText::_('Password') ?></td>
		<td class="title_dn2"><input type="password" id="passwd" name="passwd" class="inputbox" size="18" alt="password" /></td>
	</tr>
	<?php if(JPluginHelper::isEnabled('system', 'remember')) : ?>
	<tr>
		<td class="title_dn"></td>
		<td class="title_dn2">
		<input type="checkbox" id="remember" name="remember" class="inputbox" value="yes" alt="Remember Me" />
		<label for="remember"><?php echo JText::_('Remember me') ?></label>
		</td>
	</tr>
	<?php endif; ?>
	<tr>
								<td class="title_dn"></td>
								<td class="title_dn2">
								
								<input type="submit" name="Submit" value="<?php echo JText::_('Đăng nhập') ?>" id="" class="button">
								<input type="reset" name="" value="<?php echo JText::_('Hủy bỏ');?>" id="" class="button">
								</td>
							</tr>
	</tbody>
</table>
	<input type="hidden" name="option" value="com_user" />
	<input type="hidden" name="task" value="login" />
	<input type="hidden" name="return" value="<?php echo $this->return; ?>" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>
<div class="clear"></div>
	</div>
</div>