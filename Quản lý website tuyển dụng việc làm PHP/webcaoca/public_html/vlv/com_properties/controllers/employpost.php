<?php
// ensure a valid entry point
defined('_JEXEC') or die('Restricted Access');
jimport('joomla.application.component.controller');
jimport('joomla.application.helper');
//require_once(JApplicationHelper::getPath('html'));
JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_properties'.DS.'tables');

//JTable::addIncludePath( JPATH_COMPONENT_ADMINISTRATOR.DS.'tables' );
$id 	= JRequest::getVar('id', 0, 'get', 'int');
class JLORDCOREControllerEmploypost extends JController
{
	function __construct()
	{
		parent::__construct();
	}
	function display()
	{
		global $mainframe;
		$cid 	= JRequest::getVar('cid', array(0), 'post', 'array');
		$user 	= & JFactory::getUser(); 
		JArrayHelper::toInteger($cid, array(0));
		$mName 		= 'employpost';	
		$vName 		= 'employpost';	
		$vLayout 	= 'default';
		$option 	= JRequest::getVar('option');	
		$task = JRequest::getVar('task');
		switch ($task){
			case 'buy_packetedit':
				JRequest::setVar( 'hidemainmenu', 1 );				
				$vLayout = 'form_buypacketedit';
				break;
			case 'buy_packet':
				JRequest::setVar( 'hidemainmenu', 1 );				
				$vLayout = 'form_buypacket';
				break;
			case 'list_packet':
				JRequest::setVar( 'hidemainmenu', 1 );				
				$vLayout = 'list_packet';
				break;
			case 'editbuy_packet':
				$this->editProfilePacket();
				break;
			case 'editbuy_packetedit':
				$this->editProfilePacketEdit();
				break;
			case 'add':	
			case 'edit':
				JRequest::setVar( 'hidemainmenu', 1 );				
				$vLayout = 'form';
				break;
			case 'jobapply':
				JRequest::setVar( 'hidemainmenu', 1 );				
				$vLayout = 'job_apply';
				break;
			case 'cv_seecker':
				JRequest::setVar( 'hidemainmenu', 1 );				
				$vLayout = 'cv_seecker';
				break;	
			case 'cv_seecker_apply':
				JRequest::setVar( 'hidemainmenu', 1 );				
				$vLayout = 'cv_seecker_apply';
				break;
			case 'cv_seecker_save':
				JRequest::setVar( 'hidemainmenu', 1 );				
				$vLayout = 'cv_seecker_save';
				break;
			case 'show_seecker':
				JRequest::setVar( 'hidemainmenu', 1 );				
				$vLayout = 'show_seecker';
				break;
			case 'save':
			case 'apply':
				$this->saveemploypost();
				break;
			case 'unpublish':
			case 'publish':
				$this->publish();
				break;			
			case 'remove':
				$this->removePost();
				break;
			case 'cancel':
				$this->cancelpost();
				break;	
			default:
				$vName 		= 'employpost';	
				$vLayout 	= 'default';							
		}
		
		$document 	= JFactory::getDocument();
		$vType 		= $document->getType();		
		$view = $this->getView($vName, $vType);
		if ($model 	= &$this->getModel($mName)) {
			$view->setModel($model, true);
		}
		$view->setLayout($vLayout);		
		$view->display();		
	}
	function editProfilePacketEdit() {
		$db 	= JFactory::getDBO();
		$id 	= JRequest::getVar('cid');
		$userid	= JRequest::getVar('userid');
		$goi_cv = JRequest::getVar('goi_cv');
		//up date #__properties_buypacket
		$qry_buypacket = "update #__properties_buypacket set type_packet = $goi_cv where id = $id";
		$db->setQuery($qry_buypacket);
		if (!$db->query())
		{
			JError::raiseError(500, $db->getErrorMsg() );
		}
		//update #__properties_profiles
		$qry_profiles  = "update #__properties_profiles set goi_cv = $goi_cv where mid = $userid";
		$db->setQuery($qry_profiles);
		if (!$db->query())
		{
			JError::raiseError(500, $db->getErrorMsg() );
		}
		
		$link = 'index.php?option=com_properties&view=employpost&task=list_packet';
		$this->setRedirect($link, $msg);
		
	}
	function editProfilePacket() {
		$db = JFactory::getDBO();
		$post = JRequest::get('post');
		
		// insert into vao bang jos_properties_buypacket
		$query = 'INSERT INTO #__properties_buypacket (id,agent_id,name_packet,type_packet,start_datebuy,end_datebuy,published,checkbuy)'
				.'VALUES ("","'. $post['userid'].'","'. $post['goicv'].'","'. $post['goi_cv'].'","'. date('Y-m-d').'","","0","3")';
		$db->setQuery( $query );
		if (!$db->query())
		{
			JError::raiseError(500, $db->getErrorMsg() );
		}// end insert into
		
		//lay id cua cai goi vua insert into vao bang profiles
		$qry = "select id from #__properties_buypacket order by id desc";
		$db->setQuery($qry);
		$idmax = $db->loadObjectList();
		//echo $idmax[0]->id;		echo "<pre>";print_r($idmax);exit;
		//update vao bang jos_properties_profiles
		$query1 = 'UPDATE #__properties_profiles'
		. ' SET gcviec = "' . $post['gcviec']
		. '" ,goicv = "' . $post['goicv']
		. '" ,goi_cv = "' . $post['goi_cv']
		. '" ,idgoi_cv = "' . $idmax[0]->id
		. '" WHERE mid =  '. $post['userid'] .' '
		;
		//exit($query);
		$db->setQuery( $query1 );
		if (!$db->query())
		{
			JError::raiseError(500, $db->getErrorMsg());
		}// end update
		
		//exit($query);
		$link = 'index.php?option=com_properties&view=employpost&task=list_packet';
		$this->setRedirect($link, $msg);	
		//echo "<pre>";print_r($post);exit;
	}
	
	function publish(){	
		$cid		= JRequest::getVar( 'cid', array(), '', 'array' );
		$this->publish	= ( $this->getTask() == 'publish' ? 1 : 0 );		
	
		JArrayHelper::toInteger($cid);
		if (count( $cid ) < 1)		{
			$action = $publish ? 'publish' : 'unpublish';		
			JError::raiseError(500, JText::_( 'Select an item to' .$action, true ) );
		}
		$this->cids = implode( ',', $cid );
		
		$query = 'UPDATE #__properties_products'
		. ' SET published = ' . (int) $this->publish
		. ' WHERE id IN ( '. $this->cids .' )'		
		;    
		
		$db 	=& JFactory::getDBO();
		$db->setQuery( $query );
		if (!$db->query())
		{
			JError::raiseError(500, $db->getErrorMsg() );
		}
		$link = 'index.php?option=com_properties&view=employpost';
		$this->setRedirect($link, $msg);		
	}
	function removePost(){
		global $mainframe;
		$cid 	= JRequest::getVar('cid', array(), '', 'array');
		$db 	= &JFactory::getDBO();
		$t_cid 	= count($cid);  
		//exit($cid[0]." = 0");
		if($t_cid){
			$cids 	= implode(',', $cid);
			$query 	= "DELETE FROM `#__properties_products` WHERE `id` IN ($cids)";
			$db->setQuery($query);
			$msg = "Deleted ($t_cid) items";
			if (!$db->query()) {
				echo "<script> alert('".$db->getErrorMsg()."');
				window.history.go(-1); </script>\n";
			}
		}
		$mainframe->redirect('index.php?option=com_properties&view=employpost',$msg);
	}
	function cancelpost(){
		global $mainframe;
		$db 	= &JFactory::getDBO();
		$option = JRequest::getVar('option');
		// Initialize variables
		$mainframe->redirect('index.php?option='.$option.'&view=employpost');
	}
	//send mail
	function char_makerand ()
	{
		$charset 	 = "abcdefghijklmnopqrstuvwxyz";
		$charset 	.= "0123456789";
		$char = $charset[(mt_rand( 0,(strlen( $charset )-1)) )];
		return $char;
	}
	
	function str_makerand()
	{
	  	$key = "";
	  	for ( $i=0; $i < 50; $i++ ) {
	   		$key .= char_makerand();
	  	}
	  	return $key;
	}
	function saveemploypost(){
		global $mainframe;
		JRequest::checkToken() or jexit('Invalid Token');
		$db		=& JFactory::getDBO();
		$row	=& JTable::getInstance('products', 'Table');

		$post   = JRequest::get('post');
		
		$db 	=& JFactory::getDBO();
		$text 	= JRequest::getVar( 'text', '', 'post', 'string', JREQUEST_ALLOWRAW );
		$text			= str_replace( '<br>', '<br />', $text );		
		$post['text'] 	= $text;

		$description = JRequest::getVar( 'description', '', 'post', 'string', JREQUEST_ALLOWRAW );
		$description		= str_replace( '<br>', '<br />', $description );		
		$post['description'] = $description;

		$quyenloi = JRequest::getVar( 'quyenloi', '', 'post', 'string', JREQUEST_ALLOWRAW );
		$quyenloi		= str_replace( '<br>', '<br />', $quyenloi );		
		$post['quyenloi'] = $quyenloi;
		
		$userid = JFactory::getUser();
		//echo $userid->get('gid');
		$post['agent_id'] = $userid->get('id');
		if (!$row->bind( $post )) {
		JError::raiseError(500, $row->getError() );
		}
		if (!$row->check()) {
			JError::raiseError(500, $row->getError() );
		}
		
		// save the changes
		if (!$row->store()) {
			JError::raiseError(500, $row->getError() );
			
		}
		$row->checkin();
//echo "<pre>";print_r($post);exit;
		$msg = JText::_('Save success !');
		switch (JRequest::getCmd( 'task' ))
		{
			case 'save':
				$this->setRedirect('index.php?option=com_properties&view=employpost',$msg);	
			break;		
		}
		//$this->setMessage( JText::_( $msg ) );
	}
} // end class
?>