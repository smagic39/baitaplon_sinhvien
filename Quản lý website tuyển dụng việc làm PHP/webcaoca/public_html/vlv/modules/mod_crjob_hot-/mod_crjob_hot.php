<?php

// no direct access
defined('_JEXEC') or die('Restricted access');

// Include the syndicate functions only once
require_once (dirname(__FILE__).DS.'helper.php');

$list = modcrjob_hotHelper::getlists($params);
$count=  modcrjob_hotHelper::getTotal();

require(JModuleHelper::getLayoutPath('mod_crjob_hot'));
?>