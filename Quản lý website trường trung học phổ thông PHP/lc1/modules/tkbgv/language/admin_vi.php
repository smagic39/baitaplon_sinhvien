<?php

/**
 * @Author GaNguCay (gangucay@gmail.com)
 * @copyright Freeware
 * @createdate 11/08/2010
 */

 if (! defined('NV_ADMIN') or ! defined('NV_MAINFILE')){
 die('Stop!!!');
}
$lang_translator['author'] ="Trần Thế Vinh (gangucay@gmail.com)";
$lang_translator['createdate'] ="18/07/2010, 15:22";
$lang_translator['copyright'] ="Freeware";
$lang_translator['info'] ="";
$lang_translator['langtype'] ="lang_module";

$lang_module['module_info'] = "Thông tin";
#config.php
$lang_module['config'] = "Cấu hình TKB GV";
$lang_module['config_title'] = "Cấu hình cho TKB GV";
$lang_module['add_success']="Cấu hình cho TKB thành công.";
$lang_module['stt'] = "STT";
$lang_module['tento'] = "Tên tổ";
$lang_module['import'] = "Import TKB GV";
$lang_module['import_cat'] = "Import danh sách GV";
$lang_module['import_to'] = "Import danh sách các tổ";
$lang_module['import_success'] = "Import danh sách TKB giáo viên thành công.";
$lang_module['import_success_cat'] = "Import danh sách giáo viên thành công.";
$lang_module['import_success_to'] = "Import danh sách các tổ thành công";
$lang_module['del'] = "Xoá dữ liệu";
$lang_module['del_gv'] = "Xoá dữ liệu TKB giáo viên";
$lang_module['del_cat'] = "Xoá danh sách giáo viên";
$lang_module['del_to'] = "Xoá danh sách các tổ";
$lang_module['del_success']="Xoá dữ liệu TKB giáo viên thành công.";
$lang_module['del_error']="Đã có lỗi xảy ra trong quá trình xoá TKB giáo viên. Hãy kiểm tra CSDL đã được tạo chưa.";
$lang_module['del_success_cat']="Xoá dữ liệu về danh sách giáo viên thành công.";
$lang_module['del_error_cat']="Đã có lỗi xảy ra trong quá trình xoá danh sách giáo viên. Hãy kiểm tra CSDL đã được tạo chưa.";
$lang_module['del_success_to']="Xoá dữ liệu về danh sách tổ thành công.";
$lang_module['del_error_to']="Đã có lỗi xảy ra trong quá trình xoá danh sách các tổ. Hãy kiểm tra CSDL đã được tạo chưa.";
$lang_module['delyes']="Xoá";
$lang_module['delno']="Bỏ qua";
$lang_module['dslop'] = "DANH SÁCH GIÁO VIÊN";
$lang_module['gv'] = "Giáo viên";
$lang_module['quanli'] = "Quản lí TKB GV";
$lang_module['quanli_cat'] = "Quản lí DS giáo viên";
$lang_module['quanli_to'] = "Quản lí DS tổ";
$lang_module['quanli_hd'] = "DANH SÁCH CÁC GIÁO VIÊN CÓ THỜI KHOÁ BIỂU";
$lang_module['quanli_hd_cat'] = "DANH SÁCH GIÁO VIÊN";
$lang_module['quanli_hd_to'] = "DANH SÁCH CÁC TỔ";
$lang_module['tieude'] = "Tiêu đề của TKB";
$lang_module['vaohocAM'] = "Giờ vào học buổi sáng";
$lang_module['vaohocPM'] = "Giờ vào học buổi chiều";
$lang_module['canhbaodel'] = "Chức năng này sẽ xoá hết dữ liệu liên quan. Bạn hãy chắc chắn trước khi quyết định.";
$lang_module['luuyimp'] = "Chọn file dữ liệu TKB giáo viên (file mẫu tkbgv.xml)";
$lang_module['luuyimp_cat'] = "Chọn file dữ liệu danh sách giáo viên (file mẫu tkbgv_cat.xml)";
$lang_module['luuyimp_to'] = "Chọn file dữ liệu danh sách các tổ (file mẫu tkbgv_to.xml)";
$lang_module['dsgv'] = "Danh sách TKB giáo viên";
$lang_module['copyright_info'] = "Phiên bản tra cứu TKB giáo viên dành cho hệ thống NukeViet 3.0. Freeware";
$lang_module['quanli_err'] = "Đã có lỗi xảy ra, hãy kiểm tra CSDL đã có chưa hoặc CSDL chưa có dữ liệu";
$lang_module['line'] = "Tổng số dòng trong CSDL: ";
$lang_module['them'] = "Thêm mới: ";
$lang_module['sua'] = "Cập nhật: ";
$lang_module['thuocto']="Thuộc tổ";
$lang_module['add_to']="Thêm tổ";
$lang_module['edit_to']="Sửa tên tổ";
$lang_module['toid']="Tổ ID";
$lang_module['gvid'] = "Giáo viên ID";
$lang_module['cnto'] = "Bạn chưa nhập tên tổ";
$lang_module['dacoto'] = "Tên tổ đã tồn tại";
$lang_module['dacogv'] = "Tên giáo viên bạn thêm đã tồn tại";
$lang_module['dacogvedit'] = "Tên giáo viên bạn sửa đã tồn tại";
$lang_module['usto'] = "Đã cập nhật tổ thành công";
$lang_module['usgv'] = "Đã thêm giáo viên mới thành công";
$lang_module['usgvedit'] = "Đã cập nhật giáo viên thành công";
$lang_module['isto'] = "Thêm tổ mới thành công";
$lang_module['isgv'] = "Thêm giáo viên mới thành công";
$lang_module['lkptto'] = "Có liên kết phụ thuộc, hãy xoá trước khi xoá tổ.";
$lang_module['lkptgv'] = "Có liên kết phụ thuộc, hãy xoá trước khi xoá tên giáo viên.";
$lang_module['delsc'] = "Đã xoá tổ thành công.";
$lang_module['delscgv'] = "Đã xoá giáo viên thành công.";
$lang_module['delsctkbgv'] = "Đã xoá giáo viên thành công.";
$lang_module['edit_tkb'] = "Sửa TKB";
$lang_module['edit_tkb_su'] = "Cập nhật TKB thành công.";
$lang_module['add_cat']="Thêm giáo viên";
$lang_module['edit_cat']="Sửa giáo viên";
$lang_module['thuchien']="Thực hiện";
$lang_module['ccto']="Bạn chưa chọn tổ";
$lang_module['update']="Cập nhật";

?>