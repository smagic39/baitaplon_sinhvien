<?php

/**
 * @Author GaNguCay (gangucay@gmail.com)
 * @copyright Freeware
 * @createdate 11/08/2010
 */
if ( ! defined( 'NV_IS_FILE_ADMIN' ) ) die( 'Stop!!!' );
$page_title = $lang_module['quanli'];
	$delid = $nv_Request->get_int ( 'delid', 'get' );
	$tbupdate="";	//Thong bao cap nhat du lieu
	// Update  lai CSDL
	if ($nv_Request->isset_request ( 'submit', 'post' )) {
		For ($i=0; $i<10; $i++){
			$sql_up = "UPDATE `" . NV_PREFIXLANG . "_" . $module_data . "` SET `thu2`='".filter_text_input('monhoc_'.$i.'_2','post','')."', `thu3`='".filter_text_input('monhoc_'.$i.'_3','post','')."', `thu4`='".filter_text_input('monhoc_'.$i.'_4','post','')."', `thu5`='".filter_text_input('monhoc_'.$i.'_5','post','')."', `thu6`='".filter_text_input('monhoc_'.$i.'_6','post','')."', `thu7`='".filter_text_input('monhoc_'.$i.'_7','post','')."' WHERE `id`=".filter_text_input('monhoc_'.$i.'_0','post','')."";
			$db->sql_query($sql_up) or die ('Đã có lỗi xảy trong quá trình cập nhật dữ liệu.');
		}
	$tbupdate .= "<div class=\"quote\" style=\"width:780px;\">\n";
    $tbupdate .= "<blockquote class=\"error\"><span>" . $lang_module['edit_tkb_su'] . "</span></blockquote>\n";
    $tbupdate .= "</div>\n";
	}//End IF
	if($delid!=0){
		//Xoa TKB giao vien
		$db->sql_query("DELETE FROM`" . NV_PREFIXLANG . "_" . $module_data . "` WHERE `gvid`='".$delid."'");
	}//End IF

$sql = "SELECT DISTINCT " . NV_PREFIXLANG . "_" . $module_data . "_cat.gvid," . NV_PREFIXLANG . "_" . $module_data . "_cat.tengv," . NV_PREFIXLANG . "_" . $module_data . "_to.tento FROM " . NV_PREFIXLANG . "_" . $module_data . "_cat," . NV_PREFIXLANG . "_" . $module_data . "," . NV_PREFIXLANG . "_" . $module_data . "_to WHERE " . NV_PREFIXLANG . "_" . $module_data . "_cat.gvid=" . NV_PREFIXLANG . "_" . $module_data . ".gvid AND " . NV_PREFIXLANG . "_" . $module_data . "_cat.toid=" . NV_PREFIXLANG . "_" . $module_data . "_to.toid ORDER BY " . NV_PREFIXLANG . "_" . $module_data . "_cat.gvid";

$result = $db->sql_query( $sql ) or die ('Đã có lỗi xảy ra trong lệnh truy vấn CSDL.<br />');
$num = $db->sql_numrows( $result );
if ( $num > 0 )
{
	$contents .= "<div>";
    $contents .= "<form name=\"deltkb\" action=\"\" method=\"post\">";
    $contents .= "<table summary=\"\" class=\"tab1\">\n";
    $contents .= "<td>";
    $contents .= "<center><b><font color=blue size=\"3\">" . $lang_module['quanli_hd'] . "</font></b></center>";
    $contents .= "</td>\n";
    $contents .= "</table>";
	$contents .= "</form>";
    $contents .= "</div>";
    // Hien thi thong bao neu co cap nhat du lieu
	if ($nv_Request->isset_request ( 'submit', 'post' )) {
		$contents .="".$tbupdate."";
	}
	// Hien thi thong bao neu co xoa du lieu TKB
	if($delid!=0){
		$contents .= "<div class=\"quote\" style=\"width:780px;\">\n";
    	$contents .= "<blockquote class=\"error\"><span>" . $lang_module['delsctkbgv'] . "</span></blockquote>\n";
    	$contents .= "</div>\n";
	}//End IF
	$contents .= "<table class=\"tab1\">\n";
    $contents .= "<thead>\n";
    $contents .= "<tr>\n";
    $contents .= "<td align=\"center\">" . $lang_module['stt'] . "</td>\n";
    $contents .= "<td align=\"center\">" . $lang_module['gvid'] . "</td>\n";
    $contents .= "<td align=\"center\">" . $lang_module['gv'] . "</td>\n";
    $contents .= "<td align=\"center\">" . $lang_module['thuocto'] . "</td>\n";
    $contents .= "<td align=\"center\">" . $lang_module['quanli'] . "</td>\n";
    $contents .= "</tr>\n";
    $contents .= "</thead>\n";
    $a = 0;
    $b=1;
    while ( $row = $db->sql_fetchrow( $result ) )
    {
        $class = ( $a % 2 ) ? " class=\"second\"" : "";
        $contents .= "<tbody" . $class . ">\n";
        $contents .= "<tr>\n";
        $contents .= "<td align=\"center\">" . $b. "</td>\n";
        $contents .= "<td align=\"center\">" . $row[0] . "</td>\n";
        $contents .= "<td>" . $row[1] . "</td>\n";
        $contents .= "<td align=\"center\">" . $row[2] . "</td>\n";
        $contents .= "<td align=\"center\"><span class=\"edit_icon\"><a href=\"index.php?" . NV_NAME_VARIABLE . "=" . $module_name . "&" . NV_OP_VARIABLE . "=edit_tkb&amp;gvid=" . $row[0] . "\">" . $lang_global['edit'] . "</a></span>\n";
        $contents .= "&nbsp;-&nbsp;<span class=\"delete_icon\"><a href=\"index.php?" . NV_NAME_VARIABLE . "=" . $module_name . "&" . NV_OP_VARIABLE . "=quanli&amp;delid=" . $row[0] . "\">" . $lang_global['delete'] . "</a></span></td>\n";
        $contents .= "</tr>\n";
        $contents .= "</tbody>\n";
        $a ++;
        $b ++;
    }
    $contents .= "</table>\n";
    include ( NV_ROOTDIR . "/includes/header.php" );
    echo nv_admin_theme( $contents );
    include ( NV_ROOTDIR . "/includes/footer.php" );
}
else
{
	$contents .= "<div id='edit'></div>\n";
	$contents .= "<div class=\"quote\" style=\"width:780px;\">\n";
	$contents .= "<blockquote class='error'><span id='message'>" . $lang_module ['quanli_err'] . "</span></blockquote>\n";
	$contents .= "</div>\n";
	include (NV_ROOTDIR . "/includes/header.php");
	echo nv_admin_theme ( $contents);
	include (NV_ROOTDIR . "/includes/footer.php");
}
?>