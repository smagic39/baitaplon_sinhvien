<?php

/**
 * @Author GaNguCay (gangucay@gmail.com)
 * @copyright Freeware
 * @createdate 11/08/2010
 */

if ( ! defined( 'NV_ADMIN' ) or ! defined( 'NV_MAINFILE' ) ) die( 'Stop!!!' );

$module_version = array( 
    "name" => "Tkbgv", //
    "modfuncs" => "main", //
    "is_sysmod" => 0, //
    "virtual" => 1, //
    "version" => "1.0.0", //
    "date" => "Sun, 18 Jul 2010 16:13:15 GMT", //
    "author" => "Trần Thế Vinh (gangucay@gmail.com)", //
    "note" => "", //
    "uploads_dir" => array( 
        $module_name 
    ) 
);

?>