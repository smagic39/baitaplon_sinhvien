<?php

/**
 * @Author Pham Ba Tuan Chung (thptso1laocai@gmail.com)
 * @createdate 05/09/2011
 */

if ( ! defined( 'NV_ADMIN' ) or ! defined( 'NV_MAINFILE' ) ) die( 'Stop!!!' );

$module_version = array( 
    "name" => "Tradiem", //
    "modfuncs" => "main,diemthitn,tuyensinh10", //
	"submenu" => "main,diemthitn,tuyensinh10", //
    "is_sysmod" => 0, //
    "virtual" => 1, //
    "version" => "1.0.0", //
    "date" => "Sun, 05 Sep 2011 16:13:15 GMT", //
    "author" => "Pham Ba Tuan Chung (thptso1laocai@gmail.com)", //
    "note" => "", //
    "uploads_dir" => array($module_name) 
);

?>