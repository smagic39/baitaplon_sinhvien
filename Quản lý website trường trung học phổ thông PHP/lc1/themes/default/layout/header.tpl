﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        {THEME_PAGE_TITLE} {THEME_META_TAGS}
        <link rel="icon" href="{NV_BASE_SITEURL}favicon.ico" type="image/vnd.microsoft.icon" />
        <link rel="shortcut icon" href="{NV_BASE_SITEURL}favicon.ico" type="image/vnd.microsoft.icon" />
        <link rel="stylesheet" type="text/css" href="{NV_BASE_SITEURL}themes/{TEMPLATE}/css/reset.css" />
        <link rel="stylesheet" type="text/css" href="{NV_BASE_SITEURL}themes/{TEMPLATE}/css/template.css" />
        <link rel="stylesheet" type="text/css" href="{NV_BASE_SITEURL}themes/{TEMPLATE}/css/icons.css" />

        {THEME_CSS}
        {THEME_SITE_RSS}

        <!--[if IE 6]>
            <link rel="stylesheet" type="text/css" href="{NV_BASE_SITEURL}themes/{TEMPLATE}/css/ie6.css" />
            <script type="text/javascript" src="{NV_BASE_SITEURL}js/fix-png-ie6.js"></script>
            <script type="text/javascript">
                DD_belatedPNG.fix('#');
            </script>
        <![endif]-->
        {THEME_MY_HEAD}
    </head>
    <body>
    	<div id="language">
		<div id="container">
        <div id="header">
	 <embed
              src="http://imgfree.21cn.com/free/flash/1.swf"
              type=application/x-shockwave-flash quality="high" menu="false"
              wmode="transparent" style="height:182px;width:1160px;">
    </embed>
                <!-- BEGIN: language -->
	                <div class="language">
	                	Language:
	                    <select name="lang">
	                        <!-- BEGIN: langitem -->
	                        	<option value="{LANGSITEURL}" title="{SELECTLANGSITE}">{LANGSITENAME}</option>
	                        <!-- END: langitem -->
	                        <!-- BEGIN: langcuritem -->
	                        	<option value="{LANGSITEURL}" title="{SELECTLANGSITE}" selected="selected">{LANGSITENAME}</option>
	                        <!-- END: langcuritem -->
	                    </select>
	                    <script type="text/javascript">
	                        $(function(){
	                            $("select[name=lang]").change(function(){
	                                var reurl = $("select[name=lang]").val();
	                                document.location = reurl;
	                            });
	                        });
	                    </script>
	                </div>
                <!-- END: language -->
            </div>
			[MENU_SITE]
			<div id="message">
            </div>
            {THEME_ERROR_INFO}