<?php

/**
 * @Project NUKEVIET 3.0
 * @Author VINADES.,JSC (contact@vinades.vn)
 * @Copyright (C) 2010 VINADES.,JSC. All rights reserved
 * @Createdate Wed, 21 Sep 2011 07:03:06 GMT
 */

if ( ! defined( 'NV_MAINFILE' ) )
{
    die( 'Stop!!!' );
}

$global_config['admfirewall'] = 0;
$global_config['admin_theme'] = "admin_full";
$global_config['allowloginchange'] = 0;
$global_config['allowmailchange'] = 1;
$global_config['allowquestion'] = 1;
$global_config['allowuserlogin'] = 1;
$global_config['allowuserpublic'] = 0;
$global_config['allowuserreg'] = 1;
$global_config['allow_adminlangs'] = "cs,en,fr,tr,vi";
$global_config['allow_sitelangs'] = "vi";
$global_config['authors_detail_main'] = 0;
$global_config['autocheckupdate'] = 1;
$global_config['autoupdatetime'] = 24;
$global_config['block_admin_ip'] = 0;
$global_config['closed_site'] = 0;
$global_config['cookie_prefix'] = "nv3c_Gdbap";
$global_config['date_pattern'] = "l, d-m-Y";
$global_config['dump_autobackup'] = 1;
$global_config['dump_backup_day'] = 30;
$global_config['dump_backup_ext'] = "gz";
$global_config['error_send_email'] = "thptso1laocai@gmail.com";
$global_config['file_allowed_ext'] = "adobe,application,archives,audio,documents,flash,images,real,video";
$global_config['forbid_extensions'] = "php";
$global_config['forbid_mimes'] = "";
$global_config['ftp_check_login'] = 0;
$global_config['ftp_path'] = "/";
$global_config['ftp_port'] = 21;
$global_config['ftp_server'] = "localhost";
$global_config['ftp_user_name'] = "";
$global_config['ftp_user_pass'] = "";
$global_config['getloadavg'] = 0;
$global_config['gfx_chk'] = 3;
$global_config['googleAnalyticsID'] = "";
$global_config['googleAnalyticsSetDomainName'] = 0;
$global_config['gzip_method'] = 1;
$global_config['is_url_rewrite'] = 1;
$global_config['is_user_forum'] = 0;
$global_config['lang_multi'] = 0;
$global_config['mailer_mode'] = "";
$global_config['my_domains'] = "localhost,thptlaocai1.org";
$global_config['nv_max_size'] = 2097152;
$global_config['online_upd'] = 1;
$global_config['openid_mode'] = 1;
$global_config['openid_servers'] = "yahoo,google,myopenid";
$global_config['optActive'] = 1;
$global_config['proxy_blocker'] = 0;
$global_config['read_type'] = 0;
$global_config['revision'] = 1203;
$global_config['rewrite_endurl'] = "/";
$global_config['rewrite_optional'] = 0;
$global_config['session_prefix'] = "nv3s_Gkb5ry";
$global_config['site_email'] = "thptso1laocai@gmail.com";
$global_config['site_keywords'] = "Nukeviet, portal, mysql, php";
$global_config['site_lang'] = "vi";
$global_config['site_phone'] = "";
$global_config['site_timezone'] = "Asia/Bangkok";
$global_config['smtp_host'] = "smtp.gmail.com";
$global_config['smtp_password'] = "userpass";
$global_config['smtp_port'] = 465;
$global_config['smtp_ssl'] = 1;
$global_config['smtp_username'] = "user@gmail.com";
$global_config['spadmin_add_admin'] = 1;
$global_config['statistic'] = 1;
$global_config['str_referer_blocker'] = 0;
$global_config['time_pattern'] = "H:i";
$global_config['update_revision_lang_mode'] = 1;
$global_config['upload_checking_mode'] = "strong";
$global_config['useactivate'] = 2;
$global_config['version'] = "3.2.00";

?>