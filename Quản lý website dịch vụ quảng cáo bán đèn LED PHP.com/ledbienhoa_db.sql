-- MySQL dump 10.13  Distrib 5.1.62, for unknown-linux-gnu (x86_64)
--
-- Host: localhost    Database: ledbienhoa_db
-- ------------------------------------------------------
-- Server version	5.1.62

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `count`
--

DROP TABLE IF EXISTS `count`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `count` (
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `count`
--

LOCK TABLES `count` WRITE;
/*!40000 ALTER TABLE `count` DISABLE KEYS */;
INSERT INTO `count` VALUES (519);
/*!40000 ALTER TABLE `count` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nav`
--

DROP TABLE IF EXISTS `nav`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nav` (
  `id` char(100) NOT NULL,
  `title` varchar(150) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nav`
--

LOCK TABLES `nav` WRITE;
/*!40000 ALTER TABLE `nav` DISABLE KEYS */;
INSERT INTO `nav` VALUES ('','TRANG CHỦ',1,1),('gioi-thieu','GIỚI THIỆU',1,2),('san-pham','SẢN PHẨM',1,3),('tin-tuc','TIN TỨC',1,4),('du-an','DỰ ÁN',0,5),('dich-vu','DỊCH VỤ',0,6),('lien-he','LIÊN HỆ',1,7),('bang-gia','BẢNG GIÁ',1,5);
/*!40000 ALTER TABLE `nav` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` char(150) NOT NULL,
  `title` varchar(200) NOT NULL,
  `type` char(100) NOT NULL,
  `image` char(250) NOT NULL,
  `summary` text NOT NULL,
  `content` text NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `dateupdate` date NOT NULL,
  `timeupdate` time NOT NULL,
  `order` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES ('bang-led-cong-ty-tin-hoc-mai-phuong','Bảng led Công ty Tin Học Mai Phương','news','bang-led-cong-ty-tin-hoc-mai-phuonghinh-anh048.jpg','TVO thi công bảng led cho Công ty Tin Học Mai Phương','<p>\n	Bảng led C&ocirc;ng ty Tin Học Mai Phương</p>\n<p>\n	Chiều d&agrave;i: 4,16m&nbsp;</p>\n<p>\n	Chiều rộng: 0,40 m</p>\n<p>\n	Số lượng : 2 tấm</p>\n<p>\n	Tổng chiều d&agrave;i: 8,32m</p>\n<p>\n	<iframe allowfullscreen=\"\" frameborder=\"0\" height=\"315\" src=\"http://www.youtube.com/embed/24E39Bj7UPs\" width=\"420\"></iframe></p>\n','2012-08-17','08:20:39','0000-00-00','00:00:00',0,0),('bang-led-cong-ty-cuong-hung-phat','Bảng led Công ty Cường Hưng Phát','news','default.jpg','TVO thi công bảng led cho Công ty Cường Hưng Phát','<p>\n	Bảng led C&ocirc;ng ty TNHH Cường Hưng Ph&aacute;t</p>\n<p>\n	Chiều d&agrave;i: 2,4 m</p>\n<p>\n	Chiều rộng: 0,40 m</p>\n<p>\n	Số lượng : 2 tấm</p>\n<iframe width=\"420\" height=\"315\" src=\"http://www.youtube.com/embed/v7TCmpEFnx4\" frameborder=\"0\" allowfullscreen></iframe>','2012-08-20','01:59:17','0000-00-00','00:00:00',0,0);
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `online`
--

DROP TABLE IF EXISTS `online`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `online` (
  `id` char(70) NOT NULL,
  `time` int(11) NOT NULL,
  `ip` char(20) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `online`
--

LOCK TABLES `online` WRITE;
/*!40000 ALTER TABLE `online` DISABLE KEYS */;
INSERT INTO `online` VALUES ('36948b1fbe5091ca6f20d26dcb521e73',1346000223,'203.81.11.241',' - NA -  -  - KR - Korea, Republic of'),('3aeac950cdbf22767a47573ca6d98e3d',1346000713,'113.185.2.132','moz - 14 - nt - Windows 7 - VN - Vietnam');
/*!40000 ALTER TABLE `online` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `id` char(100) NOT NULL,
  `title` varchar(170) NOT NULL,
  `summary` text NOT NULL,
  `content` text NOT NULL,
  `image` char(250) NOT NULL,
  `order` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES ('services','Thông Tin Dịch Vụ','','<p>\r\n	Đang Cập Nhật</p>\r\n','',0,'0000-00-00','00:00:00',0),('introduction','Thông Tin Giới Thiệu','','<p>\n	<strong><em><span style=\"font-size: 10pt; line-height: 150%; font-family: Arial\">Ch&agrave;o mừng qu&yacute; kh&aacute;ch đến thăm trang web của <span style=\"color:#ff0000;\">lebbienhoa.com.</span></span></em></strong></p>\n<p>\n	Thay mặt to&agrave;n thể nh&acirc;n vi&ecirc;n c&ocirc;ng ty, xin gửi lời ch&uacute;c sức khỏe, hạnh ph&uacute;c, th&agrave;nh đạt v&agrave; lời cảm ơn ch&acirc;n th&agrave;nh đến qu&yacute; kh&aacute;ch h&agrave;ng&nbsp; đ&atilde; ủng hộ&nbsp; ch&uacute;ng t&ocirc;i trong suốt thời gian qua.</p>\n<p>\n	C&ocirc;ng ty ch&uacute;ng t&ocirc;i tr&acirc;n trọng giới thiệu đến qu&yacute; kh&aacute;ch h&agrave;ng c&aacute;c sản phẩm biển quảng c&aacute;o điện tử đa dạng như:</p>\n<p>\n	Bảng điện tử Led hiển thị số.<br />\n	- Bảng điện tử Led chữ chạy ma trận một m&agrave;u.<br />\n	- Bảng điện tử Led chữ chạy ma trận ba m&agrave;u.<br />\n	- Bảng điện tử hiển thị th&ocirc;ng tin Chứng kho&aacute;n.<br />\n	- Bảng điện tử hiển thị tỷ gi&aacute; Ng&acirc;n h&agrave;ng, Kết quả xổ số, Nghiệp vụ Bưu điện,&hellip;</p>\n<p>\n	L&agrave; đơn vị chuy&ecirc;n&nbsp; cung cấp c&aacute;c sản phẩm biển quảng c&aacute;o điện tử, c&ugrave;ng đội ngũ nh&acirc;n vi&ecirc;n trẻ, năng lực, chuy&ecirc;n nghiệp, ch&uacute;ng t&ocirc;i hi vọng mang đến cho qu&yacute; kh&aacute;ch sự h&agrave;i l&ograve;ng tuyệt đối khi đến với C&ocirc;ng ty ch&uacute;ng t&ocirc;i. Sự tin tưởng v&agrave; ủng hộ của qu&yacute; kh&aacute;ch h&agrave;ng l&agrave; động lực gi&uacute;p ch&uacute;ng t&ocirc;i khẳng định m&igrave;nh v&agrave; tiến xa hơn nữa trong sự nghiệp quảng c&aacute;o.</p>\n<p>\n	Rất h&acirc;n hạnh được phục vụ qu&yacute; kh&aacute;ch!</p>\n<p>\n	Tr&acirc;n trọng...</p>\n\n','',0,'0000-00-00','00:00:00',0),('contact','Thông Tin Liên Hệ','','<p>\n	<span style=\"font-size: 16px\"><strong>C&Ocirc;NG TY CỔ PHẦN TVO VIỆT NAM</strong></span></p>\n<p>\n	<strong>Địa chỉ:</strong> Số 16, Vũ Hồng Ph&ocirc;, B&igrave;nh Đa, Bi&ecirc;n Ho&agrave;, Đồng Nai&nbsp; ( Ngay nh&agrave; h&agrave;ng Sinh Đ&ocirc;i đường trường Kinh Tế )</p>\n<p>\n	<strong>Xưởng thi c&ocirc;ng:</strong> Số 40, Vũ Hồng Ph&ocirc;, B&igrave;nh Đa, Bi&ecirc;n Ho&agrave;, Đồng Nai&nbsp; ( Ngay ng&atilde; ba Th&agrave;nh Lu&acirc;n quẹo phải )</p>\n<p>\n	<strong>Điện thoại :</strong> 0618.825.825 <strong>Ext </strong>101&nbsp; Mobile:&nbsp; 0943.255.256 Mr.Long</p>\n<p>\n	<strong>Email :</strong> info@tvo.com.vn&nbsp; -&nbsp; hotro@tvo.com.vn</p>\n<p>\n	<strong>Website:</strong>&nbsp; www.ledbienhoa.com&nbsp; -&nbsp; www.tvo.com.vn</p>\n','',0,'0000-00-00','00:00:00',0),('footer','Thông Tin Footer','','<p>\n	<span style=\"font-size: 16px\"><strong>C&Ocirc;NG TY CỔ PHẦN TVO VIỆT NAM<span style=\"color:#ff8c00;\"> </span><span style=\"color:#000080;\">( Giao H&agrave;ng tr&ecirc;n To&agrave;n Quốc)</span></strong></span></p>\n<p>\n	<strong>Địa chỉ:</strong> Số 16, Vũ Hồng Ph&ocirc;, B&igrave;nh Đa, Bi&ecirc;n Ho&agrave;, Đồng Nai&nbsp; ( Ngay nh&agrave; h&agrave;ng Sinh Đ&ocirc;i đường trường Kinh Tế )</p>\n<p>\n	<strong>Xưởng thi c&ocirc;ng:</strong> Số 40, Vũ Hồng Ph&ocirc;, B&igrave;nh Đa, Bi&ecirc;n Ho&agrave;, Đồng Nai&nbsp; ( Ngay ng&atilde; ba Th&agrave;nh Lu&acirc;n quẹo phải )</p>\n<p>\n	<strong>Điện thoại :</strong> 0618.825.825 <strong>Ext </strong>101&nbsp; Mobile:&nbsp; 0943.255.256 Mr.Long</p>\n<p>\n	<strong>Email :</strong> info@tvo.com.vn&nbsp; -&nbsp; hotro@tvo.com.vn</p>\n<p>\n	<strong>Website:</strong>&nbsp; www.ledbienhoa.com&nbsp; -&nbsp; www.tvo.com.vn</p>\n','',0,'0000-00-00','00:00:00',0),('keywords','Thông Tin Keywords','ledbienhoa.com, led matrix bien hoa, led ma tran bien hoa, thi cong bang led ma tran chuyen nghiep tai bien hoa dong nai, thi cong led ma tran tai bien hoa, thi cong led matrix tai bien hoa, modune led matrix, board dieu khien led, nguon led','ledbienhoa.com, led matrix bien hoa, led ma tran bien hoa, thi cong bang led ma tran chuyen nghiep tai bien hoa dong nai, thi cong led ma tran tai bien hoa, thi cong led matrix tai bien hoa, modune led matrix, board dieu khien led, nguon led','',0,'0000-00-00','00:00:00',0),('price','Thông Tin Bảng Giá','','<p>\n	Qu&yacute; kh&aacute;ch vui l&ograve;ng li&ecirc;n hệ 0943 255 256 để được tư vấn v&agrave; b&aacute;o gi&aacute; ch&iacute;nh x&aacute;c nhất</p>\n<p>\n	Xin cảm ơn</p>\n','',0,'0000-00-00','00:00:00',0);
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` char(100) NOT NULL,
  `title` varchar(150) NOT NULL,
  `type` char(100) NOT NULL,
  `image` char(250) NOT NULL,
  `summary` text NOT NULL,
  `content` text NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `order` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES ('p10-1r-3b3cc','P10- 1R','hop-den-bang-hieu','p10-1r-16x32-semi-outdoor-c1d56banmatran.jpg','','<p>\n	K&iacute;ch thước:&nbsp; 16x32</p>\n<p>\n	Model: Semi outdoor</p>\n','2012-08-16','05:59:26',0,0,190000),('p10-1r-300c8','P10- 1R','hop-den-bang-hieu','p10-1r-16x32-chong-nuoc-eb3fcoutdoor.jpg','','<p>\n	K&iacute;ch thước:&nbsp; 16x32</p>\n<p>\n	Model: Chống nước</p>\n','2012-08-16','06:01:15',0,0,210000),('p16-2r-1g--a9d96','P16 (2R,1G)','hop-den-sieu-mong','p16-2r-1g--4c480small-10-2010-15.jpg','','','2012-08-16','06:04:33',0,0,240000),('p20-2r-1g--bc254','P20 ( 2R,1G )','hop-den-sieu-mong','p20-2r-1g--233besmall-09-2010-17.jpg','','','2012-08-16','06:05:23',0,0,290000),('bx-4t1-2e385','BX-4T1','board-dieu-khien-','bx-4t1-f04c1small-10-2010-74.jpg','','','2012-08-16','08:34:12',0,0,200000),('bx-4a3-c5020','BX - 4A3','board-dieu-khien-','bx-4a3-e6569small-08-2011-84.png.jpg','','','2012-08-16','08:37:43',0,0,540000),('bx-5a1-5215b','BX-5A1','board-dieu-khien-','bx-5a1-361bcsmall-10-2010-75.jpg','','','2012-08-16','08:42:09',0,0,320000),('bx-4a2-28a4c','BX - 4A2','board-dieu-khien-','bx-4a2-915f0small-10-2010-76.jpg','','','2012-08-16','08:45:20',0,0,395000),('nguon-12v-5a-60w-nhan-hieu-sanpu-1c043','Nguồn 12V - 5A - 60W_Nhãn hiệu SANPU','nguon-','nguon-12v-5a-60w-nhan-hieu-sanpu-b898csmall-11-2010-79.jpg','','','2012-08-16','08:54:38',0,0,140000),('nguon-12v-20.8a-250w-nhan-hieu-sanpu-a6232','Nguồn 12V - 20.8A -250W_Nhãn hiệu SANPU','nguon-','nguon-12v-20.8a-250w-nhan-hieu-sanpu-e1aa2small-08-2011-80.png.jpg','','','2012-08-16','08:57:32',0,0,290000),('nguon-12v-12.5-a-150w-nhan-hieu-sanpu-b91d6','Nguồn 12V - 12.5 A - 150W_Nhãn hiệu SANPU','nguon-','nguon-12v-12.5-a-150w-nhan-hieu-sanpu-143b4small-11-2010-79.jpg','','','2012-08-16','09:00:08',0,0,220000),('nguon-12v-33a-400w-nhan-hieu-sanpu-a36eb','Nguồn 12V - 33A - 400W Nhãn hiệu SANPU','nguon-','nguon-12v-33a-400w-nhan-hieu-sanpu-7e1d2small-08-2011-80.png.jpg','','','2012-08-16','09:02:42',0,0,360000),('4m16-x-0-16m-ec8f4','4m16 x 0,16m','tam-led-co-san','1m2-x-0-32m-92bdehinh-anh048.jpg','','<p>\n	Chiều d&agrave;i: 416 cm</p>\n<p>\n	Chiều rộng: 16 cm</p>\n<p>\n	M&agrave;u sắc: Đỏ</p>\n<p>\n	Gi&aacute; tr&ecirc;n đ&atilde; bao gồm ph&iacute; lắp đặt</p>\n<p>\n	Điều khiển: Qua m&aacute;y t&iacute;nh bằng cổng Com</p>\n<p>\n	&nbsp;</p>\n','2012-08-20','11:34:04',0,0,8000000),('4m16-x-0-32m-0d69a','4m16 x 0,32m','tam-led-co-san','4m16-x-0-32m-011c1hinh-anh048.jpg','','<p>\n	Chiều d&agrave;i: 416 cm</p>\n<p>\n	Chiều rộng: 32 cm</p>\n<p>\n	M&agrave;u sắc: Đỏ</p>\n<p>\n	Gi&aacute; tr&ecirc;n đ&atilde; bao gồm ph&iacute; lắp đặt</p>\n<p>\n	Điều khiển: Qua m&aacute;y t&iacute;nh bằng cổng Com</p>\n','2012-08-20','11:36:43',0,0,14000000),('1m28-x-0-32m-8f832','1m28 x 0,32m','tam-led-co-san','1m28-x-0-32m-54b35hinh-anh048.jpg','','<p>\n	Chiều d&agrave;i: 128 cm</p>\n<p>\n	Chiều rộng: 32 cm</p>\n<p>\n	M&agrave;u sắc: Đỏ</p>\n<p>\n	Gi&aacute; tr&ecirc;n đ&atilde; bao gồm ph&iacute; lắp đặt</p>\n<p>\n	Điều khiển: Qua m&aacute;y t&iacute;nh bằng cổng Com</p>\n','2012-08-20','11:44:13',0,0,5000000),('1m28-x-0-16m-fb4e8','1m28 x 0,16m','tam-led-co-san','1m28-x-0-16m-026c3hinh-anh048.jpg','','<p>\n	Chiều d&agrave;i:128 cm</p>\n<p>\n	Chiều rộng: 16 cm</p>\n<p>\n	M&agrave;u sắc: Đỏ</p>\n<p>\n	Gi&aacute; tr&ecirc;n đ&atilde; bao gồm ph&iacute; lắp đặt</p>\n<p>\n	Điều khiển: Qua m&aacute;y t&iacute;nh bằng cổng Com</p>\n','2012-08-20','11:47:35',0,0,3000000);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slideshow`
--

DROP TABLE IF EXISTS `slideshow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slideshow` (
  `id` char(40) NOT NULL,
  `title` varchar(150) NOT NULL,
  `link` char(150) NOT NULL,
  `image` char(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slideshow`
--

LOCK TABLES `slideshow` WRITE;
/*!40000 ALTER TABLE `slideshow` DISABLE KEYS */;
INSERT INTO `slideshow` VALUES ('bang-led-cong-ty-chung-khoan-fptd2340','Bảng led Công ty Chứng Khoán FPT','http://ledbienhoa.com','bang-led-cong-ty-chung-khoan-fptd2340'),('cong-ty-bao-ve-bao-vietb9ba4','Công ty Bảo Vệ Bảo Việt','http://ledbienhoa.com','cong-ty-bao-ve-bao-vietb9ba4'),('cong-trinh-tvoadc46','Công trình TVO','http://ledbienhoa.com','cong-trinh-tvoadc46');
/*!40000 ALTER TABLE `slideshow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type`
--

DROP TABLE IF EXISTS `type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type` (
  `id` char(100) NOT NULL,
  `title` varchar(150) NOT NULL,
  `image` char(250) NOT NULL,
  `summary` text NOT NULL,
  `content` text NOT NULL,
  `order` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type`
--

LOCK TABLES `type` WRITE;
/*!40000 ALTER TABLE `type` DISABLE KEYS */;
INSERT INTO `type` VALUES ('hop-den-bang-hieu','Modune Led Ma trận 1 màu','','','',1,0),('hop-den-sieu-mong','Modune Led Ma trận 3 màu','','','',2,0),('led-matrix-full-color','Modune ma trận Full Color','','','',3,0),('board-dieu-khien-','CPU - Mạch điều khiển','','','',4,0),('nguon-','Nguồn các loại','','','',5,0),('tam-led-co-san','Tấm Led Ma Trận có sẵn','','','',9,0),('dong-ho-led','Đồng Hồ Led Ma Trận','','','',7,0);
/*!40000 ALTER TABLE `type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `ma` char(70) NOT NULL,
  `tendangnhap` char(70) NOT NULL,
  `matkhau` char(70) NOT NULL,
  `hoten` varchar(150) NOT NULL,
  `diachi` varchar(200) NOT NULL,
  `sodienthoai` char(20) NOT NULL,
  `capdo` tinyint(4) NOT NULL,
  `kichhoat` tinyint(4) NOT NULL,
  `khoa` tinyint(4) NOT NULL,
  `ngaydangki` date NOT NULL,
  `hinhanh` char(150) NOT NULL,
  `email` char(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('bd0fddf4c41ce0addf40701e8992c9bb','tvo','e10adc3949ba59abbe56e057f20f883e','TVO Việt Nam','Biên Hoà - Đồng Nai','0915.050.892',1,1,0,'2012-08-13','','hoangtuanhung92@gmail.com'),('31cd6fcd260d8f9f2ebd7878458ac9da','tuanhung92','16332e05ab36448c98c1871902bf26e6','Hoàng Tuấn Hùng','Biên Hoà - Đồng Nai','0915050902',1,1,0,'2012-08-13','','hoangtuanhung92@gmail.com');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yahoo`
--

DROP TABLE IF EXISTS `yahoo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yahoo` (
  `id` char(70) NOT NULL,
  `yahoo` char(50) NOT NULL,
  `title` varchar(150) NOT NULL,
  `phone` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yahoo`
--

LOCK TABLES `yahoo` WRITE;
/*!40000 ALTER TABLE `yahoo` DISABLE KEYS */;
INSERT INTO `yahoo` VALUES ('9ad4b80a8cb4ef2b50d00fc86178c1b1','tvovietnam','0943 255 256',''),('28443e4c512f4fff51beaf04c5762854','linh_pc50','0129 700 2498','');
/*!40000 ALTER TABLE `yahoo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-08-27  0:14:52
