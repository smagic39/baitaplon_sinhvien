                    <div id="pageHeader" class="regHeader">{$lang1}
                        <div class="steps">
                            <div class="step1 selected"></div>
                            <div class="step2"></div>
                        </div>
                    </div>                   
                                
                    <div id="processPage">
                        <div id="columnLeft" style="margin-bottom: 30px;">
                        	<form id="registerCommand" name="registerCommand" action="{$baseurl}/register.php" method="post">
                    
                            <div id="regForm">
                            	
                                <div class="heading">
                                	<h3>{$lang2}.</h3>
                                	<div>
                                		<a href="{$baseurl}/login.php"> {$lang3}</a>.
                            		</div>
                                </div>
                            
                                <div class="clear"></div>
                                <div class="clear"></div>
        						
                                {if $err1 ne ""}
                                <div id="" class="error " style="">*** {$err1} ***</div>
                                {/if}
                                <div class="label">
                                    <div>
                                        <div class="floatLeft">{$lang4}</div>
                                        <div id="usernameStatus" style="display: none;"></div>
                                    </div>
                                    <input id="username" name="username" class="text" type="text" value="{$username|stripslashes}" maxlength="20" onfocus="toggle('usernameTip');" onblur="toggle('usernameTip');"/>
                                    <div class="tipBubble">
                                        <p id="usernameTip" class="oneLine" style="display: none;">{$lang5}</p>
                                    </div>
                                </div>
                                
                                <div class="clear"></div>
                                
                                {if $err2 ne ""}
                                <div id="" class="error " style="">*** {$err2} ***</div>
                                {/if}
                                <div class="label">
                                    <div>{$lang6}</div>
                                    <input id="password1" name="password1" class="text" type="password" value="{$pw|stripslashes}" maxlength="20" onfocus="toggle('passwordTip');" onblur="toggle('passwordTip');"/>
                                    <div class="tipBubble">
                                        <p id="passwordTip" class="oneLine" style="display: none;">{$lang7}</p>
                                    </div>
                                </div>
                                            
                                <div class="clear"></div>
                                 
                                {if $err3 ne ""}
                                <div id="" class="error " style="">*** {$err3} ***</div>
                                {/if}
                                <div class="label">
                                    <div>{$lang16}</div>
                                    <input id="confirmpassword" name="confirmpassword" class="text" type="password" value="{$pw2|stripslashes}" maxlength="20" onfocus="toggle('passwordTip2');" onblur="toggle('passwordTip2');"/>
                                    <div class="tipBubble">
                                        <p id="passwordTip2" style="display: none;">{$lang17}</p>
                                    </div>
                                </div>
                                
                                <div class="clear"></div>
                                
                                {if $err4 ne ""}
                                <div id="" class="error " style="">*** {$err4} ***</div>
                                {/if}
                                <div class="label">
                                    <div>{$lang8}</div>
                                    <input id="email" name="email" class="text" type="text" value="{$email|stripslashes}" maxlength="150" onfocus="toggle('emailTip');" onblur="toggle('emailTip');"/>
                                    <div class="tipBubble">
                                        <p id="emailTip" style="display: none;">{$lang9}</p>
                                    </div>
                                </div>
          
                                <div class="clear"></div>
                                
                                {if $err6 ne ""}
                                <div id="" class="error " style="">*** {$err6} ***</div>
                                {/if}
                                {if $err7 ne ""}
                                <div id="" class="error " style="">*** {$err7} ***</div>
                                {/if}
                                {if $err8 ne ""}
                                <div id="" class="error " style="">*** {$err8} ***</div>
                                {/if}
                                <input id="validBirthday" name="validBirthday" type="hidden" value="false"/>
                                <div class="label" style="height:50px;">
                                    <div>{$lang10}</div>
                                    <select id="day" name="day" style="width:48px;" onfocus="toggle('birthdayTip');" onblur="toggle('birthdayTip');">
                                        <option value="0">-</option>
                                        {$bdays}              
                                    </select>
                                    <select id="month" name="month" style="width:90px;" onfocus="toggle('birthdayTip');" onblur="toggle('birthdayTip');">
                                        <option value="0">-</option>
                                        {$bmonths}
                                    </select>
                                    <select id="year" name="year" style="width:65px;" onfocus="toggle('birthdayTip');" onblur="toggle('birthdayTip');">
                                        <option value="0">-</option>
                                        {$byears}
                                    </select>
                                    <div class="tipBubble">
                                        <p id="birthdayTip" style="display: none;">{$lang11}</p>
                                    </div>
                                </div>
                                
                                <div class="clear"></div>
                                
                                {if $enable_captcha ne "0"}
                                {if $err5 ne ""}
                                <div id="" class="error " style="">*** {$err5} ***</div>
                                {/if}
                                <div class="label">
                                    <div>{$lang30}</div>
                                    <input id="imagecode" name="imagecode" class="text" type="text" value="" maxlength="6" onfocus="toggle('imagecodeTip');" onblur="toggle('imagecodeTip');"/><br />
                                    <img src="{$baseurl}/include/captcha.php" />
                                    <div class="tipBubble">
                                        <p id="imagecodeTip" style="display: none;">{$lang29}</p>
                                    </div>
                                </div>
                                {/if}
                                        
                            </div>
                            
                            <div class="clear"></div>
                            <div class="divider"></div>
                                
                            <a id="submitButton" class="yellowButton" onclick="document.registerCommand.submit();"><span>{$lang12}</span></a>
                                    
                            <div class="legalText">
                                <a href="{$baseurl}/terms_of_use.php" target="_blank">{$lang13}</a>
                            </div>
                            <input type="hidden" name="register" value="1" /> 
                            </form>
                        </div>
                    </div>
                </div>
            </div>