                    <a name="uploadAnchor"></a>
					<div id="bricks">
						{include file="errormsg.tpl"}
                        <div id="uploadBoxHeading">
                            <div id="heading">
                                <h2>{$lang80}</h2>
                            </div>
                        </div>
                        
                        {literal}
						<script language="javascript" type="text/javascript">
                        function clearval(obj) {
                            document.getElementById(obj).value="";
                        }
						function chkmsg(obj) {
							var el = document.getElementById(obj);
							var gmsg = el.value;
							if ( gmsg == {/literal}'{$lang83}...'{literal} ) {
								el.value="";
							}
                        }
						function rfocus() {
							document.uploadform.description.focus();
                        }
						function rfocus2(obj) {
							document.uploadform.description.focus();
							document.getElementById('postTextBox').value = obj;
						}
                        </script>
                        {/literal}
    
                        <div id="uploadBox" class="usermain">
                            <div class="post_brick" id="uploadAreaMain">
                                <div class="post_brick_top">&nbsp;</div>
                                <div class="post_brick_content" id="uploadArea">
                                    <div id="addButton">
                                        <a href="#" onclick="toggle2('fileBrowse');toggle('addButton');toggle('removeButton');"><img src="{$imageurl}/uploadbox_camera.gif" width="29" height="22" alt="add" /></a><br />
                                        <a href="#" onclick="toggle2('fileBrowse');toggle('addButton');toggle('removeButton');">{$lang81}</a>
                                    </div>
                                    <div id="removeButton" style="display: none;">
                                        <a href="#" onclick="toggle2('fileBrowse');toggle('removeButton');toggle('addButton');clearval('fileBrowse');"><img src="{$imageurl}/uploadbox_camera.gif" width="29" height="22" alt="add" /></a><br />
                                        <a href="#" onclick="toggle2('fileBrowse');toggle('removeButton');toggle('addButton');clearval('fileBrowse');">{$lang82}</a>
                                    </div>
                                    <script type="text/javascript" src="{$baseurl}/js/charcount.js"></script>
                                    <form method="post" enctype="multipart/form-data" name="uploadform" id="uploadform" action="{$baseurl}/home.php">
                                        <textarea name="description" id="postTextBox" class="defaultText" onfocus="chkmsg('postTextBox');" onblur="textrem('postTextBox', '{$max_post_chars}');" onkeyup="textrem('postTextBox', '{$max_post_chars}');" onkeydown="textrem('postTextBox', '{$max_post_chars}');">{$lang83}...</textarea>
                                        <input maxlength="100" size="50" type="file" name="file" id="fileBrowse" style="display: none;" />
                                        <input type="hidden" name="subupdate" value="1" />
                                    </form>                                    
                                </div>
                            	<div class="post_brick_btm">&nbsp;</div>
                            </div>
                            
                            <div class="post_info_area">
                                <div style="margin-top: 5px;">{$lang84}</div>
                            </div>
                            <div class="post_buttons">                                    
                                <a href="#" onclick="document.uploadform.submit()" class="standardButton" id="updateButton"><span>{$lang85}</span></a>
                            </div>                            
                        </div>
                        
                    	<div class="clear">&nbsp;</div>
                        <div class="divider">&nbsp;</div>
                    
                        <div class="floatLeft" style="width: 100%;">
                            <h2><a href="{$baseurl}/home.php" class="link">{$lang287}</a></h2> 
                            <div class="more"><a href="#" onclick="toggle2('feedSettings');">{$lang288}</a></div>
                            
                            <div id="feedSettings" style="display: none;">
                            	<form method="post" name="filterupdates" id="filterupdates">
                                <input type="checkbox" id="fil1" name="fil1" value="1" {if $f.fil1 eq "1"}checked="checked"{/if}/><label for="fil1">{$lang286}</label>
                                <input type="checkbox" id="fil2" name="fil2" value="1" {if $f.fil2 eq "1"}checked="checked"{/if}/><label for="fil2">{$lang289}</label>
                                <input type="checkbox" id="fil3" name="fil3" value="1" {if $f.fil3 eq "1"}checked="checked"{/if}/><label for="fil3">{$lang290}</label>
                                <div class="clear" style="height: 6px;">&nbsp;</div>
                                <input type="checkbox" id="fil4" name="fil4" value="1" {if $f.fil4 eq "1"}checked="checked"{/if}/><label for="fil4">{$lang291}</label>
                                <input type="checkbox" id="fil5" name="fil5" value="1" {if $f.fil5 eq "1"}checked="checked"{/if}/><label for="fil5">{$lang292}</label>
                                <input type="hidden" name="sfilterupdates" value="1" />
                                </form>
                                <div class="floatRight"><a href="#" onclick="document.filterupdates.submit()" class="standardButton"><span>{$lang158}</span></a></div>
                                <a href="#" onclick="toggle2('feedSettings');" class="close"><img src="{$imageurl}/icon_close.gif" width="7" height="7" alt="" /></a>
                            </div>
                        </div>
                    
                        <div class="clear">&nbsp;</div>

                        <ul>               
                    
                    		{section name=i loop=$posts max=$max_posts_userhome start=$pagingstart}
                            {insert name=get_propic1 value=var assign=propic USERID=$posts[i].USERID}
                            {if $posts[i].type eq "com-update"}
                                {if $smarty.session.USERID eq $posts[i].USERID}
                                <li>
                                	<a name="goto{$posts[i].ID}"></a>
                                    <div class="largeBrick2 viewer" style="display: none;" id="edit{$posts[i].ID}">
                                        <div class="postBox">
                                            <div class="comment_box"></div>   
                                            <form method="post" name="editpost{$posts[i].ID}" id="editpost{$posts[i].ID}">                                
                                            <textarea id="editComment{$posts[i].ID}" name="editComment{$posts[i].ID}" class="defaultText" onfocus="chkmsg('postComment{$posts[i].ID}');" onblur="textrem('postComment{$posts[i].ID}', '{$max_post_chars}');" onkeyup="textrem('postComment{$posts[i].ID}', '{$max_post_chars}');" onkeydown="textrem('postComment{$posts[i].ID}', '{$max_post_chars}');">{insert name=get_post value=a assign=mpost post=$posts[i].msg}{$mpost|stripslashes}</textarea>
                                            <input type="hidden" name="subedit" value="1" />
                                            <input type="hidden" name="EID" value="{$posts[i].ID}" />
                                            </form>
                                            <div class="postBtn ">
                                                <a href="#" onclick="document.editpost{$posts[i].ID}.submit()" class="requiresLogin standardButton"><span>{$lang109}</span></a>
                                            </div>
                                        </div>
                                    </div>                                    
                                    <form method="post" name="delcom{$posts[i].ID}" id="delcom{$posts[i].ID}" action="{$baseurl}/home.php">
                                    	<input type="hidden" name="CID" value="{$posts[i].ID}" />
                                        <input type="hidden" name="subdelcom" value="1" />
                                    </form>                                    
                                </li>
                                {/if}
                            {elseif $posts[i].type eq "update"}
                                {if $smarty.session.USERID eq $posts[i].USERID}
                                <li>
                                	<a name="goto{$posts[i].ID}"></a>
                                    <div class="largeBrick2 viewer" style="display: none;" id="editupdate{$posts[i].ID}">
                                        <div class="postBox">
                                            <div class="comment_box"></div>   
                                            <form method="post" name="editpost{$posts[i].ID}" id="editpost{$posts[i].ID}">                                
                                            <textarea id="editComment{$posts[i].ID}" name="editComment{$posts[i].ID}" class="defaultText" onfocus="chkmsg('postComment{$posts[i].ID}');" onblur="textrem('postComment{$posts[i].ID}', '{$max_post_chars}');" onkeyup="textrem('postComment{$posts[i].ID}', '{$max_post_chars}');" onkeydown="textrem('postComment{$posts[i].ID}', '{$max_post_chars}');">{insert name=get_post value=a assign=mpost post=$posts[i].msg}{$mpost|stripslashes}</textarea>
                                            <input type="hidden" name="subeditupdate" value="1" />
                                            <input type="hidden" name="EID" value="{$posts[i].ID}" />
                                            </form>
                                            <div class="postBtn ">
                                                <a href="#" onclick="document.editpost{$posts[i].ID}.submit()" class="requiresLogin standardButton"><span>{$lang111}</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <form method="post" name="delupdate{$posts[i].ID}" id="delupdate{$posts[i].ID}" action="{$baseurl}/home.php">
                                    	<input type="hidden" name="DID" value="{$posts[i].ID}" />
                                        <input type="hidden" name="subdelupdate" value="1" />
                                    </form>
                                </li>
                                {/if}
                            {/if}
                            
                            <li class="first">                                
                                <div class="tinyOutlineOwner">
                                    <div class="tinyuser">
                                    {if $smarty.session.USERID eq $posts[i].USERID}
                                    <img src="{$membersprofilepicurl}/thumbs/{$propic}" class="profilepic-{$posts[i].username|stripslashes}" alt="{$posts[i].username|stripslashes}" title="{$posts[i].username|stripslashes}" />
                                    {else}
                                    <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$posts[i].username|stripslashes}"><img src="{$membersprofilepicurl}/thumbs/{$propic}" class="profilepic-{$posts[i].username|stripslashes}" alt="{$posts[i].username|stripslashes}" title="{$posts[i].username|stripslashes}" /></a>
                                    {/if}
                                    </div>
                                </div>
                            </li>                    
                    
                            <li>
                                <div class="brick {if $posts[i].type eq "com-update"}commentBrick{/if}">
                                    <div class="brick_top"></div>
                                    <div class="brick_content">
                                    	{if $posts[i].pic ne ""}
                                    	<div class="brick_preview">
                                            <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                                                <tr><td align="center" style="vertical-align:middle"><a href="{$baseurl}/viewupdate.php?id={$posts[i].ID}"><img src="{$tpicurl}/{$posts[i].pic}" /></a></td></tr>
                                            </table> 
                                        </div>
                                        {/if}
                                        <div class="brick_title" id="content-{$posts[i].ID}">{insert name=get_post value=a assign=mpost post=$posts[i].msg}{$mpost|stripslashes}{if $posts[i].pic eq ""} {if $posts[i].type eq "update"}<a class="moreLink" href="{$baseurl}/viewupdate.php?id={$posts[i].ID}">[{$lang94}]</a>{/if}{/if}</div>
                                        
                                        {if $posts[i].type eq "com-update"}
                                        {insert name=is_new_post value=a assign=npost time=$posts[i].time_added}
                                        <div class="brick_byline" style="width: 100%;">
                                        	<div class="floatLeft">{insert name=get_time_to_days_ago value=a time=$posts[i].time_added}{if $posts[i].edited ne ""}&nbsp;&nbsp;&nbsp;<span class="edittime">[{$lang112}:{insert name=get_time_to_days_ago value=a time=$posts[i].edited}]</span>{/if}</div>
                                            
                                            <div class="brick_actions">
                                                <a href="{$baseurl}/viewupdate.php?id={$posts[i].UID}#comment" class="comment_num">{insert name=com_count value=a assign=comco ID=$posts[i].UID}{$comco}</a><a href="{$baseurl}/viewupdate.php?id={$posts[i].UID}#comment" class="commentIcon{if $npost eq "1"}WhatsNew{/if}"></a>
                                                {if $smarty.session.USERID eq $posts[i].USERID}
                                                <img src="{$imageurl}/icon_divider_blue.gif" width="1" height="15" alt="" class="brick_icon_divider"/>
                                                <div class="actionsPopupContainer"  user="{$posts[i].username|stripslashes}">
                                                    <a class="editIcon blue" onclick="toggle('pop{$posts[i].ID}')"></a>
                                                    <div class="actionsPopup" style="display: none;" id="pop{$posts[i].ID}">
                                                        <div class="top"></div>
                                                        <div class="body">
                                                            <div class="content">                                                
                                                                {if $smarty.session.USERID eq $posts[i].USERID}<a href="#goto{$posts[i].ID}" onclick="toggle2('edit{$posts[i].ID}')">{$lang95}</a> {/if}<a href="#" onclick="document.delcom{$posts[i].ID}.submit()">{$lang96}</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {/if}
                                                <img src="{$imageurl}/icon_divider_blue.gif" width="1" height="15" alt="" class="brick_icon_divider"/><a href="{$baseurl}/viewupdate.php?id={$posts[i].UID}&r=@{$posts[i].username|stripslashes}:">{$lang97}</a>                                        
                                            </div>
                                        
										</div>
                                        {if $posts[i].reply ne "0"}
                                        {insert name=com_reply_info value=a assign=reply ID=$posts[i].reply}
                                        <div class="exchange_preview">
                                    		<div class="floatLeft">
                                        		<a href="{$baseurl}/viewupdate.php?id={$reply[0].UID}" class="content">"{$reply[0].msg|stripslashes}"</a>                                        
                                            	<div style="clear: left;" class="brick_byline">
                                                	{$lang99} <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$reply[0].username|stripslashes}">{$reply[0].username|stripslashes}</a>, {insert name=get_time_to_days_ago value=a time=$reply[0].time_added}
                                            	</div>
                                    		</div>                            
                        				</div>
                                        {/if}
                                        {else}
                                        <div class="brick_byline">
                                            {if $posts[i].USERID ne $smarty.session.USERID}{$lang343} <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$posts[i].username|stripslashes}">{$posts[i].username|stripslashes}</a>, {/if}<a class="link" href="{$baseurl}/viewupdate.php?id={$posts[i].ID}">{insert name=get_time_to_days_ago value=a time=$posts[i].time_added}</a>{if $posts[i].edited ne ""}&nbsp;&nbsp;&nbsp;<span class="edittime">[{$lang112}:{insert name=get_time_to_days_ago value=a time=$posts[i].edited}]</span>{/if}
                                        </div>
										{/if}



                                    </div>
                                    <div class="brick_btm">
                                    	{if $posts[i].type eq "update"}
                                        <div class="brick_actions">
                                            <a href="{$baseurl}/viewupdate.php?id={$posts[i].ID}#comment" class="comment_num">{insert name=com_count value=a assign=comco ID=$posts[i].ID}{$comco}</a><a href="{$baseurl}/viewupdate.php?id={$posts[i].ID}#comment" class="commentIcon{if $comco GT "0"}{insert name=is_new_cpost value=a assign=ncpost id=$posts[i].ID}{if $ncpost eq "1"}WhatsNew{/if}{/if}"></a>
                                            {if $smarty.session.USERID eq $posts[i].USERID}
                                            <img src="{$imageurl}/icon_divider_blue.gif" width="1" height="15" alt="" class="brick_icon_divider"/>
                                            <div class="actionsPopupContainer"  user="{$posts[i].username|stripslashes}">
                                                <a class="editIcon blue" onclick="toggle('pop{$posts[i].ID}')"></a>
                                                <div class="actionsPopup" style="display: none;" id="pop{$posts[i].ID}">
                                                    <div class="top"></div>
                                                    <div class="body">
                                                        <div class="content">                                                
                                                            <a href="#goto{$posts[i].ID}" onclick="toggle2('editupdate{$posts[i].ID}')">{$lang95}</a> <a href="#" onclick="document.delupdate{$posts[i].ID}.submit()">{$lang96}</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {/if}
                                            <img src="{$imageurl}/icon_divider_blue.gif" width="1" height="15" alt="" class="brick_icon_divider"/><a href="#" onclick="{if $smarty.session.USERID eq $posts[i].USERID}rfocus(){else}rfocus2('@{$posts[i].username|stripslashes}: '){/if};" class="requiresLogin">{$lang97}</a>                                        </div>
                                        {/if}
                                    </div>
                                </div>
                            </li>
                            {/section}

                        </ul>   
                        
                        <div class="pagingBlock">
                        	{$pagelinks}
                        </div>

                    </div>

                    <div id="rightPanel">
                        <div class="contentBlock">
                            <h2 class="sidebartop">
                                <div class="userName"><a href="{$baseurl}/{insert name=get_seo_profile value=a username=$smarty.session.USERNAME|stripslashes}">{$lang116}</a></div>
                            </h2>
                            {insert name=get_propic2 value=var assign=mypropic USERID=$smarty.session.USERID}
                            <div class="userPicPreview" style="float:left; margin-bottom:10px;">
                                <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                                    <tr><td align="center" style="vertical-align:middle"><a href="{$baseurl}/{insert name=get_seo_profile value=a username=$smarty.session.USERNAME|stripslashes}"><img src="{$membersprofilepicurl}/thumbs/{$mypropic}" class="profilepic-{$smarty.session.USERNAME|stripslashes}" /></a></td></tr>
                                </table>
                            </div>            
                            <div class="bylineOwner"><a href="{$baseurl}/viewupdates.php?id={$smarty.session.USERID}">{insert name=get_updatecount value=var assign=updatecount USERID=$smarty.session.USERID}{$updatecount} {$lang117}</a> | {$followers|@count} {$lang118}  | {insert name=get_profileviews value=var assign=profileviews USERID=$smarty.session.USERID}{$profileviews} {$lang119}<br /><br /><a href="{$baseurl}/searchupdates.php?query=@{$smarty.session.USERNAME|stripslashes}">{$lang342}</a><br /><br /><a href="{$baseurl}/widget?id={$smarty.session.USERID|stripslashes}">{$lang345}</a></div>
                            <div style="float: left; margin: 0px 0px 10px 8px;"><a href="{$baseurl}/invite_friends.php" class="callToAction_invite"></a></div>      
                        </div>
                        
                        <div class="divider"></div>
                        <div class="contentBlock">
                        	{insert name=get_following_total value=var assign=followingt USERID=$smarty.session.USERID}
                            <h2 class="sidebargrid">{$followingt} {$lang120}</h2>
                            <div class="more"><a href="{$baseurl}/viewfollowing.php?id={$smarty.session.USERID}">{$lang107}</a>&nbsp;<img src="{$imageurl}/arrow.gif" width="3" height="6" alt=""/></div>
                            
                            <div class="usersidebar grid">

                                <!-- loop -->
                                {section name=i loop=$following}
                            	{insert name=get_propic1 value=var assign=propic USERID=$following[i].USERID}
                                <div class="gridItemContainer">
                                <div class="tinyOutline">
                                <div class="tinyuser">
                                        <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$following[i].username|stripslashes}" title="{$following[i].username|stripslashes}">
                                            <img src="{$membersprofilepicurl}/thumbs/{$propic}" class="profilepic-{$following[i].username|stripslashes}" />
                                        </a>
                                </div>
                                </div>
                                </div>
                                {/section}
                                <!-- loop end -->
                            
                            </div>
                            
                        </div>

                        <div class="divider"></div>
                        <div class="contentBlock">    
                        	{insert name=get_followerscount value=var assign=followingu USERID=$smarty.session.USERID}                            
                            <h2 class="sidebargrid">{$followingu} {$lang118}</h2>
                            <div class="more"><a href="{$baseurl}/viewfollowers.php?id={$smarty.session.USERID}">{$lang107}</a>&nbsp;<img src="{$imageurl}/arrow.gif" width="3" height="6" alt=""/></div>
  
                            <div class="usersidebar grid">

                                <!-- loop -->
                                {section name=i loop=$followers}
                            	{insert name=get_propic1 value=var assign=propic USERID=$followers[i].USERID}
                                <div class="gridItemContainer">
                                <div class="tinyOutline">
                                <div class="tinyuser">
                                        <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$followers[i].username|stripslashes}" title="{$followers[i].username|stripslashes}">
                                            <img src="{$membersprofilepicurl}/thumbs/{$propic}" class="profilepic-{$followers[i].username|stripslashes}" />
                                        </a>
                                </div>
                                </div>
                                </div>
                                {/section}
                                <!-- loop end -->
                            
                            </div>
                            
                        </div>
                    </div>

                </div>
            </div>
