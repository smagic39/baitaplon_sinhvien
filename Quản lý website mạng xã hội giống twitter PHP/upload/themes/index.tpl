                {literal}
				<script language="javascript" type="text/javascript">
                function clearval(obj) {
                    document.getElementById(obj).value="";
                }
                </script>
				<script type="text/javascript">
                  var delay = 3000;
                  var count = {/literal}{$total}{literal};
                  var showing = 7;
                  var i = 0;
                  function move(i) {
                    return function() {
                      $('#recent'+i).css('display', 'none');
                    }
                  }
                  function shift() {
                    var toShow = i + showing;
                    $('#recent'+toShow).slideDown(1000, move(i));
                    if(i<count)
                    {
                        i = i + 1;
                    }
                    setTimeout('shift()', delay);
                  }    
                  $(document).ready(function() {
                    setTimeout('shift()', delay);
                  });
                </script>
                {/literal}
                <div id="home">
					<div id="bricks2" style="width:533px;">
                    	<div id="illustration">
							<img src="{$imageurl}/home.png" width="533" height="302" />
						</div>
                    	<h2 style="font-size:26px">{$lang346}</h2>
						<ul class="viewallBricks" style="margin:0px; padding:0px;">
                            <li style="margin-bottom: 15px;">
                            </li>
                            {section name=i loop=$posts step=-1}
                            {assign var="iter" value=$smarty.section.i.iteration}
            				{math equation="$total - $iter" assign="mcount"}
                            {insert name=get_propic1 value=var assign=propic USERID=$posts[i].USERID}
                            <li {if $mcount > "6"}style="display:none"{else}style="display:block"{/if} id="recent{$mcount}">
                            <div class="brick2">
                                <div class="brick2_top"></div>
                                <div class="brick2_content"> 
                                    {if $posts[i].pic ne ""}
                                    <div class="brick_preview">
                                        <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                                            <tr><td align="center" style="vertical-align:middle"><a href="{$baseurl}/viewupdate.php?id={$posts[i].ID}"><img src="{$tpicurl}/{$posts[i].pic}" /></a></td></tr>
                                        </table> 
                                    </div>
                                    {/if}  
                                    <div class="brick_title" id="content-{$posts[i].ID}">{insert name=get_post value=a assign=mpost post=$posts[i].msg}{$mpost|stripslashes}{if $posts[i].pic eq ""} {if $posts[i].type eq "update"}<a class="moreLink" href="{$baseurl}/viewupdate.php?id={$posts[i].ID}">[{$lang94}]</a>{/if}{/if}</div>
                                    <div class="brick_byline">    
                                        {$lang343} <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$posts[i].username|stripslashes}">{$posts[i].username|stripslashes}</a>, <a class="link" href="{$baseurl}/viewupdate.php?id={$posts[i].ID}">{insert name=get_time_to_days_ago value=a time=$posts[i].time_added}{if $posts[i].edited ne ""}&nbsp;&nbsp;&nbsp;<span class="edittime">[{$lang112}:{insert name=get_time_to_days_ago value=a time=$posts[i].edited}]</span>{/if}</a>
                                    </div>     
                                </div>
                                <div class="brick2_btm">
                                    <div class="brick_actions">   
                                        <a href="{$baseurl}/viewupdate.php?id={$posts[i].ID}#comment" class="comment_num">{insert name=com_count value=a assign=comco ID=$posts[i].ID}{$comco}</a><a href="{$baseurl}/viewupdate.php?id={$posts[i].ID}#comment" class="commentIcon{if $comco GT "0"}{insert name=is_new_cpost value=a assign=ncpost id=$posts[i].ID}{if $ncpost eq "1"}WhatsNew{/if}{/if}"></a>
                                        <img src="{$imageurl}/icon_divider_blue.gif" width="1" height="15" alt="" class="brick_icon_divider"/><a href="{$baseurl}/viewupdate.php?id={$posts[i].ID}&r=@{$posts[i].username|stripslashes}:" class="requiresLogin">{$lang97}</a>
                                    </div>
                                </div>
                            </div>
                            </li>
                            {/section}
                        </ul>
					</div>
					<div id="sidebar">
						
						<h1>{$lang320}</h1>
						
                        {if $smarty.session.USERID eq ""}
						<div id="loginBox">
							<div class="box">
				
								<div class="top">
									<div class="floatLeft">
										{$lang321}
									</div>
									<form id="loginForm" name="loginForm" action="{$baseurl}/login.php" method="post">
										<input type="text" name="username" id="username" value="{$lang4}" maxlength="20" class="default login" onclick="clearval('username');" style="margin-right: 5px;" />
										<input type="password" name="password1" id="password1" value="{$lang6}" class="default login" onclick="clearval('password1');" />
										<input type="hidden" name="login" value="1" /> 
									</form>
									<a href="#" id="loginButton" class="yellowButton floatLeft" onclick="document.loginForm.submit();"><span>{$lang0}</span></a>
									<div class="legalText">
										<a href="{$baseurl}/terms_of_use.php" class="underline" target="_blank">{$lang322}</a>
									</div>
								</div>
								<div class="bottom"></div>
							</div>
							<div class="forgotLogin"><a href="{$baseurl}/forgot.php" class="underline">{$lang323}</a></div>
						</div>
                        {/if}
				
						<div id="joinBox">
							<div class="box">
								<div class="top">
                                	<center>
									{insert name=get_advertisement AID=7}
                                    </center>
								</div>
								<div class="bottom"></div>
							</div>
						</div>
                        
                        <h2 style="font-size:26px">{$lang347}</h2>
						{section name=i loop=$pics}
                        <div class="brick_preview2">
                            <table width="100%" height="100%" cellpadding="0" cellspacing="20">
                                <tr><td align="center" style="vertical-align:middle"><a href="{$baseurl}/viewupdate.php?id={$pics[i].ID}"><img src="{$tpicurl}/{$pics[i].pic}" /></a></td></tr>
                            </table> 
                        </div>
                        {/section}
					</div>
				</div>
                    </div>
                </div>
