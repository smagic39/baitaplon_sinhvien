                <div id="bricksPrivate">
                	{if $display eq "4"}
                    	{$lang267}.
                    {else}
                        {$lang263}. {if $display eq "2"}{$lang264} {$p.username|stripslashes}.{elseif $display eq "3"}{$lang265}.{/if}
                        {if $display eq "2"}
                            <form id="addprifr" name="addprifr" action="{$baseurl}/{insert name=get_seo_profile value=a username=$p.username|stripslashes}" method="post">
                            <input type="hidden" name="saddprifr" value="1" />
                            </form>
                        {if $msg ne ""}
                        	<div id="" class="error mainError" style="">*** {$msg} ***</div>  
                        {/if}
                        <div style="margin-top: 35px;">
                            {insert name=req_status value=var assign=rst FID=$smarty.session.USERID USERID=$USERID}
                            {if $rst GT "0"}
                            	<span style="color: #FF6600">[{$lang242}]</span>
                            {else}
                            	<a href="#" onclick="document.addprifr.submit();" class="requiresLogin yellowButton"><span>{$lang212}</span></a>
                            {/if}
                        </div>
                        {/if}
                    {/if}
        		</div>

                <div id="rightPanel">
                    <div class="contentBlock">
                        <h2 class="sidebartop">
                            <div class="userName">{$p.username|stripslashes}</div>            
                        </h2>
                        <div class="userPic">            
                            <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                                <tr><td align="center" style="vertical-align:middle"><img src="{insert name=get_propic value=var assign=mypropic USERID=$p.USERID}{$membersprofilepicurl}/{$mypropic}" class="{$p.username|stripslashes}" /></a></td></tr>
                            </table>
                        </div>
                        {if $display eq "2"}
                        <ul id="actionButtons"> 
                        	{insert name=block_cnt value=var assign=bcnt USERID=$smarty.session.USERID BID=$USERID}
                            {if $bcnt GT "0"}
                            <li class="end"><a href="{$baseurl}/privacy.php?ub={$p.USERID}"><img src="{$imageurl}/icon_block.gif" width="11" height="11"/><span>{$lang258}</span></a></li>
                            {else}
                            <li class="end"><a href="{$baseurl}/privacy.php?bname={$p.username|stripslashes}"><img src="{$imageurl}/icon_block.gif" width="11" height="11"/><span>{$lang255}</span></a></li>
                            {/if}
                        </ul>
                        {/if}
                        <div id="sidebarUserInfo">
                            {$lang266}&nbsp;<img src="{$imageurl}/icon_privacy_locked.gif" width="7" height="9" alt="" />                
                            <br />
                            <span>{$lang128}:</span> {$p.addtime|date_format}<br />
                        </div>
                        <div class="byline">
                            {insert name=get_updatecount value=var assign=updatecount USERID=$p.USERID}{$updatecount} {$lang117} | {insert name=get_followerscount value=var assign=frscount USERID=$p.USERID}{$frscount} {$lang118}
                        </div>
                    </div>
                </div>

                    </div>
                </div>