		<div class="middle" id="anchor-content">
            <div id="page:main-container">
				<div class="columns ">
                
					<div class="side-col" id="page:left">
    					<h3>Messages</h3>
						
                        <ul id="isoft" class="tabs">
    						<li >
        						<a href="messages_manage.php" id="isoft_group_1" name="group_1" title="Manage Messages" class="tab-item-link ">
                                    <span>
                                        <span class="changed" title=""></span>
                                        <span class="error" title=""></span>
                                        Manage Messages
                                    </span>
        						</a>
                                <div id="isoft_group_1_content" style="display:none;"></div>
    						</li>
                            
                            <li >
                                <a href="messages_send.php" id="isoft_group_2" name="group_2" title="Send Message" class="tab-item-link">
                                	<span>
                                    	<span class="changed" title=""></span>
                                        <span class="error" title=""></span>
                                        Send Message
                                    </span>
                                </a>
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                <div id="isoft_group_2_content" style="display:none;">
                                	<div class="entry-edit">
                                        <div class="entry-edit-head">
                                            <h4 class="icon-head head-edit-form fieldset-legend">Send Message</h4>
                                            <div class="form-buttons">

                                            </div>
                                    	</div>

                                        <fieldset id="group_fields4">
                                            <div class="hor-scroll">
                                                <table cellspacing="0" class="form-list">
                                                <tbody>
                                                    <tr class="hidden">
                                                        <td class="label"><label for="status">To </label></td>
                                                        <td class="value">
                                                            <select name="TOUSERID" id="TOUSERID">
                                                            <option value="0">All Members</option>
                                                            {insert name=get_all_members assign=listallmembers}
                                                            {section name=i loop=$listallmembers}
                                                            <option value="{$listallmembers[i].USERID}" {if $smarty.request.TOUSERID eq $listallmembers[i].USERID}selected{/if}>{$listallmembers[i].username|stripslashes}</option>
                                                            {/section}
                                                            </select>
                                                            <div style="padding-bottom:5px;"></div>
                                                            <input type="checkbox" name="active" id="active" {if $smarty.request.active eq "on"}checked="checked"{/if} /> Active Only *
                                                            <div style="padding-bottom:5px;"></div>
                                                            <input type="checkbox" name="verified" id="verified" {if $smarty.request.verified eq "on"}checked="checked"{/if} /> Verified Only *
                                                        </td>
                                                        <td class="scope-label">[WHO TO SEND THE MESSAGE TO]</td>
                                                        <td><small></small></td>
                                                    </tr>
                                                    <tr class="hidden">
                                                        <td class="label"><label for="status">From </label></td>
                                                        <td class="value">
                                                            <select name="FROMUSERID" id="FROMUSERID">
                                                            {insert name=get_all_members2 assign=listallmembers}
                                                            {section name=i loop=$listallmembers}
                                                            <option value="{$listallmembers[i].USERID}" {if $smarty.request.FROMUSERID eq $listallmembers[i].USERID}selected{/if}>{$listallmembers[i].username|stripslashes}</option>
                                                            {/section}
                                                            </select>
                                                        </td>
                                                        <td class="scope-label">[WHO TO SEND THE MESSAGE FROM]</td>
                                                        <td><small></small></td>
                                                    </tr>
                                                    <tr class="hidden">
                                                        <td class="label"><label for="status">Subject </label></td>
                                                        <td class="value">
                                                            <input id="subject" name="subject" value="{$smarty.request.subject|stripslashes}" class=" required-entry required-entry input-text" type="text"/>
                                                        </td>
                                                        <td class="scope-label">[PRIVATE MESSAGE SUBJECT]</td>
                                                        <td><small></small></td>
                                                    </tr>
                                                    <tr class="hidden">
                                                        <td class="label"><label for="name">Message </label></td>
                                                        <td class="value">
                                                        	<textarea id="msg" name="msg" class=" textarea" type="textarea" >{$smarty.request.msg|stripslashes}</textarea>
                                                        </td>
                                                        <td class="scope-label">[FULL PRIVATE MESSAGE]</td>
                                                            <td><small></small></td>
                                                    </tr>
                                                    <tr class="hidden">
                                                    	<td colspan="4"><label for="name">* Options only apply if sending a message to All Members.</label></td>
                                                    </tr>
                                                </tbody>
                                                </table>
                                            </div>
                                        </fieldset>
									</div>
								</div>
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                               
                                
                                
                                
                                
                                
                                
                            </li>
    
						</ul>
                        
						<script type="text/javascript">
                            isoftJsTabs = new varienTabs('isoft', 'main_form', 'isoft_group_2', []);
                        </script>
                        
					</div>
                    
					<div class="main-col" id="content">
						<div class="main-col-inner">
							<div id="messages">
                            {if $message ne "" OR $error ne ""}
                            	{include file="administrator/show_message.tpl"}
                            {/if}
                            </div>

                            <div class="content-header">
                               <h3 class="icon-head head-products">Messages - Send Message</h3>
                               <p class="content-buttons form-buttons">
                                    <button  id="id_be616be1324d8ae4516f276d17d34b9c" type="button" class="scalable save" onclick="document.main_form.submit();" style=""><span>Send Message</span></button>			
                                </p>
                            </div>
                            
                            <form action="messages_send.php" method="post" id="main_form" name="main_form" enctype="multipart/form-data">
                            	<input type="hidden" id="submitform" name="submitform" value="1" >
                            	<div style="display:none"></div>
                            </form>
						</div>
					</div>
				</div>

                        </div>
        </div>