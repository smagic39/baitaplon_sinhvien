<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
	<title>{if $pagetitle ne ""}{$pagetitle} - {/if}{$site_name}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="keywords" content="{if $pagetitle ne ""}{$pagetitle},{/if}{if $metakeywords ne ""}{$metakeywords},{/if}{$site_name}" />
	<meta name="description" content="{if $pagetitle ne ""}{$pagetitle} - {/if}{if $metadescription ne ""}{$metadescription} - {/if}{$site_name}" />
	<link media="screen" rel="stylesheet" type="text/css" href="{$cssurl}/scritter.php" />
    <!--[if lt IE 7.]>
      <script defer type="text/javascript" src="{$baseurl}/js/pngfix.js"></script>
      <link type="text/css" rel="stylesheet" href="{$cssurl}/scritter_ie6.php" />
    <![endif]-->
    <link rel="icon" href="{$baseurl}/favicon.ico" />
    <link rel="shortcut icon" href="{$baseurl}/favicon.ico" />
    
    {literal}
	<script language="javascript" type="text/javascript">
    function toggle(obj) {
        if ( document.getElementById(obj).style.display != 'none' ) {
            document.getElementById(obj).style.display="none";
        }
        else {
            document.getElementById(obj).style.display="block";
        }
    }
    </script>
    {/literal}

    <script type='text/javascript' src='{$baseurl}/js/jquery.js'></script>
    
    {literal}
	<script language="javascript" type="text/javascript">
	function toggle2(obj) {
		var el = document.getElementById(obj);
		if ( el.style.display != 'none' ) {
			$('#' + obj).slideUp();
		}
		else {
			$('#' + obj).slideDown();
		}
	}
    </script>
    {/literal}
</head>

<body id="body" style="{$custombg}">
    <div id="mainCenter">
        <div id="mainContainer">

            <div id="nav">
            	<a href="{$baseurl}{if $smarty.session.USERID ne ""}/home.php{/if}" title="scritter" class="navLogo"></a>
                {if $smarty.session.USERID ne ""}
                <div class="loggedIn">
                    {$lang48}, {$smarty.session.USERNAME|stripslashes}!<span>|</span><a href="{$baseurl}/logout.php">{$lang49}</a>
                </div>
                {else}
                <div class="loggedIn">
                    <a href="{$baseurl}/register.php">{$lang12}</a><span>|</span><a href="{$baseurl}/login.php">{$lang0}</a>
                </div>
                {/if}

            	<div class="mainArea">                    
                	<div class="buttons">
                    	{if $smarty.session.USERID ne ""}
                        {insert name=new_mail value=var assign=mcnt USERID=$smarty.session.USERID}
                        <div id="navHome" class="dropdownArea" onmouseover="toggle('homeDropdown');" onmouseout="toggle('homeDropdown');">
                            <a id="homeBtn" href="{$baseurl}/home.php" class="btn "><span>{$lang236}</span>{if $mcnt GT "0"}<div id="homeBtnIsNew"></div>{/if}</a>
                            <div id="homeDropdown" class="dropdown" style="display: none;">
                                <div class="top">
                                    <a href="{$baseurl}/home.php" class="mainLink">{$lang236}{if $mcnt GT "0"}<div id="homeBtnIsNew2"></div>{/if}</a>                                    
                                    <div class="emptySpace"></div>
                                    <div class="links">
                                        <a href="{$baseurl}/inbox.php" id="navInboxLink" {if $mcnt GT "0"}class="new"{/if}><span>{$lang217}</span></a>
                                        <a href="{$baseurl}/viewfollowers.php?id={$smarty.session.USERID}" id="navFollowingLink">{$lang118}</a>
                                        <a href="{$baseurl}/viewfollowing.php?id={$smarty.session.USERID}" id="navFollowingLink">{$lang120}</a>
                                        <a href="{$baseurl}/viewupdates.php?id={$smarty.session.USERID}">{$lang286}</a>
                                        <a href="{$baseurl}/widget?id={$smarty.session.USERID}">{$lang345}</a>
                                        <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$smarty.session.USERNAME|stripslashes}">{$lang285}</a>
                                    </div>
                                </div>
                                <div class="btm"></div>
                            </div>
                        </div>
                        {/if}
	                    <div id="navTopics" class="dropdownArea" onmouseover="toggle('topicsDropdown');" onmouseout="toggle('topicsDropdown');">                        
                        	<a id="topicsBtn" href="{$baseurl}/recentupdates.php" class="btn "><span>{$lang295}</span></a>
                        	<div id="topicsDropdown" class="dropdown" style="display: none;">
                            	<div class="top">
                                	<a href="{$baseurl}/recentupdates.php" class="mainLink">{$lang295}</a>
                                    <div class="emptySpace"></div>
                                    <div class="links">
                                        <a href="{$baseurl}/recentupdates.php">{$lang296}</a>
                                        <a href="{$baseurl}/newmembers.php">{$lang297}</a>
	                                </div>
    	                        </div>
        	                    <div class="btm"></div>
            	            </div>
                	    </div>
                    	{if $smarty.session.USERID ne ""}
                        <div id="navSettings" class="dropdownArea"  onmouseover="toggle('settingsDropdown');" onmouseout="toggle('settingsDropdown');">                            
                            <a id="settingsBtn" href="{$baseurl}/myprofile.php" class="btn icon " title="{$lang148}"><span><div class="settingsIcon"></div></span></a>
                            <div id="settingsDropdown" class="dropdown" style="display: none;">
                                <div class="top">
                                    <a href="{$baseurl}/myprofile.php" class="icon" title="{$lang148}"></a>
                                    <div class="emptySpace"></div>
                                    <div class="links">
                                        <a href="{$baseurl}/myprofile.php">{$lang147}</a>
                                        <a href="{$baseurl}/design.php">{$lang149}</a>
                                        <a href="{$baseurl}/privacy.php">{$lang150}</a>
                                        <a href="{$baseurl}/alerts.php">{$lang151}</a>
                                    </div>
                                </div>
                                <div class="btm"></div>
                            </div>
                        </div>
                        {/if}
	                </div>
                    <div id="navSearch">
                        <div class="searchBox">
                        	<form method="POST" name="srchfrm" id="srchfrm" action="{$baseurl}/search.php">
                            <input type="text" maxlength="60" id="query" name="query" />
                            <input type="hidden" name="ssrchfrm" value="1" />
                            </form>
                        </div>
                        <a href="#" class="searchBtn" title="{$lang307}" onclick="document.srchfrm.submit()"></a>
                    </div>
                </div>
            </div>

            <div id="shadowContainer">
                <div id="mainContent" {if $ishp eq "1"}class="homepage"{/if}>