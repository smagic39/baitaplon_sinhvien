
                <div id="bricks">
                    <h2 class="viewall withPaging" style="width: 600px;">
                    <div class="pagingBlock">
                        {$pagelinks}
                    </div>
                        {$lang298}: {$lang232}
                    </h2>
                    
                    <div class="timeheading">{if $m eq "0"}{$lang299}{elseif $m eq "1"}{$lang300}{elseif $m eq "2"}{$lang301}{elseif $m eq "3"}{$lang302}{/if}</div>
                    
                    <ul class="viewallBricks">
                        <li style="margin-bottom: 15px;">
                        </li>
                 		{section name=i loop=$posts}
                 		{insert name=get_propic1 value=var assign=propic USERID=$posts[i].USERID}
                        <li class="first">
                            <div class="tinyOutline">
                                <div class="tinyuser">
                                <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$posts[i].username|stripslashes}"><img src="{$membersprofilepicurl}/thumbs/{$propic}" class="profilepic-{$posts[i].username|stripslashes}" alt="{$posts[i].username|stripslashes}" title="{$posts[i].username|stripslashes}" /></a>
                                </div>
                            </div>
                        </li>
                        <li>
                        <div class="brick">
                            <div class="brick_top"></div>
                            <div class="brick_content"> 
                            	{if $posts[i].pic ne ""}
                                <div class="brick_preview">
                                    <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                                        <tr><td align="center" style="vertical-align:middle"><a href="{$baseurl}/viewupdate.php?id={$posts[i].ID}"><img src="{$tpicurl}/{$posts[i].pic}" /></a></td></tr>
                                    </table> 
                                </div>
                                {/if}  
                                <div class="brick_title" id="content-{$posts[i].ID}">{insert name=get_post value=a assign=mpost post=$posts[i].msg}{$mpost|stripslashes}{if $posts[i].pic eq ""} {if $posts[i].type eq "update"}<a class="moreLink" href="{$baseurl}/viewupdate.php?id={$posts[i].ID}">[{$lang94}]</a>{/if}{/if}</div>
                                <div class="brick_byline">    
                                    {$lang343} <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$posts[i].username|stripslashes}">{$posts[i].username|stripslashes}</a>, <a class="link" href="{$baseurl}/viewupdate.php?id={$posts[i].ID}">{insert name=get_time_to_days_ago value=a time=$posts[i].time_added}{if $posts[i].edited ne ""}&nbsp;&nbsp;&nbsp;<span class="edittime">[{$lang112}:{insert name=get_time_to_days_ago value=a time=$posts[i].edited}]</span>{/if}</a>
                                </div>     
                            </div>
                            <div class="brick_btm">
                                <div class="brick_actions">   
                                    <a href="{$baseurl}/viewupdate.php?id={$posts[i].ID}#comment" class="comment_num">{insert name=com_count value=a assign=comco ID=$posts[i].ID}{$comco}</a><a href="{$baseurl}/viewupdate.php?id={$posts[i].ID}#comment" class="commentIcon{if $comco GT "0"}{insert name=is_new_cpost value=a assign=ncpost id=$posts[i].ID}{if $ncpost eq "1"}WhatsNew{/if}{/if}"></a>
                                	<img src="{$imageurl}/icon_divider_blue.gif" width="1" height="15" alt="" class="brick_icon_divider"/><a href="{$baseurl}/viewupdate.php?id={$posts[i].ID}&r=@{$posts[i].username|stripslashes}:" class="requiresLogin">{$lang97}</a>
                                </div>
                            </div>
                        </div>
                        </li>
                		{/section}
                    </ul>
                
                    <div class="pagingBlock">
                        {$pagelinks}
                    </div>
                
                </div>                
                
                {insert name=get_propic value=var assign=mypropic USERID=$p.USERID}
                <div id="rightPanel">
                    
                    <div id="filter">
                        <ul>
                        	{if $m eq "0"}
                            <li value="0" class="selected selectedfirst"><h2>{$lang299}</h2></li>
                            {else}
                            <li value="0" class="btm"><a href="{$baseurl}/recentupdates.php?m=0&si={$si}&st={$st}">{$lang299}</a></li>
                            {/if}
                            {if $m eq "1"}
                            <li value="1" class="selected selectedfirst"><h2>{$lang300}</h2></li>
                            {else}
                            <li value="1" class="btm"><a href="{$baseurl}/recentupdates.php?m=1&si={$si}&st={$st}">{$lang300}</a></li>
                            {/if}
                            {if $m eq "2"}
                            <li value="2" class="selected selectedfirst"><h2>{$lang301}</h2></li>
                            {else}
                            <li value="2" class="btm"><a href="{$baseurl}/recentupdates.php?m=2&si={$si}&st={$st}">{$lang301}</a></li>
                            {/if}
                            {if $m eq "3"}
                            <li value="3" class="selected selectedfirst"><h2>{$lang302}</h2></li>
                            {else}
                            <li value="3" class="btm"><a href="{$baseurl}/recentupdates.php?m=3&si={$si}&st={$st}">{$lang302}</a></li>
                            {/if}
                            
                            <li class="divider"></li>
                            
                            <li id="advancedForm">
                                <p style="margin-top: 8px;">
                                	<form method="post" name="filterupdates" id="filterupdates" action="{$baseurl}/recentupdates.php?m={$m}">
                                  	<input style="margin-left: 0px; vertical-align: middle;" type="checkbox" name="si" value="1" {if $si eq "1"}checked{/if}/><label for="si">{$lang303}</label>
                                  	<br/>
                                  	<input style="margin-left: 0px; vertical-align: middle;" type="checkbox" name="st" value="1" {if $st eq "1"}checked{/if}/><label for="st">{$lang304}</label>
                                    <input type="hidden" name="sfilterupdates" value="1" />
                                    </form>
                                </p>
                                <p>
                                  <a href="#" onclick="document.filterupdates.submit()" class="standardButton update"><span>{$lang305}</span></a>
                                </p>
                            </li>
                        </ul>
                    </div>
                    
                    <div style="clear:both;"></div>
    				<div class="divider dividerFilterAd"></div>
                    <div class="adArea">
						{insert name=get_advertisement AID=1}
                    </div>
                    
				</div>
  
                <div class="clear">&nbsp;</div>
            </div>
        </div>