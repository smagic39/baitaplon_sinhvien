				<div id="bricks" class="search">
    				<h2 style="margin-bottom: 12px">
                        {$total} {$lang309} <font color="#dfdfdf">|</font> {$lang308} "{$query}"
                    </h2>
                    
                    <div class="subheading">
                        <div>{$mtotal} {$lang310}</div>
                            <a href="{$baseurl}/searchchannels.php?query={$query|urlencode}" class="viewLink">{$lang107}</a>
                    </div>
                        
				    <ul class="viewallBricks">
                        <li style="margin-bottom: 15px;">                        
                        </li>
            
                        {section name=i loop=$mposts}
                        {insert name=get_propic2 value=var assign=propic USERID=$mposts[i].USERID}
                        <li>
                        	<div class="userBrick">
                            	<div class="userBrick_top"></div>
                            	<div class="userBrick_content">
                                	<div class="brick_preview">
                                        <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                                            <tr><td align="center" style="vertical-align:middle"><a href="{$baseurl}/{insert name=get_seo_profile value=a username=$mposts[i].username|stripslashes}" title="{$mposts[i].username|stripslashes}"><img src="{$membersprofilepicurl}/thumbs/{$propic}" /></a></td></tr>
                                        </table> 
	                               	</div>
									<div class="userInfo">
                                        <div class="title">
                                          <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$mposts[i].username|stripslashes}">{$mposts[i].username|stripslashes}</a>
                                        </div>
                                    	<div class="motto">{if $mposts[i].saying ne ""}"{$mposts[i].saying|stripslashes}"{/if}</div> 
	                                    <div class="infoline">
                                            <div class="floatLeft">{if $mposts[i].city ne ""}{$mposts[i].city|stripslashes}, {/if}{$mposts[i].country|stripslashes}</div>
                                            <div class="pipe">&nbsp;</div><img src="{$imageurl}/icon_friends.gif" width="18" height="13" alt="{$lang136}" title="{$lang136}" class="friendIcon"/></div>
                                        </div>
                                        {insert name=get_lastupdate value=var assign=up USERID=$mposts[i].USERID}
                                        {if $up[0].ID ne ""}
                                        <div class="latestPost">
                                            <div class="latestPost_content">
                                            	{if $up[0].pic ne ""}
                                            	<div class="latestPost_icon outline">
                                                    <div class="tinyuser">
                                                    <a href="{$baseurl}/viewupdate.php?id={$up[0].ID}" title="{$up[0].msg|stripslashes|truncate:75:"...":true}"><img src="{$tpicurl}/small_{$up[0].pic}" /></a>
                                                    </div>
                                                </div>
												{/if}
                                                {if $mposts[i].public eq "0"}
                                                <div class="latestPost_title">{$lang263}.</div>
                                                {else}
                                                <div class="latestPost_title"><a href="{$baseurl}/viewupdate.php?id={$up[0].ID}">{$up[0].msg|stripslashes|truncate:75:"...":true}</a></div>
                                                <div class="byline">{insert name=get_time_to_days_ago value=a time=$up[0].time_added}</div>
                                                {/if}
                                            </div>
                                        </div>
                                        {else}
                                        <div class="latestPostDefault">
                                            <div class="latestPost_content">
                                                <div class="latestPost_titleDefault">
                                                    {$lang306}
                                                </div> 
                                            </div>
                                        </div>
                                        {/if}
                                    </div>
			                </div>
            	        </li>
   						{/section}
    				</ul>
                    
                    <div class="subheading separator">
                        <div>{$utotal} {$lang117}</div>
                            <a href="{$baseurl}/searchupdates.php?query={$query|urlencode}" class="viewLink">{$lang107}</a>
                    </div>
                    
                    <ul class="viewallBricks">
                        <li style="margin-bottom: 15px;">
                        </li>
                 		{section name=i loop=$uposts}
                 		{insert name=get_propic1 value=var assign=propic USERID=$uposts[i].USERID}
                        <li class="first">
                            <div class="tinyOutline">
                                <div class="tinyuser">
                                <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$uposts[i].username|stripslashes}"><img src="{$membersprofilepicurl}/thumbs/{$propic}" class="profilepic-{$uposts[i].username|stripslashes}" alt="{$uposts[i].username|stripslashes}" title="{$uposts[i].username|stripslashes}" /></a>
                                </div>
                            </div>
                        </li>
                        <li>
                        <div class="brick">
                            <div class="brick_top"></div>
                            <div class="brick_content"> 
                            	{if $uposts[i].pic ne ""}
                                <div class="brick_preview">
                                    <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                                        <tr><td align="center" style="vertical-align:middle"><a href="{$baseurl}/viewupdate.php?id={$uposts[i].ID}"><img src="{$tpicurl}/{$uposts[i].pic}" /></a></td></tr>
                                    </table> 
                                </div>
                                {/if}  
                                <div class="brick_title" id="content-{$uposts[i].ID}">{insert name=get_post value=a assign=mpost post=$uposts[i].msg}{$mpost|stripslashes}{if $uposts[i].pic eq ""} {if $uposts[i].type eq "update"}<a class="moreLink" href="{$baseurl}/viewupdate.php?id={$uposts[i].ID}">[{$lang94}]</a>{/if}{/if}</div>
                                <div class="brick_byline">    
                                    {$lang343} <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$uposts[i].username|stripslashes}">{$uposts[i].username|stripslashes}</a>, <a class="link" href="{$baseurl}/viewupdate.php?id={$uposts[i].ID}">{insert name=get_time_to_days_ago value=a time=$uposts[i].time_added}{if $uposts[i].edited ne ""}&nbsp;&nbsp;&nbsp;<span class="edittime">[{$lang112}:{insert name=get_time_to_days_ago value=a time=$uposts[i].edited}]</span>{/if}</a>
                                </div>     
                            </div>
                            <div class="brick_btm">
                                <div class="brick_actions">   
                                    <a href="{$baseurl}/viewupdate.php?id={$uposts[i].ID}#comment" class="comment_num">{insert name=com_count value=a assign=comco ID=$uposts[i].ID}{$comco}</a><a href="{$baseurl}/viewupdate.php?id={$uposts[i].ID}#comment" class="commentIcon{if $comco GT "0"}{insert name=is_new_cpost value=a assign=ncpost id=$uposts[i].ID}{if $ncpost eq "1"}WhatsNew{/if}{/if}"></a>
                                	<img src="{$imageurl}/icon_divider_blue.gif" width="1" height="15" alt="" class="brick_icon_divider"/><a href="{$baseurl}/viewupdate.php?id={$uposts[i].ID}&r=@{$uposts[i].username|stripslashes}:" class="requiresLogin">{$lang97}</a>
                                </div>
                            </div>
                        </div>
                        </li>
                		{/section}
                    </ul>
                
                </div>

				<div id="rightPanel">

                    <div class="adArea">
                        {insert name=get_advertisement AID=1}
                    </div>
				</div>

            </div>
		</div>