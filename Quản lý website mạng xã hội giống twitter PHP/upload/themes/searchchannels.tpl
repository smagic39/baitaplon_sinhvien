				<div id="bricks">
    				<h2 class="viewall withPaging" style="width: 600px;">
                        <div class="pagingBlock">
                            {$pagelinks}
                        </div>
                        {$total} {$lang309} <font color="#dfdfdf">|</font> {$lang308} "{$query}"
                    </h2>
                        
				    <ul class="viewallBricks">
                        <li style="margin-bottom: 15px;">                        
                        </li>
            
                        {section name=i loop=$posts}
                        {insert name=get_propic2 value=var assign=propic USERID=$posts[i].USERID}
                        <li>
                        	<div class="userBrick">
                            	<div class="userBrick_top"></div>
                            	<div class="userBrick_content">
                                	<div class="brick_preview">
                                        <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                                            <tr><td align="center" style="vertical-align:middle"><a href="{$baseurl}/{insert name=get_seo_profile value=a username=$posts[i].username|stripslashes}" title="{$posts[i].username|stripslashes}"><img src="{$membersprofilepicurl}/thumbs/{$propic}" /></a></td></tr>
                                        </table> 
	                               	</div>
									<div class="userInfo">
                                        <div class="title">
                                          <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$posts[i].username|stripslashes}">{$posts[i].username|stripslashes}</a>
                                        </div>
                                    	<div class="motto">{if $posts[i].saying ne ""}"{$posts[i].saying|stripslashes}"{/if}</div> 
	                                    <div class="infoline">
                                            <div class="floatLeft">{if $posts[i].city ne ""}{$posts[i].city|stripslashes}, {/if}{$posts[i].country|stripslashes}</div>
                                            <div class="pipe">&nbsp;</div><img src="{$imageurl}/icon_friends.gif" width="18" height="13" alt="{$lang136}" title="{$lang136}" class="friendIcon"/></div>
                                        </div>
                                        {insert name=get_lastupdate value=var assign=up USERID=$posts[i].USERID}
                                        {if $up[0].ID ne ""}
                                        <div class="latestPost">
                                            <div class="latestPost_content">
                                            	{if $up[0].pic ne ""}
                                            	<div class="latestPost_icon outline">
                                                    <div class="tinyuser">
                                                    <a href="{$baseurl}/viewupdate.php?id={$up[0].ID}" title="{$up[0].msg|stripslashes|truncate:75:"...":true}"><img src="{$tpicurl}/small_{$up[0].pic}" /></a>
                                                    </div>
                                                </div>
												{/if}
                                                {if $posts[i].public eq "0"}
                                                <div class="latestPost_title">{$lang263}.</div>
                                                {else}
                                                <div class="latestPost_title"><a href="{$baseurl}/viewupdate.php?id={$up[0].ID}">{$up[0].msg|stripslashes|truncate:75:"...":true}</a></div>
                                                <div class="byline">{insert name=get_time_to_days_ago value=a time=$up[0].time_added}</div>
                                                {/if}
                                            </div>
                                        </div>
                                        {else}
                                        <div class="latestPostDefault">
                                            <div class="latestPost_content">
                                                <div class="latestPost_titleDefault">
                                                    {$lang306}
                                                </div> 
                                            </div>
                                        </div>
                                        {/if}
                                    </div>
			                </div>
            	        </li>
   						{/section}
    				</ul>

    				<div class="pagingBlock">
        				{$pagelinks}
                    </div>
                
                </div>

				<div id="rightPanel" class="search">
    				<div class="seeMore">
                        <a href="{$baseurl}/searchupdates.php?query={$query}">{$lang312} "{$query}"</a>
                    </div>
            
                    <div class="divider"></div>
                                        
                    <h2>{$lang313}</h2>
                    <form method="post" name="filterupdates" id="filterupdates" action="{$baseurl}/searchchannels.php?query={$query}">
                    <input type="hidden" name="sfilterupdates" value="1" />
                    </form>
                    <form method="post" name="filterupdates2" id="filterupdates2" action="{$baseurl}/searchchannels.php?query={$query}">
                    <input type="hidden" name="sfilterupdates2" value="1" />
                    </form>
                    <div class="options" style="padding-bottom:10px;">
                        <input type="checkbox" onclick="document.filterupdates{if $pics eq "1"}2{/if}.submit()" name="dpics" id="dpics" {if $pics eq "1"}checked{/if}  />
                        <label for="profilePicOnlyCheckbox">{$lang314}</label>
                    </div>
    
    				<div class="divider dividerFilterAd"></div>

                    <div class="adArea">
                        {insert name=get_advertisement AID=1}
                    </div>
				</div>

            </div>
		</div>