                {if $smarty.session.USERID eq ""}
                <div id="alertBox">
                    <div class="top">
                        <div class="leftColumn">
                            <div class="profilePic"><img src="{insert name=get_propic value=var assign=propic1 USERID=$p.USERID}{$membersprofilepicurl}/thumbs/{$propic1}" alt="" /></div>
                            <div class="content withProfile">
                                <div class="heading">{$p.username|stripslashes} {$lang121}.</div>
                                {$lang122}<br />
                        		<a href="{$baseurl}/login.php">{$lang123}</a>
                            </div>
                        </div>
                        <div class="rightColumn">
                        	<a href="{$baseurl}/login.php" class="yellowButton"><span>{$lang0}</span></a>                                
                        </div>
                    </div>
                    <div class="bottom"></div>
                </div>
                {/if}

                <div id="bricks">
                    <h2>{$lang141}</h2>
                    {literal}
					<script language="javascript" type="text/javascript">
                    function tClass(obj) {
                        var el = document.getElementById(obj);
                        if ( el.className != 'settingsBtn closed tooltip' ) {
                            el.setAttribute("class", "settingsBtn closed tooltip");
                        }
						else
						{
							el.setAttribute("class", "settingsBtn open tooltip");
						}
                    }
                    </script>
                    {/literal}
					{if $smarty.session.USERID ne "" AND $smarty.session.USERID GT "0"}
					{if $smarty.session.USERID ne $USERID}
					{insert name=is_following value=var assign=isf FID=$smarty.session.USERID USERID=$USERID}
                    {if $isf  eq "0"}
                    <div id="followUserBtn">   
						<form id="subfolform" name="subfolform" action="{$baseurl}/{insert name=get_seo_profile value=a username=$p.username|stripslashes}" method="post">
						<input type="hidden" name="subfollow" value="1" />
						</form>    
                        <a id="followButton" class="smallYellowButton tooltip requiresLogin" style="" href="#" onclick="document.subfolform.submit();">
                            <span>{$lang143} {$p.username|stripslashes}</span>
                            <div class="tt">
                                <div class="top"></div>
                                <div class="middle">
                                    {$lang142}
                                </div>
                                <div class="bottom"></div>
                            </div>
                        </a>
                    </div>
					{elseif $isf GT "0"}
                    <div id="followUserBtn">  
						<form id="subufolform" name="subufolform" action="{$baseurl}/{insert name=get_seo_profile value=a username=$p.username|stripslashes}" method="post">
						<input type="hidden" name="subufollow" value="1" />
						</form>   
                        <a id="followingButton" class="settingsBtn closed tooltip" href="#" onclick="toggle2('followSettings'); tClass('followingButton');">
                            <span>{$lang120} {$p.username|stripslashes}</span>
                            <div class="tt">
                                <div class="top"></div>
                                <div class="middle">
                                    {$lang145} {$p.username|stripslashes}. {$lang146}
                                </div>
                                <div class="bottom"></div>
                            </div>
                        </a>
    				</div>
                    
                    {insert name=is_friend value=var assign=myfr FID=$smarty.session.USERID USERID=$USERID}
                    
                    <form id="addfrform" name="addfrform" action="{$baseurl}/{insert name=get_seo_profile value=a username=$p.username|stripslashes}" method="post">
                    <input type="hidden" name="saddfr" value="1" />
                    </form>
                    <form id="remfrform" name="remfrform" action="{$baseurl}/{insert name=get_seo_profile value=a username=$p.username|stripslashes}" method="post">
                    <input type="hidden" name="sremfr" value="1" />
                    </form>
                    <div id="followSettings" style="display:none;">
                        <a href="#" onclick="toggle2('followSettings'); tClass('followingButton');" class="close"><img src="{$imageurl}/icon_close.gif" width="7" height="7" alt="" /></a>
                        <div class="heading">{if $myfr  eq "0"}{$lang214}{else}{$lang215}{/if}:</div>
                        <div id="settingsArea" class="floatLeft">        
                        	<a href="#" onclick="document.subufolform.submit();" class="link">{$lang211}</a><br />
                            {if $myfr  eq "0"}
                            	{insert name=req_status value=var assign=rst FID=$smarty.session.USERID USERID=$USERID}
                                {if $rst GT "0"}
                                [{$lang242}]<br>
                                {else}
                            	<a href="#" onclick="document.addfrform.submit();" class="link">{$lang212}</a>
                                {/if}
                            {else}               
                            <a href="#" onclick="document.remfrform.submit();" class="link">{$lang213}</a>
                            {/if}
                        </div>
                    </div>
                    {/if}
					{/if}
					{/if}
                    
                    {if $msg ne ""}
                    <div id="" class="error mainError" style="">*** {$msg} ***</div>  
                    {/if}
                    
                    <div style="padding-bottom:10px;"></div>
                    <ul class="viewallBricks">
                 		{section name=i loop=$posts}
                 		{insert name=get_propic1 value=var assign=propic USERID=$posts[i].USERID}
                        {if $smarty.section.i.iteration ne "1"}
                        <li class="first">
                            <div class="tinyOutline">
                                <div class="tinyuser">
                                <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$posts[i].username|stripslashes}"><img src="{$membersprofilepicurl}/thumbs/{$propic}" class="profilepic-{$posts[i].username|stripslashes}" alt="{$posts[i].username|stripslashes}" title="{$posts[i].username|stripslashes}" /></a>
                                </div>
                            </div>
                        </li>
                        {/if}
                        <li>
                            <div class="{if $smarty.section.i.iteration ne "1"}brick{else}largeBrick{/if}">
                                <div class="brick_top"></div>
                                <div class="brick_content"> 
                                    {if $posts[i].pic ne ""}
                                    <div class="brick_preview">
                                        <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                                            <tr><td align="center" style="vertical-align:middle"><a href="{$baseurl}/viewupdate.php?id={$posts[i].ID}"><img src="{$tpicurl}/{$posts[i].pic}" /></a></td></tr>
                                        </table> 
                                    </div>
                                    {/if}  
                                    <div class="brick_title" id="content-{$posts[i].ID}">{insert name=get_post value=a assign=mpost post=$posts[i].msg}{$mpost|stripslashes}{if $posts[i].pic eq ""} {if $posts[i].type eq "update"}<a class="moreLink" href="{$baseurl}/viewupdate.php?id={$posts[i].ID}">[{$lang94}]</a>{/if}{/if}</div>
                                    <div class="brick_byline">    
                                        <a class="link" href="{$baseurl}/viewupdate.php?id={$posts[i].ID}">{insert name=get_time_to_days_ago value=a time=$posts[i].time_added}{if $posts[i].edited ne ""}&nbsp;&nbsp;&nbsp;<span class="edittime">[{$lang112}:{insert name=get_time_to_days_ago value=a time=$posts[i].edited}]</span>{/if}</a>
                                    </div>     
                                </div>
                                <div class="brick_btm">
                                    <div class="brick_actions">   
                                        <a href="{$baseurl}/viewupdate.php?id={$posts[i].ID}#comment" class="comment_num">{insert name=com_count value=a assign=comco ID=$posts[i].ID}{$comco}</a><a href="{$baseurl}/viewupdate.php?id={$posts[i].ID}#comment" class="commentIcon{if $comco GT "0"}{insert name=is_new_cpost value=a assign=ncpost id=$posts[i].ID}{if $ncpost eq "1"}WhatsNew{/if}{/if}"></a>
                                        <img src="{$imageurl}/icon_divider_blue.gif" width="1" height="15" alt="" class="brick_icon_divider"/><a href="{$baseurl}/viewupdate.php?id={$posts[i].ID}&r=@{$posts[i].username|stripslashes}:" class="requiresLogin">{$lang97}</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                		{/section}
                    </ul>
                
                    {if $total GT $max_posts_userupdates}<div class="more2"><a href="{$baseurl}/viewupdates.php?id={$USERID}&page=2">{$lang144}</a>&nbsp;<img src="{$imageurl}/arrow.gif" width="3" height="6" alt="{$lang107}"/></div>{/if}
                
                </div>                
                
                {insert name=get_propic value=var assign=mypropic USERID=$p.USERID}
                <div id="rightPanel">
                    <div class="contentBlock">
                        <h2 class="sidebartop">
                            <div class="userName"><a href="{$baseurl}/{insert name=get_seo_profile value=a username=$p.username|stripslashes}">{$p.username|stripslashes}</a></div>
                        </h2>
                        <div class="userPic">            
                            <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                                <tr><td align="center" style="vertical-align:middle"><a href="{$baseurl}/{insert name=get_seo_profile value=a username=$p.username|stripslashes}"><img src="{$membersprofilepicurl}/{$mypropic}" class="{$p.username|stripslashes}" /></a></td></tr>
                            </table>
                        </div>
                
                        <div class="motto">{if $p.saying ne ""}"{$p.saying|stripslashes}"{/if}</div>
                        {if $smarty.session.USERID ne ""}
                        <ul id="actionButtons">            
                            <li><a href="#" onclick="toggle2('smsg');" class="requiresLogin" title="{$lang57}"><img src="{$imageurl}/icon_message.gif" width="13" height="11"/><span>{$lang57}</span></a></li>  
                            {insert name=block_cnt value=var assign=bcnt USERID=$smarty.session.USERID BID=$USERID}
                            {if $bcnt GT "0"}
                            <li class="end"><a href="{$baseurl}/privacy.php?ub={$p.USERID}"><img src="{$imageurl}/icon_block.gif" width="11" height="11"/><span>{$lang258}</span></a></li>
                            {else}
                            <li class="end"><a href="{$baseurl}/privacy.php?bname={$p.username|stripslashes}"><img src="{$imageurl}/icon_block.gif" width="11" height="11"/><span>{$lang255}</span></a></li>
                            {/if}           
                        </ul>
                        <div id="smsg" {if $mserr eq ""}style="display:none;"{/if}>
                            <a href="#" onclick="toggle2('smsg');" class="close"><img src="{$imageurl}/icon_close.gif" width="7" height="7" alt="" /></a>
                            <div class="heading"><b>{$lang237} {$p.username|stripslashes}:</b></div>
                            <div class="floatLeft"> 
                            	{if $mserr ne ""}
                                <div id="" class="error mainError" style="">*** {$mserr} ***</div>  
                                {/if}
                            	<form id="sendmsg" name="sendmsg" action="{$baseurl}/{insert name=get_seo_profile value=a username=$p.username|stripslashes}" method="post">
                            	<div style="padding-bottom:5px;"></div>  
                            	{$lang238}:<br />
                                <input type="text" name="mysub" value="{$mysub|stripslashes}" /><br />     
                                <div style="padding-bottom:10px;"></div> 
                                {$lang57}:<br />
                                <textarea name="mymsg">{$mymsg|stripslashes}</textarea>
                                <input type="hidden" name="ssendmsg" value="1" />
                            	</form>
                                <div style="padding-bottom:10px;"></div> 
                                <a class="mediumButton" href="#" onclick="document.sendmsg.submit();"><span>{$lang239}</span></a>
                            </div>
                        </div>
                        {/if}
                
                        <div id="sidebarUserInfo">
                            {if $p.gender ne ""}<span>{$lang125}:</span> {if $p.gender eq "1"}{$lang126}{elseif $p.gender eq "0"}{$lang127}{/if}<br />{/if}
                            {if $p.city ne "" OR $p.country ne ""}<span>{$lang201}: </span>{if $p.city ne ""}{$p.city|stripslashes}{/if}{if $p.city ne "" AND $p.country ne ""},{/if} {if $p.country ne ""}{$p.country|stripslashes}{/if}<br />{/if}
                            {if $p.showAge eq "1" AND $p.birthday ne "0000-00-00"}<span>{$lang202}:</span> {insert name=get_age value=var assign=age DOB=$p.birthday}{$age}<br />{/if}
                            <span>{$lang128}:</span> {$p.addtime|date_format}<br />
                            {if $p.interests ne ""}<span>{$lang129}: </span>{$p.interests|stripslashes}<br />{/if}
                            {if $p.website ne ""}
                            <span class="linksLabel">{$lang130}: </span>
                            <div class="links" style="margin-bottom: 1px;">
                                <a href="{$p.website|stripslashes}" target="_blank">{$p.website|stripslashes}</a>
                            </div>
                            {/if}
                            <br />
                            <a href="{$baseurl}/widget?id={$p.USERID}">{$p.username|stripslashes}'s {$lang344}</a>
                            
                        </div>
                        <div class="byline">           
                            <a href="{$baseurl}/viewupdates.php?id={$p.USERID}">
                            {insert name=get_updatecount value=var assign=updatecount USERID=$p.USERID}{$updatecount} {$lang117}
                            </a>
                            | <a href="{$baseurl}/viewfollowers.php?id={$p.USERID}">{insert name=get_followerscount value=var assign=frscount USERID=$p.USERID}{$frscount} {$lang118}</a>
                        </div>
                    </div>
                    
                    <div class="divider"></div>
                    <div class="contentBlock">
                    	{insert name=get_following value=var assign=following USERID=$p.USERID}
                        <h2 class="sidebargrid">{$following|@count} {$lang120}</h2>
                        <div class="more"><a href="{$baseurl}/viewfollowing.php?id={$p.USERID}">{$lang107}</a>&nbsp;<img src="{$imageurl}/arrow.gif" width="3" height="6" alt=""/></div>
                        <div class="usersidebar grid">
                            <!-- loop -->
                            {section name=i loop=$following}
                            {insert name=get_propic1 value=var assign=propic USERID=$following[i].USERID}
                            <div class="gridItemContainer">
                            <div class="tinyOutline">
                            <div class="tinyuser">
                                    <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$following[i].username|stripslashes}" title="{$following[i].username|stripslashes}">
                                        <img src="{$membersprofilepicurl}/thumbs/{$propic}" class="profilepic-{$following[i].username|stripslashes}" />
                                    </a>
                            </div>
                            </div>
                            </div>
                            {/section}
                            <!-- loop end -->
                		</div>
            		</div>
				</div>
  
                <div class="clear">&nbsp;</div>
                <div id="rssArea">
                    
                    <a href="{$baseurl}/rss_updates.php?id={$p.USERID}" target="_blank"><img src="{$imageurl}/icon_rss.gif" width="15" height="15" class="rssIcon" /></a>
                    
                    <a href="{$baseurl}/rss_updates.php?id={$p.USERID}" target="_blank">{$p.username|stripslashes}'s {$lang131}</a>
                </div>
            </div>
        </div>