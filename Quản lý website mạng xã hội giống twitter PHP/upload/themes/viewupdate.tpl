					<div id="bricks">
                        <div class="largeBrick viewer">
                            <div class="brick_top"></div>
                            <div class="brick_content">
                            	
                                {if $update.pic ne ""}
                                <div class="viewerContent"><img src="{$picurl}/{$update.pic}" /></div>
								{/if}
								
                                <div class="brick_title">
                                    <div class="updatePaging floatRight">
                                    	{if $pu ne ""}
                                        <a href="{$baseurl}/viewupdate.php?id={$pu}" id="newer">&nbsp;</a>
                                        {/if}
                                        {if $nu ne ""}
                                        <a href="{$baseurl}/viewupdate.php?id={$nu}" id="older">&nbsp;</a>
                                        {/if}
                                    </div>
                                    
                                    <div id="content-ZAJ9E" class="content">
                                       {insert name=clickable_link value=var assign=$ctxt text=$update.msg|nl2br|stripslashes}{$ctxt}
                                    </div>
                                </div>                            
                            
                            <div class="brick_byline">
                                <span class="spacing">{$lang100}: {$update.time_added|date_format} {$update.time_added|date_format:"%I:%M %p"}</span>
        						<span class="spacing">|</span>
                                <span class="spacing">{$update.views} {$lang101}</span>
                                <span class="spacing">|</span>
        						<span class="spacing"><a href="#comment" class="link" id="viewerNumComments">{$pcount} {$lang102}</a></span>
                                {if $update.edited ne ""}<span class="spacing">|</span><span class="spacing"><i>{$lang112}:{insert name=get_time_to_days_ago value=a time=$update.edited}</i></span>{/if}
                        	</div>

        					<a name="comment"></a>
							
                            {literal}
							<script language="javascript" type="text/javascript">
                            function chkmsg(obj) {
                                var el = document.getElementById(obj);
                                var gmsg = el.value;
                                if ( gmsg == {/literal}'{$lang103}...'{literal} ) {
                                    el.value="";
                                }
                            }
							function reply(obj, rid) {
                                var el = document.getElementById('postComment');
                                var gmsg = el.value;
                                el.value="@"+obj+": ";
								document.comform.postComment.focus();
								document.getElementById('reply').value = rid;
                            }
                            </script>
                            {/literal}
                        
                            <div class="postBox">
                                <div class="comment_box"></div>   
                                {if $smarty.session.USERID ne ""}
                                <form method="post" name="comform" id="comform" action="{$baseurl}/viewupdate.php?id={$id}"> 
                                <script type="text/javascript" src="{$baseurl}/js/charcount.js"></script>                               
                                <textarea id="postComment" name="postComment" class="defaultText" onfocus="chkmsg('postComment');" onblur="textrem('postComment', '{$max_post_chars}');" onkeyup="textrem('postComment', '{$max_post_chars}');" onkeydown="textrem('postComment', '{$max_post_chars}');">{if $r ne ""}{$r} {else}{$lang103}...{/if}</textarea>
                                <input type="hidden" name="subupdate" value="1" />
                                <input type="hidden" name="UID" value="{$id}" />
                                <input type="hidden" name="UIDO" value="{$update.USERID}" />
                                <input type="hidden" name="reply" id="reply" value="" />
                                </form>
                                <div class="postBtn ">
                                    <a href="#" onclick="document.comform.submit()" class="requiresLogin standardButton"><span>{$lang104}</span></a>
                                </div>
                                {else}
                                <div class="disabled">                    
                                	<div class="cta"><a href="{$baseurl}/login.php">{$lang115}</a></div>
                                </div>
                                <div class="postBtn ">
                                    <a href="{$baseurl}/login.php" class="requiresLogin standardButton"><span>{$lang104}</span></a>
                                </div>
                                {/if}
                            </div>
    
                            <ul id="actionButtons" >                                
								<li class="end">
                                	<script type="text/javascript">
									addthis_url    = location.href;   
									addthis_title  = document.title;  
									addthis_pub    = 'videowatchpro';     
									</script>
									<script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
                                </li>
                            </ul>
                        </div>
                        <div class="brick_btm"></div>
                    </div>
                    
                    <div style="padding-left:25px;">
	                    <iframe src="http://www.facebook.com/plugins/like.php?href={$baseurl}/viewupdate.php?id={$update.ID}&amp;layout=standard&amp;show_faces=false&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=35" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:35px;" allowTransparency="true"></iframe>
                    </div>

                    <div id="updateCommentsArea">

    					<div class="clear">&nbsp;</div>
                        <h2>{$lang102} ({$pcount})</h2>
                        <ul>
        					
                            {section name=i loop=$posts}
                            {insert name=get_propic1 value=var assign=propic USERID=$posts[i].USERID}
                            <li class="first">
                                <div class="tinyOutline">
                                    <div class="tinyuser">
                                    <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$posts[i].username|stripslashes}"><img src="{$membersprofilepicurl}/thumbs/{$propic}" class="profilepic-{$posts[i].username|stripslashes}" alt="{$posts[i].username|stripslashes}" title="{$posts[i].username|stripslashes}" /></a>
                                    </div>
                                </div>
                            </li>

                            <li>
                            <div class="brick commentBrick">
                                <div class="brick_top"></div>
                                <div class="brick_content">
                                    <div class="brick_title">
                                        <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$posts[i].username|stripslashes}">{$posts[i].username|stripslashes}</a>:
                                        {insert name=get_post value=a assign=mpost post=$posts[i].msg|nl2br}{$mpost|stripslashes}
                                    </div>
                                    
                                    <div class="brick_byline" style="width: 100%;">
                                        <div class="floatLeft">
                                            {$posts[i].time_added|date_format} {$posts[i].time_added|date_format:"%I:%M %p"}{if $posts[i].edited ne ""}&nbsp;&nbsp;&nbsp;<span class="edittime">[{$lang112}:{insert name=get_time_to_days_ago value=a time=$posts[i].edited}]</span>{/if}                                                
                                        </div>
                                        <div class="brick_actions">
                                        
                                        	{if $smarty.session.USERID ne ""}
                                            {insert name=com_owner value=a assign=cown id=$posts[i].UID}
                                            {if $smarty.session.USERID eq $posts[i].USERID OR $smarty.session.USERID eq $cown}
                                            <div class="actionsPopupContainer"  titleHashCode="">
                                                <a class="editIcon gray" onclick="toggle('pop{$posts[i].ID}')"></a>
                                                <div class="actionsPopup" style="display: none;" id="pop{$posts[i].ID}">
                                                    <div class="top"></div>
                                                    <div class="body">
                                                        <div class="content">  
                                                        	<form name="delform{$posts[i].ID}" method="post" action="{$baseurl}/viewupdate.php?id={$id}">                                                            <input type="hidden" name="delcom" value="{$posts[i].ID}" />
                                                            <input type="hidden" name="subdelcom" value="1" />
                                                            </form>
                                                            <a href="#" onclick="document.delform{$posts[i].ID}.submit()">{$lang105}</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                            
                                            <img src="{$imageurl}/icon_divider_gray.gif" width="1" height="15" alt="" class="brick_icon_divider"/>
                                            {/if}
                                            {/if}
                                            <a href="#" onclick="reply('{$posts[i].username}', '{$posts[i].ID}');">{$lang97}</a>
                                        </div>
                                    </div> 
                                </div>
                                <div class="brick_btm">                                    
                                </div>
                            </div>                            
                            
                            </li>
                            {/section}
        
    					</ul>

                    </div>

				</div>

                <div id="rightPanel" class="viewer">

                    <div class="contentBlock" style="margin-top:5px;">
                        <h2 class="sidebartop"><div class="userName"><a href="{$baseurl}/{insert name=get_seo_profile value=a username=$update.username|stripslashes}">{$lang99} {$update.username|stripslashes}</a></div></h2>
                        <div class="userPic" style="margin-bottom:10px;">
                            <table width="100%" height="100%" cellpadding="0" cellspacing="0">
                                <tr><td align="center" style="vertical-align:middle"><a href="{$baseurl}/{insert name=get_seo_profile value=a username=$update.username|stripslashes}"><img src="{insert name=get_propic value=var assign=propic USERID=$update.USERID}{$membersprofilepicurl}/{$propic}" class="profilepic-{$update.username|stripslashes}" /></a></td></tr>
                            </table>
                        </div>
                        <div class="userInfo">{if $update.city ne ""}{$update.city|stripslashes}, {/if}{$update.country|stripslashes}</div>
                    </div>
                
                    <div class="divider"></div>
                    
                    <div class="contentBlock withPaging">
                        <h2>{$moreuser} {$lang106} {$update.username|stripslashes}</h2>
                        <div class="more"><a href="{$baseurl}/viewupdates.php?id={$update.USERID}">{$lang107}</a>&nbsp;<img src="{$imageurl}/arrow.gif" width="3" height="6" alt=""/></div>                               
                        <div class="gridPaging">
                        	{if $pu ne ""}
                            <div class="prevArea"><a href="{$baseurl}/viewupdate.php?id={$pu}" id="prevArrow">&nbsp;</a></div>
                            {/if}
                            <div class="grid">
                                <div id="updatesArea" class="floatLeft">
                                    <div style="width: 1px; float: left;"><img src="{$imageurl}/spacer.gif" width="1" height="1" /></div>
                                    {section name=i loop=$prev2}
                                    <div class="gridItemContainer tinyOutline"><a href="{$baseurl}/viewupdate.php?id={$prev2[i].ID}"><img src="{$imageurl}/icon_textpost.jpg" title="{$prev2[i].time_added|date_format} {$prev2[i].time_added|date_format:"%I:%M %p"} {$prev2[i].msg|stripslashes|truncate:10:"...":true}" /></a></div>
                                    {/section}
                                    {section name=i loop=$prev}
                                    <div class="gridItemContainer tinyOutline"><a href="{$baseurl}/viewupdate.php?id={$prev[i].ID}"><img src="{$imageurl}/icon_textpost.jpg" title="{$prev[i].time_added|date_format} {$prev[i].time_added|date_format:"%I:%M %p"} {$prev[i].msg|stripslashes|truncate:10:"...":true}" /></a></div>
                                    {/section}
                                    <div class="gridItemContainer tinyOutline {if $actlast eq "1"}last{/if}"><a href="{$baseurl}/viewupdate.php?id={$id}" class="selected"><img src="{$imageurl}/icon_textpost.jpg" title="{$update.time_added|date_format} {$update.time_added|date_format:"%I:%M %p"} {$update.msg|stripslashes|truncate:10:"...":true}" /></a></div>
                                    {section name=i loop=$next}
                                    <div class="gridItemContainer tinyOutline {if $smarty.section.i.iteration eq $mndd}last{/if}"><a href="{$baseurl}/viewupdate.php?id={$next[i].ID}"><img src="{$imageurl}/icon_textpost.jpg" title="{$next[i].time_added|date_format} {$next[i].time_added|date_format:"%I:%M %p"} {$next[i].msg|stripslashes|truncate:10:"...":true}" /></a></div>
                                    {/section}
                                    
                                </div>
                            </div>
                            {if $nu ne ""}
                            <div class="nextArea"><a href="{$baseurl}/viewupdate.php?id={$nu}" id="nextArrow">&nbsp;</a></div>
                            {/if}
                        </div>
                    </div>     
                                   
              	</div>
            </div>
        </div>