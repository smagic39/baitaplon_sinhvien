<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("include/config.php");
include("include/functions/import.php");

$redirect = base64_decode($_REQUEST['redirect']);
STemplate::assign('r',cleanit($_REQUEST['redirect']));
if ($_SESSION[USERID] != "" && $redirect != "")
{
	header("Location:$redirect");exit;
}

if($_REQUEST['login']!="")
{
	$username = cleanit($_REQUEST['username']);
	$username = str_replace("(", "", $username);
	$username = str_replace(")", "", $username);
	STemplate::assign('username',$username);
	$password = stripslashes(strip_tags($_REQUEST['password1']));
	STemplate::assign('pw',$password);
	
	if($username=="")
	{
		$error=$lang['15'];
	}
	elseif($password=="")
	{
		$error=$lang['20'];
	}
	else
	{
		$encryptedpassword = md5($password);
		$query="SELECT USERID,email,username,verified from members WHERE username='".mysql_real_escape_string($username)."' and password='".mysql_real_escape_string($encryptedpassword)."' AND USERID>0";
		$result=$conn->execute($query);
		
		if($result->recordcount()<1)
		{
			$error = $lang['77'];
		}
		elseif($result->fields['status']=="0")
		{
			$error = $lang['78'];
		}

		if($error=="")
		{
			$query="update members set lastlogin='".time()."', lip='".$_SERVER['REMOTE_ADDR']."' WHERE username='".mysql_real_escape_string($username)."'";
			$conn->execute($query);
	    	$_SESSION[USERID]=$result->fields['USERID'];
			$_SESSION[EMAIL]=$result->fields['email'];
			$_SESSION[USERNAME]=$result->fields['username'];
			$_SESSION[VERIFIED]=$result->fields['verified'];
			
			if($redirect == "")
			{
				header("Location:$config[baseurl]/home.php");exit;
			}
			else
			{
				header("Location:$redirect");exit;
			}

		}
	}
}

$pagetitle = $lang['0'];
STemplate::assign('pagetitle',$pagetitle);
STemplate::assign('message',$message);
STemplate::assign('error',$error);

//TEMPLATES BEGIN
STemplate::display('header.tpl');
STemplate::display('login.tpl');
STemplate::display('footer.tpl');
//TEMPLATES END
?>