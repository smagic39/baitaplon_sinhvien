<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];
$theimgurl = $config['imageurl'];

$USERID = $_SESSION['USERID'];
if ($USERID != "" && $USERID >= 0 && is_numeric($USERID))
{		
	if($_REQUEST['sbgform'] == "1")
	{
		$uploadedimage = $_FILES['bgpic']['tmp_name'];
		
		if($uploadedimage != "")
		{
			$theimageinfo = getimagesize($uploadedimage);
			$thepp = $USERID;
		
			if($theimageinfo[2] == 1)
			{
				$thepp .= ".gif";
			}
			elseif($theimageinfo[2] == 2)
			{
				$thepp .= ".jpg";
			}
			elseif($theimageinfo[2] == 3)
			{
				$thepp .= ".png";
			}
			else
			{
				$error = "$lang[198]";
			}
			
			if($error == "")
			{
				$myvideoimgnew=$config['mbgdir']."/".$thepp;
				if(file_exists($myvideoimgnew))
				{
					unlink($myvideoimgnew);
				}
				$myconvertimg = $_FILES['bgpic']['tmp_name'];
				move_uploaded_file($myconvertimg, $myvideoimgnew);
				
				if(file_exists($config['mbgdir']."/".$thepp))
				{
					$query = "UPDATE members SET bg='$thepp', showbg='1', tile='1' WHERE USERID='".mysql_real_escape_string($USERID)."'";
					$conn->execute($query);
				}
				else
				{
					$error = "$lang[207]";
				}
			}
		}
		$msg = $lang['208'];
	}	
	elseif($_REQUEST['sshowform'] == "1")
	{
		$get_tile = intval(htmlentities(strip_tags($_REQUEST[tile]), ENT_COMPAT, "UTF-8"));
		$get_showbg = intval(htmlentities(strip_tags($_REQUEST[showbg]), ENT_COMPAT, "UTF-8"));
		$query = "UPDATE members SET showbg='".mysql_real_escape_string($get_showbg)."', tile='".mysql_real_escape_string($get_tile)."' WHERE USERID='".mysql_real_escape_string($USERID)."'";
		$conn->execute($query);
		$msg = $lang['209'];
	}
	
	$query = "SELECT bg, showbg, tile FROM members WHERE USERID='".mysql_real_escape_string($USERID)."'"; 
	$executequery = $conn->execute($query);
    $p = $executequery->getarray();
	STemplate::assign('p',$p[0]);
	
	$bg = $p[0][bg];
	$showbg = $p[0][showbg];
	$tile = $p[0][tile];
	if($showbg == "1" && $bg != "")
	{
		if($tile == "1")
		{
			STemplate::assign('custombg',"background-image: url('$config[mbgurl]/$bg'); background-repeat: repeat; background-attachment: scroll; background-position: top center;");
		}
		else
		{
			STemplate::assign('custombg',"background-image: url('$config[mbgurl]/$bg'); background-repeat: no-repeat; background-attachment: fixed;");
		}
	}
	
	$templateselect = "design.tpl";
}
else
{
	$redirect = base64_encode($config['baseurl']."/design.php");
	header("Location:$config[baseurl]/login.php?redirect=$redirect");exit;
}

$pagetitle = $lang[149];
STemplate::assign('pagetitle',$pagetitle);

//TEMPLATES BEGIN
STemplate::assign('msg',$msg);
STemplate::display('header.tpl');
STemplate::display($templateselect);
STemplate::display('footer.tpl');
//TEMPLATES END
?>