<?
$config = array();

// Begin Configuration
$config['basedir']     =  '/home/username/public_html';
$config['baseurl']     =  'http://www.scritterscript.com';

$DBTYPE = 'mysql';
$DBHOST = 'localhost';
$DBUSER = 'database_username';
$DBPASSWORD = 'database_password';
$DBNAME = 'database_name';
// End Configuration

ini_set('session.save_path', $config['basedir']. '/temporary/sessions');
session_start();
date_default_timezone_set('America/New_York');

$config['adminurl']      =  $config['baseurl'].'/administrator';
$config['cssurl']      =  $config['baseurl'].'/css';
$config['imagedir']      =  $config['basedir'].'/images';
$config['imageurl']      =  $config['baseurl'].'/images';
$config['picdir']      =  $config['basedir'].'/pics';
$config['picurl']      =  $config['baseurl'].'/pics';
$config['tpicdir']      =  $config['basedir'].'/pics/thumbs';
$config['tpicurl']      =  $config['baseurl'].'/pics/thumbs';
$config['membersprofilepicdir']      =  $config['imagedir'].'/membersprofilepic';
$config['membersprofilepicurl']      =  $config['imageurl'].'/membersprofilepic';
$config['mbgdir']      =  $config['imagedir'].'/mbg';
$config['mbgurl']      =  $config['imageurl'].'/mbg';

require_once($config['basedir'].'/smarty/libs/Smarty.class.php');
require_once($config['basedir'].'/libraries/mysmarty.class.php');
require_once($config['basedir'].'/libraries/SConfig.php');
require_once($config['basedir'].'/libraries/SError.php');
require_once($config['basedir'].'/libraries/adodb/adodb.inc.php');
require_once($config['basedir'].'/libraries/phpmailer/class.phpmailer.php');
require_once($config['basedir'].'/libraries/SEmail.php');

function strip_mq_gpc($arg)
{
  if (get_magic_quotes_gpc())
  {
  	$arg = str_replace('"',"'",$arg);
  	$arg = stripslashes($arg);
    return $arg;
  } 
  else
  {
    $arg = str_replace('"',"'",$arg);
    return $arg;
  }
}

$conn = &ADONewConnection($DBTYPE);
$conn->PConnect($DBHOST, $DBUSER, $DBPASSWORD, $DBNAME);
@mysql_query("SET NAMES 'UTF8'");
$sql = "SELECT * from config";
$rsc = $conn->Execute($sql);

if($rsc){while(!$rsc->EOF)
{
$field = $rsc->fields['setting'];
$config[$field] = $rsc->fields['value'];
STemplate::assign($field, strip_mq_gpc($config[$field]));
@$rsc->MoveNext();
}}

if ($_REQUEST['language'] != "")
{
	if ($_REQUEST['language'] == "english")
	{
		$_SESSION['language'] = "english";
	}
	elseif ($_REQUEST['language'] == "spanish")
	{
		$_SESSION['language'] = "spanish";
	}
	elseif ($_REQUEST['language'] == "french")
	{
		$_SESSION['language'] = "french";
	}
}

if ($_SESSION['language'] == "")
{
	$_SESSION['language'] = "english";
}

if ($_SESSION['language'] == "english")
{
	include("lang/english.php");
}
elseif ($_SESSION['language'] == "spanish")
{
	include("lang/spanish.php");
}
elseif ($_SESSION['language'] == "french")
{
	include("lang/french.php");
}
else
{
	include("lang/english.php");
}

for ($i=0; $i<count($lang); $i++)
{
	STemplate::assign('lang'.$i, $lang[$i]);
}

STemplate::assign('baseurl',       $config['baseurl']);
STemplate::assign('basedir',       $config['basedir']);
STemplate::assign('adminurl',       $config['adminurl']);
STemplate::assign('cssurl',       $config['cssurl']);
STemplate::assign('imagedir',        $config['imagedir']);
STemplate::assign('imageurl',        $config['imageurl']);
STemplate::assign('picdir',        $config['picdir']);
STemplate::assign('picurl',        $config['picurl']);
STemplate::assign('tpicdir',        $config['tpicdir']);
STemplate::assign('tpicurl',        $config['tpicurl']);
STemplate::assign('membersprofilepicdir',        $config['membersprofilepicdir']);
STemplate::assign('membersprofilepicurl',        $config['membersprofilepicurl']);
STemplate::assign('mbgdir',        $config['mbgdir']);
STemplate::assign('mbgurl',        $config['mbgurl']);
STemplate::setCompileDir($config['basedir']."/temporary");
STemplate::setTplDir($config['basedir']."/themes");

if($sban != "1")
{
	$bquery = "SELECT count(*) as total from bans_ips WHERE ip='".mysql_real_escape_string($_SERVER['REMOTE_ADDR'])."'";
	$bresult = $conn->execute($bquery);
	$bcount = $bresult->fields['total'];
	if($bcount > "0")
	{
		$brdr = $config['baseurl']."/banned.php";exit;
		header("Location:$brdr");
	}
}

?>