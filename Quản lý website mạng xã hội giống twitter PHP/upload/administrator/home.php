<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();

function insert_get_total_v1($var)
{
        global $conn;
        $query="SELECT count(*) as total FROM posts WHERE type='update'";
        $executequery=$conn->execute($query);
        $t = $executequery->fields[total];
		echo $t+0;
}

function insert_get_total_v2($var)
{
        global $conn;
        $query="SELECT count(*) as total FROM posts WHERE type='com-update'";
        $executequery=$conn->execute($query);
        $t = $executequery->fields[total];
		echo $t+0;
}

function insert_get_total_v3($var)
{
        global $conn;
		$query="SELECT count(*) as total FROM posts WHERE reply!='0'";
        $executequery=$conn->execute($query);
        $t = $executequery->fields[total];
		echo $t+0;
}

function insert_get_total_v4($var)
{
        global $conn;
		$query="SELECT count(*) as total FROM posts WHERE pic!=''";
        $executequery=$conn->execute($query);
        $t = $executequery->fields[total];
		echo $t+0;
}

function insert_get_total_v5($var)
{
        global $conn;
        $query="SELECT count(*) as total FROM posts";
        $executequery=$conn->execute($query);
        $t = $executequery->fields[total];
		echo $t+0;
}

function insert_get_total_ads($var)
{
        global $conn;
        $query="SELECT count(*) as total FROM advertisements";
        $executequery=$conn->execute($query);
        $t = $executequery->fields[total];
		echo $t+0;
}

function insert_get_total_m3($var)
{
        global $conn;
        $query="SELECT count(*) as total FROM members WHERE USERID>0";
        $executequery=$conn->execute($query);
        $t = $executequery->fields[total];
		echo $t+0;
}

$query2 = "select username,email from members WHERE USERID>0 order by USERID desc limit 5";
$executequery2 = $conn->Execute($query2);	
$results = $executequery2->getrows();
STemplate::assign('results',$results);

$mainmenu = "1";
Stemplate::assign('mainmenu',$mainmenu);
Stemplate::assign('error',$error);
STemplate::display("administrator/global_header.tpl");
STemplate::display("administrator/home.tpl");
STemplate::display("administrator/global_footer.tpl");
?>