<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];
$theimgurl = $config['imageurl'];
	
$page = intval($_REQUEST['page']);

$m = intval($_REQUEST['m']);
if($m == "1")
{
	$lastmonth = time() - (31 * 24 * 60 * 60);
	$asql = "AND addtime>$lastmonth";
}
elseif($m == "2")
{
	$lastweek = time() - (7 * 24 * 60 * 60);
	$asql = "AND addtime>$lastweek";
}
elseif($m == "3")
{
	$aday = time() - (24 * 60 * 60);
	$asql = "AND addtime>$aday";
}

if($page=="")
{
	$page = "1";
}
$currentpage = $page;

if ($page >=2)
{
	$pagingstart = ($page-1)*$config['max_posts_newmem'];
}
else
{
	$pagingstart = "0";
}

$query1 = "SELECT USERID FROM members WHERE USERID>0 AND status='1' $asql";	

$query2 = "SELECT username, USERID, saying, city, country, public FROM members WHERE USERID>0 AND status='1' $asql order by USERID desc limit $pagingstart, $config[max_posts_newmem]";
		
$executequery1 = $conn->Execute($query1);

$totalposts = count($executequery1->getrows());
if ($totalposts > 0)
{
	if($totalposts<=$config['maximum_results'])
	{
		$total = $totalposts;
	}
	else
	{
		$total = $config[maximum_results];
	}
	
	$toppage = ceil($total/$config['max_posts_newmem']);
	if($toppage==0)
	{
		$xpage=$toppage+1;
	}
	else
	{
		$xpage = $toppage;
	}
	
	$executequery2 = $conn->Execute($query2);
	$posts = $executequery2->getrows();
	$beginning=$pagingstart+1;
	$ending=$pagingstart+$executequery2->recordcount();
	$pagelinks="";
	$k=1;
	$theprevpage=$currentpage-1;
	$thenextpage=$currentpage+1;
	
	if ($currentpage > 0)
	{
		STemplate::assign('page',$currentpage);
		if($currentpage > 1) 
		{
			$pagelinks.="<a href='$thebaseurl/newmembers.php?m=$m&page=$theprevpage' class=\"standardButton leftArrow\"><span><img src=\"$theimgurl/arrow_left.gif\" width=\"4\" height=\"8\" /></span></a>";
		}
		
		$counter=0;
		
		$lowercount = $currentpage-5;
		if ($lowercount <= 0) $lowercount = 1;
		
		while ($lowercount < $currentpage)
		{
			$pagelinks.="<a href='$thebaseurl/newmembers.php?m=$m&page=$lowercount' class=\"page\">$lowercount</a>";
			$lowercount++;
			$counter++;
		}
		
		$pagelinks.="<div class=\"currentPage\">$currentpage</div>";
		
		$uppercounter = $currentpage+1;
		
		while (($uppercounter < $currentpage+10-$counter) && ($uppercounter<=$toppage))
		{
			$pagelinks.="<a href='$thebaseurl/newmembers.php?m=$m&page=$uppercounter' class=\"page\">$uppercounter</a>";
			$uppercounter++;
		}
		
		if($currentpage < $toppage) 
		{
			$pagelinks.="<a href='$thebaseurl/newmembers.php?m=$m&page=$thenextpage' class=\"standardButton rightArrow\"><span><img src=\"$theimgurl/arrow_right.gif\" width=\"4\" height=\"8\" /></span></a>";
		}
	}
}
STemplate::assign('posts',$posts);
$templateselect = "newmembers.tpl";

$pagetitle .= $lang[297];
STemplate::assign('pagetitle',$pagetitle);

//TEMPLATES BEGIN
STemplate::assign('page',$page);
STemplate::assign('m',$m);
STemplate::assign('beginning',$beginning);
STemplate::assign('ending',$ending);
STemplate::assign('pagelinks',$pagelinks);
STemplate::assign('total',$total+0);
STemplate::assign('error',$error);
STemplate::display('header.tpl');
STemplate::display($templateselect);
STemplate::display('footer.tpl');
//TEMPLATES END
?>