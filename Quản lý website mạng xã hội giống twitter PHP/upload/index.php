<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("include/config.php");
include("include/functions/import.php");

$templateselect = "index.tpl";

$query = "SELECT DISTINCT A.*, B.username FROM posts A, members B WHERE B.status=1 AND (A.USERID=B.USERID AND B.public='1' AND A.type='update') order by A.ID desc limit 20";
$executequery = $conn->Execute($query);
$posts = $executequery->getrows();
$posts = array_reverse($posts);
STemplate::assign('posts',$posts);
STemplate::assign('total',count($posts));

$query2 = "SELECT DISTINCT A.ID, A.pic FROM posts A, members B WHERE B.status=1 AND A.pic!='' AND (A.USERID=B.USERID AND B.public='1' AND A.type='update') order by A.ID desc limit 6";
$executequery2 = $conn->Execute($query2);
$pics = $executequery2->getrows();
STemplate::assign('pics',$pics);

//TEMPLATES BEGIN
STemplate::assign('ishp','1');
STemplate::display('header.tpl');
STemplate::display($templateselect);
STemplate::display('footer.tpl');
//TEMPLATES END
?>