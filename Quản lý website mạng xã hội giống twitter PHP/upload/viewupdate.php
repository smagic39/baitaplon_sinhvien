<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];

$id = intval($_REQUEST['id']);
if($id > 0)
{
	STemplate::assign('id',$id);
	$query = "UPDATE posts SET views=views+1 WHERE ID='$id'";
	$conn->execute($query);
	
	$USERID = intval(cleanit($_SESSION['USERID']));
	if ($USERID > 0)
	{	
		if($_REQUEST['subupdate'] == "1")
		{
			$description = cleanit($_REQUEST['postComment']);
			$UID = intval($_REQUEST['UID']);
			$UIDO = intval($_REQUEST['UIDO']);
			$reply = intval($_REQUEST['reply']);
			$def = $lang['103']."...";
			if($description == $def)
			{
				$description = "";
			}
			if($description != "" && $UID > 0 && $UIDO > 0)
			{		
				$query="INSERT INTO posts SET type='com-update', USERID='".mysql_real_escape_string($USERID)."', UID='".mysql_real_escape_string($UID)."', UIDO='".mysql_real_escape_string($UIDO)."', msg='".mysql_real_escape_string($description)."', time_added='".time()."', reply='".mysql_real_escape_string($reply)."'";
				$result=$conn->execute($query);
				notify_user($description, $id);
			}
		}
		
		if($_REQUEST['subdelcom'] == "1")
		{
			$delcom = intval($_REQUEST['delcom']);
			if($delcom > 0)
			{		
				delete_com_update($delcom, $USERID);
			}
		}
	}
	
	$query = "SELECT A.*, B.username, B.city, B.country, B.public, B.addtime FROM posts A, members B WHERE A.ID='".mysql_real_escape_string($id)."' AND A.USERID=B.USERID AND A.type='update' order by ID desc limit 1";	
	$executequery = $conn->execute($query);
	$update = $executequery->getarray();
	STemplate::assign('update',$update[0]);
	
	$OID = $update[0]['USERID'];
	$public = $update[0]['public'];
	
	if($public == "0")
	{
		$ME = intval($_SESSION['USERID']);
		if($ME > 0)
		{
			if($OID == $ME)
			{
				$display = "1";
			}
			else
			{
				$is_fr = check_friend($ME, $OID);
				if($is_fr == "1")
				{
					$display = "1";
				}
				else
				{
					$display = "2";
				}
			}
		}
		else
		{
			$display = "3";
		}
	}
	else
	{
		$ME = intval($_SESSION['USERID']);
		if($ME > 0)
		{
			$query="SELECT count(*) as total FROM block WHERE USERID='".mysql_real_escape_string($OID)."' AND BID='".mysql_real_escape_string($ME)."'";
			$executequery=$conn->execute($query);
			$block_count = $executequery->fields[total];
			if($block_count > 0)
			{
				$display = "4";
			}
			else
			{
				$display = "1";
			}
		}
		else
		{
			$display = "1";
		}
	}
	
	
	if($display == "1")
	{	
		$query = "SELECT A.*, B.username FROM posts A, members B WHERE A.UID='".mysql_real_escape_string($id)."' AND A.type='com-update' AND A.USERID=B.USERID order by ID desc";	
		$executequery = $conn->execute($query);
		$posts = $executequery->getarray();
		STemplate::assign('posts',$posts);
		$pcount = count($posts);
		STemplate::assign('pcount',$pcount);
		
		$msg = $update[0]['msg'];
		$pagetitle = stripslashes($update[0]['username']);
		if($msg != "")
		{
			$pagetitle .= " - ".substr(stripslashes($msg), 0, 30);
		}
		
		$query = "SELECT count(*) as total FROM posts WHERE USERID='".mysql_real_escape_string($update[0]['USERID'])."' and type='update'";	
		$executequery = $conn->execute($query);
		$moretotal = $executequery->fields[total];
		if($moretotal > 0)
		{
			$moretotal = $moretotal - 1;
			
			$query = "SELECT ID,time_added,msg,pic FROM posts WHERE USERID='".mysql_real_escape_string($update[0]['USERID'])."' and type='update' AND id<'$id' order by ID desc limit 2";	
			$executequery = $conn->execute($query);
			$prev = $executequery->getarray();
			if(count($prev) > 0)
			{
				$pu = $prev[0]['ID'];
			}
			sort($prev);
			$ltotal = count($prev);
			$mndd = 5 - $ltotal;		
			$query = "SELECT ID,time_added,msg,pic FROM posts WHERE USERID='".mysql_real_escape_string($update[0]['USERID'])."' and type='update' AND id>'$id' limit $mndd";	
			$executequery = $conn->execute($query);
			$next = $executequery->getarray();
			$atotal = $ltotal + count($next);
			if($atotal < 5)
			{
				$togo = 5 - count($next) - $ltotal;
				$lt = $prev[0]['ID'];
				$query = "SELECT ID,time_added,msg,pic FROM posts WHERE USERID='".mysql_real_escape_string($update[0]['USERID'])."' and type='update' AND id<'$lt' order by ID desc limit $togo";	
				$executequery = $conn->execute($query);
				$prev2 = $executequery->getarray();
				sort($prev2);
				STemplate::assign('prev2',$prev2);
				if(count($next) == "0")
				{
					STemplate::assign('actlast',"1");
				}
			}
			if(count($next) > 0)
			{
				$nu = $next[0]['ID'];
			}
			$mndd = $mndd - $togo;
			STemplate::assign('mndd',$mndd);
			STemplate::assign('prev',$prev);
			STemplate::assign('next',$next);
			STemplate::assign('pu',$pu);
			STemplate::assign('nu',$nu);
		}
		STemplate::assign('moreuser',$moretotal);
		
		get_bg($update[0]['USERID']);
		$tpl = "viewupdate.tpl";
	}
	else
	{
		STemplate::assign('p',$update[0]);
		if($display == "2")
		{
			if($_REQUEST['saddprifr'] == "1")
			{
				$FID = intval($_SESSION['USERID']);
				if($FID > 0)
				{
					$fquery="INSERT INTO messages_inbox SET MSGTO='".mysql_real_escape_string($OID)."', MSGFROM='".mysql_real_escape_string($FID)."', type='fr', time='".time()."'";
					$conn->execute($fquery);
					$msg = $lang['216'];
				}
			}
		}
		STemplate::assign('display',$display);
		$tpl = "profile_private.tpl";
	}
}
else
{
	$error = $lang['98'];
	$pagetitle = $error;
	$tpl = "error.tpl";
}

$r = htmlentities(strip_tags(stripslashes($_REQUEST['r'])), ENT_COMPAT, "UTF-8");
STemplate::assign('r',$r);

//TEMPLATES BEGIN
STemplate::assign('error',$error);
STemplate::assign('pagetitle',$pagetitle);
STemplate::display('header.tpl');
STemplate::display($tpl);
STemplate::display('footer.tpl');
//TEMPLATES END
?>