<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/
include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];
$UID = intval($_REQUEST['id']);

function htmlentities2unicodeentities ($input) {
  $htmlEntities = array_values (get_html_translation_table (HTML_ENTITIES, ENT_QUOTES));
  $entitiesDecoded = array_keys   (get_html_translation_table (HTML_ENTITIES, ENT_QUOTES));
  $num = count ($entitiesDecoded);
  for ($u = 0; $u < $num; $u++) {
    $utf8Entities[$u] = '&#'.ord($entitiesDecoded[$u]).';';
  }
  return str_replace ($htmlEntities, $utf8Entities, $input);
} 

$query = "SELECT A.*, B.username FROM posts A, members B WHERE (A.USERID='".mysql_real_escape_string($UID)."' AND A.USERID=B.USERID AND A.type='update') order by A.ID desc limit 20";
$pagelink   = $config['baseurl']."/rss_updates.php?id=".$UID; 

header("Content-Type: text/xml"); 
header("Expires: 0"); 
print "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n"; 
print "<rss version=\"2.0\" xmlns:atom=\"http://www.w3.org/2005/Atom\">";
print "<channel>\n"; 
print "<atom:link href=\"$pagelink\" rel=\"self\" type=\"application/rss+xml\" />";
print "<title>".$config['short_name']." - ".get_username_from_userid($UID)." - ".$lang['124']."</title>\n"; 
print "<image>";
print "<url>".$config['membersprofilepicurl']."/thumbs/".get_propic($UID)."</url>";
print "<title>".$config['short_name']." - ".get_username_from_userid($UID)." - ".$lang['124']."</title>";
print "<link>$pagelink</link>";
print "</image>";
print "<link>$pagelink</link>\n"; 
print "<description>".$config['site_name']."</description>\n"; 

$db=mysql_connect ($DBHOST,$DBUSER,$DBPASSWORD) or die ('I cannot connect to the database because: ' . mysql_error());
mysql_select_db ($DBNAME); 
@mysql_query("SET NAMES 'UTF8'");
$result = mysql_query($query) or die ('Query Error: ' . mysql_error()); 
while ($results = mysql_fetch_array($result)) 
{ 
  $link       = $config['baseurl']."/viewupdate.php?id=".$results['ID'];
  $description = $results['msg']; 
  $getusername = $results['username'];
	print "<item>\n"; 
	print "  <title><![CDATA[".htmlentities2unicodeentities($description)."]]></title>\n";
	print "  <link>".$link."</link>\n";
	print "  <guid>".$link."</guid>\n"; 
	print "  <description>\n"; 
	print "    <![CDATA["; 
	print htmlentities2unicodeentities($description);
	$pic = $results['pic'];
	if($pic != "")
	{
		print "<br><br><a href='$link'><img src='".$config['tpicurl']."/$pic'></a>";
	}
	print "<br><br>$lang[132]: <a href='".$config['baseurl']."/$getusername'>$getusername</a>"; 
	print "<br />$lang[133] ".date("r",$results[time_added]);
	print "<br/><br/><br /><hr>"; 
	print "    ]]>\n"; 
	print "  </description>\n"; 
	print "  <author>".$getusername."</author>\n"; 
	print "</item>\n"; 
} 
mysql_close(); 
print "</channel>\n"; 
print "</rss>"; 
?> 