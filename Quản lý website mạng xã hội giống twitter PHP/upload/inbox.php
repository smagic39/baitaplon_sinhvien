<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];
$theimgurl = $config['imageurl'];

$USERID = $_SESSION['USERID'];
if ($USERID != "" && $USERID >= 0 && is_numeric($USERID))
{	
	$perpage = "10";
	$page = intval($_REQUEST['page']);

	if($page=="")
	{
		$page = "1";
	}
	$currentpage = $page;
	
	if ($page >=2)
	{
		$pagingstart = ($page-1)*$perpage;
	}
	else
	{
		$pagingstart = "0";
	}
	
	$s = htmlentities(strip_tags($_REQUEST[s]), ENT_COMPAT, "UTF-8");
	if($s == "" || $s == "r")
	{
		$s = "r";
		$asql = "A.MID";
	}
	elseif($s == "u")
	{
		$asql = "A.unread";
	}
	elseif($s == "s")
	{
		$asql = "B.username";
	}
	elseif($s == "t")
	{
		$asql = "A.type";
	}
	STemplate::assign('s',$s);
	
	$o = htmlentities(strip_tags($_REQUEST[o]), ENT_COMPAT, "UTF-8");
	if($o != "a")
	{
		$o = "z";
		$asql2 = "desc";
	}
	else
	{
		$asql2 = "asc";
	}
	STemplate::assign('o',$o);
	
	$saform = intval($_REQUEST['saform']);
	$srform = intval($_REQUEST['srform']);
	$sdelform = intval($_REQUEST['sdelform']);
	if($saform == "1")
	{
		$FID = intval($_REQUEST['FID']);
		$MID = intval($_REQUEST['MID']);
		if($FID >= 0)
		{
			$queryc = "select count(*) as total from follow where USERID='".mysql_real_escape_string($USERID)."' AND FID='".mysql_real_escape_string($FID)."'"; 
			$executequeryc = $conn->execute($queryc);
			$totalc = intval($executequeryc->fields[total]);
			if($totalc == "0")
			{
				$query="INSERT INTO follow SET friend='1', USERID='".mysql_real_escape_string($USERID)."', FID='".mysql_real_escape_string($FID)."'";
				$conn->execute($query);
				
				$queryc = "select count(*) as total from follow where FID='".mysql_real_escape_string($USERID)."' AND USERID='".mysql_real_escape_string($FID)."'"; 
				$executequeryc = $conn->execute($queryc);
				$totalc = intval($executequeryc->fields[total]);
				if($totalc == "0")
				{
					$query="INSERT INTO follow SET friend='1', FID='".mysql_real_escape_string($USERID)."', USERID='".mysql_real_escape_string($FID)."'";
					$conn->execute($query);
				}
			}
			else
			{
				$query="UPDATE follow SET friend='1' WHERE USERID='".mysql_real_escape_string($USERID)."' AND FID='".mysql_real_escape_string($FID)."'";
				$conn->execute($query);
			}
			$msg = $lang['225'];
			$query = "DELETE FROM messages_inbox WHERE MID='".mysql_real_escape_string($MID)."' AND MSGTO='".mysql_real_escape_string($USERID)."' AND MSGFROM='".mysql_real_escape_string($FID)."' limit 1";
			$conn->Execute($query);
			$query="INSERT INTO messages_inbox SET MSGTO='".mysql_real_escape_string($FID)."', MSGFROM='".mysql_real_escape_string($USERID)."', time='".time()."', type='afr'";
			$conn->execute($query);
		}
	}
	elseif($srform == "1")
	{
		$FID = intval($_REQUEST['FID']);
		$MID = intval($_REQUEST['MID']);
		if($FID >= 0)
		{
			$msg = $lang['226'];
			$query = "DELETE FROM messages_inbox WHERE MID='".mysql_real_escape_string($MID)."' AND MSGTO='".mysql_real_escape_string($USERID)."' AND MSGFROM='".mysql_real_escape_string($FID)."' limit 1";
			$conn->Execute($query);
		}
	}
	elseif($sdelform == "1")
	{
		$FID = intval($_REQUEST['FID']);
		$MID = intval($_REQUEST['MID']);
		if($FID >= 0)
		{
			$msg = $lang['230'];
			$query = "DELETE FROM messages_inbox WHERE MID='".mysql_real_escape_string($MID)."' AND MSGTO='".mysql_real_escape_string($USERID)."' AND MSGFROM='".mysql_real_escape_string($FID)."' limit 1";
			$conn->Execute($query);
		}
	}
	
	$query1 = "SELECT A.MID FROM messages_inbox A, members B WHERE (A.MSGTO='".mysql_real_escape_string($USERID)."' AND A.MSGFROM=B.USERID)";	
	
	$query2 = "SELECT A.*, B.username FROM messages_inbox A, members B WHERE (A.MSGTO='".mysql_real_escape_string($USERID)."' AND A.MSGFROM=B.USERID) order by $asql $asql2 limit $pagingstart, $perpage";
			
	$executequery1 = $conn->Execute($query1);
	
	$totalposts = count($executequery1->getrows());
	if ($totalposts > 0)
	{
		if($totalposts<=$config['maximum_results'])
		{
			$total = $totalposts;
		}
		else
		{
			$total = $config[maximum_results];
		}
		
		$toppage = ceil($total/$perpage);
		if($toppage==0)
		{
			$xpage=$toppage+1;
		}
		else
		{
			$xpage = $toppage;
		}
		
		$executequery2 = $conn->Execute($query2);
		$m = $executequery2->getrows();
		$beginning=$pagingstart+1;
		$ending=$pagingstart+$executequery2->recordcount();
		$pagelinks="";
		$k=1;
		$theprevpage=$currentpage-1;
		$thenextpage=$currentpage+1;
		
		
		// vars - s v page o
		
		if ($currentpage > 0)
		{
			if($currentpage > 1) 
			{
				$pagelinks.="<a href='$thebaseurl/inbox.php?s=$s&page=$theprevpage&o=$o' class=\"standardButton leftArrow\"><span><img src=\"$theimgurl/arrow_left.gif\" width=\"4\" height=\"8\" /></span></a>";
			}
			
			$counter=0;
			
			$lowercount = $currentpage-5;
			if ($lowercount <= 0) $lowercount = 1;
			
			while ($lowercount < $currentpage)
			{
				$pagelinks.="<a href='$thebaseurl/inbox.php?s=$s&page=$lowercount&o=$o' class=\"page\">$lowercount</a>";
				$lowercount++;
				$counter++;
			}
			
			$pagelinks.="<div class=\"currentPage\">$currentpage</div>";
			
			$uppercounter = $currentpage+1;
			
			while (($uppercounter < $currentpage+$perpage-$counter) && ($uppercounter<=$toppage))
			{
				$pagelinks.="<a href='$thebaseurl/inbox.php?s=$s&page=$uppercounter&o=$o' class=\"page\">$uppercounter</a>";
				$uppercounter++;
			}
			
			if($currentpage < $toppage) 
			{
				$pagelinks.="<a href='$thebaseurl/inbox.php?s=$s&page=$thenextpage&o=$o' class=\"standardButton rightArrow\"><span><img src=\"$theimgurl/arrow_right.gif\" width=\"4\" height=\"8\" /></span></a>";
			}
		}
		STemplate::assign('m',$m);
		
		
		$v = intval($_REQUEST['v']);
		if($v == "")
		{
			$v = $m[0][MID];
		}
		if($v != "")
		{
			STemplate::assign('v',$v);
			$query = "SELECT A.*, B.username FROM messages_inbox A, members B WHERE (A.MSGTO='".mysql_real_escape_string($USERID)."' AND A.MSGFROM=B.USERID AND A.MID='".mysql_real_escape_string($v)."') limit 1";
			$equery = $conn->Execute($query);
			$n = $equery->getrows();
			STemplate::assign('n',$n);
			
			$newmail = newmail($USERID);
			STemplate::assign('newmail',$newmail);
			
			if($n[0][MID] > 0)
			{
				update_read($n[0][MID]);
			}
			
			$sreform = intval($_REQUEST['sreform']);
			if($sreform == "1")
			{
				$FID = intval($_REQUEST['FID']);
				$replytext = cleanit($_REQUEST[replytext]);
				if($FID >= 0)
				{
					if($replytext != "")
					{
						$rsub = $lang['227'].": ".$n[0][subject];
						$query="INSERT INTO messages_inbox SET MSGTO='".mysql_real_escape_string($FID)."', MSGFROM='".mysql_real_escape_string($USERID)."', subject='".mysql_real_escape_string($rsub)."', message='".mysql_real_escape_string($replytext)."', time='".time()."'";
						$conn->execute($query);
						$msg = $lang['229'];
					}
					else
					{
						$msg = $lang['228'];
					}
				}
			}
		}
	}
	
	$templateselect = "inbox.tpl";
}
else
{
	$redirect = base64_encode($config['baseurl']."/inbox.php");
	header("Location:$config[baseurl]/login.php?redirect=$redirect");exit;
}

$pagetitle = $lang[217];
STemplate::assign('pagetitle',$pagetitle);

//TEMPLATES BEGIN
STemplate::assign('beginning',$beginning);
STemplate::assign('ending',$ending);
STemplate::assign('pagelinks',$pagelinks);
STemplate::assign('page',$page);
STemplate::assign('total',$total);
STemplate::assign('msg',$msg);
STemplate::display('header.tpl');
STemplate::display($templateselect);
STemplate::display('footer.tpl');
//TEMPLATES END
?>