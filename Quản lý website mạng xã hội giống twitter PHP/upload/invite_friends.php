<?php
/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c) 2011 ScritterScript.com. All rights reserved.
|**************************************************************************************************/

include("include/config.php");
include("include/functions/import.php");

$pagetitle = "$lang[47]";
STemplate::assign('pagetitle',$pagetitle);

$redirect = base64_encode($config['baseurl']."/invite_friends.php");

if ($_SESSION['USERID'] == "")
{
	header("Location:$config[baseurl]/login.php?redirect=$redirect");exit;
}

$thebaseurl = $config['baseurl'];

if($_REQUEST['invitesubmit'] != "")
{
	$name = cleanit($_REQUEST['name']);
	$friend_1 = cleanit($_REQUEST['friend_1']);
	$friend_2 = cleanit($_REQUEST['friend_2']);
	$friend_3 = cleanit($_REQUEST['friend_3']);
	$friend_4 = cleanit($_REQUEST['friend_4']);
	$friend_5 = cleanit($_REQUEST['friend_5']);
	$msg = cleanit($_REQUEST['message']);
	
	if($name == "")
	{
		$err0 = $lang['62'];
		STemplate::assign('err0',$err0);
	}
	
	if($friend_1 == "" && $friend_2 == "" && $friend_3 == "" && $friend_4 == "" && $friend_5 == "")
	{
		$err1 = $lang['63'];
		STemplate::assign('err1',$err1);
	}
	
	if($friend_1 != "" && !verify_valid_email($friend_1))
	{
		$err1 = $lang['64'];
		STemplate::assign('err1',$err1);
	}
	
	if($friend_2 != "" && !verify_valid_email($friend_2))
	{
		$err2 = $lang['64'];
		STemplate::assign('err2',$err2);
	}
	
	if($friend_3 != "" && !verify_valid_email($friend_3))
	{
		$err3 = $lang['64'];
		STemplate::assign('err3',$err3);
	}
	
	if($friend_4 != "" && !verify_valid_email($friend_4))
	{
		$err4 = $lang['64'];
		STemplate::assign('err4',$err4);
	}
	
	if($friend_5 != "" && !verify_valid_email($friend_5))
	{
		$err5 = $lang['64'];
		STemplate::assign('err5',$err5);
	}
	
	if($_REQUEST['verification'] != $_SESSION[imagecode])
	{
		$err6 = "$lang[27]";
		STemplate::assign('err6',$err6);
	}
	
	if($err0 == "" && $err1 == "" && $err2 == "" && $err3 == "" && $err4 == "" && $err5 == "" && $err6 == "")
	{
		if($friend_1 != "")
		{
			$sendto = $friend_1;
			$sendername = $config['site_name'];
			$from = $config['site_email'];
			$subject = $name." ".$lang['66'];
			$sendmailbody = $subject;
			$sendmailbody .= "<br><a href=".$thebaseurl."/register.php>$lang[67]</a><br><br>";
			if($msg != "")
			{
				$sendmailbody .= $name." ".$lang['68']."<br>";
				$sendmailbody .= $msg."<br><br>";
			}
			$sendmailbody .= $lang['69'].",<br>".$sendername;
			$sendmailbody = stripslashes($sendmailbody);
			mailme($sendto,$sendername,$from,$subject,$sendmailbody,$bcc="");
		}
		if($friend_2 != "")
		{
			$sendto = $friend_2;
			$sendername = $config['site_name'];
			$from = $config['site_email'];
			$subject = $name." ".$lang['66'];
			$sendmailbody = $subject;
			$sendmailbody .= "<br><a href=".$thebaseurl."/register.php>$lang[67]</a><br><br>";
			if($msg != "")
			{
				$sendmailbody .= $name." ".$lang['68']."<br>";
				$sendmailbody .= $msg."<br><br>";
			}
			$sendmailbody .= $lang['69'].",<br>".$sendername;
			$sendmailbody = stripslashes($sendmailbody);
			mailme($sendto,$sendername,$from,$subject,$sendmailbody,$bcc="");
		}
		if($friend_3 != "")
		{
			$sendto = $friend_3;
			$sendername = $config['site_name'];
			$from = $config['site_email'];
			$subject = $name." ".$lang['66'];
			$sendmailbody = $subject;
			$sendmailbody .= "<br><a href=".$thebaseurl."/register.php>$lang[67]</a><br><br>";
			if($msg != "")
			{
				$sendmailbody .= $name." ".$lang['68']."<br>";
				$sendmailbody .= $msg."<br><br>";
			}
			$sendmailbody .= $lang['69'].",<br>".$sendername;
			$sendmailbody = stripslashes($sendmailbody);
			mailme($sendto,$sendername,$from,$subject,$sendmailbody,$bcc="");
		}
		if($friend_4 != "")
		{
			$sendto = $friend_4;
			$sendername = $config['site_name'];
			$from = $config['site_email'];
			$subject = $name." ".$lang['66'];
			$sendmailbody = $subject;
			$sendmailbody .= "<br><a href=".$thebaseurl."/register.php>$lang[67]</a><br><br>";
			if($msg != "")
			{
				$sendmailbody .= $name." ".$lang['68']."<br>";
				$sendmailbody .= $msg."<br><br>";
			}
			$sendmailbody .= $lang['69'].",<br>".$sendername;
			$sendmailbody = stripslashes($sendmailbody);
			mailme($sendto,$sendername,$from,$subject,$sendmailbody,$bcc="");
		}
		if($friend_5 != "")
		{
			$sendto = $friend_5;
			$sendername = $config['site_name'];
			$from = $config['site_email'];
			$subject = $name." ".$lang['66'];
			$sendmailbody = $subject;
			$sendmailbody .= "<br><a href=".$thebaseurl."/register.php>$lang[67]</a><br><br>";
			if($msg != "")
			{
				$sendmailbody .= $name." ".$lang['68']."<br>";
				$sendmailbody .= $msg."<br><br>";
			}
			$sendmailbody .= $lang['69'].",<br>".$sendername;
			$sendmailbody = stripslashes($sendmailbody);
			mailme($sendto,$sendername,$from,$subject,$sendmailbody,$bcc="");
		}
		
		$message = $lang['65'];
		STemplate::assign('message',$message);
	}
	else
	{
		STemplate::assign('friend_1',$friend_1);
		STemplate::assign('friend_2',$friend_2);
		STemplate::assign('friend_3',$friend_3);
		STemplate::assign('friend_4',$friend_4);
		STemplate::assign('friend_5',$friend_5);
	}
}

STemplate::display('header.tpl');
STemplate::display('invite_friends.tpl');
STemplate::display('footer.tpl');
?>