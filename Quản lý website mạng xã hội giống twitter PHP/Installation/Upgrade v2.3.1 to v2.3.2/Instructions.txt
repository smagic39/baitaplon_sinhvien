/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c)  ScritterScript.com. All rights reserved.
|**************************************************************************************************/

Upgrade 2.3.1 To 2.3.2

NOTE: IF YOU HAVE THE YOUTUBE MODULE INSTALLED OR THE MOBILE MODULE INSTALLED, YOU WILL NEED TO UPGRADE THEM.

1. Upload and Replace the following files on your website from the upload folder of this package:
- /themes/administrator/global_header.tpl
- /themes/index.tpl
- home.php
- login.php
- viewupdate.php



2. Open /include/config.php
Find:
session_start();

Add Below:
date_default_timezone_set('America/New_York');



3. Run the update.sql file in your database management tool such as phpMyAdmin