/**************************************************************************************************
| Scritter Script
| http://www.scritterscript.com
| webmaster@scritterscript.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at http://www.scritterscript.com/eula.html and to be bound by it.
|
| Copyright (c)  ScritterScript.com. All rights reserved.
|**************************************************************************************************/

Upgrade 2.2 To 2.3

1. Upload and Replace the following files on your website from the upload folder of this package:
- /administrator/bans_words.php
- /administrator/bans_words_add.php
- /themes/administrator/bans_ip.tpl
- /themes/administrator/bans_ip_add.tpl
- /themes/administrator/bans_words.tpl
- /themes/administrator/bans_words_add.tpl
- /themes/design.tpl
- /themes/login.tpl
- register.php



2. Open /include/functions/main.php
Find:
?>

Add Above:
function banned_words_chk($phrase)
{
	global $conn, $config;
	$query = "SELECT word from bans_words";
	$executequery = $conn->Execute($query);
	$bwords = $executequery->getarray();
	$found = 0;
	$words = explode(" ", $phrase);
	foreach($words as $word)
	{
		foreach($bwords as $bword)
		{
			if($word == $bword[0])
			{
				$found++;
			}
			else
			{
				$pos2 = strpos($word, $bword[0]);
				if($pos2 !== false)
				{
					$found++;
				}
			}
		}
	}
	if($found > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}


Find:
function verify_valid_email($emailtocheck)
{
       $eregicheck = "^([-!#\$%&'*+./0-9=?A-Z^_`a-z{|}~])+@([-!#\$%&'*+/0-9=?A-Z^_`a-z{|}~]+\\.)+[a-zA-Z]{2,4}\$";
       return eregi($eregicheck, $emailtocheck);
}

Replace With:
function verify_valid_email($emailtocheck)
{	
	if(!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $emailtocheck))
	{
		return false;
	}
	else
	{
		return true;
	}

}



3. Open home.php
Find:
$uploadedimage = $_FILES['file']['tmp_name'];

Add Above:
elseif(banned_words_chk($description))
{
	$error = $lang['350'];
}



4. Open /include/lang/english.php
Find:
$lang['350'] =  "";

Replace With
$lang['350'] =  "Error: Your message contains banned words.";
$lang['351'] =  "";



5. Open /include/lang/french.php
Find:
$lang['350'] =  "";

Replace With
$lang['350'] =  "Erreur: Votre message contient des mots interdits.";
$lang['351'] =  "";



6. Open /include/lang/spanish.php
Find:
$lang['350'] =  "";

Replace With
$lang['350'] =  "Error: El mensaje contiene palabras prohibidas.";
$lang['351'] =  "";



7. Run the update.sql file in your database management tool such as phpMyAdmin