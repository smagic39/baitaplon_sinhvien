<?php
//generated at 17.03.2007

$lang['restore_tables_completed0']="Şimdiye kadar <b>%d</b> tablo oluşturuldu.";
$lang['file_missing']="Dosya bulunamadı";
$lang['restore_db']="Veritabanı: '<b>%s</b>' Sunucu: '<b>%s</b>'.";
$lang['restore_complete']="<b>%s</b> Tablolar oluşturuldu.";
$lang['restore_run1']="<br>Şimdiye kadar <b>%s</b> / <b>%s</b> kayıt işlendi.";
$lang['restore_run2']="<br>İşlenen tablo '<b>%s</b>' kayıtlar işleniyor.<br><br>";
$lang['restore_complete2']="<b>%s</b> Kayıtlar işlendi.";
$lang['restore_tables_completed']="Şimdiye kadar <b>%d</b> / <b>%d</b> Tablo oluşturuldu.";
$lang['restore_total_complete']="<br><b>Tebrikler.</b><br><br>Veritabanı tamamen dönüştürüldü.<br>Yedeklemedeki bulunan bütün bilgiler işlenebildi.<br><br>İşlem tamamlanmıştır. :-)";
$lang['db_select_error']="<br>Hata:<br>Veritabanı seçimi '<b>";
$lang['db_select_error2']="</b>' Hata oluştu!";
$lang['file_open_error']="Hata: Dosya açılamadı.";
$lang['progress_over_all']="Süreçin tamamı";
$lang['back_to_overview']="Veritabanı listesi";
$lang['restore_run0']="<br>Şimdiye kadar <b>%s</b> kayıt başarılı olarak işlendi.";
$lang['unknown_sqlcommand']="Tanınmayan SQL-Komandosu:";
$lang['notices']="Notlar";


?>