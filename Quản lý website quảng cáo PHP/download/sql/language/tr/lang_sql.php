<?php
//generated at 17.03.2007

$lang['command']="Kommando";
$lang['sql_view_normal']="Görünüm: normal";
$lang['sql_view_compact']="Görünüm: Kısa";
$lang['import_notable']="Yüklenecek tablo seçilmemiş!";
$lang['sql_warning']="SQL emirleriinin işlenmesi kayıtlarınıza zarar verebilir! Mysqldumper işlemden hiç bir yükümlülük kabul etmez.";
$lang['sql_exec']="SQL-komandosunu işle";
$lang['sql_dataview']="Veri görüntüsü";
$lang['sql_tableview']="Tablo görüntüsü";
$lang['sql_vonins']="/";
$lang['sql_nodata']="Kayıt bulunmuyor";
$lang['sql_recordupdated']="Kayıt değiştirildi";
$lang['sql_recordinserted']="Değişiklikler kayıt edildi";
$lang['sql_backdboverview']="Veritabanı listesine dön";
$lang['sql_recorddeleted']="Kayıt silindi";
$lang['asktableempty']="`%s` Tablo boşaltılsın mı?";
$lang['sql_recordedit']="Kayıt işleniyor";
$lang['sql_recordnew']="Kayıt ekle";
$lang['askdeleterecord']="Kayıt silinsinmi?";
$lang['askdeletetable']="`%s` Tablo silinsinmi?";
$lang['sql_befehle']="SQL-Komandoları";
$lang['sql_befehlneu']="yeni komando";
$lang['sql_befehlsaved1']="SQL-komandosu";
$lang['sql_befehlsaved2']="Eklendi";
$lang['sql_befehlsaved3']="Kayıt işlendi";
$lang['sql_befehlsaved4']="üste kaydırıldı";
$lang['sql_befehlsaved5']="silindi";
$lang['sql_queryentry']="Sorgunun içeriği";
$lang['sql_columns']="dizi";
$lang['askdbdelete']="`%s` Veritabınını içeriği ile birlikte silmekmi istiyormusun?";
$lang['askdbempty']="`%s` Veritabanının gerçekten boşaltılsınmı?";
$lang['askdbcopy']=" `%s` ın içeriği `%s` veritabanına kopyalansınmı?";
$lang['sql_tablenew']="Tablolar işle";
$lang['sql_output']="SQL-çıktısı";
$lang['do_now']="Şimdi işle";
$lang['sql_namedest_missing']="Gidilecek veritabanının ismi eksik!";
$lang['askdeletefield']="Hücre silinsinmi?";
$lang['sql_commands_in']=" Dizinler ";
$lang['sql_commands_in2']=" saniyede işlendi.";
$lang['sql_out1']=" çalıştırıldı";
$lang['sql_out2']="komandolar işlendi";
$lang['sql_out3']="It had ";
$lang['sql_out4']="notlar";
$lang['sql_out5']="Veri 5000 satırı geçtiği için gösterilmiyor.";
$lang['sql_selecdb']="Veritabanı seçiniz";
$lang['sql_tablesofdb']="Veritabanının tabloları";
$lang['sql_edit']="işle";
$lang['sql_nofielddelete']="Silinemiyor, bir tabloda en azından bir hücre bulunmalı.";
$lang['sql_fielddelete1']="Hücre";
$lang['sql_deleted']="silindi.";
$lang['sql_changed']="değiştirildi.";
$lang['sql_created']="oluşturuldu.";
$lang['sql_nodest_copy']="Hedef belirlenmediği için kopyalanamıyor!";
$lang['sql_desttable_exists']="Hedeflenen tablo zaten var!";
$lang['sql_scopy']="`%s` tabloyapısı `%s` tablosuna kopyalandı.";
$lang['sql_tcopy']="`%s` Tablosu içeriği ile `%s` tablosuna kopyalandı.";
$lang['sql_tablenoname']="Tabloya isim vermelisiniz!";
$lang['sql_tblnameempty']="Tablo isimi verilmemiş!";
$lang['sql_collatenotmatch']="Karakter Seti ve Dil birbirine uymuyor (collation)!";
$lang['sql_fieldnamenotvalid']="Hata: alanadı geçersiz";
$lang['sql_createtable']="Tablo oluştur";
$lang['sql_copytable']="Tabloyu kopyala";
$lang['sql_structureonly']="Saadece yapı";
$lang['sql_structuredata']="Yapı ve veriler";
$lang['sql_notablesindb']="Veritabanında tablo bulunmuyor";
$lang['sql_selecttable']="Tablo seç";
$lang['sql_showdatatable']="Tablonun verilerini göster";
$lang['sql_tblpropsof']="Tablo özellikleri";
$lang['sql_editfield']="Alanı işle";
$lang['sql_newfield']="yeni alan";
$lang['sql_indexes']="İndeksler";
$lang['sql_atposition']="Posisyona ekle";
$lang['sql_first']="önce";
$lang['sql_after']="sonra";
$lang['sql_changefield']="alanı işle";
$lang['sql_insertfield']="Alan ekle";
$lang['sql_insertnewfield']="Yeni alan ekle";
$lang['sql_tableindexes']="Tablo indexleri";
$lang['sql_allowdups']="çift kayıt'a müsaade et";
$lang['sql_cardinality']="Kardinality";
$lang['sql_tablenoindexes']="Tablonun indexi yok";
$lang['sql_createindex']="yeni index oluştur";
$lang['sql_wasemptied']="boşaltıldı";
$lang['sql_renamedto']="yeniden adlandırıldı";
$lang['sql_dbcopy']="`%s` veritabanının içeriği `%s` veritabanına kopyalandı.";
$lang['sql_dbscopy']="`%s`veritabanının yapısı `%s` veritabanına kopyalandı.";
$lang['sql_wascreated']="oluşturuldu";
$lang['sql_renamedb']="veritabanının adını değiştir";
$lang['sql_actions']="İşlem";
$lang['sql_chooseaction']="İşlem seç";
$lang['sql_deletedb']="veritabanını sil";
$lang['sql_emptydb']="veritabanını boşalt";
$lang['sql_copydatadb']="İçeriği veritabanına kopyala";
$lang['sql_copysdb']="Yapıyı veritabanına kopyala";
$lang['sql_imexport']="İthalat / İhracat ";
$lang['info_records']="Kayıtlar";
$lang['asktableemptykeys']="`%s` Tablosu boşaltılıp indexler silinsinmi?";
$lang['edit']="İşleme";
$lang['delete']="Silme";
$lang['empty']="içeriği boşalt";
$lang['emptykeys']="Boşaltıp indexleri silme";
$lang['sql_tableemptied']="`%s` Tablosu boşaltıldı.";
$lang['sql_tableemptiedkeys']="`%s` Tablosu boşaltıldı ve indexleri silindi.";
$lang['sql_library']="SQL-Kütüphanesi";
$lang['sql_attributes']="Attributlar";
$lang['sql_enumsethelp']="ENUM ve SET hücretiplerinde lütfen SİZE de kayıtlistesini girin.

Değerler tırnak işaretleri içerisinde ve virgül ile ayrılmış olmalı.

Özel Karekter kullanımında  ile maskelenmiş olmaları gerekir.


Örnekler:

'a','b','c'

'evet','hayır'

'x','y'";
$lang['sql_uploadedfile']="Yüklenen dosya: ";
$lang['sql_import']="`%s` Veritabanına ithal";
$lang['export']="İhraç";
$lang['import']="İthal";
$lang['importoptions']="İthal-Opsyonları";
$lang['csvoptions']="CSV-Opsyonları";
$lang['importtable']="Tablosuna ithal";
$lang['newtable']="yeni tablo";
$lang['importsource']="İthal kaynağı";
$lang['fromtextbox']="Giriş kutusundan";
$lang['fromfile']="Dosyadan";
$lang['emptytablebefore']="Tabloyu önce boşalt";
$lang['createautoindex']="Auto-Index oluştur";
$lang['csv_namefirstline']="Sütun isimlerini ilk satıra yaz";
$lang['csv_fieldseperate']="Kayıtları ayırmak için ";
$lang['csv_fieldsenclosed']="Kayıtları kapsayan";
$lang['csv_fieldsescape']="Kayıtlar escaped to";
$lang['csv_eol']="Satırları ayıran";
$lang['csv_null']="NULL un yerine kullanılacak";
$lang['csv_fileopen']="CSV-Dosyasını aç";
$lang['importieren']="İthal";
$lang['sql_export']="`%s` Veritabanından ihraç";
$lang['exportoptions']="İhraç opsyonları";
$lang['excel2003']="Excel 2003 ve üstü";
$lang['showresult']="Sonuçu göster";
$lang['sendresultasfile']="Sonuçu dosya olarak gönder";
$lang['exportlines']="<strong>%s</strong> satır ihraç edildi";
$lang['csv_fieldcount_nomatch']="Tablokayıtlarının sayısı, ithal edilecek bilgilerle uyuşmuyor (%d yerine %d).";
$lang['csv_fieldslines']="%d kayıt tespit edildi, toplam %d satır";
$lang['csv_errorcreatetable']=" `%s` Tablo oluşturmada hata oluştu!";
$lang['fm_uploadfilerequest']="Dosya belirtiniz.";
$lang['csv_nodata']="İthal edilebilecek kayıt bulunamadı!";
$lang['sqllib_generalfunctions']="genel Funksyonlar";
$lang['sqllib_resetauto']="Auto-değeri geri al";
$lang['sqllib_boards']="Paneller";
$lang['sqllib_deactivateboard']="Paneli durdur";
$lang['sqllib_activateboard']="Paneli çalştır";
$lang['sql_notablesselected']="Tablo seçilmedi!";
$lang['tools']="Araçlar";
$lang['tools_toolbox']="Veritabanı seçimi / Veritabanı işlemler  / İthal / İhraç ";
$lang['sqllib_boardoffline']="Paneli kapa";
$lang['sql_openfile']="SQL dosyasını aç";
$lang['sql_openfile_button']="yükle";
$lang['max_upload_size']="maximum Dosya boyutu";
$lang['sql_search']="arama";
$lang['sql_searchwords']="aranan kelime(ler)";
$lang['start_sql_search']="aramayı başlat";
$lang['reset_searchwords']="arama sonucunu sil";
$lang['search_explain']="arama için kelime yada kelimeleri yazın ve \"arama\" tuşuna basın.<br> birden fazla kelime için boşluk bırakın.";
$lang['search_options']="arama seçeneği";
$lang['search_results']="aradığınız \"<b>%s</b>\" kelime sonucu \"<b>%s</b>\" Tablo'da bulunan sonuçlar";
$lang['search_no_results']="aradığınız \"<b>%s</b>\" kelimesi \"<b>%s</b>\" Tablo içersinde bulunamadı !";
$lang['no_entries']="\"<b>%s</b>\" isimli Tablo boş ve hiçbirşey yazılmamış.";
$lang['search_access_keys']="Çevir: ileri=ALT+V, geri=ALT+C";
$lang['search_options_or']="Satırda en azından bir aranılan kelime bulunmalıdır. (Or-arama)";
$lang['search_options_concat']="Metin'de bütün aranılan kelimeler bulunmalıdır, aranılan kelimeler değişik satırlarda bulunabilir. (Vakit alıcı)";
$lang['search_options_and']="Sütunun içinde aranan kelimelerin hepsi bulunmalı";
$lang['search_in_table']="Tablonun içinde ara";
$lang['sql_edit_tablestructure']="Edit tablestructure";
$lang['default_charset']="Default character set";


?>