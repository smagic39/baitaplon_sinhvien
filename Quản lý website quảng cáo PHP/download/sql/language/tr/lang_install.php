<?php
//generated at 17.03.2007

$lang['installfinished']="<br>Kurulum tamamlanmıştır   --> <a href=\"index.php\">MySQLDumper'i başlat </a><br>";
$lang['install_forcescript']="MySQLDumper'i yüklemeden başlat";
$lang['install_tomenu']="Ana Menüye geç";
$lang['installmenu']="Ana Menüye";
$lang['step']="Adım";
$lang['install']="Kurulum";
$lang['uninstall']="Kurulumu silme";
$lang['tools']="Araçlar";
$lang['editconf']="Ayarları elden işle";
$lang['osweiter']="Kayıt etmeden devam et";
$lang['errorman']="Ayarların kaydında hata oluştu!</strong><br>Dosyayı lütfen elden işleyiniz ";
$lang['manuell']="elden";
$lang['createdirs']="Klasörler oluşturuluyor";
$lang['install_continue']="Kuruluma devam et";
$lang['connecttomysql']=" MySQL ile bağlan ";
$lang['dbparameter']="Veritabanı-Parametreleri";
$lang['confignotwritable']="\"config.php\" yazılamıyor. FTP yazılımınız ile CHMOD ayarlarını 0777 ye çevirin.";
$lang['dbconnection']="Bağlantı Parametreleri";
$lang['connectionerror']="Hata: bağlantı kurulamıyor.";
$lang['connection_ok']="Veritabanı bağlantısı kuruldu.";
$lang['saveandcontinue']="Devam et";
$lang['confbasic']="Tabanayarları";
$lang['install_step2finished']="Veritabanı ayarları kayıt edildi.<br><br>
İsterseniz standart ayarlar ile kurulumu sürdürebilirsiniz, veya ayarları düzenleyebilirsiniz.";
$lang['install_step2_1']="Standart ayarlarla devam";
$lang['laststep']="Kurulumum tamamlanması";
$lang['ftpmode']="Klasörler FTP ile oluşturuluyor (safe_mode)";
$lang['idomanual']="Klasörleri elden oluşturacağım";
$lang['dofrom']="Yola çıkılacak veriyolu";
$lang['ftpmode2']="klasörleri FTP ile oluşturacağım";
$lang['connect']="Bağlantı kur";
$lang['dirs_created']="Klasörler oluşturuldu.";
$lang['connect_to']="Bağlantı kur:";
$lang['changedir']="Klasöre geç";
$lang['changedirerror']="Klasöre geciş mümkün değil";
$lang['ftp_ok']="FTP-Ayarları geçerli";
$lang['createdirs2']="Klasör oluşturma";
$lang['ftp_notconnected']="FTP-bağlantısı kurulmadı!";
$lang['connwith']="Kurulacak bağlantı:";
$lang['asuser']="Kullanıcı";
$lang['notpossible']="mümkün değil";
$lang['dircr1']="Oluşturuluyor: çalışma klasörü ";
$lang['dircr2']="Oluşturuluyor: yedekleme klasörü";
$lang['dircr3']="Oluşturuluyor: yapı klasörü";
$lang['dircr4']="Oluşturuluyor: protokol klasörü";
$lang['dircr5']="Oluşturuluyor: ayar klasörü";
$lang['indir']="bulunulan klasör";
$lang['check']="Kontrol ediliyor";
$lang['disabledfunctions']="İptal edilmiş Funksyonlar";
$lang['noftppossible']="FTP Bağlantısı yok!";
$lang['nogzpossible']="Sıkıştırma funksyonu bulunmuyor!";
$lang['ui1']="Çalışma klasörleri siliniyor (içerisinde bulunan yedekleme dosyaları ile birlikte).";
$lang['ui2']="Eminmisiniz?";
$lang['ui3']="Hayır, hemen iptal et";
$lang['ui4']="Evet, işlemi sürdür";
$lang['ui5']="Çalışma klasörü siliniyor";
$lang['ui6']="Hepsi silindi.";
$lang['ui7']="Skriptklasörünü lütfen elden siliniz";
$lang['ui8']="Bir düzey yukarı çık";
$lang['ui9']="Hata oluştu, silme işlemi uygulanamadı</p>Hatanın oluştuğu klasör: ";
$lang['import']="Ayarları ithal et";
$lang['import1']="Ayarları \"config.gz\" den ithal et";
$lang['import2']="Ayarları yükle ve ıthal et";
$lang['import3']="Ayarlar kayıt edildi...";
$lang['import4']="Ayarlar yedeklendi.";
$lang['import5']="MySQLDumper'i başlat";
$lang['import6']="Kurulum menüsü";
$lang['import7']="Ayarları yükle";
$lang['import8']="Yüklemeye geri dön";
$lang['import9']="Bu bir ayaryedeklemesi değil!";
$lang['import10']="Ayarlar yüklendi...";
$lang['import11']="<strong>Hata: </strong>SQL_statement lerin kaydında hata oluştu";
$lang['import12']="<strong>Hata: </strong>Config.php nin kaydında hata oluştu";
$lang['install_help_port']="(boş = Standart port)";
$lang['install_help_socket']="(boş= Standart socket)";
$lang['tryagain']="Bir daha dene";
$lang['socket']="Socket";
$lang['port']="Port";
$lang['found_no_db']="Hata: Bulunamayan Veritabanı:";
$lang['found_db']="Bulunan Veritabanı: ";
$lang['fm_fileupload']="Dosya yükle";
$lang['pass']="Şifre";
$lang['no_db_found_info']="Veritabanı sunucusu ile bağlantı kuruldu.<br>
Bağlantı parametreleri doğrulandı, kullanıcı ismi ve şifresi kabul edildi.<br>
Fakat Sunucuda Veritabanı bulunamadı.<br>
Otomatik tanıma sunucunuzda kilitli olabilir.<br>
Kurulum tamamlandıktan sonra lütfen Ayar Merkezi sayfasına gidin ve Bağlantı parametrelerin, göstertin.<br>
Veritanı ile bağlantı kurulabilmesi için gereken bilgileri oraya girmeniz gerekiyor.";


?>