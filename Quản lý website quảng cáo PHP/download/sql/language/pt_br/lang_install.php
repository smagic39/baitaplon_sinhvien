<?php
//generated at 17.03.2007

$lang['installfinished']="<br>Instalação completada  --> <a href=\"index.php\">iniciar o MySQLDumper</a><br>";
$lang['install_forcescript']="Iniciar o MySQLDumper sem instalação";
$lang['install_tomenu']="Voltar ao menu principal";
$lang['installmenu']="Menu principal";
$lang['step']="Passo";
$lang['install']="Instalação";
$lang['uninstall']="Desinstalar";
$lang['tools']="Ferramentas";
$lang['editconf']="Editar configuração";
$lang['osweiter']="Continuar sem salvar";
$lang['errorman']="<strong>Erro durante a gravação da configuração!</strong><br>Favor editar o Arquivo ";
$lang['manuell']="manualmente";
$lang['createdirs']="Criar diretórios";
$lang['install_continue']="Continuar com a  instalação";
$lang['connecttomysql']="Conectar ao MySQL ";
$lang['dbparameter']="Parâmetros do banco de dados";
$lang['confignotwritable']="Eu não consigo escrever no arquivo \"config.php\".
Favor usar seu programa de FTP e alterar as permissões deste arquivo para 0777.";
$lang['dbconnection']="Conexão do banco de dados";
$lang['connectionerror']="Erro: incapaz de conectar.";
$lang['connection_ok']="A conexão ao banco de dados foi estabelecida.";
$lang['saveandcontinue']="Salvar e continuar a instalação";
$lang['confbasic']="Parâmetros básicos";
$lang['install_step2finished']="Os parâmetros do banco de dados foram salvos.";
$lang['install_step2_1']="Continuar a instalação com as opções padrão";
$lang['laststep']="Instalação terminada";
$lang['ftpmode']="Criar os diretórios necessários em modo seguro";
$lang['idomanual']="Eu mesmo criarei os diretórios";
$lang['dofrom']="iniciando com";
$lang['ftpmode2']="Criar os diretórios pelo FTP:";
$lang['connect']="conectar";
$lang['dirs_created']="Os diretórios foram criados e as permissões corretamente atribuídas.";
$lang['connect_to']="conectar a";
$lang['changedir']="mudar para o diretório";
$lang['changedirerror']="a mudança para o diretório não foi possível";
$lang['ftp_ok']="os parâmetros de FTP estão ok";
$lang['createdirs2']="Criar diretórios";
$lang['ftp_notconnected']="A conexão de FTP não foi estabelecida!";
$lang['connwith']="Conexão com";
$lang['asuser']="como usuário";
$lang['notpossible']="impossível";
$lang['dircr1']="criar workdir";
$lang['dircr2']="criar backupdir";
$lang['dircr3']="criar structurdir";
$lang['dircr4']="criar logdir";
$lang['dircr5']="criar configurationdir";
$lang['indir']="agora no diretório";
$lang['check']="Verificar meus diretórios";
$lang['disabledfunctions']="Funções desativadas";
$lang['noftppossible']="Você não tem as funções de FTP !";
$lang['nogzpossible']="Você não tem as funções de compressão !";
$lang['ui1']="Todos os diretórios que podem conter backups serão excluidos.";
$lang['ui2']="Você tem certeza de que quer isso?";
$lang['ui3']="não, cancelar imediatamente";
$lang['ui4']="sim, favor continuar";
$lang['ui5']="excluir diretórios de serviço";
$lang['ui6']="todos foram excluidos com sucesso.";
$lang['ui7']="Fvor excluir o diretório de script";
$lang['ui8']="one level up";
$lang['ui9']="Um erro ocorreu, impossível excluir</p>Erro com o diretório ";
$lang['import']="Importar configuração";
$lang['import1']="Importar ajustes de ";
$lang['import2']="Enviar e importar os ajustes";
$lang['import3']="A configuração foi carregada ...";
$lang['import4']="A configuração foi salva.";
$lang['import5']="Iniciar o MySQLDumper";
$lang['import6']="Menu de instalação";
$lang['import7']="Enviar configuração";
$lang['import8']="voltar ao envio";
$lang['import9']="Este não é um backup de configuração !";
$lang['import10']="A configuração foi enviada com sucesso ...";
$lang['import11']="<strong>Erro: </strong>Houveram problemas ao escrever os comandos sql";
$lang['import12']="<strong>Erro: </strong>Houveram problemas ao escrever o config.php";
$lang['install_help_port']="(em branco = Porta padrão)";
$lang['install_help_socket']="(em branco = Socket padrão)";
$lang['tryagain']="Tentar novamente";
$lang['socket']="Socket";
$lang['port']="Porta";
$lang['found_no_db']="ERRO: o seguinte BD não foi localizado:";
$lang['found_db']="bd localizado";
$lang['fm_fileupload']="Enviar arquivo";
$lang['pass']="Senha";
$lang['no_db_found_info']="A conexão com o banco de dados foi estabelecida com sucesso.<br>
Seus dados de usuário são válidos e foram aceitos pelo Servidor MySQL.<br>
Mas o MySQLDumper não foi capaz de encontrar nenhuma base de dados.<br>
A detecção automática via script é bloqueada em alguns servidores.<br>
Você deve colocar seus dados do banco de dados manualmente depois de terminada a instalação.
Clique em \\"configurações\\" \\"Parâmetros de Conexão - exibir\\" e digite ali o nome do banco de dados.";


?>