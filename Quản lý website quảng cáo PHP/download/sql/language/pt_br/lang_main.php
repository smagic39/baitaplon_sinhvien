<?php
//generated at 17.03.2007

$lang['noftppossible']="Você não tem alçada para funções de FTP !";
$lang['info_location']="Sua localização é ";
$lang['info_databases']="Os seguintes bancos de dados estão no seu servidor:";
$lang['info_nodb']="O banco de dados não existe.";
$lang['info_table1']="Tabela";
$lang['info_table2']="s";
$lang['info_dbdetail']="informação detalhada do banco de dados ";
$lang['info_dbempty']="O banco de dados está vazio !";
$lang['info_records']="Registros";
$lang['info_size']="tamanho";
$lang['info_lastupdate']="Última atualização";
$lang['info_sum']="total";
$lang['info_optimized']="otimizada";
$lang['optimize_databases']="Otimizar tabelas";
$lang['check_tables']="Verificar tabelas";
$lang['clear_database']="Limpar banco de dados";
$lang['delete_database']="Excluir banco de dados";
$lang['info_cleared']="foi limpa";
$lang['info_deleted']="foi excluida";
$lang['info_emptydb1']="Deve o banco de dados";
$lang['info_emptydb2']=" ser truncado? (Atenção: Todos os dados serão perdidos para sempre!)";
$lang['info_killdb']=" ser excluído? (Atenção: Todos os dados serão perdidos para sempre!)";
$lang['processkill1']="O script tenta abortar o processo ";
$lang['processkill2']="para abortar.";
$lang['processkill3']="O script tenta desde  ";
$lang['processkill4']=" sec. abortar o processo ";
$lang['htaccess1']="Criar proteção do diretório";
$lang['htaccess2']="Senha:";
$lang['htaccess3']="Senha (repetir):";
$lang['htaccess4']="Tipo de encriptação:";
$lang['htaccess5']="Crypt (Linux e Sistemas Unix)";
$lang['htaccess6']="Linux e Sistemas Unix (MD5)";
$lang['htaccess7']="texto plano, sem encriptação (Windows)";
$lang['htaccess8']="Já existe uma proteção do diretório. Se você criar novas, as antigas serão sobrescritas !";
$lang['htaccess9']="Você tem que digitar um nome !<br>";
$lang['htaccess10']="As senhas não são idênticas ou são nulas !<br>";
$lang['htaccess11']="Deve a proteção do diretório ser gravada agora ?";
$lang['htaccess12']="A proteção do diretório foi criada.";
$lang['htaccess13']="Conteúdo do arquivo:";
$lang['htaccess14']="Houve um erro durante a criação da proteção do diretório !<br>Favor criar os 2 arquivos manualmente com o seguinte conteúdo:";
$lang['htaccess15']="Urgentemente recomendado !";
$lang['htaccess16']="Editar o .htaccess";
$lang['htaccess17']="Criar e editar o .htaccess";
$lang['htaccess18']="Criar o .htaccess em ";
$lang['htaccess19']="Recarregar ";
$lang['htaccess20']="Executar script";
$lang['htaccess21']="Adicionar handler";
$lang['htaccess22']="Tornar executável";
$lang['htaccess23']="Listar Diretórios";
$lang['htaccess24']="Documento de Erro";
$lang['htaccess25']="Ativar rewrite";
$lang['htaccess26']="Negar / Permitir";
$lang['htaccess27']="Redirecionar";
$lang['htaccess28']="Error Log";
$lang['htaccess29']="Mais exemplos e documentação";
$lang['htaccess30']="Provedor";
$lang['htaccess31']="General";
$lang['htaccess32']="Atenção! As diretivas do .htaccess afetam o comportamento do navegador.<br>Com conteúdo incorreto, as páginas podem ficar inacessíveis.";
$lang['phpbug']="Bug em zlib ! Não foi possível comprimir";
$lang['disabledfunctions']="Desativar Funções";
$lang['nogzpossible']="Como Zlib não está instalado, você não poderá usar as funções do GZip.";
$lang['delete_htaccess']="Remover proteção de diretório (apagar .htaccess)";
$lang['wrong_rights']="O arquivo ou o diretório '%s' não tem permissão de escrita para mim.<br>
As permissões (chmod) não estão configuradas apropriadamente ou não há privilégios suficientes para este usuário.<br>
Por favor configure corretamente as permissões usando o programa de FTP.<br>
O arquivo ou diretório necessitam de configuração para %s.<br>";
$lang['cant_create_dir']="Não foi possível criar o diretório '%s'. 
Por favor utilize seu programa de FTP.";


?>