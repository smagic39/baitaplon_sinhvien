<?php
//generated at 17.03.2007

$lang['help_db']="Esta é a lista de todos os bancos de dados";
$lang['help_praefix']="o prefixo é uma string no começo do nome da tabela, que funciona como um filtro.";
$lang['help_zip']="Compressão com GZip - estado recomendado é 'ativado'";
$lang['help_memorylimit']="Quantidade máxima de memória em bytes para o script
0 = desativado";
$lang['memory_limit']="Limite de memória";
$lang['help_mail1']="Se ativado, após o backup, um e-mail será encaminhado à você com o backup como anexo.";
$lang['help_mail2']="Este é o destinatário do e-mail.";
$lang['help_mail3']="Este é o remetente do e-mail.";
$lang['help_mail4']="Tamanho máximo para anexo do e-mail, se estiver configurado como 0 esta opção será ignorada";
$lang['help_mail5']="Aqui você pode selecionar se o backup deve ser encaminhado como anexo do email";
$lang['help_ad1']="Se ativado os arquivos de backup serão excluidos automaticamente.";
$lang['help_ad2']="número máximo de dias para manter o arquivo de backup (para autoexclusão)
0 = desativado";
$lang['help_ad3']="número máximo de arquivos de backup (para autoexclusão)
0 = desativado";
$lang['help_lang']="selecione seu idioma";
$lang['help_empty_db_before_restore']="Para eliminar dados sem valor você pode configurar para limpar seu banco de dados antes de restaurar";
$lang['help_cronmail']="Selecione se quiser que o trabalho de Cron encaminhe o backup por e-mail";
$lang['help_cronmailprg']="caminho para seu programa de e-mail, o padrão é sendmail com o caminho que foi fornecido";
$lang['help_cronftp']="Selecione se o trabalho de Cron deve enviar o Backup via FTP";
$lang['help_cronzip']="Compressão com GZip no trabalho Cron - o estado recomendado é 'ativado' (requer a instalação da biblioteca de compressão!)";
$lang['help_cronextender']="Extensão dos scripts Perl, o padrão é '.pl'";
$lang['help_cronsavepath']="Nome do arquivo de configuração para o script Perl";
$lang['help_cronprintout']="Se a saída estiver desativada ela não irá aparecer na tela.
Isto é independente de fazer impressão a partir do arquivo de log.";
$lang['help_cronsamedb']="Usar o mesmo bd no trabalho Cron como está configurado o banco de dados?";
$lang['help_crondbindex']="escolha o banco de dados para o Cron";
$lang['help_cronmail_dump']="Selecione se o resultado do backup via Cron deve ser anexado ao e-mail.";
$lang['help_ftptransfer']="se ativado o arquivo será encaminhado via FTP.";
$lang['help_ftpserver']="Endereço do servidor de FTP";
$lang['help_ftpport']="Porta do servidor de FTP, padrão: 21";
$lang['help_ftpuser']="digite o nome do usuário para o FTP";
$lang['help_ftppass']="digite a senha para o FTP";
$lang['help_ftpdir']="onde está o diretório para upload? Digite o caminho!";
$lang['help_speed']="Velocidade mínima e máxima. O padrão é: 50 à 5000";
$lang['speed']="Controle de velocidade";
$lang['help_cronexecpath']="Lugar dos scripts Perl.
O ponto de partida é o endereço HTTP (como um endereço no navegador)
Permitidas entradas absolutas ou relativas.";
$lang['cron_execpath']="Caminho dos scripts Perl";
$lang['help_croncompletelog']="Quando ativo o resultado completo será escrito em um arquivo_logcompleto.
Isto é independente de imprimir o texto";
$lang['help_ftp_mode']="Quando você observar problemas usando a tranferência em FTP, ative o modo passivo.";


?>