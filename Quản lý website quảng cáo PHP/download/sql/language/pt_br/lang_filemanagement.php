<?php
//generated at 17.03.2007

$lang['convert_start']="Iniciar a conversão";
$lang['convert_title']="Converter o dump para o formato MSD";
$lang['convert_wrong_parameters']="Parâmetros incorretos!  A conversão não é possível.";
$lang['fm_uploadfilerequest']="favor escolher um arquivo.";
$lang['fm_uploadnotallowed1']="Este tipo de arquivo não é suportado.";
$lang['fm_uploadnotallowed2']="Os tipos válidos são: *.gz and *.sql-files";
$lang['fm_uploadmoveerror']="Não pude mover os arqruivos selecionados para o diretório de envio.";
$lang['fm_uploadfailed']="O envio falhou!";
$lang['fm_uploadfileexists']="Um arquivo com o mesmo nome já existe !";
$lang['fm_nofile']="Você não escolheu nenhum arquivo!";
$lang['fm_delete1']="O arquivo ";
$lang['fm_delete2']=" foi excluido com sucesso.";
$lang['fm_delete3']=" não pode ser excluido!";
$lang['fm_choose_file']="Arquivo escolhido:";
$lang['fm_filesize']="Tamanho do arquivo";
$lang['fm_filedate']="Data do arquivo";
$lang['fm_nofilesfound']="Nenhum arquivo encontrado.";
$lang['fm_tables']="Tabelas";
$lang['fm_records']="Registros";
$lang['fm_all_bu']="Todos os backups";
$lang['fm_anz_bu']="Backups";
$lang['fm_last_bu']="Último backup";
$lang['fm_totalsize']="Tamanho total";
$lang['fm_selecttables']="Selecionar tabelas";
$lang['fm_comment']="Digitar comentário";
$lang['fm_restore']="Restaurar";
$lang['fm_alertrestore1']="Deve o banco de dados";
$lang['fm_alertrestore2']="ser restaurado com os rgistros do arquivo";
$lang['fm_alertrestore3']=" ?";
$lang['fm_delete']="Excluir";
$lang['fm_askdelete1']="Deve o arquivo ";
$lang['fm_askdelete2']=" realmente ser excluido?";
$lang['fm_askdelete3']="Você deseja que a autoexclusão seja executada com as regras configuradas agora?";
$lang['fm_askdelete4']="Você deseja excluir todos os arqruvos de backup?";
$lang['fm_askdelete5']="Você deseja excluir todos os arqruvos de backup com ";
$lang['fm_askdelete5_2']="_* ?";
$lang['fm_deleteauto']="Executar a autoexclusão manualmente";
$lang['fm_deleteall']="Excluir todos os arqruvos de backup";
$lang['fm_deleteallfilter']="Excluir todos com ";
$lang['fm_deleteallfilter2']="_*";
$lang['fm_startdump']="Iniciar novo backup";
$lang['fm_fileupload']="Enviar arquivo";
$lang['fm_dbname']="Nome do banco de dados";
$lang['fm_files1']="Backups do banco de dados";
$lang['fm_files2']="Estruturas do banco de dados";
$lang['fm_autodel1']="Autoexclusão: os seguintes arqruivos foram excluidos devido ao ajuste de número máximo de arquivos:";
$lang['fm_autodel2']="Autoexclusão: os seguintes arqruivos foram excluidos devido a sua data:";
$lang['fm_dumpsettings']="Configuração do script Perl Cron";
$lang['fm_dumpsettings_cron']="Propriedades do o script Perl Cron";
$lang['fm_oldbackup']="(desconhecido)";
$lang['fm_restore_header']="Restaurar o banco de dados <strong>";
$lang['fm_restore_header2']="\"</strong>";
$lang['fm_dump_header']="Backup";
$lang['DoCronButton']="Executar o script Perl Cron";
$lang['DoPerlTest']="Testar módulos Perl";
$lang['DoSimpleTest']="Testar Perl";
$lang['perloutput1']="Entrada no crondump.pl para o absolute_path_of_configdir";
$lang['perloutput2']="URL para o navegador ou serviço Cron externo";
$lang['perloutput3']="Linha de comando no terminal para o Crontab";
$lang['converter']="Conversor de backup";
$lang['convert_file']="Arquivo a ser convertido";
$lang['convert_filename']="Nome do arquivo de destino (sem extensão)";
$lang['converting']="Convertendo";
$lang['convert_fileread']="Ler arquivo '%s'";
$lang['convert_finished']="Conversão terminada, o arquivo '%s' foi gravado com sucesso.";
$lang['no_msd_backupfile']="Backups de outros scripts";
$lang['max_upload_size']="Tamanho máximo do aqruivo";
$lang['max_upload_size_info']="Se o seu arquivo de dump é maior que o limite mencionado acima, você deve enviá-lo via FTP para o diretório \"work/backup\".
Após fazer isso você poderá escolhê-lo novamente para iniciar o processo de restauração. ";
$lang['encoding']="encoding";
$lang['fm_choose_encoding']="Choose encoding of backupfile";
$lang['choose_charset']="MySQLDumper couldn't detect the encoding of the backupfile automatically.
<br>You must choose the charset with which this backup was saved.
<br>If you discover any problems with some characters after restoring, you can repeat the backup-progress and then choose another chracter set.
<br>Good luck. ;)

";


?>