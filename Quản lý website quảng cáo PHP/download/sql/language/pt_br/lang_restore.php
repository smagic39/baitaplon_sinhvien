<?php
//generated at 17.03.2007

$lang['restore_tables_completed0']="Até agora <b>%d</b> tabelas foram criadas.";
$lang['file_missing']="não pude encontrar o arquivo";
$lang['restore_db']="banco de dados '<b>%s</b>' em '<b>%s</b>'.";
$lang['restore_complete']="<b>%s</b> tabelas driadas.";
$lang['restore_run1']="<br>Up to now  <b>%s</b> of <b>%s</b> registros foram adicionados com sucesso.";
$lang['restore_run2']="<br>Agora a tabela '<b>%s</b>' está sendo restaurada.<br><br>";
$lang['restore_complete2']="<b>%s</b> registros inseridos.";
$lang['restore_tables_completed']="Até agora <b>%d</b> de <b>%d</b> tabelas foram criadas.";
$lang['restore_total_complete']="<br><b>Parabéns.</b><br><br>A restauração do banco de dados está pronta.<br>todos os dados do arquivo de backup foram restaurados.<br><br>Está tudo pronto. :-)";
$lang['db_select_error']="<br>Error:<br>Seleção do banco de dados <b>";
$lang['db_select_error2']="</b> falhou!";
$lang['file_open_error']="Erro: não pude abrir o arquivo.";
$lang['progress_over_all']="Progresso do todo";
$lang['back_to_overview']="Visão geral do banco de dados";
$lang['restore_run0']="<br>até agora <b>%s</b> registros foram adicionados com sucesso.";
$lang['unknown_sqlcommand']="comando SQL desconhecido";
$lang['notices']="Notices";


?>