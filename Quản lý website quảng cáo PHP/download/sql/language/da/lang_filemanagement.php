<?php
//generated at 17.03.2007

$lang['convert_start']="Start konvertering";
$lang['convert_title']="Konvertér dump til MSD-format";
$lang['convert_wrong_parameters']="Forkerte parametre!  Konvertering er ikke muligt.";
$lang['fm_uploadfilerequest']="vælg venligst en fil.";
$lang['fm_uploadnotallowed1']="Denne filtype understøttes ikke.";
$lang['fm_uploadnotallowed2']="Gyldige typer er: *.gz og *.sql-filer";
$lang['fm_uploadmoveerror']="Kunne ikke flytte valgte fil til upload folderen.";
$lang['fm_uploadfailed']="Upload slog fejl!";
$lang['fm_uploadfileexists']="Der findes allerede en fil med samme navn!";
$lang['fm_nofile']="Du valgte ikke en fil!";
$lang['fm_delete1']="Filen ";
$lang['fm_delete2']=" blev slettet korrekt.";
$lang['fm_delete3']=" kunne ikke slettes!";
$lang['fm_choose_file']="Valgt fil:";
$lang['fm_filesize']="Filstørrelse";
$lang['fm_filedate']="Fildato";
$lang['fm_nofilesfound']="Ingen fil fundet.";
$lang['fm_tables']="Tabeller";
$lang['fm_records']="Poster";
$lang['fm_all_bu']="Alle backups";
$lang['fm_anz_bu']="Backups";
$lang['fm_last_bu']="Seneste backup";
$lang['fm_totalsize']="Total størrelse";
$lang['fm_selecttables']="Vælg tabeller";
$lang['fm_comment']="Indtast kommentar";
$lang['fm_restore']="Genetabler";
$lang['fm_alertrestore1']="Skal databasen";
$lang['fm_alertrestore2']="genetableres med posterne fra filen";
$lang['fm_alertrestore3']=" ?";
$lang['fm_delete']="Slet";
$lang['fm_askdelete1']="Skal filen ";
$lang['fm_askdelete2']=" virkelig slettes?";
$lang['fm_askdelete3']="Vil du køre autoslet med de konfigurerede regler nu?";
$lang['fm_askdelete4']="Vil du slette alle backupfiler?";
$lang['fm_askdelete5']="Vil du slette alle backupfiler med ";
$lang['fm_askdelete5_2']="_* ?";
$lang['fm_deleteauto']="Kør autoslet manuelt";
$lang['fm_deleteall']="Slette alle backupfiler";
$lang['fm_deleteallfilter']="Slet alle med ";
$lang['fm_deleteallfilter2']="_*";
$lang['fm_startdump']="Start ny backup";
$lang['fm_fileupload']="Upload fil";
$lang['fm_dbname']="Databasenavn";
$lang['fm_files1']="Databasebackups";
$lang['fm_files2']="Databasestrukturer";
$lang['fm_autodel1']="Autoslet: følgende filer blev slettet grundet maksimalt antal filer-indstillingen:";
$lang['fm_autodel2']="Autoslet: følgende filer blev slettet grundet deres dato:";
$lang['fm_dumpsettings']="Konfiguration for Perl Cron scriptet";
$lang['fm_dumpsettings_cron']="Egenskaber for Perl Cron scriptet";
$lang['fm_oldbackup']="(ukendt)";
$lang['fm_restore_header']="Genetablering af Database <strong>";
$lang['fm_restore_header2']="\"</strong>";
$lang['fm_dump_header']="Backup";
$lang['DoCronButton']="Kør Perl Cron scriptet";
$lang['DoPerlTest']="Test Perl-moduler";
$lang['DoSimpleTest']="Test Perl";
$lang['perloutput1']="Linie i crondump.pl for absolute_path_of_configdir";
$lang['perloutput2']="URL for browseren eller for eksternt Cron job";
$lang['perloutput3']="Kommandolinie i Shell eller for Crontab";
$lang['converter']="Backupkonvertering";
$lang['convert_file']="Fil der skal konverteres";
$lang['convert_filename']="Navn på destinationsfilen (uden filtype)";
$lang['converting']="Konverterer";
$lang['convert_fileread']="Læs fil '%s'";
$lang['convert_finished']="Konvertering afsluttet, '%s' blev skrevet korrekt.";
$lang['no_msd_backupfile']="Backups af andre scripts";
$lang['max_upload_size']="Maksimal filstørrelse";
$lang['max_upload_size_info']="Hvis din Dumpfil er større end den ovennævnte grænse, skal du uploade den via FTP til folderen \"work/backup\". 
Derefter kan du vælge den og begynde genetableringsprocessen.


";
$lang['encoding']="encoding";
$lang['fm_choose_encoding']="Choose encoding of backupfile";
$lang['choose_charset']="MySQLDumper couldn't detect the encoding of the backupfile automatically.
<br>You must choose the charset with which this backup was saved.
<br>If you discover any problems with some characters after restoring, you can repeat the backup-progress and then choose another chracter set.
<br>Good luck. ;)

";


?>