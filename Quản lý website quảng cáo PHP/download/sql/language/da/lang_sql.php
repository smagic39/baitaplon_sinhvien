<?php
//generated at 17.03.2007

$lang['command']="Kommando";
$lang['sql_view_normal']="Visning: normal";
$lang['sql_view_compact']="Visning: kompakt";
$lang['import_notable']="Ingen tabel valgt til import!";
$lang['sql_warning']="Udførelse af SQL-sætninger kan manipulere data. PAS PÅ! Forfatterne af dette system påtager sig intet ansvar for beskadigede eller tabte data.";
$lang['sql_exec']="Udfør SQL-sætning";
$lang['sql_dataview']="Data Visning";
$lang['sql_tableview']="Tabel-visning";
$lang['sql_vonins']="fra totalt";
$lang['sql_nodata']="ingen poster";
$lang['sql_recordupdated']="Post blev opdateret";
$lang['sql_recordinserted']="Post blev tilføjet";
$lang['sql_backdboverview']="Tilbage til Oversigt";
$lang['sql_recorddeleted']="Post blev slettet";
$lang['asktableempty']="Skal tabellen `%s` tømmes?";
$lang['sql_recordedit']="rediger post";
$lang['sql_recordnew']="ny post";
$lang['askdeleterecord']="Er du sikker på at du vil slette denne post?";
$lang['askdeletetable']="Skal tabellen `%s` slettes?";
$lang['sql_befehle']="SQL-kommandoer";
$lang['sql_befehlneu']="Ny kommando";
$lang['sql_befehlsaved1']="SQL-kommando";
$lang['sql_befehlsaved2']="blev tilføjet";
$lang['sql_befehlsaved3']="blev gemt";
$lang['sql_befehlsaved4']="blev flyttet op";
$lang['sql_befehlsaved5']="blev slettet";
$lang['sql_queryentry']="Forespørgslen indeholder";
$lang['sql_columns']="Kolonner";
$lang['askdbdelete']="Vil du slette databasen `%s` med alt indhold?";
$lang['askdbempty']="Vil du tømme databasen `%s` ?";
$lang['askdbcopy']="Vil du kopiere database `%s` til database `%s`?";
$lang['sql_tablenew']="Ret Tabeller";
$lang['sql_output']="SQL-Output";
$lang['do_now']="gør det nu";
$lang['sql_namedest_missing']="Destinationsnavn mangler!";
$lang['askdeletefield']="Vil du slette feltet?";
$lang['sql_commands_in']=" linier i ";
$lang['sql_commands_in2']="  sek. bearbejdet.";
$lang['sql_out1']="Udført ";
$lang['sql_out2']="Kommandoer";
$lang['sql_out3']="Den havde ";
$lang['sql_out4']="Kommentarer";
$lang['sql_out5']="Da outputtet indeholder mere end 5000 linier vises det ikke.";
$lang['sql_selecdb']="Vælg Database";
$lang['sql_tablesofdb']="Tabeller i Database";
$lang['sql_edit']="ret";
$lang['sql_nofielddelete']="Slet er ikke muligt da tabeller skal indeholde mindst et felt.";
$lang['sql_fielddelete1']="Feltet";
$lang['sql_deleted']="blev slettet";
$lang['sql_changed']="blev ændret.";
$lang['sql_created']="blev oprettet.";
$lang['sql_nodest_copy']="Ingen kopiering uden en destination!";
$lang['sql_desttable_exists']="Destinationstabel findes allerede!";
$lang['sql_scopy']="Tabelstrukturen fra `%s` blev kopieret ind i Tabel `%s`.";
$lang['sql_tcopy']="Tabel `%s` blev kopieret med data ind i Tabel `%s`.";
$lang['sql_tablenoname']="Tabellen skal have et navn!";
$lang['sql_tblnameempty']="Tabelnavnet kan ikke være tomt!";
$lang['sql_collatenotmatch']="Tegnsæt og Kollation passer ikke sammen!";
$lang['sql_fieldnamenotvalid']="Fejl: Ikke gyldigt feltnavn";
$lang['sql_createtable']="opret tabel";
$lang['sql_copytable']="kopier tabel";
$lang['sql_structureonly']="Kun Struktur";
$lang['sql_structuredata']="Struktur og Data";
$lang['sql_notablesindb']="Ingen tabeller fundet i Database";
$lang['sql_selecttable']="vælg tabel";
$lang['sql_showdatatable']="Vis Data i Tabel";
$lang['sql_tblpropsof']="Tabelegenskaber for";
$lang['sql_editfield']="Ret felt";
$lang['sql_newfield']="Nyt felt";
$lang['sql_indexes']="Indeks";
$lang['sql_atposition']="indsæt på position";
$lang['sql_first']="først";
$lang['sql_after']="efter";
$lang['sql_changefield']="ændre felt";
$lang['sql_insertfield']="indsæt felt";
$lang['sql_insertnewfield']="indsæt nyt felt";
$lang['sql_tableindexes']="Indeks på tabel";
$lang['sql_allowdups']="Dubletter tilladte";
$lang['sql_cardinality']="Kardinalitet";
$lang['sql_tablenoindexes']="Ingen indeks i tabel";
$lang['sql_createindex']="opret nyt indeks";
$lang['sql_wasemptied']="blev tømt";
$lang['sql_renamedto']="blev omdøbt til";
$lang['sql_dbcopy']="Indholdet af database `%s` blev kopieret til database `%s`.";
$lang['sql_dbscopy']="Database-strukturen fra database `%s` blev kopieret til database `%s`.";
$lang['sql_wascreated']="blev oprettet";
$lang['sql_renamedb']="Omdøb database";
$lang['sql_actions']="Handlinger";
$lang['sql_chooseaction']="Vælg handling";
$lang['sql_deletedb']="Slet database";
$lang['sql_emptydb']="Tøm database";
$lang['sql_copydatadb']="Kopier hele databasen til";
$lang['sql_copysdb']="Kopier database-struktur";
$lang['sql_imexport']="Import-Eksport";
$lang['info_records']="poster";
$lang['asktableemptykeys']="Skal tabellen `%s` tømmes og indeksene nulstilles?";
$lang['edit']="ret";
$lang['delete']="slet";
$lang['empty']="tøm";
$lang['emptykeys']="tøm og nulstil alle indeks";
$lang['sql_tableemptied']="Tabel `%s` blev tømt.";
$lang['sql_tableemptiedkeys']="Tabel `%s` blev tømt og indeksene blev nulstillet.";
$lang['sql_library']="SQL-bibliotek";
$lang['sql_attributes']="Attributter";
$lang['sql_enumsethelp']="For felttyperne ENUM og SET, brug venligst Størrelse til at indtaste værdilisten.
Værdierne skal være omgivet af ' og adskilt med kommaer.
Hvis der bruges specialtegn skal de escapes med \ (backslash)
Eksempler:
'a','b','c'
'ja','nej'
'x','y'";
$lang['sql_uploadedfile']="indlæst fil: ";
$lang['sql_import']="Import i Database `%s`";
$lang['export']="Eksport";
$lang['import']="Import";
$lang['importoptions']="Import-opsætning";
$lang['csvoptions']="CSV-opsætning";
$lang['importtable']="Import i Tabel";
$lang['newtable']="Ny tabel";
$lang['importsource']="Import-kilde";
$lang['fromtextbox']="fra tekstboks";
$lang['fromfile']="fra fil";
$lang['emptytablebefore']="Tøm tabel før";
$lang['createautoindex']="Opret Auto-Indeks";
$lang['csv_namefirstline']="Feltnavne i første linie";
$lang['csv_fieldseperate']="Felter adskilt med";
$lang['csv_fieldsenclosed']="Felter lukket inde i";
$lang['csv_fieldsescape']="Felter escaped med";
$lang['csv_eol']="Udskil linier med";
$lang['csv_null']="Erstat NULL med";
$lang['csv_fileopen']="Åbn CSV-fil";
$lang['importieren']="Import";
$lang['sql_export']="Eksport fra Database `%s`";
$lang['exportoptions']="Eksport-opsætning";
$lang['excel2003']="Excel fra 2003";
$lang['showresult']="vis resultat";
$lang['sendresultasfile']="send resultat som fil";
$lang['exportlines']="<strong>%s</strong> linier eksporteret";
$lang['csv_fieldcount_nomatch']="Felt-tælleren stemmer ikke overens med de importerede data (%d i stedet for %d).";
$lang['csv_fieldslines']="%d felter genkendt, totalt %d linier";
$lang['csv_errorcreatetable']="Fejl ved oprettelse af tabel `%s` !";
$lang['fm_uploadfilerequest']="vælg venligst en fil.";
$lang['csv_nodata']="Ingen data fundet til import!";
$lang['sqllib_generalfunctions']="generelle funktioner";
$lang['sqllib_resetauto']="nulstil auto-increment (forøgelse)";
$lang['sqllib_boards']="Boards";
$lang['sqllib_deactivateboard']="deaktiver Board";
$lang['sqllib_activateboard']="aktiver Board";
$lang['sql_notablesselected']="Ingen tabeller valgt!";
$lang['tools']="Funktioner";
$lang['tools_toolbox']="Vælg Database / Datebasefunktioner / Import - Eksport";
$lang['sqllib_boardoffline']="Board offline";
$lang['sql_openfile']="Åbn SQL-fil";
$lang['sql_openfile_button']="Upload";
$lang['max_upload_size']="Maksimal filstørrelse";
$lang['sql_search']="Søg";
$lang['sql_searchwords']="Søgeord";
$lang['start_sql_search']="Start søgning";
$lang['reset_searchwords']="nulstil søgeord";
$lang['search_explain']="Indtast et eller flere søgeord og begynd at søge ved at klikke \"start søgning\".<br> Du kan indtaste mere end et søgeord ved at afskille dem med mellemrum.";
$lang['search_options']="Søgeindstillinger";
$lang['search_results']="Søgningen efter \"<b>%s</b>\" i tabellen \"<b>%s</b>\" giver følgende resultater";
$lang['search_no_results']="Søgningen efter \"<b>%s</b>\" i tabel \"<b>%s</b>\" gav ingen rsultater!";
$lang['no_entries']="Tabel \"<b>%s</b>\" er tom og indeholder ingen poster.";
$lang['search_access_keys']="Bladre: fremad=ALT+V, baglæns=ALT+C";
$lang['search_options_or']="en kolonne skal indeholde et af søgeordene (ELLER-søgning)";
$lang['search_options_concat']="en række skal indeholde alle søgeordene men kan være i hvilkensomhelst kolonne (kan tage noget tid)";
$lang['search_options_and']="en kolonne skal indeholde ALLE søgeord (OG-søgning)";
$lang['search_in_table']="Søg i tabel";
$lang['sql_edit_tablestructure']="Edit tablestructure";
$lang['default_charset']="Default character set";


?>