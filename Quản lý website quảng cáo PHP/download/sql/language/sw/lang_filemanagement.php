<?php
//generated at 17.03.2007

$lang['convert_start']="Starta konvertering";
$lang['convert_title']="Konvertera dump till MSD-formatet";
$lang['convert_wrong_parameters']="Fel parametrar! Konverteringen kan ej genomföras.";
$lang['fm_uploadfilerequest']="Gebe bitte eine Datei an.";
$lang['fm_uploadnotallowed1']="Dieser Dateityp ist nicht erlaubt.";
$lang['fm_uploadnotallowed2']="Gültige Typen sind: *.gz und *.sql-Dateien";
$lang['fm_uploadmoveerror']="Die hochgeladene Datei konnte nicht in den richtigen Ordner verschoben werden.";
$lang['fm_uploadfailed']="Der Upload ist leider fehlgeschlagen!";
$lang['fm_uploadfileexists']="Es existiert bereits eine Datei mit diesem Namen!";
$lang['fm_nofile']="Du hast gar keine Datei ausgewählt!";
$lang['fm_delete1']="Die Datei ";
$lang['fm_delete2']=" wurde erfolgreich gelöscht.";
$lang['fm_delete3']=" konnte nicht gelöscht werden!";
$lang['fm_choose_file']="gewählte Datei:";
$lang['fm_filesize']="Dateigröße";
$lang['fm_filedate']="Datum";
$lang['fm_nofilesfound']="Keine Datei gefunden.";
$lang['fm_tables']="Tabellen";
$lang['fm_records']="Einträge";
$lang['fm_all_bu']="alle Backups";
$lang['fm_anz_bu']="Anzahl aller Backups";
$lang['fm_last_bu']="letztes Backup";
$lang['fm_totalsize']="gesamte Größe";
$lang['fm_selecttables']="Select tables";
$lang['fm_comment']="Kommentar eingeben";
$lang['fm_restore']="Wiederherstellen";
$lang['fm_alertrestore1']="Soll die Datenbank ";
$lang['fm_alertrestore2']="mit den Inhalten der Datei";
$lang['fm_alertrestore3']="wiederhergestellt werden?";
$lang['fm_delete']="ausgewählte Dateien
Löschen";
$lang['fm_askdelete1']="Möchtest Du die Datei ";
$lang['fm_askdelete2']=" wirklich löschen?";
$lang['fm_askdelete3']="Möchtest Du Autodelete nach den eingestellten Regeln jetzt ausführen?";
$lang['fm_askdelete4']="Möchtest Du alle Backupdateien jetzt löschen?";
$lang['fm_askdelete5']="Möchtest Du alle Backupdateien mit ";
$lang['fm_askdelete5_2']="_* jetzt löschen?";
$lang['fm_deleteauto']="Autodelete manuell ausführen";
$lang['fm_deleteall']="alle Backupdateien löschen";
$lang['fm_deleteallfilter']="alle löschen mit
";
$lang['fm_deleteallfilter2']="_*";
$lang['fm_startdump']="Neues Backup starten";
$lang['fm_fileupload']="Datei hochladen";
$lang['fm_dbname']="Datenbankname";
$lang['fm_files1']="Datenbank-Backups";
$lang['fm_files2']="Datenbank-Strukturen";
$lang['fm_autodel1']="Autodelete: Folgende Dateien wurden aufgrund der maximalen Files gelöscht:";
$lang['fm_autodel2']="Autodelete: Folgende Dateien wurden aufgrund ihres Erstellungsdatums gelöscht:";
$lang['fm_dumpsettings']="Einstellungen für das Backup";
$lang['fm_dumpsettings_cron']="Einstellungen für das Perl-Cronscript";
$lang['fm_oldbackup']="(unbekannt)";
$lang['fm_restore_header']="Wiederherstellung der Datenbank <strong>";
$lang['fm_restore_header2']="\"</strong>";
$lang['fm_dump_header']="Backup";
$lang['DoCronButton']="Utför Perl-cronscript";
$lang['DoPerlTest']="Testa Perl-modulerna";
$lang['DoSimpleTest']="Testa Perl";
$lang['perloutput1']="Eintrag in crondump.pl für absolute_path_of_configdir";
$lang['perloutput2']="Aufruf im Browser oder für externen Cronjob";
$lang['perloutput3']="Aufruf in der Shell oder für die Crontab";
$lang['converter']="Backup-konverterare";
$lang['convert_file']="fil som skall konverteras";
$lang['convert_filename']="Målfilens namn (utan filändelse)";
$lang['converting']="Konvertering";
$lang['convert_fileread']="Filen '%s' läses in";
$lang['convert_finished']="Konverteringen avslutad, '%s' har skapats.";
$lang['no_msd_backupfile']="Dateien anderer Programme";
$lang['max_upload_size']="Maximale Dateigröße";
$lang['max_upload_size_info']="Wenn Ihre Backup-Datei größer als das angegebene Limit ist, dann müssen Sie diese per FTP in den \"work/backup\"-Ordner hochladen. 
Danach wird diese Datei hier in der Verwaltung angezeigt uns lässt sich für eine Wiederherstellung auswählen.";
$lang['encoding']="encoding";
$lang['fm_choose_encoding']="Choose encoding of backupfile";
$lang['choose_charset']="MySQLDumper couldn't detect the encoding of the backupfile automatically.
<br>You must choose the charset with which this backup was saved.
<br>If you discover any problems with some characters after restoring, you can repeat the backup-progress and then choose another chracter set.
<br>Good luck. ;)

";


?>