<?php
//generated at 17.03.2007

$lang['noftppossible']="Es stehen keine FTP-Funktionen zur Verfügung!";
$lang['info_location']="Du befindest Dich auf ";
$lang['info_databases']="Folgende Datenbank(en) befinden sich auf dem MySql-Server:";
$lang['info_nodb']="Datenbank existiert nicht";
$lang['info_table1']="Tabelle";
$lang['info_table2']="n";
$lang['info_dbdetail']="Detail-Info von Datenbank ";
$lang['info_dbempty']="Die Datenbank ist leer !";
$lang['info_records']="Datensätze";
$lang['info_size']="Größe";
$lang['info_lastupdate']="letztes Update";
$lang['info_sum']="insgesamt";
$lang['info_optimized']="optimiert";
$lang['optimize_databases']="Tabellen optimieren";
$lang['check_tables']="Kontrollera tabeller";
$lang['clear_database']="Töm databasen";
$lang['delete_database']="Radera databas";
$lang['info_cleared']="wurde geleert";
$lang['info_deleted']="wurde gelöscht";
$lang['info_emptydb1']="Soll die Datenbank";
$lang['info_emptydb2']=" wirklich geleert werden? (ACHTUNG: Alle Daten gehen unwideruflich verloren)";
$lang['info_killdb']=" wirklich gelöscht werden? (ACHTUNG: Alle Daten gehen unwideruflich verloren)";
$lang['processkill1']="Es wird versucht, Prozess ";
$lang['processkill2']=" zu beenden.";
$lang['processkill3']="Es wird seit ";
$lang['processkill4']=" Sekunde(n) versucht, Prozess ";
$lang['htaccess1']="Verzeichnisschutz erstellen";
$lang['htaccess2']="Passwort:";
$lang['htaccess3']="Passwort (Wiederholung):";
$lang['htaccess4']="Verschlüsselungsart:";
$lang['htaccess5']="Linux und Unix-Systeme (Crypt)";
$lang['htaccess6']="Linux and Unix-Systeme (MD5)";
$lang['htaccess7']="unverschlüsselt (Windows)";
$lang['htaccess8']="Es besteht bereits ein Verzeichnisschutz. Wenn Du einen neuen erstellst, wird der alte überschrieben!";
$lang['htaccess9']="Du musst einen Namen eingeben!<br>";
$lang['htaccess10']="Die Passwörter sind nicht identisch oder leer!<br>";
$lang['htaccess11']="Soll der Verzeichnisschutz jetzt erstellt werden?";
$lang['htaccess12']="Der Verzeichnisschutz wurde erstellt.";
$lang['htaccess13']="Inhalt der Datei";
$lang['htaccess14']="Es ist ein Fehler bei der Erstellung des Verzeichnisschutzes aufgetreten!<br>Bitte erzeuge die Dateien manuell mit folgendem Inhalt:";
$lang['htaccess15']="Dringend empfohlen!";
$lang['htaccess16']=".htaccess editieren";
$lang['htaccess17']=".htaccess erstellen und editieren";
$lang['htaccess18']=".htaccess erstellen in ";
$lang['htaccess19']=" neu laden ";
$lang['htaccess20']="Skript ausführen";
$lang['htaccess21']="Handler zufügen";
$lang['htaccess22']="Ausführbar machen";
$lang['htaccess23']="Verzeichnis-Listing";
$lang['htaccess24']="Error-Dokument";
$lang['htaccess25']="Rewrite aktivieren";
$lang['htaccess26']="Deny / Allow";
$lang['htaccess27']="Redirect";
$lang['htaccess28']="Error-Log";
$lang['htaccess29']="weitere Beispiele und Dokumentation";
$lang['htaccess30']="Provider";
$lang['htaccess31']="allgemein";
$lang['htaccess32']="Achtung! Die .htaccess hat eine direkte Auswirkung auf den Browser.<br>Bei falscher Anwendung sind die Seiten nicht mehr erreichbar.";
$lang['phpbug']="Bug in zlib! Keine Kompression möglich";
$lang['disabledfunctions']="Deaktiverade funktioner";
$lang['nogzpossible']="Da Zlib nicht installiert ist, stehen keine Gzip-Funktionen zur Verfügung.";
$lang['delete_htaccess']="Remove directory protection (delete .htaccess)";
$lang['wrong_rights']="The file or the directory '%s' is not writable for me.<br>
The rights (chmod) are not set properly or it has the wrong owner.<br>
Pleae set the correct attributes using your FTP-Programm.<br>
The file or the directory needs to be set to %s.<br>";
$lang['cant_create_dir']="Couldn' t create dir '%s'. 
Please create it using your FTP-Programm.";


?>