<?php
//generated at 17.03.2007

$lang['help_db']="Aquí está la lista de las bases de datos existentes.";
$lang['help_praefix']="El prefijo sirve para filtrar las tablas, sólo se mostrarán aquellas que empiezen con él.";
$lang['help_zip']="Compresión con GZip - se recomienda activarla";
$lang['help_memorylimit']="El tamaño máximo en Bytes, que el script puede almacenar en la memoria RAM\n0 = desactivado";
$lang['memory_limit']="Límite de memoria";
$lang['help_mail1']="Si está activado, se enviará un email tras completar la copia de seguridad.";
$lang['help_mail2']="Es la dirección a donde se envía el email.";
$lang['help_mail3']="Es la dirección desde la que se envía el email.";
$lang['help_mail4']="El tamaño máximo para un fichero adjunto al email, si lo deja a 0, no se establecerá límite alguno.";
$lang['help_mail5']="Aquí se puede decir, si la copia de seguridad debe adjuntarse al email.";
$lang['help_ad1']="Si está activado, se eliminarán automáticamente las copias de seguridad.";
$lang['help_ad2']="El máximo de días que se mantiene una copia de seguridad (para el borrado automático)
0 = desactivado";
$lang['help_ad3']="El máximo de ficheros, que se guardarán en el directorio de copias de seguridad (para el borrado automático)
0 = desactivado";
$lang['help_lang']="Elija el idioma adecuado.";
$lang['help_empty_db_before_restore']="Para evitar registros redundantes, se puede hacer que la base de datos se vacíe completamente antes de la recuperación de datos";
$lang['help_cronmail']="Decide si Cronjob debe enviar la copia de seguridad por email una vez haya finalizado.";
$lang['help_cronmailprg']="El camino al programa de email; por defecto se toma sendmail con el camino especificado";
$lang['help_cronftp']="Decide si Cronjob debe enviar la copia de seguridad por FTP una vez que haya finalizado";
$lang['help_cronzip']="Compresión con GZip - se recomienda activarla (la librería de compresión debe estar instalada!)";
$lang['help_cronextender']="La extensión de los scripts perl, el estándar es '.pl'";
$lang['help_cronsavepath']="El nombre del archivo de configuración del script perl.";
$lang['help_cronprintout']="Si está desactivado, no se mostrarán los resultados en pantalla. Es independiente de guardar en el archivo histórico.";
$lang['help_cronsamedb']="Debe usarse para Cronjob la misma base de datos que está definida en las propiedades?";
$lang['help_crondbindex']="Elija la base de datos para Cronjob";
$lang['help_cronmail_dump']="Select if the Cron job should attach the backup to the email.";
$lang['help_ftptransfer']="Si está activado, se enviará el archivo de copia de seguridad por FTP.";
$lang['help_ftpserver']="Dirección del servidor de FTP.";
$lang['help_ftpport']="Puerto del servidor de FTP, estándar es 21 .";
$lang['help_ftpuser']="Escriba el nombre del usuario de la conexión FTP.";
$lang['help_ftppass']="Escriba el password de la conexión FTP.";
$lang['help_ftpdir']="¿A qué directorio debe ser enviado el archivo?";
$lang['help_speed']="Velocidades máxima y mínima, estándar es 100 hasta 5000 (velocidades demasiado altas pueden conllevar pérdidas de conexión!)";
$lang['speed']="Control de velocidad.";
$lang['help_cronexecpath']="El lugar en que se encuentra el script.\nSe parte de la dirección HTTP (como en el navegador). \nSe permiten caminos relativos y absolutos.";
$lang['cron_execpath']="Ubicación del Cronscript.";
$lang['help_croncompletelog']="Si está activada, se guardará el detalle completo de la operación en complete_log.
Ésta función es independiente de la salida de texto.";
$lang['help_ftp_mode']="When problems occur while transfering via FTP, try to use the passive mode. ";


?>