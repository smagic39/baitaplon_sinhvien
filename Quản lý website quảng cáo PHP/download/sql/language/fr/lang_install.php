<?php
//generated at 17.03.2007

$lang['installfinished']="<br>Installation terminée  --> <a href=\"index.php\">start MySQLDumper</a><br>";
$lang['install_forcescript']="Démarrer MySQLDumper sans installation";
$lang['install_tomenu']="Retour au menu principal";
$lang['installmenu']="Menu principal";
$lang['step']="Étape";
$lang['install']="Installation";
$lang['uninstall']="Désinstallation";
$lang['tools']="Outils";
$lang['editconf']="Éditer la configuration";
$lang['osweiter']="continuer sans sauvegarde";
$lang['errorman']="<strong>Erreur lors de l'écriture de la configuration!</strong><br>Veuillez éditer le fichier ";
$lang['manuell']="manuellement";
$lang['createdirs']="créer répertoire";
$lang['install_continue']="continuer avec l'installation";
$lang['connecttomysql']=" connecter à MySQL ";
$lang['dbparameter']="Paramètre de la base de données";
$lang['confignotwritable']="Impossible d'écrire le fichier \"config.php\".
Veuillez utiliser votre programme FTP et chmoder ce fichier en 0777.";
$lang['dbconnection']="Connexion base de données";
$lang['connectionerror']="Erreur: aucune connexion n'a pu être créée.";
$lang['connection_ok']="Connexion avec la base de données a été établie.";
$lang['saveandcontinue']="sauvegarder et continuer l'installation";
$lang['confbasic']="Configuration de base";
$lang['install_step2finished']="Configuration de la base de données a été sauvegardée.";
$lang['install_step2_1']="Continuer l'installation avec la configuration standart";
$lang['laststep']="Terminer l'installation";
$lang['ftpmode']="Créer les répertoires par FTP (safe_mode)";
$lang['idomanual']="Créer un répertoire manuellement";
$lang['dofrom']="terminer par";
$lang['ftpmode2']="Créer un répertoire par FTP:";
$lang['connect']="connecter";
$lang['dirs_created']="Les répertoires ont été créés avec succès.";
$lang['connect_to']="connexion vers";
$lang['changedir']="muter vers le répertoire";
$lang['changedirerror']="Mutation dans le répertoire non possible";
$lang['ftp_ok']="Paramètres FTP sont ok";
$lang['createdirs2']="Créer répertoires";
$lang['ftp_notconnected']="Connexion Ftp non établie!";
$lang['connwith']="Connexion avec";
$lang['asuser']="comme utilisateur";
$lang['notpossible']="impossible";
$lang['dircr1']="créer répertoire de travail";
$lang['dircr2']="créer répertoire de sauvegarde";
$lang['dircr3']="créer structure des répertoires";
$lang['dircr4']="créer répertoire du journal";
$lang['dircr5']="créer répertoire de configuration";
$lang['indir']="je suis dans le répertoire";
$lang['check']="vérifier";
$lang['disabledfunctions']="Fonctions désactivées";
$lang['noftppossible']="Il n'y a pas de fonction FTP à disposition!";
$lang['nogzpossible']="Il n'y a pas de fonction de compression à disposition!";
$lang['ui1']="Supprimer tous les répertoires de travail avec les sauvegardes incluses.";
$lang['ui2']="Êtes-vous sûr d´exécuter l'opération ?";
$lang['ui3']="Non, arrêter immédiatement";
$lang['ui4']="Oui, veuillez continuer";
$lang['ui5']="Supprimer répertoire de travail";
$lang['ui6']="tout a été supprimé avec succès.";
$lang['ui7']="Veuillez supprimer le répertoire de script";
$lang['ui8']="un niveau supérieur";
$lang['ui9']="Une erreur est apparue, suppression impossible</p>Erreur dans le répertoire ";
$lang['import']="Importer la configuration";
$lang['import1']="Importer configuration du fichier ";
$lang['import2']="Télécharger vers le serveur et importer";
$lang['import3']="La configuration a été chargée...";
$lang['import4']="La configuration a été sauvegardée.";
$lang['import5']="Démarrer MySQLDumper";
$lang['import6']="Menu d'installation;";
$lang['import7']="Télécharger vers le serveur la configuration";
$lang['import8']="retourner vers le téléchargement";
$lang['import9']="Ceci n'est pas une sauvegarde de la configuration !";
$lang['import10']="La configuration a été téléchargée vers le serveur avec succès ...";
$lang['import11']="<strong>Erreur: </strong>Il y a des problèmes d'écriture pour les sql_statements";
$lang['import12']="<strong>Erreur: </strong>Il y a des problèmes d'écriture pour le fichier config.php";
$lang['install_help_port']="(vide = Port standart)";
$lang['install_help_socket']="(vide = Socket standart)";
$lang['tryagain']="Réessayer";
$lang['socket']="Socket";
$lang['port']="Port";
$lang['found_no_db']="Erreur: La base de données n'a pas été trouvée";
$lang['found_db']="Base de données trouvée";
$lang['fm_fileupload']="Télécharger un fichier vers le serveur";
$lang['pass']="Mot de passe";
$lang['no_db_found_info']="La connexion avec la base de données a été établie avec succès.<br>
Vos données utilisateur sont valides et ont été acceptées par le serveur MySQL.<br>
Mais, MySQLDumper n'a pas été capable de trouver une base de données.<br>
La détection automatique via le script est dans quelques cas bloquée par certains serveurs.<br>
Vous devez alors entrer manuellement le nom de votre base de données après la fin de l'installation.
Cliquer sur \"configuration\" \"Affichage des paramêtres de connexion\" et entrer le nom de votre base de données.";


?>