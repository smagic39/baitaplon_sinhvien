<?php
//generated at 17.03.2007

$lang['convert_start']="Démarrer la conversion";
$lang['convert_title']="Convertir copie de sauvegarde vers Format MSD";
$lang['convert_wrong_parameters']="Mauvais paramètres! Conversion impossible.";
$lang['fm_uploadfilerequest']="Veuillez entrer un fichier.";
$lang['fm_uploadnotallowed1']="Ce type de fichier n'est pas permis.";
$lang['fm_uploadnotallowed2']="Les types valides sont les fichiers: *.gz et *.sql";
$lang['fm_uploadmoveerror']="Le fichier téléchargé sur le serveur n'a pas pu être déplacé dans le répertoire désiré.";
$lang['fm_uploadfailed']="Le téléchargement sur le serveur a échoué!";
$lang['fm_uploadfileexists']="Il existe déjà un fichier avec ce nom !";
$lang['fm_nofile']="Vous n'avez sélectionné aucun fichier!";
$lang['fm_delete1']="Le fichier ";
$lang['fm_delete2']=" a été supprimé avec succès.";
$lang['fm_delete3']=" n'a pas pu être supprimé!";
$lang['fm_choose_file']="Fichier sélectionné:";
$lang['fm_filesize']="Taille du fichier";
$lang['fm_filedate']="Date";
$lang['fm_nofilesfound']="Aucun fichier trouvé.";
$lang['fm_tables']="Tables";
$lang['fm_records']="Enregistrements";
$lang['fm_all_bu']="toutes les sauvegardes";
$lang['fm_anz_bu']="Nombres de sauvegardes";
$lang['fm_last_bu']="dernière sauvegarde";
$lang['fm_totalsize']="taille totale";
$lang['fm_selecttables']="Nombres de tables";
$lang['fm_comment']="Ajouter un commentaire";
$lang['fm_restore']="Restauration";
$lang['fm_alertrestore1']="Voulez-vous que la base de données ";
$lang['fm_alertrestore2']="avec le contenu des fichiers";
$lang['fm_alertrestore3']="soit restaurée?";
$lang['fm_delete']="Supprimer les 
fichiers sélectionnés";
$lang['fm_askdelete1']="Voulez-vous vraiment supprimer le fichier ";
$lang['fm_askdelete2']=" ?";
$lang['fm_askdelete3']="Voulez-vous exécuter la suppression automatique d'après les règles sauvegardées?";
$lang['fm_askdelete4']="Voulez-vous supprimer toutes les copies de sauvegarde?";
$lang['fm_askdelete5']="Voulez-vous supprimer toutes les copies de sauvegarde avec ";
$lang['fm_askdelete5_2']="_* ?";
$lang['fm_deleteauto']="Exécuter manuellement\nsuppression automatique";
$lang['fm_deleteall']="Supprimer toutes les\ncopies de sauvegarde";
$lang['fm_deleteallfilter']="Supprimer tout avec
";
$lang['fm_deleteallfilter2']="_*";
$lang['fm_startdump']="Exécuter une nouvelle\ncopie de sauvegarde";
$lang['fm_fileupload']="Télécharger un fichier vers le serveur";
$lang['fm_dbname']="Nom de la base de données";
$lang['fm_files1']="Copie de sauvegarde de la base de données";
$lang['fm_files2']="Structure de la base de données";
$lang['fm_autodel1']="Suppression automatique: Les fichiers suivant ont été supprimés du fait du nombre limité de fichiers:";
$lang['fm_autodel2']="Suppression automatique: Les fichiers suivant ont été supprimés du fait de la date du fichier:";
$lang['fm_dumpsettings']="Configuration pour la copie de sauvegarde";
$lang['fm_dumpsettings_cron']="Configuration pour le script Cron en Perl";
$lang['fm_oldbackup']="(inconnu)";
$lang['fm_restore_header']="Restauration de la base de donnée <strong>";
$lang['fm_restore_header2']="\"</strong>";
$lang['fm_dump_header']="Copie de sauvegarde";
$lang['DoCronButton']="Exécuter le script Cron";
$lang['DoPerlTest']="Tester le module Perl";
$lang['DoSimpleTest']="Tester Perl";
$lang['perloutput1']="Saisie dans crondump.pl pour absolute_path_of_configdir";
$lang['perloutput2']="Saisie dans un navigateur ou pour exécuter un script Cron externe";
$lang['perloutput3']="Saisie pour 'Shell' ou pour la crontab";
$lang['converter']="Convertir copie de sauvegarde";
$lang['convert_file']="fichier qui doit être converti";
$lang['convert_filename']="Nom du fichier final (sans extensions)";
$lang['converting']="En cours de convertion";
$lang['convert_fileread']="Lire fichier '%s'";
$lang['convert_finished']="Convertion terminée, fichier '%s' créé avec succès.";
$lang['no_msd_backupfile']="Copies de sécurités d'autres programmes";
$lang['max_upload_size']="Taille maximale du fichier";
$lang['max_upload_size_info']="Si votre fichier Dump est plus grand que la taille mentionnée plus haut, vous devez alors télécharger vers le serveur dans le répertoire avec votre programme FTP.";
$lang['encoding']="encoding";
$lang['fm_choose_encoding']="Choisissez le type d'encodage de la sauvegarde";
$lang['choose_charset']="MySQLDumper n'a pas pu détecter automatiquement le type d'encodage de la sauvegarde <br>Vous devez choisir le jeux de caractères qui a été utilisé pour la sauvegarde<br> Si vous découvrez des problèmes avec quelques caractères suite à la restauration, vous pouvez répéter l'opération en choisissant un autre jeux de caractères.<br>Bonne chance ;-)";


?>