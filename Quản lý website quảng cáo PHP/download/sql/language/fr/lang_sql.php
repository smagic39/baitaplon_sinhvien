<?php
//generated at 17.03.2007

$lang['command']="Requête";
$lang['sql_view_normal']="Affichage: Normal";
$lang['sql_view_compact']="Affichage: Compact";
$lang['import_notable']="Aucune table n'a été sélectionné pour l'import!";
$lang['sql_warning']="L'exécution de requête SQL peut influencer les données. L'auteur dénie toute responsabilité pour des pertes de données.";
$lang['sql_exec']="Exécuter la requête SQL";
$lang['sql_dataview']="Aperçu données";
$lang['sql_tableview']="Aperçu des tables";
$lang['sql_vonins']="d'un total de";
$lang['sql_nodata']="aucunes données";
$lang['sql_recordupdated']="L'enregistrement a été modifié";
$lang['sql_recordinserted']="L'enregistrement a été sauvegardé";
$lang['sql_backdboverview']="retourner vers l'aperçu général des bases de données";
$lang['sql_recorddeleted']="L'enregistrement a été supprimé";
$lang['asktableempty']="Voulez-vous vider la table `%s`?";
$lang['sql_recordedit']="Éditer l'enregistrement";
$lang['sql_recordnew']="Insérer un nouvel enregistrement ";
$lang['askdeleterecord']="Voulez-vous supprimer l'enregistrement?";
$lang['askdeletetable']="voulez-vous supprimer la table `%s`?";
$lang['sql_befehle']="Requête SQL";
$lang['sql_befehlneu']="nouvelle requête";
$lang['sql_befehlsaved1']="Requête SQL";
$lang['sql_befehlsaved2']="a été ajouté";
$lang['sql_befehlsaved3']="a été sauvegardé";
$lang['sql_befehlsaved4']="a été placé plus haut";
$lang['sql_befehlsaved5']="a été supprimé";
$lang['sql_queryentry']="L'intérrogation contient";
$lang['sql_columns']="colonnes";
$lang['askdbdelete']="Voulez-vous vraiment supprimer la base de données `%s` avec son contenu?";
$lang['askdbempty']="Voulez-vous vraiment vider la base de données  `%s`?";
$lang['askdbcopy']="Voulez-vous copier le contenu de la base de données  `%s` dans la base de données  %s`?";
$lang['sql_tablenew']="Éditer tables";
$lang['sql_output']="Sortie SQL";
$lang['do_now']="Exécuter maintenant";
$lang['sql_namedest_missing']="Nom de la base de données manque!";
$lang['askdeletefield']="Voulez-vous supprimer le champ?";
$lang['sql_commands_in']=" Ligne a été travaillé en ";
$lang['sql_commands_in2']="  sec.";
$lang['sql_out1']="Il y a eu ";
$lang['sql_out2']="Requêtes exécutées";
$lang['sql_out3']="Il y a eu ";
$lang['sql_out4']="Commentaire";
$lang['sql_out5']="Comme la sortie contient plus de 5000 lignes celles-ci ne sont pas affichées.";
$lang['sql_selecdb']="Sélectionner la base de données";
$lang['sql_tablesofdb']="Tables de la base de données";
$lang['sql_edit']="éditer";
$lang['sql_nofielddelete']="Suppression impossible, car une table doit avoir au minimum 1 champ.";
$lang['sql_fielddelete1']="Le champ";
$lang['sql_deleted']="a été supprimé";
$lang['sql_changed']="a été modifié.";
$lang['sql_created']="a été créé.";
$lang['sql_nodest_copy']="Sans destination on ne peut pas copier !";
$lang['sql_desttable_exists']="La table de destination existe déjà !";
$lang['sql_scopy']="Structure de la table `%s` a été copié dans la table `%s`.";
$lang['sql_tcopy']="La table `%s` a été copié avec les données de la table `%s`.";
$lang['sql_tablenoname']="La table doit avoir un nom !";
$lang['sql_tblnameempty']="Le nom de la table ne peut pas être vide !";
$lang['sql_collatenotmatch']="Police de caractères et triage ne sont pas compatibles !";
$lang['sql_fieldnamenotvalid']="Erreur: Nom de champs invalidee";
$lang['sql_createtable']="créer table";
$lang['sql_copytable']="copier table";
$lang['sql_structureonly']="seulement la structure";
$lang['sql_structuredata']="Structure et données";
$lang['sql_notablesindb']="Il n'y a pas de table dans la base de données";
$lang['sql_selecttable']="Sélectionner la table";
$lang['sql_showdatatable']="Montrer les données dans la table";
$lang['sql_tblpropsof']="Propriété de la table";
$lang['sql_editfield']="Éditer champs";
$lang['sql_newfield']="Nouveau champ";
$lang['sql_indexes']="Indices";
$lang['sql_atposition']="créer à la position";
$lang['sql_first']="en premier";
$lang['sql_after']="après";
$lang['sql_changefield']="Changer champs";
$lang['sql_insertfield']="Créer champs";
$lang['sql_insertnewfield']="créer nouveau champs";
$lang['sql_tableindexes']="Indices de la table";
$lang['sql_allowdups']="Permettre duplicata";
$lang['sql_cardinality']="Cardinalité";
$lang['sql_tablenoindexes']="La table ne contient aucun indice";
$lang['sql_createindex']="créer nouveau index";
$lang['sql_wasemptied']="a été vidé";
$lang['sql_renamedto']="a été renommé par";
$lang['sql_dbcopy']="Le contenue de la base de données `%s` a été copié dans la base de données `%s`.";
$lang['sql_dbscopy']="La structure de la  base de données `%s` a été copié dans la base de données `%s`.";
$lang['sql_wascreated']="a été créé";
$lang['sql_renamedb']="Renommer la base de données";
$lang['sql_actions']="Actions";
$lang['sql_chooseaction']="Sélectionner l'action";
$lang['sql_deletedb']="Supprimer la base de données";
$lang['sql_emptydb']="Vider la base de données";
$lang['sql_copydatadb']="Copier le contenu dans la base de données";
$lang['sql_copysdb']="Copier la structure dans la base de données";
$lang['sql_imexport']="Import/Export";
$lang['info_records']="Enregistrements";
$lang['asktableemptykeys']="Voulez-vous vider la table `%s` et remettre les indices?";
$lang['edit']="éditer";
$lang['delete']="supprimer";
$lang['empty']="vider";
$lang['emptykeys']="vider et remettre les indices";
$lang['sql_tableemptied']="Table `%s` a été vidée.";
$lang['sql_tableemptiedkeys']="Table `%s` a été vidée et les indices ont été remis.";
$lang['sql_library']="Bibliothèque SQL";
$lang['sql_attributes']="Attributs";
$lang['sql_enumsethelp']="Pour les champs du type ENUM et SET veuillez entrer sous Taille les valeurs.

Les valeurs doivent être définis entre guillemets et séparés avec une virgule.

L'utilisation de caractères spéciaux doit être masqué avec \ (Backslash).


Exemple:

'a','b','c'

'oui','non'

'x','y'";
$lang['sql_uploadedfile']="Fichier chargé: ";
$lang['sql_import']="Import dans la base de données `%s`";
$lang['export']="Export";
$lang['import']="Import";
$lang['importoptions']="Options d'import";
$lang['csvoptions']="Options CSV";
$lang['importtable']="Import dans la table";
$lang['newtable']="nouvelle table";
$lang['importsource']="Source d'import";
$lang['fromtextbox']="de la zone de texte";
$lang['fromfile']="du fichier";
$lang['emptytablebefore']="Vider avant les tables";
$lang['createautoindex']="Créer index automatique";
$lang['csv_namefirstline']="Nom du champ dans la première ligne";
$lang['csv_fieldseperate']="Séparer les champs avec";
$lang['csv_fieldsenclosed']="Inclure les champs avec";
$lang['csv_fieldsescape']="Champs d'échappement";
$lang['csv_eol']="Ligne séparée avec";
$lang['csv_null']="Remplace ZÉRO par";
$lang['csv_fileopen']="Ouvrir fichier CSV";
$lang['importieren']="importer";
$lang['sql_export']="Export de la base de données `%s`";
$lang['exportoptions']="Options d'export";
$lang['excel2003']="à partir d'Excel 2003";
$lang['showresult']="Montrer le résultat";
$lang['sendresultasfile']="Envoyer le résultat comme fichier";
$lang['exportlines']="<strong>%s</strong> lignes exportées";
$lang['csv_fieldcount_nomatch']="Le nombre de tables ne correspondent pas au nombre de données à importer (%d à la place de %d).";
$lang['csv_fieldslines']="%d champs calculer, en tout %d lignes";
$lang['csv_errorcreatetable']="Erreur lors de la création de la table `%s` !";
$lang['fm_uploadfilerequest']="Veuillez entrer un fichier.";
$lang['csv_nodata']="Aucun fichier à importer a été trouvé!";
$lang['sqllib_generalfunctions']="Fonctions générales";
$lang['sqllib_resetauto']="Remettre valeur par défaut";
$lang['sqllib_boards']="Boards";
$lang['sqllib_deactivateboard']="Désactiver Board";
$lang['sqllib_activateboard']="Activer Board";
$lang['sql_notablesselected']="Aucune table n'est sélectionnée !";
$lang['tools']="Outils";
$lang['tools_toolbox']="Sélectionner la base de données / Fonctions de la base de données / Import et export";
$lang['sqllib_boardoffline']="Forum hors ligne";
$lang['sql_openfile']="Ouvrir le fichier SQL";
$lang['sql_openfile_button']="Envoyer";
$lang['max_upload_size']="Taille maximale du fichier";
$lang['sql_search']="Rechercher";
$lang['sql_searchwords']="rechercher le(s) mot(s)";
$lang['start_sql_search']="Commencer la recherche";
$lang['reset_searchwords']="reset searchwords";
$lang['search_explain']="Emnter one or more searchwords and start searching by clicking \"start search\".<br>
You can enter more than one searchword by separating them with a space.";
$lang['search_options']="Searchoptions";
$lang['search_results']="The search for \"<b>%s</b>\" in table \"<b>%s</b>\" brings the following results";
$lang['search_no_results']="The search for \"<b>%s</b>\" in table \"<b>%s</b>\" doesn't bring any hits!";
$lang['no_entries']="Table \"<b>%s</b>\" is empty and doesn't have any entry.";
$lang['search_access_keys']="Browse: forward=ALT+V, backwards=ALT+C";
$lang['search_options_or']="a column must have one of the searchwords (OR-search)";
$lang['search_options_concat']="a row must contain all of the searchwords but they can be in any column (could take some time)";
$lang['search_options_and']="a column must contain all searchwords (AND-search)";
$lang['search_in_table']="Search in table";
$lang['sql_edit_tablestructure']="Modifier la structure des tables";
$lang['default_charset']="Jeux de caractèères par défaut";


?>