<?php
//generated at 17.03.2007

$lang['yes']="yes";
$lang['to']="to";
$lang['activated']="aktivéiert";
$lang['not_activated']="not activated";
$lang['error']="Error";
$lang['of']=" of ";
$lang['added']="derbäigesaat";
$lang['db']="Datebank";
$lang['dbs']="Datebanken";
$lang['tables']="Tables";
$lang['table']="Table";
$lang['records']="Records";
$lang['compressed']="kompriméiert (gz)";
$lang['notcompressed']="normal (uncompressed)";
$lang['general']="general";
$lang['comment']="Kommentar";
$lang['filesize']="File size";
$lang['all']="alles";
$lang['none']="none";
$lang['with']=" with ";
$lang['dir']="Directory";
$lang['rechte']="Permissions";
$lang['status']="State";
$lang['finished']="finished";
$lang['file']="File";
$lang['fields']="Fields";
$lang['new']="new";
$lang['charset']="Zeechesaaz";
$lang['collation']="Sortéieren";
$lang['change']="änneren";
$lang['in']="in";
$lang['do']="Execute";
$lang['view']="view";
$lang['existing']="existing";
$lang['authors']="Autoren";
$lang['back']="zerëck";
$lang['normal']="normal";
$lang['db_host']="Host";
$lang['db_user']="User";
$lang['db_pass']="Password";
$lang['info_scriptdir']="Directory of MySQLDumper";
$lang['info_actdb']="Actual Database";
$lang['useconnection']="Use Connection";
$lang['wrongconnectionpars']="Wrong or no connection parameter!";
$lang['conn_not_possible']="Keng Verbindung méiglech!";
$lang['servercaption']="Display Server";
$lang['help_servercaption']="When using MySQLDumper on different domains or servers, it can be helpful to display the domain/server name at the top of the screen.";
$lang['activate_multidump']="MultiDump aktivéieren";
$lang['save']="Save";
$lang['reset']="Reset";
$lang['praefix']="Table Prefix";
$lang['autodelete']="automatescht Läschen vun Backups";
$lang['max_backup_files_each1']="For all databases";
$lang['max_backup_files_each2']="For each database";
$lang['saving_db_form']="Database";
$lang['testconnection']="Test Connection";
$lang['back_to_minisql']="Datebank editéieren";
$lang['critical_safemode']="<h3>Oppassen: Kann net am SafeMode weiderfueren!</h3>Des Ordner mussen manuell am Scriptordner erstallt ginn:<br><div style=";
$lang['create']="erstellen";
$lang['mailabsendererror']="Mail without Sender can't be sent !";
$lang['Ausgabe']="Output";
$lang['Zusatz']="Addition";
$lang['Variabeln']="Variables";
$lang['berichtsent']="Den Feelerrapport gouf mat Erfollëg gesend";
$lang['autobericht']="automatesch généréierten Feelerrapport vir";
$lang['berichtman1']="wannechgeliwt send d'E-Mail manuell un";
$lang['Statusinformationen']="Status Information";
$lang['Versionsinformationen']="Version Information";
$lang['MySQL Dumper Informationen']="MySQL Dumper Information";
$lang['Fehlerbericht']="Error Report";
$lang['backupfilesanzahl']="Am Backup Ordner sinn";
$lang['lastbackup']="Last Backup";
$lang['notavail']="<em>not available</em>";
$lang['vom']="from";
$lang['mysqlvars']="MySQL Variables";
$lang['mysqlsys']="MySQL Commands";
$lang['Status']="State";
$lang['Prozesse']="Processes";
$lang['info_novars']="no variables available";
$lang['Inhalt']="Value";
$lang['info_nostatus']="no state available";
$lang['info_noprocesses']="no running processes";
$lang['fm_freespace']="Free Space on Server";
$lang['load_database']="Reload databases";
$lang['home']="Home";
$lang['config']="Konfiguratioun";
$lang['dump']="Backup";
$lang['restore']="Restore";
$lang['file_manage']="File Administration";
$lang['log']="Log";
$lang['choose_db']="Datebank auswielen";
$lang['credits']="Credits / Hëllëf";
$lang['multi_part']="Multipart Backup";
$lang['logfilenotwritable']="Can't write Logfile !";
$lang['sql_error1']="Error in Query:";
$lang['sql_error2']="MySQL says:";
$lang['unknown']="unknown";
$lang['unknown_number_of_records']="unknown";
$lang['cron_completelog']="Ganz Ausgab loggen";
$lang['no']="no";
$lang['MySQLErrorDoc']="MySQL Documentation of Errors";
$lang['downgrade']="Downgrade (MySQL 4.x => 3.x)";
$lang['create_database']="Nei Datebank erstellen";
$lang['exportfinished']="Export finalized.";
$lang['sql_browser']="SQL-Browser";
$lang['server']="Server";
$lang['browse']="View";
$lang['mysql_connection_encoding']="Standard encoding of MySQL-Server";


?>