var xml = makeXML();
var el = new Array();
var options = new Array();
options['pagerank'] = 'PageRank';
options['alexaRank'] = 'Alexa S�ras�';
options['dmoz'] = 'DMOZ Listesinde';
options['backlinksGoogle'] = 'Google BackLinkleri';
options['backlinksYahoo'] = 'Yahoo BackLinkleri';
options['backlinksMSN'] = 'Msn BackLinkleri';
options['resultsAltaVista'] = 'AltaVista Sonucu';
options['resultsAllTheWeb'] = 'All the Web Sonucu';
options['thumbnail'] = 'Site G�r�nt�s�';
function makeXML () {
	if (typeof XMLHttpRequest == 'undefined') {
		objects = Array(
			'Microsoft.XmlHttp',
			'MSXML2.XmlHttp',
			'MSXML2.XmlHttp.3.0',
			'MSXML2.XmlHttp.4.0',
			'MSXML2.XmlHttp.5.0'
		);
		for (i in objects) {
			try {
				return new ActiveXObject(objects[i]);
			} catch (e) {}
		}
	} else {
		return new XMLHttpRequest();
	}
}
function get (id) {
	return document.getElementById(id);
}
function getResults (url) {
	el['results'] = get('results');
	xml.open('get', 'getResults.php?url=' + window.encodeURI(url));
	xml.onreadystatechange = function () {
		if (xml.readyState == 4) {
			el['results'].innerHTML = '';
			if (window.ActiveXObject) {
				doc = new ActiveXObject('Microsoft.XMLDOM');
				doc.async = 'false';
				doc.loadXML(xml.responseText);
			} else {
				parser = new DOMParser();
				doc = parser.parseFromString(xml.responseText,'text/xml');
			}
			xmlDoc = doc.documentElement;
			x = 0;
			for (i in options) {
				x++;
				title = options[i];
				value = xmlDoc.getElementsByTagName(i)[0].childNodes[0].nodeValue;
				if (i == 'thumbnail') {
					value = '<img src="' + value + '" alt="Thumbnail Not Available" />';
				}
				if (i == 'dmoz') {
					if (value == '1') {
						value = 'Evet';
					} else {
						value = 'Hay�r';
					}
				}
				style = (x % 2) ? ' style="background-color: #EEEEEE"' : '';
				el['results'].innerHTML += '<div' + style + '><span>' + value + '</span>' + title + '<b class="clear"></b></div>';
			}
		} else {
			el['results'].innerHTML = '<div>Bilgiler Y�kleniyor...</div>';
		}
	}
	xml.send(null);
}