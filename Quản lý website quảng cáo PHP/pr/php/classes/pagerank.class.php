<?php
	class pagerank {
		
		var $url;
		
		function pagerank ($url, $options) {
			header('Content-Type: text/xml');
			$this->url = parse_url('http://' . ereg_replace('^http://', '', $url));
			$this->url['full'] = 'http://' . ereg_replace('^http://', '', $url);
			echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
			echo "<result>\n";
			if ($options['pagerank']) {
				$this->getPagerank();
			}
			if ($options['alexaRank']) {
				$this->getAlexaRank();
			}
			if ($options['dmoz']) {
				$this->getDmoz();
			}
			if ($options['backlinksGoogle']) {
				$this->getBacklinksGoogle();
			}
			if ($options['backlinksYahoo']) {
				$this->getBacklinksYahoo();
			}
			if ($options['backlinksMSN']) {
				$this->getBacklinksMSN();
			}
			if ($options['resultsAltaVista']) {
				$this->getResultsAltaVista();
			}
			if ($options['resultsAllTheWeb']) {
				$this->getResultsAllTheWeb();
			}
			if ($options['thumbnail']) {
				$this->getThumbnail();
			}
			echo '</result>';
		}
		
		function getPage ($url) {
			if (function_exists('curl_init')) {
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
				curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
				return curl_exec($ch);
			} else {
				return file_get_contents($url);
			}
		}
		
		function getPagerank () {
			$url = 'info:' . $this->url['host'];
			$checksum = $this->checksum($this->strord($url));
			$url = "http://www.google.com/search?client=navclient-auto&ch=6$checksum&features=Rank&q=$url";
			$data = $this->getPage($url);
			preg_match('#Rank_[0-9]:[0-9]:([0-9]+){1,}#si', $data, $p);
			$value = ($p[1]) ? $p[1] : 0;
			echo "\t<pagerank>$value</pagerank>\n";
		}
		
		function getAlexaRank() {
			$url = $this->url['host'];
			$url = "http://data.alexa.com/data?cli=10&dat=s&url=$url";
			$data = $this->getPage($url);
			preg_match('#<POPULARITY URL="(.*?)" TEXT="([0-9]+){1,}"/>#si', $data, $p);
			$value = ($p[2]) ? number_format($this->toInt($p[2])) : 0;
			echo "\t<alexaRank>$value</alexaRank>\n";
		
		}
		
		function getDmoz() {
			$url = ereg_replace('^www\.', '', $this->url['host']);
			$url = "http://search.dmoz.org/cgi-bin/search?search=$url";
			$data = $this->getPage($url);
			if (ereg('<center>No <b><a href="http://dmoz\.org/">Open Directory Project</a></b> results found</center>', $data)) {
				$value = '0';
			} else {
				$value = '1';
			}
			echo "\t<dmoz>$value</dmoz>\n";
		}
		
		function getBacklinksGoogle() {
			$url = $this->url['host'];
			$url = 'http://www.google.com/search?q=link%3A' . urlencode($url);
			$data = $this->getPage($url);
			preg_match('#of about <b>([0-9,]+){1,}#si', $data, $p);
			$value = ($p[1]) ? number_format($this->toInt($p[1])) : 0;
			echo "\t<backlinksGoogle>$value</backlinksGoogle>\n";
		}
		
		function getBacklinksYahoo() {
			$url = $this->url['host'];
			$url = 'http://siteexplorer.search.yahoo.com/search?p=' . urlencode("http://$url") . '&bwm=i';
			$data = $this->getPage($url);
			preg_match('#Inlinks \(([0-9,]+){1,}\)#si', $data, $p);
			$value = ($p[1]) ? number_format($this->toInt($p[1])) : 0;
			echo "\t<backlinksYahoo>$value</backlinksYahoo>\n";
		}
		
		function getBacklinksMSN() {
			$url = $this->url['host'];
			$url = 'http://search.live.com/results.aspx?q=link%3A' . urlencode($url);
			$data = $this->getPage($url);
			preg_match('#1 of ([0-9,]+){1,}#si', $data, $p);
			$value = ($p[1]) ? number_format($this->toInt($p[1])) : 0;
			echo "\t<backlinksMSN>$value</backlinksMSN>\n";
		}
		
		function getResultsAltaVista() {
			$url = $this->url['host'];
			$url = 'http://www.altavista.com/web/results?q=' . urlencode($url);
			$data = $this->getPage($url);
			preg_match('#AltaVista found ([0-9,]+){1,} results#si', $data, $p);
			$value = ($p[1]) ? number_format($this->toInt($p[1])) : 0;
			echo "\t<resultsAltaVista>$value</resultsAltaVista>\n";
		}
		
		function getResultsAllTheWeb() {
			$url = $this->url['host'];
			$url = 'http://www.alltheweb.com/search?q=' . urlencode($url);
			$data = $this->getPage($url);
			preg_match('#<span class="ofSoMany">([0-9,]+){1,}</span>#si', $data, $p);
			$value = ($p[1]) ? number_format($this->toInt($p[1])) : 0;
			echo "\t<resultsAllTheWeb>$value</resultsAllTheWeb>\n";
		}
		
		function getThumbnail() {
			$url = urlencode($this->url['host']);
			echo "\t<thumbnail>http://msnsearch.srv.girafa.com/srv/i?s=MSNSEARCH&amp;r=$url</thumbnail>\n";
		}
		
		function toInt ($string) {
			return preg_replace('#[^0-9]#si', '', $string);
		}
		
		function to_int_32 (&$x) {
			$z = hexdec(80000000);
			$y = (int) $x;
			if($y ==- $z && $x <- $z){
				$y = (int) ((-1) * $x);
				$y = (-1) * $y;
			}
			$x = $y;
		}
		
		function zero_fill ($a, $b) {
			$z = hexdec(80000000);
			if ($z & $a) {
				$a = ($a >> 1);
				$a &= (~$z);
				$a |= 0x40000000;
				$a = ($a >> ($b - 1));
			} else {
				$a = ($a >> $b);
			}
			return $a;
		}
		
		function mix ($a, $b, $c) {
			$a -= $b; $a -= $c; $this->to_int_32($a); $a = (int)($a ^ ($this->zero_fill($c, 13)));
			$b -= $c; $b -= $a; $this->to_int_32($b); $b = (int)($b ^ ($a << 8));
			$c -= $a; $c -= $b; $this->to_int_32($c); $c = (int)($c ^ ($this->zero_fill($b, 13)));
			$a -= $b; $a -= $c; $this->to_int_32($a); $a = (int)($a ^ ($this->zero_fill($c, 12)));
			$b -= $c; $b -= $a; $this->to_int_32($b); $b = (int)($b ^ ($a << 16));
			$c -= $a; $c -= $b; $this->to_int_32($c); $c = (int)($c ^ ($this->zero_fill($b, 5)));
			$a -= $b; $a -= $c; $this->to_int_32($a); $a = (int)($a ^ ($this->zero_fill($c, 3)));
			$b -= $c; $b -= $a; $this->to_int_32($b); $b = (int)($b ^ ($a << 10));
			$c -= $a; $c -= $b; $this->to_int_32($c); $c = (int)($c ^ ($this->zero_fill($b, 15)));
			return array($a,$b,$c);
		}
		
		function checksum ($url, $length = null, $init = 0xE6359A60) {
			if (is_null($length)) {
				$length = sizeof($url);
			}
			$a = $b = 0x9E3779B9;
			$c = $init;
			$k = 0;
			$len = $length;
			while($len >= 12) {
				$a += ($url[$k + 0] + ($url[$k + 1] << 8) + ($url[$k + 2] << 16) + ($url[$k +3] << 24));
				$b += ($url[$k + 4] + ($url[$k + 5] << 8) + ($url[$k + 6] << 16) + ($url[$k +7] << 24));
				$c += ($url[$k + 8] + ($url[$k + 9] << 8) + ($url[$k + 10] << 16) + ($url[$k +11] << 24));
				$mix = $this->mix($a, $b, $c);
				$a = $mix[0]; $b = $mix[1]; $c = $mix[2];
				$k += 12;
				$len -= 12;
			}
			$c += $length;
			switch($len) {
				case 11: $c += ($url[$k + 10] << 24);
				case 10: $c += ($url[$k + 9] << 16);
				case 9 : $c += ($url[$k + 8] << 8);
				case 8 : $b += ($url[$k + 7] << 24);
				case 7 : $b += ($url[$k + 6] << 16);
				case 6 : $b += ($url[$k + 5] << 8);
				case 5 : $b += ($url[$k + 4]);
				case 4 : $a += ($url[$k + 3] << 24);
				case 3 : $a += ($url[$k + 2] << 16);
				case 2 : $a += ($url[$k + 1] << 8);
				case 1 : $a += ($url[$k + 0]);
			}
			$mix = $this->mix($a, $b, $c);
			return $mix[2];
		}
		
		function strord ($string) {
			for($i = 0; $i < strlen($string); $i++) {
				$result[$i] = ord($string{$i});
			}
			return $result;
		}
		
	}
?>