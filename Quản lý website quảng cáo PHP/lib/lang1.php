<?
define('_FIRST_PAGE'      ,'TRANG THỨ NHẤT');
define('_HOME'            ,'TRANG CHỦ');
define('_INTRO'           ,'GIỚI THIỆU');
define('_PRODUCT'         ,'SẢN PHẨM');
define('_RELAX'           ,'THƯ GIÃN');
define('_ODER'            ,'TUYỂN DỤNG');
define('_NEWS'            ,'TIN TỨC');
define('_CONTACT'         ,'LIÊN HỆ');
define('_SEEK'            ,'TÌM NHANH');
define('_SUPPORTONLINE'   ,'HỖ TRỢ ONLINE');
define('_LINK'            ,'LIÊN KẾT WEB');
define('_TOTALACCESS'     ,'THỐNG KÊ');
define('_LANGUAGE'        ,'Ngôn ngữ');
define('_TOTALS'          ,'Tổng tiền');
define('_QUANTITY'        ,'Số lượng');
define('_PRICE'           ,'Giá');
define('_GO'              ,'Tiếp tục mua');
define('_MEMBER'          ,'Thành Viên');
define('_SEARCH'          ,'Tìm nhanh...');
define('_TOTAL'           ,'Thống Kê ');
define('_ADVERTISEMENT'   ,'QUẢNG CÁO');
define('_ORTHERINFO'      ,'THÔNG TIN KHÁC');

define('_UID'             ,'Tên đăng nhập');
define('_PWD'             ,'Mật khẩu');
define('_REGISTRY'        ,'Đăng ký');
define('_LOGIN'           ,'Đăng nhập');
define('_LOGOUT'          ,'Đăng Xuất');
define('_FORGOTPASS'      ,'Quên mật khẩu');
define('_MEMBER'          ,'Thành viên');
define('_GUEST'           ,'Khách'); 
define('_VN'			  ,'Việt Nam');
define('_EN'			  ,'English');
?>
