<?
$l_product   = $_lang == 'vn' ? 'Sản phẩm' : 'Product';
$l_quantity  = $_lang == 'vn' ? 'Số lượng' : 'Quantity';
$l_price     = $_lang == 'vn' ? 'Đơn giá' : 'Unit price';
$l_money     = $_lang == 'vn' ? 'Thành tiền' : 'Cost';
$l_total     = $_lang == 'vn' ? 'Tổng cộng' : 'Total';

$l_btnDel    = $_lang == 'vn' ? 'Xóa' : 'Delete';
$l_btnDelAll = $_lang == 'vn' ? 'Xóa hết' : 'Delete all';
$l_btnUpdate = $_lang == 'vn' ? 'Cập nhật' : 'Update';
$l_btnPay    = $_lang == 'vn' ? 'Thanh toán' : 'Payment';

$l_cartEmpty = $_lang == 'vn' ? 'Bạn chưa chọn bất kỳ sản phẩm nào.' : 'Your cart is empty.';

function checkexist(){
	$cart=$_SESSION['cart'];
	foreach ($cart as $product)
		if ($product[0]==$_REQUEST['p']) return true;
	return false;
}

if ($_REQUEST['act']=='del'){
	if (count($_SESSION['cart'])==1){
		unset($_SESSION['cart']);
	}else{
		$cart=$_SESSION['cart'];
		unset($cart[$_REQUEST['pos']]);
		$_SESSION['cart']=$cart;
	}
}

if (isset($_POST['butUpdate'])||isset($_POST['btnCheckout'])){
	$cart=$_SESSION['cart'];
	$t=0;
	foreach ($_POST['txtQuantity'] as $quantity){
		if (is_numeric($quantity) && $quantity>0 && strlen($quantity)<5)
		$m=$m+ $cart[$t][1]=(int)$quantity;
		if ($quantity<=0){
			unset($cart[$t]);
			$t=$t-1;
		}
		$t=$t+1;
		
	}
	if (count($cart)<=0) unset($cart);
	$_SESSION['cart']=$cart;
	
	if (isset($_POST['btnCheckout'])) echo "<script>window.location='./?frame=checkout'</script>";
}
if (isset($_POST['btnDeleteAll'])) unset($_SESSION['cart']);

if (isset($_REQUEST['p'])){
	if (!isset($_SESSION['cart'])){
		$pro=$_REQUEST['p'];
		$cart=array();
		$cart[] = array($pro,1);
		$_SESSION['cart']=$cart;
	}else{
		$pro=$_REQUEST['p'];
		$cart=$_SESSION['cart'];
		if (countRecord("tbl_product","id='".$_REQUEST['p']."'")>0 && checkexist()==false){
			$cart[]=array($pro,1);
			$_SESSION['cart']=$cart;
		}
	}
}else{
	$cart=$_SESSION['cart'];
}
?>


<? if (!isset($_SESSION['cart'])){?>
<table align="center" border="0" width="98%" cellpadding="0" cellspacing="0">
	
	<tr>
		<td>
			<table align="center" border="1" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td align="center" class=td1>
						<br>
						<font size="2" face="Verdana, Arial, Helvetica, sans-serif">
							<b><?=$l_cartEmpty?></b>
						</font>
						<br><br><br>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td height="5"></td></tr>
</table>
<? }else{?>


<FORM action="./" method="POST" name="frmCart">
<input type="hidden" name="frame" value="cart"> 
<table border="1" width="100%" cellspacing="0" cellpadding="4" bordercolor="#CCCCCC">
	<tr>
		<th width="100" class=td1>Hình</th>
		<th class=td1><?=$l_product?></th>
		<th width="70" class=td1><?=$l_quantity?></th>
		<th  width="70" class=td1><?=$l_price?><br>(<font color="#CC0000"><?=$currencyUnit?></font>)</th>
		<th  width="70" class=td1><?=$l_money?></th>
		<th width="60" class=td1>Chức năng</th>
	</tr>
<?
$cnt=0;
$tongcong=0;
foreach ($cart as $product){
	$sql = "select * from tbl_product where id='".$product[0]."'";
	$result = mysql_query($sql,$conn);
	$t=0;
	if ($r=mysql_num_rows($result)>0){
	$h=$r+$h;
	$pro = mysql_fetch_assoc($result)?>
	<tr>
		<td class="smallfont" align="center">
			<a href="./?frame=product_detail&id=<?=$pro['id']?>">
				<img id="" src="<?=$pro['image']?>" alt="<?=$pro['name']?>" border="0" width="100">
			</a>
		</td>
		<td class=td1><?=$pro['name']?></td>
		<td class=td1 align="center">
			<input type="text" name="txtQuantity[]" size="5" value="<?=$product[1]?>">
		</td>
		<td class=td1 align="center"><?=number_format($pro['price'],0)?></td>
		<td class=td1 align="center"><?=number_format(($pro['price']*$product[1]),0)?></td>
		<td class="smallfont" align="center">
        	<input type="button" class="buttonorange" style="width:50" name="btnDelete" value="<?=$l_btnDel?>" onclick="window.location='./?frame=cart&act=del&pos=<?=$cnt?>';return false;">
	  </td>
	</tr>
<?
}

$_SESSION['tong']=$tongcong=$tongcong+$pro['price']*$product[1];
$cnt=$cnt+1;
}
/*if (isset($_POST['butUpdate']))
{
$_SESSION['soluong']=$m; 
}
else
{*/
$_SESSION['soluong']=$h; 

?>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr><td height="10" colspan="2"></td></tr>
	<tr>
		<td class=td1 align="right" colspan="2">
			<b><?=$l_total?> : <font color="#CC0000"><?=number_format($tongcong,0)?></font> <?=$currencyUnit?></b>
		</td>
	</tr>
	<tr><td height="10" colspan="2"></td></tr>
	<tr>
		<td>
			<input type="submit" class="buttonorange" name="butUpdate" value="<?=$l_btnUpdate?>">
			<input type="submit" class="buttonorange" name="btnDeleteAll" value="<?=$l_btnDelAll?>">
		</td>
		<td align="center">
		<input type="button"  class="buttonorange" <? if($_REQUEST["back1"]==1) echo 'onclick="javascript:history.go(-2)"'; else  echo 'onclick="javascript:history.go(-1)"'; ?>value="<?=_GO?>" />
			<input type="submit" class="buttonorange" name="btnCheckout" value="<?=$l_btnPay?>">
		</td>
	</tr>
</table>

</FORM>
<?
}
?>

