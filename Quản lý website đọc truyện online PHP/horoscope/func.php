    <?php
       // Khai báo trình duyệt nhận lấy nội dung
       function userAgent() {
       $userAgent = 'Opera/9.80 (J2ME/MIDP; Opera Mini/4.3.24214/25.685; U; vi) Presto/2.5.25 Version/10.54';
                            }
      // Khởi Tạo CURL
       function grab_link($url,$cookie='',$user_agent='',$header='') {
	if(function_exists('curl_init')){
	$ch = curl_init();
	$headers[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8';
	$headers[] = 'Accept-Language: en-us,en;q=0.5';
	$headers[] = 'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7';
	$headers[] = 'Keep-Alive: 300';
	$headers[] = 'Connection: Keep-Alive';
	$headers[] = 'Content-type: application/x-www-form-urlencoded;charset=UTF-8';
	curl_setopt($ch, CURLOPT_URL, $url);
	if($user_agent)	curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
	else curl_setopt($ch, CURLOPT_USERAGENT, userAgent());
	if($header)
	curl_setopt($ch, CURLOPT_HEADER, 1);
	else
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com.vn/search?hl=vi&client=firefox-a&rls=org.mozilla:en-US:official&hs=hKS&q=video+clip&start=20&sa=N');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	if(strncmp($url, 'https', 6)) curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	if($cookie)	curl_setopt($ch, CURLOPT_COOKIE, $cookie);
	curl_setopt($ch, CURLOPT_TIMEOUT, 100);
	$html = curl_exec($ch);
	$mess_error = curl_error($ch);
	curl_close($ch);
	}
	else {
	$matches = parse_url($url);
	$host = $matches['host'];
	$link = (isset($matches['path'])?$matches['path']:'/').(isset($matches['query'])?'?'.$matches['query']:'').(isset($matches['fragment'])?'#'.$matches['fragment']:'');
	$port = !empty($matches['port']) ? $matches['port'] : 80;
	$fp=@fsockopen($host,$port,$errno,$errval,30);
	if (!$fp) {
		$html = "$errval ($errno)<br />\n";
	} else {
		$rand_ip = rand(1,254).".".rand(1,254).".".rand(1,254).".".rand(1,254);
		$out  = "GET $link HTTP/1.1\r\n".
				"Host: $host\r\n".
				"Referer: http://www.google.com.vn/search?hl=vi&client=firefox-a&rls=org.mozilla:en-US:official&hs=hKS&q=video+clip&start=20&sa=N\r\n".
				"Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5\r\n";
		if($cookie) $out .= "Cookie: $cookie\r\n";
		if($user_agent) $out .= "User-Agent: ".$user_agent."\r\n";
		else $out .= "User-Agent: ".userAgent()."\r\n";
		$out .= "X-Forwarded-For: $rand_ip\r\n".
				"Via: CB-Prx\r\n".
				"Connection: Close\r\n\r\n";
		fwrite($fp,$out);
		while (!feof($fp)) {
			$html .= fgets($fp,4096);
		}
		fclose($fp);
		}
	}
       return $html;
      }
      // Lấy nội dung
        function laynoidung($noidung, $start, $stop) {
			$bd = strpos($noidung, $start);
			$kt = strpos(substr($noidung, $bd), $stop) + $bd;
			$content = substr($noidung, $bd, $kt - $bd);
			return $content;
			                              }
    // Thay kí tự
    function trangkute($trang9x) {
    $trang9x = str_replace('Đ', '&#272;', $trang9x);
    $trang9x = str_replace('đ', '&#273;', $trang9x);
    $trang9x = str_replace('Á', '&#193;', $trang9x);
    $trang9x = str_replace('á', '&#225;', $trang9x);
    $trang9x = str_replace('À', '&#192;', $trang9x);
    $trang9x = str_replace('à', '&#224;', $trang9x);
    $trang9x = str_replace('Ã', '&#195;', $trang9x);
    $trang9x = str_replace('ã', '&#227;', $trang9x);
    $trang9x = str_replace('Ả', '&#7842;', $trang9x);
    $trang9x = str_replace('ả', '&#7843;', $trang9x);
    $trang9x = str_replace('Ạ', '&#7840;', $trang9x);
    $trang9x = str_replace('ạ', '&#7841;', $trang9x);
    $trang9x = str_replace('Â', '&#194;', $trang9x);
    $trang9x = str_replace('â', '&#226;', $trang9x);
    $trang9x = str_replace('Ấ', '&#7844;', $trang9x);
    $trang9x = str_replace('ấ', '&#7845;', $trang9x);
    $trang9x = str_replace('Ầ', '&#7846;', $trang9x);
    $trang9x = str_replace('ầ', '&#7847;', $trang9x);
    $trang9x = str_replace('Ẫ', '&#7850;', $trang9x);
    $trang9x = str_replace('ẫ', '&#7851;', $trang9x);
    $trang9x = str_replace('Ẩ', '&#7848;', $trang9x);
    $trang9x = str_replace('ẩ', '&#7849;', $trang9x);
    $trang9x = str_replace('Ậ', '&#7852;', $trang9x);
    $trang9x = str_replace('ậ', '&#7853;', $trang9x);
    $trang9x = str_replace('Ă', '&#258;', $trang9x);
    $trang9x = str_replace('ă', '&#259;', $trang9x);
    $trang9x = str_replace('Ắ', '&#7854;', $trang9x);
    $trang9x = str_replace('ắ', '&#7855;', $trang9x);
    $trang9x = str_replace('Ằ', '&#7856;', $trang9x);
    $trang9x = str_replace('ằ', '&#7857;', $trang9x);
    $trang9x = str_replace('Ẵ', '&#7860;', $trang9x);
    $trang9x = str_replace('ẵ', '&#7861;', $trang9x);
    $trang9x = str_replace('Ẳ', '&#7858;', $trang9x);
    $trang9x = str_replace('ẳ', '&#7859;', $trang9x);
    $trang9x = str_replace('Ặ', '&#7862;', $trang9x);
    $trang9x = str_replace('ặ', '&#7863;', $trang9x);
    $trang9x = str_replace('É', '&#201;', $trang9x);
    $trang9x = str_replace('é', '&#223;', $trang9x);
    $trang9x = str_replace('È', '&#200;', $trang9x);
    $trang9x = str_replace('è', '&#232;', $trang9x);
    $trang9x = str_replace('Ẽ', '&#7868;', $trang9x);
    $trang9x = str_replace('ẽ', '&#7869;', $trang9x);
    $trang9x = str_replace('Ẻ', '&#7866;', $trang9x);
    $trang9x = str_replace('ẻ', '&#7867;', $trang9x);
    $trang9x = str_replace('Ẹ', '&#7864;', $trang9x);
    $trang9x = str_replace('ẹ', '&#7865;', $trang9x);
    $trang9x = str_replace('Ê', '&#202;', $trang9x);
    $trang9x = str_replace('ê', '&#234;', $trang9x);
    $trang9x = str_replace('Ế', '&#7870;', $trang9x);
    $trang9x = str_replace('ế', '&#7871;', $trang9x);
    $trang9x = str_replace('Ề', '&#7872;', $trang9x);
    $trang9x = str_replace('ề', '&#7873;', $trang9x);
    $trang9x = str_replace('Ễ', '&#7876;', $trang9x);
    $trang9x = str_replace('ễ', '&#7877;', $trang9x);
    $trang9x = str_replace('Ể', '&#7874;', $trang9x);
    $trang9x = str_replace('ể', '&#7875;', $trang9x);
    $trang9x = str_replace('Ệ', '&#7878;', $trang9x);
    $trang9x = str_replace('ệ', '&#7879;', $trang9x);
    $trang9x = str_replace('Í', '&#205;', $trang9x);
    $trang9x = str_replace('í', '&#237;', $trang9x);
    $trang9x = str_replace('Ì', '&#204;', $trang9x);
    $trang9x = str_replace('ì', '&#236;', $trang9x);
    $trang9x = str_replace('Ĩ', '&#296;', $trang9x);
    $trang9x = str_replace('ĩ', '&#297;', $trang9x);
    $trang9x = str_replace('Ỉ', '&#7880;', $trang9x);
    $trang9x = str_replace('ỉ', '&#7881;', $trang9x);
    $trang9x = str_replace('Ị', '&#7882;', $trang9x);
    $trang9x = str_replace('ị', '&#7883;', $trang9x);
    $trang9x = str_replace('Ó', '&#221;', $trang9x);
    $trang9x = str_replace('ó', '&#243;', $trang9x);
    $trang9x = str_replace('Ò', '&#210;', $trang9x);
    $trang9x = str_replace('ò', '&#242;', $trang9x);
    $trang9x = str_replace('Õ', '&#213;', $trang9x);
    $trang9x = str_replace('õ', '&#245;', $trang9x);
    $trang9x = str_replace('Ỏ', '&#7886;', $trang9x);
    $trang9x = str_replace('ỏ', '&#7887;', $trang9x);
    $trang9x = str_replace('Ọ', '&#7884;', $trang9x);
    $trang9x = str_replace('ọ', '&#7885;', $trang9x);
    $trang9x = str_replace('Ô', '&#212;', $trang9x);
    $trang9x = str_replace('ô', '&#244;', $trang9x);
    $trang9x = str_replace('Ố', '&#7888;', $trang9x);
    $trang9x = str_replace('ố', '&#7889;', $trang9x);
    $trang9x = str_replace('Ồ', '&#7890;', $trang9x);
    $trang9x = str_replace('ồ', '&#7891;', $trang9x);
    $trang9x = str_replace('Ỗ', '&#7894;', $trang9x);
    $trang9x = str_replace('ỗ', '&#7895;', $trang9x);
    $trang9x = str_replace('Ổ', '&#7892;', $trang9x);
    $trang9x = str_replace('ổ', '&#7893;', $trang9x);
    $trang9x = str_replace('Ộ', '&#7896;', $trang9x);
    $trang9x = str_replace('ộ', '&#7897;', $trang9x);
    $trang9x = str_replace('Ơ', '&#416;', $trang9x);
    $trang9x = str_replace('ơ', '&#417;', $trang9x);
    $trang9x = str_replace('Ớ', '&#7898;', $trang9x);
    $trang9x = str_replace('ớ', '&#7899;', $trang9x);
    $trang9x = str_replace('Ờ', '&#7900;', $trang9x);
    $trang9x = str_replace('ờ', '&#7901;', $trang9x);
    $trang9x = str_replace('Ỡ', '&#7904;', $trang9x);
    $trang9x = str_replace('ỡ', '&#7905;', $trang9x);
    $trang9x = str_replace('Ở', '&#7902;', $trang9x);
    $trang9x = str_replace('ở', '&#7903;', $trang9x);
    $trang9x = str_replace('Ợ', '&#7906;', $trang9x);
    $trang9x = str_replace('ợ', '&#7907;', $trang9x);
    $trang9x = str_replace('Ú', '&#218;', $trang9x);
    $trang9x = str_replace('ú', '&#250;', $trang9x);
    $trang9x = str_replace('Ù', '&#217;', $trang9x);
    $trang9x = str_replace('ù', '&#249;', $trang9x);
    $trang9x = str_replace('Ũ', '&#360;', $trang9x);
    $trang9x = str_replace('ũ', '&#361;', $trang9x);
    $trang9x = str_replace('Ủ', '&#7910;', $trang9x);
    $trang9x = str_replace('ủ', '&#7911;', $trang9x);
    $trang9x = str_replace('Ụ', '&#7908;', $trang9x);
    $trang9x = str_replace('ụ', '&#7909;', $trang9x);
    $trang9x = str_replace('Ư', '&#431;', $trang9x);
    $trang9x = str_replace('ư', '&#432;', $trang9x);
    $trang9x = str_replace('Ứ', '&#7912;', $trang9x);
    $trang9x = str_replace('ứ', '&#7913;', $trang9x);
    $trang9x = str_replace('Ừ', '&#7914;', $trang9x);
    $trang9x = str_replace('ừ', '&#7915;', $trang9x);
    $trang9x = str_replace('Ữ', '&#7918;', $trang9x);
    $trang9x = str_replace('ữ', '&#7919;', $trang9x);
    $trang9x = str_replace('Ử', '&#7916;', $trang9x);
    $trang9x = str_replace('ử', '&#7917;', $trang9x);
    $trang9x = str_replace('Ự', '&#7920;', $trang9x);
    $trang9x = str_replace('ự', '&#7921;', $trang9x);
    $trang9x = str_replace('Ý', '&#221;', $trang9x);
    $trang9x = str_replace('ý', '&#253;', $trang9x);
    $trang9x = str_replace('Ỳ', '&#7922;', $trang9x);
    $trang9x = str_replace('ỳ', '&#7923;', $trang9x);
    $trang9x = str_replace('Ỹ', '&#7928;', $trang9x);
    $trang9x = str_replace('ỹ', '&#7929;', $trang9x);
    $trang9x = str_replace('Ỷ', '&#7926;', $trang9x);
    $trang9x = str_replace('ỷ', '&#7927;', $trang9x);
    $trang9x = str_replace('Ỵ', '&#7924;', $trang9x);
    $trang9x = str_replace('ỵ', '&#7925;', $trang9x);
    return $trang9x;
}
?>