<?php
/**
* @package     JohnCMS
* @link        http://johncms.com
* @copyright   Copyright (C) 2008-2011 JohnCMS Community
* @license     LICENSE.txt (see attached file)
* @version     VERSION.txt (see attached file)
* @author      http://johncms.com/about
*/

defined('_IN_JOHNCMS') or die('Error: restricted access');
$textl = $lng_profile['my_office'];
require('../incfiles/head.php');

/*
-----------------------------------------------------------------
Проверяем права доступа
-----------------------------------------------------------------
*/
if ($user['id'] != $user_id) {
    echo functions::display_error($lng['access_forbidden']);
    require('../incfiles/end.php');
    exit;
}

/*
-----------------------------------------------------------------
Личный кабинет пользователя
-----------------------------------------------------------------
*/
$total_photo = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_album_files` where `website` = '$website' AND  `user_id` = '$user_id'"), 0);
echo '<div class="phdr">' .
    '<img src="http://img.infoviet.net/images/photo.gif" width="16" height="16"/>&#160;<a href="album.php?act=list">Ảnh</a>&#160;' .
    '<img src="http://img.infoviet.net/images/contacts.png" width="16" height="16"/>&#160;<a href="profile.php">' . $lng_profile['my_profile'] . '</a>' .
    '<img src="../images/rate.gif" width="16" height="16"/>&#160;<a href="profile.php?act=stat">' . $lng['statistics'] . '</a></div>';
    
    
//echo '<div><img src="../images/pt.gif" width="16" height="16"/>&#160;<a href="">' . $lng['blog'] . '</a>&#160;(0)</div>';
if ($rights >= 1) {
    $guest = counters::guestbook(2);
    echo '</p><p>' .
        '<div><img src="http://img.infoviet.net/images/admin.png" width="16" height="16"/>&#160;<a href="../guestbook/index.php?act=ga&amp;do=set">' . $lng['admin_club'] . '</a> (<span class="red">' . $guest . '</span>)</div>';
}
$arg = array (
    'comments_table' => 'cms_users_guestbook', // Таблица Гостевой
    'object_table' => 'users',                 // Таблица комментируемых объектов
    'script' => 'profile.php?act=guestbook',   // Имя скрипта (с параметрами вызова)
    'sub_id_name' => 'user',                   // Имя идентификатора комментируемого объекта
    'sub_id' => $user['id'],                   // Идентификатор комментируемого объекта
    'owner' => $user['id'],                    // Владелец объекта
    'owner_delete' => true,                    // Возможность владельцу удалять комментарий
    'owner_reply' => true,                     // Возможность владельцу отвечать на комментарий
    'title' => $lng['comments'],               // Название раздела
    'context_top' => $context_top              // Выводится вверху списка
);
$comm = new comments($arg);




// Блок почты
$count_mail = mysql_result(mysql_query("SELECT COUNT(*) FROM `privat` where `website` = '$website' AND  `user` = '$login' AND `type` = 'in'"), 0);
$count_newmail = mysql_result(mysql_query("SELECT COUNT(*) FROM `privat` where `website` = '$website' AND  `user` = '" . $login . "' AND `type` = 'in' AND `chit` = 'no'"), 0);
$count_sentmail = mysql_result(mysql_query("SELECT COUNT(*) FROM `privat` where `website` = '$website' AND  `author` = '$login' AND `type` = 'out'"), 0);
$count_sentunread = mysql_result(mysql_query("SELECT COUNT(*) FROM `privat` where `website` = '$website' AND  `author` = '$login' AND `type` = 'out' AND `chit` = 'no'"), 0);
$count_files = mysql_result(mysql_query("SELECT COUNT(*) FROM `privat` where `website` = '$website' AND  `user` = '$login' AND `type` = 'in' AND `attach` != ''"), 0);
echo '<h3><img src="http://img.infoviet.net/images/mail.png" width="16" height="16" class="left" />&#160;' . $lng_profile['my_mail'] . '</h3><ul>' .
    '<li><a href="pradd.php?act=in">' . $lng_profile['received'] . '</a>&#160;(' . $count_mail . ($count_newmail ? '&#160;/&#160;<span class="red"><a href="pradd.php?act=in&amp;new">+' . $count_newmail . '</a></span>' : '') . ')</li>' .
    '<li><a href="pradd.php?act=out">' . $lng_profile['sent'] . '</a>&#160;(' . $count_sentmail . ($count_sentunread ? '&#160;/&#160;<span class="red">' . $count_sentunread . '</span>' : '') . ')</li>';
if (!isset($ban['1']) && !isset($ban['3']))
    echo '<p><form action="pradd.php?act=write" method="post"><input type="submit" value=" ' . $lng['write'] . ' " /></form></p>';
// Блок контактов
$count_contacts = mysql_result(mysql_query("SELECT COUNT(*) FROM `privat` where `website` = '$website' AND  `me` = '$login' AND `cont` != ''"), 0);
$count_ignor = mysql_result(mysql_query("SELECT COUNT(*) FROM `privat` where `website` = '$website' AND  `me` = '$login' AND `ignor` != ''"), 0);
echo '</ul><h3><img src="http://img.infoviet.net/images/users.png" width="16" height="16" class="left" />&#160;' . $lng['contacts'] . '</h3><ul>' .
    '<li><a href="cont.php">' . $lng['contacts'] . '</a>&#160;(' . $count_contacts . ')</li>' .
    '<li><a href="ignor.php">' . $lng['blocking'] . '</a>&#160;(' . $count_ignor . ')</li>' .
    '</ul></p></div>';
// Блок настроек
echo '<div class="bmenu"><p><h3><img src="http://img.infoviet.net/images/settings.png" width="16" height="16" class="left" />&#160;' . $lng_profile['my_settings'] . '</h3><ul>' .
    '<li><a href="profile.php?act=settings">' . $lng['system_settings'] . '</a></li>' .
    '<li><a href="profile.php?act=edit">' . $lng_profile['profile_edit'] . '</a></li>' .
    '<li><a href="profile.php?act=password">' . $lng['change_password'] . '</a></li>' .
    '<li><a href="../mod/chuki.php">Cài đặt chữ kí</a></li>' .
'<li><a href="perevod.php">Chuyển tiền</a></li>' .
'<li><a href="/mod/tamtrang.php">Thay đổi tâm trạng</a></li>' .
'<li><a href="/mod/doiten.php">Đổi tên nick</a>';
if ($rights >= 1)
    echo '<li><span class="red"><a href="../' . $set['admp'] . '/index.php"><b>' . $lng['admin_panel'] . '</b></a></span></li>';
echo '</ul></p></div>';
?>