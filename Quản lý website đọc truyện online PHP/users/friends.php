<?php

/*  
Модуль Друзья для JohnCMS 4.4.0
* @link        http://fisabilillahi.com
* @author      BraTka_001 :)
*/
define('_IN_JOHNCMS', 1);

require_once('../incfiles/core.php');
 if(!$website)
require(''.$_SERVER['DOCUMENT_ROOT'].'/incfiles/websiteb.php');
/*
-------------------------------------
Закрываем от неавторизованных юзеров
-------------------------------------
*/
if (!$user_id) {
    require('../incfiles/head.php');
    echo functions::display_error('Chỉ cho thành viên đã đăng kí');
    require('../incfiles/end.php');
    exit;
}
/*
-------------------------------------
Получаем данные пользователя
-------------------------------------
*/
$user = functions::get_user($user);
if (!$user) {
    require('../incfiles/head.php');
    echo functions::display_error('Chỉ cho thành viên đã đăng kí');
    require('../incfiles/end.php');
    exit;
}
$headmod = 'friends/' . $user['id'];
$array = array (
    'add' => 'includes/friends',
    'new' => 'includes/friends',
    'del' => 'includes/friends'
);
$path = !empty($array[$act]) ? $array[$act] . '/' : '';
if (array_key_exists($act, $array) && file_exists($path . $act . '.php')) {
    require_once($path . $act . '.php');
} else {
    $textl = 'Bạn Bè ' . htmlspecialchars($user['name']);
    require('../incfiles/head.php');

$total_index = mysql_result(mysql_query("SELECT COUNT(*) FROM `friends` WHERE `website`='$website' and `user_id` = '" . $user['id'] . "' AND `friends`.`option`='3' AND `friends`.`friends`='1'"), 0);
$total_online = mysql_result(mysql_query("SELECT COUNT(*) FROM `friends` LEFT JOIN `users` ON `friends`.`friend_id`=`users`.`id` WHERE `friends`.`website`='$website' and `friends`.`user_id`='" . $user['id'] . "' AND `friends`.`option`='3' AND `friends`.`friends`='1' AND `users`.`lastdate` > " . (time() - 300) . ""), 0);
$total_family = mysql_result(mysql_query("SELECT COUNT(*) FROM `friends` WHERE `friends`.`website`='$website' and `user_id` = '" . $user['id'] . "' AND `friends`.`option`='3' AND `friends`.`friends`='1' AND `you_me`IN (1, 2, 3, 4);"), 0);
$total_best_friend = mysql_result(mysql_query("SELECT COUNT(*) FROM `friends` WHERE `friends`.`website`='$website' and `user_id` = '" . $user['id'] . "' AND `friends`.`option`='3' AND `friends`.`friends`='1' AND `you_me`='5'"), 0);
$total_classmates = mysql_result(mysql_query("SELECT COUNT(*) FROM `friends` WHERE `friends`.`website`='$website' and `user_id` = '" . $user['id'] . "' AND `friends`.`option`='3' AND `friends`.`friends`='1' AND `you_me`='6'"), 0);
$total_bandmates = mysql_result(mysql_query("SELECT COUNT(*) FROM `friends` WHERE `friends`.`website`='$website' and `user_id` = '" . $user['id'] . "' AND `friends`.`option`='3' AND `friends`.`friends`='1' AND `you_me`='7'"), 0);
$total_colleagues = mysql_result(mysql_query("SELECT COUNT(*) FROM `friends` WHERE `friends`.`website`='$website' and `user_id` = '" . $user['id'] . "' AND `friends`.`option`='3' AND `friends`.`friends`='1' AND `you_me`='8'"), 0);
switch ($mod) {
    case 'online': // Друзья на сайте
        $sql = "AND `users`.`lastdate` > " . (time() - 300) . "";
		$total = $total_online;
		$link = 'mod=online';
        break;
    case 'family': // семья
        $sql = "AND `friends`.`you_me`IN(1, 2, 3, 4)";
		$total = $total_family;
		$link = 'mod=family';
        break;
	case 'best_friend': // Лучшие
        $sql = "AND `friends`.`you_me` = '5'";
		$total = $total_best_friend;
		$link = 'mod=best_friend';
        break;
	case 'classmates': // Одноклассники
        $sql = "AND `friends`.`you_me` = '6'";
		$total = $total_classmates;
		$link = 'mod=classmates';
        break;
    case 'bandmates': //Однокурсники
        $sql = "AND `friends`.`you_me` = '7'";
		$total = $total_bandmates;
		$link = 'mod=bandmates';
        break;
    case 'colleagues': //Коллеги
        $sql = "AND `friends`.`you_me` = '8'";
		$total = $total_colleagues;
		$link = 'mod=colleagues';
        break;
	default: // Все
        $sql = "";
		$total = $total_index;
		$link = '';
}
$list = array();
if ($total_index)
$list[] = ''.(!$mod ? '<b>Tất cả</b>' : '<a href="friends.php?user=' . $user['id'] . '">Tất cả</a>').'&nbsp;' . $total_index . '';
if ($total_online)
$list[] = ''.($mod == 'online'? '<b>Online</b>' : '<a href="friends.php?mod=online&amp;user=' . $user['id'] . '">Online</a>').'&nbsp;' . $total_online . '';
if ($total_family)
$list[] = ''.($mod == 'family' ? '<b>Gia đình</b>' : '<a href="friends.php?mod=family&amp;user=' . $user['id'] . '">Gia đình</a>').'&nbsp;' . $total_family . '';
if ($total_best_friend)
$list[] = ''.($mod == 'best_friend' ? '<b>Bạn thân</b>' : '<a href="friends.php?mod=best_friend&amp;user=' . $user['id'] . '">Bạn thân</a>').'&nbsp;' . $total_best_friend . '';
if ($total_classmates)
$list[] = ''.($mod == 'classmates' ? '<b>Bạn cùng lớp</b>' : '<a href="friends.php?mod=classmates&amp;user=' . $user['id'] . '">Bạn cùng lớp</a>').'&nbsp;' . $total_classmates . '';
if ($total_bandmates)
$list[] = ''.($mod == 'bandmates' ? '<b>Bạn cùng lớp</b>' : '<a href="friends.php?mod=bandmates&amp;user=' . $user['id'] . '">Bạn cùng lớp</a>') .'&nbsp;' . $total_bandmates . '';
if ($total_colleagues)
$list[] = ''.($mod == 'colleagues' ? '<b>Đồng hương</b>' : '<a href="friends.php?mod=colleagues&amp;user=' . $user['id'] . '">Đồng hương</a>') . '&nbsp;' . $total_colleagues . '';


	echo '<div class="phdr">' . ($user['id'] != $user_id ? 'Bạn bè bạn' : 'Bạn của tôi').'</div>';
	if ($total) {
	if (!empty($list)) echo'<div class="topmenu">' . functions::display_menu($list, ' &#8226; ') . '</div>';
	$req = mysql_query("SELECT `friends`.*, `users`.`name`, `users`.`rights`, `users`.`lastdate`, `users`.`status`, `users`.`id`, `users`.`sex`, `users`.`datereg` FROM `friends` LEFT JOIN `users` ON `friends`.`friend_id` = `users`.`id` WHERE `friends`.`website`='$website' and `friends`.`user_id`='".$user['id']."' $sql AND `friends`.`friends`='1' ORDER BY `friends`.`date` DESC LIMIT $start, $kmess");  
	while ($res = mysql_fetch_assoc($req)) {
	echo ($i % 2) ? '<div class="list2">' : '<div class="list1">';
	$level = array(
		1 => ''.($res['sex'] == 'm' ? 'Em trai' : 'Chị gái').'',
		2 => ''.($res['sex'] == 'm' ? 'Bác' : 'Cô').'',
		3 => ''.($res['sex'] == 'm' ? 'Cháu trai' : 'Cháu gái').'',
   		4 => ''.($res['sex'] == 'm' ? 'Bạn trai' : 'Bạn gái').''
    );
	$arg = array(
	'header' => '' . ($mod == 'family' ? '(' . $level[$res['you_me']] . ')' : '') . '',
	'iphide' => 1
	);
	echo '' . functions::display_user($res, $arg);
	echo'</div>';
	++$i;
	}
	} else {
	echo '<div class="menu">Danh sách trống!</div>';
	}
	echo '<div class="phdr">' . ($user['id'] != $user_id ? '<a href="../users/profile.php?user=' . $user['id'] .'">Hố sơ của tôi</a>' : '<a href="../users/profile.php?user='.$user_id.'">Hố sơ</a>') . '</div>';
	if ($total > $kmess){
	echo '<p>' . functions::display_pagination('friends.php?' . $link . '&amp;user=' . $user['id'] . '&amp;', $start, $total, $kmess) . '<p>';	
        }
		}
require_once ("../incfiles/end.php");
?>