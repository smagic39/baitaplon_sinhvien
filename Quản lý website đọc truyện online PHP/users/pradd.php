<?php
/**
 * @package     JohnCMS
 * @link        http://johncms.com
 * @copyright   Copyright (C) 2008-2011 JohnCMS Community
 * @license     LICENSE.txt (see attached file)
 * @version     VERSION.txt (see attached file)
 * @author      http://johncms.com/about
 */

define('_IN_JOHNCMS', 1);

$textl = 'Mail';
$headmod = 'pradd';
require_once("../incfiles/core.php");
require_once('../incfiles/head.php');
$lng_pm = core::load_lng('pm');
if ($user_id) {
    $msg = isset($_POST['msg']) ? functions::check($_POST['msg']) : false;
    if (isset($_POST['msgtrans'])) {
        $msg = functions::trans($msg);
    }
    $foruser = isset($_POST['foruser']) ? functions::check($_POST['foruser']) : false;
    $tem = isset($_POST['tem']) ? functions::check($_POST['tem']) : false;
    $idm = isset($_POST['idm']) ? intval($_POST['idm']) : false;
	
    switch ($act) {
        case 'send':


            if (isset($ban['1']) || isset($ban['3']))
                exit;

         
            $flood = functions::antiflood();
            if ($flood) {
					                    require_once('../incfiles/head.php');
                echo functions::display_error($lng['error_flood'] . ' ' . $flood . '&#160;' . $lng['seconds'], '<a href="my_cabinet.php">' . $lng['back'] . '</a>');
                require_once('../incfiles/end.php');
                exit;
            }
            require_once('../incfiles/head.php');
            $ign = mysql_query("select * from `privat` where `website` = '$website' AND  me='" . $foruser . "' and ignor='" . $login . "';");
            $ign1 = mysql_num_rows($ign);
            if ($ign1 != 0) {
                echo functions::display_error($lng_pm['error_ignor'], '<a href="my_cabinet.php">' . $lng['back'] . '</a>');
                require_once('../incfiles/end.php');
                exit;
            }
            if (!empty($foruser) and !empty($msg)) {
                $m = mysql_query("select * from `users` where `website` = '$website' AND  name='" . $foruser . "';");
                $count = mysql_num_rows($m);
                if ($count == 1) {
                    $messag = mysql_query("select * from `users` where `website` = '$website' AND  name='" . $foruser . "';");
                    $us = mysql_fetch_assoc($messag);
                    $adres = $us['id'];
                   
                    $do_file = false;
                    $do_file_mini = false;
                  
                    
                    mysql_query("insert into `privat` values(0,'$website','" . $foruser . "','" . $msg . "','" . time() . "','" . $login . "','in','no','" . $tem . "','0','','','','" . mysql_real_escape_string($fname) . "');");
                    mysql_query("insert into `privat` values(0,'$website','" . $foruser . "','" . $msg . "','" . time() . "','" . $login . "','out','no','" . $tem . "','0','','','','" . mysql_real_escape_string($fname) . "');");
                    if (!empty($idm)) {
                        mysql_query("update `privat` set otvet='1' where `website` = '$website' AND  id='" . $idm . "';");
                    }
                    mysql_query("UPDATE `users` SET `lastpost` = '" . time() . "' where `website` = '$website' AND  `id` = '" . $user_id . "'");
                    echo '<p>' . $lng_pm['message_sent'] . '</p>';
                    if (!empty($_SESSION['refpr'])) {
                        echo "<a href='" . $_SESSION['refpr'] . "'>" . $lng_pm['back'] . "</a><br/>";
                    }
                    $_SESSION['refpr'] = "";
                } else {
                    echo $lng['error_user_not_exist'] . '<br/>';
                }
            } else {
                echo  $lng['error_empty_fields'] . "<br/>";
            }
            break;

        case 'load':
            ////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////
            $id = intval($_GET['id']);
            $fil = mysql_query("select * from `privat` where `website` = '$website' AND  id='" . $id . "';");
            $mas = mysql_fetch_assoc($fil);
            $att = $mas['attach'];
            if (!empty($att)) {
                $tfl = strtolower(functions::format(trim($att)));
                $df = array(
                    "asp",
                    "aspx",
                    "shtml",
                    "htd",
                    "php",
                    "php3",
                    "php4",
                    "php5",
                    "phtml",
                    "htt",
                    "cfm",
                    "tpl",
                    "dtd",
                    "hta",
                    "pl",
                    "js",
                    "jsp"
                );
                if (in_array($tfl, $df)) {
                    require_once('../incfiles/head.php');
                    echo "ERROR!<br/>&#187;<a href='pradd.php'>" . $lng['back'] . "</a><br/>";
                    require_once('../incfiles/end.php');
                    exit;
                }
                if (file_exists("../files/users/pm/$att")) {
                    header("location: ../files/users/pm/$att");
                }
            }
            break;

        case 'write':
            ////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////
            if (isset($ban['1']) || isset($ban['3']))
                exit;
            require_once('../incfiles/head.php');
            $flood = functions::antiflood();
            if ($flood) {
                echo functions::display_error($lng['error_flood'] . ' ' . $flood . '&#160;' . $lng['seconds'], '<a href="my_cabinet.php">' . $lng['back'] . '</a>');
                require_once('../incfiles/end.php');
                exit;
            }
            $adresat = '';
            if (!empty($_GET['adr'])) {
                $messages = mysql_query("select * from `users` where `website` = '$website' AND  id='" . intval($_GET['adr']) . "';");
                $user = mysql_fetch_assoc($messages);
                $adresat = $user['name'];
                $tema = $lng_pm['hi'] . ', ' . $adresat;
                $ign = mysql_query("select * from `privat` where `website` = '$website' AND  me='" . $adresat . "' and ignor='" . $login . "'");
                $ign1 = mysql_num_rows($ign);
                if ($ign1 != 0) {
                    echo $lng_pm['error_ignor'] . '<br/><a href="my_cabinet.php">' . $lng['back'] . '</a><br/>';
                    require_once('../incfiles/end.php');
                    exit;
                }
            } else {
                $tema = $lng_pm['hi'] . '!';
            }
            if (!empty($_GET['id'])) {
                $id = intval($_GET['id']);
                $messages2 = mysql_query("select * from `privat` where `website` = '$website' AND  id='" . $id . "';");
                $tm = mysql_fetch_assoc($messages2);
                $thm = $tm['temka'];
                if (stristr($thm, "Re:")) {
                    $thm = str_replace("Re:", "", $thm);
                    $tema = "Re[1]: $thm";
                } elseif (stristr($thm, "Re[")) {
                    $t1 = str_replace("Re[", "", $thm);
                    $t1 = strtok($t1, "]");
                    $t1 = $t1 + 1;
                    $o = explode(" ", $thm);
                    $thm = str_replace("$o[0]", "", $thm);
                    $tema = "Re[$t1]:$thm";
                } else {
                    $tema = "Re: $thm";
                }
            }
            if (isset($_GET['bir'])) {
                $tema = $lng['happy_birthday'];
            }
            echo '<div class="phdr"><b>' . $lng_pm['write_message'] . '</b></div>';
            echo '<form name="form" action="pradd.php?act=send" method="post" enctype="multipart/form-data">' .
                '<div class="menu">' .
                '<p><h3>' . $lng_pm['to'] . '</h3>' .
                '<input type="text" name="foruser" value="' . $adresat . '"/></p>' .
                '<p><h3>' . $lng_pm['subject'] . '</h3>' .
                '<input type="text" name="tem" value="' . $tema . '"/></p>' .
                '<p><h3>' . $lng['message'] . '</h3>' . bbcode::auto_bb('form', 'msg') .
                '<textarea rows="' . $set_user['field_h'] . '" name="msg"></textarea></p>'   ;
            if ($set_user['translit'])
                echo '<p><input type="checkbox" name="msgtrans" value="1" />&#160;' . $lng['translit'] . '</p>';
            echo "<input type='hidden' name='idm' value='" . $id . "'/><p><input type='submit' value='" . $lng['sent'] . "' /></p></div></form>";
            echo '<div class="phdr"><a href="../pages/faq.php?act=trans">' . $lng['translit'] . '</a> | ' .
            '<a href="../pages/faq.php?act=smileys">' . $lng['smileys'] . '</a></div>';
            break;

        case 'delch':
            ////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////
            require_once('../incfiles/head.php');
            if (isset($_GET['yes'])) {
                $dc = $_SESSION['dc'];
                $prd = $_SESSION['prd'];
                foreach ($dc as $delid) {
                    mysql_query("DELETE FROM `privat` where `website` = '$website' AND  (`user` = '$login' OR `author` = '$login') AND `id`='" . intval($delid) . "'");
                }
                echo $lng_pm['selected_msg_deleted'] . "<br/><a href='" . $prd . "'>" . $lng['back'] . "</a><br/>";
            } else {
                if (empty($_POST['delch'])) {
                    echo $lng_pm['error_not_secected'] . "<br/><a href='pradd.php?act=in'>" . $lng['back'] . "</a><br/>";
                    require_once('../incfiles/end.php');
                    exit;
                }
                foreach ($_POST['delch'] as $v) {
                    $dc[] = intval($v);
                }
                $_SESSION['dc'] = $dc;
                $_SESSION['prd'] = htmlspecialchars(getenv("HTTP_REFERER"));
                echo $lng['delete_confirmation'] . "<br/><a href='pradd.php?act=delch&amp;yes'>" . $lng['delete'] . "</a> | <a href='" . htmlspecialchars(getenv("HTTP_REFERER")) . "'>" . $lng['cancel'] . "</a><br/>";
            }
            break;

        case 'in':
            ////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////
            require_once('../incfiles/head.php');
            if (isset($_GET['new'])) {
                $_SESSION['refpr'] = htmlspecialchars(getenv("HTTP_REFERER"));
                $total = mysql_result(mysql_query("SELECT COUNT(*) FROM `privat` where `website` = '$website' AND  `user` = '$login' AND `type` = 'in' AND `chit` = 'no'"), 0);
                $req = mysql_query("SELECT * FROM `privat` where `website` = '$website' AND  `user` = '$login' AND `type` = 'in' AND `chit` = 'no' ORDER BY `id` DESC LIMIT $start,$kmess");
                echo '<div class="phdr">' . $lng_pm['new_incoming'] . '</div>';
            } else {
                $total = mysql_result(mysql_query("SELECT COUNT(*) FROM `privat` where `website` = '$website' AND  `user` = '$login' AND `type` = 'in'"), 0);
                $req = mysql_query("SELECT * FROM `privat` where `website` = '$website' AND  `user` = '$login' AND `type` = 'in' ORDER BY `id` DESC LIMIT $start,$kmess");
                echo '<div class="phdr"><b>' . $lng_pm['incoming'] . '</b></div>';
            }
            if ($total > $kmess) echo '<div class="topmenu">' . functions::display_pagination('pradd.php?act=in&amp;', $start, $total, $kmess) . '</div>';
            echo '<form action="pradd.php?act=delch" method="post">';
            $i = 0;
            while ($res = mysql_fetch_assoc($req)) {
                if ($res['chit'] == "no") {
                    echo '<div class="gmenu">';
                } else {
                    echo $i % 2 ? '<div class="list2">' : '<div class="list1">';
                }
                echo '<input type="checkbox" name="delch[]" value="' . $res['id'] . '"/><a href="pradd.php?id=' . $res['id'] . '&amp;act=readmess">От: ' . $res['author'] . '</a>';
                echo '&#160;<span class="gray">(' . functions::display_date($res['time']) . ')<br/>' . $lng_pm['subject'] . ':</span> ' . $res['temka'] . '<br/>';
                if (!empty($res['attach'])) {
                    echo '+ ' . $lng_pm['attachment'] . '<br/>';
                }
                if ($res['otvet'] == 0) {
                    echo $lng_pm['not_replyed'] . "<br/>";
                }
                echo '</div>';
                ++$i;
            }
            if ($total > 0) {
                echo '<div class="rmenu"><input type="submit" value="' . $lng_pm['delete_selected'] . '"/></div>';
            }
            echo '</form>';
            echo '<div class="phdr">' . $lng['total'] . ': ' . $total . '</div>';
            if ($total > $kmess) {
                echo '<div class="topmenu">' . functions::display_pagination('pradd.php?act=in&amp;', $start, $total, $kmess) . '</div>' .
                     '<p><form action="pradd.php?act=in" method="post"><input type="text" name="page" size="2"/><input type="submit" value="' . $lng['to_page'] . ' &gt;&gt;"/></form></p>';
            }
            if ($total > 0) {
                echo "<a href='pradd.php?act=delread'>" . $lng_pm['delete_read'] . "</a><br/>";
                echo "<a href='pradd.php?act=delin'>" . $lng_pm['delete_all'] . "</a><br/>";
            }
            break;

        case 'delread':
            ////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////
            require_once('../incfiles/head.php');
            $mess1 = mysql_query("select * from `privat` where `website` = '$website' AND  user='" . $login . "' and type='in' and chit='yes';");
            while ($mas1 = mysql_fetch_assoc($mess1)) {
                $delid = $mas1['id'];
                $delfile = $mas1['attach'];
                        mysql_query("delete from `privat` where `website` = '$website' AND  `id`='" . intval($delid) . "';");
            }
            echo  $lng_pm['read_deleted'] . "<br/>";
            break;

        case 'delin':
            ////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////
            require_once('../incfiles/head.php');
            $mess1 = mysql_query("select * from `privat` where `website` = '$website' AND  user='$login' and type='in'");
            while ($mas1 = mysql_fetch_assoc($mess1)) {
                $delfile = $mas1['attach'];
             
            }
            mysql_query("DELETE FROM `privat` where `website` = '$website' AND  `user` = '$login' AND `type` = 'in'");
            echo $lng_pm['incoming_deleted'] . "<br/>";
            break;

        case 'readmess':
            ////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////
            require_once('../incfiles/head.php');
            $messages1 = mysql_query("select * from `privat` where `website` = '$website' AND  user='" . $login . "' and type='in' and id='" . $id . "';");
            $massiv1 = mysql_fetch_assoc($messages1);
            if ($massiv1['chit'] == "no") {
                mysql_query("update `privat` set `chit`='yes' where `website` = '$website' AND  `id`='" . $massiv1['id'] . "';");
            }
            $newl = mysql_query("select * from `privat` where `website` = '$website' AND  user = '" . $login . "' and type = 'in' and chit = 'no';");
            $countnew = mysql_num_rows($newl);
            if ($countnew > 0) {
                echo "<div style='text-align: center'><a href='" . $set['homeurl'] . "/users/pradd.php?act=in&amp;new'><b><font color='red'>Вам письмо: $countnew</font></b></a></div>";
            }
            $mass = mysql_fetch_assoc(mysql_query("select * from `users` where `website` = '$website' AND  `name`='" . $massiv1['author'] . "';"));
            $text = $massiv1['text'];
            $text = bbcode::tags($text);
            if ($set_user['smileys'])
                $text = functions::smileys($text, 1);
            echo "<p>" . $lng_pm['msg_from'] . " <a href='profile.php?user=" . $mass['id'] . "'>$massiv1[author]</a><br/>";
            echo "(" . functions::display_date($massiv1['time']) . ")</p><p><div class='b'>" . $lng_pm['subject'] . ": $massiv1[temka]<br/></div>" . $lng['text'] . ": $text</p>";
            if (!empty($massiv1['attach'])) {
                echo "<p>" . $lng_pm['attachment'] . ": <a href='?act=load&amp;id=" . $id . "'>$massiv1[attach]</a></p>";
            }
            echo "<hr /><p><a href='pradd.php?act=write&amp;adr=" . $mass['id'] . "&amp;id=" . $massiv1['id'] . "'>" . $lng['reply'] . "</a><br/><a href='pradd.php?act=delmess&amp;del=" . $massiv1['id'] . "'>" . $lng['delete'] . "</a></p>";
            $mas2 = mysql_fetch_assoc(@mysql_query("select * from `privat` where `website` = '$website' AND  `time`='$massiv1[time]' and author='$massiv1[author]' and type='out';"));
            if ($mas2['chit'] == "no") {
                mysql_query("update `privat` set `chit`='yes' where `website` = '$website' AND  `id`='" . $mas2['id'] . "';");
            }
            if ($massiv1['chit'] == "no") {
                mysql_query("update `privat` set `chit`='yes' where `website` = '$website' AND  `id`='" . $massiv1['id'] . "';");
            }
            break;

        case 'delmess':
            ////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////
            require_once('../incfiles/head.php');
            $mess1 = mysql_query("SELECT * FROM `privat` where `website` = '$website' AND  `user` = '$login' AND `id` = '" . intval($_GET['del']) . "' LIMIT 1");
            $mas1 = mysql_fetch_assoc($mess1);
            mysql_query("DELETE FROM `privat` where `website` = '$website' AND  (`user` = '$login' OR `author` = '$login') AND `id` = '" . intval($_GET['del']) . "' LIMIT 1");
            echo $lng_pm['message_deleted'] . '<br/>';
            break;

        case 'delout':
            ////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////
            require_once('../incfiles/head.php');
            $mess1 = mysql_query("select * from `privat` where `website` = '$website' AND  author='$login' and type='out';");
            while ($mas1 = mysql_fetch_assoc($mess1)) {
                $delid = $mas1['id'];
                mysql_query("delete from `privat` where `website` = '$website' AND  `id`='" . intval($delid) . "';");
            }
            echo $lng_pm['sent_deleted'] . "<br/>";
            break;

        case 'out':
            ////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////
            require_once('../incfiles/head.php');
            $total = mysql_result(mysql_query("SELECT COUNT(*) FROM `privat` where `website` = '$website' AND  `author` = '$login' AND `type` = 'out'"), 0);
            $req = mysql_query("SELECT * FROM `privat` where `website` = '$website' AND  `author` = '$login' AND `type` = 'out' ORDER BY `id` DESC LIMIT $start,$kmess");
            echo '<div class="phdr"><b>' . $lng_pm['sent'] . '</b></div>';
            if ($total > $kmess) echo '<div class="topmenu">' . functions::display_pagination('pradd.php?act=out&amp;', $start, $total, $kmess) . '</div>';
            echo "<form action='pradd.php?act=delch' method='post'>";
            $i = 0;
            while ($res = mysql_fetch_assoc($req)) {
                if ($res['chit'] == "no") {
                    echo '<div class="gmenu">';
                } else {
                    echo $i % 2 ? '<div class="list2">' : '<div class="list1">';
                }
                echo '<input type="checkbox" name="delch[]" value="' . $res['id'] . '"/>' . $lng_pm['msg_for'] . ': <a href="pradd.php?id=' . $res['id'] . '&amp;act=readout">' . $res['user'] . '</a>';
                echo '&#160;<span class="gray">(' . functions::display_date($res['time']) . ')<br/>' . $lng_pm['subject'] . ':</span> ' . $res['temka'] . '<br/>';
                if (!empty($res['attach'])) {
                    echo "+ " . $lng_pm['attachment'] . "<br/>";
                }
                echo '</div>';
                ++$i;
            }
            if ($total > 0) {
                echo '<div class="rmenu"><input type="submit" value="' . $lng_pm['delete_selected'] . '"/></div>';
            }
            echo '</form>';
            echo '<div class="phdr">' . $lng['total'] . ': ' . $total . '</div>';
            if ($total > $kmess) {
                echo '<div class="topmenu">' . functions::display_pagination('pradd.php?act=out&amp;', $start, $total, $kmess) . '</div>' .
                     '<p><form action="pradd.php?act=out" method="post"><input type="text" name="page" size="2"/><input type="submit" value="' . $lng['to_page'] . ' &gt;&gt;"/></form></p>';
            }
            if ($total > 0) {
                echo "<a href='pradd.php?act=delout'>" . $lng_pm['delete_all_sent'] . "</a><br/>";
            }
            break;

        case 'readout':
            ////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////
            require_once('../incfiles/head.php');
            $messages1 = mysql_query("select * from `privat` where `website` = '$website' AND  author='" . $login . "' and type='out' and id='" . $id . "';");
            $massiv1 = mysql_fetch_assoc($messages1);
            $mass = mysql_fetch_assoc(@mysql_query("select * from `users` where `website` = '$website' AND  `name`='$massiv1[user]';"));
            $text = $massiv1['text'];
            $text = bbcode::tags($text);
            if ($set_user['smileys'])
                $text = functions::smileys($text, ($massiv1['from'] == $nickadmina || $massiv1['from'] == $nickadmina2 || $massiv11['rights'] >= 1) ? 1 : 0);
            echo "<p>" . $lng_pm['msg_for'] . " <a href='profile.php?user=" . $mass['id'] . "'>$massiv1[user]</a><br/>";
            echo "(" . functions::display_date($massiv1['time']) . ")</p><p><div class='b'>" . $lng_pm['subject'] . ": $massiv1[temka]<br/></div>" . $lng['text'] . ": $text</p>";
            if (!empty($massiv1['attach'])) {
                echo "<p>" . $lng_pm['attachment'] . ": $massiv1[attach]</p>";
            }
            echo "<hr /><p><a href='pradd.php?act=delmess&amp;del=" . $massiv1['id'] . "'>" . $lng['delete'] . "</a></p>";
            break;
	default:
			require_once('../incfiles/head.php');
			break;
			
    }
    echo "<p><a href='profile.php?act=office'>" . $lng['personal'] . "</a><br/>";
    echo "<a href='pradd.php?act=write'>" . $lng['write'] . "</a></p>";
}

require_once('../incfiles/end.php');

?>
