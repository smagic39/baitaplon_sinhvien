<?php


/*
[]------------------------------------------------------------------------------[]
||  #{Script Info}-----------------------------------------------------------#  ||
||  #[*] Name Script   : pekubu wapbuilder                                   #  ||
||  #[*] Version       : 1.0                                                 #  ||
||  #[*] Type          : Wapbuilder                                          #  ||
||  #[*] Language      : PHP/MySQL                                           #  ||
||  #[*] Website       : www.pekubu.net                                      #  ||
||  #[*] Release Date  : 22/10/2012                                          #  ||
||  #[*] Demo Script   : http://www.infoviet.net                             #  ||
||                                                                              ||
||                                                                              ||
||  #{Contact Us}------------------------------------------------------------#  ||
||  #[*] Website      : www.pekubu.net                                       #  ||
||  #[*] E-Mail       : thi3nsu_tinhy3u@yahoo.com.vn  - pekubu@gmail.com     #  ||
||       **********************************************************          #  ||
||       **********************************************************          #  ||
[]------------------------------------------------------------------------------[]
*/

if(!$is_mobile) {
echo'

    
    <div class="tvo_section">
        	<b>I. NỘI DUNG WAP/WEB</b>
</br>Không đăng tải, truyền bá thông tin mang tính chất tôn giáo, dân tộc, chính trị, lịch sử,...Mang tính chất nhạy cảm.
</br>Không đăng tả thông tin kích bác, đả kích, chửi bới, bêu riếu, xúc phạm...ảnh hưởng đến danh dự, nhân phẩm người khác.
</br>Không đăng tải thông tin, nội dung vi phạm thuần phong mỹ tục và vi phạm luật pháp nước CHXH CN Việt Nam.
</br>Không tải lên, lưu trữ các tệp tin có chứa mã độc, virus... làm tổn hại đến tài nguyên máy chủ.
</br>Không được đăng ký tài khoản có từ ngữ dung tục, khiếm nhã dưới bất kỳ ngôn ngữ nào, không được trùng với tên các vị lãnh tụ, danh nhân, hoặc tên các cơ quan, tổ chức... gây bức xúc, khó chịu, hiểu nhầm...
</br><b>II. TRÁCH NHIỆM & QUYỀN HẠN THÀNH VIÊN</b>
</br>Chịu trách nhiệm về dữ liệu lưu trữ trên tài khoản của mình.
</br>Chịu trách nhiệm trước pháp luật khi cố ý gây tổn hại đến server (máy chủ, hệ thống) hay chất lượng dịch vụ.
</br>Tự xây dựng nội dung, các vấn đề liên quan đến CODE...InFoViệt không có trách nhiệm hay hỗ trợ bất cứ gì trong việc xây dựng wapsite của thành viên.
</br>Không được phép sử dụng tài khoản của mình chỉ để lưu trữ file (tệp tin) mà không liên kết đến trên site của mình.
</br>Không cố tình làm thay đổi trực tiếp hoặc gián tiếp lưu lượng truy cập bằng các hình thức giả mạo hoặc tương tự.
</br>Không được phép xóa hoặc thay đổi cách thức hiển thị quảng cáo (với các tài khoản chưa được nâng cấp) dưới mọi hình thức.
</br>Không đăng ký nhiều tài khoản với mục đích chiếm dụng, đầu cơ, lưu trữ tên miền.
</br>Được phép sử dụng công cụ InFoViệt để xây dựng, phát triển wap/web cá nhân.
</br>Được phép sử dụng tên miền, hosting miễn phí của InFoViệt.
</br>Được tự thiết kế giao diện, nội dung cho wap/web của mình.
</br>Được sử dụng hoàn toàn miễn phí các nội dung mà InFoViệt cung cấp.
</br>Có quyền kinh doanh trên chính wap/web của mình, tuy nhiên cần thông báo rõ và cụ thể các mức giá mà người dùng phải trả cho các dịch vụ ấy. Tuyệt đối không lừa đảo, chiếm đoạt tài sản người dùng dưới mọi hình thức.
</br><b>III. TRÁCH NHIỆM & QUYỀN HẠN InFoViệt</b>
</br>Có quyền xóa các tệp tin, tài khoản vi phạm điều khoản hoặc khi có yêu cầu của cơ quan chức năng.
</br>Có quyền xóa, sửa, di chuyển các tệp tin của người dùng.
</br>Có quyền thu thập thông tin về thành viên, người dùng (visitors) nhằm tăng chất lượng dịch vụ và phát triển hệ thống.
</br>Có trách nhiệm đảm bảo hệ thống hoạt động với hiệu suất tốt nhất.
</br>Có quyền chèn quảng cáo trên các wap/web thuộc hệ thống InFoViệt dưới mọi hình thức.
</br>Có trách nhiệm hỗ trợ thành viên các vấn đề liên quan tới công cụ, hệ thống.
</br>Có quyền kinh doanh trên hệ thống InFoViệt
</br>Có quyền thu phí nâng cấp tài khoản như: mua dung lượng, xóa quảng cáo....
</br>Không chịu trách nhiệm về nội dung, tệp tin, thông tin lưu trữ khi máy chủ, hệ thống hỏng hóc hoặc bị hư hại trong các trường hợp thiên tại, hỏa hoạn....
</br>Tất cả thành viên khi tham gia InFoViệt phải tuân thủ nghiêm ngặt các quy định trên, thành viên vi phạm sẽ bị xử lý tùy vào mức độ vi phạm, InFoViệt sẽ khóa hoặc xóa vĩnh viễn các tài khoản cố tình vi phạm các quy định trên. InFoViệt có quyền thay đổi quy định sử dụng mà không cần báo trước.
</br><b>IV. Quy Định Kinh Doanh</b>
</br>1. Quy định cho nhà cung cấp và phân phối nội dung trên di động:
</br>1.1. Nội dung kinh doanh không được phép vi phạm pháp luật Việt Nam
</br>1.2. Phải đảm bảo về quyền lợi của khách hàng với nội dung cung cấp
</br>1.3. InFoViệt có quyền tạm dừng thanh toán để điều tra các trường hợp vi phạm quy định
</br>1.4. InFoViệt có quyền ngừng thanh toán đối với các trường hợp vi phạm quy định
</br>1.5. Nhà cung cấp phải chịu toàn bộ các trách nhiệm liên quan khác đối với sản phẩm của mình
</br><b>IV. Quy Định Kinh Doanh</b>
</br>1.Hình thức thanh toán
</br>+ TOPup
</br>+ Thẻ điện thoại
</br>2. Hạn mức thanh toán
</br>+ Số tiền nhận được trên 10.000 VNĐ
</br>+ Cộng dồn vào lần tiếp theo nếu chưa đủ hạn mức thanh toán
</br>3. Thực hiện thanh toán
</br>+ Thanh toán ngày chủ nhật hàng tuần



</div> ';
} else {
echo'<div class="list1">
        	<b>I. NỘI DUNG WAP/WEB</b>
</br>Không đăng tải, truyền bá thông tin mang tính chất tôn giáo, dân tộc, chính trị, lịch sử,...Mang tính chất nhạy cảm.
</br>Không đăng tả thông tin kích bác, đả kích, chửi bới, bêu riếu, xúc phạm...ảnh hưởng đến danh dự, nhân phẩm người khác.
</br>Không đăng tải thông tin, nội dung vi phạm thuần phong mỹ tục và vi phạm luật pháp nước CHXH CN Việt Nam.
</br>Không tải lên, lưu trữ các tệp tin có chứa mã độc, virus... làm tổn hại đến tài nguyên máy chủ.
</br>Không được đăng ký tài khoản có từ ngữ dung tục, khiếm nhã dưới bất kỳ ngôn ngữ nào, không được trùng với tên các vị lãnh tụ, danh nhân, hoặc tên các cơ quan, tổ chức... gây bức xúc, khó chịu, hiểu nhầm...
</br><b>II. TRÁCH NHIỆM & QUYỀN HẠN THÀNH VIÊN</b>
</br>Chịu trách nhiệm về dữ liệu lưu trữ trên tài khoản của mình.
</br>Chịu trách nhiệm trước pháp luật khi cố ý gây tổn hại đến server (máy chủ, hệ thống) hay chất lượng dịch vụ.
</br>Tự xây dựng nội dung, các vấn đề liên quan đến CODE...InFoViệt không có trách nhiệm hay hỗ trợ bất cứ gì trong việc xây dựng wapsite của thành viên.
</br>Không được phép sử dụng tài khoản của mình chỉ để lưu trữ file (tệp tin) mà không liên kết đến trên site của mình.
</br>Không cố tình làm thay đổi trực tiếp hoặc gián tiếp lưu lượng truy cập bằng các hình thức giả mạo hoặc tương tự.
</br>Không được phép xóa hoặc thay đổi cách thức hiển thị quảng cáo (với các tài khoản chưa được nâng cấp) dưới mọi hình thức.
</br>Không đăng ký nhiều tài khoản với mục đích chiếm dụng, đầu cơ, lưu trữ tên miền.
</br>Được phép sử dụng công cụ InFoViệt để xây dựng, phát triển wap/web cá nhân.
</br>Được phép sử dụng tên miền, hosting miễn phí của InFoViệt.
</br>Được tự thiết kế giao diện, nội dung cho wap/web của mình.
</br>Được sử dụng hoàn toàn miễn phí các nội dung mà InFoViệt cung cấp.
</br>Có quyền kinh doanh trên chính wap/web của mình, tuy nhiên cần thông báo rõ và cụ thể các mức giá mà người dùng phải trả cho các dịch vụ ấy. Tuyệt đối không lừa đảo, chiếm đoạt tài sản người dùng dưới mọi hình thức.
</br><b>III. TRÁCH NHIỆM & QUYỀN HẠN InFoViệt</b>
</br>Có quyền xóa các tệp tin, tài khoản vi phạm điều khoản hoặc khi có yêu cầu của cơ quan chức năng.
</br>Có quyền xóa, sửa, di chuyển các tệp tin của người dùng.
</br>Có quyền thu thập thông tin về thành viên, người dùng (visitors) nhằm tăng chất lượng dịch vụ và phát triển hệ thống.
</br>Có trách nhiệm đảm bảo hệ thống hoạt động với hiệu suất tốt nhất.
</br>Có quyền chèn quảng cáo trên các wap/web thuộc hệ thống InFoViệt dưới mọi hình thức.
</br>Có trách nhiệm hỗ trợ thành viên các vấn đề liên quan tới công cụ, hệ thống.
</br>Có quyền kinh doanh trên hệ thống InFoViệt
</br>Có quyền thu phí nâng cấp tài khoản như: mua dung lượng, xóa quảng cáo....
</br>Không chịu trách nhiệm về nội dung, tệp tin, thông tin lưu trữ khi máy chủ, hệ thống hỏng hóc hoặc bị hư hại trong các trường hợp thiên tại, hỏa hoạn....
</br>Tất cả thành viên khi tham gia InFoViệt phải tuân thủ nghiêm ngặt các quy định trên, thành viên vi phạm sẽ bị xử lý tùy vào mức độ vi phạm, InFoViệt sẽ khóa hoặc xóa vĩnh viễn các tài khoản cố tình vi phạm các quy định trên. InFoViệt có quyền thay đổi quy định sử dụng mà không cần báo trước.
</br><b>IV. Quy Định Thanh Toán</b>
</br>1. Quy định cho nhà cung cấp và phân phối nội dung trên di động:
</br>1.1. Nội dung kinh doanh không được phép vi phạm pháp luật Việt Nam
</br>1.2. Phải đảm bảo về quyền lợi của khách hàng với nội dung cung cấp
</br>1.3. InFoViệt có quyền tạm dừng thanh toán để điều tra các trường hợp vi phạm quy định
</br>1.4. InFoViệt có quyền ngừng thanh toán đối với các trường hợp vi phạm quy định
</br>1.5. Nhà cung cấp phải chịu toàn bộ các trách nhiệm liên quan khác đối với sản phẩm của mình
</br><b>IV. Quy Định Kinh Doanh</b>
</br>1.Hình thức thanh toán
</br>+ TOPup
</br>+ Thẻ điện thoại
</br>2. Hạn mức thanh toán
</br>+ Số tiền nhận được trên 10.000 VNĐ
</br>+ Cộng dồn vào lần tiếp theo nếu chưa đủ hạn mức thanh toán
</br>3. Thực hiện thanh toán
</br>+ Thanh toán ngày chủ nhật hàng tuần



</div> ';
}