<?php
/**
* @package     JohnCMS
* @link        http://johncms.com
* @copyright   Copyright (C) 2008-2011 JohnCMS Community
* @license     LICENSE.txt (see attached file)
* @version     VERSION.txt (see attached file)
* @author      http://johncms.com/about
*/

defined('_IN_JOHNCMS') or die('Error: restricted access');

require('../incfiles/head.php');
if (empty($_GET['id'])) {
echo functions::display_error($lng['error_wrong_data']);
require('../incfiles/end.php');
exit;
}
$s = intval($_GET['s']);
// Запрос сообщения
$req = mysql_query("SELECT `forum`.*, `users`.`sex`, `users`.`rights`, `users`.`lastdate`, `users`.`status`, `users`.`datereg`
FROM `forum` LEFT JOIN `users` ON `forum`.`user_id` = `users`.`id`
WHERE `forum`.`website` = '$website' AND `forum`.`type` = 'm' AND `forum`.`id` = '$id'" . ($rights >= 7 ? "" : " AND `forum`.`close` != '1'") . " LIMIT 1");
$res = mysql_fetch_assoc($req);

// Запрос темы
$them = mysql_fetch_assoc(mysql_query("SELECT * FROM `forum` WHERE `website` = '$website' AND `type` = 't' AND `id` = '" . $res['refid'] . "'"));
echo '<div class="mainblok"><div class="phdr"><b>' . $lng_forum['topic'] . ':</b> ' . $them['text'] . '</div><div class="newsx">';
// Значок пола
if ($res['sex'])
echo '<img src="http://img.infoviet.net/theme/' . $set_user['skin'] . '/images/' . ($res['sex'] == 'm' ? 'm' : 'w') . '.png" alt=""  width="16" height="16"/>&#160;';
else
echo '<img src="http://img.infoviet.net/images/del.png" width="12" height="12" />&#160;';
// Ник юзера и ссылка на его анкету
if ($user_id && $user_id != $res['user_id']) {
echo '<a href="../users/profile.php?user=' . $res['user_id'] . '&amp;fid=' . $res['id'] . '"><b>' . $res['from'] . '</b></a> ';
echo '<a href="index.php?act=say&amp;id=' . $res['id'] . '&amp;start=' . $start . '"> [о]</a> <a href="index.php?act=say&amp;id=' . $res['id'] . '&amp;start=' . $start . '&amp;cyt"> [ц]</a>';
} else {
echo '<b>' . $res['from'] . '</b>';
}
// Метка должности
switch ($res['rights']) {
case 7:
echo " Adm ";
break;

case 6:
echo " Smd ";
break;
case 3:
echo " Mod ";
break;

case 1:
echo " Kil ";
break;
}
// Метка Онлайн / Офлайн
echo (time() > $res['lastdate'] + 300 ? '<span class="red"> [Off]</span>' : '<span class="green"> [ON]</span>');
// Время поста
echo ' <span class="gray">(' . functions::display_date($res['time']) . ')</span><br/>';
// Статус юзера
if (!empty($res['status']))
echo '<div class="status"><img src="http://img.infoviet.net/images/star.gif" alt=""/>&#160;' . $res['status'] . '</div>';
echo '</div><div class="forumtxt">';
$text = htmlentities($res['text'], ENT_QUOTES, 'UTF-8');
$text = nl2br($text);
$text = tags(url($text));
if ($set_user['smileys'])
$text = functions::smileys($text, ($res['rights'] >= 1) ? 1 : 0);
$checkthank = mysql_query('SELECT COUNT(*) FROM `forum_thank` WHERE `website` = "'.$website.'" AND `userthank` = "' . $user_id . '" and `topic` = "' . $res['id'] . '" and `user` = "' . $res['user_id'] . '"');
$thankcheck = mysql_result($checkthank, 0);
if($thankcheck < 1 && $user_id != $res['user_id']) {
$text = preg_replace('#\[thank\](.*?)\[/thank\]#si', '<div class="bmenu">Nội dung ẩn</div><div class="quote">Bạn vui lòng cảm ơn bài viết này mới được xem nội dung ẩn</div>', $text);
} else {
$text = preg_replace('#\[thank\](.*?)\[/thank\]#si', '\1', $text);
}
echo $text . '</div></div>';
// Вычисляем, на какой странице сообщение?
$page = ceil(mysql_result(mysql_query("SELECT COUNT(*) FROM `forum` WHERE `website` = '$website' AND `refid` = '" . $res['refid'] . "' AND `id` " . ($set_forum['upfp'] ? ">=" : "<=") . " '$id'"), 0) / $kmess);
echo '<div class="phdr"><a href="index.php?id=' . $res['refid'] . '&amp;page=' . $page . '">' . $lng_forum['back_to_topic'] . '</a></div>';
echo '<p><a href="index.php">' . $lng['to_forum'] . '</a></p>';

?>
