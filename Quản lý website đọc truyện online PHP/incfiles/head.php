<?php
ob_start();
require(''.$_SERVER['DOCUMENT_ROOT'].'/laivt_firewall.php');
defined('_IN_JOHNCMS') or die('Error: restricted access');
$headmod = isset($headmod) ? mysql_real_escape_string($headmod) : '';
$textl = isset($textl) ? $textl : $set['copyright'];
$textl = strtr($textl, array(
'&quot;' => '',
'&amp;' => '',
'&lt;' => '',
'&gt;' => '',
'&#039;' => '',
'&acirc;' => 'a', '&aacute;' => 'á', '&agrave;' => 'à', '&acirc;̉' => 'ả', '&atilde;' => 'ã', '&acirc;̣' => 'ạ',
'&Acirc;' => 'A', '&Aacute;' => 'Á', '&Agrave;' => 'À', '&Acirc;̉' => 'Ả', '&Atilde;' => 'Ã', '&Acirc;̣' => 'Ạ',
'&acirc;' => 'ă', '&acirc;́' => 'ắ', '&acirc;̀' => 'ằ', '&acirc;̉' => 'ẳ', '&acirc;̃' => 'ẵ', '&acirc;̣' => 'ặ',
'&Acirc;' => 'Ă', '&Acirc;́' => 'Ắ', '&Acirc;̀' => 'Ằ', '&Acirc;̉' => 'Ẳ', '&Acirc;̃' => 'Ẵ', '&Acirc;̣' => 'Ặ',
'&acirc;' => 'â', '&acirc;́' => 'ấ', '&acirc;̀' => 'ầ', '&acirc;̉' => 'ẩ', '&acirc;̃' => 'ẫ', '&acirc;̣' => 'ậ',
'&Acirc;' => 'Â', '&Acirc;́' => 'Ấ', '&Acirc;̀' => 'Ầ', '&Acirc;̉' => 'Ẩ', '&Acirc;̃' => 'Ẫ', '&Acirc;̣' => 'Ậ',
'&ecirc;' => 'e', '&eacute;' => 'é', '&eagrave;' => 'è', '&ecirc;̉' => 'ẻ', '&etilde;' => 'ẽ', '&ecirc;̣' => 'ẹ',
'&Ecirc;' => 'E', '&Eacute;' => 'É', '&Egrave;' => 'È', '&Ecirc;̉' => 'Ẻ', '&Etilde;' => 'Ẽ', '&Ecirc;̣' => 'Ẹ',
'&ecirc;' => 'ê', '&ecirc;́' => 'ế', '&ecirc;̀' => 'ề', '&ecirc;̉' => 'ể', '&ecirc;̃' => 'ễ', '&ecirc;̣' => 'ệ',
'&Ecirc;' => 'Ê', '&Ecirc;́' => 'Ế', '&Ecirc;̀' => 'Ề', '&Ecirc;̉' => 'Ể', '&Ecirc;̃' => 'Ễ', '&Ecirc;̣' => 'Ệ',
'&icirc;' => 'i', '&iacute;' => 'í', '&igrave;' => 'ì', '&icirc;̉' => 'ỉ', '&itilde;' => 'ĩ', '&icirc;̣' => 'ị',
'&Icirc;' => 'I', '&Iacute;' => 'Í', '&Igrave;' => 'Ì', '&Icirc;̉' => 'Ỉ', '&Itilde;' => 'Ĩ', '&Icirc;̣' => 'Ị',
'&ucirc;' => 'u', '&uacute;' => 'ú', '&ugrave;' => 'ù', '&ucirc;̉' => 'ủ', '&utilde;' => 'ũ', '&ucirc;̣' => 'ụ',
'&Ucirc;' => 'U', '&Uacute;' => 'Ú', '&Ugrave;' => 'Ù', '&Ucirc;̉' => 'Ủ', '&Utilde;' => 'Ũ', '&Ucirc;̣' => 'Ụ',
'&ucirc;' => 'ư', '&ucirc;́' => 'ứ', '&ucirc;̀' => 'ừ', '&ucirc;̉' => 'ử', '&ucirc;̃' => 'ữ', '&ucirc;̣' => 'ự',
'&Ucirc;' => 'Ư', '&Ucirc;́' => 'Ứ', '&Ucirc;̀' => 'Ừ', '&Ucirc;̉' => 'Ử', '&Ucirc;̃' => 'Ữ', '&Ucirc;̣' => 'Ự',
'&ocirc;' => 'o', '&oacute;' => 'ó', '&ograve;' => 'ò', '&ocirc;̉' => 'ỏ', '&otilde;' => 'õ', '&ocirc;̣' => 'ọ',
'&Ocirc;' => 'O', '&Oacute;' => 'Ó', '&Ograve;' => 'Ó', '&Ocirc;̉' => 'Ỏ', '&Otilde;' => 'Õ', '&Ocirc;̣' => 'Ọ',
'&ocirc;' => 'ơ', '&ocirc;́' => 'ớ', '&ocirc;̀' => 'ờ', '&ocirc;̉' => 'ở', '&ocirc;̃' => 'ỡ', '&ocirc;̣' => 'ợ',
'&Ocirc;' => 'Ơ', '&Ocirc;́' => 'Ớ', '&Ocirc;̀' => 'Ờ', '&Ocirc;̉' => 'Ở', '&Ocirc;̃' => 'Ỡ', '&Ocirc;̣' => 'Ợ',
'&ocirc;' => 'ô', '&ocirc;́' => 'ố', '&ocirc;̀' => 'ồ', '&ocirc;̉' => 'ổ', '&ocirc;̃' => 'ỗ', '&ocirc;̣' => 'ộ',
'&Ocirc;' => 'Ô', '&Ocirc;́' => 'Ố', '&Ocirc;̀' => 'Ồ', '&Ocirc;̉' => 'Ổ', '&Ocirc;̃' => 'Ỗ', '&Ocirc;̣' => 'Ộ',
'&ycirc;' => 'y', '&yacute;' => 'ý', '&ygrave;' => 'ỳ', '&ycirc;̉' => 'ỷ', '&ytilde;' => 'Ỹ', '&ycirc;̣' => 'ỵ',
'&Ycirc;' => 'Y', '&Yacute;' => 'Ý', '&Ygrave;' => 'Ỳ', '&Ycirc;̉' => 'Ỷ', '&Ytilde;' => 'Ỹ', '&Ycirc;̣' => 'Ỵ'
));
/*
-----------------------------------------------------------------
Выводим HTML заголовки страницы, подключаем CSS файл
-----------------------------------------------------------------
*/
if(stristr(core::$user_agent, "msie") && stristr(core::$user_agent, "windows")){
// Выдаем заголовки для Internet Explorer
header("Cache-Control: no-store, no-cache, must-revalidate");
header('Content-type: text/html; charset=UTF-8');
}
$textl=html_entity_decode($textl,ENT_QUOTES,'UTF-8'); ////fix lỗi font title

header("Expires: " . date("r",  time() + 60));
echo '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
"\n" . '<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">' .
"\n" . '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru">' .
"\n" . '<head>' .
"\n" . '<meta content="dnmsuper" name="author"/>' .
"\n" . '<meta content="index,follow" name="robots"/>' .
"\n" . '<meta content="index,follow" name="googlebot"/>' .
"\n" . '<meta content="index,follow" name="Slurp"/>' .
"\n" . '<meta content="index,follow" name="MSNBot"/>' .
"\n" . '<meta content="1 day" name="revisit-after"/>' .
"\n" . '<meta content="global" name="distribution"/>' .
"\n" . '<meta content="1 day" name="revisit-after"/>' .
"\n" . '<meta content="document" name="resource-type"/>' .
"\n" . '<meta content="all" name="audience"/>' .
"\n" . '<meta content="general" name="rating"/>' .
"\n" . '<meta content="index, follow" name="robots"/>' .
"\n" . '<meta content="infoviet.net" name="author"/>' .
"\n" . '<meta content="Vietnamese, English" name="language"/>' .
"\n" . '<meta content="Vietnam" name="country"/>' .

"\n" . '<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8"/>' .
"\n" . '<meta http-equiv="Content-Style-Type" content="text/css" />' .
"\n" . '<meta name="Generator" content="JohnCMS, http://johncms.com" />' . // ВНИМАНИЕ!!! Данный копирайт удалять нельзя
(!empty($set['meta_key']) ? "\n" . '<meta name="keywords" content="' . $set['meta_key'] . '" />' : '') .
(!empty($set['meta_desc']) ? "\n" . '<meta name="description" content="' . $set['meta_desc'] . '" />' : '') .
"\n" . '<link rel="stylesheet" href="http://img.infoviet.net/theme/' . $set['skindef'] . '/style.css" type="text/css" />' .
"\n" . '<link rel="shortcut icon" href="'.($set['icon'] ? $set['icon']  :'http://img.infoviet.net/favicon.ico').'" />' .
"\n" . '<link rel="alternate" type="application/rss+xml" title="RSS | ' . $lng['site_news'] . '" href="' . $set['homeurl'] . '/rss/rss.php" />' .
"\n" . '<title>' . notags($textl) . '</title>' .
"\n" . ' '. html_entity_decode($set['meta_ver']).' ' .
"\n" . '</head><body>' . core::display_core_errors();
unset($text);
/*
-----------------------------------------------------------------
Рекламный модуль
-----------------------------------------------------------------
*/

$cms_ads = array();
if (!isset($_GET['err']) && $act != '404' && $headmod != 'admin') {
$view = $user_id ? 2 : 1;
$layout = ($headmod == 'mainpage' && !$act) ? 1 : 2;
$req = mysql_query("SELECT * FROM `cms_ads` where `website` = '$website' AND  `to` = '0' AND (`layout` = '$layout' or `layout` = '0') AND (`view` = '$view' or `view` = '0') ORDER BY  `mesto` ASC");
if (mysql_num_rows($req)) {
while (($res = mysql_fetch_assoc($req)) !== false) {
$name = explode("|", $res['name']);
$name = htmlentities($name[mt_rand(0, (count($name) - 1))], ENT_QUOTES, 'UTF-8');
if (!empty($res['color'])) $name = '<span style="color:#' . $res['color'] . '">' . $name . '</span>';
// Если было задано начертание шрифта, то применяем
$font = $res['bold'] ? 'font-weight: bold;' : false;
$font .= $res['italic'] ? ' font-style:italic;' : false;
$font .= $res['underline'] ? ' text-decoration:underline;' : false;
if ($font) $name = '<span style="' . $font . '">' . $name . '</span>';
@$cms_ads[$res['type']] .= '<a href="' . ($res['show'] ? functions::checkout($res['link']) : $set['homeurl'] . '/go.php?id=' . $res['id']) . '">' . $name . '</a><br/>';
if (($res['day'] != 0 && time() >= ($res['time'] + $res['day'] * 3600 * 24)) || ($res['count_link'] != 0 && $res['count'] >= $res['count_link']))
mysql_query("UPDATE `cms_ads` SET `to` = '1'  where `website` = '$website' AND  `id` = '" . $res['id'] . "'");
}
}
mySQL_free_result($req);
}

/*
-----------------------------------------------------------------
Рекламный блок сайта
-----------------------------------------------------------------
*/
if (isset($cms_ads[0])) echo $cms_ads[0];

/*
-----------------------------------------------------------------
Выводим логотип и переключатель языков
-----------------------------------------------------------------
*/
//online ao
mysql_query("UPDATE `users` SET $sql `total_on_site` = '$totalonsite', `lastdate` = " . time() . " where `website` = '$website' AND  `id` = '2'");
/*
-----------------------------------------------------------------
Выводим верхний блок с приветствием
-----------------------------------------------------------------
*/
echo '<div class="bodybg">';

$H=date("H")+7;

$he = mysql_query("SELECT * FROM website WHERE name_lat='$website'") or die(mysql_error());
$he_a = mysql_fetch_assoc($he);

mySQL_free_result($he);
if ($he_a['head']== null) {
if(!$user_id){
echo '<div class="top-menu">
    <a href="' . $set['homeurl'] . '/login.php">' . $lng['login'] . '</a>        
    <a href="' . $set['homeurl'] . '/registration.php" style="float:right;">' . $lng['registration'] . '</a>
</div>';
}
echo '<div class="header-block-blue">
<table width="100%">
<tr>
<td>Menu Nhanh</td>
<td align="right">'.$H.date(":i").'</td>
</tr>
</table>
</div>';
echo '<table width="100%" border="0" cellspacing="0">
                                <tr class="chon">
                                    <td><a href="../users/index.php?act=userlist" style="color:#fff;">' . $lng['users'] . '</a></td> 
                                    <td><a href="../users/index.php?act=admlist" style="color:#fff;">' . $lng['administration'] . '</a></td> 
                                    <td><a href="../users/album.php"style="color:#fff;">Album ảnh</a></td> 
                                    <td><a href="../users/index.php?act=top" style="color:#fff;">' . $lng['users_top'] . '</a></td>
                                    <td><a href="../pages/faq.php" style="color:#fff;">' . $lng['information'] . '</a></td>
                                </tr>
                             </table>';


echo '<table width="100%" border="0" cellspacing="0">
                                <tr class="nhanh">
                                    <td><a href="../forum" style="color:#C4BA00;">Diễn đàn</a></td> 
                                    <td><a href="../congcu/index.php" style="color:#57007F;">Giải trí</a></td> 
                                    <td><a href="../mp3" style="color:#FF006E;">Nghe nhạc</a></td> 
                                    <td><a href="../dantri" style="color:#0094FF;">Dân trí</a></td>
                                    <td><a href="../soo" style="color:#7F0037;">Hội nhóm</a></td>
                                </tr>
                            </table>';
							
							
							
if($user_id){
echo '<div class="sep"></div>
<div class="s">
    <table cellpadding="0" cellspacing="0" width="100%" border="0"><tr>
     <td valign="top" width="40" height="40" style="padding-right:3px;">
<a href="' . $set['homeurl'] . '/users/profile.php?act=images&amp;mod=avatar&amp;user=' . $user_id . '">';
if (file_exists('' .
     (isset($_GET['err']) || $headmod != "mainpage" || ($headmod == 'mainpage' && $act) ? '../' : '') . 'files/users/avatar/' . $user_id . '.png'))
                            echo '<img class="avatar" src="../files/users/avatar/' . $user_id . '.png" width="32" height="32" alt="' . $user_id . '" align="top" />&#160;';
                        else
                             echo '<img src="http://img.infoviet.net/images/empty.png" width="32" height="32" alt="' . $user_id . '" align="top" />&#160;';


   echo '</a>
          </td>
       <td valign="top">
Chào, <b style="color:#B5651D;">' . $login . '</b><br/>
'.$datauser['balans'].'<b> '.$set['tiente'].'</b></td></tr></table>

</div>';



echo '<table class="category-submenu">
<tr>
<td valign="top">
<a href="' . $set['homeurl'] . '">Trang chủ</a><br/>
<a href="' . $set['homeurl'] . '/users/profile.php?act=office">Cá nhân</a><br/>
';
if ($rights >= 1)
    echo '<span class="red"><a href="../' . $set['admp'] . '/index.php"><b>' . $lng['admin_panel'] . '</b></a></span></br>';

	echo'

</td>
<td valign="top"><b>
<a href="mod/yourtopic.php"color:#FF6A00;">Chủ đề của bạn</a></b>';

if(!$total==0) echo '&#160;<span class="number">' . $total . '</span>';
echo '<br />' . counters::forum_new(1) . '';
echo '<br /><b><a href="' . $set['homeurl'] . '/forum/guichude.php">Gửi chủ đề mới</a>
</b>
</td>

</tr>
<tr><td><a href="' . $set['homeurl'] . '/exit.php">Thoát</a></td><td><a href="' . $set['homeurl'] . '/users/quiz.php">Gửi trắc nghiệm</a></td></tr>
</table>';
}
echo '<div class="sep"></div><div class="pad5">
    <form action="../forum/search.php" method="post">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="auto">
                <input type="text" name="search" style="width:100%;" />
            </td>
            <td width="15">
              <input type="submit" name="submit" value="Tìm" /> 
                
            </td>
        </tr>
    </table>
</form>
</div>';							
	echo functions::chatbox();						
}


 



else {
$a=html_entity_decode($he_a['head'],ENT_QUOTES,'utf-8');
if(strpos($a,'<?')) {
echo 'Có lỗi xảy ra';
exit;
}
if(strpos($a,'base64')) {
echo 'Có lỗi xảy ra';
exit;
}
echo functions::func($a);
unset($a);
} 
if ($he_a['quatang']!=0){$new_date=strtotime ( '+30 day' , $he_a['quatang']  );
$date=strtotime(date("Y-m-j"));
$hack=$new_date-$date;
if($hack<0) 
 {
 $a=mysql_query("UPDATE `website` SET  quatang = '0' where `name_lat` = '$website' ");
 }
 
 }
 if ($he_a['ketban']!=0){
 $new_date=strtotime ( '+30 day' , $he_a['ketban']  );
$date=strtotime(date("Y-m-j"));
$hack=$new_date-$date;
if($hack<0) 
 {
 $a=mysql_query("UPDATE `website` SET  ketban = '0' where `name_lat` = '$website' ");
 }
 }
 ///////////
 if ($he_a['tuvi']!=0){
 $new_date=strtotime ( '+30 day' , $he_a['tuvi']  );
$date=strtotime(date("Y-m-j"));
$hack=$new_date-$date;
if($hack<0) 
 {
 $a=mysql_query("UPDATE `website` SET  tuvi = '0' where `name_lat` = '$website' ");
 }
 }
 
 //////////////
if($he_a['qc']!=0){
if($he_a['qc'] ==1) {
$quangcao='0';}
else {
$new_date=strtotime ( '+30 day' , $he_a['qc']  );
$date=strtotime(date("Y-m-j"));
$hack=$new_date-$date;
if($hack<0) 
 {
 $a=mysql_query("UPDATE `website` SET  qc = '0' where `name_lat` = '$website' ");
 }
$quangcao='0';
}
}
else {

$quangcao=rand(1,3);
$dkm=rand(1,5);

$noidungqc='<script id="513ad6d695d16444e95d60e0" type="text/javascript"> (function(){ var myscript = document.createElement(\'script\'); myscript.type = \'text/javascript\'; myscript.charset=\'UTF-8\'; myscript.src = \'http://static.mwork.vn/scripts/infoviet/513ad6d695d16444e95d60e0.js?v=2\'; var s = document.getElementById(\'513ad6d695d16444e95d60e0\'); s.parentNode.insertBefore(myscript, s); })(); </script>
<noscript><style>#mw513ad6d695d16444e95d60e0145403146704617694987 .mw_link_mbox { color: #999999; font-size: x-small; font-family: tahoma; display: block; } #mw513ad6d695d16444e95d60e0145403146704617694987 .mw_icon { width: 26px; height: 26px; } #mw513ad6d695d16444e95d60e0145403146704617694987 .mw_desc { color: #000000; font-size: small; padding-left: 5px; } #mw513ad6d695d16444e95d60e0145403146704617694987 .mw_title { color: #008000; font-size: normal; font-family: arial; font-weight: bold; display: block; } #mw513ad6d695d16444e95d60e0145403146704617694987 .mw_icon img { width: 26px; height: 26px; } #mw513ad6d695d16444e95d60e0145403146704617694987 .mw_item { border-color: #FFDAAF; padding: 5px; border-style: solid; border-width: 1px; background: #FFFCE9; } #mw513ad6d695d16444e95d60e0145403146704617694987 .mw_item a { text-decoration: none; }</style><div id="mw513ad6d695d16444e95d60e0145403146704617694987"><div class="mw_item"> <table cellpadding="0" cellspacing="0" border="0" width="100%"> <tbody><tr> <td class="mw_icon" valign="top"><a href="http://infoviet.mbox.sh"><img src="http://static.mwork.vn/thumbs/unsafe/26x26/static.mwork.vn/data/images/avatar189.png" border="0" align="absmiddle"></a></td> <td><a style="margin-left:5px;" class="mw_title" href="http://infoviet.mbox.sh">Game Mobile Free</a></td> </tr> </tbody></table> </div></div>
</noscript>';
}
/*
-----------------------------------------------------------------
Главное меню пользователя
-----------------------------------------------------------------
*/



//Thong bao
if ($user_id){
if (!file_exists(($rootpath . 'files/users/avatar/' . $user_id . '.png')))
echo '<div class="gmenu" style="color:black">Bạn chưa có hình đại diện click <a href="../users/profile.php?act=images&amp;mod=avatar&amp;user=' . $user_id . '" style="color:blue"> <b>vào đây</b></a> để tải lên</div>';
if($he_a['tuvi']!=0){
if ($datauser['horoscope'] == 0) 
echo '<div class="rmenu">Có vẻ như bạn chưa tích hợp chức năng Tử Vi Vui của Diễn Đàn. Click <a href="/horoscope/horoscope.php">đây</a> để thiết lập!</div>';
 }    
}

/*
-----------------------------------------------------------------
Рекламный блок сайта
-----------------------------------------------------------------
*/
if (!empty($cms_ads[1])) echo '<div class="gmenu">' . $cms_ads[1] . '</div>';

/*
-----------------------------------------------------------------
Фиксация местоположений посетителей
-----------------------------------------------------------------
*/
$sql = '';
$set_karma = unserialize($set['karma']);
if ($user_id) {
// Фиксируем местоположение авторизованных
if (!$datauser['karma_off'] && $set_karma['on'] && $datauser['karma_time'] <= (time() - 86400)) {
$sql = "`karma_time` = '" . time() . "', ";
}
$movings = $datauser['movings'];
if ($datauser['lastdate'] < (time() - 300)) {
$movings = 0;
$sql .= "`sestime` = '" . time() . "',";
}
if ($datauser['place'] != $headmod) {
++$movings;
$sql .= "`place` = '$headmod',";
}
if ($datauser['browser'] != $agn)
$sql .= "`browser` = '" . mysql_real_escape_string($agn) . "',";
$totalonsite = $datauser['total_on_site'];
if ($datauser['lastdate'] > (time() - 300))
$totalonsite = $totalonsite + time() - $datauser['lastdate'];
mysql_query("UPDATE `users` SET $sql
`movings` = '$movings',
`total_on_site` = '$totalonsite',
`lastdate` = '" . time() . "'
where `website` = '$website' AND  `id` = '$user_id'
");
} else {
// Фиксируем местоположение гостей
$movings = 0;
$session = md5(core::$ip . core::$ip_via_proxy . core::$user_agent);
$req = mysql_query("SELECT * FROM `cms_sessions` where `website` = '$website' AND  `session_id` = '$session' LIMIT 1");
if (mysql_num_rows($req)) {
// Если есть в базе, то обновляем данные
$res = mysql_fetch_assoc($req);
$movings = $res['movings'];
if ($res['sestime'] < (time() - 300)) {
$movings = 0;
$sql .= "`sestime` = '" . time() . "', `movings` = '0'";
}
if ($res['place'] != $headmod) {
++$movings;
$sql .= "`place` = '$headmod',";
}
mysql_query("UPDATE `cms_sessions` SET $sql
`movings` = '$movings',
`lastdate` = '" .time()  . "'
where `website` = '$website' AND  `session_id` = '$session'
");
} else {
// Если еще небыло в базе, то добавляем запись
mysql_query("INSERT INTO `cms_sessions` SET
`session_id` = '" . $session . "',
`ip` = '" . core::$ip . "',
`ip_via_proxy` = '" . core::$ip_via_proxy . "',
`browser` = '" . mysql_real_escape_string($agn) . "',
`lastdate` = '" . time() . "',
					`website` = '$website',
`sestime` = '" . time() . "',
`place` = '$headmod'
");
}
mySQL_free_result($req);
}

/*
-----------------------------------------------------------------
Выводим сообщение о Бане
-----------------------------------------------------------------
*/
if (!empty($ban)) { echo '<div class="alarm">' . $lng['ban'] . '&#160;<a href="' . $set['homeurl'] . '/users/profile.php?act=ban">' . $lng['in_detail'] . '</a></div>';
require('end.php');
exit;

}
/*
-----------------------------------------------------------------
Ссылки на непрочитанное
-----------------------------------------------------------------
*/
if ($user_id) {
$list = array();
$new_mail = mysql_result(mysql_query("SELECT COUNT(*) FROM `privat` where `website` = '$website' AND  `user` = '$login' AND `type` = 'in' AND `chit` = 'no'"), 0);
if ($new_mail) $list[] = '<a href="' . core::$system_set['homeurl'] . '/users/pradd.php?act=in&amp;new">' . $lng['mail'] . '</a>&#160;(' . $new_mail . ')';
if ($datauser['comm_count'] > $datauser['comm_old']) $list[] = '<a href="' . core::$system_set['homeurl'] . '/users/profile.php?act=guestbook&amp;user=' . $user_id . '">' . $lng['guestbook'] . '</a> (' . ($datauser['comm_count'] - $datauser['comm_old']) . ')';
$new_album_comm = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_album_files` where `website` = '$website' AND  `user_id` = '" . core::$user_id . "' AND `unread_comments` = 1"), 0);
if($new_album_comm) $list[] = '<a href="' . core::$system_set['homeurl'] . '/users/album.php?act=top&amp;mod=my_new_comm">' . $lng['albums_comments'] . '</a>';

if (!empty($list)) 
{
if ($headmod != "friends/$user_id") {
	$frend = mysql_query("SELECT * FROM `friends` WHERE `website`='$website' and`user_id` = '$user_id' AND `option`");
	$frends = mysql_fetch_assoc($frend);
		$new_frends = mysql_result(mysql_query("SELECT COUNT(*) FROM `friends` WHERE `website`='$website' and `user_id` = '$user_id' AND `option`IN(1, 2)"), 0);
	if ($new_frends) $list[] = '<a href="' . $set['homeurl'] . '/users/friends.php?act=new' . ($frends['option'] == '2' ? '&amp;mod=delete' : '') . '">Yêu cầu kết bạn</a> (+ ' . $new_frends . ')';
	mySQL_free_result($frend);
	}
echo '<div class="tmn">' . $lng['unread'] . ': ' . functions::display_menu($list, ', ') . '</div>';
}
}
///Timkiem

/////////////


if ($set['kiemduyet'] !=0) {
if ($headmod != "kiemduyet" && $rights > 3) {
    $kiemduyet = mysql_result(mysql_query("SELECT COUNT(*) FROM `forum` WHERE `website`='$website' AND `type` = 't' and kedit='0' AND `kiemduyet` != '1' AND `close`!='1'"), 0);
    if ($kiemduyet > 0) {
        echo "<div class=\"rmenu\" style='text-align: center'><a href='../users/kiemduyet.php'><b><font color='red'>Có $kiemduyet chủ đề đang chờ kiểm duyệt</font></b></a></div>";
  }
}
}
// Cắt chuỗi
$chuoi = explode("||", $set['caidat']);
$chuoimaunick= $chuoi[0];
$chuoimaunick= explode(",", $chuoimaunick);
$chuoichucvu= $chuoi[1];
$chuoichucvu= explode(",", $chuoichucvu);
$chuoibot= $chuoi[2];
$chuoibot= explode(",", $chuoibot);
$chuoigiacap= $chuoi[3];
$chuoigiacap= explode(",", $chuoigiacap);
$chuoicapbac= $chuoi[4];
$chuoicapbac= explode(",", $chuoicapbac);




// Dừng cắt

if($quangcao=='1' or $quangcao=='2' )
echo $noidungqc;
?>