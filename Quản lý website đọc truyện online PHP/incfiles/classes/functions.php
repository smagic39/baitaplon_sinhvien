<?php
if(!$website)
require(''.$_SERVER['DOCUMENT_ROOT'].'/incfiles/website.php');/**
* @package     JohnCMS
* @link        http://johncms.com
* @copyright   Copyright (C) 2008-2011 JohnCMS Community
* @license     LICENSE.txt (see attached file)
* @version     VERSION.txt (see attached file)
* @author      http://johncms.com/about
*/

defined('_IN_JOHNCMS') or die('Restricted access');

class functions extends core
{
/*
-----------------------------------------------------------------
Антифлуд
-----------------------------------------------------------------
Режимы работы:
1 - Адаптивный
2 - День / Ночь
3 - День
4 - Ночь
-----------------------------------------------------------------
*/
public static function create_keywords($story, $separator = ', ', $keyword_count = 20)
 { global $set; 
 $fastquotes = array ("\x22", "\x60", "\t", "\n", "\r", '"', "\\", '\r', '\n', "-", "{", "}", "[", "]" );
 $story1 = str_replace( $fastquotes, " ", $story ); 
 $story2 = preg_replace("/[^\w\x7F-\xFF\s]/", " ", $story1); 
 $story = preg_replace("/ {,1}/", " ", $story2); 
 $story3 = str_replace( $fastquotes, '', trim( strip_tags( str_replace( '<br />', ' ', stripslashes( $story1 ) ) ) ) ); 
 $story3 = preg_replace("/ {,1}/", " ", $story3);
 $set['meta_desc'] = mb_substr( $story3, 0, 190, 'UTF-8' ); 
 $arr = explode( " ", $story ); foreach ( $arr as $word ) { if( mb_strlen( $word, 'UTF-8') > 4 ) $newarr[] = $word; } 
 $arr = array_count_values( $newarr ); arsort( $arr ); 
 $arr = array_keys( $arr ); $total = count( $arr ); 
 $offset = 0; 
 $arr = array_slice( $arr, $offset, $keyword_count ); 
 $set['meta_key'] = implode( $separator, $arr ); }
public static function antiflood()
{
if(!$website)
require(''.$_SERVER['DOCUMENT_ROOT'].'/incfiles/website.php');$default = array(
'mode' => 2,
'day' => 10,
'night' => 30,
'dayfrom' => 10,
'dayto' => 22
);
$af = isset(self::$system_set['antiflood']) ? unserialize(self::$system_set['antiflood']) : $default;
switch ($af['mode']) {
case 1:
// Адаптивный режим
$adm = mysql_result(mysql_query("SELECT COUNT(*) FROM `users` where `website` = '$website' AND  `rights` > 0 AND `lastdate` > " . (time() - 300)), 0);
$limit = $adm > 0 ? $af['day'] : $af['night'];
break;
case 3:
// День
$limit = $af['day'];
break;
case 4:
// Ночь
$limit = $af['night'];
break;
default:
// По умолчанию день / ночь
$c_time = date('G', time());
$limit = $c_time > $af['day'] && $c_time < $af['night'] ? $af['day'] : $af['night'];
}
if (self::$user_rights > 0)
$limit = 4; // Для Администрации задаем лимит в 4 секунды
$flood = self::$user_data['lastpost'] + $limit - time();
if ($flood > 0)
return $flood;
else
return false;
}

/*
-----------------------------------------------------------------
Маскировка ссылок в тексте
-----------------------------------------------------------------
*/
public static function antilink($var)
{
$var = preg_replace('~\\[url=(https?://.+?)\\](.+?)\\[/url\\]|(https?://(www.)?[0-9a-z\.-]+\.[0-9a-z]{2,6}[0-9a-zA-Z/\?\.\~&amp;_=/%-:#]*)~', '###', $var);
$replace = array(
'.ru' => '***',
'.com' => '***',
'.biz' => '***',
'.cn' => '***',
'.in' => '***',
'.net' => '***',
'.org' => '***',
'.info' => '***',
'.mobi' => '***',
'.wen' => '***',
'.kmx' => '***',
'.h2m' => '***'
);
return strtr($var, $replace);
}
//////////////////

public static function forum($var,$vitri)
{
global $res_u,$is_mobile,$set,$colornick,$vip1,$res,$start,$i,$user_rights;
if(!$is_mobile) {
$var=preg_replace("/\[web\](.+?)\[\/web\]/is", "\\1", $var);
$var=preg_replace("/\[wap\](.+?)\[\/wap\]/is", "", $var);

}
else {
$var=preg_replace("/\[web\](.+?)\[\/web\]/is", "", $var);
$var=preg_replace("/\[wap\](.+?)\[\/wap\]/is", "\\1", $var);

}
if($vitri==2)
{
$var=preg_replace("/\[duoi\](.+?)\[\/duoi\]/is", "\\1", $var);
$var=preg_replace("/\[congcu\](.+?)\[\/congcu\]/is", "", $var);
$var=preg_replace("/\[than\](.+?)\[\/than\]/is", "", $var);
}
elseif($vitri==1) {
$var=preg_replace("/\[than\](.+?)\[\/than\]/is", "\\1", $var);
$var=preg_replace("/\[duoi\](.+?)\[\/duoi\]/is", "", $var);
$var=preg_replace("/\[congcu\](.+?)\[\/congcu\]/is", "", $var);
}
else{
$var=preg_replace("/\[than\](.+?)\[\/than\]/is", "", $var);
$var=preg_replace("/\[congcu\](.+?)\[\/congcu\]/is", "\\1", $var);
$var=preg_replace("/\[duoi\](.+?)\[\/duoi\]/is", "", $var);
}
/// Chưa Login 


$replace = array(
'[forum:avatar]' => functions::avatar(),
'[forum:status]' => functions::status(),
'[forum:on]' => functions::onoff(),
'[forum:like]' => $res_u['thank_duoc'],
'[forum:tien]' => $res_u['balans'],
'[forum:post]' => $res_u['postforum'],
'[forum:capbac]' => functions::capbac2(),
'[forum:time]' => functions::display_date($res['time']),
'[forum:bai]' => '#<a href="index.php?act=post&amp;id=' . $res['id']. '?#' .( $start + $i ). '"><b>' .( $start + $i ). '</b></a>',
'[forum:nick]' => '<a href="../users/profile.php?user=' . $res['user_id'] . '"><font color="#' . $colornick['colornick'] . '"><b>' . $res['from'] . '</b>' .$vip1['vip1']. '</font></a>',
'[forum:point]' => ceil($res_u['postforum']/50 + $res_u['thank_duoc']/20 ),
'[forum:chuky]' => notags($res['chuki']),
'[forum:chucvu]' => $user_rights[$res['rights']],
'[forum:ketban]' => functions::ketban(),
'[forum:tudo]' => functions::tudo(),
'[forum:giaicap]' => forum::giaicap(),
'[forum:lienquan]' => func::lienquan(),

);

return strtr($var, $replace);
}





public static function func($var)
{
global $website,$is_mobile,$start,$kmess,$rights,$login,$act,$headmod,$datauser,$user_id,$balans,$set,$set_user,$realtime,$user_id,$admp,$setdh;
$H=date("H")+7;
//// Đã Login
if(!$is_mobile) {
$var=preg_replace("/\[web\](.+?)\[\/web\]/is", "\\1", $var);
$var=preg_replace("/\[wap\](.+?)\[\/wap\]/is", "", $var);

}
else {
$var=preg_replace("/\[web\](.+?)\[\/web\]/is", "", $var);
$var=preg_replace("/\[wap\](.+?)\[\/wap\]/is", "\\1", $var);

}
if($user_id)
{
$var=preg_replace("/\[login\](.+?)\[\/login\]/is", "\\1", $var);

$var=preg_replace("/\[khach\](.+?)\[\/khach\]/is", "", $var);

if ($rights >=1){
$var=preg_replace("/\[bqt\](.+?)\[\/bqt\]/is", "\\1", $var);

}
else {
$var=preg_replace("/\[bqt\](.+?)\[\/bqt\]/is", "", $var);}
/// Chưa Login 

} else {

$login2='<a href="' . $set['homeurl'] . '/login.php">Đăng nhập</a>';
$reg='<a href="' . $set['homeurl'] . '/registration.php">Đăng ký</a>';
$var=preg_replace("/\[login\](.+?)\[\/login\]/is", "", $var);
$var=preg_replace("/\[khach\](.+?)\[\/khach\]/is", "\\1", $var);
}
$chat = explode('[chatbox]',$var);
$chat = explode('[/chatbox]',$chat[1]);
$var=preg_replace("/\[chatbox\](.+?)\[\/chatbox\]/is", "".func::chatbox($chat[0])."", $var);
unset($chat);
$topx = explode('[topx]',$var);
$topx = explode('[/topx]',$topx[1]);
$var=preg_replace("/\[topx\](.+?)\[\/topx\]/is", "".func::topx($topx[0])."", $var);
unset($topx);
$replace = array(
'[func:date]' => $H.date(":i"),
'[func:login]' => $login2,
'[func:reg]' => $reg,
'[func:topx]' => functions::topx(),
'[func:tintuc]' => functions::tintuc(),
'[func:anh]' => functions::anh(),
'[func:tracnghiem]' => functions::tracngiem(),
'[func:member]' => functions::member(),
'[func:thongke]' => functions::thongke(),
'[func:blog]' => functions::blog(),
'[func:chuyenmuc]' => functions::chuyenmuc(),
'[func:chuadoc]' => counters::forum_new(1),
'[func:nick]' => $login,
'[func:tien]' => ''.$datauser['balans'].'<b> '.$set['tiente'].'</b>',
'[func:avt]' => functions::thanhvien('avt'),
'[func:chatbox]' => functions::chatbox(),
'[func:ds]' => functions::ds(),
'[func:quatang]' => functions::quatang(),
'[func:capbac]' => functions::capbac(),
'[func:topforum]' => functions::get_top(),
'[func:memol]' => forum::member('memol'),
'[func:khachonl]' => forum::member('khachonl'),
'[func:tvm]' => forum::member('tvm'),
'[func:somem]' => forum::member('somem'),
'[func:tinmoi]' => forum::tincapnhat(),
'[func:tuvi]' => forum::tuvi(),
'[func:thongke_bv]' => forum::thongke('baiviet'),
'[func:thongke_cd]' => forum::thongke('chude'),
'[func:thongke_tv]' => forum::thongke('tv'),
'[func:love]' => forum::love(),
'[func:thongbao]' => forum::event(),
// ''.$chat.''=> ,

); 
 
return strtr($var, $replace);
}
public static function quatang() {
 global $is_mobile,$website,$home,$set,$chuoimaunick;
 $mua=mysql_query("SELECT quatang FROM website WHERE name_lat='$website'")or die(mysql_error());
$mua2=mysql_fetch_array($mua);
if($mua2['quatang']!=0){


if (!$is_mobile) {
$var.= '<div class="mainblok"><div class="phdr"><a href="'.$home.'/quatang"><b>Quà tặng âm nhạc</b></a></div><table width="100%" cellpadding="0" cellspacing="0"><tr><td align="left">';
$quatang = mysql_query("SELECT * FROM `quatang` WHERE `website` = '$website' ORDER BY `id` DESC LIMIT 1 ")or die(mysql_error());
$truyvan = mysql_fetch_array($quatang);
mySQL_free_result($quatang);
$timnguoigui = mysql_query("SELECT `name`, `rights` FROM `users` WHERE `website` = '$website' AND `id` = '".$truyvan['user_id_gui']."'");
$truyvan2 = mysql_fetch_array($timnguoigui);
mySQL_free_result($timnguoigui);
   if($set['caidat']==null){ if ($truyvan2['rights'] == 0) $maunick1 = '000000';
    if ($truyvan2['rights'] == 1) $maunick1 = '0000ff';
    if ($truyvan2['rights'] == 2) $maunick1 = '0000ff';
    if ($truyvan2['rights'] == 3) $maunick1 = '0000ff';
    if ($truyvan2['rights'] == 4) $maunick1 = '0000ff';
    if ($truyvan2['rights'] == 5) $maunick1 = '0000ff';
    if ($truyvan2['rights'] == 6) $maunick1 = '009900';
    if ($truyvan2['rights'] == 7) $maunick1 = 'ff00ff';
    if ($truyvan2['rights'] == 9) $maunick1 = 'ff0000';
} else {

   if ($truyvan2['rights'] == 0) $maunick1 = ''.$chuoimaunick[4].'';
    if ($truyvan2['rights'] == 1) $maunick1 = ''.$chuoimaunick[3].'';
    if ($truyvan2['rights'] == 2) $maunick1 = ''.$chuoimaunick[3].'';
    if ($truyvan2['rights'] == 3) $maunick1 = ''.$chuoimaunick[3].'';
    if ($truyvan2['rights'] == 4) $maunick1 = ''.$chuoimaunick[3].'';
    if ($truyvan2['rights'] == 5) $maunick1 = ''.$chuoimaunick[3].'';
    if ($truyvan2['rights'] == 6) $maunick1 = ''.$chuoimaunick[2].'';
    if ($truyvan2['rights'] == 7) $maunick1 = ''.$chuoimaunick[1].'';
    if ($truyvan2['rights'] == 9) $maunick1 = ''.$chuoimaunick[0].'';
}
$var.= '<img src="/quatang/2.png" alt="gui tang" width="25" height="25"/> <span style="color:#'.$maunick1.'">'.$truyvan2['name'].'</span> gửi tặng bạn '.$truyvan['nguoi_nhan'].'<br/>';
$var.= '<img src="/quatang/1.png" alt="bai hat" width="25" height="25"/> Bài hát: '.$truyvan['baihat'].'<br/>';
$var.= '<img src="/quatang/4.png" alt="loi nhan" width="25" height="25"/> Lời nhắn: <span style="color:purple">'.$truyvan['text'].'</span><br/>';

$var.= '<img src="/quatang/3.png" alt="tai bai hat" width="25" height="25"/><a href="' .$truyvan['url']. '" target="_blank" >Tải bài hát</a>';
                  
$var.= '</td><td align="right"><img src="/quatang/quatang.png" alt="qua tang am nhac"/><br/>';
if ($truyvan['code']) {
$var.= '<embed src="http://static.mp3.zing.vn/skins/gentle/flash/mp3player.swf?xmlURL=http://mp3.zing.vn/play/?pid='.$truyvan['code'].'||4&amp;songID=0&amp;_mp3=&amp;autoplay=false&amp;wmode=transparent" type="application/x-shockwave-flash" width="300"  height="80" quality="high" wmode="transparent"></embed>';
}
$var.= '</td></tr></table></div>';
}
else {
$var.= '<div class="mainblok"><div class="phdr"><a href="/quatang"><b>Quà tặng âm nhạc</b></a></div>';
$quatang = mysql_query("SELECT * FROM `quatang` WHERE `website` = '$website' ORDER BY `id` DESC LIMIT 1") or die(mysql_error());
$truyvan = mysql_fetch_array($quatang);
mySQL_free_result($quatang);
$timnguoigui = mysql_query("SELECT `name`, `rights` FROM `users` WHERE `website` = '$website' AND `id` = '".$truyvan['user_id_gui']."'");
$truyvan2 = mysql_fetch_array($timnguoigui);
mySQL_free_result($timnguoigui);
   if($set['caidat']==null){ if ($truyvan2['rights'] == 0) $maunick1 = '000000';
    if ($truyvan2['rights'] == 1) $maunick1 = '0000ff';
    if ($truyvan2['rights'] == 2) $maunick1 = '0000ff';
    if ($truyvan2['rights'] == 3) $maunick1 = '0000ff';
    if ($truyvan2['rights'] == 4) $maunick1 = '0000ff';
    if ($truyvan2['rights'] == 5) $maunick1 = '0000ff';
    if ($truyvan2['rights'] == 6) $maunick1 = '009900';
    if ($truyvan2['rights'] == 7) $maunick1 = 'ff00ff';
    if ($truyvan2['rights'] == 9) $maunick1 = 'ff0000';
} else {

   if ($truyvan2['rights'] == 0) $maunick1 = ''.$chuoimaunick[4].'';
    if ($truyvan2['rights'] == 1) $maunick1 = ''.$chuoimaunick[3].'';
    if ($truyvan2['rights'] == 2) $maunick1 = ''.$chuoimaunick[3].'';
    if ($truyvan2['rights'] == 3) $maunick1 = ''.$chuoimaunick[3].'';
    if ($truyvan2['rights'] == 4) $maunick1 = ''.$chuoimaunick[3].'';
    if ($truyvan2['rights'] == 5) $maunick1 = ''.$chuoimaunick[3].'';
    if ($truyvan2['rights'] == 6) $maunick1 = ''.$chuoimaunick[2].'';
    if ($truyvan2['rights'] == 7) $maunick1 = ''.$chuoimaunick[1].'';
    if ($truyvan2['rights'] == 9) $maunick1 = ''.$chuoimaunick[0].'';
}
$var.= '<span style="color:#'.$maunick1.'">'.$truyvan2['name'].'</span> gửi tặng bạn '.$truyvan['nguoi_nhan'].'<br/>';
$var.= 'Bài hát: '.$truyvan['baihat'].'<br/>';
$var.= 'Lời nhắn: <span style="color:purple">'.$truyvan['text'].'</span><br/>';
$var.= '<a href="' .$truyvan['url']. '" target="_blank">Tải bài hát</a>';
$var.= '</div>';
}
}
else { 
$var='Mod hết hạn hoặc chưa đăng ký';}

return $var;
}
public static function get_top() {
    global $lng,$website,$order;
	if (!$order) {
	$order='postforum'; }
	if($order!='postguest' and $order!='komm' and $order !='postguest' and $order !='thank_duoc' and $order != 'balans')
	{
	$order='postforum';
	}
    $req = mysql_query("SELECT * FROM `users` where `website` = '$website' AND  `$order` > 0 ORDER BY `$order` DESC LIMIT 5");
    if (mysql_num_rows($req)) {
        $out = '';
		$out .='<div class="phdr">Top Member</div>
		<div class="topmenu"> 
<select name="url" onChange="window.open(this.options [this.selectedIndex].value,\'_top\')">
<option value="?topx=postforum">Top Forum</option>
<option value="?topx=postguest">Top Chém</option>
<option value="?topx=postkomm">Top Bình luận</option>
<option value="?topx=thank_duoc">Top Được Thank</option>
<option value="?topx=balans">Top Đại Gia</option>
</select>
 </div>
		';
        $i = 0;
        while ($res = mysql_fetch_assoc($req)) {
            $out .= $i % 2 ? '<div class="list2">' : '<div class="list1">';
            $out .= functions::display_user2($res, array ('header' => ('<b><font style="float:right;">' . $res[$order]) . '</font></b>')) . '</div>';
            ++$i;
        }
		mySQL_free_result($req);
        return $out;
    } else {
        return '<div class="menu"><p>' . $lng['list_empty'] . '</p></div>';
    }
}

public static function avatar(){
global $res,$set_user;
if ($set_user['avatar']) {
 $var.='<img class="avatar" src="http://up.infoviet.net/avatar/' . $res['user_id'] . '.png" width="32" height="32" onerror="this.src=\'http://img.infoviet.net/images/empty.png\'"; width="32" height="32" alt="' . $user_id . '" align="top" "/>&#160;';
     
}
return $var;
}
public static function status(){
global $res,$set_user;
if (!empty($res['status']))
$var.= '<img src="http://img.infoviet.net/theme/' . $set_user['skin'] . '/images/label.png" alt="" align="middle"/>&#160;' . $res['status'] . '';

return $var;
}
public static function tudo(){
global $res;
$var.='<a href="../shop/tudo.php?id=' .$res['user_id'] . '">Tủ Đồ </a>';
return $var;
}

//////////////////
public static function ketban(){
global $res,$website,$user_id,$home,$user_u,$he_a;
if($he_a['ketban']!=0) {
$req_u = mysql_query("SELECT `id` FROM `users` WHERE `website` = '$website' AND `id` = '$user_u' LIMIT 1");
$res_u = mysql_fetch_array($req_u);
mySQL_free_result($req_u);
$friends = mysql_result(mysql_query("SELECT COUNT(*) FROM `friends` WHERE `website`='$website' and`friends`='1' AND `friend_id`='" . $res_u['id'] . "' AND `user_id`='$user_id' "), 0);  
	if (!$friends){ 
	$var.= '<a href="'.$home.'/users/friends.php?act=add&amp;user=' . $res_u['id'] . '">Kết Bạn</a>';
	}
	else {
	$var.= '<a href="'.$home.'/users/friends.php?act=del&amp;user=' . $res_u['id'] . '">Xóa Bạn Bè</a>';
	}
} else {
$var='mod chưa đăng ký hoặc đã hết hạn';
}
return $var;
}
///////////
public static function onoff(){
global $res;
if ($res['sex'])
$var.= (time() > $res['lastdate'] + 300 ? '<font color="red"> • </font> ' : '<font color="green"> • </font> ');
else
$var.= '<img src="http://img.infoviet.net/images/del.png" width="12" height="12" align="middle" alt=""/>&#160;';
return $var;
}
public static function capbac(){
global $website,$set,$user_id,$user_u,$res,$parent,$type1,$chuoicapbac;
if(!$user_u)
$req_u2 = mysql_query("SELECT `postforum` FROM `users` WHERE `website` = '$website' AND `id` = '$user_id' LIMIT 1");
else
$req_u2 = mysql_query("SELECT `postforum` FROM `users` WHERE `website` = '$website' AND `id` = '$user_u' LIMIT 1");
$res_u = mysql_fetch_array($req_u2);
mySQL_free_result($req_u2);
if ($set['danhhieu']==0){
if ($res_u['postforum'] != 0) {
$chucdanh= $res_u['postforum']/5;
if ($chucdanh<= 1)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/01.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 3)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/02.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 6)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/03.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 12)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/04.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 20)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/05.gif" width="32" height="32" align="middle"/> ';
elseif ($chucdanh<= 30)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/06.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 40)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/07.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 50)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/08.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 60)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/09.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 70)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/10.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 85)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/11.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 100)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/12.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 115)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/13.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 130)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/14.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 145)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/15.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 160)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/16.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 175)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/17.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 190)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/18.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 205)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/19.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 220)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/20.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 240)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/21.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 260)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/22.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 280)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/23.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 300)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/24.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 320)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/25.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 340)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/26.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 360)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/27.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 380)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/28.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 400)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/29.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 430)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/30.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 460)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/31.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 490)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/32.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 510)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/33.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 540)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/34.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 570)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/35.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 600)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/36.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 630)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/37.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 660)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/38.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 690)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/39.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 760)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/40.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 800)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/41.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 840)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/42.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 880)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/43.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 920)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/44.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 960)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/45.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1000)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/46.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1040)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/47.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1080)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/48.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1200)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/49.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1300)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/50.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1400)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/51.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1500)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/52.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1600)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/53.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1700)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/54.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1800)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/55.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1900)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/56.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 2000)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/57.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 2200)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/58.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 2500)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/59.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 3000)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/60.png" width="32" height="32" align="middle"/>';
}
}
if ($set['danhhieu']==1){
$exp = $res_u['postforum']*50;
if ($exp >= 0 && $exp <3000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/gacon.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 3000 && $exp <5250)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/buago.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 5250 && $exp <8250)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/buagodoi.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 8250 && $exp <12750)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/buada.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 12750 && $exp <19500)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/buadadoi.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 19500 && $exp <31500)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riusat.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 31500 && $exp <46500)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riusatdoi.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 46500 && $exp <70500)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riubac.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 70500 && $exp <102000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riubacdoi.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 102000 && $exp <165000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuvang.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 165000 && $exp <240000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuvangdoi.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 240000 && $exp <330000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuchiensat.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 330000 && $exp <435000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuchiensatcham.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 435000 && $exp <585000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuchienbac.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 585000 && $exp <765000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuchienbaccham.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 765000 && $exp <1140000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuchienvang.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 1140000 && $exp <1650000)
{

$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuchienvangcham.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 1650000)
{

$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/vip.gif" width="25" height="15" />';
}
}

if ($set['danhhieu']==2){
if ($res_u['postforum'] != 0) {
$chucdanh= $res_u['postforum']/5;
if ($chucdanh<= 1)
$chucdanh= '<img src="'.$chuoicapbac[0].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 3)
$chucdanh= '<img src="'.$chuoicapbac[1].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 6)
$chucdanh= '<img src="'.$chuoicapbac[2].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 12)
$chucdanh= '<img src="'.$chuoicapbac[3].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 20)
$chucdanh= '<img src="'.$chuoicapbac[4].'" width="32" height="32" align="middle"/> ';
elseif ($chucdanh<= 30)
$chucdanh= '<img src="'.$chuoicapbac[5].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 40)
$chucdanh= '<img src="'.$chuoicapbac[6].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 50)
$chucdanh= '<img src="'.$chuoicapbac[7].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 60)
$chucdanh= '<img src="'.$chuoicapbac[8].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 70)
$chucdanh= '<img src="'.$chuoicapbac[9].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 85)
$chucdanh= '<img src="'.$chuoicapbac[10].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 100)
$chucdanh= '<img src="'.$chuoicapbac[11].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 115)
$chucdanh= '<img src="'.$chuoicapbac[12].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 130)
$chucdanh= '<img src="'.$chuoicapbac[13].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 145)
$chucdanh= '<img src="'.$chuoicapbac[14].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 160)
$chucdanh= '<img src="'.$chuoicapbac[15].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 175)
$chucdanh= '<img src="'.$chuoicapbac[16].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 190)
$chucdanh= '<img src="'.$chuoicapbac[17].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 205)
$chucdanh= '<img src="'.$chuoicapbac[18].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 220)
$chucdanh= '<img src="'.$chuoicapbac[19].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 240)
$chucdanh= '<img src="'.$chuoicapbac[20].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 260)
$chucdanh= '<img src="'.$chuoicapbac[21].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 280)
$chucdanh= '<img src="'.$chuoicapbac[22].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 300)
$chucdanh= '<img src="'.$chuoicapbac[23].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 320)
$chucdanh= '<img src="'.$chuoicapbac[24].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 340)
$chucdanh= '<img src="'.$chuoicapbac[25].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 360)
$chucdanh= '<img src="'.$chuoicapbac[26].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 380)
$chucdanh= '<img src="'.$chuoicapbac[27].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 400)
$chucdanh= '<img src="'.$chuoicapbac[28].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 430)
$chucdanh= '<img src="'.$chuoicapbac[29].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 460)
$chucdanh= '<img src="'.$chuoicapbac[30].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 490)
$chucdanh= '<img src="'.$chuoicapbac[31].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 510)
$chucdanh= '<img src="'.$chuoicapbac[32].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 540)
$chucdanh= '<img src="'.$chuoicapbac[33].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 570)
$chucdanh= '<img src="'.$chuoicapbac[34].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 600)
$chucdanh= '<img src="'.$chuoicapbac[35].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 630)
$chucdanh= '<img src="'.$chuoicapbac[36].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 660)
$chucdanh= '<img src="'.$chuoicapbac[37].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 690)
$chucdanh= '<img src="'.$chuoicapbac[38].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 760)
$chucdanh= '<img src="'.$chuoicapbac[39].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 800)
$chucdanh= '<img src="'.$chuoicapbac[40].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 840)
$chucdanh= '<img src="'.$chuoicapbac[41].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 880)
$chucdanh= '<img src="'.$chuoicapbac[42].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 920)
$chucdanh= '<img src="'.$chuoicapbac[43].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 960)
$chucdanh= '<img src="'.$chuoicapbac[44].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1000)
$chucdanh= '<img src="'.$chuoicapbac[45].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1040)
$chucdanh= '<img src="'.$chuoicapbac[46].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1080)
$chucdanh= '<img src="'.$chuoicapbac[47].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1200)
$chucdanh= '<img src="'.$chuoicapbac[48].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1300)
$chucdanh= '<img src="'.$chuoicapbac[49].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1400)
$chucdanh= '<img src="'.$chuoicapbac[50].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1500)
$chucdanh= '<img src="'.$chuoicapbac[51].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1600)
$chucdanh= '<img src="'.$chuoicapbac[52].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1700)
$chucdanh= '<img src="'.$chuoicapbac[53].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1800)
$chucdanh= '<img src="'.$chuoicapbac[54].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1900)
$chucdanh= '<img src="'.$chuoicapbac[55].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 2000)
$chucdanh= '<img src="'.$chuoicapbac[56].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 2200)
$chucdanh= '<img src="'.$chuoicapbac[57].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 2500)
$chucdanh= '<img src="'.$chuoicapbac[58].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 3000)
$chucdanh= '<img src="'.$chuoicapbac[59].'" width="32" height="32" align="middle"/>';
}
}
return $chucdanh;
}
public static function capbac2(){
global $website,$set,$user_id,$user_u,$res,$parent,$type1,$chuoicapbac;
if(!$user_u)
$req_u2 = mysql_query("SELECT postforum FROM `users` WHERE `website` = '$website' AND `id` = '$user_id' LIMIT 1");
else
$req_u2 = mysql_query("SELECT postforum FROM `users` WHERE `website` = '$website' AND `id` = '$user_u' LIMIT 1");
$res_u = mysql_fetch_array($req_u2);
mySQL_free_result($req_u2);
if ($set['danhhieu']==2){
if ($res_u['postforum'] != 0) {
$chucdanh= $res_u['postforum']/5;
if ($chucdanh<= 1)
$chucdanh= '<img src="'.$chuoicapbac[0].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 3)
$chucdanh= '<img src="'.$chuoicapbac[1].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 6)
$chucdanh= '<img src="'.$chuoicapbac[2].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 12)
$chucdanh= '<img src="'.$chuoicapbac[3].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 20)
$chucdanh= '<img src="'.$chuoicapbac[4].'" width="32" height="32" align="middle"/> ';
elseif ($chucdanh<= 30)
$chucdanh= '<img src="'.$chuoicapbac[5].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 40)
$chucdanh= '<img src="'.$chuoicapbac[6].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 50)
$chucdanh= '<img src="'.$chuoicapbac[7].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 60)
$chucdanh= '<img src="'.$chuoicapbac[8].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 70)
$chucdanh= '<img src="'.$chuoicapbac[9].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 85)
$chucdanh= '<img src="'.$chuoicapbac[10].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 100)
$chucdanh= '<img src="'.$chuoicapbac[11].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 115)
$chucdanh= '<img src="'.$chuoicapbac[12].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 130)
$chucdanh= '<img src="'.$chuoicapbac[13].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 145)
$chucdanh= '<img src="'.$chuoicapbac[14].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 160)
$chucdanh= '<img src="'.$chuoicapbac[15].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 175)
$chucdanh= '<img src="'.$chuoicapbac[16].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 190)
$chucdanh= '<img src="'.$chuoicapbac[17].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 205)
$chucdanh= '<img src="'.$chuoicapbac[18].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 220)
$chucdanh= '<img src="'.$chuoicapbac[19].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 240)
$chucdanh= '<img src="'.$chuoicapbac[20].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 260)
$chucdanh= '<img src="'.$chuoicapbac[21].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 280)
$chucdanh= '<img src="'.$chuoicapbac[22].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 300)
$chucdanh= '<img src="'.$chuoicapbac[23].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 320)
$chucdanh= '<img src="'.$chuoicapbac[24].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 340)
$chucdanh= '<img src="'.$chuoicapbac[25].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 360)
$chucdanh= '<img src="'.$chuoicapbac[26].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 380)
$chucdanh= '<img src="'.$chuoicapbac[27].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 400)
$chucdanh= '<img src="'.$chuoicapbac[28].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 430)
$chucdanh= '<img src="'.$chuoicapbac[29].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 460)
$chucdanh= '<img src="'.$chuoicapbac[30].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 490)
$chucdanh= '<img src="'.$chuoicapbac[31].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 510)
$chucdanh= '<img src="'.$chuoicapbac[32].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 540)
$chucdanh= '<img src="'.$chuoicapbac[33].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 570)
$chucdanh= '<img src="'.$chuoicapbac[34].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 600)
$chucdanh= '<img src="'.$chuoicapbac[35].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 630)
$chucdanh= '<img src="'.$chuoicapbac[36].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 660)
$chucdanh= '<img src="'.$chuoicapbac[37].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 690)
$chucdanh= '<img src="'.$chuoicapbac[38].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 760)
$chucdanh= '<img src="'.$chuoicapbac[39].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 800)
$chucdanh= '<img src="'.$chuoicapbac[40].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 840)
$chucdanh= '<img src="'.$chuoicapbac[41].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 880)
$chucdanh= '<img src="'.$chuoicapbac[42].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 920)
$chucdanh= '<img src="'.$chuoicapbac[43].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 960)
$chucdanh= '<img src="'.$chuoicapbac[44].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1000)
$chucdanh= '<img src="'.$chuoicapbac[45].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1040)
$chucdanh= '<img src="'.$chuoicapbac[46].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1080)
$chucdanh= '<img src="'.$chuoicapbac[47].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1200)
$chucdanh= '<img src="'.$chuoicapbac[48].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1300)
$chucdanh= '<img src="'.$chuoicapbac[49].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1400)
$chucdanh= '<img src="'.$chuoicapbac[50].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1500)
$chucdanh= '<img src="'.$chuoicapbac[51].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1600)
$chucdanh= '<img src="'.$chuoicapbac[52].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1700)
$chucdanh= '<img src="'.$chuoicapbac[53].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1800)
$chucdanh= '<img src="'.$chuoicapbac[54].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1900)
$chucdanh= '<img src="'.$chuoicapbac[55].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 2000)
$chucdanh= '<img src="'.$chuoicapbac[56].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 2200)
$chucdanh= '<img src="'.$chuoicapbac[57].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 2500)
$chucdanh= '<img src="'.$chuoicapbac[58].'" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 3000)
$chucdanh= '<img src="'.$chuoicapbac[59].'" width="32" height="32" align="middle"/>';
}
}
if ($set['danhhieu']==0){
if ($res_u['postforum'] != 0) {
$chucdanh= $res_u['postforum']/5;
if ($chucdanh<= 1)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/01.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 3)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/02.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 6)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/03.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 12)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/04.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 20)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/05.gif" width="32" height="32" align="middle"/> ';
elseif ($chucdanh<= 30)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/06.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 40)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/07.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 50)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/08.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 60)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/09.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 70)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/10.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 85)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/11.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 100)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/12.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 115)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/13.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 130)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/14.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 145)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/15.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 160)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/16.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 175)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/17.gif" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 190)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/18.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 205)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/19.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 220)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/20.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 240)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/21.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 260)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/22.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 280)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/23.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 300)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/24.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 320)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/25.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 340)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/26.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 360)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/27.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 380)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/28.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 400)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/29.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 430)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/30.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 460)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/31.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 490)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/32.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 510)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/33.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 540)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/34.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 570)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/35.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 600)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/36.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 630)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/37.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 660)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/38.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 690)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/39.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 760)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/40.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 800)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/41.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 840)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/42.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 880)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/43.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 920)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/44.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 960)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/45.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1000)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/46.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1040)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/47.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1080)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/48.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1200)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/49.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1300)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/50.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1400)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/51.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1500)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/52.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1600)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/53.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1700)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/54.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1800)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/55.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 1900)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/56.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 2000)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/57.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 2200)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/58.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 2500)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/59.png" width="32" height="32" align="middle"/>';
elseif ($chucdanh<= 3000)
$chucdanh= '<img src="http://img.infoviet.net/images/forum/gunny/60.png" width="32" height="32" align="middle"/>';
}
}
if ($set['danhhieu']==1){
$exp = $res_u['postforum']*50;
if ($exp >= 0 && $exp <3000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/gacon.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 3000 && $exp <5250)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/buago.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 5250 && $exp <8250)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/buagodoi.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 8250 && $exp <12750)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/buada.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 12750 && $exp <19500)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/buadadoi.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 19500 && $exp <31500)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riusat.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 31500 && $exp <46500)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riusatdoi.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 46500 && $exp <70500)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riubac.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 70500 && $exp <102000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riubacdoi.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 102000 && $exp <165000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuvang.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 165000 && $exp <240000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuvangdoi.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 240000 && $exp <330000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuchiensat.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 330000 && $exp <435000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuchiensatcham.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 435000 && $exp <585000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuchienbac.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 585000 && $exp <765000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuchienbaccham.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 765000 && $exp <1140000)
{
$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuchienvang.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 1140000 && $exp <1650000)
{

$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/riuchienvangcham.gif" width="25" height="15" align="middle"/>';
}
if ($exp >= 1650000)
{

$chucdanh = '<img src="http://img.infoviet.net/images/forum/level/vip.gif" width="25" height="15" />';
}
}
return $chucdanh;
}

/////////////////////
public static function thanhvien($s){
global $website,$act,$headmod,$user_id,$set;
if($s=='avt')
$var.='<a href="' . $set['homeurl'] . '/users/profile.php?act=images&amp;mod=avatar&amp;user=' . $user_id . '">';

	                           $var.='<img class="avatar" src="http://up.infoviet.net/avatar/' . $user_id . '.png" onerror="this.src=\'http://img.infoviet.net/images/empty.png\'"; width="32" height="32" alt="' . $user_id . '" align="top" "/>&#160;';
     

return $var;
}




// Chuyên mục
public static function chuyenmuc(){
global $website;
$var.= '<div class="mainblok">';
$sql =mysql_query("SELECT `id`,`text` FROM `forum` where `website`='$website' and `type` = 'f' order by `realid` ASC")or die (mysql_error());
while($row =mysql_fetch_assoc($sql)){

$var.= '<div class="phdr"> <a href="'.$home.'/forum/'.functions::mikdaik(notags($row['text'])).'_'.$row['id'].'.html"> '.$row['text'].' </a> </div>';

$fsql =mysql_query("select * from `forum` where `website`='$website' and  `type` = 'r' && `refid` = '".$row['id']."' order by `realid` ASC");
while($frow =mysql_fetch_array($fsql)){
$total_post =mysql_result(mysql_query("select count(*) from `forum` where `website`='$website' and  `type` = 't' && `refid` = '{$frow['id']}'"),0);

$var.= '<div class="list1">
<table width="100%" cellpadding="0" cellspacing="0"><tbody><tr><td width="auto"><img src="/1.png">
<a href="'.$home.'/forum/'.functions::mikdaik(notags($frow['text'])).'_'.$frow['id'].'.html"> '.$frow['text'].' </a></td><td width="auto" align="right">[<font color="green">'.$total_post.'</font>]</td></tr></tbody></table></div>';


}}
mySQL_free_result($sql);
return $var;
}
public static function ds(){
global $website;
$var.='<form name="cngtng">
<select name="url"size="1"onChange="window.open(this.options [this.selectedIndex].value,\'_top\')">
<option value="">Chuyên Mục</option>
';

$sql5 =mysql_query("SELECT `id`,`text` FROM `forum` where `website`='$website' and `type` = 'f' order by `realid` ASC");
while($row =mysql_fetch_assoc($sql5)){

$var.= '<option value="'.$home.'/forum/'.functions::mikdaik(notags($row['text'])).'_'.$row['id'].'.html">'.$row['text'].'
</option>
';

$fsql4 =mysql_query("select `id`,`text` from `forum` where `website`='$website' and `type` = 'r' && `refid` = '".$row['id']."' order by `realid` ASC");
while($frow =mysql_fetch_array($fsql4)){


$var.= '<option value="'.$home.'/forum/'.functions::mikdaik(notags($frow['text'])).'_'.$frow['id'].'.html">->'.$frow['text'].'</option>';

}}
mySQL_free_result($sql5);
$var.= '</select></form>';

return $var;
}
public static function chatbox(){
global $website,$set_user,$realtime,$user_id,$admp;
if ($user_id) {
$var.= '<div class="mainblok"><div class="phdr">
 Shoutbox</div></div>';
$var.= '<div class="list1"><form name="form" action="/guestbook/index.php?act=say" method="post">' ;
$var.= '<input type="text" name="msg" rows="2">' ;
$var.= '<input type="submit" name="submit" value="Gửi"></form></div>' ;
$var.= '<div class="gmenu">';
{
$roq = mysql_query("SELECT `guest`.*, `users`.`name`, `users`.`rights`, `users`.`lastdate`, `users`.`sex`, `users`.`status`, `users`.`datereg`, `users`.`ip` , `users`.`browser`   FROM `guest` LEFT JOIN `users` ON `guest`.`user_id` = `users`.`id` WHERE `guest`.`website`='$website' ORDER BY `time` DESC LIMIT 7;")or die(mysql_error());;
while ($res = mysql_fetch_array($roq))
{
	  if ($res['rights'] == 0) {
$res['colornick'] = '#4e387e';
}
    if ($res['rights'] == 1) {
$res['colornick'] = '#008000';
}
      if ($res['rights'] == 2) {
$res['colornick'] = '#008000';
}
      if ($res['rights'] == 3) {
$res['colornick'] = '#008000';
}
      if ($res['rights'] == 4) {
$res['colornick'] = '#008000';
}
      if ($res['rights'] == 5) {
$res['colornick'] = '#008000';
}
      if ($res['rights'] == 6) {
$res['colornick'] = '#43d6d8';
}
      if ($res['rights'] == 7) {
$res['colornick'] = '#ff0000';
}
      if ($res['rights'] == 9)
{
$res['colornick'] = '#ff00ff';
}
       if ($res['rights'] == 10)
  {
$res['colornick'] = '#a52a2a';
}


                  $var.= (time() > $res['lastdate'] + 300 ? '<font color="red" size="4" align="absmiddle">&bull;</font> ' : '<font color="green" size="4" align="absmiddle">&bull;</font> ');
// icon seks
$var.= '<a href="../users/profile.php?user=' . $res['user_id'] . '"><font color="' . $res['colornick'] . '"><b>' . $res['name'] . '</b></b></font></a> ';
$ontimes = $res['lastdate'] + 300;
if ($realtime > $ontimes)
{
$var.= '<span style="color:black;"><b>:</b></span>';
}
else
{
$var.= '<span style="color:black"><b>:</b></span>';
}
$var.= ' ';
  /////////

 if($user_id) {
$post = str_replace('[you]', $login, $post);
} else {
$post = str_replace('[you]', 'Khaﾌ…h', $post);
}

$post = strip_tags($res['text']);
$post = functions::checkout($post,1,1);
$post = functions::smileys($post, $res['rights'] >= 1 ? 1 : 0);

// text
if (mb_strlen($post) >= 1000)
{
$post = mb_substr($post, 0, 1000);
$var.= $post.' ';
}
else
{
$var.= $post;
}
$var.= '<hr>';
++$i;
}
mySQL_free_result($roq);
}$var.= '</div><div class="list1"><center><a href="">Refresh</a> | <a href="../pages/faq.php?act=smileys">Smiles</a> | <a href="../guestbook/index.php">Shoutbox</a></center></div>';
} else {
$var.= '<div class="mainblok"><div class="phdr">
 Shoutbox</div></div>';
$var.= '<div class="gmenu">';
{
$roq = mysql_query("SELECT `guest`.*, `users`.`name`, `users`.`rights`, `users`.`lastdate`, `users`.`sex`, `users`.`status`, `users`.`datereg`, `users`.`ip` , `users`.`browser`   FROM `guest` LEFT JOIN `users` ON `guest`.`user_id` = `users`.`id` WHERE `guest`.`website`='$website' ORDER BY `time` DESC LIMIT 7;")or die(mysql_error());;
while ($res = mysql_fetch_array($roq))
{

	  if ($res['rights'] == 0) {
$res['colornick'] = '#4e387e';
}

    if ($res['rights'] == 1) {
$res['colornick'] = '#008000';
}

      if ($res['rights'] == 2) {
$res['colornick'] = '#008000';
}


      if ($res['rights'] == 3) {
$res['colornick'] = '#008000';
}


      if ($res['rights'] == 4) {
$res['colornick'] = '#008000';
}


      if ($res['rights'] == 5) {
$res['colornick'] = '#008000';
}


      if ($res['rights'] == 6) {
$res['colornick'] = '#43d6d8';
}


      if ($res['rights'] == 7) {
$res['colornick'] = '#ff0000';
}

       if ($res['rights'] == 9)

{
$res['colornick'] = '#ff00ff';
}
       if ($res['rights'] == 10)
  {
$res['colornick'] = '#a52a2a';
}
/////


                  $var.= (time() > $res['lastdate'] + 300 ? '<font color="red" size="4" align="absmiddle">&bull;</font> ' : '<font color="green" size="4" align="absmiddle">&bull;</font> ');
// icon seks
$var.= '<a href="../users/profile.php?user=' . $res['user_id'] . '"><font color="' . $res['colornick'] . '"><b>' . $res['name'] . '</b></b></font></a> ';
$ontimes = $res['lastdate'] + 300;
if ($realtime > $ontimes)
{
$var.= '<span style="color:black;"><b>:</b></span>';
}
else
{
$var.= '<span style="color:black"><b>:</b></span>';
}
$var.= ' ';
  /////////

 if($user_id) {
$post = str_replace('[you]', $login, $post);
} else {
$post = str_replace('[you]', 'Khaﾌ…h', $post);
}

$post = strip_tags($res['text']);
$post = functions::checkout($post,1,1);
$post = functions::smileys($post, $res['rights'] >= 1 ? 1 : 0);

// text
if (mb_strlen($post) >= 1000)
{
$post = mb_substr($post, 0, 1000);
$var.= $post.' ';
}
else
{
$var.= $post;
}
$var.= '<hr>';
++$i;
}
mySQL_free_result($roq);
}$var.= '</div><div class="list1"><center><a href="">Refresh</a> | <a href="../pages/faq.php?act=smileys">Smiles</a> | <a href="../guestbook/index.php">Shoutbox</a></center></div>';
}

return $var;
}
////// Shout Box

// Blog
public static function blog(){
global $website,$start,$kmess;
$var.= '<div class="mainblok"><div class="phdr">Bài Viết mới</div>';
$req =@mysql_query("SELECT * FROM `forum` WHERE `website`='$website' and `type`='t' AND `close`!='1' ORDER BY rand() DESC LIMIT $start, $kmess");
$total=@mysql_result(mysql_query("SELECT COUNT(*) FROM `forum` WHERE `website`='$website' and `type`='t' AND `close`!='1'"),0)or die (mysql_error());

if($total>0){
$i = 0;
while (($res =@mysql_fetch_assoc($req)) !== false) {
$rq=@mysql_query("SELECT `text` FROM `forum` WHERE `website`='$website' and `type`='m' AND `refid`='".$res['id']."' ORDER BY rand() LIMIT 1");
$rs=@mysql_fetch_array($rq);
$gettop = mysql_fetch_assoc(mysql_query("SELECT * FROM `forum` WHERE `website`='$website' and `type` = 'r' and id = '".$res['refid']."'"));
		$topic = $gettop['text'];
$text=mb_substr($rs['text'],0,200,'UTF-8');
$text=bbcode::tags($text);
$text=strip_tags($text,'<b><br>');
if(preg_match('#\[img\](https?://.+?)\[\/img\]#i', $rs['text'], $Img)) {
$img=$Img[1];
} else {
$img='http://infoviet.net/images/noimage.png';
}
$nikuser =@mysql_query("SELECT `from` FROM `forum` WHERE `website`='$website' and `type` = 'm' AND `close` != '1' AND `refid` = '" . $res['id'] . "' ORDER BY `time` DESC LIMIT 1");
$nam =@mysql_fetch_assoc($nikuser);
$colmes =@mysql_query("SELECT COUNT(*) FROM `forum` WHERE `website`='$website' and `type`='m' AND `refid`='" . $res['id'] . "'" . ($rights >= 7 ? '' : " AND `close` != '1'"));
$colmes1 =@mysql_result($colmes, 0);
$cpg = ceil($colmes1 / $kmess);
$var.= '<div class="list1">';
$var.= '<table cellpdding="0" cellspacing="0" width="100%"><tr><td width="40" align="center">';
$var.= '<img class="image" src="'.$img.'" width="45px" height="55px"/>';


$var.= '</td><td width="auto" valign="top"><a href="'.$home.'/forum/'.functions::mikdaik(notags($res['text'])).'_'.$res['id'].'.html"> '.$res['text']. '</a><div class="sub"><span class="gray">' . functions::display_date($res['time']) . ' </span></div>';
$var.= ' Chuyên Mục: '.$topic.'';





$var.= '</td></tr></table>';
$var.= '</div>';
$i;
}
} else {
$var.= '<div class="menu"><p>Không có bài mới</p></div>';
}




$var.= '</div>';
return $var;
}
// Topx
public static function topx()
 {
 global $website,$start,$kmess,$set;
$var.= '<div class="mainblok"><div class="phdr"><a> Diễn đàn</a><a href="' . $set['homeurl'] . '/forum" style="float:right;"><b>+</b></a></div>';
if ($set['kiemduyet'] !=0) {
$req = mysql_query("SELECT * FROM `forum` where `website` = '$website'  AND `kiemduyet`='1' AND  `type` = 't' and kedit='0' AND `close`!='1' ORDER BY `time` DESC LIMIT $start, $kmess");
} else{
$req = mysql_query("SELECT * FROM `forum` where `website` = '$website'  AND  `type` = 't' and kedit='0' AND `close`!='1' ORDER BY `time` DESC LIMIT $start, $kmess");

}

$total = mysql_result(mysql_query("SELECT COUNT(*) FROM `forum` where `website` = '$website' AND  `type` = 't' and kedit='0' AND `close`!='1'"), 0);


while ($arr = mysql_fetch_array($req)) {


$nikuser = mysql_query("SELECT `from`,`id`, `time` FROM `forum` where `website` = '$website' AND  `type` = 'm' AND `close` != '1' AND `refid` = '" . $arr['id'] . "'ORDER BY time DESC");
$colmes1 = mysql_num_rows($nikuser);

$cpg = ceil($colmes1 / $kmess);
$nam = mysql_fetch_array($nikuser);
mySQL_free_result($nikuser);
$trangxinhdep2 = mysql_query("select `rights` from `users` where `website` = '$website' AND  id='" . $nam['user_id'] . "'");
$trang3 = mysql_fetch_array($trangxinhdep2);
mySQL_free_result($trangxinhdep2);
$var.= is_integer($i / 2) ? '<div class="gmenu">' : '<div class="gmenu">';
$var.= '<img src="http://img.infoviet.net/images/' . ($arr['edit'] == 1 ? 'tz' : 'np') . '.gif" alt=""/>';
if ($arr['realid'] == 1)
$var.= '&nbsp;<img src="http://img.infoviet.net/images/rate.gif" alt=""/>';
if ($arr['realid'] == 1)
$var.= '&nbsp;<img src="http://img.infoviet.net/images/rate.gif" alt="Bình chọn"/>';
if ($arr['tiento'] ==1) {
$var.='<b><font color="red">[Share]</font></b>';
} elseif ($arr['tiento'] ==2) {
$var.='<b><font color="blue">[Giúp]</font></b
>';
} elseif ($arr['tiento'] ==3) {
$var.='<b><font color="red">[HOT]</font></b>';
} elseif ($arr['tiento'] ==4) {
$var.='<b><font color="red">[Thông báo]</font></b>';
} elseif ($arr['tiento'] ==5) {
$var.='<b><font color="green">[Thảo luận]</font></b>';
} elseif ($arr['tiento'] ==6) {
$var.='<b><font color="blue">[Hướng dẫn]</font></b>';
}
$var.= '&nbsp;<a href="'.$home.'/forum/'.functions::mikdaik($arr["text"]).'_' . $arr['id'] . ($cpg > 1 && $set_forum['upfp'] && $set_forum['postclip'] ? '_clip_' : '') . ($set_forum['upfp'] && $cpg > 1 ? '_p' . $cpg : '.html') . '">' . bbcode::tags($arr['text']) . '</a>';
if ($cpg <= 1){
$var.= '&nbsp;(' . $colmes1 . ')';
}
if ($cpg > 1)
$var.= '&nbsp;(<a href="'.$home.'/forum/'.functions::mikdaik($arr["text"]).'_' . $arr['id'] . (!$set_forum['upfp'] && $set_forum['postclip'] ? '_clip_' : '') . ($set_forum['upfp'] ? '' : '_p' . $cpg) . '.html"> ' . $colmes1 . '</a>)';


if ($trang3['rights'] == 0 ) {
$colornick['colornick'] = '4e387e';
$colornickk['colornick'] = '4e387e';
}
if ($trang3['rights'] == 3 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($trang3['rights'] == 6 ) {
$colornick['colornick'] = '43d6d8';
$colornickk['colornick'] = '43d6d8';
}
if ($trang3['rights'] == 7 ) {
$colornick['colornick'] = 'ff0000';
$colornickk['colornick'] = 'ff0000';
}
if ($trang3['rights'] == 9 ) {
$colornick['colornick'] = 'ff00ff';
$colornickk['colornick'] = 'ff00ff';
}


if (!empty ($nam['from'])) {
$var.= '&nbsp;<font color="#' . $colornick['colornick'] . '">' .$nam['from']. '</font>
';
}
$var.= '</div>';
$i++;
}

$var.= '</div>';

if ($total > $kmess) {
$var.= '<div class="list1">' . functions::display_pagination('index.php?', $start, $total, $kmess) . '</div>';
}
return $var;
}

public static function tintuc()
{
global $website,$start,$kmess;
$req = mysql_query("SELECT `id`,`text`,`time`,`refid` FROM `forum` where `website` = '$website' AND  `type`='t' AND `portal`>'0' ORDER BY `portal` DESC LIMIT 3");
if(mysql_num_rows($req)!=0) {
$var.= '<div class="mainblok"><div class="phdr">
<table width="100%">
<tr>
<td> Tin Tức HOT</td>
<td align="right"><a href=".../forum"><b> </b> </a></td>
</tr>
</table>
</div></div>';}

$i = 0;
while($res = mysql_fetch_array($req)) {
$noidung = mysql_query("SELECT `id`, `refid`, `text` FROM `forum` where `website` = '$website' AND  `type`='m' AND `refid`='" . $res['id'] . "'");
$t = mysql_fetch_array($noidung);
$q3 = mysql_query("SELECT `id`, `refid`, `text` FROM `forum` where `website` = '$website' AND  `type`='r' AND `id`='" . $res['refid'] . "'");
$razd = mysql_fetch_array($q3);
$q4 = mysql_query("SELECT `text` FROM `forum` where `website` = '$website' AND  `type`='f' AND `id`='" . $razd['refid'] . "'");
$frm = mysql_fetch_array($q4);
$colmes = mysql_query("SELECT * FROM `forum` where `website` = '$website' AND  `refid` = '" . $res['id'] . "' AND `type` = 'm'" . ($rights >= 7 ? '' : " AND `close` != '1'") . " ORDER BY `time` DESC");
$colmes1 = mysql_num_rows($colmes);
$cpg = ceil($colmes1 / $kmess);

$var.= '<div class="pad5">';
$var.= '&#160;<a href="../forum/' . functions::mikdaik($res['text']) . '_' . $res['id'] . '.html"><b>' . bbcode::tags($res['text']) .'</b></a>&#160;<br />';
$var.= '<span class="gray">'.$res['from'].' - ' . functions::display_date($res['time']) . '</span><br/>';

/*
-----------------------------------------------------------------
Noi dung bai viet
-----------------------------------------------------------------
*/

$text = $t['text'];
$cut = '200';

$text = mb_substr($text, 0, $cut);
$text = functions::checkout($text, 1, 1);
if ($set_user['smileys'])
$text = functions::smileys($text);
$var.= $text;

$var.= '</div>';
++$i;
}
return $var;
}
// Func ảnh
public static function anh()
{
global $website;
$hammad4=mysql_query("SELECT `id`, `user_id`, `tmb_name`, `time` FROM `cms_album_files` where `website`='$website' order by rand() limit 3") or die (mysql_error());
if(mysql_num_rows($hammad4)!=0)
{
$var.='<div class="phdr"><b>Album Ảnh</b></div><div class="list2">';
while($ar34=mysql_fetch_array($hammad4)){
$var.='<img src="../files/users/album/'.$ar34['user_id'] .'/'.$ar34['tmb_name'] .'" alt="'.$ar34['id'] .'"
height="42" width="42"
/> ';
}
$var.='</div>';
}
return $var;
}
// Func Trắc Nghiệm

public static function tracngiem()
{
global $website;

$req = mysql_query("SELECT * FROM `quiz` where `website` = '$website' AND  `type`='q' ORDER BY `time` DESC LIMIT 5");
if(mysql_num_rows($req)!=0) {
$var.= '<div class="mainblok"><div class="phdr"><a> Trắc nghiệm</a><a href="users/quiz.php" style="float:right;"><b>+</b></a></div>';

}
$i = 0;
while($res = mysql_fetch_assoc($req)) {
$var.= $i % 2 ? '<div class="list1">' : '<div class="list1">';
$price = $res['price'];
$text = functions::checkout($res['text'], 1, 1);
$text = functions::smileys($text, 1);
$var.= '» <a href="users/quiz.php?quiz='.$res['id'].'"> '.$text.' ('.$price.'xu)'.($user_id ? ' </a>' : '</a>');
$var.= '</div>';
++$i;
}
return $var;
}
// Func Member

public static function member()
{
global $website,$set,$chuoimaunick;

$var.='<div class="phdr">Trực tuyến</div>';
$var.= '<div class="orangex">';
$onltime = '".time() - 300."';
$gbot = 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)';
$yanbot = 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)';
$bing = 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)';
$msn = 'msnbot/1.1 (+http://search.msn.com/msnbot.htm)';
$bd = 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)';
$DCM = 'DoCoMo/2.0 N905i(c100;TB;W24H16) (compatible; Googlebot-Mobile/2.1; +http://www.google.com/bot.html)';
$yahoo = 'Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)';
$Ahrefs = 'Mozilla/5.0 (compatible; AhrefsBot/3.1; +http://ahrefs.com/robot/)';
$Sosos = 'Sosospider+(+http://help.soso.com/webspider.htm)';

$googlebot = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_sessions` WHERE `lastdate` > '$onltime' AND `browser`='$gbot' AND `website` = '$website'"), 0);
$yandexbot = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_sessions` WHERE `lastdate` > '$onltime' AND `browser`='$yanbot' AND `website` = '$website'"), 0);
$bingbot = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_sessions` WHERE `lastdate` > '$onltime' AND `browser`='$bing' AND `website` = '$website'"), 0);
$baidu = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_sessions` WHERE `lastdate` > '$onltime' AND `browser`='$msn' AND `website` = '$website'"), 0);
$msnbot = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_sessions` WHERE `lastdate` > '$onltime' AND `browser`='$bd' AND `website` = '$website'"), 0);
$yahoobot = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_sessions` WHERE `lastdate` > '$onltime' AND `browser`='$yahoo' AND `website` = '$website'"), 0);
$DoCoMo = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_sessions` WHERE `lastdate` > '$onltime' AND `browser`='$DCM' AND `website` = '$website'"), 0);
$AhrefsBot = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_sessions` WHERE `lastdate` > '$onltime' AND `browser`='$Ahrefs' AND `website` = '$website'"), 0);
$Sosospider = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_sessions` WHERE `lastdate` > '$onltime' AND `browser`='$Sosos' AND `website` = '$website'"), 0);
$usere = mysql_result(mysql_query("SELECT COUNT(*) FROM `users` WHERE `website` = '$website' AND `lastdate` > '" . (time() - 300) . "'"), 0);
$tamune = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_sessions` WHERE `website` = '$website' AND `lastdate` > '" . (time() - 300) . "'"), 0);
$total = $total+$googlebot+$msnbot+$yandexbot+$bingbot+$yahoobot+$baidu+$DoCoMo+$AhrefsBot+$Sosospider+$usere+$tamune;

if ($total) {
$var.= '•Online: '.($googlebot > 0 ? '<font color="red">Google[BOT]</font>, ' : '').''.($yandexbot > 0 ? '<font color="117cd6">Yandex[BOT]</font>, ' : '').''.($bingbot > 0 ? '<font color="red">Bing[BOT]</font>, ' : '').''.($baidu > 0 ? '<font color="1d942e">Baidu[BOT]</font>, ' : '').''.($msnbot > 0 ? '<font color="c4a916">MSN[BOT]</font>, ' : '').''.($yahoobot > 0 ? '<font color="b70db9">Yahoo[BOT]</font>, ' : '').''.($DoCoMo > 0 ? '<font color="red">DoCoMo[BOT]</font>, ' : '').''.($AhrefsBot > 0 ? '<font color="red">AhrefsBot[BOT]</font>, ' : '').''.($Sosospider > 0 ? '<font color="0d66b9">Soso[BOT]</font>, ' : '').'';
}
$online = array();
$hadir = @mysql_query("SELECT * FROM `users` WHERE `lastdate` >= '" . intval(time() - 300) . "' AND `website` = '$website' ORDER BY RAND() ;");
$hitung = mysql_num_rows($hadir);
while ($notal = mysql_fetch_array($hadir)){
if($set['caidat']==null) {
if ($notal['rights'] == 0 ) {
$colornick['colornick'] = '4e387e';
}
if ($notal['rights'] == 1 ) {
$colornick['colornick'] = '008000';
}
if ($notal['rights'] == 2 ) {
$colornick['colornick'] = '008000';
}
if ($notal['rights'] == 3 ) {
$colornick['colornick'] = '008000';
}
if ($notal['rights'] == 4 ) {
$colornick['colornick'] = '008000';
}
if ($notal['rights'] == 5 ) {
$colornick['colornick'] = '008000';
}
if ($notal['rights'] == 6 ) {
$colornick['colornick'] = '43d6d8';
}
if ($notal['rights'] == 7 ) {
$colornick['colornick'] = 'ff0000';
}
if ($notal['rights'] == 9 ) {
$colornick['colornick'] = 'ff00ff';
}
if ($notal['rights'] == 10 ) {
$colornick['colornick'] = 'ff0000';
}

/////////////
} else {
//////////
if ($notal['rights'] == 0 ) {
$colornick['colornick'] = ''.$chuoimaunick[4].'';
}
if ($notal['rights'] == 1 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
}
if ($notal['rights'] == 2 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
}
if ($notal['rights'] == 3 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
}
if ($notal['rights'] == 4 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
}
if ($notal['rights'] == 5 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
}
if ($notal['rights'] == 6 ) {
$colornick['colornick'] = ''.$chuoimaunick[2].'';
}
if ($notal['rights'] == 7 ) {
$colornick['colornick'] = ''.$chuoimaunick[1].'';
}
if ($notal['rights'] == 9 ) {
$colornick['colornick'] = ''.$chuoimaunick[0].'';
}
if ($notal['rights'] == 10 ) {
$colornick['colornick'] = ''.$chuoimaunick[1].'';
}
}
$online[] = '<a href="' . core::$system_set['homeurl'] . '/users/profile.php?user=' . $notal['id'] . '"><span style="color:#' . $colornick['colornick'] . '">' . $notal['name'] . '</span></a>';
}

$var.= implode(', ',$online).'.';

$var.= '<br/>•Khách: ';
if ($tamune >= 1)
$var.= '<a href="' . core::$system_set['homeurl'] . '/users/index.php?act=online&amp;mod=guest">' . $tamune . '</a>';
else
$var.= 'No Guest';
//new member//
$var.= '<br/>•T.v mới:&nbsp;';
$req = mysql_query("SELECT `id`, `name`, `datereg` FROM `users` WHERE (preg='1') AND `website` = '$website' ORDER BY `datereg` DESC LIMIT 1; ");
$arr = mysql_fetch_array($req);
$var.= '<a href="' . core::$system_set['homeurl'] . '/'.$arr['id'].'-'.$arr['name'].'">'.$arr['name'].'</a>';
$var.='</div>';
return $var;
}
public static function thongke()
{
global $website;
$chude=mysql_result(mysql_query("SELECT COUNT(*) FROM `forum` WHERE `website`='$website' and `type` = 't' AND `close` != '1'"), 0);
$baiviet=mysql_result(mysql_query("SELECT COUNT(*) FROM `forum` WHERE `website`='$website' and `type` = 'm' AND `close` != '1'"), 0);
$thanhvien=mysql_result(mysql_query("SELECT COUNT(*) FROM `users` where `website`='$website'"), 0);

$var.= '<div class="phdr">Thống kê</div>';
$var.='<div class="orangex">';
$var.='<div class="list2">Chủ đề: <b>' .$chude . '</b></div>';
$var.='<div class="list2">Bài viết: <b>' . $baiviet. '</b></div>';
$var.='<div class="list2">Thành viên: <b>' .$thanhvien. '</b></div>';
$var.='</div>';
return $var;
}






/////////////


/*/////////////


Bài viết

////////////////*/


//////////////////////////////

/*
-----------------------------------------------------------------
Проверка переменных
-----------------------------------------------------------------
*/
public static function check($str)
{
$str = htmlentities(trim($str), ENT_QUOTES, 'UTF-8');
$str = nl2br($str);
$str = strtr($str, array(
chr(0) => '',
chr(1) => '',
chr(2) => '',
chr(3) => '',
chr(4) => '',
chr(5) => '',
chr(6) => '',
chr(7) => '',
chr(8) => '',
chr(9) => '',
chr(10) => '',
chr(11) => '',
chr(12) => '',
chr(13) => '',
chr(14) => '',
chr(15) => '',
chr(16) => '',
chr(17) => '',
chr(18) => '',
chr(19) => '',
chr(20) => '',
chr(21) => '',
chr(22) => '',
chr(23) => '',
chr(24) => '',
chr(25) => '',
chr(26) => '',
chr(27) => '',
chr(28) => '',
chr(29) => '',
chr(30) => '',
chr(31) => ''
));
$str = str_replace("'", "&#39;", $str);
$str = str_replace('\\', "&#92;", $str);
$str = str_replace("|", "I", $str);
$str = str_replace("||", "I", $str);
$str = str_replace("/\\\$/", "&#36;", $str);
$str = mysql_real_escape_string($str);
return $str;
}

/*
-----------------------------------------------------------------
Обработка текстов перед выводом на экран
-----------------------------------------------------------------
$br=1           обработка переносов строк
$br=2           подстановка пробела, вместо переноса
$tags=1         обработка тэгов
$tags=2         вырезание тэгов
-----------------------------------------------------------------
*/
public static function checkout($str, $br = 0, $tags = 0)
{
$str = htmlentities(trim($str), ENT_QUOTES, 'UTF-8');
if ($br == 1)
$str = nl2br($str);
elseif ($br == 2)
$str = str_replace("\r\n", ' ', $str);
if ($tags == 1)
$str = tags(url($str));
elseif ($tags == 2)
$str = notags($str);
$replace = array(
chr(0) => '',
chr(1) => '',
chr(2) => '',
chr(3) => '',
chr(4) => '',
chr(5) => '',
chr(6) => '',
chr(7) => '',
chr(8) => '',
chr(9) => '',
chr(11) => '',
chr(12) => '',
chr(13) => '',
chr(14) => '',
chr(15) => '',
chr(16) => '',
chr(17) => '',
chr(18) => '',
chr(19) => '',
chr(20) => '',
chr(21) => '',
chr(22) => '',
chr(23) => '',
chr(24) => '',
chr(25) => '',
chr(26) => '',
chr(27) => '',
chr(28) => '',
chr(29) => '',
chr(30) => '',
chr(31) => ''
);
return strtr($str, $replace);
}

/*
-----------------------------------------------------------------
Показ различных счетчиков внизу страницы
-----------------------------------------------------------------
*/
public static function display_counters()
{
global $headmod,$website;
$req = mysql_query("SELECT * FROM `cms_counters` where `website` = '$website' AND  `switch` = '1' ORDER BY `sort` ASC");
if (mysql_num_rows($req) > 0) {
while (($res = mysql_fetch_array($req)) !== false) {
$link1 = ($res['mode'] == 1 || $res['mode'] == 2) ? $res['link1'] : $res['link2'];
$link2 = $res['mode'] == 2 ? $res['link1'] : $res['link2'];
$count = ($headmod == 'mainpage') ? $link1 : $link2;
if (!empty($count))
echo $count;
}
}
}

/*
-----------------------------------------------------------------
Показываем дату с учетом сдвига времени
-----------------------------------------------------------------
*/
public static function display_date($var)
{
$shift = (self::$system_set['timeshift'] + self::$user_set['timeshift']) * 3600;

return date("d.m.Y / H:i", $var + $shift);
}

/*
-----------------------------------------------------------------
Сообщения об ошибках
-----------------------------------------------------------------
*/
public static function display_error($error = NULL, $link = NULL)
{
if (!empty($error)) {
return '<div class="rmenu"><p><b>' . self::$lng['error'] . '!</b><br />' .
(is_array($error) ? implode('<br />', $error) : $error) . '</p>' .
(!empty($link) ? '<p>' . $link . '</p>' : '') . '</div>';
} else {
return false;
}
}

/*
-----------------------------------------------------------------
Отображение различных меню
-----------------------------------------------------------------
$delimiter - разделитель между пунктами
$end_space - выводится в конце
-----------------------------------------------------------------
*/
public static function display_menu($val = array(), $delimiter = ' | ', $end_space = '')
{
return implode($delimiter, array_diff($val, array(''))) . $end_space;
}

/*
-----------------------------------------------------------------
Постраничная навигация
За основу взята аналогичная функция от форума SMF2.0
-----------------------------------------------------------------
*/
public static function display_pagination($base_url, $start, $max_value, $num_per_page)
{
$neighbors = 2;
if ($start >= $max_value)
$start = max(0, (int)$max_value - (((int)$max_value % (int)$num_per_page) == 0 ? $num_per_page : ((int)$max_value % (int)$num_per_page)));
else
$start = max(0, (int)$start - ((int)$start % (int)$num_per_page));
$base_link = '<a class="pagenav" href="' . strtr($base_url, array('%' => '%%')) . 'page=%d' . '">%s</a>';
$out[] = $start == 0 ? '' : sprintf($base_link, $start / $num_per_page, '&lt;&lt;');
if ($start > $num_per_page * $neighbors)
$out[] = sprintf($base_link, 1, '1');
if ($start > $num_per_page * ($neighbors + 1))
$out[] = '<span style="font-weight: bold;">...</span>';
for ($nCont = $neighbors; $nCont >= 1; $nCont--)
if ($start >= $num_per_page * $nCont) {
$tmpStart = $start - $num_per_page * $nCont;
$out[] = sprintf($base_link, $tmpStart / $num_per_page + 1, $tmpStart / $num_per_page + 1);
}
$out[] = '<span class="currentpage"><b>' . ($start / $num_per_page + 1) . '</b></span>';
$tmpMaxPages = (int)(($max_value - 1) / $num_per_page) * $num_per_page;
for ($nCont = 1; $nCont <= $neighbors; $nCont++)
if ($start + $num_per_page * $nCont <= $tmpMaxPages) {
$tmpStart = $start + $num_per_page * $nCont;
$out[] = sprintf($base_link, $tmpStart / $num_per_page + 1, $tmpStart / $num_per_page + 1);
}
if ($start + $num_per_page * ($neighbors + 1) < $tmpMaxPages)
$out[] = '<span style="font-weight: bold;">...</span>';
if ($start + $num_per_page * $neighbors < $tmpMaxPages)
$out[] = sprintf($base_link, $tmpMaxPages / $num_per_page + 1, $tmpMaxPages / $num_per_page + 1);
if ($start + $num_per_page < $max_value) {
$display_page = ($start + $num_per_page) > $max_value ? $max_value : ($start / $num_per_page + 2);
$out[] = sprintf($base_link, $display_page, '&gt;&gt;');
}
return implode(' ', $out);
}

/*
-----------------------------------------------------------------
Показываем местоположение пользователя
-----------------------------------------------------------------
*/
public static function display_place($user_id = '', $place = '')
{
global $headmod;
$place = explode(",", $place);
$placelist = parent::load_lng('places');
if (array_key_exists($place[0], $placelist)) {
if ($place[0] == 'profile') {
if ($place[1] == $user_id) {
return '<a href="' . self::$system_set['homeurl'] . '/users/profile.php?user=' . $place[1] . '">' . $placelist['profile_personal'] . '</a>';
} else {
$user = self::get_user($place[1]);
return $placelist['profile'] . ': <a href="' . self::$system_set['homeurl'] . '/users/profile.php?user=' . $user['id'] . '">' . $user['name'] . '</a>';
}
}
elseif ($place[0] == 'online' && isset($headmod) && $headmod == 'online') return $placelist['here'];
else return str_replace('#home#', self::$system_set['homeurl'], $placelist[$place[0]]);
}
else return '<a href="' . self::$system_set['homeurl'] . '/index.php">' . $placelist['homepage'] . '</a>';
}

/*
-----------------------------------------------------------------
Отображения личных данных пользователя
-----------------------------------------------------------------
$user          (array)     массив запроса в таблицу `users`
$arg           (array)     Массив параметров отображения
[lastvisit] (boolean)   Дата и время последнего визита
[stshide]   (boolean)   Скрыть статус (если есть)
[iphide]    (boolean)   Скрыть (не показывать) IP и UserAgent
[iphist]    (boolean)   Показывать ссылку на историю IP

[header]    (string)    Текст в строке после Ника пользователя
[body]      (string)    Основной текст, под ником пользователя
[sub]       (string)    Строка выводится вверху области "sub"
[footer]    (string)    Строка выводится внизу области "sub"
-----------------------------------------------------------------
*/
public static function display_user2($user = false, $arg = false)
{

global $rootpath, $mod,$website,$set,$chuoimaunick,$chuoichucvu;
$out = false;

if (!$user['id']) {
$out = '<b>' . self::$lng['guest'] . '</b>';
if (!empty($user['name']))
$out .= ': ' . $user['name'];
if (!empty($arg['header']))
$out .= ' ' . $arg['header'];
} else {

if ($user['sex'])
$out .= '<div class="newsx" style="border-bottom:2px solid #cecece"><img src="http://img.infoviet.net/theme/' . self::$user_set['skin'] . '/images/' . ($user['sex'] == 'm' ? 'm' : 'w') . ($user['datereg'] > time() - 86400 ? '_new' : '')
. '.png" width="16" height="16" align="middle" alt="' . ($user['sex'] == 'm' ? 'М' : 'Ж') . '" />&#160;';
else
$out .= '<img src="http://img.infoviet.net/images/del.png" width="12" height="12" align="middle" />&#160;';
if($set['caidat']==null) {
if ($user['rights'] == 0 ) {
$colornick['colornick'] = '4e387e';
$colornickk['colornick'] = '4e387e';
}
if ($user['rights'] == 1 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($user['rights'] == 2 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($user['rights'] == 3 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($user['rights'] == 4 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($user['rights'] == 5 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($user['rights'] == 6 ) {
$colornick['colornick'] = '43d6d8';
$colornickk['colornick'] = '43d6d8';
}
if ($user['rights'] == 7 ) {
$colornick['colornick'] = 'ff0000';
$colornickk['colornick'] = 'ff0000';
}
if ($user['rights'] == 9 ) {
$colornick['colornick'] = 'ff00ff';
$colornickk['colornick'] = 'ff00ff';
}
if ($user['rights'] == 10 ) {
$colornick['colornick'] = 'ff0000';
$colornickk['colornick'] = 'ff0000';
} } else{
if ($user['rights'] == 0 ) {
$colornick['colornick'] = ''.$chuoimaunick[4].'';
$colornickk['colornick'] = ''.$chuoimaunick[4].'';
}
if ($user['rights'] == 1 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
$colornickk['colornick'] = ''.$chuoimaunick[3].'';
}
if ($user['rights'] == 2 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
$colornickk['colornick'] = ''.$chuoimaunick[3].'';
}
if ($user['rights'] == 3 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
$colornickk['colornick'] = ''.$chuoimaunick[3].'';
}
if ($user['rights'] == 4 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
$colornickk['colornick'] = ''.$chuoimaunick[3].'';
}
if ($user['rights'] == 5 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
$colornickk['colornick'] = ''.$chuoimaunick[3].'';
}
if ($user['rights'] == 6 ) {
$colornick['colornick'] = ''.$chuoimaunick[2].'';
$colornickk['colornick'] = ''.$chuoimaunick[2].'';
}
if ($user['rights'] == 7 ) {
$colornick['colornick'] = ''.$chuoimaunick[1].'';
$colornickk['colornick'] = ''.$chuoimaunick[1].'';
}
if ($user['rights'] == 9 ) {
$colornick['colornick'] = ''.$chuoimaunick[0].'';
$colornickk['colornick'] = ''.$chuoimaunick[0].'';
}
if ($user['rights'] == 10 ) {
$colornick['colornick'] = ''.$chuoimaunick[1].'';
$colornickk['colornick'] = ''.$chuoimaunick[1].'';
}
}
if ($user['rights'] == 10 ) {
$out .= !$user_id || $user_id == $user['id'] ? '<a
href="../users/profile.php?user=' . $user['id'] . '"><span style="color:
rgb(255, 0, 0); background: url(/images/6.gif) repeat scroll 0% 0%
transparent;" border="0"><b>' . $user['name'] . '
</b></span></a>' : '<a href="../users/profile.php?user=' .
$user['id'] . '"><span style="color: rgb(255, 0, 0); background:
url(/images/6.gif) repeat scroll 0% 0% transparent;"
border="0"><b>' . $user['name'] . ' </b></span></a>';
}else {

$out .= !$user_id || $user_id == $user['id'] ? '<a
href="../users/profile.php?user=' . $user['id'] . '"><span
style="color:#' . $colornick['colornick'] . '"><b>' . $user['name'] . '
</b></span></a>' : '<a href="../users/profile.php?user=' .
$user['id'] . '"><span style="color:#' . $colornickk['colornick'] .
'"><b>' . $user['name'] . ' </b></span></a>';
}

//    $out .= '<a href="/user/perevod.php?id=' .$user['id'].
'><font color="#FFA500"><small>' .$user['balans']. '
Xu</font></small></a>';
$out .= (time() > $user['lastdate'] + 300 ? '<font
color="red"> [Off]</font>' : '<font color="green"> [ON]</font>');
if (!empty($arg['header']))
$out .= ' ' . $arg['header'];
if (!isset($arg['stshide']) && !empty($user['status']))
$out .= '</div>';
if (self::$user_set['avatar'])
$out .= '</td></tr></table>';
}

return $out;
} 

public static function display_user($user = false, $arg = false)
{

global $rootpath, $mod,$website,$set,$chuoimaunick,$chuoichucvu;
$out = false;

if (!$user['id']) {
$out = '<b>' . self::$lng['guest'] . '</b>';
if (!empty($user['name']))
$out .= ': ' . $user['name'];
if (!empty($arg['header']))
$out .= ' ' . $arg['header'];
} else {
if (self::$user_set['avatar']) {
$out .= '<table cellpadding="0" cellspacing="0"><tr><td>';
 $var.='<img class="avatar" src="http://up.infoviet.net/avatar/' . $user['id'] . '.png"  width="32" height="32" onerror="this.src=\'http://img.infoviet.net/images/empty.png\'"; width="32" height="32" alt="' . $user_id . '" align="top" "/>&#160;';
     

$out .= '</td><td>';
}
if ($user['sex'])
$out .= '<div class="newsx" style="border-bottom:2px solid #cecece"><img src="http://img.infoviet.net/theme/' . self::$user_set['skin'] . '/images/' . ($user['sex'] == 'm' ? 'm' : 'w') . ($user['datereg'] > time() - 86400 ? '_new' : '')
. '.png" width="16" height="16" align="middle" alt="' . ($user['sex'] == 'm' ? 'М' : 'Ж') . '" />&#160;';
else
$out .= '<img src="http://img.infoviet.net/images/del.png" width="12" height="12" align="middle" />&#160;';
if($set['caidat']==null) {
if ($user['rights'] == 0 ) {
$colornick['colornick'] = '4e387e';
$colornickk['colornick'] = '4e387e';
}
if ($user['rights'] == 1 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($user['rights'] == 2 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($user['rights'] == 3 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($user['rights'] == 4 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($user['rights'] == 5 ) {
$colornick['colornick'] = '008000';
$colornickk['colornick'] = '008000';
}
if ($user['rights'] == 6 ) {
$colornick['colornick'] = '43d6d8';
$colornickk['colornick'] = '43d6d8';
}
if ($user['rights'] == 7 ) {
$colornick['colornick'] = 'ff0000';
$colornickk['colornick'] = 'ff0000';
}
if ($user['rights'] == 9 ) {
$colornick['colornick'] = 'ff00ff';
$colornickk['colornick'] = 'ff00ff';
}
if ($user['rights'] == 10 ) {
$colornick['colornick'] = 'ff0000';
$colornickk['colornick'] = 'ff0000';
} } else{
if ($user['rights'] == 0 ) {
$colornick['colornick'] = ''.$chuoimaunick[4].'';
$colornickk['colornick'] = ''.$chuoimaunick[4].'';
}
if ($user['rights'] == 1 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
$colornickk['colornick'] = ''.$chuoimaunick[3].'';
}
if ($user['rights'] == 2 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
$colornickk['colornick'] = ''.$chuoimaunick[3].'';
}
if ($user['rights'] == 3 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
$colornickk['colornick'] = ''.$chuoimaunick[3].'';
}
if ($user['rights'] == 4 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
$colornickk['colornick'] = ''.$chuoimaunick[3].'';
}
if ($user['rights'] == 5 ) {
$colornick['colornick'] = ''.$chuoimaunick[3].'';
$colornickk['colornick'] = ''.$chuoimaunick[3].'';
}
if ($user['rights'] == 6 ) {
$colornick['colornick'] = ''.$chuoimaunick[2].'';
$colornickk['colornick'] = ''.$chuoimaunick[2].'';
}
if ($user['rights'] == 7 ) {
$colornick['colornick'] = ''.$chuoimaunick[1].'';
$colornickk['colornick'] = ''.$chuoimaunick[1].'';
}
if ($user['rights'] == 9 ) {
$colornick['colornick'] = ''.$chuoimaunick[0].'';
$colornickk['colornick'] = ''.$chuoimaunick[0].'';
}
if ($user['rights'] == 10 ) {
$colornick['colornick'] = ''.$chuoimaunick[1].'';
$colornickk['colornick'] = ''.$chuoimaunick[1].'';
}
}
if ($user['rights'] == 10 ) {
$out .= !$user_id || $user_id == $user['id'] ? '<a
href="../users/profile.php?user=' . $user['id'] . '"><span style="color:
rgb(255, 0, 0); background: url(/images/6.gif) repeat scroll 0% 0%
transparent;" border="0"><b>' . $user['name'] . '
</b></span></a>' : '<a href="../users/profile.php?user=' .
$user['id'] . '"><span style="color: rgb(255, 0, 0); background:
url(/images/6.gif) repeat scroll 0% 0% transparent;"
border="0"><b>' . $user['name'] . ' </b></span></a>';
}else {

$out .= !$user_id || $user_id == $user['id'] ? '<a
href="../users/profile.php?user=' . $user['id'] . '"><span
style="color:#' . $colornick['colornick'] . '"><b>' . $user['name'] . '
</b></span></a>' : '<a href="../users/profile.php?user=' .
$user['id'] . '"><span style="color:#' . $colornickk['colornick'] .
'"><b>' . $user['name'] . ' </b></span></a>';
}
if($set['caidat']==null){
$rank = array(
0 => '<font color="gray">(</font><font
color="black">Member</font><font color="gray">)</font>',
1 => '<font color="gray">(</font><font
color="#9900ff">Auto</font><font color="gray">)</font>',
3 => '<font color="gray">(</font><font
color="green">Mod</font><font color="gray">)</font>',
4 => '<font color="gray">(</font><font
color="green>DMod</font><font color="gray">)</font>',
6 => '<font color="gray">(</font><font
color="blue">Smod</font><font color="gray">)</font>',
7 => '<font color="gray">(</font><font
color="red">Admin</font><font color="gray">)</font>',
9 => '<font color="gray">(</font><font
color="red">Người Sáng Lập</font><font color="gray">)</font>',
10 => '<font color="gray">(</font><font
color="red">Máy Chém Tự Động</font><font
color="gray">)</font>'
);
} else {

$rank= array(
0 => '<font color="gray">(</font><font
color="black">'.$chuoichucvu[4].'</font><font color="gray">)</font>',
1 => '<font color="gray">(</font><font
color="green">'.$chuoichucvu[3].'</font><font color="gray">)</font>',
3 => '<font color="gray">(</font><font
color="green">'.$chuoichucvu[3].'</font><font color="gray">)</font>',
4 => '<font color="gray">(</font><font
color="green>'.$chuoichucvu[3].'</font><font color="gray">)</font>',
6 => '<font color="gray">(</font><font
color="blue">'.$chuoichucvu[2].'</font><font color="gray">)</font>',
7 => '<font color="gray">(</font><font
color="red">'.$chuoichucvu[1].'</font><font color="gray">)</font>',
9 => '<font color="gray">(</font><font color="red">'.$chuoichucvu[0].'</font><font color="gray">)</font>',
10 => '<font color="gray">(</font><font color="red">Máy Chém
Tự Động</font><font color="gray">)</font>'
);}
$out .= ' ' . $rank[$user['rights']];
//    $out .= '<a href="/user/perevod.php?id=' .$user['id'].
'><font color="#FFA500"><small>' .$user['balans']. '
Xu</font></small></a>';
$out .= (time() > $user['lastdate'] + 300 ? '<font
color="red"> [Off]</font>' : '<font color="green"> [ON]</font>');
if (!empty($arg['header']))
$out .= ' ' . $arg['header'];
if (!isset($arg['stshide']) && !empty($user['status']))
$out .= '</div>';
if (self::$user_set['avatar'])
$out .= '</td></tr></table>';
}
if (isset($arg['body']))
$out .= '<div>' . $arg['body'] . '</div>';
$ipinf = !isset($arg['iphide']) && (self::$user_rights || ($user['id'] && $user['id'] == self::$user_id)) ? 1 : 0;
$lastvisit = time() > $user['lastdate'] + 300 && isset($arg['lastvisit']) ? self::display_date($user['lastdate']) : false;
if ($ipinf || $lastvisit || isset($arg['sub']) && !empty($arg['sub']) || isset($arg['footer'])) {
$out .= '<div class="sub">';
if (isset($arg['sub']))
$out .= '<div>' . $arg['sub'] . '</div>';
if ($lastvisit)
$out .= '<div><span class="gray">' . self::$lng['last_visit'] . ':</span> ' . $lastvisit . '</div>';
$iphist = '';
if ($ipinf) {
$out.='<div><span class="gray">' . self::$lng['ip_address'] . ':</span> ';
$hist = $mod == 'history' ? '&amp;mod=history' : '';
$ip = long2ip($user['ip']);
if (self::$user_rights && isset($user['ip_via_proxy']) && $user['ip_via_proxy']) {
$out .= '<b class="red"><a href="' . self::$system_set['homeurl'] . '/' . self::$system_set['admp'] . '/index.php?act=search_ip&amp;ip=' . $ip . $hist . '">' . $ip . '</a></b> / ';
$out .= '<a href="' . self::$system_set['homeurl'] . '/' . self::$system_set['admp'] . '/index.php?act=search_ip&amp;ip=' . long2ip($user['ip_via_proxy']) . $hist . '">' . long2ip($user['ip_via_proxy']) . '</a>';
} elseif (self::$user_rights) {
$out .= '<a href="' . self::$system_set['homeurl'] . '/' . self::$system_set['admp'] . '/index.php?act=search_ip&amp;ip=' . $ip . $hist . '">' . $ip . '</a>';
} else {
$out .= $ip . $iphist;
}
// if (isset($arg['iphist'])) {
// $iptotal = mysql_result(mysql_query("SELECT COUNT(*) FROM `cms_users_iphistory` where `website` = '$website' AND  `user_id` = '" . $user['id'] . "'"), 0);
// $out .= '<div><span class="gray">' . self::$lng['ip_history'] . ':</span> <a href="' . self::$system_set['homeurl'] . '/users/profile.php?act=ip&amp;user=' . $user['id'] . '">[' . $iptotal . ']</a></div>';
// }
$out .= '</div>';
}
if (isset($arg['footer']))
$out .= $arg['footer'];
$out .= '</div>';
}
return $out;
}

/*
-----------------------------------------------------------------
Форматирование имени файла
-----------------------------------------------------------------
*/
public static function format($name)
{
$f1 = strrpos($name, ".");
$f2 = substr($name, $f1 + 1, 999);
$fname = strtolower($f2);
return $fname;
}

/*
-----------------------------------------------------------------
Получаем данные пользователя
-----------------------------------------------------------------
*/
public static function get_user($id = false)
{
global $website;
if ($id && $id != self::$user_id) {
$req = mysql_query("SELECT * FROM `users` where `website` = '$website' AND  `id` = '$id'");
if (mysql_num_rows($req)) {
return mysql_fetch_assoc($req);
} else {
return false;
}
} else {
return self::$user_data;
}
}

/*
-----------------------------------------------------------------
Транслитерация с Русского в латиницу
-----------------------------------------------------------------
*/
public static function rus_lat($str)
{
$replace = array(
'а' => 'a',
'б' => 'b',
'в' => 'v',
'г' => 'g',
'д' => 'd',
'е' => 'e',
'ё' => 'e',
'ж' => 'j',
'з' => 'z',
'и' => 'i',
'й' => 'i',
'к' => 'k',
'л' => 'l',
'м' => 'm',
'н' => 'n',
'о' => 'o',
'п' => 'p',
'р' => 'r',
'с' => 's',
'т' => 't',
'у' => 'u',
'ф' => 'f',
'х' => 'h',
'ц' => 'c',
'ч' => 'ch',
'ш' => 'sh',
'щ' => 'sch',
'ъ' => "",
'ы' => 'y',
'ь' => "",
'э' => 'ye',
'ю' => 'yu',
'я' => 'ya'
);
return strtr($str, $replace);
}

/*
-----------------------------------------------------------------
Обработка смайлов
-----------------------------------------------------------------
*/
public static function smileys($str, $adm = false)
{
global $rootpath;
static $smileys_cache = array();
if (empty($smileys_cache)) {
$file = $rootpath . 'files/cache/smileys.dat';
if (file_exists($file) && ($smileys = file_get_contents($file)) !== false) {
$smileys_cache = unserialize($smileys);
return strtr($str, ($adm ? array_merge($smileys_cache['usr'], $smileys_cache['adm']) : $smileys_cache['usr']));
} else {
return $str;
}
} else {
return strtr($str, ($adm ? array_merge($smileys_cache['usr'], $smileys_cache['adm']) : $smileys_cache['usr']));
}
}

/*
-----------------------------------------------------------------
Функция пересчета на дни, или часы
-----------------------------------------------------------------
*/
public static function timecount($var)
{
global $lng;
if ($var < 0) $var = 0;
$day = ceil($var / 86400);
if ($var > 345600) return $day . ' ' . $lng['timecount_days'];
if ($var >= 172800) return $day . ' ' . $lng['timecount_days_r'];
if ($var >= 86400) return '1 ' . $lng['timecount_day'];
return date("G:i:s", mktime(0, 0, $var));
}

/*
-----------------------------------------------------------------
Транслитерация текста
-----------------------------------------------------------------
*/

/*
-----------------------------------------------------------------
Транслитерация текста
-----------------------------------------------------------------
*/
public static function mikdaik($text)
{
$text = html_entity_decode(trim($text), ENT_QUOTES, 'UTF-8');
$text=str_replace(" ","-", $text);$text=str_replace("--","-", $text);
$text=str_replace("@","-",$text);$text=str_replace("/","-",$text);
$text=str_replace("\\","-",$text);$text=str_replace(":","",$text);
$text=str_replace("\"","",$text);$text=str_replace("'","",$text);
$text=str_replace("<","",$text);$text=str_replace(">","",$text);
$text=str_replace(",","",$text);$text=str_replace("?","",$text);
$text=str_replace(";","",$text);$text=str_replace(".","",$text);
$text=str_replace("[","",$text);$text=str_replace("]","",$text);
$text=str_replace("(","",$text);$text=str_replace(")","",$text);
$text=str_replace("́","", $text);
$text=str_replace("̀","", $text);
$text=str_replace("̃","", $text);
$text=str_replace("̣","", $text);
$text=str_replace("̉","", $text);
$text=str_replace("*","",$text);$text=str_replace("!","",$text);
$text=str_replace("$","-",$text);$text=str_replace("&","-and-",$text);
$text=str_replace("%","",$text);$text=str_replace("#","",$text);
$text=str_replace("^","",$text);$text=str_replace("=","",$text);
$text=str_replace("+","",$text);$text=str_replace("~","",$text);
$text=str_replace("`","",$text);$text=str_replace("--","-",$text);
$text = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $text);
$text = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $text);
$text = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $text);
$text = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $text);
$text = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $text);
$text = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $text);
$text = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $text);
$text = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $text);
$text = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $text);
$text = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $text);
$text = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $text);
$text = preg_replace("/(đ)/", 'd', $text);
$text = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $text);
$text = preg_replace("/(đ)/", 'd', $text);
$text = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $text);
$text = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $text);
$text = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $text);
$text = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $text);
$text = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $text);
$text = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $text);
$text = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $text);
$text = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $text);
$text = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $text);
$text = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $text);
$text = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $text);
$text = preg_replace("/(Đ)/", 'D', $text);
$text = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $text);
$text = preg_replace("/(Đ)/", 'D', $text);
$text=strtolower($text);
return $text;
}
////////////////////////
public static function display_pagination2($base_url, $start, $max_value, $num_per_page)
{
$neighbors = 2;
if ($start >= $max_value)
$start = max(0, (int)$max_value - (((int)$max_value % (int)$num_per_page) == 0 ? $num_per_page : ((int)$max_value % (int)$num_per_page)));
else
$start = max(0, (int)$start - ((int)$start % (int)$num_per_page));
$base_link = '<a class="pagenav" href="' . strtr($base_url, array('%' => '%%')) . '_p%d.html' . '">%s</a>';
$out[] = $start == 0 ? '' : sprintf($base_link, $start / $num_per_page, '&lt;&lt;');
if ($start > $num_per_page * $neighbors)
$out[] = sprintf($base_link, 1, '1');
if ($start > $num_per_page * ($neighbors + 1))
$out[] = '<span style="font-weight: bold;">...</span>';
for ($nCont = $neighbors; $nCont >= 1; $nCont--)
if ($start >= $num_per_page * $nCont) {
$tmpStart = $start - $num_per_page * $nCont;
$out[] = sprintf($base_link, $tmpStart / $num_per_page + 1, $tmpStart / $num_per_page + 1);
}
$out[] = '<span class="currentpage"><b>' . ($start / $num_per_page + 1) . '</b></span>';
$tmpMaxPages = (int)(($max_value - 1) / $num_per_page) * $num_per_page;
for ($nCont = 1; $nCont <= $neighbors; $nCont++)
if ($start + $num_per_page * $nCont <= $tmpMaxPages) {
$tmpStart = $start + $num_per_page * $nCont;
$out[] = sprintf($base_link, $tmpStart / $num_per_page + 1, $tmpStart / $num_per_page + 1);
}
if ($start + $num_per_page * ($neighbors + 1) < $tmpMaxPages)
$out[] = '<span style="font-weight: bold;">...</span>';
if ($start + $num_per_page * $neighbors < $tmpMaxPages)
$out[] = sprintf($base_link, $tmpMaxPages / $num_per_page + 1, $tmpMaxPages / $num_per_page + 1);
if ($start + $num_per_page < $max_value) {
$display_page = ($start + $num_per_page) > $max_value ? $max_value : ($start / $num_per_page + 2);
$out[] = sprintf($base_link, $display_page, '&gt;&gt;');
}
return implode(' ', $out);
}
public static function trans($str)
{
$replace = array(
"A"=>"A","a"=>"a",
"B"=>"B","b"=>"b",
"C"=>"C","c"=>"c",
"D"=>"D","d"=>"d",
"E"=>"E","e"=>"e",
"F"=>"F","f"=>"f",
"G"=>"G","g"=>"g",
"H"=>"H","h"=>"h",
"I"=>"I","i"=>"i",
"J"=>"J","j"=>"j",
"K"=>"K","k"=>"k",
"L"=>"L","l"=>"l",
"M"=>"M","m"=>"m",
"N"=>"N","n"=>"n",
"O"=>"O","o"=>"o",
"P"=>"P","p"=>"p",
"R"=>"R","r"=>"r",
"S"=>"S","s"=>"s",
"T"=>"T","t"=>"t",
"U"=>"U","u"=>"u",
"V"=>"V","v"=>"v",
"W"=>"W","w"=>"w",
"Y"=>"Y","y"=>"y",
"Z"=>"Z","z"=>"z",
"As"=>"Á","Ax"=>"Ã","Aj"=>"Ạ","Af"=>"À","Ar"=>"Ả",
"Es"=>"É","Ex"=>"Ẽ","Ej"=>"Ẹ","Ef"=>"È","Er"=>"Ẻ",
"Ys"=>"Ý","Yx"=>"Ỹ","Yj"=>"Ỵ","Yf"=>"Ỳ","Yr"=>"Ỷ",
"Us"=>"Ú","Ux"=>"Ũ","Uj"=>"Ụ","Uf"=>"Ù","Ur"=>"Ủ",
"Os"=>"Ó","Ox"=>"Õ","Oj"=>"Ọ","Of"=>"Ò","Or"=>"Ỏ",
"Is"=>"Í","Ix"=>"Ĩ","Ij"=>"Ị","If"=>"Ì","Ir"=>"Ỉ",
"Aas"=>"Ấ","Aax"=>"Ẫ","Aaj"=>"Ậ","Aaf"=>"Ầ","Aar"=>"Ẩ",
"Ees"=>"Ế","Eex"=>"Ễ","Eej"=>"Ệ","Eef"=>"Ề","Eer"=>"Ể",
"Oos"=>"Ố","Oox"=>"Ỗ","Ooj"=>"Ộ","Oof"=>"Ồ","Oor"=>"Ổ",
"Ows"=>"Ớ","Owx"=>"Ớ","Owj"=>"Ợ","Owf"=>"Ờ","Owr"=>"Ở",
"Aws"=>"Ẵ","Awx"=>"Ẵ","Awj"=>"Ặ","Awf"=>"Ằ","Awr"=>"Ẳ",
"Uws"=>"Ứ","Uwx"=>"Ữ","Uwj"=>"Ự","Uwf"=>"Ừ","Uwr"=>"Ử",
"as"=>"á","ax"=>"ã","aj"=>"ạ","af"=>"à","ar"=>"ả",
"es"=>"é","ex"=>"ẽ","ej"=>"ẹ","ef"=>"è","er"=>"ẻ",
"ys"=>"ý","yx"=>"ỹ","yj"=>"ỵ","yf"=>"ỳ","yr"=>"ỷ",
"us"=>"ú","ux"=>"ũ","uj"=>"ụ","uf"=>"ù","ur"=>"ủ",
"os"=>"ó","ox"=>"õ","oj"=>"ọ","of"=>"ò","or"=>"ỏ",
"is"=>"í","ix"=>"ĩ","ij"=>"ị","if"=>"ì","ir"=>"ỉ",
"aas"=>"ấ","aax"=>"ẫ","aaj"=>"ậ","aaf"=>"ầ","aar"=>"ẩ",
"ees"=>"ế","eex"=>"ễ","eej"=>"ệ","eef"=>"ề","eer"=>"ể",
"oos"=>"ố","oox"=>"ỗ","ooj"=>"ộ","oof"=>"ồ","oor"=>"ổ",
"ees"=>"ế","eex"=>"ễ","eej"=>"ệ","eef"=>"ề","eer"=>"ể",
"ows"=>"ớ","owx"=>"ớ","owj"=>"ợ","owf"=>"ờ","owr"=>"ở",
"aws"=>"ắ","awx"=>"ẵ","awj"=>"ặ","awf"=>"ằ","awr"=>"ẳ",
"uws"=>"ứ","uwx"=>"ữ","uwj"=>"ự","uwf"=>"ừ","uwr"=>"ử",
"uw"=>"ư","aw"=>"ă","aa"=>"â","oo"=>"ô", "ee"=>"ê",
"ow"=>"ơ", "dd"=>"đ","uw"=>"ư",
"lon"=>"l*n", "dit"=>"d*t", "dyt"=>"d*t",
"djt"=>"d*t", "địt"=>"đ*t", "lồn"=>"l*n", "dcm"=>"đập con
muỗi", "dkm"=>"đập con muỗi", "fuck"=>"fuc*"
);
return strtr($str, $replace);
}
    public static function display_htrang($user = false, $arg = false)
    {
        global $rootpath, $mod;
        $out = false;
            $out .= !self::$user_id || self::$user_id == $user['id'] ? '<b>' . $user['name'] . '</b>' : '<a href="' . self::$system_set['homeurl'] . '/users/profile.php?user=' . $user['id'] . '"><b>' . $user['name'] . '</b></a>';
        return $out;
    }
}
?>
